PROJECT( LesionSizingSandbox )

# Install stuff

set(BUILD_SHARED_LIBS ${ITK_BUILD_SHARED_LIBS})
set(LesionSizingSandbox_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})


if(NOT LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)
  set(LesionSizingSandbox_INSTALL_NO_DEVELOPMENT 0)
endif(NOT LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)

if(NOT LesionSizingSandbox_INSTALL_NO_RUNTIME)
  set(LesionSizingSandbox_INSTALL_NO_RUNTIME 0)
endif(NOT LesionSizingSandbox_INSTALL_NO_RUNTIME)

set(LesionSizingSandbox_INSTALL_NO_LIBRARIES)
if(LesionSizingSandbox_BUILD_SHARED_LIBS)
  if(LesionSizingSandbox_INSTALL_NO_RUNTIME AND LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)
    set(LesionSizingSandbox_INSTALL_NO_LIBRARIES 1)
  endif(LesionSizingSandbox_INSTALL_NO_RUNTIME AND LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)
else(LesionSizingSandbox_BUILD_SHARED_LIBS)
  if(LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)
    set(LesionSizingSandbox_INSTALL_NO_LIBRARIES 1)
  endif(LesionSizingSandbox_INSTALL_NO_DEVELOPMENT)
endif(LesionSizingSandbox_BUILD_SHARED_LIBS)

if(NOT LesionSizingSandbox_INSTALL_BIN_DIR)
  set(LesionSizingSandbox_INSTALL_BIN_DIR "/bin")
endif(NOT LesionSizingSandbox_INSTALL_BIN_DIR)

if(NOT LesionSizingSandbox_INSTALL_LIB_DIR)
  set(LesionSizingSandbox_INSTALL_LIB_DIR "/lib/${PROJECT_NAME}")
endif(NOT LesionSizingSandbox_INSTALL_LIB_DIR)

if(NOT LesionSizingSandbox_INSTALL_PACKAGE_DIR)
  set(LesionSizingSandbox_INSTALL_PACKAGE_DIR ${LesionSizingSandbox_INSTALL_LIB_DIR}
    CACHE INTERNAL "")
endif(NOT LesionSizingSandbox_INSTALL_PACKAGE_DIR)


SET(LesionSizingSandbox_SRCS
  itkNumericTraitsMatrixPixel.cxx
  )

ADD_LIBRARY(LesionSizingSandbox ${LesionSizingSandbox_SRCS})

TARGET_LINK_LIBRARIES(LesionSizingSandbox ITKIO ITKNumerics ITKCommon)

if(NOT LesionSizingSandbox_INSTALL_NO_LIBRARIES)
    install_targets(
      ${LesionSizingSandbox_INSTALL_LIB_DIR}
      RUNTIME_DIRECTORY ${LesionSizingSandbox_INSTALL_BIN_DIR}
      LesionSizingSandbox)
endif(NOT LesionSizingSandbox_INSTALL_NO_LIBRARIES)




