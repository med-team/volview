/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVolViewApplication.h"

#include "vtkKWSplashScreen.h"
#include "vtkKWTkUtilities.h"
#include "vtkObjectFactory.h"

// Splash screen

#include "Resources/VolViewSplashScreen.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVolViewApplication);
vtkCxxRevisionMacro(vtkVolViewApplication, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
void vtkVolViewApplication::CreateSplashScreen()
{
  const unsigned int width = image_VolViewSplashScreen_width;
  const unsigned int height = image_VolViewSplashScreen_height;
  const unsigned int pixel_size = image_VolViewSplashScreen_pixel_size;
  const unsigned int nb_sections = image_VolViewSplashScreen_nb_sections;
  const unsigned long length = image_VolViewSplashScreen_length;
  const unsigned char **sections = image_VolViewSplashScreen_sections;
  
  unsigned char *buffer = new unsigned char [length];
  unsigned char *ptr = buffer;
  for (unsigned int i = 0; i < nb_sections; i++)
    {
    size_t len = strlen((const char*)sections[i]);
    memcpy(ptr, sections[i], len);
    ptr += len;
    }
  
  if (!vtkKWTkUtilities::UpdatePhoto(
        this->GetMainInterp(),
        "KWVolViewDefaultSplashScreen", 
        buffer, width, height, pixel_size, length))
    {
    vtkWarningMacro("Error updating splashscreen ");
    }
  else
    {
    this->GetSplashScreen()->SetImageName("KWVolViewDefaultSplashScreen");
    }

  delete [] buffer; 
}

//----------------------------------------------------------------------------
void vtkVolViewApplication::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
