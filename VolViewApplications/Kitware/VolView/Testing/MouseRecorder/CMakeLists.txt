##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
IF(NOT VolView_DATA_ROOT)
  MESSAGE(FATAL_ERROR 
    "Testing is enabled but VolView_DATA_ROOT is not defined. Please set VolView_DATA_ROOT to the location of the testing data directory or disable testing.")
ENDIF(NOT VolView_DATA_ROOT)

SET(MOUSE_RECORDER_SCRIPTS
  GUITest3Dcursor
  GUITestAngleWidget
  GUITestBiDimensionalMeasurementWidget
  GUITestContourSegmentation2D
  GUITestContourSegmentation2DSliceBySlice
  GUITestContourWidget
  GUITestCropWidget
  GUITestDistance
  GUITestMIP
  GUITestOblique
  GUITestPanels
  GUITestPanelsB
  GUITestWindowLevel
)

GET_TARGET_PROPERTY(EXECUTABLE VolViewLauncher LOCATION)
IF(NOT EXECUTABLE)
  GET_TARGET_PROPERTY(EXECUTABLE VolView LOCATION)
ENDIF(NOT EXECUTABLE)
SET(TEST_PREFIX "VVC-")

# Creates directory if it doesn't exist
SET(MOUSERECORDER_TEMP_DIR "C:/TEMP")
MAKE_DIRECTORY("${MOUSERECORDER_TEMP_DIR}/") 

STRING(REGEX REPLACE "\\$\\(OutDir\\)/" "" 
  driver "${KWVolView_MOUSE_RECORDER_DRIVER_EXE}")

FOREACH(script ${MOUSE_RECORDER_SCRIPTS})
  ADD_TEST(${TEST_PREFIX}${script}
    ${driver}
    --script              "${CMAKE_CURRENT_SOURCE_DIR}/SelfPlayingScripts/${script}.exe"
    --baseline-snapshot  "${VolView_DATA_ROOT}/Baseline/SelfPlayingScripts/${script}.png"
    --generated-snapshot "${MOUSERECORDER_TEMP_DIR}/${script}.png"
    --volview3-executable "${EXECUTABLE}"
    --test-description    "GUI Testing with an external keyboard-mouse event recorder"
    --load-data           "${VolView_DATA_ROOT}/Data/Synarc/MRSiemens/Image100.dcm"
    --temp-directory      "${EXECUTABLE_OUTPUT_PATH}/../Testing/Temporary"
    ) 
ENDFOREACH(script)
