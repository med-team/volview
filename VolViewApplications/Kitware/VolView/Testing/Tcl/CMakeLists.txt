##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
IF(NOT VolView_DATA_ROOT)
  MESSAGE(FATAL_ERROR 
    "Testing is enabled but VolView_DATA_ROOT is not defined. Please set VolView_DATA_ROOT to the location of the testing data directory or disable testing.")
ENDIF(NOT VolView_DATA_ROOT)

INCLUDE("${KWVolView_CMAKE_DIR}/KWVolViewTestingMacros.cmake")

GET_TARGET_PROPERTY(EXECUTABLE VolViewLauncher LOCATION)
IF(NOT EXECUTABLE)
  GET_TARGET_PROPERTY(EXECUTABLE VolView LOCATION)
ENDIF(NOT EXECUTABLE)
STRING(REGEX REPLACE "\\$\\(OutDir\\)/" "" EXECUTABLE "${EXECUTABLE}")
SET(TEST_PREFIX "VVC-")

SET(APPLICATION_NAME vtkVVApplication)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Synarc-MR-Siemens
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Synarc/MRSiemens/Image100.dcm
  ${VolView_DATA_ROOT}/Baseline/Synarc/MRSiemens/Slice.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}AAA-CT-Siemens
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/AAA/CTSiemens/OK-000140_anon.D3I
  ${VolView_DATA_ROOT}/Baseline/AAA/CTSiemens/Slice.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head1
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0001.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0001.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head2
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0002.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0002.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head3
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0003.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0003.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head4
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0004.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0004.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head5
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0005.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0005.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Head6
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00020001/0002.1
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Head/00020001/0002.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-ToshibaAquilion-Chest
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-ToshibaAquilion-Chest/1.2.392.200036.9116.2.2.2.1762676583.1077596893.116155.001
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-ToshibaAquilion-Chest/00020001/1.2.392.200036.9116.2.2.2.1762676583.1077596893.116155.001.png)
 
KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Coactiv-HelCT-GE-HiSpeed-PixelPaddVal
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0005.001
  ${VolView_DATA_ROOT}/Baseline/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0005.001.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}CT-ChestWContrast-GELightSpeed16
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/CTTherapyAccess/ChestWContrast-GELightSpeed16/9999.9999.5.dcm
  ${VolView_DATA_ROOT}/Baseline/CTTherapyAccess/ChestWContrast-GELightSpeed16/9999.9999.5.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}MR-Neck-PhilipsOutlookProview0.23T
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/IHE-PDI-DICOM/PHILIPS/070801A/20010807/10083045/00000002
  ${VolView_DATA_ROOT}/Baseline/IHE-PDI-DICOM/PHILIPS/070801A/20010807/10083045/00000002.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}MR-Neck-PhilipsOutlookProview0.23T-a
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  ${VolView_DATA_ROOT}/Data/IHE-PDI-DICOM/PHILIPS/070801A/20010807/9584406/00000004
  ${VolView_DATA_ROOT}/Baseline/IHE-PDI-DICOM/PHILIPS/070801A/20010807/9584406/00000004.png)

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Osirix-PET-CT-pet
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Osirix/PET-CT/PT604001001.dcm"
  "${VolView_DATA_ROOT}/Baseline/Osirix/PET-CT/PT604001001.png") 

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Osirix-PET-CT-ct
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Osirix/PET-CT/CT002002001.dcm"
  "${VolView_DATA_ROOT}/Baseline/Osirix/PET-CT/CT002002001.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Osirix-CT-Siemens-CardiacAngioWContr
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Osirix/AGECANONIX/Specials-1CoronaryCTA_with_spiral_CTA_pre/CorCTA-w-c-1.0-B20f/IM-0001-0001.dcm"
  "${VolView_DATA_ROOT}/Baseline/Osirix/AGECANONIX/Specials-1CoronaryCTA_with_spiral_CTA_pre/CorCTA-w-c-1.0-B20f/IM-0001-0001a.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}Osirix-CT-Cardiac-SiemensSensation64
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Osirix/Cardiac 1CTA_CORONARY_ARTERIES_TESTBOLUS \(Adult\)/Chest Topo  0.6  T20s/IM-0001-0001.dcm"
  "${VolView_DATA_ROOT}/Baseline/Osirix/Cardiac 1CTA_CORONARY_ARTERIES_TESTBOLUS \(Adult\)/Chest Topo  0.6  T20s/IM-0001-0001.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewTIFF-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/TiffDataset/stack_unablated.TIF"
  "${VolView_DATA_ROOT}/Baseline/TiffDataset/stack_unablated.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewTIFF-RLE-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Tif/DartmouthRLECompressed/3007d_reg10_HbO1.tif"
  "${VolView_DATA_ROOT}/Baseline/Tif/DartmouthRLECompressed/slice.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewMHD-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/RAW/IntensityRaw/Intensity.mhd"
  "${VolView_DATA_ROOT}/Baseline/Raw/IntensityRaw/IntensityMHD.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewRaw-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/RAW/IntensityRaw/Intensity0000.raw"
  "${VolView_DATA_ROOT}/Baseline/Raw/IntensityRaw/IntensityRaw.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewPGM-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/PGM/IntensityPGM/Intensity0000.pgm"
  "${VolView_DATA_ROOT}/Baseline/PGM/IntensityPGM/IntensityPGM.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewPNG-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/PNG/IntensityPNG/Intensity0000.png"
  "${VolView_DATA_ROOT}/Baseline/PNG/IntensityPNG/IntensityPNG.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewBMP-Resize-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/BMP/IntensityBMP/Intensity00.bmp"
  "${VolView_DATA_ROOT}/Baseline/BMP/IntensityBMP/IntensityBMP.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewSLC-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/SLC/bolt/bolt.slc"
  "${VolView_DATA_ROOT}/Baseline/SLC/bolt/bolt.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewSLC-Respaced-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/SLC/bolt_respaced/bolt.slc"
  "${VolView_DATA_ROOT}/Baseline/SLC/bolt_respaced/boltRespaced.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewSTK-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/STK/10xcalib/10xcalib.stk"
  "${VolView_DATA_ROOT}/Baseline/STK/10xcalib/10xcalib.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewSTK-Respaced-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/STK/10xcalibRespaced/10xcalib.stk"
  "${VolView_DATA_ROOT}/Baseline/STK/10xcalibRespaced/10xcalibRespaced.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewAnalyze-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Analyze/CTTherapyAssess/20000101-091423-2-3.hdr"
  "${VolView_DATA_ROOT}/Baseline/Analyze/CTTherapyAssess/20000101-091423-2-3.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewAnalyze-Respaced-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/Analyze/CTTherapyAssessRespaced/20000101-091423-2-3.hdr"
  "${VolView_DATA_ROOT}/Baseline/Analyze/CTTherapyAssessRespaced/20000101-091423-2-3Respaced.png")

KWVolView_TEST_LOAD_VOLUME(${TEST_PREFIX}VolViewLSM-Test
  ${EXECUTABLE}
  ${APPLICATION_NAME}
  "${VolView_DATA_ROOT}/Data/LSM/blastocyste.lsm"
  "${VolView_DATA_ROOT}/Baseline/LSM/blastocyste.png")







