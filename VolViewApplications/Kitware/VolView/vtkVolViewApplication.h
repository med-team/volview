/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVolViewApplication - the VolView application class.
// .SECTION Description
// A subclass of vtkKWApplication specific to VolView, which requires
// its own splashscreen. This class is bound to disappear once the KWWidgets
// resource framework is in place.

#ifndef __vtkVolViewApplication_h
#define __vtkVolViewApplication_h

#include "vtkVVApplication.h"

class VTK_EXPORT vtkVolViewApplication : public vtkVVApplication
{
public:
  static vtkVolViewApplication* New();
  vtkTypeRevisionMacro(vtkVolViewApplication,vtkVVApplication);
  void PrintSelf(ostream& os, vtkIndent indent);
  
protected:
  vtkVolViewApplication() {};
  ~vtkVolViewApplication() {};

  // Description:
  // Create the splash screen
  virtual void CreateSplashScreen();

private:
  vtkVolViewApplication(const vtkVolViewApplication&); // Not implemented
  void operator=(const vtkVolViewApplication&); // Not implemented
};

#endif



