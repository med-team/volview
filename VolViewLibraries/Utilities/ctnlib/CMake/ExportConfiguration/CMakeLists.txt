# Generate the CTNLIBConfig.cmake file in the build tree. Also configure
# one for installation. The file tells external projects how to use
# CTNLIB.

# Help store a literal dollar in a string.  CMake 2.2 allows escaped
# dollars but we have to support CMake 2.0.
SET(DOLLAR "$")

#-----------------------------------------------------------------------------
# Settings shared between the build tree and install tree.

EXPORT_LIBRARY_DEPENDENCIES(
  ${CTNLIB_BINARY_DIR}/CTNLIBLibraryDepends.cmake)
INCLUDE(${CMAKE_ROOT}/Modules/CMakeExportBuildSettings.cmake)
CMAKE_EXPORT_BUILD_SETTINGS(
  ${CTNLIB_BINARY_DIR}/CTNLIBBuildSettings.cmake)

IF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
  INSTALL_FILES(${CTNLIB_INSTALL_PACKAGE_DIR} FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/UseCTNLIB.cmake
    ${CTNLIB_BINARY_DIR}/CTNLIBLibraryDepends.cmake
    ${CTNLIB_BINARY_DIR}/CTNLIBBuildSettings.cmake
    )
ENDIF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)

#-----------------------------------------------------------------------------
# Settings specific to the build tree.

# The install-only section is empty for the build tree.
SET(CTNLIB_CONFIG_INSTALL_ONLY)

# The "use" file.
SET(CTNLIB_USE_FILE_CONFIG 
  ${CMAKE_CURRENT_SOURCE_DIR}/UseCTNLIB.cmake)

# The build settings file.
SET(CTNLIB_BUILD_SETTINGS_FILE_CONFIG 
  ${CTNLIB_BINARY_DIR}/CTNLIBBuildSettings.cmake)

# The library directories.
SET(CTNLIB_LIBRARY_DIRS_CONFIG ${CTNLIB_LIBRARY_DIRS})

# The runtime directories.
SET(CTNLIB_RUNTIME_DIRS_CONFIG ${CTNLIB_RUNTIME_DIRS})

# The include directories.
SET(CTNLIB_INCLUDE_DIRS_CONFIG ${CTNLIB_INCLUDE_PATH})

# The library dependencies file.
SET(CTNLIB_LIBRARY_DEPENDS_FILE 
  ${CTNLIB_BINARY_DIR}/CTNLIBLibraryDepends.cmake)

# The CMake macros dir.
SET(CTNLIB_CMAKE_DIR_CONFIG 
  ${CTNLIB_CMAKE_DIR})

# The build configuration information.
SET(CTNLIB_CONFIGURATION_TYPES_CONFIG ${CTNLIB_CONFIGURATION_TYPES})
SET(CTNLIB_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})

# Configure CTNLIBConfig.cmake for the build tree.
CONFIGURE_FILE(
  ${CMAKE_CURRENT_SOURCE_DIR}/CTNLIBConfig.cmake.in
  ${CTNLIB_BINARY_DIR}/CTNLIBConfig.cmake @ONLY IMMEDIATE)

#-----------------------------------------------------------------------------
# Settings specific to the install tree.

# The "use" file.
SET(CTNLIB_USE_FILE_CONFIG 
  ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_PACKAGE_DIR}/UseCTNLIB.cmake)

# The build settings file.
SET(CTNLIB_BUILD_SETTINGS_FILE_CONFIG 
  ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_PACKAGE_DIR}/CTNLIBBuildSettings.cmake)

# The library directories.
IF(CYGWIN AND CTNLIB_BUILD_SHARED_LIBS)
  # In Cygwin programs directly link to the .dll files.
  SET(CTNLIB_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_BIN_DIR})
ELSE(CYGWIN AND CTNLIB_BUILD_SHARED_LIBS)
  SET(CTNLIB_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_LIB_DIR})
ENDIF(CYGWIN AND CTNLIB_BUILD_SHARED_LIBS)

# The runtime directories.
IF(WIN32)
  SET(CTNLIB_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_BIN_DIR})
ELSE(WIN32)
  SET(CTNLIB_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_LIB_DIR})
ENDIF(WIN32)

# The include directories.
SET(CTNLIB_INCLUDE_DIRS_CONFIG
  ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_INCLUDE_DIR})

# The library dependencies file.
SET(CTNLIB_LIBRARY_DEPENDS_FILE 
  ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_PACKAGE_DIR}/CTNLIBLibraryDepends.cmake)

# The CMake macros dir.
SET(CTNLIB_CMAKE_DIR_CONFIG 
  ${DOLLAR}{CTNLIB_INSTALL_PREFIX}${CTNLIB_INSTALL_PACKAGE_DIR}/CMake)

# The build configuration information.
# The install tree only has one configuration.
SET(CTNLIB_CONFIGURATION_TYPES_CONFIG)

# Configure CTNLIBConfig.cmake for the install tree.

# Construct the proper number of GET_FILENAME_COMPONENT(... PATH)
# calls to compute the installation prefix from CTNLIB_DIR.
STRING(REGEX REPLACE "/" ";" CTNLIB_INSTALL_PACKAGE_DIR_COUNT
  "${CTNLIB_INSTALL_PACKAGE_DIR}")
SET(CTNLIB_CONFIG_INSTALL_ONLY "
# Compute the installation prefix from CTNLIB_DIR.
SET(CTNLIB_INSTALL_PREFIX \"${DOLLAR}{CTNLIB_DIR}\")
")
FOREACH(p ${CTNLIB_INSTALL_PACKAGE_DIR_COUNT})
  SET(CTNLIB_CONFIG_INSTALL_ONLY
    "${CTNLIB_CONFIG_INSTALL_ONLY}GET_FILENAME_COMPONENT(CTNLIB_INSTALL_PREFIX \"${DOLLAR}{CTNLIB_INSTALL_PREFIX}\" PATH)\n"
    )
ENDFOREACH(p)

IF(CMAKE_CONFIGURATION_TYPES)
  # There are multiple build configurations.  Configure one
  # CTNLIBConfig.cmake for each configuration.
  FOREACH(config ${CMAKE_CONFIGURATION_TYPES})
    SET(CTNLIB_BUILD_TYPE_CONFIG ${config})
    CONFIGURE_FILE(
      ${CMAKE_CURRENT_SOURCE_DIR}/CTNLIBConfig.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${config}/CTNLIBConfig.cmake
      @ONLY IMMEDIATE)
  ENDFOREACH(config)

  # Install the config file corresponding to the build configuration
  # specified when building the install target.  The BUILD_TYPE variable
  # will be set while CMake is processing the install files.
  IF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${CTNLIB_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${DOLLAR}{BUILD_TYPE}/CTNLIBConfig.cmake)
  ENDIF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
ELSE(CMAKE_CONFIGURATION_TYPES)
  # There is only one build configuration. Configure one CTNLIBConfig.cmake.
  SET(CTNLIB_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})
  CONFIGURE_FILE(
    ${CMAKE_CURRENT_SOURCE_DIR}/CTNLIBConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/CTNLIBConfig.cmake @ONLY IMMEDIATE)

  # Setup an install rule for the config file.
  IF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${CTNLIB_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/CTNLIBConfig.cmake)
  ENDIF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
ENDIF(CMAKE_CONFIGURATION_TYPES)
