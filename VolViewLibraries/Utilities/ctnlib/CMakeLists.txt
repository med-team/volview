CMAKE_MINIMUM_REQUIRED(VERSION 2.4)
IF(COMMAND CMAKE_POLICY)
  CMAKE_POLICY(SET CMP0003 NEW)
ENDIF(COMMAND CMAKE_POLICY)

PROJECT(CTNLIB)

# --------------------------------------------------------------------------
# To use this library in a larger project you will need to SUBDIR into this
# directory so that it gets built and then you will use the following variables
# in your CMakeLists files to get the proper include paths and libraries

SET(CTNLIB_INCLUDE_PATH 
  "${CTNLIB_SOURCE_DIR}/facilities/condition;${CTNLIB_SOURCE_DIR}/facilities/objects;${CTNLIB_SOURCE_DIR}/facilities/dicom;${CTNLIB_SOURCE_DIR}/facilities/lst;${CTNLIB_SOURCE_DIR}/facilities/thread;${CTNLIB_SOURCE_DIR}/facilities/uid;${CTNLIB_BINARY_DIR}/facilities/dicom"
  CACHE INTERNAL "include paths for ctnlib"
  )
SET(CTNLIB_LIBRARIES ctnlib 
  CACHE INTERNAL "libraries for ctnlib")

# You will also need to define a value for the following variables:
# CTNLIB_INSTALL_BIN_DIR          - binary dir (executables)
# CTNLIB_INSTALL_LIB_DIR          - library dir (libs)
# CTNLIB_INSTALL_DATA_DIR         - share dir (say, examples, data, etc)
# CTNLIB_INSTALL_INCLUDE_DIR      - include dir (headers)
# CTNLIB_INSTALL_PACKAGE_DIR      - package/export configuration files
# CTNLIB_INSTALL_NO_DEVELOPMENT   - do not install development files
# CTNLIB_INSTALL_NO_RUNTIME       - do not install runtime files
# CTNLIB_INSTALL_NO_DOCUMENTATION - do not install documentation files
# --------------------------------------------------------------------------

# --------------------------------------------------------------------------
# Version

SET(CTNLIB_MAJOR_VERSION 1)
SET(CTNLIB_MINOR_VERSION 0)

# --------------------------------------------------------------------------
# Output directories

IF(NOT LIBRARY_OUTPUT_PATH)
  SET(LIBRARY_OUTPUT_PATH ${CTNLIB_BINARY_DIR}/bin CACHE INTERNAL
    "Single output directory for building all libraries.")
ENDIF(NOT LIBRARY_OUTPUT_PATH)
SET(CTNLIB_LIBRARY_DIRS ${LIBRARY_OUTPUT_PATH})
SET(CTNLIB_RUNTIME_DIRS ${LIBRARY_OUTPUT_PATH})

IF(NOT EXECUTABLE_OUTPUT_PATH)
  SET(EXECUTABLE_OUTPUT_PATH ${CTNLIB_BINARY_DIR}/bin CACHE INTERNAL
    "Single output directory for building all executables.")
ENDIF(NOT EXECUTABLE_OUTPUT_PATH)

# --------------------------------------------------------------------------
# Some flags

INCLUDE_REGULAR_EXPRESSION("^.*\\.[ch]$")
IF(CMAKE_COMPILER_2005)
  ADD_DEFINITIONS(-D_CRT_SECURE_NO_DEPRECATE -D_CRT_NONSTDC_NO_DEPRECATE)
ENDIF(CMAKE_COMPILER_2005)

# with -fPIC
IF(UNIX AND NOT WIN32)
  FIND_PROGRAM(CMAKE_UNAME uname /bin /usr/bin /usr/local/bin )
  IF(CMAKE_UNAME)
    EXEC_PROGRAM(uname ARGS -m OUTPUT_VARIABLE CMAKE_SYSTEM_PROCESSOR)
    SET(CMAKE_SYSTEM_PROCESSOR ${CMAKE_SYSTEM_PROCESSOR} CACHE INTERNAL "processor type (i386 and x86_64)")
    IF(CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
      ADD_DEFINITIONS(-fPIC)
    ENDIF(CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
  ENDIF(CMAKE_UNAME)
ENDIF(UNIX AND NOT WIN32)

SET(BUILD_SHARED_LIBS 0)
SET(CTNLIB_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})

SET(CTNLIB_CMAKE_DIR "${CTNLIB_SOURCE_DIR}/CMake")
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CTNLIB_CMAKE_DIR}")

SET(CTNLIB_CONFIGURATION_TYPES ${CMAKE_CONFIGURATION_TYPES})

INCLUDE(${CMAKE_ROOT}/Modules/CheckTypeSize.cmake)
CHECK_TYPE_SIZE(int CMAKE_SIZEOF_INT)
CHECK_TYPE_SIZE(long CMAKE_SIZEOF_LONG)
CHECK_TYPE_SIZE(short CMAKE_SIZEOF_SHORT)

INCLUDE(${CMAKE_ROOT}/Modules/TestBigEndian.cmake)
TEST_BIG_ENDIAN(CMAKE_WORDS_BIGENDIAN2)

IF(APPLE)
  SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fno-common")
  SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-common")
ENDIF(APPLE)

MARK_AS_ADVANCED(CMAKE_BACKWARDS_COMPATIBILITY)

# --------------------------------------------------------------------------
# Install directories

STRING(TOLOWER ${PROJECT_NAME} project_name)

IF(NOT CTNLIB_INSTALL_BIN_DIR)
  SET(CTNLIB_INSTALL_BIN_DIR "/bin")
ENDIF(NOT CTNLIB_INSTALL_BIN_DIR)

IF(NOT CTNLIB_INSTALL_LIB_DIR)
  SET(CTNLIB_INSTALL_LIB_DIR "/lib/${project_name}")
ENDIF(NOT CTNLIB_INSTALL_LIB_DIR)

IF(NOT CTNLIB_INSTALL_DATA_DIR)
  SET(CTNLIB_INSTALL_DATA_DIR "/share/${project_name}")
ENDIF(NOT CTNLIB_INSTALL_DATA_DIR)

IF(NOT CTNLIB_INSTALL_INCLUDE_DIR)
  SET(CTNLIB_INSTALL_INCLUDE_DIR "/include/${project_name}")
ENDIF(NOT CTNLIB_INSTALL_INCLUDE_DIR)

IF(NOT CTNLIB_INSTALL_PACKAGE_DIR)
  SET(CTNLIB_INSTALL_PACKAGE_DIR ${CTNLIB_INSTALL_LIB_DIR}
    CACHE INTERNAL "")
ENDIF(NOT CTNLIB_INSTALL_PACKAGE_DIR)

IF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
  SET(CTNLIB_INSTALL_NO_DEVELOPMENT 0)
ENDIF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)

IF(NOT CTNLIB_INSTALL_NO_RUNTIME)
  SET(CTNLIB_INSTALL_NO_RUNTIME 0)
ENDIF(NOT CTNLIB_INSTALL_NO_RUNTIME)

IF(NOT CTNLIB_INSTALL_NO_DOCUMENTATION)
  SET(CTNLIB_INSTALL_NO_DOCUMENTATION 0)
ENDIF(NOT CTNLIB_INSTALL_NO_DOCUMENTATION)

SET(CTNLIB_INSTALL_NO_LIBRARIES)
IF(CTNLIB_BUILD_SHARED_LIBS)
  IF(CTNLIB_INSTALL_NO_RUNTIME AND CTNLIB_INSTALL_NO_DEVELOPMENT)
    SET(CTNLIB_INSTALL_NO_LIBRARIES 1)
  ENDIF(CTNLIB_INSTALL_NO_RUNTIME AND CTNLIB_INSTALL_NO_DEVELOPMENT)
ELSE(CTNLIB_BUILD_SHARED_LIBS)
  IF(CTNLIB_INSTALL_NO_DEVELOPMENT)
    SET(CTNLIB_INSTALL_NO_LIBRARIES 1)
  ENDIF(CTNLIB_INSTALL_NO_DEVELOPMENT)
ENDIF(CTNLIB_BUILD_SHARED_LIBS)

# --------------------------------------------------------------------------
# Sources

SET(ctnlib_SRCS
  facilities/condition/condition.c
  facilities/lst/lst.c
  facilities/objects/dcm.c
  facilities/objects/dcmcond.c
  facilities/objects/dcmdict.c
  facilities/objects/dcmsupport.c
)

# --------------------------------------------------------------------------
# Include dirs

INCLUDE_DIRECTORIES(${CTNLIB_INCLUDE_PATH})

CONFIGURE_FILE(
  ${CTNLIB_SOURCE_DIR}/.NoDartCoverage 
  ${CTNLIB_BINARY_DIR}/.NoDartCoverage)

CONFIGURE_FILE(
  ${CTNLIB_SOURCE_DIR}/facilities/dicom/dicom_platform.h.in 
  ${CTNLIB_BINARY_DIR}/facilities/dicom/dicom_platform.h)

# --------------------------------------------------------------------------
# Build the library

ADD_LIBRARY(ctnlib STATIC ${ctnlib_SRCS})

IF(NOT WIN32 AND NOT APPLE AND SHARED AND CMAKE_SYSTEM_NAME MATCHES "HP")
  SET_TARGET_PROPERTIES(ctnlib PROPERTIES LINK_FLAGS "-multi_module")
ENDIF(NOT WIN32 AND NOT APPLE AND SHARED AND CMAKE_SYSTEM_NAME MATCHES "HP")

IF(NOT CTNLIB_INSTALL_NO_LIBRARIES)
  INSTALL_TARGETS(
    ${CTNLIB_INSTALL_LIB_DIR}
    RUNTIME_DIRECTORY ${CTNLIB_INSTALL_BIN_DIR}
    ${CTNLIB_LIBRARIES})
ENDIF(NOT CTNLIB_INSTALL_NO_LIBRARIES)

# --------------------------------------------------------------------------
# Install the headers

IF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)
  INSTALL_FILES(${CTNLIB_INSTALL_INCLUDE_DIR} .h 
  facilities/condition/condition.h
  facilities/dicom/dicom.h
  facilities/lst/lst.h
  facilities/objects/dicom_objects.h
  facilities/uid/dicom_uids.h
  ${CTNLIB_BINARY_DIR}/facilities/dicom/dicom_platform.h)
ENDIF(NOT CTNLIB_INSTALL_NO_DEVELOPMENT)

# --------------------------------------------------------------------------
# Configure the export configuration

SUBDIRS(CMake/ExportConfiguration)
