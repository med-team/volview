##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
#
# This module is provided as VolViewLibraries_USE_FILE by VolViewLibrariesConfig.cmake.
# It can be INCLUDEd in a project to load the needed compiler and linker
# settings to use VolViewLibraries:
#   FIND_PACKAGE(VolViewLibraries REQUIRED)
#   INCLUDE(${VolViewLibraries_USE_FILE})

IF(NOT VolViewLibraries_USE_FILE_INCLUDED)
  SET(VolViewLibraries_USE_FILE_INCLUDED 1)

  # Load the compiler settings used for VolViewLibraries.
  IF(VolViewLibraries_BUILD_SETTINGS_FILE)
    INCLUDE(${CMAKE_ROOT}/Modules/CMakeImportBuildSettings.cmake)
    CMAKE_IMPORT_BUILD_SETTINGS(${VolViewLibraries_BUILD_SETTINGS_FILE})
  ENDIF(VolViewLibraries_BUILD_SETTINGS_FILE)

  # Add compiler flags needed to use VolViewLibraries.
  SET(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} ${VolViewLibraries_REQUIRED_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} ${VolViewLibraries_REQUIRED_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} ${VolViewLibraries_REQUIRED_EXE_LINKER_FLAGS}")
  SET(CMAKE_SHARED_LINKER_FLAGS
    "${CMAKE_SHARED_LINKER_FLAGS} ${VolViewLibraries_REQUIRED_SHARED_LINKER_FLAGS}")
  SET(CMAKE_MODULE_LINKER_FLAGS
    "${CMAKE_MODULE_LINKER_FLAGS} ${VolViewLibraries_REQUIRED_MODULE_LINKER_FLAGS}")

  # Add include directories needed to use VolViewLibraries.
  INCLUDE_DIRECTORIES(${VolViewLibraries_INCLUDE_DIRS})

  # Add link directories needed to use VolViewLibraries.
  LINK_DIRECTORIES(${VolViewLibraries_LIBRARY_DIRS})

  # Add cmake module path.
  SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${VolViewLibraries_CMAKE_DIR}")

  # Use VTK.
  IF(NOT VolViewLibraries_NO_USE_VTK)
    SET(VTK_DIR ${VolViewLibraries_VTK_DIR})
    FIND_PACKAGE(VTK)
    IF(VTK_FOUND)
      INCLUDE(${VTK_USE_FILE})
    ELSE(VTK_FOUND)
      MESSAGE("VTK not found in VolViewLibraries_VTK_DIR=\"${VolViewLibraries_VTK_DIR}\".")
    ENDIF(VTK_FOUND)
  ENDIF(NOT VolViewLibraries_NO_USE_VTK)

  # Use KWVolView.
  IF(NOT VolViewLibraries_NO_USE_KWVolView)
    SET(KWVolView_DIR ${VolViewLibraries_KWVolView_DIR})
    FIND_PACKAGE(KWVolView)
    IF(KWVolView_FOUND)
      INCLUDE(${KWVolView_USE_FILE})
    ELSE(KWVolView_FOUND)
      MESSAGE("KWVolView not found in VolViewLibraries_KWVolView_DIR=\"${VolViewLibraries_KWVolView_DIR}\".")
    ENDIF(KWVolView_FOUND)
  ENDIF(NOT VolViewLibraries_NO_USE_KWVolView)

  # Use ITK.
  IF(VolViewLibraries_USE_ITK)
    SET(ITK_DIR ${VolViewLibraries_ITK_DIR})
    FIND_PACKAGE(ITK)
    IF(ITK_FOUND)
      INCLUDE(${ITK_USE_FILE})
    ELSE(ITK_FOUND)
      MESSAGE("ITK not found in VolViewLibraries_ITK_DIR=\"${VolViewLibraries_ITK_DIR}\".")
    ENDIF(ITK_FOUND)
  ENDIF(VolViewLibraries_USE_ITK)

ENDIF(NOT VolViewLibraries_USE_FILE_INCLUDED)
