##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
# Generate the VolViewLibrariesConfig.cmake file in the build tree. Also configure
# one for installation. The file tells external projects how to use
# VolViewLibraries.

# Help store a literal dollar in a string.  CMake 2.2 allows escaped
# dollars but we have to support CMake 2.0.
SET(DOLLAR "$")

#-----------------------------------------------------------------------------
# Settings shared between the build tree and install tree.

EXPORT_LIBRARY_DEPENDENCIES(
  ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesLibraryDepends.cmake)
INCLUDE(${CMAKE_ROOT}/Modules/CMakeExportBuildSettings.cmake)
CMAKE_EXPORT_BUILD_SETTINGS(
  ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesBuildSettings.cmake)

IF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)
  INSTALL_FILES(${VolViewLibraries_INSTALL_PACKAGE_DIR} FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/UseVolViewLibraries.cmake
    ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesLibraryDepends.cmake
    ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesBuildSettings.cmake
    )
ENDIF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)

#-----------------------------------------------------------------------------
# Settings specific to the build tree.

# The install-only section is empty for the build tree.
SET(VolViewLibraries_CONFIG_INSTALL_ONLY)

# The "use" file.
SET(VolViewLibraries_USE_FILE_CONFIG 
  ${CMAKE_CURRENT_SOURCE_DIR}/UseVolViewLibraries.cmake)

# The build settings file.
SET(VolViewLibraries_BUILD_SETTINGS_FILE_CONFIG 
  ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesBuildSettings.cmake)

# The library directories.
SET(VolViewLibraries_LIBRARY_DIRS_CONFIG ${VolViewLibraries_LIBRARY_DIRS})

# The runtime directories.
SET(VolViewLibraries_RUNTIME_DIRS_CONFIG ${VolViewLibraries_RUNTIME_DIRS})

# The include directories.
SET(VolViewLibraries_INCLUDE_DIRS_CONFIG ${VolViewLibraries_INCLUDE_PATH})

# The library dependencies file.
SET(VolViewLibraries_LIBRARY_DEPENDS_FILE 
  ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesLibraryDepends.cmake)

# The CMake macros dir.
SET(VolViewLibraries_CMAKE_DIR_CONFIG 
  ${VolViewLibraries_CMAKE_DIR})

# The VTK options.
SET(VolViewLibraries_VTK_DIR_CONFIG ${VTK_DIR})

# The ITK options.
SET(VolViewLibraries_ITK_DIR_CONFIG ${ITK_DIR})

# The KWVolView options.
SET(VolViewLibraries_KWVolView_DIR_CONFIG ${KWVolView_DIR})

# The build configuration information.
SET(VolViewLibraries_CONFIGURATION_TYPES_CONFIG ${VolViewLibraries_CONFIGURATION_TYPES})
SET(VolViewLibraries_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})

# Configure VolViewLibrariesConfig.cmake for the build tree.
CONFIGURE_FILE(
  ${CMAKE_CURRENT_SOURCE_DIR}/VolViewLibrariesConfig.cmake.in
  ${VolViewLibraries_BINARY_DIR}/VolViewLibrariesConfig.cmake @ONLY IMMEDIATE)

#-----------------------------------------------------------------------------
# Settings specific to the install tree.

# The "use" file.
SET(VolViewLibraries_USE_FILE_CONFIG 
  ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_PACKAGE_DIR}/UseVolViewLibraries.cmake)

# The build settings file.
SET(VolViewLibraries_BUILD_SETTINGS_FILE_CONFIG 
  ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_PACKAGE_DIR}/VolViewLibrariesBuildSettings.cmake)

# The library directories.
IF(CYGWIN AND VolViewLibraries_BUILD_SHARED_LIBS)
  # In Cygwin programs directly link to the .dll files.
  SET(VolViewLibraries_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_BIN_DIR})
ELSE(CYGWIN AND VolViewLibraries_BUILD_SHARED_LIBS)
  SET(VolViewLibraries_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_LIB_DIR})
ENDIF(CYGWIN AND VolViewLibraries_BUILD_SHARED_LIBS)

# The runtime directories.
IF(WIN32)
  SET(VolViewLibraries_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_BIN_DIR})
ELSE(WIN32)
  SET(VolViewLibraries_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_LIB_DIR})
ENDIF(WIN32)

# The include directories.
SET(VolViewLibraries_INCLUDE_DIRS_CONFIG
  ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_INCLUDE_DIR})
IF(VolViewLibraries_USE_XML_RW)
  SET(VolViewLibraries_INCLUDE_DIRS_CONFIG ${VolViewLibraries_INCLUDE_DIRS_CONFIG}
    ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_INCLUDE_DIR}/XML)
ENDIF(VolViewLibraries_USE_XML_RW)

# The library dependencies file.
SET(VolViewLibraries_LIBRARY_DEPENDS_FILE 
  ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_PACKAGE_DIR}/VolViewLibrariesLibraryDepends.cmake)

# The CMake macros dir.
SET(VolViewLibraries_CMAKE_DIR_CONFIG 
  ${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}${VolViewLibraries_INSTALL_PACKAGE_DIR}/CMake)

# The VTK options.
# If VTK_BINARY_DIR is set, then VolViewLibraries is part of a larger parent
# project that also builds VTK. In that case, 
# VolViewLibraries_VTK_INSTALL_PACKAGE_DIR should be set to the value of
# VTK_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside VTK whose location will not change when we install.
IF(VTK_BINARY_DIR)
  IF(NOT VolViewLibraries_VTK_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "VolViewLibraries_VTK_INSTALL_PACKAGE_DIR must be set by the parent project to the value of VTK_INSTALL_PACKAGE_DIR it uses to configure VTK.")
  ENDIF(NOT VolViewLibraries_VTK_INSTALL_PACKAGE_DIR)
  SET(VolViewLibraries_VTK_DIR_CONFIG ${VolViewLibraries_VTK_INSTALL_PACKAGE_DIR})
ELSE(VTK_BINARY_DIR)
  SET(VolViewLibraries_VTK_DIR_CONFIG ${VTK_DIR})
ENDIF(VTK_BINARY_DIR)

# The ITK options.
# If ITK_BINARY_DIR is set, then VolViewLibraries is part of a larger parent
# project that also builds ITK. In that case, 
# VolViewLibraries_ITK_INSTALL_PACKAGE_DIR should be set to the value of
# ITK_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside ITK whose location will not change when we install.
IF(ITK_BINARY_DIR)
  IF(NOT VolViewLibraries_ITK_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "VolViewLibraries_ITK_INSTALL_PACKAGE_DIR must be set by the parent project to the value of ITK_INSTALL_PACKAGE_DIR it uses to configure ITK.")
  ENDIF(NOT VolViewLibraries_ITK_INSTALL_PACKAGE_DIR)
  SET(VolViewLibraries_ITK_DIR_CONFIG ${VolViewLibraries_ITK_INSTALL_PACKAGE_DIR})
ELSE(ITK_BINARY_DIR)
  SET(VolViewLibraries_ITK_DIR_CONFIG ${ITK_DIR})
ENDIF(ITK_BINARY_DIR)

# The KWVolView options.
# If KWVolView_BINARY_DIR is set, then VolViewLibraries is part of a larger parent
# project that also builds KWVolView. In that case, 
# VolViewLibraries_KWVolView_INSTALL_PACKAGE_DIR should be set to the value of
# KWVolView_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside KWVolView whose location will not change when we install.
IF(KWVolView_BINARY_DIR)
  IF(NOT VolViewLibraries_KWVolView_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "VolViewLibraries_KWVolView_INSTALL_PACKAGE_DIR must be set by the parent project to the value of KWVolView_INSTALL_PACKAGE_DIR it uses to configure KWVolView.")
  ENDIF(NOT VolViewLibraries_KWVolView_INSTALL_PACKAGE_DIR)
  SET(VolViewLibraries_KWVolView_DIR_CONFIG ${VolViewLibraries_KWVolView_INSTALL_PACKAGE_DIR})
ELSE(KWVolView_BINARY_DIR)
  SET(VolViewLibraries_KWVolView_DIR_CONFIG ${KWVolView_DIR})
ENDIF(KWVolView_BINARY_DIR)

# The build configuration information.
# The install tree only has one configuration.
SET(VolViewLibraries_CONFIGURATION_TYPES_CONFIG)

# Configure VolViewLibrariesConfig.cmake for the install tree.

# Construct the proper number of GET_FILENAME_COMPONENT(... PATH)
# calls to compute the installation prefix from VolViewLibraries_DIR.
STRING(REGEX REPLACE "/" ";" VolViewLibraries_INSTALL_PACKAGE_DIR_COUNT
  "${VolViewLibraries_INSTALL_PACKAGE_DIR}")
SET(VolViewLibraries_CONFIG_INSTALL_ONLY "
# Compute the installation prefix from VolViewLibraries_DIR.
SET(VolViewLibraries_INSTALL_PREFIX \"${DOLLAR}{VolViewLibraries_DIR}\")
")
FOREACH(p ${VolViewLibraries_INSTALL_PACKAGE_DIR_COUNT})
  SET(VolViewLibraries_CONFIG_INSTALL_ONLY
    "${VolViewLibraries_CONFIG_INSTALL_ONLY}GET_FILENAME_COMPONENT(VolViewLibraries_INSTALL_PREFIX \"${DOLLAR}{VolViewLibraries_INSTALL_PREFIX}\" PATH)\n"
    )
ENDFOREACH(p)

IF(CMAKE_CONFIGURATION_TYPES)
  # There are multiple build configurations.  Configure one
  # VolViewLibrariesConfig.cmake for each configuration.
  FOREACH(config ${CMAKE_CONFIGURATION_TYPES})
    SET(VolViewLibraries_BUILD_TYPE_CONFIG ${config})
    CONFIGURE_FILE(
      ${CMAKE_CURRENT_SOURCE_DIR}/VolViewLibrariesConfig.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${config}/VolViewLibrariesConfig.cmake
      @ONLY IMMEDIATE)
  ENDFOREACH(config)

  # Install the config file corresponding to the build configuration
  # specified when building the install target.  The BUILD_TYPE variable
  # will be set while CMake is processing the install files.
  IF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${VolViewLibraries_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${DOLLAR}{BUILD_TYPE}/VolViewLibrariesConfig.cmake)
  ENDIF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)
ELSE(CMAKE_CONFIGURATION_TYPES)
  # There is only one build configuration. Configure one VolViewLibrariesConfig.cmake.
  SET(VolViewLibraries_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})
  CONFIGURE_FILE(
    ${CMAKE_CURRENT_SOURCE_DIR}/VolViewLibrariesConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/VolViewLibrariesConfig.cmake @ONLY IMMEDIATE)

  # Setup an install rule for the config file.
  IF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${VolViewLibraries_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/VolViewLibrariesConfig.cmake)
  ENDIF(NOT VolViewLibraries_INSTALL_NO_DEVELOPMENT)
ENDIF(CMAKE_CONFIGURATION_TYPES)
