/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWCroppingRegionsWidget - widget for manipulating 2D cursor
// .SECTION Description

#ifndef __vtkKWCroppingRegionsWidget_h
#define __vtkKWCroppingRegionsWidget_h

#include "vtkKW3DWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkActor2D;
class vtkImageData;
class vtkLineSource;
class vtkVolumeMapper;
class vtkPolyData;

class VTK_EXPORT vtkKWCroppingRegionsWidget : public vtkKW3DWidget
{
public:
  static vtkKWCroppingRegionsWidget *New();
  vtkTypeRevisionMacro(vtkKWCroppingRegionsWidget, vtkKW3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Place/Adjust widget within bounds
  virtual void PlaceWidget(double bounds[6]);
  void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}

  // Description:
  // Enable/disable the widget
  virtual void SetEnabled(int enabling);

  // Description:
  // Set/Get the plane positions
  vtkGetVector6Macro(PlanePositions, double);
  virtual void SetPlanePositions(double pos[6]) 
    {this->SetPlanePositions(pos[0], pos[1], pos[2], pos[3], pos[4], pos[5]);}
  virtual void SetPlanePositions(double xMin, double xMax, 
                                 double yMin, double yMax,
                                 double zMin, double zMax);

  // Description:
  // Set/Get the cropping region flags
  virtual void SetCroppingRegionFlags(int flags);
  vtkGetMacro(CroppingRegionFlags, int);
  
  // Description:
  // Set/Get the slice type
  // See constant in vtkKW2DRenderWidget
  virtual void SetSliceOrientation(int);
  vtkGetMacro(SliceOrientation, int);
  
  // Description:
  // Set/Get the slice number
  virtual void SetSlice(int num);
  vtkGetMacro(Slice, int);
  
  // Description:
  // Set/Get line 1 color
  virtual void SetLine1Color(double r, double g, double b);
  virtual void SetLine1Color(double rgb[3])
    { this->SetLine1Color(rgb[0], rgb[1], rgb[2]); }
  virtual double *GetLine1Color();
  virtual void GetLine1Color(double rgb[3]);
  
  // Description:
  // Set/Get line 2 color
  virtual void SetLine2Color(double r, double g, double b);
  virtual void SetLine2Color(double rgb[3])
    { this->SetLine2Color(rgb[0], rgb[1], rgb[2]); }
  virtual double *GetLine2Color();
  virtual void GetLine2Color(double rgb[3]);
    
  // Description:
  // Set/Get line 3 color
  virtual void SetLine3Color(double r, double g, double b);
  virtual void SetLine3Color(double rgb[3])
    { this->SetLine3Color(rgb[0], rgb[1], rgb[2]); }
  virtual double *GetLine3Color();
  virtual void GetLine3Color(double rgb[3]);
  
  // Description:
  // Set/Get line 4 color
  virtual void SetLine4Color(double r, double g, double b);
  virtual void SetLine4Color(double rgb[3])
    { this->SetLine4Color(rgb[0], rgb[1], rgb[2]); }
  virtual double *GetLine4Color();
  virtual void GetLine4Color(double rgb[3]);

  // Description:
  // Set/Get the input volume mapper
  // Update the widget according to its mapper
  virtual void SetVolumeMapper(vtkVolumeMapper *mapper);
  vtkGetObjectMacro(VolumeMapper, vtkVolumeMapper);
  virtual void UpdateAccordingToInput();

  // Description:
  // Callbacks
  void MoveHorizontalLine();
  void MoveVerticalLine();
  void MoveIntersectingLines();
  void UpdateCursorIcon();
  void OnButtonPress();
  void OnButtonRelease();
  void OnMouseMove();
  
protected:
  vtkKWCroppingRegionsWidget();
  ~vtkKWCroppingRegionsWidget();

  vtkVolumeMapper *VolumeMapper;
  
  vtkLineSource *LineSources[4];
  vtkActor2D *LineActors[4];
  vtkPolyData* RegionPolyData[9];
  vtkActor2D* RegionActors[9];
  
  double PlanePositions[6];
  
  int SliceOrientation;
  int Slice;

  double GetSlicePosition();

  int CroppingRegionFlags;

  int MouseCursorState;
  int Moving;
  
  // Handles the events

  static void ProcessEvents(vtkObject* object,
                            unsigned long event,
                            void* clientdata,
                            void* calldata);
  
  void ResetCursorIcon();
  void SetMouseCursor(int state);
  
  //BTX
  enum WidgetStates
  {
    NoLine = 0,
    MovingH1AndV1,
    MovingH2AndV1,
    MovingH1AndV2,
    MovingH2AndV2,
    MovingV1,
    MovingV2,
    MovingH1,
    MovingH2
  };
  //ETX

  int ComputeWorldCoordinate(int x, int y, double* coord);

  void UpdateOpacity();
  void UpdateGeometry();
  void ConstrainPlanePositions(double positions[6]);
  
private:
  vtkKWCroppingRegionsWidget(const vtkKWCroppingRegionsWidget&);  //Not implemented
  void operator=(const vtkKWCroppingRegionsWidget&);  //Not implemented
};

#endif
