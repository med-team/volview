/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWScalarBarWidget.h"

#include "vtkObjectFactory.h"
#include "vtkScalarBarActor.h"

vtkCxxRevisionMacro(vtkKWScalarBarWidget, "$Revision: 1.11 $");
vtkStandardNewMacro(vtkKWScalarBarWidget);

//----------------------------------------------------------------------------
void vtkKWScalarBarWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
