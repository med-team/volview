/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWScalarBarWidget - KW subclass for scalar bar widget
// .SECTION Description

#ifndef __vtkKWScalarBarWidget_h
#define __vtkKWScalarBarWidget_h

#include "vtkScalarBarWidget.h"

class vtkKWObject;

class VTK_EXPORT vtkKWScalarBarWidget : public vtkScalarBarWidget
{
public:
  vtkTypeRevisionMacro(vtkKWScalarBarWidget, vtkScalarBarWidget);
  static vtkKWScalarBarWidget* New();
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkKWScalarBarWidget() { }
  ~vtkKWScalarBarWidget() {}

private:
  vtkKWScalarBarWidget(const vtkKWScalarBarWidget&);  //Not implemented
  void operator=(const vtkKWScalarBarWidget&);  //Not implemented
};

#endif
