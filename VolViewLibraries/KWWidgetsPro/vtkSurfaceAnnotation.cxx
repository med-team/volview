/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkSurfaceAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkActor.h"
#include "vtkPolyData.h"
#include "vtkKWRenderWidget.h"
#include "vtkPolyDataMapper.h"

//---------------------------------------------------------------------------

vtkCxxRevisionMacro(vtkSurfaceAnnotation, "$Revision: 1.9 $" );
vtkStandardNewMacro(vtkSurfaceAnnotation);

//---------------------------------------------------------------------------
vtkSurfaceAnnotation::vtkSurfaceAnnotation()
{
  this->Input        = NULL;
  this->RenderWidget = NULL;
  this->Actor = vtkActor::New();
}

//---------------------------------------------------------------------------
vtkSurfaceAnnotation::~vtkSurfaceAnnotation()
{
  this->Close();

  if (this->Actor)
    {
    this->Actor->Delete();
    }
  
  this->SetRenderWidget(NULL);
}

//---------------------------------------------------------------------------
void vtkSurfaceAnnotation::Close()
{
  this->Remove();

  // We assume that Close() means "closing the data", so let's release
  // the input now.

  this->SetInput(NULL);
}

//---------------------------------------------------------------------------
void vtkSurfaceAnnotation::Remove()
{
  if (this->RenderWidget)
    {
    this->RenderWidget->RemoveViewProp(this->Actor);
    }
}

//----------------------------------------------------------------------------
void vtkSurfaceAnnotation::SetRenderWidget(vtkKWRenderWidget *arg)
{
  if (this->RenderWidget == arg)
    {
    return;
    }
    
  this->RenderWidget = arg;

  if (this->RenderWidget && this->Actor->GetMapper())
    {
    this->RenderWidget->AddViewProp(this->Actor);
    }

  this->Modified();
}

//----------------------------------------------------------------------------
void vtkSurfaceAnnotation::SetInput(vtkPolyData *in)
{
  if (this->Input == in)
    {
    return;
    }
  
  if (this->Input)
    {
    this->Input->UnRegister(this);
    }
  this->Input = in;
  if (this->Input)
    {
    this->Input->Register(this);
    }    
  this->Modified();

  if (!in)
    {
    this->Actor->SetMapper(0);
    return;
    }
  
  vtkPolyDataMapper *mpr = vtkPolyDataMapper::New();
  mpr->SetInput(in);
  this->Actor->SetMapper(mpr);
  mpr->Delete();
  
  if (this->RenderWidget)
    {
    this->RenderWidget->AddViewProp(this->Actor);
    }
}

//---------------------------------------------------------------------------
void vtkSurfaceAnnotation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  
  os << indent << "Input: " << this->Input << endl;
  os << indent << "RenderWidget: " << this->RenderWidget << endl;
}
