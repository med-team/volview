/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/* 
 * Resource generated for file:
 *    KWCross.png (zlib, base64)
 */
static const unsigned int  image_KWCross_width          = 30;
static const unsigned int  image_KWCross_height         = 30;
static const unsigned int  image_KWCross_pixel_size     = 4;
static const unsigned long image_KWCross_length         = 152;
static const unsigned long image_KWCross_decoded_length = 3600;

static const unsigned char image_KWCross[] = 
  "eNrtlUsOgCAMRD16bz66MRHCT8oUhZK8LY8hQwFwwHE25lpIwXaKSADbm8t5u62zPjMz/J"
  "be1J3WvNozxJ5Sp0Z1LXf+Vnq7ps06wlvLxPSW9v6SV9Nl6/fzZiYz52RLr2bO55X+wFn/"
  "veP8gRNK0Fu0";

/* 
 * Resource generated for file:
 *    KWFence.png (zlib, base64)
 */
static const unsigned int  image_KWFence_width          = 30;
static const unsigned int  image_KWFence_height         = 30;
static const unsigned int  image_KWFence_pixel_size     = 4;
static const unsigned long image_KWFence_length         = 184;
static const unsigned long image_KWFence_decoded_length = 3600;

static const unsigned char image_KWFence[] = 
  "eNrtlt0KwCAIhffovrntZrBA03ksJhicm4I+jn/FzBe3WoLuxZJ2M4loUiZX8+RVpifvXo"
  "SL+nzYqNeVJ81/JN7v+yxPO7mru62zk9xonqWYWrHUcuDlojUV4Wb1z9ceRmdFRj0hXKR/"
  "KvnN6h+0rpD5jMyr01zk7UXmVYU/zZ/+b63aGg/XotE=";

/* 
 * Resource generated for file:
 *    KWICross.png (zlib, base64)
 */
static const unsigned int  image_KWICross_width          = 30;
static const unsigned int  image_KWICross_height         = 30;
static const unsigned int  image_KWICross_pixel_size     = 4;
static const unsigned long image_KWICross_length         = 172;
static const unsigned long image_KWICross_decoded_length = 3600;

static const unsigned char image_KWICross[] = 
  "eNrtllEKgDAMQz16bx798UNwsKShc26DMBDxkdlmBXBga3ldCxmpzIh4iHmmcLM+b7bDZ+"
  "v7b++xXJdPhuv4nyzX7bO3rrJeW3sVl62rWblq/7rqis3JbFa4z3kWrquPmEz+Sl5VZLNy"
  "H7hzo3K2UO75UTPNqPlt6386AShOMWE=";

/* 
 * Resource generated for file:
 *    KWIFence.png (zlib, base64)
 */
static const unsigned int  image_KWIFence_width          = 30;
static const unsigned int  image_KWIFence_height         = 30;
static const unsigned int  image_KWIFence_pixel_size     = 4;
static const unsigned long image_KWIFence_length         = 152;
static const unsigned long image_KWIFence_decoded_length = 3600;

static const unsigned char image_KWIFence[] = 
  "eNrtllEKwCAMQz16b575M3CjQlzYJhohf4/G2FIEUGBtr3qQ6SnHekbERVk9llNynjVHOT"
  "Vr5stwas72DRlO7ee9Hst9nVPNmvkx3KxzNKtvr5cMp86VchfvK+8r76s999Vbf5q//m/W"
  "ejoAn0hm9Q==";

/* 
 * Resource generated for file:
 *    KWSubvolume.png (zlib, base64)
 */
static const unsigned int  image_KWSubvolume_width          = 30;
static const unsigned int  image_KWSubvolume_height         = 30;
static const unsigned int  image_KWSubvolume_pixel_size     = 4;
static const unsigned long image_KWSubvolume_length         = 120;
static const unsigned long image_KWSubvolume_decoded_length = 3600;

static const unsigned char image_KWSubvolume[] = 
  "eNrt1s0KABAQBGCPvm8+XByV/WGKUZPjR8ksgAbl+4yFTKKmmYUSdbPm3G+bXrfK9LiV5q"
  "6bfbers9y+q1y5cuWe+CNZfcDqPlbPs2Ya1vymvJcOKpcJcA==";

