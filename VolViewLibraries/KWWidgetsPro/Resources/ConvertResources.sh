KWConvertImageToHeader --zlib --base64 \
    vtkKWCrop.h \
    KWCross.png \
    KWFence.png \
    KWICross.png \
    KWIFence.png \
    KWSubvolume.png

KWConvertImageToHeader --zlib --base64 \
    vtkKWLettersTexture.h \
    KWLettersTextureGeneral.png \
    KWLettersTextureMedical.png \

KWConvertImageToHeader --zlib --base64 \
    vtkKWColorPresets.h \
    KWColorPresetBlackToWhite.png \
    KWColorPresetBlue.png \
    KWColorPresetCyan.png \
    KWColorPresetGreen.png \
    KWColorPresetMagenta.png \
    KWColorPresetRainbow.png \
    KWColorPresetRed.png \
    KWColorPresetWhite.png \
    KWColorPresetYellow.png
