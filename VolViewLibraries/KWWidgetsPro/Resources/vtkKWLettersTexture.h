/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/* 
 * Resource generated for file:
 *    KWLettersTextureGeneral.png (zlib, base64)
 */
static const unsigned int  image_KWLettersTextureGeneral_width          = 512;
static const unsigned int  image_KWLettersTextureGeneral_height         = 256;
static const unsigned int  image_KWLettersTextureGeneral_pixel_size     = 3;
static const unsigned long image_KWLettersTextureGeneral_length         = 1708;
static const unsigned long image_KWLettersTextureGeneral_decoded_length = 393216;

static const unsigned char image_KWLettersTextureGeneral[] = 
  "eNrt3TFi2zAQBED9/9NU4caVRFLk4Xg7UydKsjgsAcl2tg0AAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgmtcOa19Q+A1f"
  "XNTiJWRrLHw14Td8ZVGL1yHH1rAp2j4CLIQ5d8ixEJWPUc9i/W/Ozbkhz1wgy+GtCXNuzv"
  "V/4BpZC/1vzmVryJfnWX87diPuszsshDnX//q/co0C16JP+DaFOXfICVyIm/7tR7PNXAv9"
  "n9ZFsm3yCLAQa/v//x8ReyNu8vC1L8y5/tf/1/7zL9wXwr82/Dt+oy4y5095BFiIDv3/96"
  "ckH4o6PHztC3M+MnmHnLUVtHPmkzfFwvDti+JHgPJvcgWwEGUV9PvWEH7Z7rAvzHnCFcBC"
  "6H/he2ta/wdeASxEcQX9sjWEX3ZAsinM+fhHgIVYUkHntobwy3aHtTDnCe8CWQj9L/yj/a"
  "9ezPmAK4AfL3C5+/4yHr59JsGmMOcDrgC+vXRt/+//+wh/bSk5FJnztB0nEPtC+DaFOQ/c"
  "cQIpyy1nLfR/bP9vPlIfvbgqyLlo5MPXocicW1z973Oxkf2vmjaf/8o//ttL9b/+1//m3P"
  "1L1DUVFHgv69n/3po25/rf+29fs/K+nP7HnD80IrH36X9fl7X24XtoRewIc67/LUeTY7Dw"
  "9X/CdU9QlR91GVf9r/9tDf2v/83tfRGtfaNb/9sajyj/qOT1f0gF7VxZ34uk/8cX2ub//+"
  "20LsLp0P8nfqXwbY3H9b851/9pFXR01P0sGv0/u/w9AvR/SAWduOpm3o71f8I7P+Zc/+v/"
  "ry+u//X/7MO/K4D+H19Bp084gUcj/R91+HcF0P/6/6bfK3xbo//h3xVA/9sUtob+N+ceAY"
  "bcjdjt2NYw59bCkDsUORrZGubcWhjy2E1ha9ga5txyGPK0G7Hbsa1hzq2IIQ8/FDka2Rrm"
  "3IoY8thNYWvYGubcohjytBux27GtYc6tiyEHQP8DAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAjtf9hCxqQC"
  "mJWtSAUhK1qAGlJGpRA0pJ1KIGlJKoRQ0oJVGLGnTIhRtfKel/ILP/CzrNqokaGNb/GknU"
  "QGD/ayRRA4H97+1oUQOB/a+RRA0E9r9GEjWg/zWSqIGQ/tdIyh8I7H+NpPyBwP7XSMofCO"
  "x/H0SW/SgGUYNu7/PDYTRSWf+LGvT/4/rf0pT1v20CSkYjDet/UYP+79P/Gqms/0UN+r9P"
  "/2uksv4XNdCn/zVSWf+LGujT/74Kpaz/RQ306X+N1G25BQV8Lo3KUrIKogaG9b9GUv5AYP"
  "9rJOUPBPa/RlL+QGD/+yCyVfmLGqjpf42k/IHA/tdIyh/Q/xppef9LCajpf42k/IHA/tdI"
  "yh/Q/xppbf+LCOh2/l/+X4/lnP9FDSglUYsaUEqiFjWglEQtakApiVrUgFIStagBpSRq/Q"
  "8oJVEDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAMXeMTnPvg==";

/* 
 * Resource generated for file:
 *    KWLettersTextureMedical.png (zlib, base64)
 */
static const unsigned int  image_KWLettersTextureMedical_width          = 512;
static const unsigned int  image_KWLettersTextureMedical_height         = 256;
static const unsigned int  image_KWLettersTextureMedical_pixel_size     = 3;
static const unsigned long image_KWLettersTextureMedical_length         = 1512;
static const unsigned long image_KWLettersTextureMedical_decoded_length = 393216;

static const unsigned char image_KWLettersTextureMedical[] = 
  "eNrt3UGW2zgQREHd/9LsC2jBpoiqAjJiP14kyA+2n8e+LgAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIDRPmsYtuWkrLRi"
  "XuPsVSF7tvffQei//uMrNLn/TkH/9Z/GCtm2vf9OofKYbKX/KuT4RvXfKei//tNVIQvrf8"
  "gZmUv/VcghTuu/Iyg7I4vpvwo5R/3Xf/RfhZzj0sfeEUx7Neym/wnjey9GPfb2138vAmO/"
  "Qk29+rG3v/57EWgZ33uh/+Jvav2PHd9L0f7Y27/mdDzq+m98/df/wI9/j7r+G99R6r/+W1"
  "v/je8oJ2ylSJVHY239N76j1P+0j39r67/x9WeXm9f+9f03uP7rv6Nct7w/kdgYfw+8/hvf"
  "9+e61Pj7N3bvv9n1P2R8CZrcf1Ovex0sr/+B40uQ/od//Ou//quQBFle/+2v/yrkEC0fFX"
  "8/Aui/CqnQwOXNq//6T3v/LVy8vGFH9d+J6L+vUGqWt2px/P0IoP8q5OyWPvZm37r/DkX/"
  "fYXy+2Nv/+1+BHYi+p/z8Nuz4LF3EPrvRcD4sctrzl79dxwSZHz0PzP+jkOCjM+Ly2uO/n"
  "sRMH7s8rKzS/ydhQQZn9eX1xz99yJgfP13jsP77ywkyPi8u7zsLIr/il/W7BJkfK7Cf3nT"
  "UVaG2kFIkPEZ1X+nOaT/DuLZ0+v33PTf8rIzPP5+BNB//adleS+C/uu/wfVf/51p40XpFP"
  "Rf/6lf3rsw5PvcKei//lO/vNdB//Xf2vqv/0626350BPqv/7Qs742Y8GXuCPRf/wEAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAICBPrfZCiAq+24BgPDyuwVaDsU4wJz465L+A7Hxlyb9B2Ljr076D8TGX6D0H0"
  "juv0bpP3BA/J/9h0bWf2DT/v/4KxhZ/4Ht+v/WJWJn/Qf0H/0HTmqL/us/EPj9r//6D8T2"
  "X6b0Hzip/wqj/0Bs/3VG/4Hk/guO/gOuAPHRfyC8/0Kk/4ArQJH0H3AF6JL+A8lXgDrpP+"
  "AKQP+BwFvAnvoPxN4CxtR/IPYiMKP+A5l3gen0H4i9Cyym/0DmRWAo/QcybwET6T9walj0"
  "X/+B3T/X9V//Af0XK/0H9F+s9B84u//P2uL3f/QfyLwC9F//gTP6/6/C+POf+g+c1P87kf"
  "H/f+k/cOoV8DU4/vIH/Qdy+u/vf9N/wBUg/voPuAI0Sv8BV4BA6T8QfgWYVP+BwCvAmPoP"
  "pN0CBtR/IOoiMJf+AwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
  "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcNMfSCQ7eg==";

