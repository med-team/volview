/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/* 
 * Resource generated for file:
 *    KWColorPresetBlackToWhite.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetBlackToWhite_width          = 12;
static const unsigned int  image_KWColorPresetBlackToWhite_height         = 12;
static const unsigned int  image_KWColorPresetBlackToWhite_pixel_size     = 3;
static const unsigned long image_KWColorPresetBlackToWhite_length         = 212;
static const unsigned long image_KWColorPresetBlackToWhite_decoded_length = 432;

static const unsigned char image_KWColorPresetBlackToWhite[] = 
  "eNqNkNENwyAMRDOCWQLPgReBTWATs4gzBywBK7Q4qCJEquLvp7t3Po63BwYQkZwLIcSUmF"
  "nkrKX23hfGIFpypExkziJSSmm9/RgDmkPkQ0gxas7OABhrBxP8lTOY0dXWHGPROnLee/WZ"
  "XX3NGT7TOd18bs7fLkfqHOeuUjcfsOo8t2c+nzlgcGFyZjkvnwej/0nzh7J1/b8PkG2Gdg"
  "==";

/* 
 * Resource generated for file:
 *    KWColorPresetBlue.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetBlue_width          = 12;
static const unsigned int  image_KWColorPresetBlue_height         = 12;
static const unsigned int  image_KWColorPresetBlue_pixel_size     = 3;
static const unsigned long image_KWColorPresetBlue_length         = 32;
static const unsigned long image_KWColorPresetBlue_decoded_length = 432;

static const unsigned char image_KWColorPresetBlue[] = 
  "eNpjYCAK+Pj8x4NG1VBLDUEAADe8nv0=";

/* 
 * Resource generated for file:
 *    KWColorPresetCyan.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetCyan_width          = 12;
static const unsigned int  image_KWColorPresetCyan_height         = 12;
static const unsigned int  image_KWColorPresetCyan_pixel_size     = 3;
static const unsigned long image_KWColorPresetCyan_length         = 32;
static const unsigned long image_KWColorPresetCyan_decoded_length = 432;

static const unsigned char image_KWColorPresetCyan[] = 
  "eNpjYCAK+Pz/jweNqqGWGoIAAF1H5Ok=";

/* 
 * Resource generated for file:
 *    KWColorPresetGreen.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetGreen_width          = 12;
static const unsigned int  image_KWColorPresetGreen_height         = 12;
static const unsigned int  image_KWColorPresetGreen_pixel_size     = 3;
static const unsigned long image_KWColorPresetGreen_length         = 32;
static const unsigned long image_KWColorPresetGreen_decoded_length = 432;

static const unsigned char image_KWColorPresetGreen[] = 
  "eNpjYCAK+Pz3wYNG1VBLDUEAAH2onv0=";

/* 
 * Resource generated for file:
 *    KWColorPresetMagenta.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetMagenta_width          = 12;
static const unsigned int  image_KWColorPresetMagenta_height         = 12;
static const unsigned int  image_KWColorPresetMagenta_pixel_size     = 3;
static const unsigned long image_KWColorPresetMagenta_length         = 32;
static const unsigned long image_KWColorPresetMagenta_decoded_length = 432;

static const unsigned char image_KWColorPresetMagenta[] = 
  "eNpjYCAK/Pf5jweNqqGWGoIAAKMz5Ok=";

/* 
 * Resource generated for file:
 *    KWColorPresetRainbow.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetRainbow_width          = 12;
static const unsigned int  image_KWColorPresetRainbow_height         = 12;
static const unsigned int  image_KWColorPresetRainbow_pixel_size     = 3;
static const unsigned long image_KWColorPresetRainbow_length         = 68;
static const unsigned long image_KWColorPresetRainbow_decoded_length = 432;

static const unsigned char image_KWColorPresetRainbow[] = 
  "eNpjYCAKJNT999ny3+frT5//B7z+l6b8917z3/vDX5//x33+9yaMqqGWGoIAANLYxUU=";

/* 
 * Resource generated for file:
 *    KWColorPresetRed.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetRed_width          = 12;
static const unsigned int  image_KWColorPresetRed_height         = 12;
static const unsigned int  image_KWColorPresetRed_pixel_size     = 3;
static const unsigned long image_KWColorPresetRed_length         = 32;
static const unsigned long image_KWColorPresetRed_decoded_length = 432;

static const unsigned char image_KWColorPresetRed[] = 
  "eNpjYCAK/PfxwYNG1VBLDUEAAMOUnv0=";

/* 
 * Resource generated for file:
 *    KWColorPresetWhite.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetWhite_width          = 12;
static const unsigned int  image_KWColorPresetWhite_height         = 12;
static const unsigned int  image_KWColorPresetWhite_pixel_size     = 3;
static const unsigned long image_KWColorPresetWhite_length         = 28;
static const unsigned long image_KWColorPresetWhite_decoded_length = 432;

static const unsigned char image_KWColorPresetWhite[] = 
  "eNpjYCAK/McLRtVQSw1BAADIvirk";

/* 
 * Resource generated for file:
 *    KWColorPresetYellow.png (zlib, base64)
 */
static const unsigned int  image_KWColorPresetYellow_width          = 12;
static const unsigned int  image_KWColorPresetYellow_height         = 12;
static const unsigned int  image_KWColorPresetYellow_pixel_size     = 3;
static const unsigned long image_KWColorPresetYellow_length         = 32;
static const unsigned long image_KWColorPresetYellow_decoded_length = 432;

static const unsigned char image_KWColorPresetYellow[] = 
  "eNpjYCAK/P/vgweNqqGWGoIAAOkf5Ok=";

