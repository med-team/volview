/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyleImageView - Interactor Style specific for Image
// .SECTION Description
// This is a specialed vtkKWInteractorStyle2DView which add a few more event
// like PlaceMarker3D, Roll and Reslice
// Roll and Reslice are unusual as they do not involve Camera interaction but
// manipulate an instance of vtkImageReslice, this only make sense when doing
// linked widget such as in VolView

#ifndef __vtkKWInteractorStyleImageView_h
#define __vtkKWInteractorStyleImageView_h

#include "vtkKWInteractorStyle2DView.h"

class vtkImageReslice;

class VTK_EXPORT vtkKWInteractorStyleImageView : public vtkKWInteractorStyle2DView
{
public:
  static vtkKWInteractorStyleImageView *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyleImageView, vtkKWInteractorStyle2DView);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Actions to perform when an event happens
  virtual void PlaceMarker3D();
  virtual void WindowLevel();
  virtual void StartWindowLevel();
  virtual void StopWindowLevel();
  virtual void StartPan(){};
  virtual void StopPan();
  virtual void StartZoom(){};
  virtual void StopZoom();
  virtual void Roll();
  virtual void StopRoll();
  virtual void Reslice();
  virtual void StopReslice();
  virtual void InAndOut();
  virtual void StopInAndOut();
  
//BTX
  // Description:
  // Events that this can throw.
  enum 
  {
    ImageCameraFocalPointAndPositionChangedEndEvent = 25000,
    ZoomFactorChangedEndEvent
  };
//ETX


protected:
  vtkKWInteractorStyleImageView();
  ~vtkKWInteractorStyleImageView();

  virtual int PerformAction(const char* action);
  virtual int StopAction(const char* action);

  double InitialWindow;
  double InitialLevel;
  double InitialX;
  double InitialY;

private:
  vtkKWInteractorStyleImageView(const vtkKWInteractorStyleImageView&);  // Not implemented
  void operator=(const vtkKWInteractorStyleImageView&);  // Not implemented
};

#endif
