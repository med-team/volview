/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWCameraLightWidget - widget to control lights in the scene
// .SECTION Description
// vtkKWCameraLightWidget is a widget to control up to 4 lights in a 3D
// view.  The lights can be interactively moved, and their visibility, color,
// and intensity can be changed.

#ifndef __vtkKWCameraLightWidget_h
#define __vtkKWCameraLightWidget_h

#include "vtkKWCompositeWidget.h"

class vtkKWChangeColorButton;
class vtkKWLabel;
class vtkKWCheckButtonWithLabel;
class vtkKWFrameWithLabel;
class vtkKWFrame;
class vtkKWMenuButtonWithLabel;
class vtkKWScaleWithEntry;
class vtkKWVolumeWidget;

class VTK_EXPORT vtkKWCameraLightWidget : public vtkKWCompositeWidget
{
public:
  static vtkKWCameraLightWidget *New();
  vtkTypeRevisionMacro(vtkKWCameraLightWidget, vtkKWCompositeWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Set/get the volume widget you want to control the lights of
  virtual void SetVolumeWidget(vtkKWVolumeWidget *widget);
  vtkGetObjectMacro(VolumeWidget, vtkKWVolumeWidget);

  // Description:
  // Refresh the interface given the value extracted from the current widget.
  virtual void Update();

  // Description:
  // Set the active light
  virtual void SetActiveLight(int);

  // Description:
  // Callbacks to change light visibility, color, and intensity
  virtual void ActiveLightCallback();
  virtual void LightVisibilityCallback(int state);
  virtual void LightColorCallback(double r, double g, double b);
  virtual void LightIntensityCallback(double value);
  
  // Description:
  // Callbacks for mouse interactions to place the lights in the scene
  virtual void MouseButtonPressCallback(int x, int y);
  virtual void MouseMoveCallback(int x, int y);
  virtual void MouseButtonReleaseCallback(int x, int y);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Access to the main labeled frame
  vtkGetObjectMacro(CameraLightFrame, vtkKWFrameWithLabel);

protected:
  vtkKWCameraLightWidget();
  ~vtkKWCameraLightWidget();

  // Description:
  // Create the widget.
  virtual void CreateWidget();

  vtkKWVolumeWidget *VolumeWidget;

  vtkKWFrameWithLabel       *CameraLightFrame;
  vtkKWFrame              *WidgetFrame;
  vtkKWMenuButtonWithLabel  *ActiveLightMenu;
  vtkKWCheckButtonWithLabel *LightVisibilityCheckButton;
  vtkKWChangeColorButton  *LightColorButton;
  vtkKWScaleWithEntry     *LightIntensityScale;
  vtkKWLabel              *LightPreviewLabel;

  //BTX
  class Light
  {
  public:
    int Position[2];
    int Visibility;
    double Color[3];
    double Intensity;
  };
  vtkKWCameraLightWidget::Light *Lights;
  //ETX

  int NumberOfLights;
  int CurrentLight;
  int MovingLight;

  virtual void UpdateCurrentParameters();
  virtual void UpdatePreview();
  virtual void UpdateLights();
  virtual void DeleteLights();

private:
  vtkKWCameraLightWidget(const vtkKWCameraLightWidget&);  //Not implemented
  void operator=(const vtkKWCameraLightWidget&);  //Not implemented
};

#endif
