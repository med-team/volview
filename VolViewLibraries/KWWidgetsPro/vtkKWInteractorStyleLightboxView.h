/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyleLightboxView -
// .SECTION Description
//

#ifndef __vtkKWInteractorStyleLightboxView_h
#define __vtkKWInteractorStyleLightboxView_h

#include "vtkKWInteractorStyle2DView.h"

class vtkImageActor;
class vtkImageData;

class VTK_EXPORT vtkKWInteractorStyleLightboxView : public vtkKWInteractorStyle2DView
{
public:
  static vtkKWInteractorStyleLightboxView *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyleLightboxView, vtkKWInteractorStyle2DView);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Actions to perform when an event happens
  virtual void WindowLevel();
  virtual void StartWindowLevel();
  virtual void StopWindowLevel();
  
  void SetInteractiveMap(vtkKWImageMapToWindowLevelColors *map);
  void SetImageActors(vtkImageActor **actors);
  void SetImageData(vtkImageData *iData);
  
protected:
  vtkKWInteractorStyleLightboxView();
  ~vtkKWInteractorStyleLightboxView();

  vtkKWImageMapToWindowLevelColors *InteractiveMap;
  vtkImageActor **ImageActors;
  vtkImageData *ImageData;

  int InitialRendererIndex;
  
  double InitialWindow;
  double InitialLevel;
  double InitialX;
  double InitialY;
  
private:
  vtkKWInteractorStyleLightboxView(const vtkKWInteractorStyleLightboxView&);  // Not implemented
  void operator=(const vtkKWInteractorStyleLightboxView&);  // Not implemented
};

#endif
