/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWVolumePropertyWidgetPro.h"

#include "vtkObjectFactory.h"
#include "vtkVolumeProperty.h"
#include "vtkKWEvent.h"

#include "vtkKWWidgetsConfigure.h" // Needed for KWWidgets_BUILD_VTK_WIDGETS
#ifdef KWWidgets_BUILD_VTK_WIDGETS
#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW
#endif

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLVolumePropertyWriter.h"
#endif

//----------------------------------------------------------------------------
vtkCxxRevisionMacro(vtkKWVolumePropertyWidgetPro, "$Revision: 1.7 $");
vtkStandardNewMacro(vtkKWVolumePropertyWidgetPro);

//----------------------------------------------------------------------------
void vtkKWVolumePropertyWidgetPro::InvokeVolumePropertyChangedCommand()
{
  this->InvokeObjectMethodCommand(this->VolumePropertyChangedCommand);

  if (!this->VolumeProperty)
    {
    this->InvokeEvent(vtkKWEvent::VolumePropertyChangedEvent, NULL);
    }
  else
    {
#ifdef KWCommonPro_USE_XML_RW
    ostrstream event;

    vtkXMLVolumePropertyWriter *xmlw = vtkXMLVolumePropertyWriter::New();
    xmlw->SetObject(this->VolumeProperty);
    xmlw->SetNumberOfComponents(this->GetNumberOfComponents());
    xmlw->WriteToStream(event);
    xmlw->Delete();

    event << ends;
    
    this->InvokeEvent(vtkKWEvent::VolumePropertyChangedEvent, event.str());
    event.rdbuf()->freeze(0);
#else
    this->InvokeEvent(vtkKWEvent::VolumePropertyChangedEvent, NULL);
#endif
    }
}

//----------------------------------------------------------------------------
void vtkKWVolumePropertyWidgetPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
