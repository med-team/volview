/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWCursorWidget - widget for manipulating 2D cursor
// .SECTION Description

#ifndef __vtkKWCursorWidget_h
#define __vtkKWCursorWidget_h

#include "vtkKW3DWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkActor2D;
class vtkImageData;
class vtkLineSource;

class VTK_EXPORT vtkKWCursorWidget : public vtkKW3DWidget
{
public:
  static vtkKWCursorWidget *New();
  vtkTypeRevisionMacro(vtkKWCursorWidget, vtkKW3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Enable/disable the widget
  virtual void SetEnabled(int enabling);

  // Description:
  // Set/Get the cursor position
  virtual void SetPosition(double pos[3]) 
    { this->SetPosition(pos[0], pos[1], pos[2]); }
  virtual void SetPosition(double x, double y, double z);
  vtkGetVector3Macro(Position, double);

  // Description:
  // Query if the cursor position is in given bounds
  virtual int IsPositionInBounds(double bounds[6]);

  // Description:
  // Place/Adjust widget within bounds
  virtual void PlaceWidget(double bounds[6]);
  void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}
  
  // Description:
  // Set/Get the slice type
  // See constant in vtkKW2DRenderWidget
  virtual void SetSliceOrientation(int);
  vtkGetMacro(SliceOrientation, int);
  
  // Description:
  // Set/Get axis 1 color
  void SetAxis1Color(double r, double g, double b);
  void SetAxis1Color(double rgb[3])
    { this->SetAxis1Color(rgb[0], rgb[1], rgb[2]); }
  double *GetAxis1Color();
  void GetAxis1Color(double rgb[3]);
  
  // Description:
  // Set/Get axis 2 color
  void SetAxis2Color(double r, double g, double b);
  void SetAxis2Color(double rgb[3])
    { this->SetAxis2Color(rgb[0], rgb[1], rgb[2]); }
  double *GetAxis2Color();
  void GetAxis2Color(double rgb[3]);

  vtkSetClampMacro(Interactive, int, 0, 1);
  vtkGetMacro(Interactive, int);
  vtkBooleanMacro(Interactive, int);
  
  // Description:
  // Callbacks
  void MoveCursorHorizontalAxis(int changing);
  void MoveCursorVerticalAxis(int changing);
  void MoveCursorBothAxes(int changing);
  void UpdateCursorIcon();
  void OnButtonPress();
  void OnButtonRelease();
  void OnMouseMove();
  
protected:
  vtkKWCursorWidget();
  ~vtkKWCursorWidget();
  
  vtkLineSource *Axis1Source;
  vtkLineSource *Axis2Source;
  vtkActor2D *Axis1Actor;
  vtkActor2D *Axis2Actor;

  int SliceOrientation;
  int MouseCursorState;
  double Position[3];
  int Moving;

  int Interactive;
  
  // handles the events
  static void ProcessEvents(vtkObject* object,
                            unsigned long event,
                            void* clientdata,
                            void* calldata);
  
  void ResetCursorIcon();
  void SetMouseCursor(int state);
  
//BTX
  enum WidgetStates
  {
    NeitherAxis = 0,
    VerticalAxis,
    HorizontalAxis,
    BothAxes
  };
//ETX

  int ComputeWorldCoordinate(int x, int y, double* coord);

  void UpdatePosition();

private:
  vtkKWCursorWidget(const vtkKWCursorWidget&);  //Not implemented
  void operator=(const vtkKWCursorWidget&);  //Not implemented
};

#endif
