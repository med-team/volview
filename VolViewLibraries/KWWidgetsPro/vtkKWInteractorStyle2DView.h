/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyle2DView - Interactor Style specific for 2D View
// .SECTION Description
// The strings for events are hardcoded, be extremely carefull when copy/pasting
// events

#ifndef __vtkKWInteractorStyle2DView_h
#define __vtkKWInteractorStyle2DView_h

#include "vtkKWInteractorStyleView.h"

class vtkKW2DRenderWidget;
class vtkKWImageMapToWindowLevelColors;
class vtkLineSource;

class VTK_EXPORT vtkKWInteractorStyle2DView : public vtkKWInteractorStyleView
{
public:
  static vtkKWInteractorStyle2DView *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyle2DView, vtkKWInteractorStyleView);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Actions to perform when an event happens
  virtual void StartWindowLevel() {};
  virtual void WindowLevel() {};
  virtual void StopWindowLevel() {};
  virtual void StartPan() {};
  virtual void Pan();
  virtual void StopPan() {};
  virtual void StartZoom() {};
  virtual void Zoom();
  virtual void StopZoom() {};
  virtual void Reset();
  virtual void DecrementSlice();
  virtual void IncrementSlice();
  virtual void DecrementPage();
  virtual void IncrementPage();
  virtual void GoToFirstSlice();
  virtual void GoToLastSlice();

  virtual void SetWindowLevel(double window, double level);
  virtual void Zoom(double factor);
  virtual void SetCameraFocalPointAndPosition(
    double fpx, double fpy, double fpz, double px, double py, double pz);
  virtual void SetCameraFocalPointAndPosition(double a[6])
    { this->SetCameraFocalPointAndPosition(a[0],a[1],a[2],a[3],a[4],a[5]);};

  virtual void SetImageMapToRGBA(vtkKWImageMapToWindowLevelColors *map);
  vtkGetObjectMacro(ImageMapToRGBA, vtkKWImageMapToWindowLevelColors);

  // Description:
  // Event bindings controlling the effects of pressing mouse buttons,
  // moving the mouse, or pressing keys.
  virtual void OnMouseMove();

  // Description:
  // Probe
  virtual void Probe();

  // Description:
  // Mouse wheel is translated into slice navigation
  virtual void OnMouseWheelForward();
  virtual void OnMouseWheelBackward();

protected:
  vtkKWInteractorStyle2DView();
  ~vtkKWInteractorStyle2DView();

  int   InCallback;
  
  vtkKWImageMapToWindowLevelColors *ImageMapToRGBA;

  virtual int StartAction(const char* action);
  virtual int PerformAction(const char* action);
  virtual int StopAction(const char* action);

  // Description:
  // Proxy used to get some vars that are either in the widget
  virtual vtkKW2DRenderWidget* Get2DRenderWidget();
  virtual int GetSliceOrientation();
  virtual char* GetDistanceUnits();

  virtual void InvokeSliceChangedEvent();

  void FormatProbeDisplay(char *display, double location[3], double *values);

private:
  vtkKWInteractorStyle2DView(const vtkKWInteractorStyle2DView&);  // Not implemented
  void operator=(const vtkKWInteractorStyle2DView&);  // Not implemented
};

#endif
