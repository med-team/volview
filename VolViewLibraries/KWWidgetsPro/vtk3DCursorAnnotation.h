/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtk3DCursorAnnotation
// .SECTION Description

#ifndef __vtk3DCursorAnnotation_h
#define __vtk3DCursorAnnotation_h

#include "vtkActor.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkDataSet;
class vtkKWRenderWidget;
class vtkLookupTable;
class vtkPolyData;

class VTK_EXPORT vtk3DCursorAnnotation : public vtkActor
{
public:
  static vtk3DCursorAnnotation* New();
  vtkTypeRevisionMacro(vtk3DCursorAnnotation, vtkActor);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Set/Get the input data for this annotation
  void SetInput(vtkDataSet*);
  vtkGetObjectMacro(Input, vtkDataSet);

  // Description:
  // Set/Get the cursor type
  //BTX  
  enum 
  {
    CURSOR_TYPE_CROSSHAIR = 0,
    CURSOR_TYPE_PLANE     = 1
  };
  //ETX
  void SetCursorType(int type);
  vtkGetMacro(CursorType, int);
  void SetCursorTypeToCrossHairs() 
    { this->SetCursorType(vtk3DCursorAnnotation::CURSOR_TYPE_CROSSHAIR); }
  void SetCursorTypeToPlanes() 
    { this->SetCursorType(vtk3DCursorAnnotation::CURSOR_TYPE_PLANE); }
  
  // Description:
  // Set/Get the color of each axis
  vtkGetVector3Macro(CursorXAxisColor, double);
  vtkGetVector3Macro(CursorYAxisColor, double);
  vtkGetVector3Macro(CursorZAxisColor, double);
  void SetCursorXAxisColor(double r, double g, double b);
  void SetCursorYAxisColor(double r, double g, double b);
  void SetCursorZAxisColor(double r, double g, double b);
  void SetCursorXAxisColor(double rgb[3])
    { this->SetCursorXAxisColor(rgb[0], rgb[1], rgb[2]); };
  void SetCursorYAxisColor(double rgb[3])
    { this->SetCursorYAxisColor(rgb[0], rgb[1], rgb[2]); };
  void SetCursorZAxisColor(double rgb[3])
    { this->SetCursorZAxisColor(rgb[0], rgb[1], rgb[2]); };
  
  // Description:
  // Set/Get the cursor position
  virtual void SetCursorPosition(double x, double y, double z);
  vtkGetVector3Macro(CursorPosition, double);
  virtual void SetCursorPosition(double _arg[3])
    { this->SetCursorPosition(_arg[0], _arg[1], _arg[2]); }
  virtual int IsCursorPositionInBounds(double bounds[6]);
  
  // Description:
  // Set visibility
  void SetVisibility(int state);
  
  // Description:
  // Set/Get the render widget to display the cursor in
  void SetRenderWidget(vtkKWRenderWidget*);
  vtkGetObjectMacro(RenderWidget, vtkKWRenderWidget);

  // Description:
  // Actually render (empty method in superclass)
  void Render(vtkRenderer *ren, vtkMapper *m);

protected:
  vtk3DCursorAnnotation();
  ~vtk3DCursorAnnotation();
  
  vtkDataSet *Input;

  vtkKWRenderWidget *RenderWidget;
  vtkPolyData *Cursor;

  int CursorType;

  double CursorPosition[3];
  double CursorXAxisColor[3];
  double CursorYAxisColor[3];
  double CursorZAxisColor[3];

  void UpdateGeometry();
  void UpdateLookupTableColors();
  
private:
  vtk3DCursorAnnotation(const vtk3DCursorAnnotation&);  // Not implemented
  void operator=(const vtk3DCursorAnnotation&);  // Not implemented
};

#endif
