/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWLightboxWidget
// .SECTION Description

#ifndef __vtkKWLightboxWidget_h
#define __vtkKWLightboxWidget_h

#include "vtkKW2DRenderWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkActor2D;
class vtkImageActor;
class vtkKWInteractorStyleLightboxView;
class vtkKWImageMapToWindowLevelColors;

class VTK_EXPORT vtkKWLightboxWidget : public vtkKW2DRenderWidget
{
public:
  static vtkKWLightboxWidget* New();
  vtkTypeRevisionMacro(vtkKWLightboxWidget, vtkKW2DRenderWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Control the resolution in images by images of this view
  void SetResolution(int i, int j);
  vtkGetVector2Macro(Resolution, int);

  // Description:
  // Get and control the 3D markers, probably all of these routines
  // should be removed. The manipulation of markers whould not be done
  // at this level, remove with event cleanup.
  virtual void SetMarkers3DVisibility(int v);

  // Description:
  // Slice navigation
  virtual void SetSlice(int n);
  virtual void IncrementPage();
  virtual void DecrementPage();
  virtual int GoToDefaultSlice();
  
  // Description:
  // Get the interactor style
  virtual vtkKWInteractorStyle2DView* GetInteractorStyle();
  
  // Description
  // Callbacks
  virtual void SliceSelectedCallback(double value);
 
  // Description:
  // Given mouse coordinate, compute world coordinate, eventually return
  // the id of the renderer if the widget supports multiple renderers
  // (see vtkKWRenderWidget::GetNthRenderer, 
  // vtkKWRenderWidget::GetRendererIndex)
  virtual int ComputeWorldCoordinate(int x, int y, double *result, int *id = 0);

  // Description:
  // Enable/Disable linear interpolation
  virtual void SetInterpolate(int state);
  virtual int  GetInterpolate();

  // Description:
  // Update the color mapping parameters
  virtual void UpdateColorMapping();
  
protected:
  vtkKWLightboxWidget();
  ~vtkKWLightboxWidget();
  
  // Description:
  // Create the widget
  virtual void CreateWidget();
  
  // Description:
  // Connects the internal object together given the Input.
  // Returns 1 on success, 0 on error or to signal that it is safe to abort
  virtual int ConnectInternalPipeline();

  // Description:
  // Update the display extent according to what part of the Input has to
  // be displayed. It is probably the right place to adjust/place the 3D
  // widget which position depends on the display extent.
  virtual void UpdateDisplayExtent();

  // Description:
  // Update the image map
  virtual void UpdateImageMap();

  virtual void UpdateResolution();

  // Description:
  // Create the default renderers inside the render window.
  // Superclass can override to create different renderers.
  // It is called by Create().
  virtual void CreateDefaultRenderers();

  // Description:
  // Install the renderers inside the render window.
  // Superclass can override to install them in a different layout.
  // It is called by Create().
  virtual void InstallRenderers();

  // Description:
  // Populate the context menu
  virtual void PopulateContextMenuWithOptionEntries(vtkKWMenu*);
  virtual void PopulateContextMenuWithCameraEntries(vtkKWMenu*);

  int Resolution[2];
  
  int UpdateExtent[6];
  
  vtkActor2D *BoxFrame;

  vtkImageActor **Images;
  int NumberOfImages;

  vtkKWInteractorStyleLightboxView *InteractorStyle;

  vtkKWImageMapToWindowLevelColors *InteractiveMap;

private:
  vtkKWLightboxWidget(const vtkKWLightboxWidget&);  // Not implemented
  void operator=(const vtkKWLightboxWidget&);  // Not implemented
};

#endif
