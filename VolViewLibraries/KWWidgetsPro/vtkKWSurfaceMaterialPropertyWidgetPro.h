/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWSurfaceMaterialPropertyWidgetPro - widget to control the material property of a polygonal data (vtkProperty)
// .SECTION Description

#ifndef __vtkKWSurfaceMaterialPropertyWidgetPro_h
#define __vtkKWSurfaceMaterialPropertyWidgetPro_h

#include "vtkKWSurfaceMaterialPropertyWidget.h"


class VTK_EXPORT vtkKWSurfaceMaterialPropertyWidgetPro : public vtkKWSurfaceMaterialPropertyWidget
{
public:
  static vtkKWSurfaceMaterialPropertyWidgetPro *New();
  vtkTypeRevisionMacro(vtkKWSurfaceMaterialPropertyWidgetPro, vtkKWSurfaceMaterialPropertyWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  
protected:

  vtkKWSurfaceMaterialPropertyWidgetPro() {};
  ~vtkKWSurfaceMaterialPropertyWidgetPro() {};
  
  // Send an event representing the state of the widget
  virtual void SendStateEvent(int event);

private:
  vtkKWSurfaceMaterialPropertyWidgetPro(const vtkKWSurfaceMaterialPropertyWidgetPro&);  //Not implemented
  void operator=(const vtkKWSurfaceMaterialPropertyWidgetPro&);  //Not implemented
};

#endif
