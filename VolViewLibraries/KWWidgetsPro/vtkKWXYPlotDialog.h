/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWXYPlotDialog - a class to load data into VV

#ifndef __vtkKWXYPlotDialog_h
#define __vtkKWXYPlotDialog_h

#include "vtkKWDialog.h"

class vtkXYPlotActor;
class vtkKWPushButton;
class vtkKWRenderWidget;

class VTK_EXPORT vtkKWXYPlotDialog : public vtkKWDialog
{
public:
  static vtkKWXYPlotDialog* New();
  vtkTypeRevisionMacro(vtkKWXYPlotDialog,vtkKWDialog);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Return the vtkXYPlotActor. This gives indirect access to
  // all the options for plotting.
  vtkXYPlotActor * GetXYPlotActor();

protected:
  vtkKWXYPlotDialog();
  ~vtkKWXYPlotDialog();

  // Description:
  // Create the widget
  virtual void CreateWidget();

private:
  vtkKWXYPlotDialog(const vtkKWXYPlotDialog&); // Not implemented
  void operator=(const vtkKWXYPlotDialog&); // Not Implemented

  vtkKWRenderWidget   *RenderWidget;
  vtkXYPlotActor      *XYPlotActor;
  vtkKWPushButton     *OKButton;
};


#endif

