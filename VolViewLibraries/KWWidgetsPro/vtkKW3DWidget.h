/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKW3DWidget - Parent class for all 3D widgets
// .SECTION Description

#ifndef __vtkKW3DWidget_h
#define __vtkKW3DWidget_h

#include "vtk3DWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkKWObject;

class VTK_EXPORT vtkKW3DWidget : public vtk3DWidget
{
public:
  vtkTypeRevisionMacro(vtkKW3DWidget, vtk3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // This method is used to initially place the widget.
  virtual void PlaceWidget(double bounds[6]);
  virtual void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                           double zmin, double zmax)
    { this->Superclass::PlaceWidget(xmin, xmax, ymin, ymax, zmin, zmax); }
  virtual void PlaceWidget() { this->Superclass::PlaceWidget(); }

  vtkGetVector6Macro(InitialBounds, double);

  // Description:
  // This method is used to try to adjust the widget given some bounds.
  // The default implementation is to call PlaceWidget if the bounds
  // are different than the bounds used in the last call to PlaceWidget.
  virtual void AdjustWidget(double bounds[6]);

  // Description:
  // Set and get the parent widget. The parent widget is not reference
  // counted to prevent circular dependencies.
  virtual void SetParent(vtkKWObject* parent) { this->Parent = parent; }
  virtual vtkKWObject* GetParent() { return this->Parent; }

  // Description:
  // GetCurrentRendererIndex() gets the index of the renderer this widget is 
  // attached to (the default implementation tries to safe downcast the 
  // Parent to a vtkKWRenderWidget and call GetRendererIndex()).
  // SetCurrentRendererToNthRenderer() sets the current renderer to 
  // another renderer given its index (the default implementation tries to
  // safe downcast the Parent to a vtkKWRenderWidget and call 
  // GetNthRenderer() to find the new renderer). 
  virtual int GetCurrentRendererIndex();
  virtual void SetCurrentRendererToNthRenderer(int);

protected:
  vtkKW3DWidget();
  ~vtkKW3DWidget() {};

  vtkKWObject* Parent;

  vtkSetVector6Macro(InitialBounds, double);

private:
  vtkKW3DWidget(const vtkKW3DWidget&);  //Not implemented
  void operator=(const vtkKW3DWidget&);  //Not implemented
};

#endif
