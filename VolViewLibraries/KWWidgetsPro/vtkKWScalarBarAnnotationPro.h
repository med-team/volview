/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWScalarBarAnnotationPro - a scalar bar annotation widget
// .SECTION Description
// Derived class of the vtkKWScalarBarAnnotation that adds XML support.

#ifndef __vtkKWScalarBarAnnotationPro_h
#define __vtkKWScalarBarAnnotationPro_h

#include "vtkKWScalarBarAnnotation.h"

class VTK_EXPORT vtkKWScalarBarAnnotationPro : public vtkKWScalarBarAnnotation
{
public:
  static vtkKWScalarBarAnnotationPro* New();
  vtkTypeRevisionMacro(vtkKWScalarBarAnnotationPro,vtkKWScalarBarAnnotation);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkKWScalarBarAnnotationPro();
  ~vtkKWScalarBarAnnotationPro();

  // Description:
  // Create the widget.
  virtual void CreateWidget();

  // Send an event representing the state of the widget
  virtual void SendChangedEvent();

private:
  vtkKWScalarBarAnnotationPro(const vtkKWScalarBarAnnotationPro&); // Not implemented
  void operator=(const vtkKWScalarBarAnnotationPro&); // Not Implemented
};

#endif

