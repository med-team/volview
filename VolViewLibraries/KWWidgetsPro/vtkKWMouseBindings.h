/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWMouseBindings - a mouse bindings widget
// .SECTION Description
// This class creates a GUI that can be used to setup mouse bindings.

#ifndef __vtkKWMouseBindings_h
#define __vtkKWMouseBindings_h

#include "vtkKWCompositeWidget.h"

//----------------------------------------------------------------------------

#define VTK_KW_MOUSE_BINDINGS_NB_BUTTONS   3
#define VTK_KW_MOUSE_BINDINGS_NB_MODIFIERS 3

//----------------------------------------------------------------------------

class vtkKWLabel;
class vtkKWMenuButton;
class vtkKWEventMap;

//----------------------------------------------------------------------------
class VTK_EXPORT vtkKWMouseBindings : public vtkKWCompositeWidget
{
public:
  static vtkKWMouseBindings* New();
  vtkTypeRevisionMacro(vtkKWMouseBindings, vtkKWCompositeWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the event map.
  vtkGetObjectMacro(EventMap, vtkKWEventMap);
  virtual void SetEventMap(vtkKWEventMap*);

  // Description:
  // Refresh the interface given the current value of the event map.
  virtual void Update();

  // Description:
  // Set the event invoked when the a mouse binding is changed
  // Defaults to vtkKWEvent::MouseBindingChangedEvent
  vtkSetMacro(MouseBindingChangedEvent, int);
  vtkGetMacro(MouseBindingChangedEvent, int);

  // Description:
  // Set the command called when a mouse binding has changed. 
  // This command will be passed a pointer to this object.
  virtual void SetMouseBindingChangedCommand(
    vtkKWObject *object, const char *method);

  // Description:
  // Enable/Disable operations.
  virtual void SetAllowWindowLevel(int);
  vtkGetMacro(AllowWindowLevel, int);
  vtkBooleanMacro(AllowWindowLevel, int);
  virtual void SetAllowPan(int);
  vtkGetMacro(AllowPan, int);
  vtkBooleanMacro(AllowPan, int);
  virtual void SetAllowZoom(int);
  vtkGetMacro(AllowZoom, int);
  vtkBooleanMacro(AllowZoom, int);
  virtual void SetAllowMeasure(int);
  vtkGetMacro(AllowMeasure, int);
  vtkBooleanMacro(AllowMeasure, int);
  virtual void SetAllowRotate(int);
  vtkGetMacro(AllowRotate, int);
  vtkBooleanMacro(AllowRotate, int);
  virtual void SetAllowRoll(int);
  vtkGetMacro(AllowRoll, int);
  vtkBooleanMacro(AllowRoll, int);
  virtual void SetAllowFlyIn(int);
  vtkGetMacro(AllowFlyIn, int);
  vtkBooleanMacro(AllowFlyIn, int);
  virtual void SetAllowFlyOut(int);
  vtkGetMacro(AllowFlyOut, int);
  vtkBooleanMacro(AllowFlyOut, int);

  // Description:
  // Convenience method to enable/disable set of operations.
  // Allow2DOperationsOnly() enable: WindowLevel, Pan, Zoom, Measure
  // Allow3DOperationsOnly() enable: Rotate, Pan, Zoom, Roll, FlyOut, FlyOut
  virtual void Allow2DOperationsOnly();
  virtual void Allow3DOperationsOnly();

  // Description:
  // Callback used when interaction has been performed.
  virtual void MouseOperationCallback(int button, int modifier, char *action);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

protected:
  vtkKWMouseBindings();
  ~vtkKWMouseBindings();

  // Description:
  // Create the widget
  virtual void CreateWidget();

  vtkKWEventMap   *EventMap;
  int MouseBindingChangedEvent;
  char *MouseBindingChangedCommand;

  int AllowWindowLevel;
  int AllowPan;
  int AllowZoom;
  int AllowMeasure;
  int AllowRotate;
  int AllowRoll;
  int AllowFlyIn;
  int AllowFlyOut;

  vtkKWLabel      *ButtonLabels   [VTK_KW_MOUSE_BINDINGS_NB_BUTTONS];
  vtkKWLabel      *ModifierLabels [VTK_KW_MOUSE_BINDINGS_NB_MODIFIERS];
  vtkKWMenuButton *OperationMenus [VTK_KW_MOUSE_BINDINGS_NB_BUTTONS]
                                  [VTK_KW_MOUSE_BINDINGS_NB_MODIFIERS];

  // Update menus according to the operations allowed at the moment
  virtual void UpdateMenus();

private:
  vtkKWMouseBindings(const vtkKWMouseBindings&); // Not implemented
  void operator=(const vtkKWMouseBindings&); // Not implemented
};

#endif
