/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWInteractorStyleView.h"

#include "vtkKWEvent.h"
#include "vtkKWGenericRenderWindowInteractor.h"
#include "vtkKWRenderWidgetPro.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkKWInteractorStyleView, "$Revision: 1.8 $");
vtkStandardNewMacro(vtkKWInteractorStyleView);

//----------------------------------------------------------------------------
vtkKWInteractorStyleView::vtkKWInteractorStyleView()
{
}

//----------------------------------------------------------------------------
vtkKWInteractorStyleView::~vtkKWInteractorStyleView()
{
}

//----------------------------------------------------------------------------
int vtkKWInteractorStyleView::PerformAction(const char* action)
{
  if (!action)
    {
    return 0;
    }

  if (!strcmp(action, "ToggleMarker2D"))
    {
    this->ToggleMarker2D();
    return 1;
    }
  
  return 0;
}

//----------------------------------------------------------------------------
vtkKWRenderWidget* vtkKWInteractorStyleView::GetRenderWidget()
{
  vtkKWGenericRenderWindowInteractor *rwi =
    vtkKWGenericRenderWindowInteractor::SafeDownCast(this->Interactor);
  if (rwi)
    {
    return vtkKWRenderWidget::SafeDownCast(rwi->GetRenderWidget());
    }

  return NULL;
}

//----------------------------------------------------------------------------
void vtkKWInteractorStyleView::PerformInteractiveRender()
{
  vtkKWRenderWidget *rw = this->GetRenderWidget();
  if (rw)
    {
    rw->Render();
    }
  else
    {
    vtkKWGenericRenderWindowInteractor *rwi =
      vtkKWGenericRenderWindowInteractor::SafeDownCast(this->Interactor);
    if (rwi)
      {
      rwi->Render();
      }
    else if (this->Interactor)
      {
      this->Interactor->Render();
      }
    }
}

//----------------------------------------------------------------------------
void vtkKWInteractorStyleView::ToggleMarker2D()
{
  vtkKWRenderWidgetPro *widget = 
    vtkKWRenderWidgetPro::SafeDownCast(this->GetRenderWidget());
  if (widget)
    {
    float fargs[2];
    fargs[0] = (widget->GetMarker2DVisibility() ? 0 : 1);
    fargs[1] = this->EventIdentifier;
    this->InvokeEvent(vtkKWEvent::Marker2DVisibilityChangedEvent, fargs);
    }
}

//----------------------------------------------------------------------------
void vtkKWInteractorStyleView::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
