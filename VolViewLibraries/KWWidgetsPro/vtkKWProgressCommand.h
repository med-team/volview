/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWProgressCommand - a progress callback
// .SECTION Description
// This class can be used on the Start, End, and Progress events of
// a process object to update the status text and progress gauge on
// the window.

#ifndef __vtkKWProgressCommand_h
#define __vtkKWProgressCommand_h

#include "vtkCommand.h"

class vtkKWWindowBase;

class VTK_EXPORT vtkKWProgressCommand : public vtkCommand
{
public:
  static vtkKWProgressCommand *New() {return new vtkKWProgressCommand;};
  
  void Execute(vtkObject *caller, unsigned long event, void *callData);

  // Description:
  // Set the window this progress command will be attached to.
  // It will be used to retrieve GUI elements like a progress gauge 
  // for example.
  void SetWindow(vtkKWWindowBase *Window);

  // Description:
  // Set start/end message. 
  // As a convenience, if EndMessage is NULL, it is assigned to
  // StartMessage + " -- Done" when StartMessage is set.
  void SetStartMessage(const char *message);
  void SetEndMessage(const char *message);

  // Description:
  // Set/Get if the start/end message should be retrieved from the 
  // calldata pointer associated to the event. Default is OFF, see
  // SetStartMessage and SetEndMessage too.
  void SetRetrieveStartMessageFromCallData(int v);
  int GetRetrieveStartMessageFromCallData();
  void RetrieveStartMessageFromCallDataOn() 
    { this->SetRetrieveStartMessageFromCallData(1); };
  void RetrieveStartMessageFromCallDataOff() 
    { this->SetRetrieveStartMessageFromCallData(0); };
  void SetRetrieveEndMessageFromCallData(int v);
  int GetRetrieveEndMessageFromCallData();
  void RetrieveEndMessageFromCallDataOn() 
    { this->SetRetrieveEndMessageFromCallData(1); };
  void RetrieveEndMessageFromCallDataOff() 
    { this->SetRetrieveEndMessageFromCallData(0); };

  // Description:
  // Set/Get start/progress/event to listen to.
  // Default to vtkCommand's StartEvent, EndEvent, ProgressEvent
  void SetStartEvent(unsigned long event);
  void SetEndEvent(unsigned long event);
  void SetProgressEvent(unsigned long event);
  unsigned long GetStartEvent();
  unsigned long GetEndEvent();
  unsigned long GetProgressEvent();

  // Description:
  // Set the way the progress value is retrieved. Either from:
  // - the vtkProcessObject's GetProgress() method (default), or 
  // - the calldata (cast to a double)
  enum 
  {
    GET_PROGRESS     = 0,
    CALLDATA         = 1
  };
  void SetRetrieveProgressMethod(int value);
  int GetRetrieveProgressMethod();
  void SetRetrieveProgressMethodToGetProgress()
    { this->SetRetrieveProgressMethod(vtkKWProgressCommand::GET_PROGRESS); };
  void SetRetrieveProgressMethodToCallData()
    { this->SetRetrieveProgressMethod(vtkKWProgressCommand::CALLDATA); };

protected:
  vtkKWProgressCommand();
  ~vtkKWProgressCommand();
  
  vtkKWWindowBase   *Window;

  char          *StartMessage;
  char          *EndMessage;

  unsigned long StartEvent;
  unsigned long EndEvent;
  unsigned long ProgressEvent;

  unsigned long StartClock;

  int           RetrieveProgressMethod;
  int           RetrieveStartMessageFromCallData;
  int           RetrieveEndMessageFromCallData;

private:
  vtkKWProgressCommand(const vtkKWProgressCommand&); // Not implemented
  void operator=(const vtkKWProgressCommand&); // Not implemented
};


#endif


