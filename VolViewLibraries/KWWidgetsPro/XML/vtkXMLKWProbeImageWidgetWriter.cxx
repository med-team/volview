/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWProbeImageWidgetWriter.h"

#include "vtkKWProbeImageWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWProbeImageWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWProbeImageWidgetWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWProbeImageWidgetWriter::GetRootElementName()
{
  return "KWProbeImageWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWProbeImageWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWProbeImageWidget *obj = 
    vtkKWProbeImageWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWProbeImageWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("ImageVisibility", obj->GetImageVisibility());

  return 1;
}

