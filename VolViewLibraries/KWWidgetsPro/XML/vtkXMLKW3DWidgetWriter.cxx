/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKW3DWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKW3DWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKW3DWidgetWriter, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DWidgetWriter::GetRootElementName()
{
  return "KW3DWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKW3DWidget *obj = vtkKW3DWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("RendererId", obj->GetCurrentRendererIndex());

  double *bounds = obj->GetInitialBounds();
  if (bounds[0] != VTK_FLOAT_MAX && bounds[1] != VTK_FLOAT_MIN &&
      bounds[2] != VTK_FLOAT_MAX && bounds[3] != VTK_FLOAT_MIN &&
      bounds[4] != VTK_FLOAT_MAX && bounds[5] != VTK_FLOAT_MIN)
    {
    elem->SetVectorAttribute("InitialBounds", 6, bounds);
    }

  return 1;
}
