/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWLightboxWidgetWriter - vtkKWLightboxWidget XML Writer.
// .SECTION Description
// vtkXMLKWLightboxWidgetWriter provides XML writing functionality to 
// vtkKWLightboxWidget.
// .SECTION See Also
// vtkXMLKWLightboxWidgetReader

#ifndef __vtkXMLKWLightboxWidgetWriter_h
#define __vtkXMLKWLightboxWidgetWriter_h

#include "XML/vtkXMLKW2DRenderWidgetWriter.h"

class VTK_EXPORT vtkXMLKWLightboxWidgetWriter : public vtkXMLKW2DRenderWidgetWriter
{
public:
  static vtkXMLKWLightboxWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWLightboxWidgetWriter,vtkXMLKW2DRenderWidgetWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWLightboxWidgetWriter() {};
  ~vtkXMLKWLightboxWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWLightboxWidgetWriter(const vtkXMLKWLightboxWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWLightboxWidgetWriter&);  // Not implemented.
};

#endif

