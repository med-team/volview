/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCroppingRegionsWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWCroppingRegionsWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWCroppingRegionsWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWCroppingRegionsWidgetWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCroppingRegionsWidgetWriter::GetRootElementName()
{
  return "KWCroppingRegionsWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWCroppingRegionsWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWCroppingRegionsWidget *obj = 
    vtkKWCroppingRegionsWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCroppingRegionsWidget is not set!");
    return 0;
    }

  elem->SetVectorAttribute("PlanePositions", 6, obj->GetPlanePositions());

  elem->SetIntAttribute("CroppingRegionFlags", obj->GetCroppingRegionFlags());

  elem->SetIntAttribute("SliceOrientation", obj->GetSliceOrientation());

  elem->SetIntAttribute("Slice", obj->GetSlice());

  elem->SetVectorAttribute("Line1Color", 3, obj->GetLine1Color());

  elem->SetVectorAttribute("Line2Color", 3, obj->GetLine2Color());

  elem->SetVectorAttribute("Line3Color", 3, obj->GetLine3Color());

  elem->SetVectorAttribute("Line4Color", 3, obj->GetLine4Color());

  return 1;
}
