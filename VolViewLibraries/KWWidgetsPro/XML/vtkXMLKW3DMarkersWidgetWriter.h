/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DMarkersWidgetWriter - vtkKW3DMarkersWidget XML Writer.
// .SECTION Description
// vtkXMLKW3DMarkersWidgetWriter provides XML writing functionality to 
// vtkKW3DMarkersWidget.
// .SECTION See Also
// vtkXMLKW3DMarkersWidgetReader

#ifndef __vtkXMLKW3DMarkersWidgetWriter_h
#define __vtkXMLKW3DMarkersWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKW3DMarkersWidgetWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKW3DMarkersWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKW3DMarkersWidgetWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the markers group element used inside that tree to
  // store a group of markers.
  static const char* GetMarkersGroupElementName();

  // Description:
  // Return the name of the marker element used inside that tree to
  // store a marker.
  static const char* GetMarkerElementName();

protected:
  vtkXMLKW3DMarkersWidgetWriter() {};
  ~vtkXMLKW3DMarkersWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKW3DMarkersWidgetWriter(const vtkXMLKW3DMarkersWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKW3DMarkersWidgetWriter&);  // Not implemented.
};

#endif
