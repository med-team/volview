/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DSplineSurfacesWidgetReader - vtkKW3DSplineSurfacesWidget XML Reader.
// .SECTION Description
// vtkXMLKW3DSplineSurfacesWidgetReader provides XML reading functionality to 
// vtkKW3DSplineSurfacesWidget.
// .SECTION See Also
// vtkXMLKW3DSplineSurfacesWidgetWriter

#ifndef __vtkXMLKW3DSplineSurfacesWidgetReader_h
#define __vtkXMLKW3DSplineSurfacesWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKW3DSplineSurfacesWidgetReader : 
  public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKW3DSplineSurfacesWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKW3DSplineSurfacesWidgetReader, 
                       vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKW3DSplineSurfacesWidgetReader() {};
  ~vtkXMLKW3DSplineSurfacesWidgetReader() {};

private:
  vtkXMLKW3DSplineSurfacesWidgetReader(const vtkXMLKW3DSplineSurfacesWidgetReader&); // Not implemented
  void operator=(const vtkXMLKW3DSplineSurfacesWidgetReader&); // Not implemented    
};

#endif
