/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWImageWidgetWriter - vtkKWImageWidget XML Writer.
// .SECTION Description
// vtkXMLKWImageWidgetWriter provides XML writing functionality to 
// vtkKWImageWidget.
// .SECTION See Also
// vtkXMLKWImageWidgetReader

#ifndef __vtkXMLKWImageWidgetWriter_h
#define __vtkXMLKWImageWidgetWriter_h

#include "XML/vtkXMLKW2DRenderWidgetWriter.h"

class VTK_EXPORT vtkXMLKWImageWidgetWriter : public vtkXMLKW2DRenderWidgetWriter
{
public:
  static vtkXMLKWImageWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWImageWidgetWriter,vtkXMLKW2DRenderWidgetWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the cropping widget parameters.
  static const char* GetCroppingWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the cursor widget parameters.
  static const char* GetCursorWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the scale bar widget parameters.
  static const char* GetScaleBarWidgetElementName();

protected:
  vtkXMLKWImageWidgetWriter() {};
  ~vtkXMLKWImageWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);
 
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWImageWidgetWriter(const vtkXMLKWImageWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWImageWidgetWriter&);  // Not implemented.
};

#endif
