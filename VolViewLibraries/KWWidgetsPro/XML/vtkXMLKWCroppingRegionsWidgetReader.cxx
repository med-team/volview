/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCroppingRegionsWidgetReader.h"

#include "vtkKWCroppingRegionsWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWCroppingRegionsWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWCroppingRegionsWidgetReader, "$Revision: 1.12 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCroppingRegionsWidgetReader::GetRootElementName()
{
  return "KWCroppingRegionsWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWCroppingRegionsWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWCroppingRegionsWidget *obj = 
    vtkKWCroppingRegionsWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCroppingRegionsWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3], dbuffer6[6];
  int ival;

  if (elem->GetVectorAttribute("PlanePositions", 6, dbuffer6) == 6)
    {
    obj->SetPlanePositions(dbuffer6);
    }

  if (elem->GetScalarAttribute("CroppingRegionFlags", ival))
    {
    obj->SetCroppingRegionFlags(ival);
    }
  
  // Backward compat SliceType

  if (elem->GetScalarAttribute("SliceType", ival))
    {
    obj->SetSliceOrientation(ival);
    }
  
  if (elem->GetScalarAttribute("SliceOrientation", ival))
    {
    obj->SetSliceOrientation(ival);
    }
  
  if (elem->GetScalarAttribute("Slice", ival))
    {
    obj->SetSlice(ival);
    }

  if (elem->GetVectorAttribute("Line1Color", 3, dbuffer3) == 3)
    {
    obj->SetLine1Color(dbuffer3);
    }

  if (elem->GetVectorAttribute("Line2Color", 3, dbuffer3) == 3)
    {
    obj->SetLine1Color(dbuffer3);
    }

  if (elem->GetVectorAttribute("Line3Color", 3, dbuffer3) == 3)
    {
    obj->SetLine1Color(dbuffer3);
    }

  if (elem->GetVectorAttribute("Line4Color", 3, dbuffer3) == 3)
    {
    obj->SetLine1Color(dbuffer3);
    }

  return 1;
}
