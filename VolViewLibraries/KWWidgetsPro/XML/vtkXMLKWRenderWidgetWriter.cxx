/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWRenderWidgetWriter.h"

#include "vtkCamera.h"
#include "vtkCornerAnnotation.h"
#include "vtkKWRenderWidget.h"
#include "vtkObjectFactory.h"
#include "vtkTextActor.h"
#include "vtkRenderer.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLCameraWriter.h"
#include "XML/vtkXMLCornerAnnotationWriter.h"
#include "XML/vtkXMLTextActorWriter.h"

vtkStandardNewMacro(vtkXMLKWRenderWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWRenderWidgetWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetWriter::GetRootElementName()
{
  return "KWRenderWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetWriter::GetCurrentCameraElementName()
{
  return "CurrentCamera";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetWriter::GetCornerAnnotationElementName()
{
  return "CornerAnnotation";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetWriter::GetHeaderAnnotationElementName()
{
  return "HeaderAnnotation";
}

//----------------------------------------------------------------------------
int vtkXMLKWRenderWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWRenderWidget *obj = vtkKWRenderWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWRenderWidget is not set!");
    return 0;
    }

  double rgb[3];
  obj->GetRendererBackgroundColor(rgb, rgb + 1, rgb + 2);
  elem->SetVectorAttribute("RendererBackgroundColor", 3, rgb);

  obj->GetRendererBackgroundColor2(rgb, rgb + 1, rgb + 2);
  elem->SetVectorAttribute("RendererBackgroundColor2", 3, rgb);

  elem->SetIntAttribute("RendererGradientBackground", 
                        obj->GetRendererGradientBackground());

  elem->SetAttribute("DistanceUnits", obj->GetDistanceUnits());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWRenderWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWRenderWidget *obj = vtkKWRenderWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWRenderWidget is not set!");
    return 0;
    }

  // Camera

  vtkCamera *cam = obj->GetRenderer()->GetActiveCamera();
  if (cam)
    {
    vtkXMLCameraWriter *xmlw = vtkXMLCameraWriter::New();
    xmlw->SetObject(cam);
    xmlw->CreateInNestedElement(elem, this->GetCurrentCameraElementName());
    xmlw->Delete();
    }

  // Corner Annotation

  vtkCornerAnnotation *canno = obj->GetCornerAnnotation();
  if (canno)
    {
    vtkXMLCornerAnnotationWriter *xmlw = vtkXMLCornerAnnotationWriter::New();
    xmlw->SetObject(canno);
    xmlw->CreateInNestedElement(elem, this->GetCornerAnnotationElementName());
    xmlw->Delete();
    }

  // Header Annotation

  vtkTextActor *texta = obj->GetHeaderAnnotation();
  if (texta)
    {
    vtkXMLTextActorWriter *xmlw = vtkXMLTextActorWriter::New();
    xmlw->SetObject(texta);
    xmlw->CreateInNestedElement(elem, this->GetHeaderAnnotationElementName());
    xmlw->Delete();
    }
  
  return 1;
}

