/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWEventMapWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWEventMap.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWEventMapWriter);
vtkCxxRevisionMacro(vtkXMLKWEventMapWriter, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetRootElementName()
{
  return "KWEventMap";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetMouseEventsElementName()
{
  return "MouseEvents";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetMouseEventElementName()
{
  return "MouseEvent";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetKeyEventsElementName()
{
  return "KeyEvents";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetKeyEventElementName()
{
  return "KeyEvent";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetKeySymEventsElementName()
{
  return "KeySyms";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapWriter::GetKeySymEventElementName()
{
  return "KeySym";
}

//----------------------------------------------------------------------------
vtkXMLKWEventMapWriter::vtkXMLKWEventMapWriter()
{
  this->OutputMouseEvents  = 1;
  this->OutputKeyEvents    = 1;
  this->OutputKeySymEvents = 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWEventMapWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWEventMap *obj = vtkKWEventMap::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWEventMap is not set!");
    return 0;
    }

  // Mouse events

  if (this->OutputMouseEvents && obj->GetNumberOfMouseEvents())
    {
    vtkXMLDataElement *events_elem = this->NewDataElement();
    elem->AddNestedElement(events_elem);
    events_elem->Delete();
    events_elem->SetName(this->GetMouseEventsElementName());

    for (int i = 0; i < obj->GetNumberOfMouseEvents(); i++)
      {
      vtkKWEventMap::MouseEvent *event = obj->GetMouseEvent(i);
      if (event)
        {
        vtkXMLDataElement *event_elem = this->NewDataElement();
        events_elem->AddNestedElement(event_elem);
        event_elem->Delete();
        event_elem->SetName(this->GetMouseEventElementName());
        event_elem->SetIntAttribute("Button", event->Button);
        event_elem->SetIntAttribute("Modifier", event->Modifier);
        event_elem->SetAttribute("Action", event->Action);
        }
      }
    }

  // Key events

  if (this->OutputKeyEvents && obj->GetNumberOfKeyEvents())
    {
    vtkXMLDataElement *events_elem = this->NewDataElement();
    elem->AddNestedElement(events_elem);
    events_elem->Delete();
    events_elem->SetName(this->GetKeyEventsElementName());

    for (int i = 0; i < obj->GetNumberOfKeyEvents(); i++)
      {
      vtkKWEventMap::KeyEvent *event = obj->GetKeyEvent(i);
      if (event)
        {
        vtkXMLDataElement *event_elem = this->NewDataElement();
        events_elem->AddNestedElement(event_elem);
        event_elem->Delete();
        event_elem->SetName(this->GetKeyEventElementName());
        event_elem->SetIntAttribute("Key", event->Key);
        event_elem->SetIntAttribute("Modifier", event->Modifier);
        event_elem->SetAttribute("Action", event->Action);
        }
      }
    }

  // KeySym events

  if (this->OutputKeySymEvents && obj->GetNumberOfKeySymEvents())
    {
    vtkXMLDataElement *events_elem = this->NewDataElement();
    elem->AddNestedElement(events_elem);
    events_elem->Delete();
    events_elem->SetName(this->GetKeySymEventsElementName());

    for (int i = 0; i < obj->GetNumberOfKeySymEvents(); i++)
      {
      vtkKWEventMap::KeySymEvent *event = obj->GetKeySymEvent(i);
      if (event)
        {
        vtkXMLDataElement *event_elem = this->NewDataElement();
        events_elem->AddNestedElement(event_elem);
        event_elem->Delete();
        event_elem->SetName(this->GetKeySymEventElementName());
        event_elem->SetAttribute("KeySym", event->KeySym);
        event_elem->SetIntAttribute("Modifier", event->Modifier);
        event_elem->SetAttribute("Action", event->Action);
        }
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLKWEventMapWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "OutputMouseEvents: "
     << (this->OutputMouseEvents ? "On" : "Off") << endl;
  os << indent << "OutputKeyEvents: "
     << (this->OutputKeyEvents ? "On" : "Off") << endl;
  os << indent << "OutputKeySymEvents: "
     << (this->OutputKeySymEvents ? "On" : "Off") << endl;
}
