/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWIconReader.h"

#include "vtkKWIcon.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWIconWriter.h"

vtkStandardNewMacro(vtkXMLKWIconReader);
vtkCxxRevisionMacro(vtkXMLKWIconReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWIconReader::GetRootElementName()
{
  return "KWIcon";
}

//----------------------------------------------------------------------------
int vtkXMLKWIconReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWIcon *obj = vtkKWIcon::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWIcon is not set!");
    return 0;
    }

  // Get attributes

  int width, height, pixel_size;

  if (elem->GetScalarAttribute("Width", width) &&
      elem->GetScalarAttribute("Height", height) &&
      elem->GetScalarAttribute("PixelSize", pixel_size) &&
      elem->GetCharacterData())
    {
    obj->SetImage((const unsigned char*)elem->GetCharacterData(),
                  width, height, pixel_size, 
                  (unsigned long)strlen(elem->GetCharacterData()));
    return 1;
    }

  return 0;
  
}

