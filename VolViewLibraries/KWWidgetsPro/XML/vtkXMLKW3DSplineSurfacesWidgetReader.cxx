/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DSplineSurfacesWidgetReader.h"

#include "vtkKW3DSplineSurfacesWidget.h"
#include "vtkObjectFactory.h"
#include "vtkProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKW3DSplineSurfacesWidgetWriter.h"
#include "XML/vtkXMLPropertyReader.h"

vtkStandardNewMacro(vtkXMLKW3DSplineSurfacesWidgetReader);
vtkCxxRevisionMacro(vtkXMLKW3DSplineSurfacesWidgetReader, "$Revision: 1.13 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DSplineSurfacesWidgetReader::GetRootElementName()
{
  return "KW3DSplineSurfacesWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DSplineSurfacesWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKW3DSplineSurfacesWidget *obj = vtkKW3DSplineSurfacesWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DSplineSurfacesWidget is not set!");
    return 0;
    }

  // Get attributes
  double dbuffer3[3];

  int nb_surfaces = 0;
  if (!elem->GetScalarAttribute("NumberOfSplineSurfaces", nb_surfaces))
    {
    vtkWarningMacro(<< "Missing NumberOfSplineSurfaces attribute!");
    return 0;
    }

  // Get the surfaces
  int nb_nested_elems = elem->GetNumberOfNestedElements();
  for (int idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);

    if (!strcmp(nested_elem->GetName(), 
                vtkXMLKW3DSplineSurfacesWidgetWriter::GetSplineSurfaceElementName()))
      {
      const char * splinename;
      splinename = nested_elem->GetAttribute("Name");
      if( splinename == NULL )
        {
        continue;
        }
      obj->AddSplineSurface(splinename);
      int visibility;
      int numHandles;
      nested_elem->GetScalarAttribute("Visibility", visibility);
      nested_elem->GetScalarAttribute("NumberOfHandles", numHandles);

        obj->SetSplineSurfaceNumberOfHandles(splinename,numHandles);
        int nb_nested_markers = nested_elem->GetNumberOfNestedElements();
        for (int idx2 = 0; idx2 < nb_nested_markers; idx2++)
          {
          vtkXMLDataElement *nested_marker = 
            nested_elem->GetNestedElement(idx2);
          if (!strcmp(nested_marker->GetName(), 
                      vtkXMLKW3DSplineSurfacesWidgetWriter::GetMarkerElementName()))
            {
            if (nested_marker->GetVectorAttribute("Position", 3, dbuffer3) == 3)
              {
              obj->SetSplineSurfaceControlPoint(splinename, idx2, dbuffer3);
              }
            }
          }
        obj->SetSplineSurfaceVisibility(splinename,visibility);

      vtkXMLPropertyReader *xmlr = vtkXMLPropertyReader::New();
      vtkProperty *property = obj->GetSplineSurfaceProperty(splinename);
      xmlr->SetObject(property);
      xmlr->ParseInElement(nested_elem);
      xmlr->Delete();

      }
    }
  
  return 1;
}
