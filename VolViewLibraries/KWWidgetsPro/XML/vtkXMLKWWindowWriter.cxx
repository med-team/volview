/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWWindowWriter.h"

#include "vtkKWUserInterfaceManagerNotebook.h"
#include "vtkKWWindow.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWUserInterfaceManagerNotebookWriter.h"

vtkStandardNewMacro(vtkXMLKWWindowWriter);
vtkCxxRevisionMacro(vtkXMLKWWindowWriter, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWWindowWriter::GetRootElementName()
{
  return "KWWindow";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWWindowWriter::GetUserInterfaceElementName()
{
  return "UserInterface";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWWindowWriter::GetUserInterfaceManagerElementName()
{
  return "UserInterfaceManager";
}

//----------------------------------------------------------------------------
vtkXMLKWWindowWriter::vtkXMLKWWindowWriter()
{
  this->OutputUserInterfaceElement = 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWWindowWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWWindow *obj = vtkKWWindow::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWWindow is not set!");
    return 0;
    }

  // User Interface

  if (this->OutputUserInterfaceElement)
    {
    vtkXMLDataElement *ui_elem = this->NewDataElement();
    elem->AddNestedElement(ui_elem);
    ui_elem->Delete();
    this->CreateUserInterfaceElement(ui_elem);
    }

  // The rest (to be filled)

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWWindowWriter::CreateUserInterfaceElement(
  vtkXMLDataElement *ui_elem)
{
  if (!ui_elem)
    {
    return 0;
    }

  vtkKWWindow *obj = vtkKWWindow::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWWindow is not set!");
    return 0;
    }

  // Set name

  ui_elem->SetName(vtkXMLKWWindowWriter::GetUserInterfaceElementName());

  // Set attributes
 
  ui_elem->SetIntAttribute(
    "MainPanelVisibility", obj->GetMainPanelVisibility());

  // User Interface Manager

  vtkKWUserInterfaceManagerNotebook *uim_nb = 
    vtkKWUserInterfaceManagerNotebook::SafeDownCast(
      obj->GetMainUserInterfaceManager());
  if (uim_nb)
    {
    vtkXMLKWUserInterfaceManagerNotebookWriter *xmlw = 
      vtkXMLKWUserInterfaceManagerNotebookWriter::New();
    xmlw->SetObject(uim_nb);
    xmlw->CreateInNestedElement(
      ui_elem, vtkXMLKWWindowWriter::GetUserInterfaceManagerElementName());
    xmlw->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLKWWindowWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "OutputUserInterfaceElement: "
     << (this->OutputUserInterfaceElement ? "On" : "Off") << endl;
}
