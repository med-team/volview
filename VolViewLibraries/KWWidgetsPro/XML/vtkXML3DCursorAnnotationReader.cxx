/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXML3DCursorAnnotationReader.h"

#include "vtk3DCursorAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXML3DCursorAnnotationReader);
vtkCxxRevisionMacro(vtkXML3DCursorAnnotationReader, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXML3DCursorAnnotationReader::GetRootElementName()
{
  return "Cursor3DAnnotation";
}

//----------------------------------------------------------------------------
int vtkXML3DCursorAnnotationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtk3DCursorAnnotation *obj = 
    vtk3DCursorAnnotation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The 3DCursorAnnotation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;

  if (elem->GetVectorAttribute("CursorPosition", 3, dbuffer3) == 3)
    {
    obj->SetCursorPosition(dbuffer3);
    }

  if (elem->GetScalarAttribute("CursorType", ival))
    {
    obj->SetCursorType(ival);
    }
  
  if (elem->GetVectorAttribute("CursorXAxisColor", 3, dbuffer3) == 3)
    {
    obj->SetCursorXAxisColor(dbuffer3);
    }

  if (elem->GetVectorAttribute("CursorYAxisColor", 3, dbuffer3) == 3)
    {
    obj->SetCursorYAxisColor(dbuffer3);
    }

  if (elem->GetVectorAttribute("CursorZAxisColor", 3, dbuffer3) == 3)
    {
    obj->SetCursorZAxisColor(dbuffer3);
    }

  return 1;
}
