/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWProbeImageWidgetReader - vtkKWProbeImageWidget XML Reader.
// .SECTION Description
// vtkXMLKWProbeImageWidgetReader provides XML reading functionality to 
// vtkKWProbeImageWidget.
// .SECTION See Also
// vtkXMLKWProbeImageWidgetWriter

#ifndef __vtkXMLKWProbeImageWidgetReader_h
#define __vtkXMLKWProbeImageWidgetReader_h

#include "XML/vtkXMLKWImageWidgetReader.h"

class VTK_EXPORT vtkXMLKWProbeImageWidgetReader : public vtkXMLKWImageWidgetReader
{
public:
  static vtkXMLKWProbeImageWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWProbeImageWidgetReader, vtkXMLKWImageWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWProbeImageWidgetReader() {};
  ~vtkXMLKWProbeImageWidgetReader() {};

private:
  vtkXMLKWProbeImageWidgetReader(const vtkXMLKWProbeImageWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWProbeImageWidgetReader&); // Not implemented    
};

#endif

