/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWMarker2DReader.h"

#include "vtkKWMarker2D.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWMarker2DReader);
vtkCxxRevisionMacro(vtkXMLKWMarker2DReader, "$Revision: 1.13 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWMarker2DReader::GetRootElementName()
{
  return "KWMarker2D";
}

//----------------------------------------------------------------------------
int vtkXMLKWMarker2DReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWMarker2D *obj = vtkKWMarker2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWMarker2D is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3], dbuffer4[4];

  if (elem->GetVectorAttribute("Position", 4, dbuffer4) == 4)
    {
    obj->SetPosition(dbuffer4);
    }

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    obj->SetColor(dbuffer3);
    }

  return 1;
}

