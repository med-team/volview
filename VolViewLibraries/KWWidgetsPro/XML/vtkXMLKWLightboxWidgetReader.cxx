/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWLightboxWidgetReader.h"

#include "vtkKWLightboxWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCamera.h"
#include "vtkRenderer.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWLightboxWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWLightboxWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWLightboxWidgetReader, "$Revision: 1.13 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWLightboxWidgetReader::GetRootElementName()
{
  return "KWLightboxWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWLightboxWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWLightboxWidget *obj = vtkKWLightboxWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWLightboxWidget is not set!");
    return 0;
    }

  // Get attributes

  int ibuffer2[2];

  if (elem->GetVectorAttribute("Resolution", 2, ibuffer2) == 2)
    {
    double cam_pos[3], cam_fp[3], cam_vu[3];
    int cam_pp;

    vtkCamera *cam = obj->GetRenderer()->GetActiveCamera();
    cam->GetPosition(cam_pos);
    cam->GetFocalPoint(cam_fp);
    cam->GetViewUp(cam_vu);
    cam_pp = cam->GetParallelProjection();

    // This will reset the camera (and has to)
    obj->SetResolution(ibuffer2[0], ibuffer2[1]);

    cam->SetPosition(cam_pos);
    cam->SetFocalPoint(cam_fp);
    cam->SetViewUp(cam_vu);
    cam->SetParallelProjection(cam_pp);
    }

  // Get nested elements
  
  return 1;
}
