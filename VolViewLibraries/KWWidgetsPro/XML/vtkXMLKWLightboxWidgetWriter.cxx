/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWLightboxWidgetWriter.h"

#include "vtkKWLightboxWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWLightboxWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWLightboxWidgetWriter, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWLightboxWidgetWriter::GetRootElementName()
{
  return "KWLightboxWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWLightboxWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWLightboxWidget *obj = vtkKWLightboxWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWLightboxWidget is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Resolution", 2, obj->GetResolution());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWLightboxWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWLightboxWidget *obj = vtkKWLightboxWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWLightboxWidget is not set!");
    return 0;
    }

  return 1;
}

