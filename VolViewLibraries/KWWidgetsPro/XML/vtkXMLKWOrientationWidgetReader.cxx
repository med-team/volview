/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWOrientationWidgetReader.h"

#include "vtkKWOrientationWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWOrientationWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWOrientationWidgetReader, "$Revision: 1.10 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWOrientationWidgetReader::GetRootElementName()
{
  return "KWOrientationWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWOrientationWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWOrientationWidget *obj = 
    vtkKWOrientationWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWOrientationWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3], dbuffer4[4];
  int ival;

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    obj->SetColor(dbuffer3);
    }

  if (elem->GetScalarAttribute("AnnotationType", ival))
    {
    obj->SetAnnotationType(ival);
    }
  
  if (elem->GetVectorAttribute("Viewport", 4, dbuffer4) == 4)
    {
    obj->SetViewport(dbuffer4);
    }

  return 1;
}

