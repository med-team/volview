/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWProbeImageWidgetWriter - vtkKWProbeImageWidget XML Writer.
// .SECTION Description
// vtkXMLKWProbeImageWidgetWriter provides XML writing functionality to 
// vtkKWProbeImageWidget.
// .SECTION See Also
// vtkXMLKWProbeImageWidgetReader

#ifndef __vtkXMLKWProbeImageWidgetWriter_h
#define __vtkXMLKWProbeImageWidgetWriter_h

#include "XML/vtkXMLKWImageWidgetWriter.h"

class VTK_EXPORT vtkXMLKWProbeImageWidgetWriter : public vtkXMLKWImageWidgetWriter
{
public:
  static vtkXMLKWProbeImageWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWProbeImageWidgetWriter,vtkXMLKWImageWidgetWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWProbeImageWidgetWriter() {};
  ~vtkXMLKWProbeImageWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWProbeImageWidgetWriter(const vtkXMLKWProbeImageWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWProbeImageWidgetWriter&);  // Not implemented.
};

#endif

