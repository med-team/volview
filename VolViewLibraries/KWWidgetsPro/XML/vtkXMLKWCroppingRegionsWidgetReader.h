/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCroppingRegionsWidgetReader - vtkKWCroppingRegionsWidget XML Reader.
// .SECTION Description
// vtkXMLKWCroppingRegionsWidgetReader provides XML reading functionality to 
// vtkKWCroppingRegionsWidget.
// .SECTION See Also
// vtkXMLKWCroppingRegionsWidgetWriter

#ifndef __vtkXMLKWCroppingRegionsWidgetReader_h
#define __vtkXMLKWCroppingRegionsWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKWCroppingRegionsWidgetReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKWCroppingRegionsWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWCroppingRegionsWidgetReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWCroppingRegionsWidgetReader() {};
  ~vtkXMLKWCroppingRegionsWidgetReader() {};

private:
  vtkXMLKWCroppingRegionsWidgetReader(const vtkXMLKWCroppingRegionsWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWCroppingRegionsWidgetReader&); // Not implemented    
};

#endif
