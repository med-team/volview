/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWEventMapReader.h"

#include "vtkKWEventMap.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWEventMapWriter.h"

vtkStandardNewMacro(vtkXMLKWEventMapReader);
vtkCxxRevisionMacro(vtkXMLKWEventMapReader, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWEventMapReader::GetRootElementName()
{
  return "KWEventMap";
}

//----------------------------------------------------------------------------
int vtkXMLKWEventMapReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWEventMap *obj = vtkKWEventMap::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWEventMap is not set!");
    return 0;
    }

  // Loop over all nested elements

  int idx, n_idx, nb_nested_elems, nb_events_elems;
  int button, modifier, key;
  const char *action, *keysym;

  nb_nested_elems = elem->GetNumberOfNestedElements();
  for (idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);

    // Mouse events

    if (!strcmp(nested_elem->GetName(), 
                vtkXMLKWEventMapWriter::GetMouseEventsElementName()))
      {
      obj->RemoveAllMouseEvents();
      nb_events_elems = nested_elem->GetNumberOfNestedElements();
      for (n_idx = 0; n_idx < nb_events_elems; n_idx++)
        {
        vtkXMLDataElement *event_elem = nested_elem->GetNestedElement(n_idx);
        if (!strcmp(event_elem->GetName(), 
                    vtkXMLKWEventMapWriter::GetMouseEventElementName()) &&
            event_elem->GetScalarAttribute("Button", button) &&
            event_elem->GetScalarAttribute("Modifier", modifier))
          {
          action = event_elem->GetAttribute("Action");
          if (action)
            {
            obj->AddMouseEvent(button, modifier, action);
            }
          }
        }
      }

    // Key events

    if (!strcmp(nested_elem->GetName(), 
                vtkXMLKWEventMapWriter::GetKeyEventsElementName()))
      {
      obj->RemoveAllKeyEvents();
      nb_events_elems = nested_elem->GetNumberOfNestedElements();
      for (n_idx = 0; n_idx < nb_events_elems; n_idx++)
        {
        vtkXMLDataElement *event_elem = nested_elem->GetNestedElement(n_idx);
        if (!strcmp(event_elem->GetName(), 
                    vtkXMLKWEventMapWriter::GetKeyEventElementName()) &&
            event_elem->GetScalarAttribute("Key", key) &&
            event_elem->GetScalarAttribute("Modifier", modifier))
          {
          action = event_elem->GetAttribute("Action");
          if (action)
            {
            obj->AddKeyEvent(key, modifier, action);
            }
          }
        }
      }


    // KeySym events

    if (!strcmp(nested_elem->GetName(), 
                vtkXMLKWEventMapWriter::GetKeySymEventsElementName()))
      {
      obj->RemoveAllKeySymEvents();
      nb_events_elems = nested_elem->GetNumberOfNestedElements();
      for (n_idx = 0; n_idx < nb_events_elems; n_idx++)
        {
        vtkXMLDataElement *event_elem = nested_elem->GetNestedElement(n_idx);
        if (!strcmp(event_elem->GetName(), 
                    vtkXMLKWEventMapWriter::GetKeySymEventElementName()))
          {
          keysym = event_elem->GetAttribute("KeySym");
          action = event_elem->GetAttribute("Action");
          modifier = vtkKWEventMap::NoModifier;
          event_elem->GetScalarAttribute("Modifier", modifier);

          if (keysym && action)
            {
            obj->AddKeySymEvent(keysym, modifier, action);
            }
          }
        }
      }

    }

  return 1;
}


