/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWIconWriter - vtkKWIcon XML Writer.
// .SECTION Description
// vtkXMLKWIconWriter provides XML writing functionality to 
// vtkKWIcon.
// .SECTION See Also
// vtkXMLKWIconReader

#ifndef __vtkXMLKWIconWriter_h
#define __vtkXMLKWIconWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLKWIconWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLKWIconWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWIconWriter,vtkXMLObjectWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWIconWriter() {};
  ~vtkXMLKWIconWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWIconWriter(const vtkXMLKWIconWriter&);  // Not implemented.
  void operator=(const vtkXMLKWIconWriter&);  // Not implemented.
};

#endif

