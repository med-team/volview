/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DSplineSurfacesWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKW3DSplineSurfacesWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty.h"

#include "XML/vtkXMLPropertyWriter.h"

vtkStandardNewMacro(vtkXMLKW3DSplineSurfacesWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKW3DSplineSurfacesWidgetWriter, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DSplineSurfacesWidgetWriter::GetRootElementName()
{
  return "KW3DSplineSurfacesWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKW3DSplineSurfacesWidgetWriter::GetMarkerElementName()
{
  return "Marker";
}

//----------------------------------------------------------------------------
const char* vtkXMLKW3DSplineSurfacesWidgetWriter::GetSplineSurfaceElementName()
{
  return "SplineSurface";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DSplineSurfacesWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKW3DSplineSurfacesWidget *obj = 
    vtkKW3DSplineSurfacesWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DSplineSurfacesWidget is not set!");
    return 0;
    }

  // currently only write one
  elem->SetIntAttribute("NumberOfSplineSurfaces", 1);

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKW3DSplineSurfacesWidgetWriter::AddNestedElements(
  vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKW3DSplineSurfacesWidget *obj = 
    vtkKW3DSplineSurfacesWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DSplineSurfacesWidget is not set!");
    return 0;
    }

  // Iterate over all surfaces and create a surface group XML 
  // data element for each one
  vtkKW3DSplineSurfacesWidget::Iterator itrSpline = obj->Begin();
  vtkKW3DSplineSurfacesWidget::Iterator endSpline = obj->End();
  while( itrSpline != endSpline )
    {
      vtkKW3DSplineSurfacesWidget::MapKeyType currSpline = itrSpline->first;
    vtkXMLDataElement *surface_elem = this->NewDataElement();
    elem->AddNestedElement(surface_elem);
    surface_elem->Delete();
    surface_elem->SetName(this->GetSplineSurfaceElementName());

   surface_elem->SetAttribute("Name",currSpline.c_str() ); 
   surface_elem->SetIntAttribute("Visibility", 
                               obj->GetSplineSurfaceVisibility(currSpline.c_str()));
  
    int nb_markers = obj->GetNumberOfHandles(currSpline.c_str());

    surface_elem->SetIntAttribute( "NumberOfHandles", nb_markers);
        
    // Iterate over all markers and create a marker XML data element
    // for each one
    for (int i = 0; i < nb_markers; i++)
      {
      vtkXMLDataElement *marker_elem = this->NewDataElement();
      surface_elem->AddNestedElement(marker_elem);
      marker_elem->Delete();
      marker_elem->SetName(this->GetMarkerElementName());
      marker_elem->SetVectorAttribute(
        "Position", 3, obj->GetSplineSurfaceControlPoint(currSpline.c_str(),i));
      }
    vtkXMLPropertyWriter *xmlpw = vtkXMLPropertyWriter::New();
    vtkProperty *property = obj->GetSplineSurfaceProperty(currSpline.c_str());
    if (property)
      {
      xmlpw->SetObject(property);
      xmlpw->CreateInElement(surface_elem);
      }
    xmlpw->Delete();
    ++itrSpline;
    }

  return 1;
}

