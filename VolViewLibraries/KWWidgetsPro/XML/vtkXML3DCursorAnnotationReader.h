/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXML3DCursorAnnotationReader - vtk3DCursorAnnotation XML Reader.
// .SECTION Description
// vtkXML3DCursorAnnotationReader provides XML reading functionality to 
// vtk3DCursorAnnotation.
// .SECTION See Also
// vtkXML3DCursorAnnotationWriter

#ifndef __vtkXML3DCursorAnnotationReader_h
#define __vtkXML3DCursorAnnotationReader_h

#include "XML/vtkXMLActorReader.h"

class VTK_EXPORT vtkXML3DCursorAnnotationReader : public vtkXMLActorReader
{
public:
  static vtkXML3DCursorAnnotationReader* New();
  vtkTypeRevisionMacro(vtkXML3DCursorAnnotationReader, vtkXMLActorReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXML3DCursorAnnotationReader() {};
  ~vtkXML3DCursorAnnotationReader() {};

private:
  vtkXML3DCursorAnnotationReader(const vtkXML3DCursorAnnotationReader&); // Not implemented
  void operator=(const vtkXML3DCursorAnnotationReader&); // Not implemented    
};

#endif
