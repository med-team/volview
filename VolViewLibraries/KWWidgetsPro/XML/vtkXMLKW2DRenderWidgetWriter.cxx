/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW2DRenderWidgetWriter.h"

#include "vtkKW2DRenderWidget.h"
#include "vtkKWImageMapToWindowLevelColors.h"
#include "vtkObjectFactory.h"
#include "vtkSideAnnotation.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLSideAnnotationWriter.h"

vtkStandardNewMacro(vtkXMLKW2DRenderWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKW2DRenderWidgetWriter, "$Revision: 1.13 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW2DRenderWidgetWriter::GetRootElementName()
{
  return "KW2DRenderWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKW2DRenderWidgetWriter::GetSideAnnotationElementName()
{
  return "SideAnnotation";
}

//----------------------------------------------------------------------------
int vtkXMLKW2DRenderWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKW2DRenderWidget *obj = vtkKW2DRenderWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW2DRenderWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("Interpolate", obj->GetInterpolate());

  elem->SetIntAttribute("SliceOrientation", obj->GetSliceOrientation());

  elem->SetIntAttribute("SliceType", obj->GetSliceType());

  elem->SetIntAttribute("Slice", obj->GetSlice());

  int slicemin = obj->GetSliceMin(), slicemax = obj->GetSliceMax(); 
  elem->SetFloatAttribute("SliceRelative", 
    (float)(obj->GetSlice()-slicemin + 1)/(float)(slicemax-slicemin+1));
  
  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKW2DRenderWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKW2DRenderWidget *obj = vtkKW2DRenderWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW2DRenderWidget is not set!");
    return 0;
    }

  // Side Annotation

  vtkSideAnnotation *sanno = obj->GetSideAnnotation();
  if (sanno)
    {
    vtkXMLSideAnnotationWriter *xmlw = vtkXMLSideAnnotationWriter::New();
    xmlw->SetObject(sanno);
    xmlw->CreateInNestedElement(elem, this->GetSideAnnotationElementName());
    xmlw->Delete();
    }

  return 1;
}
