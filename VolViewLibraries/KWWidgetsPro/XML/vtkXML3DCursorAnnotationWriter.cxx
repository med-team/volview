/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXML3DCursorAnnotationWriter.h"

#include "vtkObjectFactory.h"
#include "vtk3DCursorAnnotation.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXML3DCursorAnnotationWriter);
vtkCxxRevisionMacro(vtkXML3DCursorAnnotationWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXML3DCursorAnnotationWriter::GetRootElementName()
{
  return "Cursor3DAnnotation";
}

//----------------------------------------------------------------------------
int vtkXML3DCursorAnnotationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtk3DCursorAnnotation *obj = 
    vtk3DCursorAnnotation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The 3DCursorAnnotation is not set!");
    return 0;
    }

  elem->SetVectorAttribute("CursorPosition", 3, obj->GetCursorPosition());

  elem->SetIntAttribute("CursorType", obj->GetCursorType());

  elem->SetVectorAttribute("CursorXAxisColor", 3, obj->GetCursorXAxisColor());

  elem->SetVectorAttribute("CursorYAxisColor", 3, obj->GetCursorYAxisColor());

  elem->SetVectorAttribute("CursorZAxisColor", 3, obj->GetCursorZAxisColor());

  return 1;
}
