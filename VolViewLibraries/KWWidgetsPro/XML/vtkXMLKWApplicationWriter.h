/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWApplicationWriter - vtkKWApplication XML Writer.
// .SECTION Description
// vtkXMLKWApplicationWriter provides XML writing functionality to 
// vtkKWApplication.
// .SECTION See Also
// vtkXMLKWApplicationReader

#ifndef __vtkXMLKWApplicationWriter_h
#define __vtkXMLKWApplicationWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLKWApplicationWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLKWApplicationWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWApplicationWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWApplicationWriter() {};
  ~vtkXMLKWApplicationWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWApplicationWriter(const vtkXMLKWApplicationWriter&);  // Not implemented.
  void operator=(const vtkXMLKWApplicationWriter&);  // Not implemented.
};

#endif

