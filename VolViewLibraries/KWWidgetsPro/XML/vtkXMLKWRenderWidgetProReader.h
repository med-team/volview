/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWRenderWidgetProReader - vtkKWRenderWidgetPro XML Reader.
// .SECTION Description
// vtkXMLKWRenderWidgetProReader provides XML reading functionality to 
// vtkKWRenderWidgetPro.
// .SECTION See Also
// vtkXMLKWRenderWidgetProWriter

#ifndef __vtkXMLKWRenderWidgetProReader_h
#define __vtkXMLKWRenderWidgetProReader_h

#include "XML/vtkXMLKWRenderWidgetReader.h"

class vtkVolumeProperty;
class vtkImageData;

class VTK_EXPORT vtkXMLKWRenderWidgetProReader : public vtkXMLKWRenderWidgetReader
{
public:
  static vtkXMLKWRenderWidgetProReader* New();
  vtkTypeRevisionMacro(vtkXMLKWRenderWidgetProReader, vtkXMLKWRenderWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();


  // Description:
  // Parse an XML representation of a set of transfer functions and store them
  // in the volume property of the current vtkKWVolumeWidget object associated
  // to that reader through SetObject (or 'vprop' if specified). 
  // The Input of the vtkKWVolumeWidget (or 'input' if specified) is used to
  // check that the properties of the data that was in memory at the time the
  // transfer functions were saved are matching.
  // The GetIndependentComponents() value vtkKWVolumeWidget (or 'indep_comp' if
  // >= 0), has to match the independent component flag stored in the XML
  // representation of the volume property.
  // Any error/warning (example: transfer function does not match the current
  // number of components, or the independent flag) will be reported and
  // appended to the ErrorLog Ivar (see vtkXMLIOBase). No WarningMacro will
  // be used, check the contents of ErrorLog instead after a call to this
  // method.
  // Return 1 on success, 0 otherwise.
  virtual int ParseTransferFunctionsElement(vtkXMLDataElement *elem);
  virtual int ParseTransferFunctionsElement(vtkXMLDataElement *elem,
                                            vtkVolumeProperty *vprop, 
                                            vtkImageData *input = NULL,
                                            int indep_comp = -1);

protected:  
  vtkXMLKWRenderWidgetProReader() {};
  ~vtkXMLKWRenderWidgetProReader() {};

private:
  vtkXMLKWRenderWidgetProReader(const vtkXMLKWRenderWidgetProReader&); // Not implemented
  void operator=(const vtkXMLKWRenderWidgetProReader&); // Not implemented    
};

#endif

