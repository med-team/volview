/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWEventMapWriter - vtkKWEventMap XML Writer.
// .SECTION Description
// vtkXMLKWEventMapWriter provides XML writing functionality to 
// vtkKWEventMap.
// .SECTION See Also
// vtkXMLKWEventMapReader

#ifndef __vtkXMLKWEventMapWriter_h
#define __vtkXMLKWEventMapWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLKWEventMapWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLKWEventMapWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWEventMapWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store a mouse, key, or keysym event.
  static const char* GetMouseEventsElementName();
  static const char* GetMouseEventElementName();
  static const char* GetKeyEventsElementName();
  static const char* GetKeyEventElementName();
  static const char* GetKeySymEventsElementName();
  static const char* GetKeySymEventElementName();

  // Description:
  // Output the mouse, key, or keysym event selectively.
  vtkBooleanMacro(OutputMouseEvents, int);
  vtkGetMacro(OutputMouseEvents, int);
  vtkSetMacro(OutputMouseEvents, int);
  vtkBooleanMacro(OutputKeyEvents, int);
  vtkGetMacro(OutputKeyEvents, int);
  vtkSetMacro(OutputKeyEvents, int);
  vtkBooleanMacro(OutputKeySymEvents, int);
  vtkGetMacro(OutputKeySymEvents, int);
  vtkSetMacro(OutputKeySymEvents, int);
  
protected:
  vtkXMLKWEventMapWriter();
  ~vtkXMLKWEventMapWriter() {};

  int OutputMouseEvents;
  int OutputKeyEvents;
  int OutputKeySymEvents;
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWEventMapWriter(const vtkXMLKWEventMapWriter&);  // Not implemented.
  void operator=(const vtkXMLKWEventMapWriter&);  // Not implemented.
};

#endif


