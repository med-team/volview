/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWImageWidgetReader - vtkKWImageWidget XML Reader.
// .SECTION Description
// vtkXMLKWImageWidgetReader provides XML reading functionality to 
// vtkKWImageWidget.
// .SECTION See Also
// vtkXMLKWImageWidgetWriter

#ifndef __vtkXMLKWImageWidgetReader_h
#define __vtkXMLKWImageWidgetReader_h

#include "XML/vtkXMLKW2DRenderWidgetReader.h"

class VTK_EXPORT vtkXMLKWImageWidgetReader : public vtkXMLKW2DRenderWidgetReader
{
public:
  static vtkXMLKWImageWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWImageWidgetReader, vtkXMLKW2DRenderWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWImageWidgetReader() {};
  ~vtkXMLKWImageWidgetReader() {};

private:
  vtkXMLKWImageWidgetReader(const vtkXMLKWImageWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWImageWidgetReader&); // Not implemented    
};

#endif
