/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWUserInterfaceManagerNotebookReader - vtkKWUserInterfaceManagerNotebook XML Reader.
// .SECTION Description
// vtkXMLKWUserInterfaceManagerNotebookReader provides XML reading functionality to 
// vtkKWUserInterfaceManagerNotebook.
// .SECTION See Also
// vtkXMLKWUserInterfaceManagerNotebookWriter

#ifndef __vtkXMLKWUserInterfaceManagerNotebookReader_h
#define __vtkXMLKWUserInterfaceManagerNotebookReader_h

#include "XML/vtkXMLKWUserInterfaceManagerReader.h"

class VTK_EXPORT vtkXMLKWUserInterfaceManagerNotebookReader : public vtkXMLKWUserInterfaceManagerReader
{
public:
  static vtkXMLKWUserInterfaceManagerNotebookReader* New();
  vtkTypeRevisionMacro(vtkXMLKWUserInterfaceManagerNotebookReader, vtkXMLKWUserInterfaceManagerReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWUserInterfaceManagerNotebookReader() {};
  ~vtkXMLKWUserInterfaceManagerNotebookReader() {};

private:
  vtkXMLKWUserInterfaceManagerNotebookReader(const vtkXMLKWUserInterfaceManagerNotebookReader&); // Not implemented
  void operator=(const vtkXMLKWUserInterfaceManagerNotebookReader&); // Not implemented    
};

#endif

