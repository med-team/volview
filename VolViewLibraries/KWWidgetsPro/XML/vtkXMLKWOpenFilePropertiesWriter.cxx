/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWOpenFilePropertiesWriter.h"

#include "vtkKWOpenFileProperties.h"
#include "vtkObjectFactory.h"
#include "vtkKWOpenFileProperties.h"
#include "vtkXMLDataElement.h"

#include <vtksys/SystemTools.hxx>

vtkStandardNewMacro(vtkXMLKWOpenFilePropertiesWriter);
vtkCxxRevisionMacro(vtkXMLKWOpenFilePropertiesWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
vtkXMLKWOpenFilePropertiesWriter::vtkXMLKWOpenFilePropertiesWriter()
{
  this->DiscardFilePatternDirectory = 0;
}

//----------------------------------------------------------------------------
const char* vtkXMLKWOpenFilePropertiesWriter::GetRootElementName()
{
  return "KWOpenFileProperties";
}

//----------------------------------------------------------------------------
int vtkXMLKWOpenFilePropertiesWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWOpenFileProperties *obj = vtkKWOpenFileProperties::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWOpenFileProperties is not set!");
    return 0;
    }

  int ibuffer3[3], ival, i;

  elem->SetVectorAttribute("Spacing", 3, obj->GetSpacing());

  elem->SetVectorAttribute("Origin", 3, obj->GetOrigin());

  elem->SetAttribute("DistanceUnits", obj->GetDistanceUnits());

  for (i = 0; i < obj->GetNumberOfScalarComponents(); i++)
    {
    char scalarunits[256];
    sprintf(scalarunits, "ScalarUnits%d", i);
    elem->SetAttribute(scalarunits, obj->GetScalarUnits(i));
    }

  elem->SetIntAttribute("ScalarType", obj->GetScalarType());

  elem->SetVectorAttribute("WholeExtent", 6, obj->GetWholeExtent());

  elem->SetIntAttribute("NumberOfScalarComponents",
                        obj->GetNumberOfScalarComponents());
  
  elem->SetIntAttribute("IndependentComponents", 
                        obj->GetIndependentComponents());

  if (obj->GetSliceAxis() != 
      vtkKWOpenFileProperties::AxisOrientationUnknown &&
      obj->GetRowAxis() != 
      vtkKWOpenFileProperties::AxisOrientationUnknown &&
      obj->GetColumnAxis() != 
      vtkKWOpenFileProperties::AxisOrientationUnknown)
    {
    ibuffer3[0] = obj->GetSliceAxis();
    ibuffer3[1] = obj->GetRowAxis();
    ibuffer3[2] = obj->GetColumnAxis();
    elem->SetVectorAttribute("FileOrientation", 3, ibuffer3);
    }

  if (obj->GetDataByteOrder() != 
      vtkKWOpenFileProperties::DataByteOrderUnknown)
    {
    ival = (obj->GetDataByteOrder() == 
            vtkKWOpenFileProperties::DataByteOrderBigEndian ? 1 : 0);
    }
  else
    {
#ifdef VTK_WORDS_BIGENDIAN
    ival = 1;
#else
    ival = 0;
#endif
    }
  elem->SetIntAttribute("BigEndianFlag", ival);

  if (obj->GetFilePattern())
    {
    if (this->DiscardFilePatternDirectory)
      {
      vtksys_stl::string basename = 
        vtksys::SystemTools::GetFilenameName(obj->GetFilePattern());
      elem->SetAttribute("FilePattern", basename.c_str());
      }
    else
      {
      elem->SetAttribute("FilePattern", obj->GetFilePattern());
      }
    }

  if (obj->GetFileDimensionality())
    {
    elem->SetIntAttribute("FileDimensionality", 
                          obj->GetFileDimensionality());
    }

  elem->SetIntAttribute("Scope", obj->GetScope());
  
  return 1;
}


//----------------------------------------------------------------------------
void vtkXMLKWOpenFilePropertiesWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "DiscardFilePatternDirectory: "
     << (this->DiscardFilePatternDirectory ? "On" : "Off") << endl;
}

