/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWOrientationWidgetWriter - vtkKWOrientationWidget XML Writer.
// .SECTION Description
// vtkXMLKWOrientationWidgetWriter provides XML writing functionality to 
// vtkKWOrientationWidget.
// .SECTION See Also
// vtkXMLKWOrientationWidgetReader

#ifndef __vtkXMLKWOrientationWidgetWriter_h
#define __vtkXMLKWOrientationWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKWOrientationWidgetWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKWOrientationWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWOrientationWidgetWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWOrientationWidgetWriter() {};
  ~vtkXMLKWOrientationWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWOrientationWidgetWriter(const vtkXMLKWOrientationWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWOrientationWidgetWriter&);  // Not implemented.
};

#endif

