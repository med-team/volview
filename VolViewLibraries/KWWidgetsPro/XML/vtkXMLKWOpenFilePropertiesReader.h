/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWOpenFilePropertiesReader - vtkKWOpenFileProperties XML Reader.
// .SECTION Description
// vtkXMLKWOpenFilePropertiesReader provides XML reading functionality to 
// vtkKWOpenFileProperties.
// .SECTION See Also
// vtkXMLKWOpenFilePropertiesWriter

#ifndef __vtkXMLKWOpenFilePropertiesReader_h
#define __vtkXMLKWOpenFilePropertiesReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLKWOpenFilePropertiesReader : public vtkXMLObjectReader
{
public:
  static vtkXMLKWOpenFilePropertiesReader* New();
  vtkTypeRevisionMacro(vtkXMLKWOpenFilePropertiesReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Check if the XML stream that has been parsed was describing a valid
  // VVI file.
  virtual int IsValid();

  // Description:
  // Check if the XML stream that has been parsed was describing a series
  // (i.e. something with a FilePattern and a WholeExtent)
  virtual int IsDescribingPatternSeries();

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

  // Description:
  // Set/Get the file pattern directory.
  // The FilePattern element (of the OpenWizard's ImageReader) is
  // stored as a relative path (the directory part is chopped off) by the
  // vtkXMLKWOpenFilePropertiesWriter. When this element is found by the 
  // XML reader, it tries to get the directory part using this variable.
  vtkSetStringMacro(FilePatternDirectory);
  vtkGetStringMacro(FilePatternDirectory);

protected:  
  vtkXMLKWOpenFilePropertiesReader();
  ~vtkXMLKWOpenFilePropertiesReader();

  char *FilePatternDirectory;

private:
  vtkXMLKWOpenFilePropertiesReader(const vtkXMLKWOpenFilePropertiesReader&); // Not implemented
  void operator=(const vtkXMLKWOpenFilePropertiesReader&); // Not implemented    
};

#endif

