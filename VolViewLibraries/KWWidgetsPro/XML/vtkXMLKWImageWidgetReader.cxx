/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWImageWidgetReader.h"

#include "vtkKWCroppingRegionsWidget.h"
#include "vtkKWCursorWidget.h"
#include "vtkKWImageWidget.h"
#include "vtkKWScaleBarWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWCroppingRegionsWidgetReader.h"
#include "XML/vtkXMLKWCursorWidgetReader.h"
#include "XML/vtkXMLKWImageWidgetWriter.h"
#include "XML/vtkXMLKWScaleBarWidgetReader.h"

vtkStandardNewMacro(vtkXMLKWImageWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWImageWidgetReader, "$Revision: 1.16 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWImageWidgetReader::GetRootElementName()
{
  return "KWImageWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWImageWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWImageWidget *obj = vtkKWImageWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWImageWidget is not set!");
    return 0;
    }

  // Get attributes

  int ival;

  // Get nested elements
  
  vtkXMLDataElement *nested_elem;

  // Cropping widget

  vtkKWCroppingRegionsWidget *cropw = obj->GetCroppingWidget();
  if (cropw)
    {
    vtkXMLKWCroppingRegionsWidgetReader *xmlr = 
      vtkXMLKWCroppingRegionsWidgetReader::New();
    xmlr->SetObject(cropw);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWImageWidgetWriter::GetCroppingWidgetElementName());
    xmlr->Delete();
    }

  // Cursor widget

  vtkKWCursorWidget *cursorw = obj->GetCursor3DWidget();
  if (cursorw)
    {
    nested_elem = elem->FindNestedElementWithName(
      vtkXMLKWImageWidgetWriter::GetCursorWidgetElementName());
    if (nested_elem)
      {
      if (nested_elem->GetScalarAttribute("Cursor3DType", ival))
        {
        obj->SetCursor3DType(ival);
        }
      
      vtkXMLKWCursorWidgetReader *xmlr = vtkXMLKWCursorWidgetReader::New();
      xmlr->SetObject(cursorw);
      xmlr->ParseInElement(nested_elem);
      xmlr->Delete();
      }
    }

  // Scale bar widget
  
  vtkKWScaleBarWidget *scalebarw = obj->GetScaleBarWidget();
  if (scalebarw)
    {
    vtkXMLKWScaleBarWidgetReader *xmlr = vtkXMLKWScaleBarWidgetReader::New();
    xmlr->SetObject(scalebarw);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWImageWidgetWriter::GetScaleBarWidgetElementName());
    xmlr->Delete();
    }

  return 1;
}
