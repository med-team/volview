/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWScaleBarWidgetReader - vtkKWScaleBarWidget XML Reader.
// .SECTION Description
// vtkXMLKWScaleBarWidgetReader provides XML reading functionality to 
// vtkKWScaleBarWidget.
// .SECTION See Also
// vtkXMLKWScaleBarWidgetWriter

#ifndef __vtkXMLKWScaleBarWidgetReader_h
#define __vtkXMLKWScaleBarWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKWScaleBarWidgetReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKWScaleBarWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWScaleBarWidgetReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWScaleBarWidgetReader() {};
  ~vtkXMLKWScaleBarWidgetReader() {};

private:
  vtkXMLKWScaleBarWidgetReader(const vtkXMLKWScaleBarWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWScaleBarWidgetReader&); // Not implemented    
};

#endif

