/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWScaleBarWidgetWriter - vtkKWScaleBarWidget XML Writer.
// .SECTION Description
// vtkXMLKWScaleBarWidgetWriter provides XML writing functionality to 
// vtkKWScaleBarWidget.
// .SECTION See Also
// vtkXMLKWScaleBarWidgetReader

#ifndef __vtkXMLKWScaleBarWidgetWriter_h
#define __vtkXMLKWScaleBarWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKWScaleBarWidgetWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKWScaleBarWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWScaleBarWidgetWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWScaleBarWidgetWriter() {};
  ~vtkXMLKWScaleBarWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWScaleBarWidgetWriter(const vtkXMLKWScaleBarWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWScaleBarWidgetWriter&);  // Not implemented.
};

#endif

