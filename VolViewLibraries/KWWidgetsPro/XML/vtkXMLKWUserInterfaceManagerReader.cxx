/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWUserInterfaceManagerReader.h"

#include "vtkKWUserInterfaceManager.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkXMLKWUserInterfaceManagerReader);
vtkCxxRevisionMacro(vtkXMLKWUserInterfaceManagerReader, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWUserInterfaceManagerReader::GetRootElementName()
{
  return "KWUserInterfaceManager";
}
