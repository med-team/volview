/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWOrientationWidgetReader - vtkKWOrientationWidget XML Reader.
// .SECTION Description
// vtkXMLKWOrientationWidgetReader provides XML reading functionality to 
// vtkKWOrientationWidget.
// .SECTION See Also
// vtkXMLKWOrientationWidgetWriter

#ifndef __vtkXMLKWOrientationWidgetReader_h
#define __vtkXMLKWOrientationWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKWOrientationWidgetReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKWOrientationWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWOrientationWidgetReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWOrientationWidgetReader() {};
  ~vtkXMLKWOrientationWidgetReader() {};

private:
  vtkXMLKWOrientationWidgetReader(const vtkXMLKWOrientationWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWOrientationWidgetReader&); // Not implemented    
};

#endif

