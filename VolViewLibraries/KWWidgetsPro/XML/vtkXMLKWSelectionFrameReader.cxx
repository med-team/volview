/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWSelectionFrameReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWSelectionFrame.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWSelectionFrameWriter.h"

vtkStandardNewMacro(vtkXMLKWSelectionFrameReader);
vtkCxxRevisionMacro(vtkXMLKWSelectionFrameReader, "$Revision: 1.10 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWSelectionFrameReader::GetRootElementName()
{
  return "KWSelectionFrame";
}

//----------------------------------------------------------------------------
int vtkXMLKWSelectionFrameReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWSelectionFrame *obj = vtkKWSelectionFrame::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWSelectionFrame is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  const char *cptr;
  int ival;

  cptr = elem->GetAttribute("Title");
  if (cptr)
    {
    obj->SetTitle(cptr);
    }

  if (elem->GetScalarAttribute("Selected", ival))
    {
    obj->SetSelected(ival);
    }

  if (elem->GetScalarAttribute("SelectionListVisibility", ival))
    {
    obj->SetSelectionListVisibility(ival);
    }

  if (elem->GetVectorAttribute("TitleColor", 3, dbuffer3) == 3)
    {
    obj->SetTitleColor(dbuffer3);
    }

  if (elem->GetVectorAttribute("TitleSelectedColor", 3, dbuffer3) == 3)
    {
    obj->SetTitleSelectedColor(dbuffer3);
    }
  
  if (elem->GetVectorAttribute("TitleBackgroundColor", 3, dbuffer3) == 3)
    {
    obj->SetTitleBackgroundColor(dbuffer3);
    }
  
  if (elem->GetVectorAttribute("TitleSelectedBackgroundColor",3,dbuffer3) == 3)
    {
    obj->SetTitleSelectedBackgroundColor(dbuffer3);
    }
  
  if (elem->GetScalarAttribute("ToolbarSetVisibility", ival))
    {
    obj->SetToolbarSetVisibility(ival);
    }

  return 1;
}

