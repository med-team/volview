/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWOpenFilePropertiesWriter - vtkKWOpenFileProperties XML Writer.
// .SECTION Description
// vtkXMLKWOpenFilePropertiesWriter provides XML writing functionality to 
// vtkKWOpenFileProperties.
// .SECTION See Also
// vtkXMLKWOpenFilePropertiesReader

#ifndef __vtkXMLKWOpenFilePropertiesWriter_h
#define __vtkXMLKWOpenFilePropertiesWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLKWOpenFilePropertiesWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLKWOpenFilePropertiesWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWOpenFilePropertiesWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Set/Get if the directory component of the FilePattern variable should
  // be discarded on write. This can make things easier later on to
  // relocate data files, *provided* that you set the corresponding
  // FilePatternDirectory in the vtkXMLKWOpenFilePropertiesReader
  vtkBooleanMacro(DiscardFilePatternDirectory, int);
  vtkGetMacro(DiscardFilePatternDirectory, int);
  vtkSetMacro(DiscardFilePatternDirectory, int);

protected:
  vtkXMLKWOpenFilePropertiesWriter();
  ~vtkXMLKWOpenFilePropertiesWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  int DiscardFilePatternDirectory;

private:
  vtkXMLKWOpenFilePropertiesWriter(const vtkXMLKWOpenFilePropertiesWriter&);  // Not implemented.
  void operator=(const vtkXMLKWOpenFilePropertiesWriter&);  // Not implemented.
};

#endif

