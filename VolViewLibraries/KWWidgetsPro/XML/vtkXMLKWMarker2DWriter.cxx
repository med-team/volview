/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWMarker2DWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWMarker2D.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWMarker2DWriter);
vtkCxxRevisionMacro(vtkXMLKWMarker2DWriter, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWMarker2DWriter::GetRootElementName()
{
  return "KWMarker2D";
}

//----------------------------------------------------------------------------
int vtkXMLKWMarker2DWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWMarker2D *obj = vtkKWMarker2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWMarker2D is not set!");
    return 0;
    }

  double dbuffer4[4];

  obj->GetPosition(dbuffer4);
  elem->SetVectorAttribute("Position", 4, dbuffer4);

  elem->SetVectorAttribute("Color", 3, obj->GetColor());

  return 1;
}

