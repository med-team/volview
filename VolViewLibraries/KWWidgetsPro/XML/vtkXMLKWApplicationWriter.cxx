/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWApplicationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkKWApplication.h"

vtkStandardNewMacro(vtkXMLKWApplicationWriter);
vtkCxxRevisionMacro(vtkXMLKWApplicationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWApplicationWriter::GetRootElementName()
{
  return "KWApplication";
}

//----------------------------------------------------------------------------
int vtkXMLKWApplicationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWApplication *obj = 
    vtkKWApplication::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWApplication is not set!");
    return 0;
    }

  // For reference, info and backward compatibility purposes.
  // DO NOT unserialize them back in the reader!

  elem->SetAttribute("Name", obj->GetName());

  elem->SetIntAttribute("MajorVersion", obj->GetMajorVersion());

  elem->SetIntAttribute("MinorVersion", obj->GetMinorVersion());

  elem->SetAttribute("ReleaseName", obj->GetReleaseName());

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLKWApplicationWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

