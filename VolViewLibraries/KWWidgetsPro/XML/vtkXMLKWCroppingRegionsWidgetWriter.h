/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCroppingRegionsWidgetWriter - vtkKWCroppingRegionsWidget XML Writer.
// .SECTION Description
// vtkXMLKWCroppingRegionsWidgetWriter provides XML writing functionality to 
// vtkKWCroppingRegionsWidget.
// .SECTION See Also
// vtkXMLKWCroppingRegionsWidgetReader

#ifndef __vtkXMLKWCroppingRegionsWidgetWriter_h
#define __vtkXMLKWCroppingRegionsWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKWCroppingRegionsWidgetWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKWCroppingRegionsWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWCroppingRegionsWidgetWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWCroppingRegionsWidgetWriter() {};
  ~vtkXMLKWCroppingRegionsWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWCroppingRegionsWidgetWriter(const vtkXMLKWCroppingRegionsWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWCroppingRegionsWidgetWriter&);  // Not implemented.
};

#endif
