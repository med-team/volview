/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW2DRenderWidgetReader - vtkKW2DRenderWidget XML Reader.
// .SECTION Description
// vtkXMLKW2DRenderWidgetReader provides XML reading functionality to 
// vtkKW2DRenderWidget.
// .SECTION See Also
// vtkXMLKW2DRenderWidgetWriter

#ifndef __vtkXMLKW2DRenderWidgetReader_h
#define __vtkXMLKW2DRenderWidgetReader_h

#include "XML/vtkXMLKWRenderWidgetProReader.h"

class VTK_EXPORT vtkXMLKW2DRenderWidgetReader : public vtkXMLKWRenderWidgetProReader
{
public:
  static vtkXMLKW2DRenderWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKW2DRenderWidgetReader, vtkXMLKWRenderWidgetProReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKW2DRenderWidgetReader() {};
  ~vtkXMLKW2DRenderWidgetReader() {};

private:
  vtkXMLKW2DRenderWidgetReader(const vtkXMLKW2DRenderWidgetReader&); // Not implemented
  void operator=(const vtkXMLKW2DRenderWidgetReader&); // Not implemented    
};

#endif
