/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWEventMapReader - vtkKWEventMap XML Reader.
// .SECTION Description
// vtkXMLKWEventMapReader provides XML reading functionality to 
// vtkKWEventMap.
// .SECTION See Also
// vtkXMLKWEventMapWriter

#ifndef __vtkXMLKWEventMapReader_h
#define __vtkXMLKWEventMapReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLKWEventMapReader : public vtkXMLObjectReader
{
public:
  static vtkXMLKWEventMapReader* New();
  vtkTypeRevisionMacro(vtkXMLKWEventMapReader, vtkXMLObjectReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWEventMapReader() {};
  ~vtkXMLKWEventMapReader() {};

private:
  vtkXMLKWEventMapReader(const vtkXMLKWEventMapReader&); // Not implemented
  void operator=(const vtkXMLKWEventMapReader&); // Not implemented    
};

#endif


