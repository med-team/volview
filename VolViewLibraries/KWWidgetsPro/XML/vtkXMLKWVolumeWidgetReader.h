/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWVolumeWidgetReader - vtkKWVolumeWidget XML Reader.
// .SECTION Description
// vtkXMLKWVolumeWidgetReader provides XML reading functionality to 
// vtkKWVolumeWidget.
// .SECTION See Also
// vtkXMLKWVolumeWidgetWriter

#ifndef __vtkXMLKWVolumeWidgetReader_h
#define __vtkXMLKWVolumeWidgetReader_h

#include "XML/vtkXMLKWRenderWidgetProReader.h"

class vtkVolumeProperty;
class vtkImageData;

class VTK_EXPORT vtkXMLKWVolumeWidgetReader : public vtkXMLKWRenderWidgetProReader
{
public:
  static vtkXMLKWVolumeWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWVolumeWidgetReader, vtkXMLKWRenderWidgetProReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWVolumeWidgetReader() {};
  ~vtkXMLKWVolumeWidgetReader() {};

private:
  vtkXMLKWVolumeWidgetReader(const vtkXMLKWVolumeWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWVolumeWidgetReader&); // Not implemented    
};

#endif

