/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCursorWidgetReader.h"

#include "vtkKWCursorWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWCursorWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWCursorWidgetReader, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCursorWidgetReader::GetRootElementName()
{
  return "KWCursorWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWCursorWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWCursorWidget *obj = vtkKWCursorWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCursorWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;

  if (elem->GetVectorAttribute("Position", 3, dbuffer3) == 3)
    {
    obj->SetPosition(dbuffer3);
    }

  // Backward compat SliceType

  if (elem->GetScalarAttribute("SliceType", ival))
    {
    obj->SetSliceOrientation(ival);
    }
  
  if (elem->GetScalarAttribute("SliceOrientation", ival))
    {
    obj->SetSliceOrientation(ival);
    }
  
  if (elem->GetVectorAttribute("Axis1Color", 3, dbuffer3) == 3)
    {
    obj->SetAxis1Color(dbuffer3);
    }

  if (elem->GetVectorAttribute("Axis2Color", 3, dbuffer3) == 3)
    {
    obj->SetAxis2Color(dbuffer3);
    }

  if (elem->GetScalarAttribute("Interactive", ival))
    {
    obj->SetInteractive(ival);
    }
  
  return 1;
}
