/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DSplineSurfacesWidgetWriter - vtkKW3DSplineSurfacesWidget XML Writer.
// .SECTION Description
// vtkXMLKW3DSplineSurfacesWidgetWriter provides XML writing functionality to 
// vtkKW3DSplineSurfacesWidget.
// .SECTION See Also
// vtkXMLKW3DSplineSurfacesWidgetReader

#ifndef __vtkXMLKW3DSplineSurfacesWidgetWriter_h
#define __vtkXMLKW3DSplineSurfacesWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKW3DSplineSurfacesWidgetWriter : 
  public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKW3DSplineSurfacesWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKW3DSplineSurfacesWidgetWriter,
                       vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the surface element used inside that tree to
  // store a surface
  static const char* GetSplineSurfaceElementName();

  // Description:
  // Return the name of the marker element used inside that tree to
  // store a marker.
  static const char* GetMarkerElementName();

protected:
  vtkXMLKW3DSplineSurfacesWidgetWriter() {};
  ~vtkXMLKW3DSplineSurfacesWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKW3DSplineSurfacesWidgetWriter(const vtkXMLKW3DSplineSurfacesWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKW3DSplineSurfacesWidgetWriter&);  // Not implemented.
};

#endif
