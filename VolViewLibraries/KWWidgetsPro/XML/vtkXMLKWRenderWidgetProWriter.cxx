/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWRenderWidgetProWriter.h"

#include "vtkKW3DMarkersWidget.h"
#include "vtkKWMarker2D.h"
#include "vtkKWRenderWidgetPro.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVolumeProperty.h"
#include "vtkImageData.h"

#include "XML/vtkXMLKW3DMarkersWidgetWriter.h"
#include "XML/vtkXMLKWMarker2DWriter.h"
#include "XML/vtkXMLVolumePropertyWriter.h"

vtkStandardNewMacro(vtkXMLKWRenderWidgetProWriter);
vtkCxxRevisionMacro(vtkXMLKWRenderWidgetProWriter, "$Revision: 1.17 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetProWriter::GetRootElementName()
{
  return "KWRenderWidgetPro";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetProWriter::GetTransferFunctionsElementName()
{
  return "TransferFunctions";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetProWriter::GetMarker2DWidgetElementName()
{
  return "Marker2D";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWRenderWidgetProWriter::GetMarkers3DWidgetElementName()
{
  return "Markers3D";
}

//----------------------------------------------------------------------------
int vtkXMLKWRenderWidgetProWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWRenderWidgetPro *obj = vtkKWRenderWidgetPro::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWRenderWidgetPro is not set!");
    return 0;
    }

  int i;

  elem->SetIntAttribute(
    "IndependentComponents", obj->GetIndependentComponents());

  for (i = 0; i < VTK_MAX_VRCOMP; i++)
    {
    char scalarunits[256];
    sprintf(scalarunits, "ScalarUnits%d", i);
    elem->SetAttribute(scalarunits, obj->GetScalarUnits(i));
    }

  elem->SetIntAttribute("DisplayChannels", obj->GetDisplayChannels());

  elem->SetIntAttribute("UseOpacityModulation", 
                        obj->GetUseOpacityModulation());

  elem->SetFloatAttribute("Window", obj->GetWindow());

  elem->SetFloatAttribute("Level", obj->GetLevel());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWRenderWidgetProWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWRenderWidgetPro *obj = vtkKWRenderWidgetPro::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWRenderWidgetPro is not set!");
    return 0;
    }

  // 2D marker

  vtkKWMarker2D *marker2d = obj->GetMarker2D();
  if (marker2d)
    {
    vtkXMLKWMarker2DWriter *xmlw = vtkXMLKWMarker2DWriter::New();
    xmlw->SetObject(marker2d);
    xmlw->CreateInNestedElement(elem, this->GetMarker2DWidgetElementName());
    xmlw->Delete();
    }

  // 3D markers

  vtkKW3DMarkersWidget *markers3d = obj->GetMarkers3D();
  if (markers3d)
    {
    vtkXMLKW3DMarkersWidgetWriter *xmlw = vtkXMLKW3DMarkersWidgetWriter::New();
    xmlw->SetObject(markers3d);
    xmlw->CreateInNestedElement(elem, this->GetMarkers3DWidgetElementName());
    xmlw->Delete();
    }

  // Transfer functions

  vtkXMLDataElement *tfuncs_elem = 
    this->CreateTransferFunctionsElement(obj->GetVolumeProperty(), 
                                         obj->GetInput());
  if (tfuncs_elem)
    {
    elem->AddNestedElement(tfuncs_elem);
    tfuncs_elem->Delete();
    }

  return 1;
}


//----------------------------------------------------------------------------
vtkXMLDataElement* vtkXMLKWRenderWidgetProWriter::CreateTransferFunctionsElement()
{
  vtkKWRenderWidgetPro *obj = vtkKWRenderWidgetPro::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWRenderWidgetPro is not set!");
    return 0;
    }

  return this->CreateTransferFunctionsElement(obj->GetVolumeProperty(), 
                                              obj->GetInput());
}

//----------------------------------------------------------------------------
vtkXMLDataElement* vtkXMLKWRenderWidgetProWriter::CreateTransferFunctionsElement(
  vtkVolumeProperty *vprop, vtkImageData *input)
{
  vtkXMLDataElement *elem = this->NewDataElement();

  // Name and attributes

  elem->SetName(this->GetTransferFunctionsElementName());

  if (input)
    {
    elem->SetIntAttribute("ScalarTypeHint", input->GetScalarType());

    elem->SetIntAttribute(
      "NumberOfScalarComponentsHint", input->GetNumberOfScalarComponents());
    }

  // Volume Property

  if (vprop)
    {
    vtkXMLVolumePropertyWriter *xmlw = vtkXMLVolumePropertyWriter::New();
    xmlw->SetObject(vprop);
    if (input)
      {
      xmlw->SetNumberOfComponents(input->GetNumberOfScalarComponents());
      }
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  return elem;
}


