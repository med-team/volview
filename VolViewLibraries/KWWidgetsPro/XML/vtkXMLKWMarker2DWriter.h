/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWMarker2DWriter - vtkKWMarker2D XML Writer.
// .SECTION Description
// vtkXMLKWMarker2DWriter provides XML writing functionality to 
// vtkKWMarker2D.
// .SECTION See Also
// vtkXMLKWMarker2DReader

#ifndef __vtkXMLKWMarker2DWriter_h
#define __vtkXMLKWMarker2DWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKWMarker2DWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKWMarker2DWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWMarker2DWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWMarker2DWriter() {};
  ~vtkXMLKWMarker2DWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWMarker2DWriter(const vtkXMLKWMarker2DWriter&);  // Not implemented.
  void operator=(const vtkXMLKWMarker2DWriter&);  // Not implemented.
};

#endif

