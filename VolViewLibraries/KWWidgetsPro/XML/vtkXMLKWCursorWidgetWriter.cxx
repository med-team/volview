/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCursorWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWCursorWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWCursorWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWCursorWidgetWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCursorWidgetWriter::GetRootElementName()
{
  return "KWCursorWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWCursorWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWCursorWidget *obj = vtkKWCursorWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCursorWidget is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Position", 3, obj->GetPosition());

  elem->SetIntAttribute("SliceOrientation", obj->GetSliceOrientation());

  elem->SetVectorAttribute("Axis1Color", 3, obj->GetAxis1Color());

  elem->SetVectorAttribute("Axis2Color", 3, obj->GetAxis2Color());

  elem->SetIntAttribute("Interactive", obj->GetInteractive());

  return 1;
}
