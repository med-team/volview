/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWLightboxWidgetReader - vtkKWLightboxWidget XML Reader.
// .SECTION Description
// vtkXMLKWLightboxWidgetReader provides XML reading functionality to 
// vtkKWLightboxWidget.
// .SECTION See Also
// vtkXMLKWLightboxWidgetWriter

#ifndef __vtkXMLKWLightboxWidgetReader_h
#define __vtkXMLKWLightboxWidgetReader_h

#include "XML/vtkXMLKW2DRenderWidgetReader.h"

class VTK_EXPORT vtkXMLKWLightboxWidgetReader : public vtkXMLKW2DRenderWidgetReader
{
public:
  static vtkXMLKWLightboxWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWLightboxWidgetReader, vtkXMLKW2DRenderWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWLightboxWidgetReader() {};
  ~vtkXMLKWLightboxWidgetReader() {};

private:
  vtkXMLKWLightboxWidgetReader(const vtkXMLKWLightboxWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWLightboxWidgetReader&); // Not implemented    
};

#endif
