/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DWidgetReader - vtkKW3DWidget XML Reader.
// .SECTION Description
// vtkXMLKW3DWidgetReader provides XML reading functionality to 
// vtkKW3DWidget.
// .SECTION See Also
// vtkXMLKW3DWidgetWriter

#ifndef __vtkXMLKW3DWidgetReader_h
#define __vtkXMLKW3DWidgetReader_h

#include "XML/vtkXML3DWidgetReader.h"

class VTK_EXPORT vtkXMLKW3DWidgetReader : public vtkXML3DWidgetReader
{
public:
  static vtkXMLKW3DWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKW3DWidgetReader, vtkXML3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKW3DWidgetReader() {};
  ~vtkXMLKW3DWidgetReader() {};

private:
  vtkXMLKW3DWidgetReader(const vtkXMLKW3DWidgetReader&); // Not implemented
  void operator=(const vtkXMLKW3DWidgetReader&); // Not implemented    
};

#endif
