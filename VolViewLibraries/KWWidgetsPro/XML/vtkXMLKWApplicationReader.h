/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWApplicationReader - vtkKWApplication XML Reader.
// .SECTION Description
// vtkXMLKWApplicationReader provides XML reading functionality to 
// vtkKWApplication.
// .SECTION See Also
// vtkXMLKWApplicationWriter

#ifndef __vtkXMLKWApplicationReader_h
#define __vtkXMLKWApplicationReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLKWApplicationReader : public vtkXMLObjectReader
{
public:
  static vtkXMLKWApplicationReader* New();
  vtkTypeRevisionMacro(vtkXMLKWApplicationReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWApplicationReader() {};
  ~vtkXMLKWApplicationReader() {};

private:
  vtkXMLKWApplicationReader(const vtkXMLKWApplicationReader&); // Not implemented
  void operator=(const vtkXMLKWApplicationReader&); // Not implemented    
};

#endif

