/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWApplicationReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWApplication.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWApplicationWriter.h"

vtkStandardNewMacro(vtkXMLKWApplicationReader);
vtkCxxRevisionMacro(vtkXMLKWApplicationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWApplicationReader::GetRootElementName()
{
  return "KWApplication";
}

//----------------------------------------------------------------------------
int vtkXMLKWApplicationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWApplication *obj = vtkKWApplication::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWApplication is not set!");
    return 0;
    }

  // Get Attributes

  // Get nested elements

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLKWApplicationReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
