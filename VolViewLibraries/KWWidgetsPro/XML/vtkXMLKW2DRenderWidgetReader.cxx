/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW2DRenderWidgetReader.h"

#include "vtkKW2DRenderWidget.h"
#include "vtkImageData.h"
#include "vtkObjectFactory.h"
#include "vtkSideAnnotation.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKW2DRenderWidgetWriter.h"
#include "XML/vtkXMLSideAnnotationReader.h"

vtkStandardNewMacro(vtkXMLKW2DRenderWidgetReader);
vtkCxxRevisionMacro(vtkXMLKW2DRenderWidgetReader, "$Revision: 1.14 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW2DRenderWidgetReader::GetRootElementName()
{
  return "KW2DRenderWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKW2DRenderWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKW2DRenderWidget *obj = vtkKW2DRenderWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW2DRenderWidget is not set!");
    return 0;
    }

  // Get attributes

  int ival;
  float fval;

  if (elem->GetScalarAttribute("Interpolate", ival))
    {
    obj->SetInterpolate(ival);
    }

  if (elem->GetScalarAttribute("SliceOrientation", ival))
    {
    obj->SetSliceOrientation(ival);
    }

  if (elem->GetScalarAttribute("SliceType", ival))
    {
    obj->SetSliceType(ival);
    }

  if (elem->GetScalarAttribute("Slice", ival))
    {
    obj->SetSlice(ival);
    }

  if (elem->GetScalarAttribute("SliceRelative", fval))
    {
    int min_slice = obj->GetSliceMin();
    int max_slice = obj->GetSliceMax();
    obj->SetSlice(
        (int)(((float)(max_slice-min_slice+1))*fval + min_slice - 1 + 0.5));
    }
  
  // Get nested elements
  
  // Side Annotation

  vtkSideAnnotation *sanno = obj->GetSideAnnotation();
  if (sanno)
    {
    vtkXMLSideAnnotationReader *xmlr = vtkXMLSideAnnotationReader::New();
    xmlr->SetObject(sanno);
    if (xmlr->ParseInNestedElement(
          elem, vtkXMLKW2DRenderWidgetWriter::GetSideAnnotationElementName()))
      {
      obj->SetSideAnnotationVisibility(sanno->GetVisibility()); // add prop
      }
    xmlr->Delete();
    }
 
  return 1;
}
