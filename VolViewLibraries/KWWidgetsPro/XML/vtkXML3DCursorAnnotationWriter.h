/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXML3DCursorAnnotationWriter - vtk3DCursorAnnotation XML Writer.
// .SECTION Description
// vtkXML3DCursorAnnotationWriter provides XML writing functionality to 
// vtk3DCursorAnnotation.
// .SECTION See Also
// vtkXML3DCursorAnnotationReader

#ifndef __vtkXML3DCursorAnnotationWriter_h
#define __vtkXML3DCursorAnnotationWriter_h

#include "XML/vtkXMLActorWriter.h"

class VTK_EXPORT vtkXML3DCursorAnnotationWriter : public vtkXMLActorWriter
{
public:
  static vtkXML3DCursorAnnotationWriter* New();
  vtkTypeRevisionMacro(vtkXML3DCursorAnnotationWriter,vtkXMLActorWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXML3DCursorAnnotationWriter() {};
  ~vtkXML3DCursorAnnotationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXML3DCursorAnnotationWriter(const vtkXML3DCursorAnnotationWriter&);  // Not implemented.
  void operator=(const vtkXML3DCursorAnnotationWriter&);  // Not implemented.
};

#endif
