/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWIconReader - vtkKWIcon XML Reader.
// .SECTION Description
// vtkXMLKWIconReader provides XML reading functionality to 
// vtkKWIcon.
// .SECTION See Also
// vtkXMLKWIconWriter

#ifndef __vtkXMLKWIconReader_h
#define __vtkXMLKWIconReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLKWIconReader : public vtkXMLObjectReader
{
public:
  static vtkXMLKWIconReader* New();
  vtkTypeRevisionMacro(vtkXMLKWIconReader, vtkXMLObjectReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWIconReader() {};
  ~vtkXMLKWIconReader() {};

private:
  vtkXMLKWIconReader(const vtkXMLKWIconReader&); // Not implemented
  void operator=(const vtkXMLKWIconReader&); // Not implemented    
};

#endif

