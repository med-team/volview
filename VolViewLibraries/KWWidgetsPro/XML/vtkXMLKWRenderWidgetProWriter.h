/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWRenderWidgetProWriter - vtkKWRenderWidgetPro XML Writer.
// .SECTION Description
// vtkXMLKWRenderWidgetProWriter provides XML writing functionality to 
// vtkKWRenderWidgetPro.
// .SECTION See Also
// vtkXMLKWRenderWidgetProReader

#ifndef __vtkXMLKWRenderWidgetProWriter_h
#define __vtkXMLKWRenderWidgetProWriter_h

#include "XML/vtkXMLKWRenderWidgetWriter.h"

class vtkVolumeProperty;
class vtkImageData;

class VTK_EXPORT vtkXMLKWRenderWidgetProWriter : public vtkXMLKWRenderWidgetWriter
{
public:
  static vtkXMLKWRenderWidgetProWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWRenderWidgetProWriter,vtkXMLKWRenderWidgetWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the 2D marker widget parameters.
  static const char* GetMarker2DWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the 3D markers widget parameters.
  static const char* GetMarkers3DWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the contours parameters.
  static const char* GetContourAnnotationElementName();

  // Description:
  // Return the name of the transfer functions element used inside that tree to
  // store the transfer functions.
  static const char* GetTransferFunctionsElementName();

  // Description:
  // Create an XML representation of the transfer functions for the
  // volume property of the current vtkKWVolumeWidget object associated to
  // that writer through SetObject (or 'vprop' if specified). 
  // The Input of the vtkKWVolumeWidget (or 'input' if specified) is used
  // to add some hints to the XML element describing what kind of data was
  // loaded when the transfer functions were created. The hints will be used
  // once these transfer functions will be loaded later on.
  // Return a pointer to an element on success (it's up to the caller to
  // Delete() it), NULL otherwise.
  virtual vtkXMLDataElement* CreateTransferFunctionsElement();
  virtual vtkXMLDataElement* CreateTransferFunctionsElement(
    vtkVolumeProperty *vprop, vtkImageData *input = NULL);

protected:
  vtkXMLKWRenderWidgetProWriter() {};
  ~vtkXMLKWRenderWidgetProWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWRenderWidgetProWriter(const vtkXMLKWRenderWidgetProWriter&);  // Not implemented.
  void operator=(const vtkXMLKWRenderWidgetProWriter&);  // Not implemented.
};

#endif

