/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKW3DWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKW3DWidgetReader);
vtkCxxRevisionMacro(vtkXMLKW3DWidgetReader, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DWidgetReader::GetRootElementName()
{
  return "KW3DWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKW3DWidget *obj = vtkKW3DWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer6[6];
  int ival;

  if (elem->GetScalarAttribute("RendererId", ival))
    {
    obj->SetCurrentRendererToNthRenderer(ival);
    }

  if (elem->GetVectorAttribute("InitialBounds", 6, dbuffer6) == 6)
    {
    obj->PlaceWidget(dbuffer6);
    }

  return 1;
}
