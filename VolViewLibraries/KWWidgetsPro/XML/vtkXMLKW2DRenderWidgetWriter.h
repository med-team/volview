/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW2DRenderWidgetWriter - vtkKW2DRenderWidget XML Writer.
// .SECTION Description
// vtkXMLKW2DRenderWidgetWriter provides XML writing functionality to 
// vtkKW2DRenderWidget.
// .SECTION See Also
// vtkXMLKW2DRenderWidgetReader

#ifndef __vtkXMLKW2DRenderWidgetWriter_h
#define __vtkXMLKW2DRenderWidgetWriter_h

#include "XML/vtkXMLKWRenderWidgetProWriter.h"

class VTK_EXPORT vtkXMLKW2DRenderWidgetWriter : public vtkXMLKWRenderWidgetProWriter
{
public:
  static vtkXMLKW2DRenderWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKW2DRenderWidgetWriter,vtkXMLKWRenderWidgetProWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the side annotation parameters.
  static const char* GetSideAnnotationElementName();

protected:
  vtkXMLKW2DRenderWidgetWriter() {};
  ~vtkXMLKW2DRenderWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKW2DRenderWidgetWriter(const vtkXMLKW2DRenderWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKW2DRenderWidgetWriter&);  // Not implemented.
};

#endif
