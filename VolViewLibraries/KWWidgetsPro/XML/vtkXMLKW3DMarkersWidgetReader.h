/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DMarkersWidgetReader - vtkKW3DMarkersWidget XML Reader.
// .SECTION Description
// vtkXMLKW3DMarkersWidgetReader provides XML reading functionality to 
// vtkKW3DMarkersWidget.
// .SECTION See Also
// vtkXMLKW3DMarkersWidgetWriter

#ifndef __vtkXMLKW3DMarkersWidgetReader_h
#define __vtkXMLKW3DMarkersWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKW3DMarkersWidgetReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKW3DMarkersWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKW3DMarkersWidgetReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKW3DMarkersWidgetReader() {};
  ~vtkXMLKW3DMarkersWidgetReader() {};

private:
  vtkXMLKW3DMarkersWidgetReader(const vtkXMLKW3DMarkersWidgetReader&); // Not implemented
  void operator=(const vtkXMLKW3DMarkersWidgetReader&); // Not implemented    
};

#endif
