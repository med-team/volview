/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWProbeImageWidgetReader.h"

#include "vtkKWProbeImageWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWProbeImageWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWProbeImageWidgetReader, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWProbeImageWidgetReader::GetRootElementName()
{
  return "KWProbeImageWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWProbeImageWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWProbeImageWidget *obj = 
    vtkKWProbeImageWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWProbeImageWidget is not set!");
    return 0;
    }

  // Get attributes

  int ival;

  if (elem->GetScalarAttribute("ImageVisibility", ival))
    {
    obj->SetImageVisibility(ival);
    }

  return 1;
}

