/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWImageWidgetWriter.h"

#include "vtkKWCroppingRegionsWidget.h"
#include "vtkKWCursorWidget.h"
#include "vtkKWImageWidget.h"
#include "vtkKWScaleBarWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWCroppingRegionsWidgetWriter.h"
#include "XML/vtkXMLKWCursorWidgetWriter.h"
#include "XML/vtkXMLKWScaleBarWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWImageWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWImageWidgetWriter, "$Revision: 1.16 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWImageWidgetWriter::GetRootElementName()
{
  return "KWImageWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWImageWidgetWriter::GetCroppingWidgetElementName()
{
  return "CroppingWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWImageWidgetWriter::GetCursorWidgetElementName()
{
  return "CursorWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWImageWidgetWriter::GetScaleBarWidgetElementName()
{
  return "ScaleBarWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWImageWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWImageWidget *obj = vtkKWImageWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWImageWidget is not set!");
    return 0;
    }

  return 1;
}


//----------------------------------------------------------------------------
int vtkXMLKWImageWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWImageWidget *obj = vtkKWImageWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWImageWidget is not set!");
    return 0;
    }

  // Cropping widget

  vtkKWCroppingRegionsWidget *cropw = obj->GetCroppingWidget();
  if (cropw)
    {
    vtkXMLKWCroppingRegionsWidgetWriter *xmlw = 
      vtkXMLKWCroppingRegionsWidgetWriter::New();
    xmlw->SetObject(cropw);
    xmlw->CreateInNestedElement(elem, this->GetCroppingWidgetElementName());
    xmlw->Delete();
    }

  // Cursor widget

  vtkKWCursorWidget *cursorw = obj->GetCursor3DWidget();
  if (cursorw)
    {
    vtkXMLDataElement *cursor_elem = this->NewDataElement();
    elem->AddNestedElement(cursor_elem);
    cursor_elem->Delete();
    cursor_elem->SetName(this->GetCursorWidgetElementName());
    cursor_elem->SetIntAttribute("Cursor3DType", obj->GetCursor3DType());

    vtkXMLKWCursorWidgetWriter *xmlw = vtkXMLKWCursorWidgetWriter::New();
    xmlw->SetObject(cursorw);
    xmlw->CreateInElement(cursor_elem);
    xmlw->Delete();
    }

  // Scale bar widget

  vtkKWScaleBarWidget *scalebarw = obj->GetScaleBarWidget();
  if (scalebarw)
    {
    vtkXMLKWScaleBarWidgetWriter *xmlw = vtkXMLKWScaleBarWidgetWriter::New();
    xmlw->SetObject(scalebarw);
    xmlw->CreateInNestedElement(elem, this->GetScaleBarWidgetElementName());
    xmlw->Delete();
    }

  return 1;
}
