/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DMarkersWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKW3DMarkersWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKW3DMarkersWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKW3DMarkersWidgetWriter, "$Revision: 1.15 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DMarkersWidgetWriter::GetRootElementName()
{
  return "KW3DMarkersWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKW3DMarkersWidgetWriter::GetMarkerElementName()
{
  return "Marker";
}

//----------------------------------------------------------------------------
const char* vtkXMLKW3DMarkersWidgetWriter::GetMarkersGroupElementName()
{
  return "MarkersGroup";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DMarkersWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKW3DMarkersWidget *obj = vtkKW3DMarkersWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DMarkersWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("NumberOfMarkers", obj->GetNumberOfMarkers());

  elem->SetIntAttribute("NumberOfMarkersGroups", obj->GetNumberOfMarkersGroups());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKW3DMarkersWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKW3DMarkersWidget *obj = vtkKW3DMarkersWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DMarkersWidget is not set!");
    return 0;
    }

  int nb_markers = obj->GetNumberOfMarkers();

  // Iterate over all markers groups and create a markers group XML 
  // data element for each one

  unsigned int nb_markers_groups = obj->GetNumberOfMarkersGroups();
  for (unsigned int g = 0; g < nb_markers_groups; g++)
    {
    vtkXMLDataElement *markers_group_elem = this->NewDataElement();
    elem->AddNestedElement(markers_group_elem);
    markers_group_elem->Delete();
    markers_group_elem->SetName(this->GetMarkersGroupElementName());
    markers_group_elem->SetAttribute("Name", obj->GetMarkersGroupName(g));
    markers_group_elem->SetVectorAttribute(
      "Color", 3, obj->GetMarkersGroupColor(g));

    // Iterate over all markers and create a marker XML data element
    // for each one

    for (int i = 0; i < nb_markers; i++)
      {
      if (obj->GetMarkerGroupId(i) == (int)g)
        {
        vtkXMLDataElement *marker_elem = this->NewDataElement();
        markers_group_elem->AddNestedElement(marker_elem);
        marker_elem->Delete();
        marker_elem->SetName(this->GetMarkerElementName());
        marker_elem->SetVectorAttribute(
          "Position", 3, obj->GetMarkerPosition(i));
        }
      }
    }

  return 1;
}
