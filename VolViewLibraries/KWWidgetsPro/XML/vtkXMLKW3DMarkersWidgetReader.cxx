/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKW3DMarkersWidgetReader.h"

#include "vtkKW3DMarkersWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKW3DMarkersWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKW3DMarkersWidgetReader);
vtkCxxRevisionMacro(vtkXMLKW3DMarkersWidgetReader, "$Revision: 1.17 $");

//----------------------------------------------------------------------------
const char* vtkXMLKW3DMarkersWidgetReader::GetRootElementName()
{
  return "KW3DMarkersWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKW3DMarkersWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKW3DMarkersWidget *obj = vtkKW3DMarkersWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KW3DMarkersWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];

  int default_gid = obj->GetDefaultMarkersGroupId();

  // If 'Color' is found at that level, then it's an old session file where markers were not
  // organized into groups. In that case, just assign that color to the Default group.

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    obj->SetMarkersGroupColor(default_gid, dbuffer3);
    }

  int nb_markers = 0;
  if (!elem->GetScalarAttribute("NumberOfMarkers", nb_markers))
    {
    vtkWarningMacro(<< "Missing NumberOfMarkers attribute!");
    return 0;
    }

  int nb_markers_groups = 0;
  elem->GetScalarAttribute("NumberOfMarkersGroups", nb_markers_groups);

  // Get the markers groups (or markers for old session file)

  int nb_markers_found = 0;
  int nb_markers_groups_found = 0;

  int nb_nested_elems = elem->GetNumberOfNestedElements();
  for (int idx = 0; 
       (idx < nb_nested_elems && 
        nb_markers_found <= nb_markers &&
        nb_markers_groups_found <= nb_markers_groups); idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);

    // If 'Marker' is found at that level, then it's an old session file where markers were not
    // organized into groups. In that case, just add that maker to the Default group.

    if (!strcmp(nested_elem->GetName(), 
                vtkXMLKW3DMarkersWidgetWriter::GetMarkerElementName()))
      {
      if (nested_elem->GetVectorAttribute("Position", 3, dbuffer3) == 3)
        {
        obj->AddMarker(default_gid, dbuffer3[0], dbuffer3[1], dbuffer3[2]);
        nb_markers_found++;
        }
      }

    // Otherwise it's most probably a group, and we should look for element inside it
    
    else if (!strcmp(nested_elem->GetName(), 
                     vtkXMLKW3DMarkersWidgetWriter::GetMarkersGroupElementName()))
      {
      const char *cptr = nested_elem->GetAttribute("Name");
      if (cptr && nested_elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
        {
        nb_markers_groups_found++;
        int gid = obj->AddMarkersGroup(cptr, dbuffer3);
        if (gid >= 0)
          {
          int nb_nested_markers = nested_elem->GetNumberOfNestedElements();
          for (int idx2 = 0; idx2 < nb_nested_markers && nb_markers_found <= nb_markers; idx2++)
            {
            vtkXMLDataElement *nested_marker = nested_elem->GetNestedElement(idx2);
            if (!strcmp(nested_marker->GetName(), 
                        vtkXMLKW3DMarkersWidgetWriter::GetMarkerElementName()))
              {
              if (nested_marker->GetVectorAttribute("Position", 3, dbuffer3) == 3)
                {
                obj->AddMarker(gid, dbuffer3[0], dbuffer3[1], dbuffer3[2]);
                nb_markers_found++;
                }
              }
            }
          }
        }
      }
    }
  
  return 1;
}
