/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWWindowReader.h"

#include "vtkKWNotebook.h"
#include "vtkKWUserInterfaceManagerNotebook.h"
#include "vtkKWWindow.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLKWUserInterfaceManagerNotebookReader.h"
#include "XML/vtkXMLKWWindowWriter.h"

vtkStandardNewMacro(vtkXMLKWWindowReader);
vtkCxxRevisionMacro(vtkXMLKWWindowReader, "$Revision: 1.8 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWWindowReader::GetRootElementName()
{
  return "KWWindow";
}

//----------------------------------------------------------------------------
int vtkXMLKWWindowReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWWindow *obj = vtkKWWindow::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWWindow is not set!");
    return 0;
    }

  // User Interface

  vtkXMLDataElement *ui_elem = elem->FindNestedElementWithName(
    vtkXMLKWWindowWriter::GetUserInterfaceElementName());
  if (ui_elem)
    {
    this->ParseUserInterfaceElement(ui_elem);
    }

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLKWWindowReader::ParseUserInterfaceElement(
  vtkXMLDataElement *ui_elem)
{
  if (!ui_elem)
    {
    return 0;
    }

  vtkKWWindow *obj = vtkKWWindow::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWWindow is not set!");
    return 0;
    }

  // Get attributes

  int ival;

  if (ui_elem->GetScalarAttribute("MainPanelVisibility", ival))
    {
    obj->SetMainPanelVisibility(ival);
    }

  vtkXMLDataElement *nested_elem;

  // User Interface Manager

  vtkKWUserInterfaceManagerNotebook *uim_nb = 
    vtkKWUserInterfaceManagerNotebook::SafeDownCast(
      obj->GetMainUserInterfaceManager());
  if (uim_nb)
    {
    nested_elem = ui_elem->FindNestedElementWithName(
      vtkXMLKWWindowWriter::GetUserInterfaceManagerElementName());
    if (nested_elem)
      {
      vtkXMLKWUserInterfaceManagerNotebookReader *xmlr = 
        vtkXMLKWUserInterfaceManagerNotebookReader::New();
      xmlr->SetObject(uim_nb);
      xmlr->ParseInElement(nested_elem);
      xmlr->Delete();
      }
    }

  return 1;
}

