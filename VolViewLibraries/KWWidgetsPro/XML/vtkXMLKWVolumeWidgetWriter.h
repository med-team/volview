/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWVolumeWidgetWriter - vtkKWVolumeWidget XML Writer.
// .SECTION Description
// vtkXMLKWVolumeWidgetWriter provides XML writing functionality to 
// vtkKWVolumeWidget.
// .SECTION See Also
// vtkXMLKWVolumeWidgetReader

#ifndef __vtkXMLKWVolumeWidgetWriter_h
#define __vtkXMLKWVolumeWidgetWriter_h

#include "XML/vtkXMLKWRenderWidgetProWriter.h"

class vtkVolumeProperty;
class vtkImageData;

class VTK_EXPORT vtkXMLKWVolumeWidgetWriter : public vtkXMLKWRenderWidgetProWriter
{
public:
  static vtkXMLKWVolumeWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWVolumeWidgetWriter,vtkXMLKWRenderWidgetProWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the cropping element used inside that tree to
  // store the cropping parameters.
  static const char* GetCroppingElementName();

  // Description:
  // Return the name of the reformat element used inside that tree to
  // store the reformat parameters.
  static const char* GetReformatElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the 3D cursor parameters.
  static const char* GetCursor3DAnnotationElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the bounding box parameters.
  static const char* GetBoundingBoxAnnotationElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the scale bar widget parameters.
  static const char* GetScaleBarWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the orientation widget parameters.
  static const char* GetOrientationWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the scalar bar widget parameters.
  static const char* GetScalarBarWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the plane widget parameters.
  static const char* GetPlaneWidgetElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the lights parameters.
  static const char* GetLightsElementName();

protected:
  vtkXMLKWVolumeWidgetWriter() {};
  ~vtkXMLKWVolumeWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWVolumeWidgetWriter(const vtkXMLKWVolumeWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWVolumeWidgetWriter&);  // Not implemented.
};

#endif

