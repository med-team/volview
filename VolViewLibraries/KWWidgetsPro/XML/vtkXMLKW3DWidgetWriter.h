/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKW3DWidgetWriter - vtkKW3DWidget XML Writer.
// .SECTION Description
// vtkXMLKW3DWidgetWriter provides XML writing functionality to 
// vtkKW3DWidget.
// .SECTION See Also
// vtkXMLKW3DWidgetReader

#ifndef __vtkXMLKW3DWidgetWriter_h
#define __vtkXMLKW3DWidgetWriter_h

#include "XML/vtkXML3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKW3DWidgetWriter : public vtkXML3DWidgetWriter
{
public:
  static vtkXMLKW3DWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKW3DWidgetWriter,vtkXML3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKW3DWidgetWriter() {};
  ~vtkXMLKW3DWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKW3DWidgetWriter(const vtkXMLKW3DWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKW3DWidgetWriter&);  // Not implemented.
};

#endif
