/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWUserInterfaceManagerNotebookWriter - vtkKWUserInterfaceManagerNotebook XML Writer.
// .SECTION Description
// vtkXMLKWUserInterfaceManagerNotebookWriter provides XML writing functionality to 
// vtkKWUserInterfaceManagerNotebook.
// .SECTION See Also
// vtkXMLKWUserInterfaceManagerNotebookReader

#ifndef __vtkXMLKWUserInterfaceManagerNotebookWriter_h
#define __vtkXMLKWUserInterfaceManagerNotebookWriter_h

#include "XML/vtkXMLKWUserInterfaceManagerWriter.h"

class VTK_EXPORT vtkXMLKWUserInterfaceManagerNotebookWriter : public vtkXMLKWUserInterfaceManagerWriter
{
public:
  static vtkXMLKWUserInterfaceManagerNotebookWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWUserInterfaceManagerNotebookWriter,vtkXMLKWUserInterfaceManagerWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the visible pages.
  static const char* GetVisiblePagesElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store a page.
  static const char* GetPageElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the Drag&Drop entries.
  static const char* GetDragAndDropEntriesElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store a Drag&Drop entry.
  static const char* GetDragAndDropEntryElementName();

protected:
  vtkXMLKWUserInterfaceManagerNotebookWriter() {};
  ~vtkXMLKWUserInterfaceManagerNotebookWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWUserInterfaceManagerNotebookWriter(const vtkXMLKWUserInterfaceManagerNotebookWriter&);  // Not implemented.
  void operator=(const vtkXMLKWUserInterfaceManagerNotebookWriter&);  // Not implemented.
};

#endif

