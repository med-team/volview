/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWOrientationWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWOrientationWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLKWOrientationWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWOrientationWidgetWriter, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWOrientationWidgetWriter::GetRootElementName()
{
  return "KWOrientationWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWOrientationWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkKWOrientationWidget *obj = 
    vtkKWOrientationWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWOrientationWidget is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Color", 3, obj->GetColor());

  elem->SetIntAttribute("AnnotationType", obj->GetAnnotationType());

  elem->SetVectorAttribute("Viewport", 4, obj->GetViewport());

  return 1;
}

