/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCursorWidgetReader - vtkKWCursorWidget XML Reader.
// .SECTION Description
// vtkXMLKWCursorWidgetReader provides XML reading functionality to 
// vtkKWCursorWidget.
// .SECTION See Also
// vtkXMLKWCursorWidgetWriter

#ifndef __vtkXMLKWCursorWidgetReader_h
#define __vtkXMLKWCursorWidgetReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKWCursorWidgetReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKWCursorWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWCursorWidgetReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWCursorWidgetReader() {};
  ~vtkXMLKWCursorWidgetReader() {};

private:
  vtkXMLKWCursorWidgetReader(const vtkXMLKWCursorWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWCursorWidgetReader&); // Not implemented    
};

#endif
