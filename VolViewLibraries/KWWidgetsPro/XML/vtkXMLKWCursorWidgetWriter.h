/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCursorWidgetWriter - vtkKWCursorWidget XML Writer.
// .SECTION Description
// vtkXMLKWCursorWidgetWriter provides XML writing functionality to 
// vtkKWCursorWidget.
// .SECTION See Also
// vtkXMLKWCursorWidgetReader

#ifndef __vtkXMLKWCursorWidgetWriter_h
#define __vtkXMLKWCursorWidgetWriter_h

#include "XML/vtkXMLKW3DWidgetWriter.h"

class VTK_EXPORT vtkXMLKWCursorWidgetWriter : public vtkXMLKW3DWidgetWriter
{
public:
  static vtkXMLKWCursorWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWCursorWidgetWriter,vtkXMLKW3DWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLKWCursorWidgetWriter() {};
  ~vtkXMLKWCursorWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLKWCursorWidgetWriter(const vtkXMLKWCursorWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWCursorWidgetWriter&);  // Not implemented.
};

#endif
