/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWMarker2DReader - vtkKWMarker2D XML Reader.
// .SECTION Description
// vtkXMLKWMarker2DReader provides XML reading functionality to 
// vtkKWMarker2D.
// .SECTION See Also
// vtkXMLKWMarker2DWriter

#ifndef __vtkXMLKWMarker2DReader_h
#define __vtkXMLKWMarker2DReader_h

#include "XML/vtkXMLKW3DWidgetReader.h"

class VTK_EXPORT vtkXMLKWMarker2DReader : public vtkXMLKW3DWidgetReader
{
public:
  static vtkXMLKWMarker2DReader* New();
  vtkTypeRevisionMacro(vtkXMLKWMarker2DReader, vtkXMLKW3DWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWMarker2DReader() {};
  ~vtkXMLKWMarker2DReader() {};

private:
  vtkXMLKWMarker2DReader(const vtkXMLKWMarker2DReader&); // Not implemented
  void operator=(const vtkXMLKWMarker2DReader&); // Not implemented    
};

#endif

