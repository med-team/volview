/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKW2DSplineSurfacesWidget - widget for measuring distance along a line
// .SECTION Description

#ifndef __vtkKW2DSplineSurfacesWidget_h
#define __vtkKW2DSplineSurfacesWidget_h

#include "vtkKW3DWidget.h"

#include <vtkstd/map>    // used for keeping track of the SplineSurfaces
#include <vtkstd/string> // used for the key of the SplineSurfaces map

class vtkKW3DSplineSurfacesWidget;
class vtkSplineSurfaceWidget;
class vtkSplineSurface2DWidget;
class vtkProperty;
class vtkPoints;

class VTK_EXPORT vtkKW2DSplineSurfacesWidget : public vtkKW3DWidget
{
public:
  static vtkKW2DSplineSurfacesWidget *New();
  vtkTypeRevisionMacro(vtkKW2DSplineSurfacesWidget, vtkKW3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  virtual void SetEnabled(int enabling);

  // Description:
  // Add, a Spline Surface
  virtual void AddSplineSurface(const char * splineId, vtkSplineSurfaceWidget * spline );
  
  // Description:
  // Get the current number of spline surface
  virtual unsigned int GetNumberOfSplineSurfaces();
   
  // Description:
  // Get the selected active spline surface
  virtual vtkSplineSurface2DWidget * GetSplineSurfaceWidget(const char * splineId);
  
  // Description:
  // Remove a spline surface using its Id
  // Remove a spline surface using its pointer to vtkSplineSurfaceWidget
  virtual void RemoveSplineSurfaceId(const char * splineId);
  virtual void RemoveSplineSurface(vtkSplineSurfaceWidget * surface);

  // Description:
  // Check if a particular spline surface already exists
  virtual int HasSplineSurface(const char * surfaceId);

  // Description:
  // Set/Get the Property associated with the surface of a particular spline widget.
  // Return the Visibility associated with the surface of a particular spline widget.
  virtual  void          SetSplineSurfaceProperty(const char * surfaceId,vtkProperty * property);
  virtual  vtkProperty * GetSplineSurfaceProperty(const char * surfaceId);
  virtual  int   GetSplineSurfaceVisibility(const char * surfaceId);
  virtual  void  SetSplineSurfaceVisibility(int v);
  virtual  void  SetSplineSurfaceVisibility(const char * surfaceId,int v);

  // Description:
  // Update widget representation when the bound have changed.
  virtual void PlaceWidget( double bounds[6] );
  virtual void PlaceWidget() { this->Superclass::PlaceWidget(); }
  virtual void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                           double zmin, double zmax)
    { this->Superclass::PlaceWidget(xmin, xmax, ymin, ymax, zmin, zmax); }

  // Description:
  // Set the normal of the image plane
  // Set the origin of the image plane
  void SetNormal( double normal[3] );
  void SetOrigin( double origin[3] );

  // Description:
  // Get / Set the widget holding multiple spline surfaces
  vtkGetObjectMacro(SplineSurfaces3D, vtkKW3DSplineSurfacesWidget);
  virtual void SetSplineSurfaces3D( vtkKW3DSplineSurfacesWidget * surfaces );

protected:
  vtkKW2DSplineSurfacesWidget();
  ~vtkKW2DSplineSurfacesWidget();

   //handles the events
  static void ProcessEvents(vtkObject* object,
                            unsigned long event,
                            void* clientdata,
                            void* calldata);
 
//BTX
  typedef vtkstd::string                                      MapKeyType;
  typedef vtkstd::map<MapKeyType,vtkSplineSurface2DWidget *>  SplineSurfacesContainer;
  SplineSurfacesContainer SplineSurfaces;
  // The active spline surface
  typedef SplineSurfacesContainer::iterator                 Iterator;
  typedef SplineSurfacesContainer::const_iterator           ConstIterator;

  virtual void AddSplineSurface(const MapKeyType & splineId, vtkSplineSurfaceWidget * spline );
  Iterator Begin();
  Iterator End();
//ETX


  int DeallocateMarkerResources(unsigned int id);


private:
  vtkKW2DSplineSurfacesWidget(const vtkKW2DSplineSurfacesWidget&);  //Not implemented
  void operator=(const vtkKW2DSplineSurfacesWidget&);  //Not implemented


  // The pointer to the 3D collection of splines
  vtkKW3DSplineSurfacesWidget * SplineSurfaces3D;

  // Orientation and position of the plane associated to the ImageWidget.
  double Normal[3];
  double Origin[3];
};

#endif
