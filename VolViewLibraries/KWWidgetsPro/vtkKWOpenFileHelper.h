/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWOpenFileHelper - a "mostly GUI-independent" helper class
// used by vtkKWOpenWizard, and other GUIs like it

#ifndef __vtkKWOpenFileHelper_h
#define __vtkKWOpenFileHelper_h

#include "vtkObject.h" // parent class

class vtkAlgorithm;
class vtkDICOMCollectorOptions;
class vtkImageReader2;
class vtkKWOpenFileHelperInternals;
class vtkKWOpenFileProperties;
class vtkKWOpenWizard;
class vtkStringArray;

class VTK_EXPORT vtkKWOpenFileHelper : public vtkObject
{
public:
  static vtkKWOpenFileHelper* New();
  vtkTypeRevisionMacro(vtkKWOpenFileHelper,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the optionally associated vtkKWOpenWizard (if any).
  vtkGetObjectMacro(OpenWizard, vtkKWOpenWizard);
  virtual void SetOpenWizard(vtkKWOpenWizard *);

  // Description:
  // Set/Get the reader to be used for reading in the data
  vtkGetObjectMacro(LastReader, vtkAlgorithm);
  virtual void SetLastReader(vtkAlgorithm *);

  // Description:
  // Check if a valid can be loaded.
  // Note that this will allocate a new reader if the appropriate reader
  // is found, and update the OpenFileProperties accordingly.
  // FILE_IS_INVALID             = no it probably is not valid
  // FILE_IS_PROBABLY_VALID      = it probably is valid
  // FILE_IS_VALID               = it is valid
  // FILE_IS_VALID_NOT_DATA      = it is valid but not a data file
  // FILE_IS_VALID_NOT_SUPPORTED = it is valid but not supported
  //BTX
  enum
  {
    FILE_IS_INVALID        = 0,
    FILE_IS_PROBABLY_VALID = 1,
    FILE_IS_VALID          = 2,
    FILE_IS_VALID_NOT_DATA = 3,
    FILE_IS_VALID_NOT_SUPPORTED = 4
  };
  //ETX
  virtual int IsFileValid(const char *fname);
  virtual int IsFileValid(vtkStringArray *filenames);

  // Description: 
  // When IsFileValid returns a value of FILE_IS_VALID, it sets the internal 
  // values of this vtkKWOpenFileProperties object to hold the properties
  // of that file.
  vtkGetObjectMacro(OpenFileProperties, vtkKWOpenFileProperties);

  // Description:
  // Add a user-defined valid file extension
  // If that extension is recognized, IsFileValid will automatically return
  // FILE_IS_VALID_NOT_DATA. Extension includes the period (e.g. ".txt").
  // The exension can be a space-separated list of extensions.
  virtual void AddValidFileExtension(
    const char *description, const char *extension);

  // Description:
  // Support DICOM files only
  vtkGetMacro(SupportDICOMFormatOnly, int);
  vtkSetMacro(SupportDICOMFormatOnly, int);
  vtkBooleanMacro(SupportDICOMFormatOnly, int);

  // Description:
  // Support DICOM files only
  vtkGetMacro(AllowVTKUnstructuredGrid, int);
  vtkSetMacro(AllowVTKUnstructuredGrid, int);
  vtkBooleanMacro(AllowVTKUnstructuredGrid, int);

  // Description:
  // Set/Get the DICOM collector options
  vtkGetObjectMacro(DICOMOptions,vtkDICOMCollectorOptions);
  virtual void SetDICOMOptions(vtkDICOMCollectorOptions *);

  // Description:
  // Description of file extensions added via AddValidFileExtension
  // as a Tcl-array formatted string.
  const char* GetFileTypesTclString();

  // Description:
  // Analyze a RAW file and sets the properties of the current reader and
  // the open file properties accordingly.
  virtual void AnalyzeRawFile(const char *fname);

  // Description:
  // Try to find a sequential pattern in a directory.
  // Note that 'pattern' has to be allocated big enough to store the pattern.
  // Return 0 on errors, 1 if a guess was computed.
  static int FindSeriesPattern(
    const char *seed_file, char *pattern, int *zmin, int *zmax);

  // Description:
  // Try to compute (guess) the number of columns and rows in a RAW file
  static int ComputeRawFileColumns(
    const char *fname, int guess, int nb_components);
  static int ComputeRawFileRows(
    const char *fname, int columns, int guess, int nb_components);

protected:
  vtkKWOpenFileHelper();
  ~vtkKWOpenFileHelper();

  int CheckReader(
    vtkImageReader2 *reader, const char *path, int &bestReaderValue);

private:
  vtkKWOpenWizard              *OpenWizard;
  vtkAlgorithm                 *LastReader;
  vtkKWOpenFileProperties      *OpenFileProperties;
  vtkDICOMCollectorOptions     *DICOMOptions;
  vtkKWOpenFileHelperInternals *Internals;

  int SupportDICOMFormatOnly;
  int AllowVTKUnstructuredGrid;

  vtkKWOpenFileHelper(const vtkKWOpenFileHelper&); // Not implemented
  void operator=(const vtkKWOpenFileHelper&); // Not Implemented
};

#endif
