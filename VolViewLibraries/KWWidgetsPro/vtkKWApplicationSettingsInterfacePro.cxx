/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkKWApplicationSettingsInterfacePro.h"

#include "vtkObjectFactory.h"

#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"
#include "vtkKWEntry.h"
#include "vtkKWFrame.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWInternationalization.h"

#include "vtkKWApplicationPro.h"

#include "vtkKWWidgetsProConfigure.h" // Needed for KWWidgetsPro_USE_Flickcurl

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkKWApplicationSettingsInterfacePro);
vtkCxxRevisionMacro(vtkKWApplicationSettingsInterfacePro, "$Revision: 1.8 $");

//----------------------------------------------------------------------------
vtkKWApplicationSettingsInterfacePro::vtkKWApplicationSettingsInterfacePro()
{
  // Flickr settings

  this->FlickrSettingsFrame            = NULL;
  this->FlickrApplicationKeyEntry      = NULL;
  this->FlickrSharedSecretEntry        = NULL;
  this->FlickrAuthenticationTokenEntry = NULL;

  // Graphics settings

  this->GraphicsSettingsFrame = NULL;
  this->UseGPURenderingCheckButton = NULL;
}

//----------------------------------------------------------------------------
vtkKWApplicationSettingsInterfacePro::~vtkKWApplicationSettingsInterfacePro()
{
  // Flickr settings

  if (this->FlickrSettingsFrame)
    {
    this->FlickrSettingsFrame->Delete();
    this->FlickrSettingsFrame = NULL;
    }

  if (this->FlickrApplicationKeyEntry)
    {
    this->FlickrApplicationKeyEntry->Delete();
    this->FlickrApplicationKeyEntry = NULL;
    }

  if (this->FlickrSharedSecretEntry)
    {
    this->FlickrSharedSecretEntry->Delete();
    this->FlickrSharedSecretEntry = NULL;
    }

  if (this->FlickrAuthenticationTokenEntry)
    {
    this->FlickrAuthenticationTokenEntry->Delete();
    this->FlickrAuthenticationTokenEntry = NULL;
    }

  // Graphics settings

  if (this->GraphicsSettingsFrame)
    {
    this->GraphicsSettingsFrame->Delete();
    this->GraphicsSettingsFrame = NULL;
    }

  if (this->UseGPURenderingCheckButton)
    {
    this->UseGPURenderingCheckButton->Delete();
    this->UseGPURenderingCheckButton = NULL;
    }
}

// ---------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::Create()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("The panel is already created.");
    return;
    }

  // Create the superclass instance (and set the application)

  this->Superclass::Create();

  ostrstream tk_cmd;

  vtkKWWidget *page;
  vtkKWFrame *frame;

  page = this->GetPageWidget(this->GetName());
  
#ifdef KWWidgetsPro_USE_Flickcurl
  // --------------------------------------------------------------
  // Flickr settings : main frame

  if (!this->FlickrSettingsFrame)
    {
    this->FlickrSettingsFrame = vtkKWFrameWithLabel::New();
    }

  this->FlickrSettingsFrame->SetParent(this->GetPagesParentWidget());
  this->FlickrSettingsFrame->Create();
  this->FlickrSettingsFrame->SetLabelText(
    ks_("Application Settings|Flickr Settings"));
    
  tk_cmd << "pack " << this->FlickrSettingsFrame->GetWidgetName()
         << " -side top -anchor nw -fill x -padx 2 -pady 2 " 
         << " -in " << page->GetWidgetName() << endl;
  
  frame = this->FlickrSettingsFrame->GetFrame();

  // --------------------------------------------------------------
  // Flickr settings : Application Key

  if (!this->FlickrApplicationKeyEntry)
    {
    this->FlickrApplicationKeyEntry = vtkKWEntryWithLabel::New();
    }

  this->FlickrApplicationKeyEntry->SetParent(frame);
  this->FlickrApplicationKeyEntry->Create();

  this->FlickrApplicationKeyEntry->SetLabelText(ks_("Flickr|API Key:"));
  this->FlickrApplicationKeyEntry->SetLabelWidth(20);
  this->FlickrApplicationKeyEntry->GetWidget()->SetCommand(
    this, "FlickrApplicationKeyCallback");
  this->FlickrApplicationKeyEntry->GetWidget()->SetWidth(40);
  this->FlickrApplicationKeyEntry->SetBalloonHelpString(
    k_("Your Flickr API Key (see http://www.flickr.com/services/api/keys/)"));

  tk_cmd << "pack " << this->FlickrApplicationKeyEntry->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none" << endl;

  // --------------------------------------------------------------
  // Flickr settings : Shared Secret

  if (!this->FlickrSharedSecretEntry)
    {
    this->FlickrSharedSecretEntry = vtkKWEntryWithLabel::New();
    }

  this->FlickrSharedSecretEntry->SetParent(frame);
  this->FlickrSharedSecretEntry->Create();

  this->FlickrSharedSecretEntry->SetLabelText(ks_("Flickr|Shared Secret:"));
  this->FlickrSharedSecretEntry->SetLabelWidth(
    this->FlickrApplicationKeyEntry->GetLabelWidth());
  this->FlickrSharedSecretEntry->GetWidget()->SetCommand(
    this, "FlickrSharedSecretCallback");
  this->FlickrSharedSecretEntry->GetWidget()->SetWidth(
    this->FlickrApplicationKeyEntry->GetWidget()->GetWidth());
  this->FlickrSharedSecretEntry->SetBalloonHelpString(
    k_("Your Flickr Shared Secret (see http://www.flickr.com/services/api/keys/)"));
    
  tk_cmd << "pack " << this->FlickrSharedSecretEntry->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none" << endl;

  // --------------------------------------------------------------
  // Flickr settings : Authentication Token

  if (!this->FlickrAuthenticationTokenEntry)
    {
    this->FlickrAuthenticationTokenEntry = vtkKWEntryWithLabel::New();
    }

  this->FlickrAuthenticationTokenEntry->SetParent(frame);
  this->FlickrAuthenticationTokenEntry->Create();

  this->FlickrAuthenticationTokenEntry->SetLabelText(
    ks_("Flickr|Authentication Token:"));
  this->FlickrAuthenticationTokenEntry->SetLabelWidth(
    this->FlickrApplicationKeyEntry->GetLabelWidth());
  this->FlickrAuthenticationTokenEntry->GetWidget()->SetCommand(
    this, "FlickrAuthenticationTokenCallback");
  this->FlickrAuthenticationTokenEntry->GetWidget()->SetWidth(
    this->FlickrApplicationKeyEntry->GetWidget()->GetWidth());
  this->FlickrAuthenticationTokenEntry->SetBalloonHelpString(
    k_("Your Flickr Authentication Token (see http://www.flickr.com/services/api/keys/)"));
    
  tk_cmd << "pack " << this->FlickrAuthenticationTokenEntry->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none" << endl;
#endif

  // --------------------------------------------------------------
  // Graphics settings : main frame

  if (!this->GraphicsSettingsFrame)
    {
    this->GraphicsSettingsFrame = vtkKWFrameWithLabel::New();
    }

  this->GraphicsSettingsFrame->SetParent(this->GetPagesParentWidget());
  this->GraphicsSettingsFrame->Create();
  this->GraphicsSettingsFrame->SetLabelText(
    ks_("Application Settings|Graphics Settings"));
    
  tk_cmd << "pack " << this->GraphicsSettingsFrame->GetWidgetName()
         << " -side top -anchor nw -fill x -padx 2 -pady 2 " 
         << " -in " << page->GetWidgetName() << endl;
  
  frame = this->GraphicsSettingsFrame->GetFrame();

  // --------------------------------------------------------------
  // Graphics settings : Use GPU Rendering

  if (!this->UseGPURenderingCheckButton)
    {
    this->UseGPURenderingCheckButton = vtkKWCheckButton::New();
    }

  this->UseGPURenderingCheckButton->SetParent(frame);
  this->UseGPURenderingCheckButton->Create();

  this->UseGPURenderingCheckButton->SetText(
    ks_("Application Settings|Use GPU rendering"));
  this->UseGPURenderingCheckButton->SetCommand(
    this, "UseGPURenderingCallback");
  this->UseGPURenderingCheckButton->SetBalloonHelpString(
    k_("Volumetric data displayed in 3D views can be rendered much faster by "
       "offloading computations to your graphics card (GPU). Given the "
       "variety of hardware and graphics drivers, you may experience "
       "application crashes and data losses, please use it at your own risk. "
       "If crashes happens, turn this option OFF make sure to contact us so "
       "that we can improve GPU support, we would welcome your feedback."));

  tk_cmd 
    << "pack " 
    << this->UseGPURenderingCheckButton->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none" << endl;

  // Pack 

  tk_cmd << ends;
  this->Script(tk_cmd.str());
  tk_cmd.rdbuf()->freeze(0);

  // Update according to the current Window

  this->Update();
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::Update()
{
  this->Superclass::Update();

  vtkKWApplicationPro *app_pro = 
    vtkKWApplicationPro::SafeDownCast(this->GetApplication());

  if (!this->IsCreated() || !app_pro)
    {
    return;
    }

#ifdef KWWidgetsPro_USE_Flickcurl
  // Flickr settings : Application Key

  if (this->FlickrApplicationKeyEntry &&
      app_pro->GetFlickrApplicationKey())
    {
    this->FlickrApplicationKeyEntry->GetWidget()->SetValue(
      app_pro->GetFlickrApplicationKey());
    }

  // Flickr settings : Shared Secret

  if (this->FlickrSharedSecretEntry &&
      app_pro->GetFlickrSharedSecret())
    {
    this->FlickrSharedSecretEntry->GetWidget()->SetValue(
      app_pro->GetFlickrSharedSecret());
    }

  // Flickr settings : Authentication Token

  if (this->FlickrAuthenticationTokenEntry &&
      app_pro->GetFlickrAuthenticationToken())
    {
    this->FlickrAuthenticationTokenEntry->GetWidget()->SetValue(
      app_pro->GetFlickrAuthenticationToken());
    }
#endif

  // Graphics settings : Use GPU Rendering
  // Let's disable it in release mode.

  if (this->UseGPURenderingCheckButton && app_pro)
    {
    this->UseGPURenderingCheckButton->SetSelectedState(
      app_pro->GetUseGPURendering());
#if 0
    this->UseGPURenderingCheckButton->SetEnabled(
      app_pro->GetReleaseMode() && !app_pro->GetUseGPURendering() 
      ? 0 : this->GetEnabled());
#endif
    }
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::FlickrApplicationKeyCallback(
  const char *value)
{
#ifdef KWWidgetsPro_USE_Flickcurl
  vtkKWApplicationPro *app_pro = 
    vtkKWApplicationPro::SafeDownCast(this->GetApplication());
  if (app_pro && value)
    {
    app_pro->SetFlickrApplicationKey(value);
    }
#else
  (void)value;
#endif
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::FlickrSharedSecretCallback(
  const char *value)
{
#ifdef KWWidgetsPro_USE_Flickcurl
  vtkKWApplicationPro *app_pro = 
    vtkKWApplicationPro::SafeDownCast(this->GetApplication());
  if (app_pro && value)
    {
    app_pro->SetFlickrSharedSecret(value);
    }
#else
  (void)value;
#endif
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::FlickrAuthenticationTokenCallback(
  const char *value)
{
#ifdef KWWidgetsPro_USE_Flickcurl
  vtkKWApplicationPro *app_pro = 
    vtkKWApplicationPro::SafeDownCast(this->GetApplication());
  if (app_pro && value)
    {
    app_pro->SetFlickrAuthenticationToken(value);
    }
#else
  (void)value;
#endif
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::UseGPURenderingCallback(
  int state)
{
  vtkKWApplicationPro *app_pro = 
    vtkKWApplicationPro::SafeDownCast(this->GetApplication());
  if (app_pro)
    {
    app_pro->SetUseGPURendering(state);
    this->Update();
    }
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

#ifdef KWWidgetsPro_USE_Flickcurl
  // Flickr settings

  if (this->FlickrSettingsFrame)
    {
    this->FlickrSettingsFrame->SetEnabled(this->GetEnabled());
    }

  if (this->FlickrApplicationKeyEntry)
    {
    this->FlickrApplicationKeyEntry->SetEnabled(this->GetEnabled());
    }

  if (this->FlickrSharedSecretEntry)
    {
    this->FlickrSharedSecretEntry->SetEnabled(this->GetEnabled());
    }

  if (this->FlickrAuthenticationTokenEntry)
    {
    this->FlickrAuthenticationTokenEntry->SetEnabled(this->GetEnabled());
    }
#endif

  // Graphics settings

  if (this->GraphicsSettingsFrame)
    {
    this->GraphicsSettingsFrame->SetEnabled(this->GetEnabled());
    }

  if (this->UseGPURenderingCheckButton)
    {
    this->UseGPURenderingCheckButton->SetEnabled(this->GetEnabled());
    }
}

//----------------------------------------------------------------------------
void vtkKWApplicationSettingsInterfacePro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

