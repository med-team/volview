/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyleEventMap - 
// .SECTION Description

#ifndef __vtkKWInteractorStyleEventMap_h
#define __vtkKWInteractorStyleEventMap_h

#include "vtkInteractorStyle.h"

class vtkKWEventMap;

class VTK_EXPORT vtkKWInteractorStyleEventMap : public vtkInteractorStyle
{
public:
  static vtkKWInteractorStyleEventMap *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyleEventMap, vtkInteractorStyle);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Event bindings controlling the effects of pressing mouse buttons,
  // moving the mouse, or pressing keys.
  virtual void OnMouseMove();
  virtual void OnLeftButtonDown();
  virtual void OnLeftButtonUp();
  virtual void OnMiddleButtonDown();
  virtual void OnMiddleButtonUp();
  virtual void OnRightButtonDown();
  virtual void OnRightButtonUp();
  virtual void OnKeyPress();
  virtual void OnChar();

  // Description:
  // Set/Get the event map
  void SetEventMap(vtkKWEventMap *eventMap);
  vtkGetObjectMacro(EventMap, vtkKWEventMap);

  // Description:
  // This id is a hint that should be appended to the call data and
  // should help identify who sent the event. Of course, the client data
  // is a pointer to who sent the event, but in some situation it is either
  // not available nor very usable. Default to -1.
  vtkSetMacro(EventIdentifier, int);
  vtkGetMacro(EventIdentifier, int);

protected:
  vtkKWInteractorStyleEventMap();
  ~vtkKWInteractorStyleEventMap();
  
  virtual int StartAction(const char* )   { return 0; }
  virtual int PerformAction(const char* ) { return 0; }
  virtual int StopAction(const char* )    { return 0; }
  
  vtkKWEventMap *EventMap;
  char *CurrentAction;
  vtkSetStringMacro(CurrentAction);

  int EventIdentifier;

private:
  vtkKWInteractorStyleEventMap(const vtkKWInteractorStyleEventMap&);  // Not implemented
  void operator=(const vtkKWInteractorStyleEventMap&);  // Not implemented
};

#endif
