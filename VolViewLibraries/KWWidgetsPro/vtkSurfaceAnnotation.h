/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkSurfaceAnnotation - a surface
// .SECTION Description

#ifndef __vtkSurfaceAnnotation_h
#define __vtkSurfaceAnnotation_h

#include "vtkObject.h"

class vtkActor;
class vtkPolyData;
class vtkKWRenderWidget;

class VTK_EXPORT vtkSurfaceAnnotation : public vtkObject
{
public:

  static vtkSurfaceAnnotation* New();
  vtkTypeRevisionMacro(vtkSurfaceAnnotation, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Set/Get the input data for this annotation
  virtual void SetInput(vtkPolyData *);
  vtkGetObjectMacro(Input, vtkPolyData);

  // Description:
  // Set/Get the render widget for this annotation
  // Not ref counted because we do not know in Close() if it should
  // be released or not (a widget can be closed without being destroyed,
  // for example when new data is loaded).
  virtual void SetRenderWidget(vtkKWRenderWidget*);
  vtkGetObjectMacro(RenderWidget, vtkKWRenderWidget);

  // Description:
  // Remove any actors related to this annotation.
  virtual void Remove();

  // Description:
  // Close out (and remove any actors) prior to deletion.
  virtual void Close();
  
protected:
  vtkSurfaceAnnotation();
  ~vtkSurfaceAnnotation();

  vtkPolyData       *Input;
  vtkKWRenderWidget *RenderWidget;
  vtkActor *Actor;

private:
  vtkSurfaceAnnotation(const vtkSurfaceAnnotation&); // Not implemented
  void operator=(const vtkSurfaceAnnotation&); // Not implemented
};

#endif
