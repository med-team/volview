/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyleVolumeView -
// .SECTION Description
//

#ifndef __vtkKWInteractorStyleVolumeView_h
#define __vtkKWInteractorStyleVolumeView_h

#include "vtkKWInteractorStyleView.h"

class VTK_EXPORT vtkKWInteractorStyleVolumeView : public vtkKWInteractorStyleView
{
public:
  static vtkKWInteractorStyleVolumeView *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyleVolumeView, vtkKWInteractorStyleView);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Actions to perform when an event happens
  virtual void Rotate();
  virtual void Roll();
  virtual void Pan();
  virtual void Zoom();
  virtual void Fly(int direction);
  virtual void Reset();

  vtkSetMacro(FlySpeed, double);
  vtkGetMacro(FlySpeed, double);
  
  // Description:
  // Mouse wheel is translated into zoom in/out
  virtual void OnMouseWheelForward();
  virtual void OnMouseWheelBackward();

protected:
  vtkKWInteractorStyleVolumeView();
  ~vtkKWInteractorStyleVolumeView() {};

  virtual int StartAction(const char* action);
  virtual int PerformAction(const char* action);
  virtual int StopAction(const char* action);

  int FlyFlag;
  int InFlight;
  double FlySpeed;

  virtual void PerformZoom(int prev_pos, int current_pos);

private:
  vtkKWInteractorStyleVolumeView(const vtkKWInteractorStyleVolumeView&);  // Not implemented
  void operator=(const vtkKWInteractorStyleVolumeView&);  // Not implemented
};

#endif
