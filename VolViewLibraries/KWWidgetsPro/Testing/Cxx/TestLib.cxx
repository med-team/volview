/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWApplicationPro.h"

#ifdef KWWidgets_BUILD_VTK_WIDGETS
#include "vtkKWCroppingRegionsWidget.h"
#include "vtkKWCursorWidget.h"
#include "vtkKWGenericRenderWindowInteractor.h"
#include "vtkKWInteractorStyleEventMap.h"
#include "vtkKWInteractorStyleImageView.h"
#include "vtkKWInteractorStyleLightboxView.h"
#include "vtkKWInteractorStyleVolumeView.h"
#include "vtkKWOrientationWidget.h"
#include "vtkKWProgressCommand.h"
#include "vtkKWScaleBarWidget.h"
#include "vtkSideAnnotation.h"
#endif

#include "vtkKWWidgetsConfigure.h" // Needed for KWWidgets_BUILD_VTK_WIDGETS

#ifdef _WIN32
#include <windows.h>
#endif

template<class DType>
int DoTest(DType*)
{
  DType * t = DType::New();
  t->GetClassName();
  t->Delete();
  return 0;
}

int main(int argc, char **argv)
{
  ostrstream err;
  Tcl_Interp *interp = vtkKWApplicationPro::InitializeTcl(argc, argv, &err);
  if (!interp)
    {
    err << ends;
#ifdef _WIN32
    ::MessageBox(0, err.str(), 
                 "Error: InitializeTcl failed", MB_ICONERROR|MB_OK);
#else
    cerr << "Error: InitializeTcl failed: " << err.str() << endl;
#endif
    return 1;
    }

  int res = 0;
  res += ::DoTest(static_cast<vtkKWApplicationPro*>(0));
#ifdef KWWidgets_BUILD_VTK_WIDGETS
  res += ::DoTest(static_cast<vtkKWCroppingRegionsWidget*>(0));
  res += ::DoTest(static_cast<vtkKWCursorWidget*>(0));
  res += ::DoTest(static_cast<vtkKWInteractorStyleEventMap*>(0));
  res += ::DoTest(static_cast<vtkKWInteractorStyleImageView*>(0));
  res += ::DoTest(static_cast<vtkKWInteractorStyleLightboxView*>(0));
  res += ::DoTest(static_cast<vtkKWInteractorStyleVolumeView*>(0));
  res += ::DoTest(static_cast<vtkKWOrientationWidget*>(0));
  res += ::DoTest(static_cast<vtkKWProgressCommand*>(0));
  res += ::DoTest(static_cast<vtkKWScaleBarWidget*>(0));
  res += ::DoTest(static_cast<vtkSideAnnotation*>(0));
#endif
  return res;
}
