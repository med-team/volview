/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWHeaderAnnotationEditorPro.h"

#include "vtkObjectFactory.h"
#include "vtkKWRenderWidget.h"
#include "vtkTextActor.h"

#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLTextActorWriter.h"
#endif

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWHeaderAnnotationEditorPro );
vtkCxxRevisionMacro(vtkKWHeaderAnnotationEditorPro, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkKWHeaderAnnotationEditorPro::vtkKWHeaderAnnotationEditorPro()
{
}

//----------------------------------------------------------------------------
vtkKWHeaderAnnotationEditorPro::~vtkKWHeaderAnnotationEditorPro()
{
}


//----------------------------------------------------------------------------
void vtkKWHeaderAnnotationEditorPro::CreateWidget()
{
  // Create the superclass widgets

  if (this->IsCreated())
    {
    vtkErrorMacro("HeaderAnnotation already created");
    return;
    }

  this->Superclass::CreateWidget();
}


//----------------------------------------------------------------------------
void vtkKWHeaderAnnotationEditorPro::SendChangedEvent()
{
  if (!this->RenderWidget || !this->RenderWidget->GetHeaderAnnotation())
    {
    return;
    }

#ifdef KWCommonPro_USE_XML_RW
  ostrstream event;

  vtkXMLTextActorWriter *xmlw = vtkXMLTextActorWriter::New();
  xmlw->SetObject(this->RenderWidget->GetHeaderAnnotation());
  xmlw->WriteToStream(event);
  xmlw->Delete();

  event << ends;

  this->InvokeEvent(this->AnnotationChangedEvent, event.str());
  event.rdbuf()->freeze(0);
#else
  this->InvokeEvent(this->AnnotationChangedEvent, NULL);
#endif
}

//----------------------------------------------------------------------------
void vtkKWHeaderAnnotationEditorPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
