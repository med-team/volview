/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWScalarBarAnnotationPro.h"

#include "vtkObjectFactory.h"
#include "vtkScalarBarWidget.h"

#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLScalarBarWidgetWriter.h"
#endif

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWScalarBarAnnotationPro );
vtkCxxRevisionMacro(vtkKWScalarBarAnnotationPro, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkKWScalarBarAnnotationPro::vtkKWScalarBarAnnotationPro()
{
}

//----------------------------------------------------------------------------
vtkKWScalarBarAnnotationPro::~vtkKWScalarBarAnnotationPro()
{
}


//----------------------------------------------------------------------------
void vtkKWScalarBarAnnotationPro::CreateWidget()
{
  // Create the superclass widgets

  if (this->IsCreated())
    {
    vtkErrorMacro("ScalarBarAnnotation already created");
    return;
    }

  this->Superclass::CreateWidget();

}

//----------------------------------------------------------------------------
void vtkKWScalarBarAnnotationPro::SendChangedEvent()
{
  if (!this->ScalarBarWidget || !this->ScalarBarWidget->GetScalarBarActor())
    {
    return;
    }

#ifdef KWCommonPro_USE_XML_RW
  ostrstream event;

  vtkXMLScalarBarWidgetWriter *xmlw = vtkXMLScalarBarWidgetWriter::New();
  xmlw->SetObject(this->ScalarBarWidget);
  xmlw->WriteToStream(event);
  xmlw->Delete();

  event << ends;

  this->InvokeEvent(this->AnnotationChangedEvent, event.str());
  event.rdbuf()->freeze(0);
#else
  this->InvokeEvent(this->AnnotationChangedEvent, NULL);
#endif
}

//----------------------------------------------------------------------------
void vtkKWScalarBarAnnotationPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
