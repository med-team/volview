/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWInteractorStyleView -
// .SECTION Description
//

#ifndef __vtkKWInteractorStyleView_h
#define __vtkKWInteractorStyleView_h

#include "vtkKWInteractorStyleEventMap.h"

class vtkKWRenderWidget;

class VTK_EXPORT vtkKWInteractorStyleView : public vtkKWInteractorStyleEventMap
{
public:
  static vtkKWInteractorStyleView *New();
  vtkTypeRevisionMacro(vtkKWInteractorStyleView, vtkKWInteractorStyleEventMap);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Actions to perform when an event happens
  virtual void ToggleMarker2D();

protected:
  vtkKWInteractorStyleView();
  ~vtkKWInteractorStyleView();

  virtual int PerformAction(const char* action);

  // Description:
  // Convenience method to get the widget currently using that style
  virtual vtkKWRenderWidget* GetRenderWidget();

  // Description:
  // Convenience method to render the widget currently using that style
  // during interaction
  virtual void PerformInteractiveRender();

private:
  vtkKWInteractorStyleView(const vtkKWInteractorStyleView&);  // Not implemented
  void operator=(const vtkKWInteractorStyleView&);  // Not implemented
};

#endif
