/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWHeaderAnnotationEditorPro - a header annotation widget
// .SECTION Description
// Derived class of the vtkKWHeaderAnnotationEditor, that adds XML support

#ifndef __vtkKWHeaderAnnotationEditorPro_h
#define __vtkKWHeaderAnnotationEditorPro_h

#include "vtkKWHeaderAnnotationEditor.h"


class VTK_EXPORT vtkKWHeaderAnnotationEditorPro : public vtkKWHeaderAnnotationEditor
{
public:
  static vtkKWHeaderAnnotationEditorPro* New();
  vtkTypeRevisionMacro(vtkKWHeaderAnnotationEditorPro,vtkKWHeaderAnnotationEditor);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkKWHeaderAnnotationEditorPro();
  ~vtkKWHeaderAnnotationEditorPro();

  // Description:
  // Create the widget.
  virtual void CreateWidget();
  
  // Send an event representing the state of the widget
  virtual void SendChangedEvent();

private:
  vtkKWHeaderAnnotationEditorPro(const vtkKWHeaderAnnotationEditorPro&); // Not implemented
  void operator=(const vtkKWHeaderAnnotationEditorPro&); // Not Implemented
};

#endif

