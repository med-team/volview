/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWApplicationPro - the application class with licensing support.
// .SECTION Description
// A subclass of vtkKWApplication that provides licensing support

#ifndef __vtkKWApplicationPro_h
#define __vtkKWApplicationPro_h

#include "vtkKWApplication.h"

class vtkImageData;

class VTK_EXPORT vtkKWApplicationPro : public vtkKWApplication
{
public:
  static vtkKWApplicationPro* New();
  vtkTypeRevisionMacro(vtkKWApplicationPro,vtkKWApplication);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Start running the main application.
  virtual void Start() { this->Superclass::Start(); }
  virtual void Start(int argc, char *argv[]);

  // Description:
  // Query if the application is allowed to run, given the current
  // licensing scheme, or the expiration date, etc.
  virtual int IsAllowedToRun();

  // Description:
  // Invoke the registration wizard. 
  // Return 1 if the application is ready to run according to the
  // current license requirement, 0 otherwise (or Cancel())
  virtual int InvokeRegistrationWizard(vtkKWWindowBase *masterWin);

  // Description:
  // Set/Get the purchase URL, i.e. the web address where the application
  // can be purchased manually.
  vtkSetStringMacro(PurchaseURL);
  vtkGetStringMacro(PurchaseURL);

  // Description:
  // Set/Get the name of the company providing the application. 
  // Default to: Kitware.
  vtkSetStringMacro(CompanyName);
  vtkGetStringMacro(CompanyName);

  // Description:
  // Set/Get the way to contact the company providing the application,
  // for sales-related question or support question. 
  // Sales default to: kitware@kitware.com or phone number/fax.
  // Support default to: support@kitware.com or phone number/fax.
  vtkSetStringMacro(CompanySalesContact);
  vtkGetStringMacro(CompanySalesContact);
  vtkSetStringMacro(CompanySupportContact);
  vtkGetStringMacro(CompanySupportContact);

  // Description:
  // Set/Get the primary copyright holder. They will show in the 
  // "About" dialog box, on top of all other copyrights notice.
  vtkSetStringMacro(PrimaryCopyright);
  vtkGetStringMacro(PrimaryCopyright);

  // Description:
  // Set/Get if the application is run in a testing mode.
  //BTX
  vtkBooleanMacro(TestingMode, int);
  vtkSetMacro(TestingMode, int);
  vtkGetMacro(TestingMode, int);
  //ETX

  // Description:
  // Set/Get if the application should use GPU rendering.
  // Windows that create any 3D views (and vtkKWVolumeWidget) should make
  // sure they propagate that setting to their volume rendering mapper.
  // They should also listen to the UseGPURenderingChangedEvent.
  vtkBooleanMacro(UseGPURendering, int);
  virtual void SetUseGPURendering(int);
  vtkGetMacro(UseGPURendering, int);

  // Description:
  // Set an expiration time (in secs. since Epoch, or as a YYYY-MM-DD HH:MM:SS
  // string; MM (1-12), DD (1-31), HH (0-23).
  // Check if the app has expired.
  //BTX
  virtual void SetExpireTime(double);
  vtkGetMacro(ExpireTime, double);
  virtual void SetExpireTime(const char *str);
  //ETX
  virtual int HasExpired();

  // Description:
  // Get/Set the internal character encoding of the application.
  virtual void SetCharacterEncoding(int val);

  // Description:
  // Log a message.  The logging mechanism is application specific,
  // for KWAppplicationPro, it is non-existent.
  virtual void LogMessage(const char * ) {}

  // Description:
  // Initialize Tcl/Tk
  // Return NULL on error (eventually provides an ostream where detailed
  // error messages will be stored).
  //BTX
  static Tcl_Interp *InitializeTcl(int argc, char *argv[], ostream *err = 0);
  //ETX

  // Description:
  // Set/Get the Flickr application key, shared secret and authentication
  // token.
  //BTX
  vtkSetStringMacro(FlickrApplicationKey);
  vtkGetStringMacro(FlickrApplicationKey);
  vtkSetStringMacro(FlickrSharedSecret);
  vtkGetStringMacro(FlickrSharedSecret);
  vtkSetStringMacro(FlickrAuthenticationToken);
  vtkGetStringMacro(FlickrAuthenticationToken);
  //ETX

  // Descrition:
  // Save/Retrieve the application settings to/from registry.
  // Do not call that method before the application name is known and the
  // proper registry level set (if any).
  virtual void RestoreApplicationSettingsFromRegistry();
  virtual void SaveApplicationSettingsToRegistry();

  // Description:
  // Add system information to a stream.
  // Will be used to report system info in the About dialog, or the Feedback
  // email...
  virtual void AddSystemInformation(ostream &);
  
  // Description:
  // Some constants
  //BTX
  static const char *FlickrRegSubKey;
  static const char *FlickrApplicationKeyRegKey;
  static const char *FlickrSharedSecretRegKey;
  static const char *FlickrAuthenticationTokenRegKey;
  static const char *UseGPURenderingRegKey;
  //ETX

  // Description:
  // Event list
  //BTX
  enum
  {
    UseGPURenderingChangedEvent = 25000
  };
  //ETX

protected:
  vtkKWApplicationPro();
  ~vtkKWApplicationPro();

  // Description:
  // Give a change to the application to parse the command-line arguments
  // and set some variables accordingly. Subclasses that do not intend
  // to call their super's Start(argc, argv) should make sure they
  // call their super's ParseCommandLineArguments.
  virtual void ParseCommandLineArguments(int argc, char *argv[]);

  int TestingMode;
  int UseGPURendering;

  double ExpireTime;

  // Description:
  // Cleanup before exit.
  virtual void PrepareForDelete();

  virtual void AddAboutText(ostream &);
  virtual void AddAboutCopyrights(ostream &);

  char *PrimaryCopyright;
  char *PurchaseURL;
  char *CompanyName;
  char *CompanySalesContact;
  char *CompanySupportContact;

  char *FlickrApplicationKey;
  char *FlickrSharedSecret;
  char *FlickrAuthenticationToken;

  // Description:
  // Send image to Flickr
  virtual int SendImageToFlickr(vtkImageData *img,
                                const char *api_key,
                                const char *shared_secret,
                                const char *auth_token,
                                const char *title,
                                const char *description,
                                const char *tags,
                                int is_public);

  // Description:
  // Send screenshot to Flickr
  virtual int SendScreenshotToFlickr();

private:
  vtkKWApplicationPro(const vtkKWApplicationPro&); // Not implemented
  void operator=(const vtkKWApplicationPro&); // Not implemented
};

#endif
