/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWCornerAnnotationEditorPro.h"

#include "vtkObjectFactory.h"
#include "vtkCornerAnnotation.h"

#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLCornerAnnotationWriter.h"
#endif

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWCornerAnnotationEditorPro );
vtkCxxRevisionMacro(vtkKWCornerAnnotationEditorPro, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkKWCornerAnnotationEditorPro::vtkKWCornerAnnotationEditorPro()
{
}

//----------------------------------------------------------------------------
vtkKWCornerAnnotationEditorPro::~vtkKWCornerAnnotationEditorPro()
{
}


//----------------------------------------------------------------------------
void vtkKWCornerAnnotationEditorPro::CreateWidget()
{
  // Create the superclass widgets

  if (this->IsCreated())
    {
    vtkErrorMacro("CornerAnnotationPro already created");
    return;
    }

  this->Superclass::CreateWidget();

}


//----------------------------------------------------------------------------
void vtkKWCornerAnnotationEditorPro::SendChangedEvent()
{
  if (!this->CornerAnnotation)
    {
    return;
    }

#ifdef KWCommonPro_USE_XML_RW
  ostrstream event;

  vtkXMLCornerAnnotationWriter *xmlw = vtkXMLCornerAnnotationWriter::New();
  xmlw->SetObject(this->CornerAnnotation);
  xmlw->WriteToStream(event);
  xmlw->Delete();

  event << ends;

  this->InvokeEvent(this->AnnotationChangedEvent, event.str());
  event.rdbuf()->freeze(0);
#else
  this->InvokeEvent(this->AnnotationChangedEvent, NULL);
#endif
}


//----------------------------------------------------------------------------
void vtkKWCornerAnnotationEditorPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

