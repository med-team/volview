/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWSurfaceMaterialPropertyWidgetPro.h"

#include "vtkProperty.h"
#include "vtkObjectFactory.h"

#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLPropertyWriter.h"
#endif

//----------------------------------------------------------------------------

vtkStandardNewMacro(vtkKWSurfaceMaterialPropertyWidgetPro);
vtkCxxRevisionMacro(vtkKWSurfaceMaterialPropertyWidgetPro, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
void vtkKWSurfaceMaterialPropertyWidgetPro::SendStateEvent(int event)
{
  if (!this->Property)
    {
    return;
    }
  
#ifdef KWCommonPro_USE_XML_RW
  ostrstream event_str;

  vtkXMLPropertyWriter *xmlw = vtkXMLPropertyWriter::New();
  xmlw->SetObject(this->Property);
  xmlw->OutputShadingOnlyOn();
  xmlw->WriteToStream(event_str);
  xmlw->Delete();

  event_str << ends;
  this->InvokeEvent(event, event_str.str());
  event_str.rdbuf()->freeze(0);
#else
  this->InvokeEvent(event, NULL);
#endif
}

//----------------------------------------------------------------------------
void vtkKWSurfaceMaterialPropertyWidgetPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
