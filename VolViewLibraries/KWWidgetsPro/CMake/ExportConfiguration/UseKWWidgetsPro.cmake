##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
#
# This module is provided as KWWidgetsPro_USE_FILE by KWWidgetsProConfig.cmake.
# It can be INCLUDEd in a project to load the needed compiler and linker
# settings to use KWWidgetsPro:
#   FIND_PACKAGE(KWWidgetsPro REQUIRED)
#   INCLUDE(${KWWidgetsPro_USE_FILE})

IF(NOT KWWidgetsPro_USE_FILE_INCLUDED)
  SET(KWWidgetsPro_USE_FILE_INCLUDED 1)

  # Load the compiler settings used for KWWidgetsPro.
  IF(KWWidgetsPro_BUILD_SETTINGS_FILE)
    INCLUDE(${CMAKE_ROOT}/Modules/CMakeImportBuildSettings.cmake)
    CMAKE_IMPORT_BUILD_SETTINGS(${KWWidgetsPro_BUILD_SETTINGS_FILE})
  ENDIF(KWWidgetsPro_BUILD_SETTINGS_FILE)

  # Add compiler flags needed to use KWWidgetsPro.
  SET(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} ${KWWidgetsPro_REQUIRED_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} ${KWWidgetsPro_REQUIRED_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} ${KWWidgetsPro_REQUIRED_EXE_LINKER_FLAGS}")
  SET(CMAKE_SHARED_LINKER_FLAGS
    "${CMAKE_SHARED_LINKER_FLAGS} ${KWWidgetsPro_REQUIRED_SHARED_LINKER_FLAGS}")
  SET(CMAKE_MODULE_LINKER_FLAGS
    "${CMAKE_MODULE_LINKER_FLAGS} ${KWWidgetsPro_REQUIRED_MODULE_LINKER_FLAGS}")

  # Add include directories needed to use KWWidgetsPro.
  INCLUDE_DIRECTORIES(${KWWidgetsPro_INCLUDE_DIRS})

  # Add link directories needed to use KWWidgetsPro.
  LINK_DIRECTORIES(${KWWidgetsPro_LIBRARY_DIRS})

  # Add cmake module path.
  SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${KWWidgetsPro_CMAKE_DIR}")

  # Use KWWidgets.
  IF(NOT KWWidgetsPro_NO_USE_KWWidgets)
    SET(KWWidgets_DIR ${KWWidgetsPro_KWWidgets_DIR})
    FIND_PACKAGE(KWWidgets)
    IF(KWWidgets_FOUND)
      INCLUDE(${KWWidgets_USE_FILE})
    ELSE(KWWidgets_FOUND)
      MESSAGE("KWWidgets not found in KWWidgetsPro_KWWidgets_DIR=\"${KWWidgetsPro_KWWidgets_DIR}\".")
    ENDIF(KWWidgets_FOUND)
  ENDIF(NOT KWWidgetsPro_NO_USE_KWWidgets)

  # Use KWCommonPro.
  IF(KWWidgets_BUILD_VTK_WIDGETS AND NOT KWWidgetsPro_NO_USE_KWCommonPro)
    SET(KWCommonPro_DIR ${KWWidgetsPro_KWCommonPro_DIR})
    FIND_PACKAGE(KWCommonPro)
    IF(KWCommonPro_FOUND)
      INCLUDE(${KWCommonPro_USE_FILE})
    ELSE(KWCommonPro_FOUND)
      MESSAGE("KWCommonPro not found in KWWidgetsPro_KWCommonPro_DIR=\"${KWWidgetsPro_KWCommonPro_DIR}\".")
    ENDIF(KWCommonPro_FOUND)
  ENDIF(KWWidgets_BUILD_VTK_WIDGETS AND NOT KWWidgetsPro_NO_USE_KWCommonPro)

  # Use Flickcurl.
  IF(KWWidgetsPro_USE_Flickcurl)
    SET(Flickcurl_DIR ${KWWidgetsPro_Flickcurl_DIR})
    FIND_PACKAGE(Flickcurl)
    IF(Flickcurl_FOUND)
      INCLUDE(${Flickcurl_USE_FILE})
    ELSE(Flickcurl_FOUND)
      MESSAGE("Flickcurl not found in KWWidgetsPro_Flickcurl_DIR=\"${KWWidgetsPro_Flickcurl_DIR}\".")
    ENDIF(Flickcurl_FOUND)
  ENDIF(KWWidgetsPro_USE_Flickcurl)

ENDIF(NOT KWWidgetsPro_USE_FILE_INCLUDED)
