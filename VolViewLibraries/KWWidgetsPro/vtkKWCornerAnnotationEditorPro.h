/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWCornerAnnotationEditorPro - a corner annotation widget
// .SECTION Description
// This class derives from vtkKWCornerAnnotationEditor and adds XML support.

#ifndef __vtkKWCornerAnnotationEditorPro_h
#define __vtkKWCornerAnnotationEditorPro_h

#include "vtkKWCornerAnnotationEditor.h"


class VTK_EXPORT vtkKWCornerAnnotationEditorPro : public vtkKWCornerAnnotationEditor
{
public:
  static vtkKWCornerAnnotationEditorPro* New();
  vtkTypeRevisionMacro(vtkKWCornerAnnotationEditorPro,vtkKWCornerAnnotationEditor);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkKWCornerAnnotationEditorPro();
  ~vtkKWCornerAnnotationEditorPro();
  
 // Description:
  // Create the widget.
  virtual void CreateWidget();

  // Send an event representing the state of the widget
  // This method provides XML support.
  virtual void SendChangedEvent();

private:
  vtkKWCornerAnnotationEditorPro(const vtkKWCornerAnnotationEditorPro&); // Not implemented
  void operator=(const vtkKWCornerAnnotationEditorPro&); // Not Implemented
};

#endif

