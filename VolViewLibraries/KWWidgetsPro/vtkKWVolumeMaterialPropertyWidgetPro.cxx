/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWVolumeMaterialPropertyWidgetPro.h"

#include "vtkObjectFactory.h"
#include "vtkVolumeProperty.h"

#include "vtkKWWidgetsConfigure.h" // Needed for KWWidgets_BUILD_VTK_WIDGETS
#ifdef KWWidgets_BUILD_VTK_WIDGETS
#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW
#endif

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLVolumePropertyWriter.h"
#endif

//----------------------------------------------------------------------------

vtkStandardNewMacro(vtkKWVolumeMaterialPropertyWidgetPro);
vtkCxxRevisionMacro(vtkKWVolumeMaterialPropertyWidgetPro, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
void vtkKWVolumeMaterialPropertyWidgetPro::SendStateEvent(int event)
{
  if (!this->VolumeProperty)
    {
    return;
    }
  
#ifdef KWCommonPro_USE_XML_RW
  ostrstream event_str;

  vtkXMLVolumePropertyWriter *xmlw = vtkXMLVolumePropertyWriter::New();
  xmlw->SetObject(this->VolumeProperty);
  xmlw->OutputShadingOnlyOn();
  xmlw->SetNumberOfComponents(this->NumberOfComponents);
  xmlw->WriteToStream(event_str);
  xmlw->Delete();

  event_str << ends;
  this->InvokeEvent(event, event_str.str());
  event_str.rdbuf()->freeze(0);
#else
  this->InvokeEvent(event, NULL);
#endif
}

//----------------------------------------------------------------------------
void vtkKWVolumeMaterialPropertyWidgetPro::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
