/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWVolumeMaterialPropertyWidgetPro - widget to control the material property of a volume
// .SECTION Description

#ifndef __vtkKWVolumeMaterialPropertyWidgetPro_h
#define __vtkKWVolumeMaterialPropertyWidgetPro_h

#include "vtkKWVolumeMaterialPropertyWidget.h"

class VTK_EXPORT vtkKWVolumeMaterialPropertyWidgetPro : public vtkKWVolumeMaterialPropertyWidget
{
public:
  static vtkKWVolumeMaterialPropertyWidgetPro *New();
  vtkTypeRevisionMacro(vtkKWVolumeMaterialPropertyWidgetPro, vtkKWVolumeMaterialPropertyWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  
protected:

  vtkKWVolumeMaterialPropertyWidgetPro() {};
  ~vtkKWVolumeMaterialPropertyWidgetPro() {};

  // Send an event representing the state of the widget
  virtual void SendStateEvent(int event);

private:
  vtkKWVolumeMaterialPropertyWidgetPro(const vtkKWVolumeMaterialPropertyWidgetPro&);  //Not implemented
  void operator=(const vtkKWVolumeMaterialPropertyWidgetPro&);  //Not implemented
};

#endif
