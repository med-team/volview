/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWMarker2D - a 2D cursor annotation
// .SECTION Description
// The vtkKWMarker2D class defines a 2D cursor that can be positioned
// anywhere in the view.


#ifndef __vtkKWMarker2D_h
#define __vtkKWMarker2D_h

#include "vtkKW3DWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkActor2D;
class vtkPoints;

class VTK_EXPORT vtkKWMarker2D : public vtkKW3DWidget
{
public:
  static vtkKWMarker2D* New();
  vtkTypeRevisionMacro(vtkKWMarker2D,vtkKW3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // This method is used to initially place the widget.
  //BTX
  virtual void PlaceWidget(double vtkNotUsed(bounds)[6]) {};
  void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}
  //ETX

  // Description:
  // Set the position of the marker.
  virtual void SetPosition(double *pos);
  virtual void SetPosition(double x1, double y1, double x2, double y2);
  virtual void GetPosition(double *pos);

  // Description:
  // Set/Get the size of widget.
  virtual void SetSize(double x, double y);
  virtual void SetSize(double* s) { this->SetSize(s[0], s[1]); };
  virtual void GetSize(double* s);

  // Description:
  // Set/Get color.
  virtual void SetColor(double r, double g, double b);
  virtual void SetColor(double rgb[3]) 
    { this->SetColor(rgb[0], rgb[1], rgb[2]); };
  virtual double* GetColor();

  // Description:
  // Methods for turning the interactor observer on and off.
  virtual void SetEnabled(int);

protected:
  vtkKWMarker2D();
  ~vtkKWMarker2D();

  //handles the events
  static void ProcessEvents(vtkObject* object, 
                            unsigned long event,
                            void* clientdata, 
                            void* calldata);

  // ProcessEvents() dispatches to these methods.
  void OnLeftButtonDown();
  void OnLeftButtonUp();
  void OnMouseMove();

  // used to compute relative movements
  double StartPosition[2];

  //BTX - manage the state of the widget
  int State;
  enum WidgetState
  {
    Moving=0,
    AdjustingP1,
    AdjustingP2,
    AdjustingP3,
    AdjustingP4,
    AdjustingE1,
    AdjustingE2,
    AdjustingE3,
    AdjustingE4,
    Inside,
    Outside
  };
  //ETX

  // use to determine what state the mouse is over, edge1 p1, etc.
  // returns a state from the WidgetState enum above
  int ComputeStateBasedOnPosition(int X, int Y, int *pos1, int *pos2);

  // set the cursor to the correct shape based on State argument
  void SetCursor(int State);

  vtkActor2D *Actor;
  vtkPoints*  Points;

private:
  vtkKWMarker2D(const vtkKWMarker2D&); // Not implemented
  void operator=(const vtkKWMarker2D&); // Not implemented
};

#endif
