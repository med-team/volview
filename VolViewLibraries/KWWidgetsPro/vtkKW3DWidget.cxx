/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKW3DWidget.h"

#include "vtkKWRenderWidget.h"

vtkCxxRevisionMacro(vtkKW3DWidget, "$Revision: 1.13 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLKW3DWidgetReader.h"
#include "XML/vtkXMLKW3DWidgetWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkKW3DWidget, vtkXMLKW3DWidgetReader, vtkXMLKW3DWidgetWriter);

//----------------------------------------------------------------------------
vtkKW3DWidget::vtkKW3DWidget()
{
  this->Parent = 0;
  this->PlaceFactor = 1.0;

  for (int i = 0; i < 3; i++)
    {
    this->InitialBounds[i * 2] = VTK_FLOAT_MAX;
    this->InitialBounds[i * 2 + 1] = VTK_FLOAT_MIN;
    }
}

//----------------------------------------------------------------------------
void vtkKW3DWidget::PlaceWidget(double bounds[6])
{
  double center[6];
  this->AdjustBounds(bounds, this->InitialBounds, center);
  
  for (int i = 0; i < 3; i++)
    {
    if (this->InitialBounds[i * 2] > this->InitialBounds[i * 2 + 1])
      {
      double temp = this->InitialBounds[i * 2];
      this->InitialBounds[i * 2] = this->InitialBounds[i * 2 + 1];
      this->InitialBounds[i * 2 + 1] = temp;
      }
    }
}

//----------------------------------------------------------------------------
void vtkKW3DWidget::AdjustWidget(double bounds[6])
{
  double center[6], new_bounds[6];
  this->AdjustBounds(bounds, new_bounds, center);
  
  for (int i = 0; i < 6; i++)
    {
    if (this->InitialBounds[i] != new_bounds[i])
      {
      this->PlaceWidget(bounds);
      return;
      }
    }
}

//----------------------------------------------------------------------------
int vtkKW3DWidget::GetCurrentRendererIndex()
{
  vtkKWRenderWidget *rw =
    vtkKWRenderWidget::SafeDownCast(this->GetParent());
  if (!rw)
    {
    return -1;
    }

  return rw->GetRendererIndex(this->CurrentRenderer);
}

//----------------------------------------------------------------------------
void vtkKW3DWidget::SetCurrentRendererToNthRenderer(int id)
{
  if (id == this->GetCurrentRendererIndex())
    {
    return;
    }

  vtkKWRenderWidget *rw =
    vtkKWRenderWidget::SafeDownCast(this->GetParent());
  if (!rw)
    {
    return;
    }

  vtkRenderer *ren = rw->GetNthRenderer(id);
  if (ren)
    {
    int state = this->GetEnabled();
    this->EnabledOff();
    this->SetCurrentRenderer(ren);
    this->SetEnabled(state);
    }
}

//----------------------------------------------------------------------------
void vtkKW3DWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);

  os << indent << "InitialBounds: " << endl
     << indent << "  Xmin,Xmax: (" << this->InitialBounds[0] << ", "
     << this->InitialBounds[1] << ")" << endl
     << indent << "  Ymin,Ymax: (" << this->InitialBounds[2] << ", "
     << this->InitialBounds[3] << ")" << endl
     << indent << "  Zmin,Zmax: (" << this->InitialBounds[4] << ", "
     << this->InitialBounds[5] << ")" << endl;
}
