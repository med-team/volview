/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWVolumePropertyWidgetPro - a transfer function widget
// .SECTION Description
// This class contains the UI components and methods to edit a 
// ColorTransferFunction in concert with a PiecewiseFunction for opacity.
// New control points can be added by clicking with the left mouse button
// and they can be removed by dragging them out of the window.

#ifndef __vtkKWVolumePropertyWidgetPro_h
#define __vtkKWVolumePropertyWidgetPro_h

#include "vtkKWVolumePropertyWidget.h"

class VTK_EXPORT vtkKWVolumePropertyWidgetPro : public vtkKWVolumePropertyWidget
{
public:
  static vtkKWVolumePropertyWidgetPro* New();
  void PrintSelf(ostream& os, vtkIndent indent);
  vtkTypeRevisionMacro(vtkKWVolumePropertyWidgetPro,vtkKWVolumePropertyWidget);

  // Description:
  // Set commands.
  virtual void InvokeVolumePropertyChangedCommand();

protected:
  vtkKWVolumePropertyWidgetPro() {};
  ~vtkKWVolumePropertyWidgetPro() {};

private:
  vtkKWVolumePropertyWidgetPro(const vtkKWVolumePropertyWidgetPro&); // Not implemented
  void operator=(const vtkKWVolumePropertyWidgetPro&); // Not implemented
};

#endif
