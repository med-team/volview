/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWApplicationSettingsInterfacePro - a user interface panel.
// .SECTION Description
// Extends the user interface defined in vtkKWApplicationSettingsInterfacePro.
// .SECTION See Also

#ifndef __vtkKWApplicationSettingsInterfacePro_h
#define __vtkKWApplicationSettingsInterfacePro_h

#include "vtkKWApplicationSettingsInterface.h"

class vtkKWEntryWithLabel;

class VTK_EXPORT vtkKWApplicationSettingsInterfacePro : public vtkKWApplicationSettingsInterface
{
public:
  static vtkKWApplicationSettingsInterfacePro* New();
  vtkTypeRevisionMacro(vtkKWApplicationSettingsInterfacePro,
                       vtkKWApplicationSettingsInterface);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();
  
  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Callbacks
  virtual void FlickrApplicationKeyCallback(const char*);
  virtual void FlickrSharedSecretCallback(const char*);
  virtual void FlickrAuthenticationTokenCallback(const char*);
  virtual void UseGPURenderingCallback(int state);

protected:
  vtkKWApplicationSettingsInterfacePro();
  ~vtkKWApplicationSettingsInterfacePro();
  
  // Flickr settings

  vtkKWFrameWithLabel  *FlickrSettingsFrame;
  vtkKWEntryWithLabel  *FlickrApplicationKeyEntry;
  vtkKWEntryWithLabel  *FlickrSharedSecretEntry;
  vtkKWEntryWithLabel  *FlickrAuthenticationTokenEntry;

  // Graphics settings

  vtkKWFrameWithLabel  *GraphicsSettingsFrame;
  vtkKWCheckButton     *UseGPURenderingCheckButton;

private:
  vtkKWApplicationSettingsInterfacePro(const vtkKWApplicationSettingsInterfacePro&); // Not implemented
  void operator=(const vtkKWApplicationSettingsInterfacePro&); // Not Implemented
};

#endif

