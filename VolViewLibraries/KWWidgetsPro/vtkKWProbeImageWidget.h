/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWProbeImageWidget - subclass of vtkKWImageWidget
// .SECTION Description
// vtkKWProbeImageWidget is a subclass of vtkKWImageWidget that
// displays data from a vtkProbeImage as an image.
// See parent class : InteractorStyle to find all event associated with it
// .See Also vtkKWInteractorStyleImageView

#ifndef __vtkKWProbeImageWidget_h
#define __vtkKWProbeImageWidget_h

#include "vtkKWImageWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkPolyDataAlgorithm;
class vtkImageReslice;
class vtkKWImageMapToWindowLevelColors;
class vtkTransform;

class VTK_EXPORT vtkKWProbeImageWidget : public vtkKWImageWidget
{
public:
  // Description:
  // Standard New, Type, and PrintSelf methods
  static vtkKWProbeImageWidget* New();
  vtkTypeRevisionMacro(vtkKWProbeImageWidget, vtkKWImageWidget);
  void PrintSelf(ostream &os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Set/get the interaction style.
  // Add more interaction mode to the superclass
  //BTX
  enum
  {
    INTERACTION_MODE_RESLICE = 1000,
    INTERACTION_MODE_ROLL,
    INTERACTION_MODE_TRANSLATE
  };
  //ETX
  virtual void SetInteractionModeToReslice()
    { this->SetInteractionMode(INTERACTION_MODE_RESLICE); };
  virtual void SetInteractionModeToRoll()
    { this->SetInteractionMode(INTERACTION_MODE_ROLL); };
  virtual void SetInteractionModeToTranslate()
    { this->SetInteractionMode(INTERACTION_MODE_TRANSLATE); };

  // Description:
  // Set / get whether this widget is displaying an image
  virtual void SetImageVisibility(int state);
  virtual int GetImageVisibility();
  vtkBooleanMacro(ImageVisibility, int);
  
  // Description:
  // Reset the camera
  virtual void ResetCamera();

  // Description:
  // Render this Widget
  virtual void Render();
  
  // Description:
  // Given mouse coordinate, compute world coordinate, eventually return
  // the id of the renderer if the widget supports multiple renderers
  // (see vtkKWRenderWidget::GetNthRenderer, 
  // vtkKWRenderWidget::GetRendererIndex)
  virtual int ComputeWorldCoordinate(int x, int y, double *result, int *id = 0);

  // Description:
  // Set the probe algorithm that will create input data for this widget.
  virtual void SetProbeInputAlgorithm(vtkPolyDataAlgorithm *alg);
  vtkGetObjectMacro(ProbeInputAlgorithm, vtkPolyDataAlgorithm);

  // Description:
  // Give access to internal structures
  vtkGetObjectMacro(ImageReslice, vtkImageReslice);

  // Description
  // Rotate infinite plane of an angle theta.
  void RollPlane(double theta);
  void TiltPlane(double rxf, double ryf);
  void TranslatePlane(double factor);
  
protected:
  vtkKWProbeImageWidget();
  ~vtkKWProbeImageWidget();

  // Description:
  // Create the widget
  virtual void CreateWidget();
  
  // Description:
  // Connects the internal object together given the Input.
  // Returns 1 on success, 0 on error or to signal that it is safe to abort
  virtual int ConnectInternalPipeline();

  // Description:
  // Update the display extent according to what part of the Input has to
  // be displayed. It is probably the right place to adjust/place the 3D
  // widget which position depends on the display extent.
  virtual void UpdateDisplayExtent();

  // Description:
  // Connect ImageMapToRGBA to its input (can be overriden in subclasses)
  virtual void ConnectImageMapToRGBA();

  // Description:
  // Internally recompute equation to go from the plane of the vtkCutter (vtkImplicitPlaneWidget)
  // to the expected input of vtkImageReslice
  void UpdatePlane();

  // Description:
  // Configure the event map
  virtual void ConfigureEventMap();

  // Description:
  // Populate the context menu
  virtual void PopulateContextMenuWithInteractionEntries(vtkKWMenu*);

  vtkPolyDataAlgorithm *ProbeInputAlgorithm;
  vtkImageReslice *ImageReslice;
  vtkTransform *Transform;
  
private:
  vtkKWProbeImageWidget(const vtkKWProbeImageWidget&);  // Not implemented
  void operator=(const vtkKWProbeImageWidget&);  // Not implemented
};

#endif
