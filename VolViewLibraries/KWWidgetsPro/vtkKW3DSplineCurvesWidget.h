/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKW3DSplineCurvesWidget - widget for measuring distance along a line
// .SECTION Description

#ifndef __vtkKW3DSplineCurvesWidget_h
#define __vtkKW3DSplineCurvesWidget_h

#include "vtkKW3DWidget.h"

#include <vtkstd/vector> // used for keeping track of the SplineCurves

class vtkSplineWidget;

class VTK_EXPORT vtkKW3DSplineCurvesWidget : public vtkKW3DWidget
{
public:
  static vtkKW3DSplineCurvesWidget *New();
  vtkTypeRevisionMacro(vtkKW3DSplineCurvesWidget, vtkKW3DWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  virtual void SetEnabled(int enabling);

  // Description:
  // Add, a Spline Curve
  virtual int AddSplineCurve();
  virtual int AddSplineCurve( double bounds[6] );
  
  // Description:
  // Get the current number of spline curves
  virtual unsigned int GetNumberOfSplineCurves();
 
protected:
  vtkKW3DSplineCurvesWidget();
  ~vtkKW3DSplineCurvesWidget();

  
//BTX
  typedef vtkstd::vector<vtkSplineWidget *> SplineCurvesContainer;
  SplineCurvesContainer SplineCurves;
//ETX

  int DeallocateMarkerResources(unsigned int id);

  // handles the events
  static void ProcessEvents(vtkObject* object,
                            unsigned long event,
                            void* clientdata,
                            void* calldata);
  
private:
  vtkKW3DSplineCurvesWidget(const vtkKW3DSplineCurvesWidget&);  //Not implemented
  void operator=(const vtkKW3DSplineCurvesWidget&);  //Not implemented
};

#endif
