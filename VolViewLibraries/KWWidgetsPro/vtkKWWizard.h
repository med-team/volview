/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWWizard - a superclass for creating wizards 

#ifndef __vtkKWWizard_h
#define __vtkKWWizard_h

#include "vtkKWTopLevel.h"

#include <vtkstd/stack> // used for keeping track of Back Button actions
#include <vtkstd/string> // used for keeping track of Back Button actions

class vtkKWWindow;
class vtkKWPushButton;
class vtkKWLabel;
class vtkKWFrame;

class VTK_EXPORT vtkKWWizard : public vtkKWTopLevel
{
public:
  static vtkKWWizard* New();
  vtkTypeRevisionMacro(vtkKWWizard,vtkKWTopLevel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Invoke the dialog and display it in a modal manner. 
  // This method returns a zero if the dilaog was killed or 
  // canceled, nonzero otherwise.
  virtual int Invoke();

  // Description:
  // Display the dialog in a non-modal manner.
  virtual void Display();

  // Description:
  // Close this Wizard
  virtual void Cancel();
  virtual void OK();

  // Description:
  // Returns 0 if the dialog is active e.g. displayed
  // 1 if it was Canceled 2 if it was OK.
  int GetStatus() {return this->Done;};

  // Description:
  // Return frame to pack into.
  vtkKWWidget* GetFrame() { return this; }

  // Description:
  // Play beep when the dialog is displayed
  vtkSetClampMacro(Beep, int, 0, 1);
  vtkBooleanMacro(Beep, int);
  vtkGetMacro(Beep, int);

  // Description:
  // Sets the beep type
  vtkSetMacro(BeepType, int);
  vtkGetMacro(BeepType, int);

  // Description:
  // This function should eb called when the BackButton is pressed.
  // It will go back one step in the wizrd.
  void BackButtonAction();
  void AddBackButtonCommand(const char *cmd);

  // Description:
  // Call LastStep() this when you are on the last step.
  // LastStep() is a Convenience method that will enable the Finish button, 
  // and make the Next button point to OK().
  // On the other hand, NoNextStep() will disable both buttons.
  void LastStep();
  void NoNextStep();
  
  // Description:
  // Set the pre- and post- text. Set it to "" or NULL to remove the
  // corresponding widget.
  void SetPreText(const char *);
  char* GetPreText();
  void SetPostText(const char *);
  char* GetPostText();

  // Description:
  // The wizard icon
  vtkGetObjectMacro(Icon, vtkKWLabel);

  // Description:
  // Enable/Disable buttons all at once
  void SetEnabledButtons(int arg);

  // Description:
  // Callbacks
  virtual void CloseCallback();

protected:
  vtkKWWizard();
  ~vtkKWWizard();

  // Description:
  // Create the widget
  virtual void CreateWidget();

  // Description:
  // Used by subclasses to clear (forget, unpack) the client area
  // ClearPage() will call ForgetClientArea() and set the pre- and
  // post- text label to "".
  void ForgetClientArea();
  void ClearPage();
  
  int Done;
  int Beep;
  int BeepType;

  vtkKWFrame *ButtonFrame;
  vtkKWFrame *LayoutFrame;
  vtkKWFrame *TitleFrame;
  vtkKWFrame *Sep1;
  vtkKWFrame *Sep2;
  
  vtkKWPushButton *HelpButton;
  vtkKWPushButton *NextButton;
  vtkKWPushButton *BackButton;
  vtkKWPushButton *CancelButton;
  vtkKWPushButton *FinishButton;

  vtkKWFrame *ClientArea;
  vtkKWLabel *TitleLabel;
  vtkKWLabel *SubTitleLabel;
  vtkKWLabel *PreTextLabel;
  vtkKWLabel *PostTextLabel;

  vtkKWLabel *Icon;
  
private:
  vtkKWWizard(const vtkKWWizard&); // Not implemented
  void operator=(const vtkKWWizard&); // Not Implemented

  //BTX
  vtkstd::stack<vtkstd::string> BackButtonCommands;
  //ETX
};


#endif
