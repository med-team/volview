/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWXYPlotDialog.h"

#include "vtkObjectFactory.h"

// Some widgets we use
#include "vtkKWApplication.h"
#include "vtkKWPushButton.h"
#include "vtkXYPlotActor.h"
#include "vtkKWRenderWidget.h"
#include "vtkProperty2D.h"
#include "vtkTextProperty.h"
#include "vtkAxisActor2D.h" 
#include "vtkKWInternationalization.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWXYPlotDialog );
vtkCxxRevisionMacro(vtkKWXYPlotDialog, "$Revision: 1.17 $");

//----------------------------------------------------------------------------
vtkKWXYPlotDialog::vtkKWXYPlotDialog()
{
  this->OKButton = vtkKWPushButton::New();
  this->XYPlotActor = vtkXYPlotActor::New();
  this->RenderWidget = vtkKWRenderWidget::New();
}

//----------------------------------------------------------------------------
vtkKWXYPlotDialog::~vtkKWXYPlotDialog()
{
  if (this->OKButton)
    {
    this->OKButton->Delete();
    this->OKButton = NULL;
    }

  if (this->XYPlotActor)
    {
    this->XYPlotActor->RemoveAllInputs();
    this->XYPlotActor->Delete();
    this->XYPlotActor = NULL;
    }

  if (this->RenderWidget)
    {
    this->RenderWidget->Delete();
    this->RenderWidget = NULL;
    }
}

void vtkKWXYPlotDialog::CreateWidget()
{
  // Check if already created

  if (this->IsCreated())
    {
    vtkErrorMacro("vtkKWXYPlotDialog already created");
    return;
    }

  ostrstream tk_cmd;

  this->SetTitle(ks_("XY Plot Dialog|Title|XY Plot Dialog"));
  this->Superclass::CreateWidget();
  
  this->OKButton->Create();
  this->OKButton->SetWidth(16);
  this->OKButton->SetCommand(this, "OK");
  this->OKButton->SetText(ks_("XY Plot Dialog|Button|OK"));
  this->OKButton->SetParent(this);


  tk_cmd << "pack " << this->OKButton->GetWidgetName() 
         << " -side bottom -padx 4 -expand yes " << vtkstd::endl;

  this->RenderWidget->SetParent(this);
  this->RenderWidget->Create();
  this->RenderWidget->SetConfigurationOptionAsInt("-width", 400);
  this->RenderWidget->SetConfigurationOptionAsInt("-height", 400);
  this->RenderWidget->AddBindings();
  this->RenderWidget->SetRendererBackgroundColor(1.0, 1.0, 1.0);

  tk_cmd << "pack " << this->RenderWidget->GetWidgetName()
         << " -side top -anchor w -padx 6 -pady 6 -fill both" 
         << vtkstd::endl;

  this->XYPlotActor->SetTitle(NULL);
  this->XYPlotActor->GetProperty()->SetColor(0.0, 0.0, 0.0);
  this->XYPlotActor->LegendOff();

  vtkTextProperty *tprop = 
    this->XYPlotActor->GetAxisTitleTextProperty();
  tprop->SetColor(0.0, 0.0, 0.0);
  tprop->ShadowOff();
  tprop->ItalicOff();

  vtkAxisActor2D *axis = this->XYPlotActor->GetXAxisActor2D();
  axis->SetFontFactor(1.7);

  this->XYPlotActor->SetYTitle(NULL);
  axis = this->XYPlotActor->GetYAxisActor2D();
  axis->SetFontFactor(1.7);

  tprop = 
    this->XYPlotActor->GetAxisLabelTextProperty();
  tprop->SetColor(0.0, 0.0, 0.0);
  tprop->ShadowOff();

  vtkCoordinate *coord = 
    this->XYPlotActor->GetPositionCoordinate();
  coord->SetCoordinateSystemToNormalizedViewport();
  coord->SetValue(0.0, 0.0);
  coord = this->XYPlotActor->GetPosition2Coordinate();
  coord->SetCoordinateSystemToNormalizedViewport();
  coord->SetValue(1.0, 1.0);

  this->RenderWidget->AddViewProp(this->XYPlotActor);
  this->RenderWidget->Reset();
  this->RenderWidget->Render();
  this->RenderWidget->AddBindings();

  tk_cmd << ends;
  this->Script(tk_cmd.str());
  tk_cmd.rdbuf()->freeze(0);
}

//----------------------------------------------------------------------------
vtkXYPlotActor * vtkKWXYPlotDialog::GetXYPlotActor()
{
  return this->XYPlotActor;
}

//----------------------------------------------------------------------------
void vtkKWXYPlotDialog::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "RenderWidget: " << this->RenderWidget << endl;
  os << indent << "XYPlotActor:  " << this->XYPlotActor  << endl;
  os << indent << "OKButton:     " << this->OKButton     << endl;
}

