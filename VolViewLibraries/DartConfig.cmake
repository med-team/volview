##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
# Dashboard is opened for submissions for a 24 hour period starting at
# the specified NIGHLY_START_TIME. Time is specified in 24 hour format.
SET (NIGHTLY_START_TIME "21:00:00 EDT")

# Dart server to submit results (used by client)
SET (DROP_METHOD "http")
SET (DROP_SITE "www.kitware.com")
SET (DROP_LOCATION "/Dashboards/Submit.cgi")
SET (TRIGGER_SITE "http://${DROP_SITE}/Dashboards/VolViewTrigger.cgi")

# Project Home Page
SET (PROJECT_URL "http://www.kitware.com/products/volview.html")

# Dart server configuration 
SET (ROLLUP_URL "http://${DROP_SITE}/Dashboards/VolView/Rollup.cgi")
SET (CVS_WEB_URL "https://www.kitware.com/scripts/cvsweb/VolViewLibraries/")
SET (CVS_WEB_CVSROOT "VolView")

SET (USE_GNATS "On")
SET (GNATS_WEB_URL "https://www.kitware.com/Bug/query.php?projects=1&status%5B%5D=1&status%5B%5D=2&status%5B%5D=3&status%5B%5D=4&status%5B%5D=6&op=doquery")
#SET (USE_DOXYGEN "On")
#SET (DOXYGEN_URL "http://golgot/" )


# Continuous email delivery variables
SET (CONTINUOUS_FROM "rick.avila@kitware.com")
SET (SMTP_MAILHOST "public.kitware.com")
SET (CONTINUOUS_MONITOR_LIST "rick.avila@kitware.com karthik.krishnan@kitware.com")
SET (CONTINUOUS_BASE_URL "http://${DROP_SITE}/Dashboards")

SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_TEST_FAILURES ON)
SET (DELIVER_BROKEN_BUILD_EMAIL "Continuous Nightly")
SET (EMAIL_FROM "andy.cedilnik@kitware.com")
SET (DARTBOARD_BASE_URL "http://${DROP_SITE}/Dashboards")

SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_CONFIGURE_FAILURES 1)
SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_BUILD_ERRORS 1)
SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_BUILD_WARNINGS 0)
SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_TEST_NOT_RUNS 0)
SET (DELIVER_BROKEN_BUILD_EMAIL_WITH_TEST_FAILURES 0)
