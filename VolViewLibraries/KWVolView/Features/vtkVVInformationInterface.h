/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVInformationInterface - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVInformationInterface_h
#define __vtkVVInformationInterface_h

#include "vtkVVUserInterfacePanel.h"

class vtkKWFrameWithLabel;
class vtkKWMultiColumnListWithScrollbars;
class vtkVVInformationInterfaceLabels;
class vtkVVDataItemVolume;

class VTK_EXPORT vtkVVInformationInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVInformationInterface* New();
  vtkTypeRevisionMacro(vtkVVInformationInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

protected:
  vtkVVInformationInterface();
  ~vtkVVInformationInterface();

  // The list of infos

  vtkKWMultiColumnListWithScrollbars *InformationList;

  vtkVVInformationInterfaceLabels *Labels;

  void UpdateInformationList(vtkVVDataItemVolume *volume_data);

private:
  vtkVVInformationInterface(const vtkVVInformationInterface&); // Not implemented
  void operator=(const vtkVVInformationInterface&); // Not Implemented
};

#endif

