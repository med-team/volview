/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVAdvancedAlgorithmsInterface - Measurement user interface panel customized for algorithms
// .SECTION Description
// .SECTION See Also

#ifndef __vtkVVAdvancedAlgorithmsInterface_h
#define __vtkVVAdvancedAlgorithmsInterface_h

#include "vtkVVUserInterfacePanel.h"

class vtkKWNotebook;
class vtkKWUserInterfaceManager;
class vtkKWUserInterfaceManagerNotebook;

class VTK_EXPORT vtkVVAdvancedAlgorithmsInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVAdvancedAlgorithmsInterface* New();
  vtkTypeRevisionMacro(vtkVVAdvancedAlgorithmsInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Panel manager. 
  virtual vtkKWNotebook* GetNotebook();
  virtual int HasUserInterfaceManager();
  virtual vtkKWUserInterfaceManager* GetUserInterfaceManager();
  virtual void ShowUserInterface(const char *name);

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

protected:
  vtkVVAdvancedAlgorithmsInterface();
  ~vtkVVAdvancedAlgorithmsInterface();

  // Description:
  // Show a main or secondary user interface panel.
  // The ShowUserInterface() method will
  // query the main UserInterfaceManager (UIM) to check if it is indeed
  // managing the UIP, and show/raise that UIP accordingly.
  virtual void ShowUserInterface(vtkKWUserInterfacePanel *panel);

  virtual int GetPageId();

private:

  vtkKWNotebook   *Notebook;
  vtkKWUserInterfaceManagerNotebook *UserInterfaceManager;
  int PageId;

  vtkVVAdvancedAlgorithmsInterface(const vtkVVAdvancedAlgorithmsInterface&); // Not implemented
  void operator=(const vtkVVAdvancedAlgorithmsInterface&); // Not Implemented
};

#endif

