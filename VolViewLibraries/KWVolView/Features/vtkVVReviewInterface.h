/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVReviewInterface - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVReviewInterface_h
#define __vtkVVReviewInterface_h

#include "vtkVVUserInterfacePanel.h"

class vtkKWFrameWithLabel;
class vtkVVWindowBase;
class vtkKWSimpleAnimationWidget;
class vtkVVSnapshotPresetSelector;

class VTK_EXPORT vtkVVReviewInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVReviewInterface* New();
  vtkTypeRevisionMacro(vtkVVReviewInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the window (do not ref count it since the window will ref count
  // this widget).
  vtkGetObjectMacro(Window, vtkVVWindowBase);
  virtual void SetWindow(vtkVVWindowBase*);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Callbacks.
  virtual void SnapshotPresetApplyCallback(int id);
  virtual int SnapshotPresetAddCallback();
  virtual void SnapshotPresetUpdateCallback(int id);
  virtual int SnapshotPresetRemoveCallback(int id);
  virtual void SnapshotPresetRemovedCallback();
  virtual void SnapshotPresetHasChangedCallback(int id);

  // Description:
  // Get the snapshot preset selector
  vtkGetObjectMacro(SnapshotPresetSelector, vtkVVSnapshotPresetSelector);
  vtkGetObjectMacro(AnimationWidget, vtkKWSimpleAnimationWidget);

protected:
  vtkVVReviewInterface();
  ~vtkVVReviewInterface();

  vtkVVWindowBase         *Window;

  // Snapshot presets

  vtkKWFrameWithLabel          *SnapshotPresetFrame;
  vtkVVSnapshotPresetSelector  *SnapshotPresetSelector;

  // Movie

  vtkKWFrameWithLabel          *AnimationFrame;
  vtkKWSimpleAnimationWidget   *AnimationWidget;

  // Description:
  // Update a snapshot preset thumbnail
  virtual void UpdateSnapshotPresetThumbnail(int id);

  // Description:
  // Populate the snapshots
  virtual void PopulateSnapshotPresets();

  // Description:
  // Update preset counter
  virtual void UpdateSmallPresetCounter();

  // Description:
  // Page ids
  int ReviewPageID;

private:
  vtkVVReviewInterface(const vtkVVReviewInterface&); // Not implemented
  void operator=(const vtkVVReviewInterface&); // Not Implemented
};

#endif

