/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVInformationInterfaceLabels.h"
#include "vtkObjectFactory.h"
#include "vtkKWInternationalization.h"

vtkCxxRevisionMacro(vtkVVInformationInterfaceLabels, "$Revision: 1.9 $");
vtkStandardNewMacro(vtkVVInformationInterfaceLabels);

vtkVVInformationInterfaceLabels::vtkVVInformationInterfaceLabels()
{
  this->InstitutionNameLabel        = NULL;
  this->PatientIDLabel              = NULL;
  this->PatientNameLabel            = NULL;
  this->PatientAgeLabel             = NULL;
  this->PatientSexLabel             = NULL;
  this->PatientBirthDateLabel       = NULL;
  this->AcquisitionDateLabel        = NULL;
  this->AcquisitionTimeLabel        = NULL;
  this->ModalityLabel               = NULL;
  this->ModelNameLabel              = NULL;
  this->StationNameLabel            = NULL;
  this->StudyDescriptionLabel       = NULL;
  this->StudyIdLabel                = NULL;
  this->SeriesNumberLabel           = NULL;
  this->ImagesLabel                 = NULL;
  this->EchoTimeLabel               = NULL;
  this->EchoTrainLengthLabel        = NULL;
  this->RepetitionTimeLabel         = NULL;
  this->SeriesDescriptionLabel      = NULL;
  this->KVPLabel                    = NULL;
  this->MALabel                     = NULL;
  this->ExposureTimeLabel           = NULL;
  this->XrayTubeCurrentLabel        = NULL;
  this->ExposureLabel               = NULL;
  this->GantryTiltLabel             = NULL;
  this->SliceThicknessLabel         = NULL;
  this->ConvolutionKernelLabel      = NULL;
  this->DistanceUnitsLabel          = NULL;
  this->VoxelDimensionsLabel        = NULL;
  this->PhysicalDimensionsLabel     = NULL;
  this->PhysicalOriginLabel         = NULL;
  this->VoxelSpacingLabel           = NULL;
  this->ScalarUnitsLabel            = NULL;
  this->ScalarRangeLabel            = NULL;
  this->ScalarTypeLabel             = NULL;
  this->ScalarSizeLabel             = NULL;
  this->FilenameLabel               = NULL;
  this->DirectoryLabel              = NULL;
  this->DirectionCosineLabel        = NULL;
  this->DataScopeLabel              = NULL;
  this->UnknownValueLabel           = NULL;
  this->TableHeightLabel            = NULL;

  this->SetInstitutionNameLabel        ( k_("Institution") );
  this->SetPatientNameLabel            ( k_("Patient Name") );
  this->SetPatientAgeLabel             ( k_("Patient Age") );
  this->SetPatientSexLabel             ( k_("Patient Sex") );
  this->SetPatientBirthDateLabel       ( k_("Patient Birth Date") );
  this->SetAcquisitionDateLabel        ( k_("Acquisition Date") );
  this->SetAcquisitionTimeLabel        ( k_("Acquisition Time") );
  this->SetModalityLabel               ( k_("Modality") );
  this->SetModelNameLabel              ( k_("Model Name") );
  this->SetStationNameLabel            ( k_("Station Name") );
  this->SetStudyDescriptionLabel       ( k_("Study Description") );
  this->SetStudyIdLabel                ( k_("Exam") );
  this->SetSeriesNumberLabel           ( k_("Series") );
  this->SetImagesLabel                 ( k_("Images") );
  this->SetKVPLabel                    ( "kVp" );
  this->SetMALabel                     ( "mA" );
  this->SetTableHeightLabel            ( "Table Height (mm)" );
  this->SetExposureTimeLabel           ( k_("Exposure Time") );
  this->SetXrayTubeCurrentLabel        ( k_("X-Ray Tube Current") );
  this->SetExposureLabel               ( k_("Exposure") );
  this->SetGantryTiltLabel             ( k_("Gantry/Detector Tilt") );
  this->SetSliceThicknessLabel         ( k_("Slice Thickness") );
  this->SetConvolutionKernelLabel      ( k_("Convolution Kernel") );
  this->SetDistanceUnitsLabel          ( k_("Distance Units") );
  this->SetVoxelDimensionsLabel        ( k_("Voxel Dimensions") );
  this->SetPhysicalDimensionsLabel     ( k_("Physical Dimensions") ); 
  this->SetPhysicalOriginLabel         ( k_("Physical Origin") ); 
  this->SetVoxelSpacingLabel           ( k_("Voxel Spacing") );
  this->SetScalarUnitsLabel            ( k_("Scalar Units") );
  this->SetScalarRangeLabel            ( k_("Scalar Range") ); 
  this->SetScalarTypeLabel             ( k_("Scalar Type") ); 
  this->SetScalarSizeLabel             ( k_("Scalar Size") ); 
  this->SetFilenameLabel               ( k_("File Name") );
  this->SetDirectoryLabel              ( k_("Directory") );
  this->SetDirectionCosineLabel        ( k_("Direction Cosine") );
  this->SetEchoTimeLabel               ( k_("Echo Time") );
  this->SetEchoTrainLengthLabel        ( k_("Echo Train Length") );
  this->SetRepetitionTimeLabel         ( k_("Repetition Time") );
  this->SetSeriesDescriptionLabel      ( k_("Series Description") );
  this->SetPatientIDLabel              ( k_("Patient ID") );
  this->SetDataScopeLabel              ( k_("Scope") );
  this->SetUnknownValueLabel           ( "" );
}

vtkVVInformationInterfaceLabels::~vtkVVInformationInterfaceLabels()
{
  this->SetInstitutionNameLabel        (NULL);
  this->SetPatientIDLabel              (NULL);
  this->SetPatientNameLabel            (NULL);
  this->SetPatientAgeLabel             (NULL);
  this->SetPatientSexLabel             (NULL);
  this->SetPatientBirthDateLabel       (NULL);
  this->SetAcquisitionDateLabel        (NULL);
  this->SetAcquisitionTimeLabel        (NULL);
  this->SetModalityLabel               (NULL);
  this->SetModelNameLabel              (NULL);
  this->SetStationNameLabel            (NULL);
  this->SetStudyDescriptionLabel       (NULL);
  this->SetStudyIdLabel                (NULL);
  this->SetSeriesNumberLabel           (NULL);
  this->SetImagesLabel                 (NULL);
  this->SetEchoTimeLabel               (NULL);
  this->SetEchoTrainLengthLabel        (NULL);
  this->SetRepetitionTimeLabel         (NULL);
  this->SetSeriesDescriptionLabel      (NULL);
  this->SetKVPLabel                    (NULL);
  this->SetMALabel                     (NULL);
  this->SetExposureTimeLabel           (NULL);
  this->SetXrayTubeCurrentLabel        (NULL);
  this->SetExposureLabel               (NULL);
  this->SetGantryTiltLabel             (NULL);
  this->SetSliceThicknessLabel         (NULL);
  this->SetConvolutionKernelLabel      (NULL);
  this->SetDistanceUnitsLabel          (NULL);
  this->SetVoxelDimensionsLabel        (NULL);
  this->SetPhysicalDimensionsLabel     (NULL);
  this->SetPhysicalOriginLabel         (NULL);
  this->SetVoxelSpacingLabel           (NULL);
  this->SetScalarUnitsLabel            (NULL);
  this->SetScalarRangeLabel            (NULL);
  this->SetScalarTypeLabel             (NULL);
  this->SetScalarSizeLabel             (NULL);
  this->SetFilenameLabel               (NULL);
  this->SetDirectoryLabel              (NULL);
  this->SetDirectionCosineLabel        (NULL);
  this->SetDataScopeLabel              (NULL);
  this->SetUnknownValueLabel           (NULL);
  this->SetTableHeightLabel            (NULL);
}

//----------------------------------------------------------------------------
void vtkVVInformationInterfaceLabels::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  
  os << indent << "InstitutionNameLabel     " << this->GetInstitutionNameLabel        () << endl;
  os << indent << "PatientIDLabel           " << this->GetPatientIDLabel              () << endl;
  os << indent << "PatientNameLabel         " << this->GetPatientNameLabel            () << endl;
  os << indent << "PatientAgeLabel          " << this->GetPatientAgeLabel             () << endl;
  os << indent << "PatientSexLabel          " << this->GetPatientSexLabel             () << endl;
  os << indent << "PatientBirthDateLabel    " << this->GetPatientBirthDateLabel       () << endl;
  os << indent << "AcquisitionDateLabel     " << this->GetAcquisitionDateLabel        () << endl;
  os << indent << "AcquisitionTimeLabel     " << this->GetAcquisitionTimeLabel        () << endl;
  os << indent << "ModalityLabel            " << this->GetModalityLabel               () << endl;
  os << indent << "ModelNameLabel           " << this->GetModelNameLabel              () << endl;
  os << indent << "StationNameLabel         " << this->GetStationNameLabel            () << endl;
  os << indent << "StudyDescriptionLabel    " << this->GetStudyDescriptionLabel       () << endl;
  os << indent << "StudyIdLabel             " << this->GetStudyIdLabel                () << endl;
  os << indent << "SeriesNumberLabel        " << this->GetSeriesNumberLabel           () << endl;
  os << indent << "ImagesLabel              " << this->GetImagesLabel                 () << endl;
  os << indent << "EchoTime              " << this->GetEchoTimeLabel               () << endl;
  os << indent << "EchoTrainLength       " << this->GetEchoTrainLengthLabel        () << endl;
  os << indent << "RepetitionTime        " << this->GetRepetitionTimeLabel         () << endl;
  os << indent << "SeriesDescription     " << this->GetSeriesDescriptionLabel      () << endl;
  os << indent << "KVPLabel                 " << this->GetKVPLabel                    () << endl;
  os << indent << "MALabel                  " << this->GetMALabel                     () << endl;
  os << indent << "ExposureTimeLabel        " << this->GetExposureTimeLabel           () << endl;
  os << indent << "XrayTubeCurrentLabel     " << this->GetXrayTubeCurrentLabel        () << endl;
  os << indent << "ExposureLabel            " << this->GetExposureLabel               () << endl;
  os << indent << "GantryTiltLabel          " << this->GetGantryTiltLabel             () << endl;
  os << indent << "SliceThicknessLabel      " << this->GetSliceThicknessLabel         () << endl;
  os << indent << "ConvolutionKernelLabel   " << this->GetConvolutionKernelLabel      () << endl;
  os << indent << "DistanceUnitsLabel       " << this->GetDistanceUnitsLabel          () << endl;
  os << indent << "VoxelDimensionsLabel     " << this->GetVoxelDimensionsLabel        () << endl;
  os << indent << "PhysicalDimensionsLabel  " << this->GetPhysicalDimensionsLabel     () << endl;
  os << indent << "PhysicalOriginLabel  " << this->GetPhysicalOriginLabel     () << endl;
  os << indent << "VoxelSpacingLabel        " << this->GetVoxelSpacingLabel           () << endl;
  os << indent << "ScalarUnitsLabel         " << this->GetScalarUnitsLabel            () << endl;
  os << indent << "ScalarRangeLabel         " << this->GetScalarRangeLabel            () << endl;
  os << indent << "ScalarTypeLabel         " << this->GetScalarTypeLabel            () << endl;
  os << indent << "ScalarSizeLabel         " << this->GetScalarSizeLabel            () << endl;
  os << indent << "FilenameLabel            " << this->GetFilenameLabel               () << endl;
  os << indent << "DirectoryLabel           " << this->GetDirectoryLabel              () << endl;
  os << indent << "DirectionCosineLabel           " << this->GetDirectionCosineLabel              () << endl;
  os << indent << "DataScopeLabel     " << this->GetDataScopeLabel        () << endl;
  os << indent << "TableHeightLabel               " << this->GetTableHeightLabel                  () << endl;
  os << indent << "UnknownValueLabel               " << this->GetUnknownValueLabel                  () << endl;
}
