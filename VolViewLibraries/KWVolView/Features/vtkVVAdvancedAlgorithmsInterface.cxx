/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVAdvancedAlgorithmsInterface.h"

#include "vtkObjectFactory.h"

#include "vtkKWMessageDialog.h"
#include "vtkKWNotebook.h"
#include "vtkKWUserInterfaceManagerNotebook.h"
#include "vtkKWIcon.h"

#include "vtkVVWindowBase.h"

#include <vtksys/ios/sstream> 

//---------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVAdvancedAlgorithmsInterface);
vtkCxxRevisionMacro(vtkVVAdvancedAlgorithmsInterface, "$Revision: 1.3 $");

//---------------------------------------------------------------------------
vtkVVAdvancedAlgorithmsInterface::vtkVVAdvancedAlgorithmsInterface()
{
  this->SetName("Advanced Algorithms");

  this->Notebook = NULL;
  this->UserInterfaceManager = NULL;
  this->PageId = -1;
}

//---------------------------------------------------------------------------
vtkVVAdvancedAlgorithmsInterface::~vtkVVAdvancedAlgorithmsInterface()
{
  if (this->Notebook)
    {
    this->Notebook->Delete();
    this->Notebook = NULL;
    }

  if (this->UserInterfaceManager)
    {
    this->UserInterfaceManager->Delete();
    this->UserInterfaceManager = NULL;
    }
}

// --------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::Create()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("The panel is already created.");
    return;
    }

  // Create the superclass instance (and set the application)

  this->Superclass::Create();
 
}

//----------------------------------------------------------------------------
int vtkVVAdvancedAlgorithmsInterface::GetPageId()
{
  // Add page

  if (this->PageId < 0)
    {
    this->PageId = this->AddPage(NULL, this->GetName());
    this->SetPageIconToPredefinedIcon(
      this->PageId, vtkKWIcon::IconNuvola22x22ActionsWizard);
    }

  return this->PageId;
}

//----------------------------------------------------------------------------
vtkKWNotebook* vtkVVAdvancedAlgorithmsInterface::GetNotebook()
{
  if (!this->Notebook)
    {
    this->Notebook = vtkKWNotebook::New();
    this->Notebook->PagesCanBePinnedOn();
    this->Notebook->EnablePageTabContextMenuOn();
    this->Notebook->AlwaysShowTabsOn();
    }

  if (!this->Notebook->IsCreated() && this->IsCreated())
    {
    this->Notebook->SetParent(this->GetPagesParentWidget());
    this->Notebook->Create();
    this->Script(
      "pack %s -pady 2 -padx 2 -fill both -expand yes -anchor n -in %s",
      this->Notebook->GetWidgetName(),
      this->GetPageWidget(this->GetPageId())->GetWidgetName());
    }

  return this->Notebook;
}

//----------------------------------------------------------------------------
int vtkVVAdvancedAlgorithmsInterface::HasUserInterfaceManager()
{
  return this->UserInterfaceManager ? 1 : 0;
}

//----------------------------------------------------------------------------
vtkKWUserInterfaceManager* 
vtkVVAdvancedAlgorithmsInterface::GetUserInterfaceManager()
{
  if (!this->UserInterfaceManager)
    {
    this->UserInterfaceManager = vtkKWUserInterfaceManagerNotebook::New();
    this->UserInterfaceManager->SetNotebook(this->GetNotebook());
    this->UserInterfaceManager->EnableDragAndDropOn();
    this->GetNotebook()->ShowIconsOn();
    }

  if (!this->UserInterfaceManager->IsCreated() && this->IsCreated())
    {
    this->UserInterfaceManager->Create();
    }
                        
  return this->UserInterfaceManager;
}

//----------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::ShowUserInterface(const char *name)
{
  if (this->GetUserInterfaceManager())
    {
    this->ShowUserInterface(
      this->GetUserInterfaceManager()->GetPanel(name));
    }
}

//----------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::ShowUserInterface(
  vtkKWUserInterfacePanel *panel)
{
  if (!panel)
    {
    return;
    }

  vtkKWUserInterfaceManager *uim = this->GetUserInterfaceManager();
  if (!uim || !uim->HasPanel(panel))
    {
    vtkErrorMacro(
      "Sorry, the user interface panel you are trying to display ("
      << panel->GetName() << ") is not managed by the  "
      "User Interface Manager");
    return;
    }

  if (!panel->Raise())
    {
    vtksys_ios::ostringstream msg;
    msg << "The panel you are trying to access could not be displayed "
        << "properly. Please make sure there is enough room in the notebook "
        << "to bring up this part of the interface.";
    if (this->Notebook && 
        this->Notebook->GetShowOnlyMostRecentPages() &&
        this->Notebook->GetPagesCanBePinned())
      {
      msg << " This may happen if you displayed " 
          << this->Notebook->GetNumberOfMostRecentPages() 
          << " notebook pages "
          << "at the same time and pinned/locked all of them. In that case, "
          << "try to hide or unlock a notebook page first.";
      }
    vtkKWMessageDialog::PopupMessage( 
      this->GetApplication(), this->GetWindow(), 
      "User Interface Warning", msg.str().c_str(),
      vtkKWMessageDialog::WarningIcon);
    }
}

//---------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::Update()
{
  this->Superclass::Update();
}

//---------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

  if (this->Notebook)
    {
    this->Notebook->SetEnabled(this->GetEnabled());
    }

  // Update all the user interface panels

  if (this->HasUserInterfaceManager())
    {
    this->GetUserInterfaceManager()->SetEnabled(this->GetEnabled());
    // As a side effect, SetEnabled() call an Update() on the panel, 
    // which will call an UpdateEnableState() too,
    // this->GetUserInterfaceManager()->UpdateEnableState();
    // this->GetUserInterfaceManager()->Update();
    }

}

//---------------------------------------------------------------------------
void vtkVVAdvancedAlgorithmsInterface::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "Notebook: " << this->GetNotebook() << endl;
}

