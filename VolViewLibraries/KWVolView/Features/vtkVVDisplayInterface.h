/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDisplayInterface - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVDisplayInterface_h
#define __vtkVVDisplayInterface_h

#include "vtkVVUserInterfacePanel.h"

//---------------------------------------------------------------------------

class vtkKWChangeColorButton;
class vtkKWCheckButton;
class vtkKWCheckButtonWithChangeColorButton;
class vtkKWEntryWithLabel;
class vtkKWERepresentativeVolumeImageCreator;
class vtkKWFrame;
class vtkKWFrameWithLabel;
class vtkKWImageMapToWindowLevelColors;
class vtkKWLabelWithLabelSet;
class vtkKWMenuButtonWithSpinButtons;
class vtkKWMenuButtonWithSpinButtonsWithLabel;
class vtkKWPiecewiseFunctionEditor;
class vtkKWPushButton;
class vtkKWPushButtonSet;
class vtkKWRadioButtonSetWithLabel;
class vtkKWScaleWithEntry;
class vtkKWVolumePropertyPresetSelector;
class vtkKWVolumePropertyWidget;
class vtkKWWindowLevelPresetSelector;
class vtkVVDisplayInterfaceInternals;
class vtkVolumeProperty;

class VTK_EXPORT vtkVVDisplayInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVDisplayInterface* New();
  vtkTypeRevisionMacro(vtkVVDisplayInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Get the id of the 2D and 3D page in this user interface
  virtual int GetPage2DId();
  virtual int GetPage3DId();

  // Description:
  // Callbacks used when interaction has been performed.
  virtual void WindowLevelCallback(const char *);
  virtual void WindowLevelResetCallback();
  virtual void BlendModeCallback(int mode);

  // Description:
  // Callbacks used when interaction has been performed.
  virtual void WindowLevelPresetApplyCallback(int id);
  virtual int WindowLevelPresetAddCallback();
  virtual void WindowLevelPresetUpdateCallback(int id);
  virtual void WindowLevelPresetHasChangedCallback(int id);
  virtual int WindowLevelPresetRemoveCallback(int id);
  virtual int WindowLevelPresetLoadCallback();
  virtual void VolumePropertyPresetApplyCallback(int id);
  virtual int VolumePropertyPresetAddCallback();
  virtual void VolumePropertyPresetUpdateCallback(int id);
  virtual void VolumePropertyPresetHasChangedCallback(int id);
  virtual int VolumePropertyPresetLoadCallback();
  virtual void UpdateVolumePropertyPresetThumbnailsCallback();
  virtual void UpdateWindowLevelPresetThumbnailsCallback();
  virtual void WindowLevelPresetFilteringHasChangedCallback();
  virtual void VolumePropertyPresetFilteringHasChangedCallback();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Get some internal objects
  vtkGetObjectMacro(WindowLevelPresetSelector, vtkKWWindowLevelPresetSelector);
  vtkGetObjectMacro(VolumePropertyPresetSelector, vtkKWVolumePropertyPresetSelector);
  vtkGetObjectMacro(WindowLevelFunctionEditor, vtkKWPiecewiseFunctionEditor);

protected:
  vtkVVDisplayInterface();
  ~vtkVVDisplayInterface();

  // Display: Window/Level Settings

  vtkKWFrameWithLabel                     *WindowLevelSettingsFrame;
  vtkKWFrame                              *WindowLevelEntriesFrame;
  vtkKWEntryWithLabel                     *WindowEntry;
  vtkKWEntryWithLabel                     *LevelEntry;
  vtkKWPushButton                         *WindowLevelResetButton;
  vtkKWPiecewiseFunctionEditor            *WindowLevelFunctionEditor;

  // Display: Volume Appearance Settings

  vtkKWVolumePropertyWidget               *VolumePropertyWidget;
  vtkKWMenuButtonWithSpinButtons          *BlendModeMenuButton;

  // Presets: Window/Level Presets

  vtkKWFrameWithLabel                     *WindowLevelPresetFrame;
  vtkKWWindowLevelPresetSelector          *WindowLevelPresetSelector;

  // Presets: Volume Appearance Presets

  vtkKWFrameWithLabel                     *VolumePropertyPresetFrame;
  vtkKWVolumePropertyPresetSelector       *VolumePropertyPresetSelector;

  // Description:
  // Rebuild the presets
  virtual void PopulateWindowLevelPresets();

  // Description:
  // Set the window/level in the UI programmatically.
  // Provided because we might want to update the window/level
  // faster than by calling Update().
  virtual void SetWindowLevelInInterface(double w, double l);
  virtual void SetInteractiveWindowLevelInInterface(double w, double l);

  // Description:
  // Set the window/level in all the render widgets that are using the
  // currently selected data item.
  virtual void SetWindowLevelInRenderWidgetsUsingSelectedDataItem(
    double w, double l);
  virtual void SetInteractiveWindowLevelInRenderWidgetsUsingSelectedDataItem(
    double w, double l);

  // Description:
  // Update a window/level preset thumbnail using the selected
  // widget, or all the thumbnails in a given group.
  virtual void UpdateWindowLevelPresetThumbnail(int id);
  virtual void UpdateWindowLevelPresetThumbnails();
  
  // Description:
  // Save/Load window level presets
  virtual void LoadWindowLevelPresets();
  virtual int LoadWindowLevelPreset(const char*);
  virtual void SaveWindowLevelPreset(int id);

  // Description:
  // Update a volume appearance preset thumbnail using the selected
  // widget, or all the thumbnails in a given group.
  virtual void UpdateVolumePropertyPresetThumbnail(int id);
  virtual void UpdateVolumePropertyPresetThumbnails();
  virtual void ScheduleUpdateVolumePropertyPresetThumbnail(int id);
  virtual void ScheduleUpdateVolumePropertyPresetThumbnails();

  // Description:
  // Save/Load a volume appearance preset
  virtual void LoadVolumePropertyPresets();
  virtual int LoadVolumePropertyPreset(const char *);
  virtual void SaveVolumePropertyPreset(int id);
  virtual void ScheduleUpdateWindowLevelPresetThumbnails(int delay);

  // Description:
  // Pack
  virtual void Pack();

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // Description:
  // Helper class to create thumbnails
  vtkKWERepresentativeVolumeImageCreator *ThumbnailCreator;
  int WaitingOnThumbnailCreator;
  int ThumbnailCreatorID;
  
  // Description:
  // PIMPL Encapsulation for STL containers
  vtkVVDisplayInterfaceInternals *Internals;

private:
  vtkVVDisplayInterface(const vtkVVDisplayInterface&); // Not implemented
  void operator=(const vtkVVDisplayInterface&); // Not Implemented
};

#endif

