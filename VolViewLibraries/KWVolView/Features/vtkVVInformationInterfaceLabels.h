/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// Details for the vtkVVInformationInterface and the 
// vtkVVDetailedInformationInterface classes
// 

#ifndef __vtkVVInformationInterfaceLabels_h
#define __vtkVVInformationInterfaceLabels_h

#include "vtkObject.h"

class VTK_EXPORT vtkVVInformationInterfaceLabels : public vtkObject
{
public:

  static vtkVVInformationInterfaceLabels* New();
  vtkTypeRevisionMacro(vtkVVInformationInterfaceLabels,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  vtkGetStringMacro(   AcquisitionDateLabel           );
  vtkGetStringMacro(   AcquisitionTimeLabel           );
  vtkGetStringMacro(   ConvolutionKernelLabel         );
  vtkGetStringMacro(   EchoTimeLabel                  );
  vtkGetStringMacro(   EchoTrainLengthLabel           );
  vtkGetStringMacro(   ExposureLabel                  );
  vtkGetStringMacro(   ExposureTimeLabel              );
  vtkGetStringMacro(   GantryTiltLabel                );
  vtkGetStringMacro(   InstitutionNameLabel           );
  vtkGetStringMacro(   KVPLabel                       );
  vtkGetStringMacro(   ModalityLabel                  );
  vtkGetStringMacro(   ModelNameLabel                 ); // not in vtkMedicalImageProperties
  vtkGetStringMacro(   PatientAgeLabel                );
  vtkGetStringMacro(   PatientBirthDateLabel          );
  vtkGetStringMacro(   PatientIDLabel                 );
  vtkGetStringMacro(   PatientNameLabel               );
  vtkGetStringMacro(   PatientSexLabel                );
  vtkGetStringMacro(   RepetitionTimeLabel            );
  vtkGetStringMacro(   SeriesDescriptionLabel         );
  vtkGetStringMacro(   SeriesNumberLabel              );
  vtkGetStringMacro(   SliceThicknessLabel            );
  vtkGetStringMacro(   StationNameLabel               );
  vtkGetStringMacro(   StudyDescriptionLabel          );
  vtkGetStringMacro(   StudyIdLabel                   );
  vtkGetStringMacro(   XrayTubeCurrentLabel           );
  vtkGetStringMacro(   TableHeightLabel               );

  vtkGetStringMacro(   ImagesLabel                    ); // not in vtkMedicalImageProperties
  vtkGetStringMacro(   MALabel                        );
  vtkGetStringMacro(   DistanceUnitsLabel             );
  vtkGetStringMacro(   VoxelDimensionsLabel           );
  vtkGetStringMacro(   PhysicalDimensionsLabel        );
  vtkGetStringMacro(   PhysicalOriginLabel            );
  vtkGetStringMacro(   VoxelSpacingLabel              );
  vtkGetStringMacro(   ScalarUnitsLabel               );
  vtkGetStringMacro(   ScalarRangeLabel               );
  vtkGetStringMacro(   ScalarTypeLabel                );
  vtkGetStringMacro(   ScalarSizeLabel                );
  vtkGetStringMacro(   FilenameLabel                  );
  vtkGetStringMacro(   DirectoryLabel                 );
  vtkGetStringMacro(   DirectionCosineLabel           );
  vtkGetStringMacro(   DataScopeLabel                 );
  vtkGetStringMacro(   UnknownValueLabel              );

  vtkSetStringMacro(   InstitutionNameLabel           );
  vtkSetStringMacro(   PatientIDLabel                 );
  vtkSetStringMacro(   PatientNameLabel               );
  vtkSetStringMacro(   PatientAgeLabel                );
  vtkSetStringMacro(   PatientSexLabel                );
  vtkSetStringMacro(   PatientBirthDateLabel          );
  vtkSetStringMacro(   AcquisitionDateLabel           );
  vtkSetStringMacro(   AcquisitionTimeLabel           );
  vtkSetStringMacro(   ModalityLabel                  );
  vtkSetStringMacro(   ModelNameLabel                 );
  vtkSetStringMacro(   StationNameLabel               );
  vtkSetStringMacro(   StudyDescriptionLabel          );
  vtkSetStringMacro(   StudyIdLabel                   );
  vtkSetStringMacro(   SeriesNumberLabel              );
  vtkSetStringMacro(   ImagesLabel                    );
  vtkSetStringMacro(   EchoTimeLabel                  );
  vtkSetStringMacro(   EchoTrainLengthLabel           );
  vtkSetStringMacro(   RepetitionTimeLabel            );
  vtkSetStringMacro(   SeriesDescriptionLabel         );
  vtkSetStringMacro(   KVPLabel                       );
  vtkSetStringMacro(   MALabel                        );
  vtkSetStringMacro(   ExposureTimeLabel              );
  vtkSetStringMacro(   XrayTubeCurrentLabel           );
  vtkSetStringMacro(   ExposureLabel                  );
  vtkSetStringMacro(   GantryTiltLabel                );
  vtkSetStringMacro(   SliceThicknessLabel            );
  vtkSetStringMacro(   ConvolutionKernelLabel         );
  vtkSetStringMacro(   DistanceUnitsLabel             );
  vtkSetStringMacro(   VoxelDimensionsLabel           );
  vtkSetStringMacro(   PhysicalDimensionsLabel        );
  vtkSetStringMacro(   PhysicalOriginLabel            );
  vtkSetStringMacro(   VoxelSpacingLabel              );
  vtkSetStringMacro(   ScalarUnitsLabel               );
  vtkSetStringMacro(   ScalarRangeLabel               );
  vtkSetStringMacro(   ScalarTypeLabel                );
  vtkSetStringMacro(   ScalarSizeLabel                );
  vtkSetStringMacro(   FilenameLabel                  );
  vtkSetStringMacro(   DirectoryLabel                 );
  vtkSetStringMacro(   DirectionCosineLabel           );
  vtkSetStringMacro(   DataScopeLabel                 );
  vtkSetStringMacro(   TableHeightLabel               );
  vtkSetStringMacro(   UnknownValueLabel              );

protected:
  vtkVVInformationInterfaceLabels();
  ~vtkVVInformationInterfaceLabels();

private:
  char*  InstitutionNameLabel        ;
  char*  PatientIDLabel              ;
  char*  PatientNameLabel            ;
  char*  PatientAgeLabel             ;
  char*  PatientSexLabel             ;
  char*  PatientBirthDateLabel       ;
  char*  AcquisitionDateLabel        ;
  char*  AcquisitionTimeLabel        ;
  char*  ModalityLabel               ;
  char*  ModelNameLabel              ;
  char*  ModalityModelNameLabel      ;
  char*  StationNameLabel            ;
  char*  StudyDescriptionLabel       ;
  char*  StudyIdLabel                ;
  char*  SeriesNumberLabel           ;
  char*  ImagesLabel                 ;
  char*  EchoTimeLabel               ;
  char*  EchoTrainLengthLabel        ;
  char*  RepetitionTimeLabel         ;
  char*  SeriesDescriptionLabel      ;
  char*  KVPLabel                    ;
  char*  MALabel                     ;
  char*  ExposureTimeLabel           ;
  char*  XrayTubeCurrentLabel        ;
  char*  ExposureLabel               ;
  char*  GantryTiltLabel             ;
  char*  SliceThicknessLabel         ;
  char*  ConvolutionKernelLabel      ;
  char*  DistanceUnitsLabel          ;
  char*  VoxelDimensionsLabel        ;
  char*  PhysicalDimensionsLabel     ;
  char*  PhysicalOriginLabel         ;
  char*  VoxelSpacingLabel           ;
  char*  ScalarUnitsLabel            ;
  char*  ScalarRangeLabel            ;
  char*  ScalarTypeLabel             ;
  char*  ScalarSizeLabel             ;
  char*  FilenameLabel               ;
  char*  DirectoryLabel              ;
  char*  DirectionCosineLabel        ;
  char*  DataScopeLabel              ;
  char*  UnknownValueLabel           ;
  char*  TableHeightLabel            ;

};

#endif //  __vtkVVInformationInterfaceLabels_h
