/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDicomTagPrinter - Prints relevant DICOM tags and presents them for DART 
// .SECTION Description
// Print out relevant DICOM tags. Internally uses GDCM
//
// .SECTION Parameters and Usage
// Set the DICOM filename. 
// PrintTags() should print out the tags in a manner suitable for DART to cout.

#ifndef __vtkVVDicomTagPrinter_h
#define __vtkVVDicomTagPrinter_h

#include "vtkObject.h"
#include "gdcmFile.h"

class VTK_EXPORT vtkVVDicomTagPrinter : public vtkObject
{
public:
  static vtkVVDicomTagPrinter* New();
  vtkTypeRevisionMacro(vtkVVDicomTagPrinter,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the DICOM file name.
  vtkSetStringMacro(FileName);

  // Description:
  // Print relevant DICOM tags for Dart.
  virtual void PrintTags();

  // Description:
  // Check if the input file is a DICOM file
  // return value of 
  // 1 : DICOM 
  // 0: otherwise
  virtual int IsFileDicom();

protected:
  vtkVVDicomTagPrinter();
  ~vtkVVDicomTagPrinter();

  char* FileName;
  
private:
  vtkVVDicomTagPrinter(const vtkVVDicomTagPrinter&); // Not implemented
  void operator=(const vtkVVDicomTagPrinter&); // Not implemented

  void PrintPixelType();
  void PrintImageDimensions();
  void PrintPatientOrientation();
  void PrintRescaleSI();

//BTX
  gdcm::File *header;
//ETX
};

#endif

