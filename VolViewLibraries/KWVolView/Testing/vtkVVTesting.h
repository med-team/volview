/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVTesting - wrapper around vtkTesting class to suit VV to diff with screenshots 
// .SECTION Description
// Compare render windows or image data with a baseline image
//
// .SECTION Parameters and Usage
//   The class uses vtkTesting to compare RenderWindows or ImageData with a 
// baseline image file.
//   The actual difference comparison is done by the vtkTesting class. 
// Appropriate arguments need to be passed over to the vtkTesting class, such 
// as the filename etc..
//   There are two ways to use this class. In a Tcl script, you would set the
// render window or you could set the imagedata.
//
// \code
// vtkVVTesting testing
// testing SetRenderView $view
// for {set i 1} {$i < $argc} {incr i} {
//   testing AddArgument "[lindex $argv $i]"
// }
// set res [ testing RegressionTest ${threshold} ]
// testing Delete
// \endcode
//
// or replace the second line in the code with
// \code
// testing SetImageData $image
// \endcode
// 
// The additional arguments here should contain args as used by vtkTesting:
// -T Temp_dir -V BaselineFileName

#ifndef __vtkVVTesting_h
#define __vtkVVTesting_h

#include "vtkObject.h"

class vtkTesting;
class vtkKWRenderWidget;
class vtkImageAppend;
class vtkImageData;

class VTK_EXPORT vtkVVTesting : public vtkObject
{
public:
  static vtkVVTesting* New();
  vtkTypeRevisionMacro(vtkVVTesting,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set get the render view
  virtual void SetRenderView(vtkKWRenderWidget* view);
  vtkGetObjectMacro(RenderView, vtkKWRenderWidget);

  // Description:
  // Set get the render view
  virtual void SetImageData(vtkImageData*);
  vtkGetObjectMacro(ImageData, vtkImageData);

  // Description:
  // Set the comparison image file name.
  vtkSetStringMacro(ComparisonImage);

  // Description:
  // Add argument
  virtual void AddArgument(const char* arg);

  // Description:
  // Perform the actual test.
  virtual int RegressionTest(float thresh);

  // Description: 
  // Append a test image. Thsi is useful for combining multiple
  // tests into one test with a large image. Thsi method will append test
  // images together (left to right) in order to make a large valid image
  virtual void AppendTestImage(vtkKWRenderWidget *RenderView);

  // Description: 
  // Print the baseline image to the dashboard anyway, even if the test passed.
  // This is useful for validation, since we have scripts that automatically
  // generate baseline images.. Its just convenient sometimes to look at all
  // the baselines on the dashboard. [On by default].
  vtkSetMacro( PrintBaselinesToDashboard, int ) 
    
  // Description:
  // Displays an image on the dashboard. The filename is determined by the
  // ValidImageFileName+Suffix. The name on the dashboard will be determined
  // by the comment string.
  void DisplayImageOnDashboard( vtkImageData *, const char *suffix, 
      const char *comment );

protected:
  vtkVVTesting();
  ~vtkVVTesting();

  vtkTesting* Testing;
  vtkKWRenderWidget* RenderView;
  vtkImageData *ImageData;
  char* ComparisonImage;
  
  vtkImageAppend *AppendFilter;
  int PrintBaselinesToDashboard;
  
private:
  vtkVVTesting(const vtkVVTesting&); // Not implemented
  void operator=(const vtkVVTesting&); // Not implemented

  void DisplayBaselineImageOnDashboard();
};

#endif

