##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
IF(VTK_PRINT_SELF_CHECK_TCL)
  ADD_TEST(KWVolView-PrintSelf ${TCL_TCLSH}
    ${VTK_PRINT_SELF_CHECK_TCL}
    ${KWVolView_SOURCE_DIR})
ENDIF(VTK_PRINT_SELF_CHECK_TCL)

IF(VTK_FIND_STRING_TCL)
  ADD_TEST(KWVolView-TestSetObjectMacro ${TCL_TCLSH}
    ${VTK_FIND_STRING_TCL}
    "${KWVolView_SOURCE_DIR}/vtk\\\\*.h"
    "vtkSetObjectMacro")
ENDIF(VTK_FIND_STRING_TCL)

IF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
  INSTALL_FILES(${KWVolView_INSTALL_DATA_DIR}/Testing/Tcl FILES 
    ${KWVolView_TESTING_DIR}/Tcl/KWVolViewDiffImages.tcl
    ${KWVolView_TESTING_DIR}/Tcl/KWVolViewLoadVolumeAndGrabScreenshot.tcl
    ${KWVolView_TESTING_DIR}/Tcl/KWVolViewStartAndExitApplication.tcl)
ENDIF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
