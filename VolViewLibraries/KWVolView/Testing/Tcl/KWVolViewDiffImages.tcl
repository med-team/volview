proc DiffImages { imageData {threshold 60}} {
   global argc argv

   vtkVVTesting testing
   testing SetImageData $imageData
   for {set i 1} {$i < $argc} {incr i} {
     testing AddArgument "[lindex $argv $i]"
   }

   set res [ testing RegressionTest ${threshold} ]

   testing Delete  
   return $res
}
