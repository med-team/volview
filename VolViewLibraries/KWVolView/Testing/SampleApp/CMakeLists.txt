##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
CMAKE_MINIMUM_REQUIRED(VERSION 2.4)
IF(COMMAND CMAKE_POLICY)
  CMAKE_POLICY(SET CMP0003 NEW)
ENDIF(COMMAND CMAKE_POLICY)

PROJECT(KWVolViewSampleApp)

# Build a barebones sample application if testing is on..
INCLUDE("${KWVolView_CMAKE_DIR}/KWVolViewCreateExecutableMacros.cmake")
SET(BUILD_KWVolViewSampleApp            ON  CACHE INTERNAL "" FORCE)
SET(KWVolViewSampleApp_BUILD_AS_RELEASE OFF CACHE INTERNAL "" FORCE)
SET(KWVolVIewSampleApp_BUILD_TESTING    OFF CACHE INTERNAL "" FORCE)
KWVolView_CREATE_EXECUTABLE(
  APPLICATION_NAME    "VolView Application without toolbars or UI panels"
  SUPPORT_TRIAL_MODE  0
  SUPPORT_TESTING     0 
  SUPPORT_TESTING_GUI 0
  WINDOW_CLASS_NAME   "vtkVVWindowBase"
)

# Test if we can start the application and exit safely. 

GET_TARGET_PROPERTY(EXECUTABLE ${PROJECT_NAME}Launcher LOCATION)
IF(NOT EXECUTABLE)
  GET_TARGET_PROPERTY(EXECUTABLE ${PROJECT_NAME} LOCATION)
ENDIF(NOT EXECUTABLE)
STRING(REGEX REPLACE "\\$\\(OutDir\\)/" "" EXECUTABLE "${EXECUTABLE}")

ADD_TEST(KWVolView-StartAndExitApp ${EXECUTABLE}
  ${KWVolView_TESTING_DIR}/Tcl/KWVolViewStartAndExitApplication.tcl
  )

