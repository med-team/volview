/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSnapshotPool - a class that stores a pool of snapshots.
// .SECTION Description
// This class stores a pool of snapshots (vtkVVSnapshot). 

#ifndef __vtkVVSnapshotPool_h
#define __vtkVVSnapshotPool_h

#include "vtkKWObject.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkVVSnapshot;
class vtkVVSnapshotPoolInternals;

class VTK_EXPORT vtkVVSnapshotPool : public vtkKWObject
{
public:
  static vtkVVSnapshotPool* New();
  vtkTypeRevisionMacro(vtkVVSnapshotPool,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Add a snapshot.
  // It is ref counted (and will be released by RemoveSnapshot()).
  // Return 1 on success, 0 otherwise
  int AddSnapshot(vtkVVSnapshot*);
                 
  // Description:
  // Get number of snapshots stored so far
  virtual int GetNumberOfSnapshots();
  
  // Description:
  // Query if snapshot in the pool
  virtual int HasSnapshot(vtkVVSnapshot *snapshot);

  // Description:
  // Retrieve a given snapshot (i-th, or by name)
  vtkVVSnapshot* GetNthSnapshot(int i);

  // Description::
  // Retrieve the index of the snapshot based on it's name
  int GetIndexOfSnapshot(vtkVVSnapshot *snapshot);

  // Description:
  // Remove a given snapshot, or all of them
  virtual void RemoveSnapshot(vtkVVSnapshot*);
  virtual void RemoveAllSnapshots();

protected:
  vtkVVSnapshotPool();
  ~vtkVVSnapshotPool();

  // Description:
  // PIMPL Encapsulation for STL containers
  vtkVVSnapshotPoolInternals *Internals;

private:
  vtkVVSnapshotPool(const vtkVVSnapshotPool&); // Not implemented
  void operator=(const vtkVVSnapshotPool&); // Not implemented
};

#endif
