/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVFileAuthenticator.h"

#include "vtkObjectFactory.h"
#include "vtkVVFileInstance.h"
#include "vtkVVApplication.h"

#include "vtksys/SystemTools.hxx"
#include <vtkstd/vector>
#include <vtkstd/string>

#include "vtkKWWidgetsProConfigure.h" // Needed for KWWidgetsPro_USE_LICENSE
#ifdef KWWidgetsPro_USE_LICENSE
#include "vtkKWRSA.h"
#endif

//---------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVFileAuthenticator);
vtkCxxRevisionMacro(vtkVVFileAuthenticator, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkVVFileAuthenticator::vtkVVFileAuthenticator()
{
  this->ReasonString  = NULL;
  this->FileInstance  = NULL;
  this->PublicKey     = NULL;
  this->PublicKeyFile = NULL;
  this->SetPublicKeyFile("Authentication/license.pub");
}

//----------------------------------------------------------------------------
vtkVVFileAuthenticator::~vtkVVFileAuthenticator()
{
  this->SetReasonString(NULL);
  this->SetPublicKey(NULL);
  this->SetPublicKeyFile(NULL);
  this->SetFileInstance(NULL);
}

//----------------------------------------------------------------------------
int vtkVVFileAuthenticator::AuthenticateFile( const char * )
{
  this->SetReasonString("NULL");
  return 1;
}

//----------------------------------------------------------------------------
void vtkVVFileAuthenticator::SetFileInstance( vtkVVFileInstance * v )
{
  // Not ref counted.
  this->FileInstance = v;
}

//---------------------------------------------------------------------------
const char* vtkVVFileAuthenticator::GetPublicKey()
{
  if (!this->PublicKey && this->PublicKeyFile)
    {
    // Find the correct directory -- choose (in order of precedence)
    // binary tree, install tree (1 level up, 2 level up).  

    vtksys_stl::vector<vtksys_stl::string> dirs;
    vtksys_stl::string dir;

    dir = this->GetApplication()->GetInstallationDirectory();
    dir = dir + "/../share/" + this->GetApplication()->GetName();
    dirs.push_back(dir);

    dir = this->GetApplication()->GetInstallationDirectory();
    dir = dir + "/../../share/" + this->GetApplication()->GetName();
    dirs.push_back(dir);

    dirs.push_back(this->GetApplication()->GetUserDataDirectory());

    vtksys_stl::vector<vtksys_stl::string>::iterator it = dirs.begin();
    vtksys_stl::vector<vtksys_stl::string>::iterator end = dirs.end();

    int found = 0;
    vtksys_stl::string public_key;
    for (; it != end; it++)
      {
      public_key = (*it) + "/" + this->PublicKeyFile;
      if (vtksys::SystemTools::FileExists(public_key.c_str()))
        {
        found = 1;
        break;
        }
      }

    if (found)
      {
#ifdef KWWidgetsPro_USE_LICENSE
      this->SetPublicKey(
        vtkKWRSA::ExtractPublicKey(public_key.c_str() ).c_str());
#endif
      }
    }

  return this->PublicKey;
}

//----------------------------------------------------------------------------
void vtkVVFileAuthenticator::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

