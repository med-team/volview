/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVFileInstancePool - a class that stores a pool of file instances.
// .SECTION Description
// This class stores a pool of file instances (vtkVVFileInstance). 

#ifndef __vtkVVFileInstancePool_h
#define __vtkVVFileInstancePool_h

#include "vtkKWObject.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros
 
class vtkVVFileInstance;
class vtkVVFileInstancePoolInternals;

class VTK_EXPORT vtkVVFileInstancePool : public vtkKWObject
{
public:
  static vtkVVFileInstancePool* New();
  vtkTypeRevisionMacro(vtkVVFileInstancePool,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Add a file instance.
  // It is ref counted (and will be released by RemoveFileInstance()).
  // Return 1 on success, 0 otherwise
  int AddFileInstance(vtkVVFileInstance*);
                 
  // Description:
  // Get number of file instances stored so far
  virtual int GetNumberOfFileInstances();

  // Description:
  // Query if a file instance is in the pool, given an instance or just
  // its name
  virtual int HasFileInstance(vtkVVFileInstance *instance);
  virtual int HasFileInstanceWithName(const char *name);

  // Description:
  // Retrieve a given file instance (i-th, or by name)
  vtkVVFileInstance* GetNthFileInstance(int i);
  vtkVVFileInstance* GetFileInstanceWithName(const char *name);

  // Description::
  // Retrieve the index of a file instance
  int GetIndexOfFileInstance(vtkVVFileInstance *instance);

  // Description:
  // Remove a given file instance, or all of them
  virtual void RemoveFileInstance(vtkVVFileInstance*);
  virtual void RemoveAllFileInstances();
  
  // Description:
  // Get/Query/Remove/Enumerate file instances with the same filename(s) as
  // a given instance.
  virtual int HasFileInstanceWithSameFileNames(vtkVVFileInstance *instance);
  virtual int GetNumberOfFileInstancesWithSameFileNames(
    vtkVVFileInstance *instance);
  vtkVVFileInstance* GetNthFileInstanceWithSameFileNames(
    int i, vtkVVFileInstance *instance);
  int GetIndexOfNthFileInstanceWithSameFileNames(
    int i, vtkVVFileInstance *instance);
  virtual void RemoveNthFileInstanceWithSameFileNames(
    int i, vtkVVFileInstance *instance);

  // Description:
  // Suggest a unique name for a file instance, given the other file 
  // instances in the pool and their filenames.
  // Also accept a filename as parameter, suggesting to find a unique name
  // for an potential instance with a unique filename.
  // Return a pointer to a static buffer, use it ASAP or save it.
  virtual const char* SuggestUniqueNameForFileInstance(
    vtkVVFileInstance *instance);
  virtual const char* SuggestUniqueNameForFileInstanceWithFileName(
    const char *filename);

  // Description:
  // Get/Query/Remove/Enumerate file instances with the same filename(s) 
  // *and* the same open properties as a given instance (i.e. a file that
  // was open using the same custom parameters, say, custom extent, spacing, 
  // or scalar component, etc).
  virtual int HasSimilarFileInstance(vtkVVFileInstance *instance);
  virtual int GetNumberOfSimilarFileInstances(
    vtkVVFileInstance *instance);
  vtkVVFileInstance* GetNthSimilarFileInstance(
    int i, vtkVVFileInstance *instance);
  int GetIndexOfNthSimilarFileInstance(int i, vtkVVFileInstance *instance);
  virtual void RemoveNthSimilarFileInstance(int i, vtkVVFileInstance *instance);
  
protected:
  vtkVVFileInstancePool();
  ~vtkVVFileInstancePool();

  // Description:
  // PIMPL Encapsulation for STL containers
  vtkVVFileInstancePoolInternals *Internals;

private:
  vtkVVFileInstancePool(const vtkVVFileInstancePool&); // Not implemented
  void operator=(const vtkVVFileInstancePool&); // Not implemented
};

#endif
