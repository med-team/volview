/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVSnapshot.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkKWIcon.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSnapshot);
vtkCxxRevisionMacro(vtkVVSnapshot, "$Revision: 1.7 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLVVSnapshotReader.h"
#include "XML/vtkXMLVVSnapshotWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkVVSnapshot, vtkXMLVVSnapshotReader, vtkXMLVVSnapshotWriter);

vtkCxxSetObjectMacro(vtkVVSnapshot, SerializedForm, vtkXMLDataElement);
vtkCxxSetObjectMacro(vtkVVSnapshot, Thumbnail, vtkKWIcon);
vtkCxxSetObjectMacro(vtkVVSnapshot, Screenshot, vtkKWIcon);

//----------------------------------------------------------------------------
vtkVVSnapshot::vtkVVSnapshot()
{ 
  this->Description    = NULL;
  this->SerializedForm = NULL;
  this->Thumbnail      = NULL;
  this->Screenshot     = NULL;
  this->InternalFlag   = 0;
}
  
//----------------------------------------------------------------------------
vtkVVSnapshot::~vtkVVSnapshot()
{
  this->SetDescription(NULL);
  this->SetSerializedForm(NULL);
  this->SetThumbnail(NULL);
  this->SetScreenshot(NULL);
}

//----------------------------------------------------------------------------
void vtkVVSnapshot::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  
  os << indent << "Description: " 
     << (this->Description ? this->Description : "(NULL)") << endl;
  os << indent << "InternalFlag: " << this->InternalFlag << endl;
}
