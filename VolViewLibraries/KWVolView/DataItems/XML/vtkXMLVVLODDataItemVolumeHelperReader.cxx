/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVLODDataItemVolumeHelperReader.h"

#include "vtkObjectFactory.h"
#include "vtkVVLODDataItemVolumeHelper.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLVVLODDataItemVolumeHelperReader);
vtkCxxRevisionMacro(vtkXMLVVLODDataItemVolumeHelperReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVLODDataItemVolumeHelperReader::GetRootElementName()
{
  return "VVLODDataItemVolumeHelper";
}

//----------------------------------------------------------------------------
int vtkXMLVVLODDataItemVolumeHelperReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVLODDataItemVolumeHelper *obj = 
    vtkVVLODDataItemVolumeHelper::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVLODDataItemVolumeHelper is not set!");
    return 0;
    }

  // Get Attributes

  int ival, ival3[3];
  double dval;

  if (elem->GetScalarAttribute("LODLevel", ival))
    {
    obj->SetLODLevel(ival);
    }

  if (elem->GetScalarAttribute("LODMode", ival))
    {
    obj->SetLODMode(ival);
    }

  if (elem->GetScalarAttribute("LODShrinkFactor", dval))
    {
    obj->SetLODShrinkFactor(dval);
    }

  if (elem->GetScalarAttribute("CompressionRatio", dval))
    {
    obj->SetCompressionRatio(dval);
    }

  if (elem->GetScalarAttribute("CompressionRatio", dval))
    {
    obj->SetCompressionRatio(dval);
    }

  if (elem->GetVectorAttribute("MinimumSize", 3, ival3) == 3)
    {
    obj->SetMinimumSize(ival3);
    }

  if (elem->GetScalarAttribute("InterpolationMode", ival))
    {
    obj->SetInterpolationMode(ival);
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVLODDataItemVolumeHelperReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
