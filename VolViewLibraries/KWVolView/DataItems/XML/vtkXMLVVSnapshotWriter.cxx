/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVSnapshotWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVSnapshot.h"
#include "vtkVVWindowBase.h"
#include "vtkStringArray.h"
#include "vtkKWIcon.h"

#include "XML/vtkXMLObjectReader.h"
#include "XML/vtkXMLVVWindowBaseWriter.h"
#include "XML/vtkXMLKWIconWriter.h"

vtkStandardNewMacro(vtkXMLVVSnapshotWriter);
vtkCxxRevisionMacro(vtkXMLVVSnapshotWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotWriter::GetRootElementName()
{
  return "VVSnapshot";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotWriter::GetSerializedFormElementName()
{
  return "SerializedForm";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotWriter::GetThumbnailElementName()
{
  return "Thumbnail";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotWriter::GetScreenshotElementName()
{
  return "Screenshot";
}

//----------------------------------------------------------------------------
int vtkXMLVVSnapshotWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVSnapshot *obj = vtkVVSnapshot::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSnapshot is not set!");
    return 0;
    }

  elem->SetAttribute("Description", obj->GetDescription());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLVVSnapshotWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVSnapshot *obj = vtkVVSnapshot::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSnapshot is not set!");
    return 0;
    }

  // Thumbnail

  vtkKWIcon *icon = obj->GetThumbnail();
  if (icon)
    {
    vtkXMLKWIconWriter *xmlw = vtkXMLKWIconWriter::New();
    xmlw->SetObject(icon);
    xmlw->CreateInNestedElement(
      elem, this->GetThumbnailElementName());
    xmlw->Delete();
    }

  // Screenshot

  icon = obj->GetScreenshot();
  if (icon)
    {
    vtkXMLKWIconWriter *xmlw = vtkXMLKWIconWriter::New();
    xmlw->SetObject(icon);
    xmlw->CreateInNestedElement(
      elem, this->GetScreenshotElementName());
    xmlw->Delete();
    }

  // Serialized form

  vtkXMLDataElement *serialized_form = obj->GetSerializedForm();
  if (serialized_form)
    {
    vtkXMLDataElement *nested_elem = this->NewDataElement();
    nested_elem->SetName(this->GetSerializedFormElementName());
    vtkXMLDataElement *serialized_form_elem = this->NewDataElement();
    // We need to deepcopy here because the whole XML tree may be refactorize
    // and reshuffled and we could end up altering the original serialized form.
    serialized_form_elem->DeepCopy(serialized_form);
    // Also remove the attribute that specifies which file it was read from
    serialized_form_elem->GetRoot()->RemoveAttribute(
      vtkXMLObjectReader::GetParsedFromFileAttributeName());
    nested_elem->AddNestedElement(serialized_form_elem);
    serialized_form_elem->Delete();
    elem->AddNestedElement(nested_elem);
    nested_elem->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVSnapshotWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

