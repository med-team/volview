/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVFileInstanceWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVFileInstance.h"
#include "vtkVVWindowBase.h"
#include "vtkStringArray.h"
#include "vtkKWOpenFileProperties.h"

#include "XML/vtkXMLVVWindowBaseWriter.h"
#include "XML/vtkXMLKWOpenFilePropertiesWriter.h"

vtkStandardNewMacro(vtkXMLVVFileInstanceWriter);
vtkCxxRevisionMacro(vtkXMLVVFileInstanceWriter, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVFileInstanceWriter::GetRootElementName()
{
  return "VVFileInstance";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVFileInstanceWriter::GetFileNameElementName()
{
  return "FileName";
}

//----------------------------------------------------------------------------
int vtkXMLVVFileInstanceWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVFileInstance *obj = vtkVVFileInstance::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVFileInstance is not set!");
    return 0;
    }

  elem->SetAttribute("Name", obj->GetName());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLVVFileInstanceWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVFileInstance *obj = vtkVVFileInstance::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVFileInstance is not set!");
    return 0;
    }

  // Open File Properties

  if (obj->GetOpenFileProperties())
    {
    vtkXMLKWOpenFilePropertiesWriter *xmlw = 
      vtkXMLKWOpenFilePropertiesWriter::SafeDownCast(
        obj->GetOpenFileProperties()->GetNewXMLWriter());
    xmlw->DiscardFilePatternDirectoryOn();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  // File Names

  for (int i = 0; i < obj->GetNumberOfFileNames(); i++)
    {
    vtkXMLDataElement *nested_elem = this->NewDataElement();
    nested_elem->SetName(this->GetFileNameElementName());
    nested_elem->SetAttribute("Value", obj->GetNthFileName(i));

    // URI's for remote downloading.
    const char *sourceURI = obj->GetNthFileNameSourceURI(i);
    const char *destinationURI = obj->GetNthFileNameDestinationURI(i);
    if (sourceURI && destinationURI)
      {
      nested_elem->SetAttribute("SourceURI", sourceURI);
      nested_elem->SetAttribute("DestinationURI", destinationURI);
      }

    // Preview URI's for remote downloading.
    const char *previewSourceURI = obj->GetNthFileNamePreviewSourceURI(i);
    const char *previewDestinationURI = obj->GetNthFileNamePreviewDestinationURI(i);
    const char *previewFile = obj->GetNthFileNamePreviewFilename(i);
    if (previewSourceURI && previewDestinationURI && previewFile)
      {
      nested_elem->SetAttribute("PreviewSourceURI", previewSourceURI);
      nested_elem->SetAttribute("PreviewDestinationURI", previewDestinationURI);
      nested_elem->SetAttribute("PreviewFile", previewFile);
      }

    elem->AddNestedElement(nested_elem);
    nested_elem->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVFileInstanceWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

