/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVDataItem.h"
#include "vtkVVFileInstance.h"
#include "vtkStringArray.h"

#include "XML/vtkXMLVVWindowBaseWriter.h"

vtkStandardNewMacro(vtkXMLVVDataItemWriter);
vtkCxxRevisionMacro(vtkXMLVVDataItemWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemWriter::GetRootElementName()
{
  return "VVDataItem";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVDataItem *obj = vtkVVDataItem::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItem is not set!");
    return 0;
    }

  elem->SetAttribute("Name", obj->GetName());

  elem->SetAttribute("DescriptiveName", obj->GetDescriptiveName());

  elem->SetAttribute("DistanceUnits", obj->GetDistanceUnits());

  if (obj->GetFileInstance())
    {
    elem->SetAttribute("FileInstanceName", obj->GetFileInstance()->GetName());
    }

  elem->SetIntAttribute("Scope", obj->GetScope());
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

