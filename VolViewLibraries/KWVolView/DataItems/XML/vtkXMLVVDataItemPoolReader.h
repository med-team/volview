/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemPoolReader - vtkVVDataItemPool XML Reader.
// .SECTION Description
// vtkXMLVVDataItemPoolReader provides XML reading functionality to 
// vtkVVDataItemPool.
// .SECTION See Also
// vtkXMLVVDataItemPoolWriter

#ifndef __vtkXMLVVDataItemPoolReader_h
#define __vtkXMLVVDataItemPoolReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVDataItemPoolReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVDataItemPoolReader* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemPoolReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

  // Description:
  // Turn on/off the UpdateMode. In this mode, the pool is not emptied
  // before unserializing the XML. For each DataItem instance retrieved,
  // a check is made if an instance with the same name already exists: if
  // found, this will be the instance updated in-place; otherwise a new
  // instance is added. Instances that were in the pool but not in the
  // XML stream are removed.
  vtkGetMacro(UpdateMode,int);
  vtkSetMacro(UpdateMode,int);
  vtkBooleanMacro(UpdateMode,int);

protected:  
  vtkXMLVVDataItemPoolReader();
  ~vtkXMLVVDataItemPoolReader() {};

  int UpdateMode;

private:
  vtkXMLVVDataItemPoolReader(const vtkXMLVVDataItemPoolReader&); // Not implemented
  void operator=(const vtkXMLVVDataItemPoolReader&); // Not implemented    
};

#endif

