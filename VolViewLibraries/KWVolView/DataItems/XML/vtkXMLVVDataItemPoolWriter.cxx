/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemPoolWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVDataItemPool.h"
#include "vtkVVDataItem.h"

vtkStandardNewMacro(vtkXMLVVDataItemPoolWriter);
vtkCxxRevisionMacro(vtkXMLVVDataItemPoolWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemPoolWriter::GetRootElementName()
{
  return "VVDataItemPool";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemPoolWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVDataItemPool *obj = 
    vtkVVDataItemPool::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItemPool is not set!");
    return 0;
    }

  // Data Items

  for (int i = 0; i < obj->GetNumberOfDataItems(); i++)
    {
    vtkXMLObjectWriter *xmlw = obj->GetNthDataItem(i)->GetNewXMLWriter();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemPoolWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

