/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemReader - vtkVVDataItem XML Reader.
// .SECTION Description
// vtkXMLVVDataItemReader provides XML reading functionality to 
// vtkVVDataItem.
// .SECTION See Also
// vtkXMLVVDataItemWriter

#ifndef __vtkXMLVVDataItemReader_h
#define __vtkXMLVVDataItemReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVDataItemReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVDataItemReader* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVDataItemReader() {};
  ~vtkXMLVVDataItemReader() {};

private:
  vtkXMLVVDataItemReader(const vtkXMLVVDataItemReader&); // Not implemented
  void operator=(const vtkXMLVVDataItemReader&); // Not implemented    
};

#endif

