/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVSnapshotReader.h"

#include "vtkObjectFactory.h"
#include "vtkVVSnapshot.h"
#include "vtkXMLDataElement.h"
#include "vtkStringArray.h"
#include "vtkKWIcon.h"

#include "XML/vtkXMLVVSnapshotWriter.h"
#include "XML/vtkXMLKWIconReader.h"

vtkStandardNewMacro(vtkXMLVVSnapshotReader);
vtkCxxRevisionMacro(vtkXMLVVSnapshotReader, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotReader::GetRootElementName()
{
  return "VVSnapshot";
}

//----------------------------------------------------------------------------
int vtkXMLVVSnapshotReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVSnapshot *obj = vtkVVSnapshot::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSnapshot is not set!");
    return 0;
    }

  // Get Attributes

  obj->SetDescription(elem->GetAttribute("Description"));

  // Get nested elements

  vtkXMLDataElement *nested_elem;

  // Thumbnail

  obj->SetThumbnail(NULL);
  nested_elem = elem->FindNestedElementWithName(
    vtkXMLVVSnapshotWriter::GetThumbnailElementName());
  if (nested_elem)
    {
    vtkKWIcon *icon = vtkKWIcon::New();
    vtkXMLKWIconReader *xmlr = vtkXMLKWIconReader::New();
    xmlr->SetObject(icon);
    if (xmlr->ParseInElement(nested_elem))
      {
      obj->SetThumbnail(icon);
      }
    icon->Delete();
    xmlr->Delete();
    }

  // Screenshot

  obj->SetScreenshot(NULL);
  nested_elem = elem->FindNestedElementWithName(
    vtkXMLVVSnapshotWriter::GetScreenshotElementName());
  if (nested_elem)
    {
    vtkKWIcon *icon = vtkKWIcon::New();
    vtkXMLKWIconReader *xmlr = vtkXMLKWIconReader::New();
    xmlr->SetObject(icon);
    if (xmlr->ParseInElement(nested_elem))
      {
      obj->SetScreenshot(icon);
      }
    icon->Delete();
    xmlr->Delete();
    }

  // Serialized form

  obj->SetSerializedForm(NULL);
  nested_elem = elem->FindNestedElementWithName(
    vtkXMLVVSnapshotWriter::GetSerializedFormElementName());
  if (nested_elem)
    {
    vtkXMLDataElement *serialized_form_elem = this->NewDataElement();
    // We need to deepcopy here because the rest of XML tree may be
    // deleted, and accessing the root from a child will go *boom*.
    serialized_form_elem->DeepCopy(nested_elem->GetNestedElement(0));
    obj->SetSerializedForm(serialized_form_elem);
    serialized_form_elem->Delete();

    // Also if the whole tree was read from a file, copy the attribute
    // that specifies which file it was, that can help relocating absolute
    // paths...
    const char *parsed_from_file = elem->GetRoot()->GetAttribute(
      vtkXMLObjectReader::GetParsedFromFileAttributeName());
    if (parsed_from_file)
      {
      serialized_form_elem->GetRoot()->SetAttribute(
        vtkXMLObjectReader::GetParsedFromFileAttributeName(), parsed_from_file);
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVSnapshotReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
