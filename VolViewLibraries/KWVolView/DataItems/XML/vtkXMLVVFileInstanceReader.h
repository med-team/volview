/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVFileInstanceReader - vtkVVFileInstance XML Reader.
// .SECTION Description
// vtkXMLVVFileInstanceReader provides XML reading functionality to 
// vtkVVFileInstance.
// .SECTION See Also
// vtkXMLVVFileInstanceWriter

#ifndef __vtkXMLVVFileInstanceReader_h
#define __vtkXMLVVFileInstanceReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVFileInstanceReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVFileInstanceReader* New();
  vtkTypeRevisionMacro(vtkXMLVVFileInstanceReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVFileInstanceReader() {};
  ~vtkXMLVVFileInstanceReader() {};

private:
  vtkXMLVVFileInstanceReader(const vtkXMLVVFileInstanceReader&); // Not implemented
  void operator=(const vtkXMLVVFileInstanceReader&); // Not implemented    
};

#endif

