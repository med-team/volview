/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVFileInstancePoolWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVFileInstancePool.h"
#include "vtkVVFileInstance.h"

vtkStandardNewMacro(vtkXMLVVFileInstancePoolWriter);
vtkCxxRevisionMacro(vtkXMLVVFileInstancePoolWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVFileInstancePoolWriter::GetRootElementName()
{
  return "VVFileInstancePool";
}

//----------------------------------------------------------------------------
int vtkXMLVVFileInstancePoolWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVFileInstancePool *obj = 
    vtkVVFileInstancePool::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVFileInstancePool is not set!");
    return 0;
    }

  // File Instances

  for (int i = 0; i < obj->GetNumberOfFileInstances(); i++)
    {
    vtkXMLObjectWriter *xmlw = obj->GetNthFileInstance(i)->GetNewXMLWriter();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVFileInstancePoolWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

