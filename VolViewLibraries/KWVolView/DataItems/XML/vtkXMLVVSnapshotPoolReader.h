/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSnapshotPoolReader - vtkVVSnapshotPool XML Reader.
// .SECTION Description
// vtkXMLVVSnapshotPoolReader provides XML reading functionality to 
// vtkVVSnapshotPool.
// .SECTION See Also
// vtkXMLVVSnapshotPoolWriter

#ifndef __vtkXMLVVSnapshotPoolReader_h
#define __vtkXMLVVSnapshotPoolReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVSnapshotPoolReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVSnapshotPoolReader* New();
  vtkTypeRevisionMacro(vtkXMLVVSnapshotPoolReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVSnapshotPoolReader() {};
  ~vtkXMLVVSnapshotPoolReader() {};

private:
  vtkXMLVVSnapshotPoolReader(const vtkXMLVVSnapshotPoolReader&); // Not implemented
  void operator=(const vtkXMLVVSnapshotPoolReader&); // Not implemented    
};

#endif

