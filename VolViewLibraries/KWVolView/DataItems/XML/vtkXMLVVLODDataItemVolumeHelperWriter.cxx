/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVLODDataItemVolumeHelperWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVLODDataItemVolumeHelper.h"

vtkStandardNewMacro(vtkXMLVVLODDataItemVolumeHelperWriter);
vtkCxxRevisionMacro(vtkXMLVVLODDataItemVolumeHelperWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVLODDataItemVolumeHelperWriter::GetRootElementName()
{
  return "VVLODDataItemVolumeHelper";
}

//----------------------------------------------------------------------------
int vtkXMLVVLODDataItemVolumeHelperWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVLODDataItemVolumeHelper *obj = 
    vtkVVLODDataItemVolumeHelper::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVLODDataItemVolumeHelper is not set!");
    return 0;
    }

  elem->SetIntAttribute("LODLevel", obj->GetLODLevel());

  elem->SetIntAttribute("LODMode", obj->GetLODMode());

  elem->SetDoubleAttribute("LODShrinkFactor", obj->GetLODShrinkFactor());

  elem->SetDoubleAttribute("CompressionRatio", obj->GetCompressionRatio());

  elem->SetVectorAttribute("MinimumSize", 3, obj->GetMinimumSize());

  elem->SetIntAttribute("InterpolationMode", obj->GetInterpolationMode());

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVLODDataItemVolumeHelperWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

