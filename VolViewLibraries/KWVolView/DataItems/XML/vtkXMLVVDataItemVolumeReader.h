/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemVolumeReader - vtkVVDataItemVolume XML Reader.
// .SECTION Description
// vtkXMLVVDataItemVolumeReader provides XML reading functionality to 
// vtkVVDataItemVolume.
// .SECTION See Also
// vtkXMLVVDataItemVolumeWriter

#ifndef __vtkXMLVVDataItemVolumeReader_h
#define __vtkXMLVVDataItemVolumeReader_h

#include "XML/vtkXMLVVDataItemReader.h"

class VTK_EXPORT vtkXMLVVDataItemVolumeReader : public vtkXMLVVDataItemReader
{
public:
  static vtkXMLVVDataItemVolumeReader* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemVolumeReader, vtkXMLVVDataItemReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVDataItemVolumeReader() {};
  ~vtkXMLVVDataItemVolumeReader() {};

private:
  vtkXMLVVDataItemVolumeReader(const vtkXMLVVDataItemVolumeReader&); // Not implemented
  void operator=(const vtkXMLVVDataItemVolumeReader&); // Not implemented    
};

#endif

