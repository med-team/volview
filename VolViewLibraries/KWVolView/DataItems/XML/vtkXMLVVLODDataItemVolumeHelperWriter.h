/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVLODDataItemVolumeHelperWriter - vtkVVLODDataItemVolumeHelper XML Writer.
// .SECTION Description
// vtkXMLVVLODDataItemVolumeHelperWriter provides XML writing functionality to 
// vtkVVLODDataItemVolumeHelper.
// .SECTION See Also
// vtkXMLVVLODDataItemVolumeHelperReader

#ifndef __vtkXMLVVLODDataItemVolumeHelperWriter_h
#define __vtkXMLVVLODDataItemVolumeHelperWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVLODDataItemVolumeHelperWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVLODDataItemVolumeHelperWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVLODDataItemVolumeHelperWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVLODDataItemVolumeHelperWriter() {};
  ~vtkXMLVVLODDataItemVolumeHelperWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLVVLODDataItemVolumeHelperWriter(const vtkXMLVVLODDataItemVolumeHelperWriter&);  // Not implemented.
  void operator=(const vtkXMLVVLODDataItemVolumeHelperWriter&);  // Not implemented.
};

#endif

