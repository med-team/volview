/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemPoolReader.h"

#include "vtkObjectFactory.h"
#include "vtkInstantiator.h"
#include "vtkVVDataItemPool.h"
#include "vtkXMLDataElement.h"
#include "vtkKWApplication.h"
#include "vtkVVDataItem.h"

#include "XML/vtkXMLVVDataItemPoolWriter.h"
#include "KWVolViewInstantiator.h"

vtkStandardNewMacro(vtkXMLVVDataItemPoolReader);
vtkCxxRevisionMacro(vtkXMLVVDataItemPoolReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
vtkXMLVVDataItemPoolReader::vtkXMLVVDataItemPoolReader()
{
  this->UpdateMode = 0;
}

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemPoolReader::GetRootElementName()
{
  return "VVDataItemPool";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemPoolReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVDataItemPool *obj = 
    vtkVVDataItemPool::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItemPool is not set!");
    return 0;
    }

  // Get Attributes

  // Data Items

  vtkVVDataItemPool *old_pool = NULL;

  if (this->GetUpdateMode())
    {
    // In UpdateMode, keep track of the old pool
    old_pool = vtkVVDataItemPool::New();
    for (int i = 0; i < obj->GetNumberOfDataItems(); i++)
      {
      old_pool->AddDataItem(obj->GetNthDataItem(i));
      }
    }

  obj->RemoveAllDataItems();

  int idx, nb_nested_elems = elem->GetNumberOfNestedElements();
  for (idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);
    const char *classname = nested_elem->GetAttribute("ClassName");
    if (classname)
      {
      vtkVVDataItem *data_item = NULL;
      int has_old_data_item = 0;
      if (this->GetUpdateMode())
        {
        data_item = old_pool->GetDataItemWithName(
          nested_elem->GetAttribute("Name"));
        has_old_data_item = data_item ? 1 : 0;
        }
      if (!has_old_data_item)
        {
        data_item = vtkVVDataItem::SafeDownCast(
          vtkInstantiator::CreateInstance(classname));
        }
      if (data_item)
        {
        if (!data_item->GetApplication())
          {
          data_item->SetApplication(obj->GetApplication());
          }
        vtkXMLObjectReader *xmlr = data_item->GetNewXMLReader();
        xmlr->Parse(nested_elem);
        obj->AddDataItem(data_item);
        if (!has_old_data_item)
          {
          data_item->Delete();
          }
        xmlr->Delete();
        }
      }
    }

  if (old_pool)
    {
    old_pool->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemPoolReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "UpdateMode: " 
    << (this->UpdateMode ? "On\n" : "Off\n");
}
