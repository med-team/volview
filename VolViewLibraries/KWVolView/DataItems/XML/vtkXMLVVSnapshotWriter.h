/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSnapshotWriter - vtkVVSnapshot XML Writer.
// .SECTION Description
// vtkXMLVVSnapshotWriter provides XML writing functionality to 
// vtkVVSnapshot.
// .SECTION See Also
// vtkXMLVVSnapshotReader

#ifndef __vtkXMLVVSnapshotWriter_h
#define __vtkXMLVVSnapshotWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVSnapshotWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVSnapshotWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVSnapshotWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the serialized form.
  static const char* GetSerializedFormElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the thumbnail.
  static const char* GetThumbnailElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the screenshot.
  static const char* GetScreenshotElementName();

protected:
  vtkXMLVVSnapshotWriter() {};
  ~vtkXMLVVSnapshotWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVSnapshotWriter(const vtkXMLVVSnapshotWriter&);  // Not implemented.
  void operator=(const vtkXMLVVSnapshotWriter&);  // Not implemented.
};

#endif

