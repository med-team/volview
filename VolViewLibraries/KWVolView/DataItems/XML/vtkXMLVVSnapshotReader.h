/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSnapshotReader - vtkVVSnapshot XML Reader.
// .SECTION Description
// vtkXMLVVSnapshotReader provides XML reading functionality to 
// vtkVVSnapshot.
// .SECTION See Also
// vtkXMLVVSnapshotWriter

#ifndef __vtkXMLVVSnapshotReader_h
#define __vtkXMLVVSnapshotReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVSnapshotReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVSnapshotReader* New();
  vtkTypeRevisionMacro(vtkXMLVVSnapshotReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVSnapshotReader() {};
  ~vtkXMLVVSnapshotReader() {};

private:
  vtkXMLVVSnapshotReader(const vtkXMLVVSnapshotReader&); // Not implemented
  void operator=(const vtkXMLVVSnapshotReader&); // Not implemented    
};

#endif

