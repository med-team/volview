/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemPoolWriter - vtkVVDataItemPool XML Writer.
// .SECTION Description
// vtkXMLVVDataItemPoolWriter provides XML writing functionality to 
// vtkVVDataItemPool.
// .SECTION See Also
// vtkXMLVVDataItemPoolReader

#ifndef __vtkXMLVVDataItemPoolWriter_h
#define __vtkXMLVVDataItemPoolWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVDataItemPoolWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVDataItemPoolWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemPoolWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVDataItemPoolWriter() {};
  ~vtkXMLVVDataItemPoolWriter() {};

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVDataItemPoolWriter(const vtkXMLVVDataItemPoolWriter&);  // Not implemented.
  void operator=(const vtkXMLVVDataItemPoolWriter&);  // Not implemented.
};

#endif

