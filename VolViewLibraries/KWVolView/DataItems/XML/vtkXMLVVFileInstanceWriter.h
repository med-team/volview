/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVFileInstanceWriter - vtkVVFileInstance XML Writer.
// .SECTION Description
// vtkXMLVVFileInstanceWriter provides XML writing functionality to 
// vtkVVFileInstance.
// .SECTION See Also
// vtkXMLVVFileInstanceReader

#ifndef __vtkXMLVVFileInstanceWriter_h
#define __vtkXMLVVFileInstanceWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVFileInstanceWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVFileInstanceWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVFileInstanceWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store each filename.
  static const char* GetFileNameElementName();

protected:
  vtkXMLVVFileInstanceWriter() {};
  ~vtkXMLVVFileInstanceWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVFileInstanceWriter(const vtkXMLVVFileInstanceWriter&);  // Not implemented.
  void operator=(const vtkXMLVVFileInstanceWriter&);  // Not implemented.
};

#endif

