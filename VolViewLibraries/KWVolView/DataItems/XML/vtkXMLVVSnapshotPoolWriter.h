/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSnapshotPoolWriter - vtkVVSnapshotPool XML Writer.
// .SECTION Description
// vtkXMLVVSnapshotPoolWriter provides XML writing functionality to 
// vtkVVSnapshotPool.
// .SECTION See Also
// vtkXMLVVSnapshotPoolReader

#ifndef __vtkXMLVVSnapshotPoolWriter_h
#define __vtkXMLVVSnapshotPoolWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVSnapshotPoolWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVSnapshotPoolWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVSnapshotPoolWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVSnapshotPoolWriter() {};
  ~vtkXMLVVSnapshotPoolWriter() {};

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVSnapshotPoolWriter(const vtkXMLVVSnapshotPoolWriter&);  // Not implemented.
  void operator=(const vtkXMLVVSnapshotPoolWriter&);  // Not implemented.
};

#endif

