/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemWriter - vtkVVDataItem XML Writer.
// .SECTION Description
// vtkXMLVVDataItemWriter provides XML writing functionality to 
// vtkVVDataItem.
// .SECTION See Also
// vtkXMLVVDataItemReader

#ifndef __vtkXMLVVDataItemWriter_h
#define __vtkXMLVVDataItemWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVDataItemWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVDataItemWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVDataItemWriter() {};
  ~vtkXMLVVDataItemWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLVVDataItemWriter(const vtkXMLVVDataItemWriter&);  // Not implemented.
  void operator=(const vtkXMLVVDataItemWriter&);  // Not implemented.
};

#endif

