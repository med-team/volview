/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemVolumeReader.h"

#include "vtkObjectFactory.h"
#include "vtkVVDataItemVolume.h"
#include "vtkXMLDataElement.h"
#include "vtkMedicalImageProperties.h"
#include "vtkVVLODDataItemVolumeHelper.h"

#include "XML/vtkXMLVVDataItemVolumeWriter.h"
#include "XML/vtkXMLMedicalImagePropertiesReader.h"
#include "XML/vtkXMLVVLODDataItemVolumeHelperReader.h"

vtkStandardNewMacro(vtkXMLVVDataItemVolumeReader);
vtkCxxRevisionMacro(vtkXMLVVDataItemVolumeReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemVolumeReader::GetRootElementName()
{
  return "VVDataItemVolume";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemVolumeReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVDataItemVolume *obj = vtkVVDataItemVolume::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItemVolume is not set!");
    return 0;
    }

  // Get Attributes

  int i;
  for (i = 0; i < VTK_MAX_VRCOMP; i++)
    {
    char scalarunits[256];
    sprintf(scalarunits, "ScalarUnits%d", i);
    const char *cptr = elem->GetAttribute(scalarunits);
    if (cptr)
      {
      obj->SetScalarUnits(i, cptr);
      }
    }

  // Transfer functions
  // Not serialized, see writer for explanations

  // Medical Image Properties

  vtkMedicalImageProperties *med_prop = vtkMedicalImageProperties::New();
  if (med_prop)
    {
    vtkXMLMedicalImagePropertiesReader *xmlr = 
      vtkXMLMedicalImagePropertiesReader::New();
    xmlr->SetObject(med_prop);
    if (xmlr->ParseInElement(elem))
      {
      if (obj->GetMedicalImageProperties())
        {
        obj->GetMedicalImageProperties()->DeepCopy(med_prop);
        }
      else
        {
        obj->SetMedicalImageProperties(med_prop); // ref counted
        }
      }
    med_prop->Delete();
    xmlr->Delete();
    }

  // LOD Helper

  if (obj->GetLODHelper())
    {
    vtkXMLVVLODDataItemVolumeHelperReader *xmlr = 
      vtkXMLVVLODDataItemVolumeHelperReader::New();
    xmlr->SetObject(obj->GetLODHelper());
    xmlr->ParseInElement(elem);
    xmlr->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemVolumeReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
