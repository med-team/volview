/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVSnapshotPoolReader.h"

#include "vtkObjectFactory.h"
#include "vtkInstantiator.h"
#include "vtkVVSnapshotPool.h"
#include "vtkXMLDataElement.h"
#include "vtkVVSnapshot.h"

#include "XML/vtkXMLVVSnapshotPoolWriter.h"
#include "KWVolViewInstantiator.h"

vtkStandardNewMacro(vtkXMLVVSnapshotPoolReader);
vtkCxxRevisionMacro(vtkXMLVVSnapshotPoolReader, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotPoolReader::GetRootElementName()
{
  return "VVSnapshotPool";
}

//----------------------------------------------------------------------------
int vtkXMLVVSnapshotPoolReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVSnapshotPool *obj = 
    vtkVVSnapshotPool::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSnapshotPool is not set!");
    return 0;
    }

  // Get Attributes

  // Snapshots
  
  obj->RemoveAllSnapshots();

  // Backward compat, we used to put them in "Snapshots"

  vtkXMLDataElement *compat_nested_elem = 
    elem->FindNestedElementWithName("Snapshots");
  if (compat_nested_elem)
    {
    elem = compat_nested_elem;
    }

  int idx, nb_nested_elems = elem->GetNumberOfNestedElements();
  for (idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);
    const char *classname = nested_elem->GetAttribute("ClassName");
    if (classname)
      {
      vtkVVSnapshot *snapshot = vtkVVSnapshot::SafeDownCast(
        vtkInstantiator::CreateInstance(classname));
      if (snapshot)
        {
        vtkXMLObjectReader *xmlr = snapshot->GetNewXMLReader();
        xmlr->Parse(nested_elem);
        obj->AddSnapshot(snapshot);
        snapshot->Delete();
        xmlr->Delete();
        }
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVSnapshotPoolReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
