/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVFileInstancePoolWriter - vtkVVFileInstancePool XML Writer.
// .SECTION Description
// vtkXMLVVFileInstancePoolWriter provides XML writing functionality to 
// vtkVVFileInstancePool.
// .SECTION See Also
// vtkXMLVVFileInstancePoolReader

#ifndef __vtkXMLVVFileInstancePoolWriter_h
#define __vtkXMLVVFileInstancePoolWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVFileInstancePoolWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVFileInstancePoolWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVFileInstancePoolWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVFileInstancePoolWriter() {};
  ~vtkXMLVVFileInstancePoolWriter() {};

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVFileInstancePoolWriter(const vtkXMLVVFileInstancePoolWriter&);  // Not implemented.
  void operator=(const vtkXMLVVFileInstancePoolWriter&);  // Not implemented.
};

#endif

