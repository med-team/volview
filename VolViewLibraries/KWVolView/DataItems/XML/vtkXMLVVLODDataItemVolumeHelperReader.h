/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVLODDataItemVolumeHelperReader - vtkVVLODDataItemVolumeHelper XML Reader.
// .SECTION Description
// vtkXMLVVLODDataItemVolumeHelperReader provides XML reading functionality to 
// vtkVVLODDataItemVolumeHelper.
// .SECTION See Also
// vtkXMLVVLODDataItemVolumeHelperWriter

#ifndef __vtkXMLVVLODDataItemVolumeHelperReader_h
#define __vtkXMLVVLODDataItemVolumeHelperReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVLODDataItemVolumeHelperReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVLODDataItemVolumeHelperReader* New();
  vtkTypeRevisionMacro(vtkXMLVVLODDataItemVolumeHelperReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVLODDataItemVolumeHelperReader() {};
  ~vtkXMLVVLODDataItemVolumeHelperReader() {};

private:
  vtkXMLVVLODDataItemVolumeHelperReader(const vtkXMLVVLODDataItemVolumeHelperReader&); // Not implemented
  void operator=(const vtkXMLVVLODDataItemVolumeHelperReader&); // Not implemented    
};

#endif

