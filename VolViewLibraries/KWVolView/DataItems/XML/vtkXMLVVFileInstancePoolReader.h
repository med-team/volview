/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVFileInstancePoolReader - vtkVVFileInstancePool XML Reader.
// .SECTION Description
// vtkXMLVVFileInstancePoolReader provides XML reading functionality to 
// vtkVVFileInstancePool.
// .SECTION See Also
// vtkXMLVVFileInstancePoolWriter

#ifndef __vtkXMLVVFileInstancePoolReader_h
#define __vtkXMLVVFileInstancePoolReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVFileInstancePoolReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVFileInstancePoolReader* New();
  vtkTypeRevisionMacro(vtkXMLVVFileInstancePoolReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVFileInstancePoolReader() {};
  ~vtkXMLVVFileInstancePoolReader() {};

private:
  vtkXMLVVFileInstancePoolReader(const vtkXMLVVFileInstancePoolReader&); // Not implemented
  void operator=(const vtkXMLVVFileInstancePoolReader&); // Not implemented    
};

#endif

