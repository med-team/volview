/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemVolumeWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVDataItemVolume.h"
#include "vtkMedicalImageProperties.h"
#include "vtkVVLODDataItemVolumeHelper.h"
#include "vtkStringArray.h"

#include "XML/vtkXMLVVWindowBaseWriter.h"
#include "XML/vtkXMLMedicalImagePropertiesWriter.h"
#include "XML/vtkXMLVVLODDataItemVolumeHelperWriter.h"

vtkStandardNewMacro(vtkXMLVVDataItemVolumeWriter);
vtkCxxRevisionMacro(vtkXMLVVDataItemVolumeWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemVolumeWriter::GetRootElementName()
{
  return "VVDataItemVolume";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemVolumeWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVDataItemVolume *obj = vtkVVDataItemVolume::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItemVolume is not set!");
    return 0;
    }

  // Scalar units

  int i;
  for (i = 0; i < VTK_MAX_VRCOMP; i++)
    {
    if (obj->GetScalarUnits(i))
      {
      char scalarunits[256];
      sprintf(scalarunits, "ScalarUnits%d", i);
      elem->SetAttribute(scalarunits, obj->GetScalarUnits(i));
      }
    }

  // Do not serialize DisplayMode, so that people can't modify the XML
  // manually and sudently allow more display modes!

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemVolumeWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVDataItemVolume *obj = vtkVVDataItemVolume::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItemVolume is not set!");
    return 0;
    }

  // Transfer functions
  // Let's not serialize it at the moment; we should do it but this is
  // taken care of by the  vtkKWVolumeWidget's XML serializer that is bound
  // to be associated to the data in the session. There seems to be a decent
  // amount of smart in the vtkKWVolumeWidget serialization (and especially
  // deserialization), especially with respect to range.

  // Medical Image Properties

  if (obj->GetMedicalImageProperties())
    {
    vtkXMLMedicalImagePropertiesWriter *xmlw = 
      vtkXMLMedicalImagePropertiesWriter::New();
    xmlw->SetObject(obj->GetMedicalImageProperties());
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  // LOD Helper

  if (obj->GetLODHelper())
    {
    vtkXMLVVLODDataItemVolumeHelperWriter *xmlw = 
      vtkXMLVVLODDataItemVolumeHelperWriter::New();
    xmlw->SetObject(obj->GetLODHelper());
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemVolumeWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

