/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVDataItemReader.h"

#include "vtkObjectFactory.h"
#include "vtkVVDataItem.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLVVDataItemWriter.h"

vtkStandardNewMacro(vtkXMLVVDataItemReader);
vtkCxxRevisionMacro(vtkXMLVVDataItemReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVDataItemReader::GetRootElementName()
{
  return "VVDataItem";
}

//----------------------------------------------------------------------------
int vtkXMLVVDataItemReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVDataItem *obj = vtkVVDataItem::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVDataItem is not set!");
    return 0;
    }

  // Get Attributes

  int ival;

  obj->SetName(elem->GetAttribute("Name"));

  obj->SetDescriptiveName(elem->GetAttribute("DescriptiveName"));

  obj->SetDistanceUnits(elem->GetAttribute("DistanceUnits"));

  if (elem->GetScalarAttribute("Scope", ival))
    {
    obj->SetScope(ival);
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVDataItemReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
