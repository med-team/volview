/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVSnapshotPoolWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVSnapshotPool.h"
#include "vtkVVSnapshot.h"

vtkStandardNewMacro(vtkXMLVVSnapshotPoolWriter);
vtkCxxRevisionMacro(vtkXMLVVSnapshotPoolWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVSnapshotPoolWriter::GetRootElementName()
{
  return "VVSnapshotPool";
}

//----------------------------------------------------------------------------
int vtkXMLVVSnapshotPoolWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVSnapshotPool *obj = 
    vtkVVSnapshotPool::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSnapshotPool is not set!");
    return 0;
    }

  // Snapshots

  for (int i = 0; i < obj->GetNumberOfSnapshots(); i++)
    {
    vtkVVSnapshot *snapshot = obj->GetNthSnapshot(i);
    if (snapshot && !snapshot->GetInternalFlag())
      {
      vtkXMLObjectWriter *xmlw = snapshot->GetNewXMLWriter();
      xmlw->SetObject(snapshot);
      xmlw->CreateInElement(elem);
      xmlw->Delete();
      }
    }
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVSnapshotPoolWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

