/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVDataItemVolumeWriter - vtkVVDataItemVolume XML Writer.
// .SECTION Description
// vtkXMLVVDataItemVolumeWriter provides XML writing functionality to 
// vtkVVDataItemVolume.
// .SECTION See Also
// vtkXMLVVDataItemVolumeReader

#ifndef __vtkXMLVVDataItemVolumeWriter_h
#define __vtkXMLVVDataItemVolumeWriter_h

#include "XML/vtkXMLVVDataItemWriter.h"

class VTK_EXPORT vtkXMLVVDataItemVolumeWriter : public vtkXMLVVDataItemWriter
{
public:
  static vtkXMLVVDataItemVolumeWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVDataItemVolumeWriter,vtkXMLVVDataItemWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLVVDataItemVolumeWriter() {};
  ~vtkXMLVVDataItemVolumeWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVDataItemVolumeWriter(const vtkXMLVVDataItemVolumeWriter&);  // Not implemented.
  void operator=(const vtkXMLVVDataItemVolumeWriter&);  // Not implemented.
};

#endif

