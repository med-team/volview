/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSnapshot - a class that encapsulates a snapshot of the application
// .SECTION Description
// This class is used to store a snapshot of the application (as a serialized
// XML form).

#ifndef __vtkVVSnapshot_h
#define __vtkVVSnapshot_h

#include "vtkKWObject.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkXMLDataElement;
class vtkKWIcon;

class VTK_EXPORT vtkVVSnapshot : public vtkKWObject
{
public:
  static vtkVVSnapshot* New();
  vtkTypeRevisionMacro(vtkVVSnapshot,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Set/Get the description of the snapshot. 
  vtkSetStringMacro(Description);
  vtkGetStringMacro(Description);

  // Description:
  // Set/Get the pointer to the serialized XML form of the snapshot..
  vtkGetObjectMacro(SerializedForm, vtkXMLDataElement);  
  virtual void SetSerializedForm(vtkXMLDataElement *elem);

  // Description:
  // Set/Get the thumbnail of the snapshot. 
  vtkGetObjectMacro(Thumbnail, vtkKWIcon);  
  virtual void SetThumbnail(vtkKWIcon *elem);

  // Description:
  // Set/Get the screenshot of the snapshot. 
  vtkGetObjectMacro(Screenshot, vtkKWIcon);  
  virtual void SetScreenshot(vtkKWIcon *elem);

  // Description:
  // Set/Get if a snapshot is for internal use. Internal snapshots should
  // not be serialized.
  vtkBooleanMacro(InternalFlag, int);
  vtkSetMacro(InternalFlag, int);
  vtkGetMacro(InternalFlag, int);

protected:
  vtkVVSnapshot();
  ~vtkVVSnapshot();

  // Description:
  // Snapshot description
  char *Description;
  
  // Description:
  // Pointer to the snapshot serialized XML form.
  vtkXMLDataElement *SerializedForm;

  // Description:
  // Pointer to the snapshot thumbnail.
  vtkKWIcon *Thumbnail;

  // Description:
  // Pointer to the snapshot screenshot.
  vtkKWIcon *Screenshot;

  // Description:
  // Is the snapshot internal
  int InternalFlag;

private:
  vtkVVSnapshot(const vtkVVSnapshot&); // Not implemented
  void operator=(const vtkVVSnapshot&); // Not implemented
};

#endif
