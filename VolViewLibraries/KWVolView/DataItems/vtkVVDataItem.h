/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDataItem - an abstract class that encapsulates a single piece of data 
// .SECTION Description
// This is an abstract single piece of data, that is likely to be subclassed
// into a single volume (or a set of slices), or single polydata. Multiple
// vtkVVDataItem are usually stored in a vtkVVDataItemPool (an
// instance of that pool can be found in vtkVVWindowBase).

#ifndef __vtkVVDataItem_h
#define __vtkVVDataItem_h

#include "vtkKWObject.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkVVDataItemInternals;
class vtkVVWindowBase;
class vtkVVFileInstance;

class VTK_EXPORT vtkVVDataItem : public vtkKWObject
{
public:
  vtkTypeRevisionMacro(vtkVVDataItem,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Set/Get the name of the data item. 
  // As a rule of thumb, the data name should be unique enough that it can
  // be used efficiently to find the data inside a vtkVVDataItemPool. 
  // The filename to the data itself is a reasonable choice.
  // NOTE: this should be fixed, as a single file may lead to several data items
  // (retrieved from different Algorithm's ports for example, so one could
  // append the port number to the file name).
  vtkSetStringMacro(Name);
  vtkGetStringMacro(Name);

  // Description:
  // Set/Get the descriptive name of the data item. 
  // The descriptive name is used to provide a reasonable but short clue to 
  // the user about the data. Since the data 'Name' is usually the filename
  // to the data, subclasses are suggested to set the descriptive name
  // to something more meaningful, say the name of the patient if the data
  // item is a medical data.
  // If not set when GetDescriptiveName is called, it is set automatically
  // from the value of 'Name' by considering it a filename and removing
  // both the path and the extension.
  virtual const char *GetDescriptiveName();
  vtkSetStringMacro(DescriptiveName);

  // Description:
  // Set/Get the physical units for this dataset (e.g. mm, cm, inches)
  vtkSetStringMacro(DistanceUnits);
  vtkGetStringMacro(DistanceUnits);

  // Description:
  // Set/Get the scope of this data
  // Better match vtkKWOpenFileProperties::Scope. Though we do have a pointer
  // to a file instance, which may have a pointer to a open file properties,
  // some data item may be created programmatically without any file to
  // refer to.
  //BTX
  enum
  {
    ScopeUnknown = 0,
    ScopeMedical,
    ScopeScientific
  };
  //ETX
  vtkGetMacro(Scope, int);
  virtual void SetScope(int);
  virtual void SetScopeToUnknown();
  virtual void SetScopeToMedical();
  virtual void SetScopeToScientific();

  // Description:
  // Get the bounds of the data
  virtual void GetBounds(double bounds[6]) = 0;

  // Description:
  // Release data. Each subclass will implement specific methods to
  // set and allocate the proper data (say, image data/volume), but
  // this method should be implemented to release the data properly.
  virtual void ReleaseData() {};

  // Description:
  // Add/Remove default widgets to a window
  // For image/volume data, this will create the default 3D view, 2D views,
  // lightbox, etc.
  virtual void AddDefaultRenderWidgets(vtkVVWindowBase *) {};
  virtual void RemoveDefaultRenderWidgets(vtkVVWindowBase *) {};

  // Description:
  // Get number of renderwidgets (including all windows)
  virtual int GetNumberOfRenderWidgets() { return 0; };

  // Description:
  // Query if the data item has a representation (say a 2D or 3D view) in
  // a given window.
  virtual int HasRenderWidgetInWindow(vtkVVWindowBase *) { return 0; };
  
  // Description:
  // Update the default widgets annotations, if any
  virtual void UpdateRenderWidgetsAnnotations() {};

  // Description:
  // Set/Get the pointer to the file instance that data item may have been
  // loaded from (not all data items are created from files though).
  // Note that the FileInstance itself may store pointers to all the data items
  // that were created by loading it (see vtkVVFileInstance::DataItemPool).
  vtkGetObjectMacro(FileInstance, vtkVVFileInstance);  
  virtual void SetFileInstance(vtkVVFileInstance *instance);

protected:
  vtkVVDataItem();
  ~vtkVVDataItem();

  // Description:
  // Data item name
  char *Name;
  
  // Description:
  // Data item descriptive name
  char *DescriptiveName;
  
  // Description:
  // PIMPL Encapsulation for STL containers
  //BTX
  vtkVVDataItemInternals *Internals;
  //ETX

  // Description:
  // Distance units
  char *DistanceUnits;

  // Description:
  // Scope
  int Scope;

  // Description:
  // Pointer to the file instance that data item may have been
  // loaded from (not all data items are created from files though).
  vtkVVFileInstance *FileInstance;

private:
  vtkVVDataItem(const vtkVVDataItem&); // Not implemented
  void operator=(const vtkVVDataItem&); // Not implemented
};

#endif
