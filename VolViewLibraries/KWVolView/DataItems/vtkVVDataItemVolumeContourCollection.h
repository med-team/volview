/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDataItemVolumeContourCollection - maintain a list of contours
// .SECTION Description
// .SECTION See Also
// vtkCollection

#ifndef __vtkVVDataItemVolumeContourCollection_h
#define __vtkVVDataItemVolumeContourCollection_h

#include "vtkKWObject.h"

class vtkVVDataItemVolumeContour;
class vtkVVDataItemVolume;
class vtkCollection;

class VTK_EXPORT vtkVVDataItemVolumeContourCollection : public vtkKWObject
{
public:
  vtkTypeRevisionMacro(vtkVVDataItemVolumeContourCollection, vtkKWObject);
  static vtkVVDataItemVolumeContourCollection *New();
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the data on which all contours in this collection reside. 
  // Not ref counted, since the data item volume will reference count us.
  virtual void SetDataItemVolume( vtkVVDataItemVolume * );
  vtkGetObjectMacro( DataItemVolume, vtkVVDataItemVolume );
  
  // Description:
  // Add a contour to the list. Returns the added item.
  vtkVVDataItemVolumeContour *AddNewItem();

  // Description:
  // Add an item to the list.
  virtual void AddItem( vtkVVDataItemVolumeContour *a );
  
  // Description:
  // Get the number of contours.
  int GetNumberOfItems();
  
  // Description:
  // Get the nth contour in the list.
  vtkVVDataItemVolumeContour *GetNthItem( int i );

  // Description:
  // Remove the i'th item in the list.
  void RemoveItem(int i);

  // Description:
  // Remove an object from the list. 
  void RemoveItem(vtkVVDataItemVolumeContour *);

  // Description:
  // Remove all objects from the list.
  void RemoveAllItems();

  // Description:
  // Search for an object and return location in list. If the return value is
  // 0, the object was not found. If the object was found, the location is
  // the return value-1.
  int IsItemPresent(vtkVVDataItemVolumeContour *a);

  // Description:
  // Search by the description string identifier of the contours.
  int GetNumberOfContoursWithDescription( const char *description );
  vtkVVDataItemVolumeContour *GetNthContourWithDescription( int i, const char *description );

  // Description:
  // Check all the items in the collection for their MTTime.
  virtual unsigned long GetMTime();  

  // Description:
  // Enable garbage collection. There are ref counting cycles with 
  // vtkVVDataItemVolume
  virtual void Register(vtkObjectBase* o);
  virtual void UnRegister(vtkObjectBase* o);

protected:
  vtkVVDataItemVolumeContourCollection(); 
  ~vtkVVDataItemVolumeContourCollection();
  
  vtkCollection       * Collection;
  vtkVVDataItemVolume * DataItemVolume;

  // Report reference count loops. The dataitem refcounts us
  virtual void ReportReferences(vtkGarbageCollector* collector);

private:
  vtkVVDataItemVolumeContourCollection(const vtkVVDataItemVolumeContourCollection&);  // Not implemented.
  void operator=(const vtkVVDataItemVolumeContourCollection&);  // Not implemented.
};

#endif

