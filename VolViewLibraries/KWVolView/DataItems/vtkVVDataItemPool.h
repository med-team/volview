/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDataItemPool - a class that stores all the data item
// .SECTION Description
// This class stores a pool of data items (vtkVVDataItem). 

#ifndef __vtkVVDataItemPool_h
#define __vtkVVDataItemPool_h

#include "vtkKWObject.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkDataObject;
class vtkImageData;
class vtkVVDataItem;
class vtkVVDataItemPoolInternals;
class vtkCallbackCommand;

class VTK_EXPORT vtkVVDataItemPool : public vtkKWObject
{
public:
  static vtkVVDataItemPool* New();
  vtkTypeRevisionMacro(vtkVVDataItemPool,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Add a data item.
  // It is ref counted (and will be released by RemoveDataItem()).
  // Return 1 on success, 0 otherwise
  int AddDataItem(vtkVVDataItem*);
                 
  // Description:
  // Get number of data items stored so far
  virtual int GetNumberOfDataItems();
  
  // Description:
  // Query if data in the pool
  virtual int HasDataItemWithName(const char *name);
  virtual int HasDataItem(vtkVVDataItem *data);

  // Description:
  // Retrieve a given data item (i-th, or by name)
  vtkVVDataItem* GetNthDataItem(int i);
  vtkVVDataItem* GetDataItemWithName(const char *name);

  // Description::
  // Retrieve the index of the data based on it's name
  int GetIndexOfDataItemWithName(const char *name);
  int GetIndexOfDataItem(vtkVVDataItem *data);

  // Description:
  // Remove a given data item, or all of them
  virtual void RemoveDataItemWithName(const char *name);
  virtual void RemoveDataItem(vtkVVDataItem*);
  virtual void RemoveAllDataItems();

protected:
  vtkVVDataItemPool();
  ~vtkVVDataItemPool();

  // Description:
  // Initialize (or re-initialize) some internal data
  virtual void Initialize();

  // Description:
  // Get the callback command. 
  // Subclasses can override this method to set specific flags, like
  // the AbortFlagOnExecute flag.
  virtual vtkCallbackCommand* GetCallbackCommand();

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can override this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // Description:
  // PIMPL Encapsulation for STL containers
  vtkVVDataItemPoolInternals *Internals;

private:
  vtkVVDataItemPool(const vtkVVDataItemPool&); // Not implemented
  void operator=(const vtkVVDataItemPool&); // Not implemented
};

#endif
