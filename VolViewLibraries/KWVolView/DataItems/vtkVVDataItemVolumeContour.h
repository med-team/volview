/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVDataItemVolumeContour - a set of contours.
// .SECTION Description

#ifndef __vtkVVDataItemVolumeContour_h
#define __vtkVVDataItemVolumeContour_h

#include "vtkKWObject.h"
#include "vtkVVDataItemVolumeContourInternals.h"

class vtkVVDataItemVolume;
class vtkImageData;
class vtkContourFilter;
class vtkPolyDataConnectivityFilter;
class vtkMassProperties;
class vtkTriangleFilter;
class vtkFeatureEdges;
class vtkRECISTCalculator;

class VTK_EXPORT vtkVVDataItemVolumeContour : public vtkKWObject
{
public:

  static vtkVVDataItemVolumeContour* New();
  vtkTypeRevisionMacro(vtkVVDataItemVolumeContour, vtkKWObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Set/Get the data on which this contour resides. Not ref counted.
  virtual void SetDataItemVolume( vtkVVDataItemVolume * );
  vtkGetObjectMacro( DataItemVolume, vtkVVDataItemVolume );

  // Description:
  // Set the image to contour. This allows the user to contour either the
  // image he gets from this data item or from another dataitem, say a 
  // segmentation. Ref counted.
  virtual void SetImageData( vtkImageData * );
  vtkGetObjectMacro( ImageData, vtkImageData );

  // Description:
  // Set the Polydata that represents the contour directly. This is an 
  // alternative to the SetImageData method above. You should not use both
  // methods. Ref counted.
  virtual void SetPolyData( vtkPolyData * );
  virtual vtkPolyData * GetPolyData();
  
  // Description:
  // Close out and remove any actors prior to deletion.
  virtual void Close();

  // Description:
  // Set the isovalue for the contour
  vtkSetMacro( IsoValue, double );
  vtkGetMacro( IsoValue, double );

  // Description:
  // Visibility
  vtkSetMacro( Visibility, int );
  vtkGetMacro( Visibility, int );
  vtkBooleanMacro( Visibility, int );

  // Description:
  // Placeholder for a textual description for the contour. The application
  // may or may not use this.
  vtkSetStringMacro( Description );
  vtkGetStringMacro( Description );

  // Description:
  // Get the volume/surface area. Make sure you've called ComputeStatistics 
  // first. Volume and surface area are defined only if the surface is simple.
  virtual double GetVolume();
  virtual double GetSurfaceArea();
  int IsSimpleSurface();
  int GetNumberOfSurfaces();

  // Description:
  // Get the bounds of the contour.
  void GetContourBounds( double bounds[6] );

  // Description:
  // Change contour color / opacity
  vtkSetVector3Macro( Color, double );
  vtkGetVector3Macro( Color, double );
  vtkSetMacro( Opacity, double );
  vtkGetMacro( Opacity, double );
  vtkSetMacro( LineWidth, double );
  vtkGetMacro( LineWidth, double );

  // Description:
  // Compute the RECIST (axial unidimensional equivalent) length for this
  // contour. If true, the RECISTMeasure is computed
  vtkSetMacro( ComputeRECISTMeasure, int );
  vtkGetMacro( ComputeRECISTMeasure, int );
  vtkBooleanMacro( ComputeRECISTMeasure, int );
  vtkGetMacro( RECISTMeasure, double );

  // Description:
  // Compute contour statistics .
  virtual void ComputeStatistics();

  // Description:
  // Update. Checks build time etc. This does not compute statistics. You 
  // must explicitly call compute statistics if you need statistics.
  virtual void Update();

  // Description:
  // Force a Render() for this contour on all render widgets for this contour
  virtual void Render();
  
protected:
  vtkVVDataItemVolumeContour();
  ~vtkVVDataItemVolumeContour();

  // Add/remove actors from the all the render widgets that display this data
  // item. Called automatically from SetDataItemVolume(..)
  virtual void AddActors();
  virtual void RemoveActors();

  // Description:
  virtual void AddCallbackCommandObservers();
  virtual void RemoveCallbackCommandObservers();

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);
  
  vtkImageData         * ImageData;
  vtkPolyData          * PolyData;
  vtkVVDataItemVolume  * DataItemVolume;
  vtkContourFilter     * ContourFilter;
  double                 Opacity;
  double                 Color[3];
  double                 LineWidth;
  double                 IsoValue;
  int                    Visibility;
  int                    ComputeRECISTMeasure;
  vtkTimeStamp           BuildTime;

  // Filters to compute statitics
  vtkPolyDataConnectivityFilter *Connectivity;
  vtkFeatureEdges               *FeatureEdges;
  vtkMassProperties             *MassProperties;
  vtkTriangleFilter             *TriangleFilter;
  int                            SimpleSurface;
  int                            NumberOfSurfaces;
  double                         Volume;
  double                         SurfaceArea;
  char                          *Description;
  vtkRECISTCalculator           *RECISTCalculator;
  double                         RECISTMeasure;

  vtkVVDataItemVolumeContourInternals * Internals;

private:
  vtkVVDataItemVolumeContour(const vtkVVDataItemVolumeContour&); // Not implemented
  void operator=(const vtkVVDataItemVolumeContour&); // Not implemented
};

#endif

