/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVFileAuthenticator - Authenticates a file instance to check if loading it is supported.
// .SECTION Description

#ifndef __vtkVVFileAuthenticator_h
#define __vtkVVFileAuthenticator_h

#include "vtkKWObject.h"

class vtkVVFileInstance;
class vtkKWDataTransfer;

class VTK_EXPORT vtkVVFileAuthenticator : public vtkKWObject
{
public:
  static vtkVVFileAuthenticator * New();
  vtkTypeRevisionMacro(vtkVVFileAuthenticator,vtkKWObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the file instance.
  // Not reference counted to avoid cycles.
  virtual void SetFileInstance(vtkVVFileInstance *);
  vtkGetObjectMacro(FileInstance, vtkVVFileInstance);

  // Description:
  // Are we allowed to load the supplied file ?
  // The default implementation here returns 1. Subclasses that wish to
  // enforce constraints on file loading via licensing schemens etc will 
  // wish to override this.
  //   Returns 1 if the file can be loaded.
  //           0 if the file cannot be loaded. In this case, the "ReasonString"
  //             is populated with a textual description of an appropriate 
  //             reason.
  virtual int AuthenticateFile(const char *file);

  // Description:
  // Reason why "Authenticate.." failed may be queried. Needless to say, this
  // method must be called only after calling one of the Authenticate methods.
  vtkGetStringMacro(ReasonString);

  // Description:
  // Get the public key. A key may be specified on a per application basis.
  // See PublicKeyFile
  virtual const char *GetPublicKey();

  // Description:
  // Set/Get the public key file where the public key will be extracted from
  // (if not set already). This is a *relative* path, that will be used to
  // search from the application's InstallationDirectory 
  // (see vtkKWApplication::InstallationDirectory) and the UserDataDirectory/
  // It can include a subdirector (still relative).
  // Defaults to "Authentication/license.pub"
  //BTX
  vtkSetStringMacro(PublicKeyFile);
  vtkGetStringMacro(PublicKeyFile);
  //ETX

protected:
  vtkVVFileAuthenticator();
  ~vtkVVFileAuthenticator();

  vtkVVFileInstance * FileInstance;

  char *ReasonString;
  vtkSetStringMacro(ReasonString);

  char *PublicKeyFile;

  // Description:
  // The public key
  vtkSetStringMacro(PublicKey);
  char *PublicKey;  

private:
  vtkVVFileAuthenticator(const vtkVVFileAuthenticator&); // Not implemented
  void operator=(const vtkVVFileAuthenticator&); // Not implemented
};

#endif
