/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVMD5FileAuthenticator - Authenticates a file instance to check if loading it is supported.
// .SECTION Description

#ifndef __vtkVVMD5FileAuthenticator_h
#define __vtkVVMD5FileAuthenticator_h

#include "vtkVVFileAuthenticator.h"

class vtkVVFileInstance;
class vtkKWDataTransfer;

class VTK_EXPORT vtkVVMD5FileAuthenticator : public vtkVVFileAuthenticator
{
public:
  static vtkVVMD5FileAuthenticator * New();
  vtkTypeRevisionMacro(vtkVVMD5FileAuthenticator,vtkVVFileAuthenticator);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Are we allowed to load the supplied file ?
  // The default implementation here returns 1. Subclasses that wish to
  // enforce constraints on file loading via licensing schemens etc will 
  // wish to override this.
  //   Returns 1 if the file can be loaded.
  //           0 if the file cannot be loaded. In this case, the "ReasonString"
  //             is populated with a textual description of an appropriate 
  //             reason.
  virtual int AuthenticateFile( const char *file );

protected:
  vtkVVMD5FileAuthenticator();
  ~vtkVVMD5FileAuthenticator();

private:
  vtkVVMD5FileAuthenticator(const vtkVVMD5FileAuthenticator&); // Not implemented
  void operator=(const vtkVVMD5FileAuthenticator&); // Not implemented
};

#endif
