/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkRECISTCalculator - 
#ifndef __vtkRECISTCalculator_h
#define __vtkRECISTCalculator_h

#include "vtkPolyDataAlgorithm.h"

class vtkImageData;

class VTK_EXPORT vtkRECISTCalculator : public vtkPolyDataAlgorithm
{
public:
  // Description:
  // Constructs with initial values of zero.
  static vtkRECISTCalculator *New();

  vtkTypeMacro(vtkRECISTCalculator,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Compute and return the volume.
  double GetRECISTMeasure() {this->Update(); return this->RECISTMeasure;}

  virtual void SetImageData( vtkImageData * );
  vtkImageData * GetImageData();

protected:
  vtkRECISTCalculator();
  ~vtkRECISTCalculator();

  virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);
  virtual int FillInputPortInformation(int port, vtkInformation *info);
  virtual int RequestUpdateExtent(vtkInformation*,
                                  vtkInformationVector**,
                                  vtkInformationVector*);

  double  RECISTMeasure;

private:
  vtkRECISTCalculator(const vtkRECISTCalculator&);  // Not implemented.
  void operator=(const vtkRECISTCalculator&);  // Not implemented.
};

#endif

