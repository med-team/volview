/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVDataItem.h"

#include "vtkObjectFactory.h"
#include <vtksys/SystemTools.hxx>
#include "vtkVVFileInstance.h"

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLVVDataItemReader.h"
#include "XML/vtkXMLVVDataItemWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkVVDataItem, vtkXMLVVDataItemReader, vtkXMLVVDataItemWriter);

//----------------------------------------------------------------------------
vtkCxxRevisionMacro(vtkVVDataItem, "$Revision: 1.11 $");

vtkCxxSetObjectMacro(vtkVVDataItem, FileInstance, vtkVVFileInstance);

//----------------------------------------------------------------------------
class vtkVVDataItemInternals
{
public:

};

//----------------------------------------------------------------------------
vtkVVDataItem::vtkVVDataItem()
{ 
  this->Internals       = new vtkVVDataItemInternals;
  this->Name            = NULL;
  this->DescriptiveName = NULL;
  this->DistanceUnits   = NULL;
  this->FileInstance    = NULL;
  this->Scope           = vtkVVDataItem::ScopeUnknown;
}
  
//----------------------------------------------------------------------------
vtkVVDataItem::~vtkVVDataItem()
{
  this->ReleaseData();

  // Delete our pool

  if (this->Internals)
    {
    delete this->Internals;
    }

  this->SetName(NULL);
  this->SetDescriptiveName(NULL);
  this->SetDistanceUnits(NULL);
  this->SetFileInstance(NULL);
}

//----------------------------------------------------------------------------
const char* vtkVVDataItem::GetDescriptiveName()
{
  if (!this->DescriptiveName && this->Name)
    {
    vtksys_stl::string short_data_name = 
      vtksys::SystemTools::GetFilenameName(this->Name);
    this->SetDescriptiveName(short_data_name.c_str());
    }

  return this->DescriptiveName;
}

//----------------------------------------------------------------------------
void vtkVVDataItem::SetScope(int arg)
{
  if (arg < vtkVVDataItem::ScopeUnknown ||
      arg > vtkVVDataItem::ScopeScientific ||
      this->Scope == arg)
    {
    return;
    }

  this->Scope = arg;
  this->Modified();
}

void vtkVVDataItem::SetScopeToUnknown()
{ 
  this->SetScope(vtkVVDataItem::ScopeUnknown); 
}

void vtkVVDataItem::SetScopeToMedical()
{ 
  this->SetScope(vtkVVDataItem::ScopeMedical); 
}

void vtkVVDataItem::SetScopeToScientific()
{ 
  this->SetScope(vtkVVDataItem::ScopeScientific); 
}

//----------------------------------------------------------------------------
void vtkVVDataItem::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  
  os << indent << "Name: " 
     << (this->Name ? this->Name : "(NULL)") << endl;
  os << indent << "DescriptiveName: " 
     << (this->DescriptiveName ? this->DescriptiveName : "(NULL)") << endl;
  os << indent << "DistanceUnits: " 
     << (this->DistanceUnits ? this->DistanceUnits : "(NULL)") << endl;
  os << indent << "Scope: " << this->Scope << endl;
}
