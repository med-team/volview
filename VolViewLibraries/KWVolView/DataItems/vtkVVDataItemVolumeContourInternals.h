/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef __vtkVVDataItemVolumeContourInternals_h
#define __vtkVVDataItemVolumeContourInternals_h

#include "vtkSmartPointer.h"
#include "vtkActor.h"
#include "vtkCutter.h"
#include "vtkPolyDataMapper.h"
#include "vtkPolyData.h"
#include "vtkPlane.h"
#include "vtkProperty.h"

class vtkVVDataItemVolumeContourInternals
{
public:
  vtkSmartPointer< vtkActor >          VolumeActor;
  vtkSmartPointer< vtkActor >          SliceActor[3];
  vtkSmartPointer< vtkActor >          ObliqueActor;
  vtkSmartPointer< vtkCutter >         ObliquePlaneCutter;
  vtkSmartPointer< vtkCutter >         SlicePlaneCutter[3];
  vtkSmartPointer< vtkPlane >          ObliquePlane;
  vtkSmartPointer< vtkPlane >          SlicePlane[3];
  vtkSmartPointer< vtkPolyData >       Contour;
  vtkSmartPointer< vtkPolyDataMapper > VolumeMapper;
  vtkSmartPointer< vtkPolyDataMapper > SliceMapper[3];
  vtkSmartPointer< vtkPolyDataMapper > ObliqueMapper;

  vtkVVDataItemVolumeContourInternals()
    {
    VolumeActor         = vtkSmartPointer< vtkActor >::New();
    ObliqueActor        = vtkSmartPointer< vtkActor >::New();
    VolumeMapper        = vtkSmartPointer< vtkPolyDataMapper >::New();
    ObliqueMapper       = vtkSmartPointer< vtkPolyDataMapper >::New();
    ObliquePlaneCutter  = vtkSmartPointer< vtkCutter >::New();
    ObliquePlane        = vtkSmartPointer< vtkCutter >::New();
    VolumeMapper->ScalarVisibilityOff();
    ObliqueMapper->ScalarVisibilityOff();
    ObliqueMapper->SetResolveCoincidentTopologyToPolygonOffset();
    VolumeActor->SetMapper( VolumeMapper );
    ObliqueActor->SetMapper( ObliqueMapper );  
    ObliquePlaneCutter->SetCutFunction(ObliquePlane);

    for (int i = 0; i < 3; i++)
      {
      SliceActor[i]       = vtkSmartPointer< vtkActor >::New();
      SliceMapper[i]      = vtkSmartPointer< vtkPolyDataMapper >::New();
      SliceMapper[i]->ScalarVisibilityOff();
      SliceMapper[i]->SetResolveCoincidentTopologyToPolygonOffset();
      SlicePlaneCutter[i] = vtkSmartPointer< vtkCutter >::New();
      SlicePlane[i]       = vtkSmartPointer< vtkPlane >::New();
      SliceActor[i]->SetMapper( SliceMapper[i] );
      SlicePlaneCutter[i]->SetCutFunction(SlicePlane[i]);
      SliceActor[i]->GetProperty()->SetInterpolationToFlat();
      }
    }
};

#endif

