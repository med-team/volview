/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSaveVolume - Save out the volume from image data
// .SECTION Description
// A simple class that writes out the volume, by picking the right
// writer class.

#ifndef __vtkVVSaveVolume_h
#define __vtkVVSaveVolume_h

#include "vtkKWObject.h"

class vtkImageWriter;
class vtkKWWindow;
class vtkVVDataItemVolume;
class vtkAlgorithm;

class VTK_EXPORT vtkVVSaveVolume : public vtkKWObject
{
public:
  static vtkVVSaveVolume* New();
  vtkTypeMacro(vtkVVSaveVolume,vtkKWObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Save the file
  int Write();

  // Description:
  // Set/Get the filename
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);

  // Description:
  // Set/Get the data item volume that has to be saved.
  virtual void SetDataItemVolume(vtkVVDataItemVolume*);
  vtkGetObjectMacro(DataItemVolume, vtkVVDataItemVolume);
  
  // Description:
  // Set the parent window.
  virtual void SetWindow(vtkKWWindow *);

protected:
  vtkVVSaveVolume();
  ~vtkVVSaveVolume();

  // Description:
  // When writing as sequence of images, use this method to 
  // perform all the necessary checking, deleting of files and 
  // making of sequence.
  // If pattern is not NULL, the file pattern that was used to write
  // the series will be written to that location.
  int WriteImages(const char* file, char *filepattern = 0);

  // Description:
  // Ask and delete the series.
  int AskAndDeleteSeries(const char* filename, 
                         const char* pattern,
                         int zmin, int zmax);

  // Description:
  // Determine the format of the image and set writer object.
  int InstantiateWriter(const char* file);

  vtkVVDataItemVolume *DataItemVolume;
  char *FileName;
  vtkAlgorithm   *Writer;
  vtkImageWriter *ImageWriter;
  vtkKWWindow *Window;
  int IsImage;

private:
  vtkVVSaveVolume(const vtkVVSaveVolume&); // Not implemented
  void operator=(const vtkVVSaveVolume&); // Not implemented
};


#endif



