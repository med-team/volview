/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWApplication.h"
#include "vtkVVSaveDialog.h"
#include "vtkObjectFactory.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWLoadSaveDialog.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSaveDialog);

//----------------------------------------------------------------------------
vtkVVSaveDialog::vtkVVSaveDialog()
{
  this->SaveDialog = 1;
  this->SetTitle("Save Volume");
  // * The TIFF writer seems to work, but when loading the new files
  //   the app will crash on exit, HEAD corruption. Orientation is flaky too.
  // * Wo reads PGM nowadays..
  this->SetFileTypes("{{VTK XML Format} {.vti}} "
                     "{{MetaImage (compressed)} {.mha}} "
                     "{{MetaImage (un-compressed)} {.mhd}} "
                     "{{Series of BMP} {.bmp}} "
        //           "{{Series of TIFF} {.tif}} "
                     "{{Series of JPEG} {.jpg}} "
        //           "{{Series of PGM} {.pgm}} "
                     "{{Series of PNG} {.png}} "
                     "{{Raw format} {.raw}}");
  this->SetDefaultExtension(".vti");
}
