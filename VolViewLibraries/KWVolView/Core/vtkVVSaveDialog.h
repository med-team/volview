/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSaveDialog - a dialog for saving VolView data
// .SECTION Description
// A simple dialog for saving VolView data.

#ifndef __vtkVVSaveDialog_h
#define __vtkVVSaveDialog_h

#include "vtkKWLoadSaveDialog.h"

class VTK_EXPORT vtkVVSaveDialog : public vtkKWLoadSaveDialog
{
public:
  static vtkVVSaveDialog* New();
  vtkTypeMacro(vtkVVSaveDialog,vtkKWLoadSaveDialog);

protected:
  vtkVVSaveDialog();
  ~vtkVVSaveDialog() {};

private:
  vtkVVSaveDialog(const vtkVVSaveDialog&); // Not implemented
  void operator=(const vtkVVSaveDialog&); // Not implemented
};


#endif



