/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#define VTK_WINDOWS_FULL
#include "vtkVVWindow.h"

#include "vtkObjectFactory.h"
#include "vtkImageData.h"

#include "vtkKW2DRenderWidget.h"
#include "vtkKWEvent.h"
#include "vtkKWFrame.h"
#include "vtkKWIcon.h"
#include "vtkKWImageWidget.h"
#include "vtkKWInternationalization.h"
#include "vtkKWLabel.h"
#include "vtkKWLightboxWidget.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWMostRecentFilesManager.h"
#include "vtkKWNotebook.h"
#include "vtkKWOpenWizard.h"
#include "vtkKWOptions.h"
#include "vtkKWProbeImageWidget.h"
#include "vtkKWPushButton.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRenderWidget.h"
#include "vtkKWSplashScreen.h"
#include "vtkKWSplitFrame.h"
#include "vtkKWTkUtilities.h"
#include "vtkKWToolbar.h"
#include "vtkKWToolbarSet.h"
#include "vtkKWUserInterfaceManagerNotebook.h"
#include "vtkKWVolumeWidget.h"
#include "vtkKWRemoteIOManager.h"
#include "vtkKWCacheManager.h"

#include "vtkVVApplication.h"
#include "vtkVVApplicationSettingsInterface.h"
#include "vtkVVDataItemVolume.h"
#include "vtkVVDataItemPool.h"
#include "vtkVVInformationInterface.h"
#include "vtkVVAdvancedAlgorithmsInterface.h"
#include "vtkVVDisplayInterface.h"
#include "vtkVVReviewInterface.h"
#include "vtkVVSelectionFrame.h"
#include "vtkVVSelectionFrameLayoutManager.h"
#include "vtkVVSnapshotPresetSelector.h"
#include "vtkVVPluginInterface.h"

#ifdef KWVolView_USE_LESION_SIZING_KIT  
#include "vtkVVLesionSizingInterface.h"
#endif


//#include "vtkKWVolViewConfigure.h"
#include "vtkKWWidgetsProConfigure.h"

#include <vtksys/stl/string>
#include <vtksys/SystemTools.hxx>

#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
#include "vtkVVWidgetInterface.h"
#include "vtkVVInteractorWidgetSelector.h" 
#endif

#include "vtkVVWindowLayout.h"

//---------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVWindow);
vtkCxxRevisionMacro(vtkVVWindow, "$Revision: 1.94 $");

//----------------------------------------------------------------------------
vtkVVWindow::vtkVVWindow()
{  
  // User Interface Panels

  this->InformationInterface = NULL;
  this->DisplayInterface     = NULL;
  this->WidgetInterface     = NULL;
  this->ReviewInterface      = NULL;
  this->PluginInterface      = NULL;
  this->AdvancedAlgorithmsInterface = NULL;
  this->LesionSizingInterface = NULL;
  this->SupportPlugins = 0;
  this->SupportLesionSizingInterface = 1;
}

//----------------------------------------------------------------------------
vtkVVWindow::~vtkVVWindow()
{
  // First delete the User Interface Panels

  if (this->InformationInterface)
    {
    this->InformationInterface->Delete();
    this->InformationInterface = NULL;
    }

  if (this->DisplayInterface)
    {
    this->DisplayInterface->Delete();
    this->DisplayInterface = NULL;
    }

#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
  if (this->WidgetInterface)
    {
    this->WidgetInterface->Delete();
    this->WidgetInterface = NULL;
    }
#endif

  if (this->ReviewInterface)
    {
    this->ReviewInterface->Delete();
    this->ReviewInterface = NULL;
    }

  
  if (this->PluginInterface)
    {
    this->PluginInterface->Delete();
    this->PluginInterface = NULL;
    }

  if (this->AdvancedAlgorithmsInterface)
    {
    this->AdvancedAlgorithmsInterface->Delete();
    this->AdvancedAlgorithmsInterface = NULL;
    }

#ifdef KWVolView_USE_LESION_SIZING_KIT  
  if (this->LesionSizingInterface)
    {
    this->LesionSizingInterface->Delete();
    }
#endif

}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateWidget()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("widget already created " << this->GetClassName());
    return;
    }

  this->Superclass::CreateWidget();

  // Create the UI panels

  this->CreateUserInterfacePanels();

  this->Update();

  this->DeIconify();

}

//----------------------------------------------------------------------------
int vtkVVWindow::Close()
{
  return this->Superclass::Close();
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateDisplayInterface()
{
  if (this->DisplayInterface)
    {
    return 0;
    }

  this->DisplayInterface = vtkVVDisplayInterface::New();
  this->DisplayInterface->SetUserInterfaceManager(
    this->GetMainUserInterfaceManager());
  this->DisplayInterface->SetWindow(this);

  //this->DisplayInterface->Raise();
  this->DisplayInterface->Show();
  return 1;
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateWidgetInterface()
{
#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
  if (this->WidgetInterface)
    {
    return 0;
    }
  this->WidgetInterface = vtkVVWidgetInterface::New();
  this->WidgetInterface->SetUserInterfaceManager(
    this->GetMainUserInterfaceManager());
  this->WidgetInterface->SetWindow(this);
  this->WidgetInterface->Show();
  return 1;
#else
  return 0;
#endif
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateReviewInterface()
{
  if (this->ReviewInterface)
    {
    return 0;
    }

  this->ReviewInterface = vtkVVReviewInterface::New();
  this->ReviewInterface->SetUserInterfaceManager(
    this->GetMainUserInterfaceManager());
  this->ReviewInterface->SetWindow(this);
  this->ReviewInterface->Show();
  return 1;
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreatePluginInterface()
{
  if (this->PluginInterface || !this->SupportPlugins)
    {
    return 0;
    }

  this->PluginInterface = vtkVVPluginInterface::New();
  this->PluginInterface->SetUserInterfaceManager(
    this->GetMainUserInterfaceManager());
  this->PluginInterface->SetWindow(this);
  this->PluginInterface->Show();  
  return 1;
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateAdvancedAlgorithmsInterface()
{
  if (this->AdvancedAlgorithmsInterface)
    {
    return 0;
    }

  this->AdvancedAlgorithmsInterface = vtkVVAdvancedAlgorithmsInterface::New();
  this->AdvancedAlgorithmsInterface->SetUserInterfaceManager(
    //      this->GetSecondaryUserInterfaceManager());
    this->GetMainUserInterfaceManager());
  this->AdvancedAlgorithmsInterface->SetWindow(this);
  //is->AdvancedAlgorithmsInterface->Show();
  this->AdvancedAlgorithmsInterface->Raise();
  return 1;
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateInformationInterface()
{
  if (this->InformationInterface)
    {
    return 0;
    }

  this->InformationInterface = vtkVVInformationInterface::New();
  this->InformationInterface->SetUserInterfaceManager(
    //      this->GetSecondaryUserInterfaceManager());
    this->GetMainUserInterfaceManager());
  this->InformationInterface->SetWindow(this);
  //is->InformationInterface->Show();
  this->InformationInterface->Raise();
  return 1;
}

//----------------------------------------------------------------------------
int vtkVVWindow::CreateLesionSizingInterface()
{
#ifdef KWVolView_USE_LESION_SIZING_KIT  
  if (this->LesionSizingInterface || !this->SupportLesionSizingInterface)
    {
    return 0;
    }

  this->LesionSizingInterface = vtkVVLesionSizingInterface::New();
  this->LesionSizingInterface->SetUserInterfaceManager(
    this->GetAdvancedAlgorithmsInterface()->GetUserInterfaceManager());
  this->LesionSizingInterface->SetWindow(this);
  this->LesionSizingInterface->Show();
  return 1;
#else
  return 0;
#endif
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateUserInterfacePanels()
{
  this->GetSecondaryNotebook()->AlwaysShowTabsOff();
  this->CreateDisplayInterface();
  this->CreateWidgetInterface();
  this->CreateReviewInterface();
  this->CreatePluginInterface();
  this->CreateAdvancedAlgorithmsInterface();
  this->CreateInformationInterface();
  this->CreateLesionSizingInterface();
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateToolbars()
{
  // Create the user interface panels first.. 
  // Some of our toolbars are queried from the UI panels, such as the 
  // measurement toolbar.

  this->CreateUserInterfacePanels();

  this->Superclass::CreateToolbars();

  this->CreateMeasurementToolbar();

  this->CreateSnapshotToolbar();
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateQuickViewToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  this->Superclass::CreateQuickViewToolbar();

  vtkKWPushButton *pb;

  // Toolbar : quick view
  
  if (!this->GetMainToolbarSet()->HasToolbar(this->QuickViewToolbar))
    {  
    this->GetMainToolbarSet()->AddToolbar(this->QuickViewToolbar);
    }

  pb = vtkKWPushButton::New();
  pb->SetParent(this->QuickViewToolbar->GetFrame());
  pb->Create();
  pb->SetCommand(this, "QuickViewImageCallback");
  pb->SetBalloonHelpString(
    ks_("Toolbar|Quick View|Set view to image"));
  pb->SetText("QuickViewImage");
  pb->SetImageToPixels(image_VVLayout1Stack2D, 
                       image_VVLayout1Stack2D_width, 
                       image_VVLayout1Stack2D_height,
                       image_VVLayout1Stack2D_pixel_size,
                       image_VVLayout1Stack2D_length);
  this->QuickViewToolbar->AddWidget(pb);
  pb->Delete();

  if (this->SupportVolumeWidget)
    {
    pb = vtkKWPushButton::New();
    pb->SetParent(this->QuickViewToolbar->GetFrame());
    pb->Create();
    pb->SetBalloonHelpString(
      ks_("Toolbar|Quick View|Set view to volume"));
    pb->SetCommand(this, "QuickViewVolumeCallback");
    pb->SetText("QuickViewVolume");
    pb->SetImageToPixels(image_VVLayout1Volume3D, 
                         image_VVLayout1Volume3D_width, 
                         image_VVLayout1Volume3D_height,
                         image_VVLayout1Volume3D_pixel_size,
                         image_VVLayout1Volume3D_length);
    this->QuickViewToolbar->AddWidget(pb);
    pb->Delete();
    }
    
  if (this->SupportLightboxWidget)
    {
    pb = vtkKWPushButton::New();
    pb->SetParent(this->QuickViewToolbar->GetFrame());
    pb->Create();
    pb->SetBalloonHelpString(
      ks_("Toolbar|Quick View|Set view to lightbox"));
    pb->SetCommand(this, "QuickViewLightboxCallback");
    pb->SetText("QuickViewLightbox");
    pb->SetImageToPixels(image_VVLayout1Lightbox, 
                         image_VVLayout1Lightbox_width, 
                         image_VVLayout1Lightbox_height,
                         image_VVLayout1Lightbox_pixel_size,
                         image_VVLayout1Lightbox_length);
    this->QuickViewToolbar->AddWidget(pb);
    pb->Delete();
    }
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateLayoutManagerToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  this->Superclass::CreateLayoutManagerToolbar();

  // Layout manager
   
  if (!this->GetMainToolbarSet()->HasToolbar(
        this->GetDataSetWidgetLayoutManager()->GetResolutionEntriesToolbar()))
    {  
    this->GetMainToolbarSet()->AddToolbar(
      this->GetDataSetWidgetLayoutManager()->GetResolutionEntriesToolbar());
    }
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateInteractionMode2DToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  this->Superclass::CreateInteractionMode2DToolbar();

  vtkKWRadioButton *rb;
  vtkKWLabel *label;

  const char *varname;
  char command[1024];

  // Toolbar : interaction mode 2D

  if (!this->GetMainToolbarSet()->HasToolbar(this->InteractionMode2DToolbar))
    {  
    this->GetMainToolbarSet()->AddToolbar(this->InteractionMode2DToolbar);  
    }

  label = vtkKWLabel::New();
  label->SetParent(this->InteractionMode2DToolbar->GetFrame());
  label->Create();
  label->SetText(ks_("Toolbar|2D Interaction|2D:"));
  this->InteractionMode2DToolbar->AddWidget(label);
  label->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
  rb->Create();
  rb->SetValueAsInt(vtkKW2DRenderWidget::INTERACTION_MODE_WINDOWLEVEL);
  varname = rb->GetVariableName();
  rb->SetVariableValueAsInt(rb->GetValueAsInt());
  sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|2D Interaction|Set 2D interaction mode to Window/Level"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconContrast);
  rb->SetText("Window/Level");
  this->InteractionMode2DToolbar->AddWidget(rb);
  rb->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
  rb->Create();
  rb->SetVariableName(varname);
  rb->SetValueAsInt(vtkKW2DRenderWidget::INTERACTION_MODE_PAN);
  sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|2D Interaction|Set 2D interaction mode to Pan"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconPanHand);
  rb->SetText("Pan");
  this->InteractionMode2DToolbar->AddWidget(rb);
  rb->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
  rb->Create();
  rb->SetVariableName(varname);
  rb->SetValueAsInt(vtkKW2DRenderWidget::INTERACTION_MODE_ZOOM);
  sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|2D Interaction|Set 2D interaction mode to Zoom"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconNuvola16x16ActionsViewMag);
  rb->SetText("Zoom");
  this->InteractionMode2DToolbar->AddWidget(rb);
  rb->Delete();

  if (this->SupportObliqueProbeWidget)
    {
    rb = vtkKWRadioButton::New();
    rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
    rb->Create();
    rb->SetVariableName(varname);
    rb->SetValueAsInt(vtkKWProbeImageWidget::INTERACTION_MODE_ROLL);
    sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
    rb->SetCommand(this, command);
    rb->SetBalloonHelpString(
      ks_("Toolbar|2D Interaction|Set 2D interaction mode to Rotate"));
    rb->IndicatorVisibilityOff();
    rb->SetImageToPredefinedIcon(
      vtkKWIcon::IconCrystalProject16x16ActionsRotate);
    rb->SetText("Rotate");
    this->InteractionMode2DToolbar->AddWidget(rb);
    rb->Delete();

    rb = vtkKWRadioButton::New();
    rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
    rb->Create();
    rb->SetVariableName(varname);
    rb->SetValueAsInt(vtkKWProbeImageWidget::INTERACTION_MODE_RESLICE);
    sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
    rb->SetCommand(this, command);
    rb->SetBalloonHelpString(
      ks_("Toolbar|2D Interaction|Set 2D interaction mode to Reslice"));
    rb->IndicatorVisibilityOff();
    rb->SetImageToPredefinedIcon(vtkKWIcon::IconObliqueProbe);
    rb->SetText("Reslice");
    this->InteractionMode2DToolbar->AddWidget(rb);
    rb->Delete();

    rb = vtkKWRadioButton::New();
    rb->SetParent(this->InteractionMode2DToolbar->GetFrame());
    rb->Create();
    rb->SetVariableName(varname);
    rb->SetValueAsInt(vtkKWProbeImageWidget::INTERACTION_MODE_TRANSLATE);
    sprintf(command, "InteractionMode2DCallback %d", rb->GetValueAsInt());
    rb->SetCommand(this, command);
    rb->SetBalloonHelpString(
      ks_("Toolbar|2D Interaction|Set 2D interaction mode to Translate"));
    rb->IndicatorVisibilityOff();
    rb->SetImageToPredefinedIcon(vtkKWIcon::IconMoveV);
    rb->SetText("Translate");
    this->InteractionMode2DToolbar->AddWidget(rb);
    rb->Delete();
    }
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateInteractionMode3DToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  this->Superclass::CreateInteractionMode3DToolbar();

  if (!this->SupportVolumeWidget)
    {
    return;
    }

  vtkKWRadioButton *rb;
  vtkKWLabel *label;

  const char *varname;
  char command[1024];

  // Toolbar : interaction mode 3D

  if (!this->GetMainToolbarSet()->HasToolbar(this->InteractionMode3DToolbar))
    {  
    this->GetMainToolbarSet()->AddToolbar(this->InteractionMode3DToolbar);  
    }
  
  label = vtkKWLabel::New();
  label->SetParent(this->InteractionMode3DToolbar->GetFrame());
  label->Create();
  label->SetText(
    ks_("Toolbar|3D Interaction|3D:"));
  this->InteractionMode3DToolbar->AddWidget(label);
  label->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode3DToolbar->GetFrame());
  rb->Create();
  rb->SetValueAsInt(vtkKWVolumeWidget::INTERACTION_MODE_ROTATE);
  varname = rb->GetVariableName();
  rb->SetVariableValueAsInt(rb->GetValueAsInt());
  sprintf(command, "InteractionMode3DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|3D Interaction|Set 3D interaction mode to Rotate"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconCrystalProject16x16ActionsRotate);
  rb->SetText("Rotate");
  this->InteractionMode3DToolbar->AddWidget(rb);
  rb->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode3DToolbar->GetFrame());
  rb->Create();
  rb->SetVariableName(varname);
  rb->SetValueAsInt(vtkKWVolumeWidget::INTERACTION_MODE_PAN);
  sprintf(command, "InteractionMode3DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|3D Interaction|Set 3D interaction mode to Pan"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconPanHand);
  rb->SetText("Pan");
  this->InteractionMode3DToolbar->AddWidget(rb);
  rb->Delete();

  rb = vtkKWRadioButton::New();
  rb->SetParent(this->InteractionMode3DToolbar->GetFrame());
  rb->Create();
  rb->SetVariableName(varname);
  rb->SetValueAsInt(vtkKWVolumeWidget::INTERACTION_MODE_ZOOM);
  sprintf(command, "InteractionMode3DCallback %d", rb->GetValueAsInt());
  rb->SetCommand(this, command);
  rb->SetBalloonHelpString(
    ks_("Toolbar|3D Interaction|Set 3D interaction mode to Zoom"));
  rb->IndicatorVisibilityOff();
  rb->SetImageToPredefinedIcon(vtkKWIcon::IconNuvola16x16ActionsViewMag);
  rb->SetText("Zoom");
  this->InteractionMode3DToolbar->AddWidget(rb);
  rb->Delete();

}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateToolsToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  this->Superclass::CreateToolsToolbar();
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateMeasurementToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  // Toolbar : measurements
  
#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
  if (this->WidgetInterface)
    {
    vtkVVInteractorWidgetSelector *iw_sel = 
      this->WidgetInterface->GetInteractorWidgetSelector();
    if (iw_sel)
      {
      vtkKWToolbar *toolbar = iw_sel->GetToolbar();
      toolbar->SetParent(this->GetMainToolbarSet()->GetToolbarsFrame());
      iw_sel->CreateToolbar();
      toolbar->SetName(ks_("Toolbar|Measurement"));
      if (!this->GetMainToolbarSet()->HasToolbar(toolbar))
        {  
        this->GetMainToolbarSet()->AddToolbar(toolbar);
        }
      }
    }
#endif
}

//----------------------------------------------------------------------------
void vtkVVWindow::CreateSnapshotToolbar()
{
  if (!this->IsCreated())
    {
    return;
    }

  // Toolbar : snapshot
  
  if (this->ReviewInterface)
    {
    vtkVVSnapshotPresetSelector *sn_sel = 
      this->ReviewInterface->GetSnapshotPresetSelector();
    if (sn_sel)
      {
      vtkKWToolbar *toolbar = sn_sel->GetToolbar();
      toolbar->SetParent(this->GetMainToolbarSet()->GetToolbarsFrame());
      sn_sel->CreateToolbar();
      toolbar->SetName(ks_("Toolbar|Snapshot"));
      if (!this->GetMainToolbarSet()->HasToolbar(toolbar))
        {  
        this->GetMainToolbarSet()->AddToolbar(toolbar);
        }
      }
    }
}

//----------------------------------------------------------------------------
vtkKWApplicationSettingsInterface * 
vtkVVWindow::GetApplicationSettingsInterface()
{
  // If not created, create the application settings interface, connect it
  // to the current window, and manage it with the current interface manager.

  if (!this->ApplicationSettingsInterface)
    {
    this->ApplicationSettingsInterface = 
      vtkVVApplicationSettingsInterface::New();
    this->ApplicationSettingsInterface->SetWindow(this);
    this->ApplicationSettingsInterface->SetUserInterfaceManager(
      this->GetApplicationSettingsUserInterfaceManager());
    }
  return this->ApplicationSettingsInterface;
}

//----------------------------------------------------------------------------
void vtkVVWindow::Update()
{
  this->Superclass::Update();

  int has_data = this->GetDataItemPool() 
    && this->GetDataItemPool()->GetNumberOfDataItems();
  vtkVVDataItem* data = this->GetSelectedDataItem();
  vtkVVDataItemVolume* volume_data = vtkVVDataItemVolume::SafeDownCast(data);
  vtkKWRenderWidget *sel_rw = this->GetSelectedRenderWidget();

  // Update interaction mode toolbars

  vtkKWRadioButton *rb = NULL;
  vtkKWPushButton *pb = NULL;

  vtkKW2DRenderWidget *rw2d = NULL;
  vtkKWVolumeWidget *vw = NULL;
  int nb_widgets = this->GetDataSetWidgetLayoutManager()->GetNumberOfWidgets();
  for (int i = 0; i < nb_widgets; i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetDataSetWidgetLayoutManager()->GetNthWidget(i));
    if (sel_frame)
      {
      if (!rw2d)
        { 
        rw2d = vtkKW2DRenderWidget::SafeDownCast(
          sel_frame->GetRenderWidget());
        if (rw2d)
          {
          rb = vtkKWRadioButton::SafeDownCast(
            this->InteractionMode2DToolbar->GetWidget("Zoom"));
          if (rb)
            {
            rb->SetVariableValueAsInt(rw2d->GetInteractionMode());
            }
          }
        }
      if (!vw)
        {
        vw = vtkKWVolumeWidget::SafeDownCast(sel_frame->GetRenderWidget());
        if (vw)
          {
          rb = vtkKWRadioButton::SafeDownCast(
            this->InteractionMode3DToolbar->GetWidget("Zoom"));
          if (rb)
            {
            rb->SetVariableValueAsInt(vw->GetInteractionMode());
            }
          }
        }
      }
    }
    
  // Quick views
  
  if (!nb_widgets || !data)
    {
    this->QuickViewToolbar->SetEnabled(0);
    }
  else
    {
    pb = vtkKWPushButton::SafeDownCast(
      this->QuickViewToolbar->GetWidget("QuickViewImage"));
    if (pb && !this->GetNthImageWidgetUsingSelectedDataItem(0))
      {
      pb->SetEnabled(0);
      }

    pb = vtkKWPushButton::SafeDownCast(
      this->QuickViewToolbar->GetWidget("QuickViewVolume"));
    if (pb && !this->GetNthVolumeWidgetUsingSelectedDataItem(0))
      {
      pb->SetEnabled(0);
      }

    pb = vtkKWPushButton::SafeDownCast(
      this->QuickViewToolbar->GetWidget("QuickViewLightbox"));
    if (pb && !this->GetNthLightboxWidgetUsingSelectedDataItem(0))
      {
      pb->SetEnabled(0);
      }
    }
  
  // Interaction modes
  
  if (!rw2d)
    {
    this->InteractionMode2DToolbar->SetEnabled(0);
    }
  else
    {
    int enabled = vtkKWProbeImageWidget::SafeDownCast(sel_rw) 
      ? this->InteractionMode2DToolbar->GetEnabled() : 0;
    rb = vtkKWRadioButton::SafeDownCast(
      this->InteractionMode2DToolbar->GetWidget("Rotate"));
    if (rb)
      {
      rb->SetEnabled(enabled);
      }
    rb = vtkKWRadioButton::SafeDownCast(
      this->InteractionMode2DToolbar->GetWidget("Reslice"));
    if (rb)
      {
      rb->SetEnabled(enabled);
      }
    rb = vtkKWRadioButton::SafeDownCast(
      this->InteractionMode2DToolbar->GetWidget("Translate"));
    if (rb)
      {
      rb->SetEnabled(enabled);
      }
    }
  
  if (!vw)
    {
    this->InteractionMode3DToolbar->SetEnabled(0);
    }
}

//----------------------------------------------------------------------------
void vtkVVWindow::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
