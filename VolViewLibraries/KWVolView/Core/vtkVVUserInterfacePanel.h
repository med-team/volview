/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVUserInterfacePanel - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVUserInterfacePanel_h
#define __vtkVVUserInterfacePanel_h

#include "vtkKWUserInterfacePanel.h"

class vtkVVWindowBase;

class VTK_EXPORT vtkVVUserInterfacePanel : public vtkKWUserInterfacePanel
{
public:
  static vtkVVUserInterfacePanel* New();
  vtkTypeRevisionMacro(vtkVVUserInterfacePanel,vtkKWUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the window (do not ref count it since the window will ref count
  // this widget).
  vtkGetObjectMacro(Window, vtkVVWindowBase);
  virtual void SetWindow(vtkVVWindowBase*);

protected:
  vtkVVUserInterfacePanel();
  ~vtkVVUserInterfacePanel();

  vtkVVWindowBase        *Window;

private:
  vtkVVUserInterfacePanel(const vtkVVUserInterfacePanel&); // Not implemented
  void operator=(const vtkVVUserInterfacePanel&); // Not Implemented
};

#endif

