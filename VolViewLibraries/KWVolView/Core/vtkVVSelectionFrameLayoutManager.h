/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSelectionFrameLayoutManager - a MxN layout manager for a set of vtkKWSelectionFrame
// .SECTION Description
// This class is a layout manager for vtkKWSelectionFrame. It will grid them
// according to a given MxN resolution, allocate new ones, handle print/screenshots, etc. 

#ifndef __vtkVVSelectionFrameLayoutManager_h
#define __vtkVVSelectionFrameLayoutManager_h

#include "vtkKWSelectionFrameLayoutManager.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkKWSelectionFrame;
class vtkVVSelectionFrameLayoutManagerInternals;
class vtkKWRenderWidget;
class vtkImageData;
class vtkKWMenu;
class vtkKWToolbar;
class vtkVVSelectionFrame;

class VTK_EXPORT vtkVVSelectionFrameLayoutManager : public vtkKWSelectionFrameLayoutManager
{
public:
  static vtkVVSelectionFrameLayoutManager* New();
  vtkTypeRevisionMacro(vtkVVSelectionFrameLayoutManager, vtkKWSelectionFrameLayoutManager);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Allocate a new widget.
  virtual vtkKWSelectionFrame* AllocateWidget();

  // Description:
  // Return the slection frame containing a given renderer, or null
  // if it does not exist.
  virtual vtkKWSelectionFrame* GetContainingSelectionFrame(
      vtkKWRenderWidget* ren);

  // Description:
  // Convenience method to update the annotation to all the render widgets
  // in this manager.
  virtual void UpdateRenderWidgetsAnnotations();

  // Description:
  // Convenience method to set the visibility of the some annotations of
  // all the render widgets in this manager.
  virtual void SetCornerAnnotationsVisibility(int v);
  vtkBooleanMacro(CornerAnnotationsVisibility, int);
  virtual int GetCornerAnnotationsVisibility();
  virtual void SetScaleBarsVisibility(int v);
  vtkBooleanMacro(ScaleBarsVisibility, int);
  virtual int GetScaleBarsVisibility();
  virtual void ToggleAlmostAllAnnotationsVisibility();

  // Description:
  // Convenience method to check if (and/or how many) paintbrush widgets are
  // defined over all the selection frames
  virtual int GetNumberOfPaintbrushWidgets();

  // Description:
  // Override the superclass method to see if the application is set in quiet
  // mode, in which case, no dialogs are popped up during the save process.
  virtual int SaveScreenshotAllWidgetsToFile(const char* fileName);

protected:
  vtkVVSelectionFrameLayoutManager();
  ~vtkVVSelectionFrameLayoutManager() {};

  // Description:
  // Get preferred frame for annotations checking. This selects a frame
  // that will be used to check which annotations are On/Off, assuming
  // all of them are in-sync. The selected frame is favored, to cope
  // we unsynchronized frames
  virtual vtkVVSelectionFrame* GetPreferredFrameForAnnotationsCheck();

private:

  vtkVVSelectionFrameLayoutManager(const vtkVVSelectionFrameLayoutManager&); // Not implemented
  void operator=(const vtkVVSelectionFrameLayoutManager&); // Not implemented
};

#endif
