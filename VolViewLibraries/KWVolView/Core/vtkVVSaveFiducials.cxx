/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVSaveFiducials.h"

#include "vtkErrorCode.h"
#include "vtkImageData.h"
#include "vtkImageReader2.h"
#include "vtkObjectFactory.h"
#include "vtkImageClip.h"
#include "vtkImageShiftScale.h"
#include "vtkVolumeProperty.h"

#include "vtkKWApplication.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWProgressCommand.h"
#include "vtkKWProgressGauge.h"
#include "vtkKWOpenWizard.h"
#include "vtkKWOpenFileProperties.h"
#include "vtkKWOpenFileHelper.h"
#include "vtkKWWindow.h"

#include "vtkVVDataItemVolume.h"
#include "vtkVVHandleWidget.h"

#include <vtksys/SystemTools.hxx>

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSaveFiducials);

vtkSetObjectImplementationMacro(vtkVVSaveFiducials, DataItemVolume, vtkVVDataItemVolume);
vtkSetObjectImplementationMacro(vtkVVSaveFiducials, Window, vtkKWWindow);

//----------------------------------------------------------------------------
vtkVVSaveFiducials::vtkVVSaveFiducials()
{
  this->DataItemVolume = NULL;
  this->FileName       = NULL;
  this->Window         = NULL;
}

//----------------------------------------------------------------------------
vtkVVSaveFiducials::~vtkVVSaveFiducials()
{
  this->SetDataItemVolume(NULL);
  this->SetFileName(NULL);
  if (this->Window)
    {
    this->SetWindow(NULL);
    }
}

//----------------------------------------------------------------------------
int vtkVVSaveFiducials::Write()
{
  int success = 0;

  if (!this->DataItemVolume || !this->FileName)
    {
    vtkErrorMacro("Input or filename not set. Don't know how to continue");
    return success;
    }

  int nb_of_markers = vtkVVHandleWidget::GetNumberOfHandlesInDataItem(
        this->DataItemVolume);

  if (!nb_of_markers)
    {
    vtkErrorMacro("No fiducials set on selected volume.");
    return success;
    }

  // Write file
  ofstream file(this->FileName, ios::out);
  if (file.fail())
    {
    vtkErrorMacro("Could not open file for writing" << 
                  this->FileName);
    return success;
    }
  
  for (int i = 0; i < nb_of_markers; ++i)
    {
    vtkVVHandleWidget *handle = 
      vtkVVHandleWidget::GetNthHandleInDataItem( this->DataItemVolume, i );
    double pos[3];
    handle->GetWorldPosition(pos);
    file << (i+1) << "," << pos[0] << "," << pos[1] << "," 
	    << pos[2] << std::endl;
    }

  file.close();
  if (file.fail())
    {
    vtkErrorMacro("Failure trying to close file." << 
                  this->FileName);
    return success;
    }
  success = 1;
  return success;
}

//----------------------------------------------------------------------------
void vtkVVSaveFiducials::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  
  os << indent << "DataItemVolume: " << this->DataItemVolume << endl;
  os << indent << "FileName: " << this->FileName << endl;
}

