/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVApplicationSettingsInterfaceBase - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel. It extends the
// user interface defined in vtkKWApplicationSettingsInterfaceBase.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWApplicationSettingsInterfaceBase vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVApplicationSettingsInterfaceBase_h
#define __vtkVVApplicationSettingsInterfaceBase_h

#include "vtkKWApplicationSettingsInterfacePro.h"

//----------------------------------------------------------------------------

class vtkKWLoadSaveButton;
class vtkKWEntryWithLabel;

class VTK_EXPORT vtkVVApplicationSettingsInterfaceBase 
                   : public vtkKWApplicationSettingsInterfacePro
{
public:
  static vtkVVApplicationSettingsInterfaceBase* New();
  vtkTypeRevisionMacro(vtkVVApplicationSettingsInterfaceBase,
                       vtkKWApplicationSettingsInterfacePro);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();
  
  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Callbacks
  virtual void UseAlphaChannelInScreenshotCallback(int state);
  virtual void ExternalApplicationParametersCallback(const char*);
  virtual void ExternalApplicationPathCallback();

protected:
  vtkVVApplicationSettingsInterfaceBase();
  ~vtkVVApplicationSettingsInterfaceBase();

  // Graphics settings

  vtkKWCheckButton *UseAlphaChannelInScreenshotCheckButton;

  // External Applications

  vtkKWFrameWithLabel *ExternalApplicationsFrame;
  vtkKWLoadSaveButton *ExternalApplicationLoadSaveButton;
  vtkKWEntryWithLabel *ExternalApplicationParametersEntry;

private:
  vtkVVApplicationSettingsInterfaceBase(const vtkVVApplicationSettingsInterfaceBase&); // Not implemented
  void operator=(const vtkVVApplicationSettingsInterfaceBase&); // Not Implemented
};

#endif

