/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWApplication.h"
#include "vtkVVSaveFiducialsDialog.h"
#include "vtkObjectFactory.h"
#include "vtkKWMessageDialog.h"
#include "vtkKWLoadSaveDialog.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSaveFiducialsDialog);

//----------------------------------------------------------------------------
vtkVVSaveFiducialsDialog::vtkVVSaveFiducialsDialog()
{
  this->SaveDialog = 1;
  this->SetTitle("Save Fiducials");
  // * The TIFF writer seems to work, but when loading the new files
  //   the app will crash on exit, HEAD corruption. Orientation is flaky too.
  // * Wo reads PGM nowadays..
  this->SetFileTypes("{{Comma Separated Value} {.csv}} "
                     "{{Text} {.txt}} ");
  this->SetDefaultExtension(".csv");
}
