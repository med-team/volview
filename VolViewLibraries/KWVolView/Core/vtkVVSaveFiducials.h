/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSaveFiducials - Save out the volume from image data
// .SECTION Description
// A simple class that writes out the volume, by picking the right
// writer class.

#ifndef __vtkVVSaveFiducials_h
#define __vtkVVSaveFiducials_h

#include "vtkKWObject.h"

class vtkImageWriter;
class vtkKWWindow;
class vtkVVDataItemVolume;
class vtkAlgorithm;

class VTK_EXPORT vtkVVSaveFiducials : public vtkKWObject
{
public:
  static vtkVVSaveFiducials* New();
  vtkTypeMacro(vtkVVSaveFiducials,vtkKWObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Save the file
  int Write();

  // Description:
  // Set/Get the filename
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);

  // Description:
  // Set/Get the data item volume that has to be saved.
  virtual void SetDataItemVolume(vtkVVDataItemVolume*);
  vtkGetObjectMacro(DataItemVolume, vtkVVDataItemVolume);
  
  // Description:
  // Set the parent window.
  virtual void SetWindow(vtkKWWindow *);

protected:
  vtkVVSaveFiducials();
  ~vtkVVSaveFiducials();

  vtkVVDataItemVolume *DataItemVolume;
  char *FileName;
  vtkKWWindow *Window;

private:
  vtkVVSaveFiducials(const vtkVVSaveFiducials&); // Not implemented
  void operator=(const vtkVVSaveFiducials&); // Not implemented
};


#endif



