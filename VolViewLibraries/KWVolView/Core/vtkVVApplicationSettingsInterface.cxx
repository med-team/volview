/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVApplicationSettingsInterface.h"

#include "vtkKWLabel.h"
#include "vtkKWFrame.h"
#include "vtkObjectFactory.h"
#include "vtkKWCheckButton.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWInternationalization.h"
#include "vtkKWRadioButton.h"
#include "vtkKWRadioButtonSet.h"

#include "vtkVVWindow.h"
#include "vtkVVDisplayInterface.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVApplicationSettingsInterface);
vtkCxxRevisionMacro(vtkVVApplicationSettingsInterface, "$Revision: 1.14 $");

//----------------------------------------------------------------------------
vtkVVApplicationSettingsInterface::vtkVVApplicationSettingsInterface()
{
}

//----------------------------------------------------------------------------
vtkVVApplicationSettingsInterface::~vtkVVApplicationSettingsInterface()
{
}

// ---------------------------------------------------------------------------
void vtkVVApplicationSettingsInterface::Create()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("The panel is already created.");
    return;
    }

  // Create the superclass instance (and set the application)

  this->Superclass::Create();

  // Update according to the current Window

  this->Update();
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterface::Update()
{
  this->Superclass::Update();
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterface::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterface::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

