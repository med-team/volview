/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVWindow - the VolView top level window
// .SECTION Description
// This class represents a top level window with menu bar and status
// line. It is the main window for a volview application. 

#ifndef __vtkVVWindow_h
#define __vtkVVWindow_h

#include "vtkVVWindowBase.h"
#include "vtkKWVolViewConfigure.h" // KWVolView_USE_LESION_SIZING_KIT  

class vtkVVWidgetInterface;
class vtkVVDisplayInterface;
class vtkVVPluginInterface;
class vtkVVInformationInterface;
class vtkVVAdvancedAlgorithmsInterface;
class vtkVVReviewInterface;
class vtkVVLesionSizingInterface;

class VTK_EXPORT vtkVVWindow : public vtkVVWindowBase
{
public:
  static vtkVVWindow* New();
  vtkTypeRevisionMacro(vtkVVWindow,vtkVVWindowBase);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Close this window, possibly prompting the user.
  // Return 1 if the window closed successfully, 0 otherwise (for example,
  // if some dialogs are still up, or the user did not confirm, etc).
  virtual int Close();

  // Description:
  // Get/Show the interfaces. 
  vtkGetObjectMacro(InformationInterface, vtkVVInformationInterface);
  vtkGetObjectMacro(DisplayInterface,     vtkVVDisplayInterface);
  vtkGetObjectMacro(WidgetInterface,     vtkVVWidgetInterface);
  vtkGetObjectMacro(ReviewInterface,      vtkVVReviewInterface);
  vtkGetObjectMacro(PluginInterface,      vtkVVPluginInterface);
  vtkGetObjectMacro(AdvancedAlgorithmsInterface, vtkVVAdvancedAlgorithmsInterface);
  
  // Description:
  // Get/Show the interfaces. 
  virtual vtkKWApplicationSettingsInterface *GetApplicationSettingsInterface();

  // Description:
  // Update the UI.
  virtual void Update();

  // Description:
  // This is set automatically by the application when created. You should
  // not need to muck with it. If you must muck with it, do it before you 
  // invoke Window::Create().
  vtkSetMacro(SupportPlugins, int);
  vtkGetMacro(SupportPlugins, int);
  vtkBooleanMacro(SupportPlugins, int);

  // Description:
  // Support Lesion Sizing Toolkit interface (to be set in the constructor
  // by subclasses)
  vtkSetMacro(SupportLesionSizingInterface, int);
  vtkGetMacro(SupportLesionSizingInterface, int);
  vtkBooleanMacro(SupportLesionSizingInterface, int);

  // Description:
  // Some constants
  vtkGetStringMacro(CreatePresetThumbnailAutomaticallyRegKey);
  vtkGetStringMacro(PresetsLayoutRegKey);

protected:
  vtkVVWindow();
  ~vtkVVWindow();

  // Description:
  // Create the widget
  virtual void CreateWidget();

  // Description:
  // Create the toolbars.
  virtual void CreateToolbars();
  virtual void CreateQuickViewToolbar();
  virtual void CreateLayoutManagerToolbar();
  virtual void CreateInteractionMode2DToolbar();
  virtual void CreateInteractionMode3DToolbar();
  virtual void CreateToolsToolbar();
  virtual void CreateMeasurementToolbar();
  virtual void CreateSnapshotToolbar();

  // Description:
  // Create the UserInterface panels
  virtual void CreateUserInterfacePanels();

  // Description:
  // Create each UserInterface panel
  // Return 1 if the panel was effectively created, or 0 if the panel had
  // already been created or on error
  virtual int CreateDisplayInterface();
  virtual int CreateWidgetInterface();
  virtual int CreateReviewInterface();
  virtual int CreatePluginInterface();
  virtual int CreateAdvancedAlgorithmsInterface();
  virtual int CreateInformationInterface();
  virtual int CreateLesionSizingInterface();

  // Description:
  // User Interface Panels
  vtkVVInformationInterface *InformationInterface;
  vtkVVDisplayInterface     *DisplayInterface;
  vtkVVWidgetInterface     *WidgetInterface;
  vtkVVReviewInterface      *ReviewInterface;
  vtkVVPluginInterface      *PluginInterface;
  vtkVVAdvancedAlgorithmsInterface *AdvancedAlgorithmsInterface;

  vtkVVLesionSizingInterface *LesionSizingInterface;

  int SupportPlugins;
  int SupportLesionSizingInterface;

  // Description:
  // Some constants
  vtkSetStringMacro(CreatePresetThumbnailAutomaticallyRegKey);
  vtkSetStringMacro(PresetsLayoutRegKey);

  char *CreatePresetThumbnailAutomaticallyRegKey;
  char *PresetsLayoutRegKey;
  
private:

  vtkVVWindow(const vtkVVWindow&); // Not implemented
  void operator=(const vtkVVWindow&); // Not implemented
};

#endif
