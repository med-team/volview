/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSnapshotPresetSelector - a snapshot preset selector.
// .SECTION Description
// This class is a widget that can be used to store snapshot presets
// (also known as snapshots). 
// .SECTION See Also
// vtkKWPresetSelector

#ifndef __vtkVVSnapshotPresetSelector_h
#define __vtkVVSnapshotPresetSelector_h

#include "vtkKWPresetSelector.h"

class vtkVVDataItemPool;
class vtkVVSnapshot;
class vtkVVSnapshotPresetSelectorInternals;

class VTK_EXPORT vtkVVSnapshotPresetSelector : public vtkKWPresetSelector
{
public:
  static vtkVVSnapshotPresetSelector* New();
  vtkTypeRevisionMacro(vtkVVSnapshotPresetSelector, vtkKWPresetSelector);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the snapshot associated to the preset in the pool.
  // Return 1 on success, 0 on error
  virtual int SetPresetSnapshot(int id, vtkVVSnapshot *snapshot);
  virtual vtkVVSnapshot* GetPresetSnapshot(int id);

  // Description:
  // Get id of preset with given snapshot (-1 if not found), or query if the
  // pool has a given snapshot in a preset.
  virtual int GetIdOfPresetWithSnapshot(vtkVVSnapshot *snapshot);
  virtual int HasPresetWithSnapshot(vtkVVSnapshot *snapshot);

  // Description:
  // Callback invoked when the user ends editing a specific preset field
  // located at cell ('row', 'col').
  // The main purpose of this method is to perform a final validation of
  // the edit window's contents 'text'.
  // This method returns the value that is to become the new contents
  // for that cell.
  // The next step (updating) is handled by PresetCellUpdateCallback
  virtual const char* PresetCellEditEndCallback(
    int row, int col, const char *text);

  // Description:
  // Most (if not all) of the information associated to a preset (say group, 
  // comment, filename, creation time, thumbnail and screenshot) is stored
  // under the hood as user slots using the corresponding API (i.e. 
  // Set/GetPresetUserSlotAs...()). Since each slot requires a unique name,
  // the following methods are provided to retrieve the slot name for
  // the default preset fields. This can be useful to avoid collision between
  // the default slots and your own user slots. Note that the default slot
  // names can be changed too, but doing so will not transfer the value
  // stored at the old slot name to the new slot name (it is up to you to do
  // so, if needed).
  virtual void SetPresetSnapshotSlotName(const char *);
  virtual const char* GetPresetSnapshotSlotName();

  // Description:
  // Update the preset row, i.e. add a row for that preset if it is not
  // displayed already, hide it if it does not match GroupFilter, and
  // update the table columns with the corresponding preset fields.
  // Subclass should override this method to display their own fields.
  // Return 1 on success, 0 if the row was not (or can not be) updated.
  // Subclasses should call the parent's UpdatePresetRow, and abort
  // if the result is not 1.
  virtual int UpdatePresetRow(int id);

protected:
  vtkVVSnapshotPresetSelector();
  ~vtkVVSnapshotPresetSelector();

  // Description:
  // Set the toolbar preset buttons balloon help strings
  // Subclass can override this method to change the help strings
  // associated to the buttons.
  virtual void SetToolbarPresetButtonsHelpStrings(vtkKWToolbar*);

  // PIMPL Encapsulation for STL containers
  //BTX
  vtkVVSnapshotPresetSelectorInternals *Internals;
  //ETX

private:

  vtkVVSnapshotPresetSelector(const vtkVVSnapshotPresetSelector&); // Not implemented
  void operator=(const vtkVVSnapshotPresetSelector&); // Not implemented
};

#endif
