/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVSelectionFrameLayoutManager.h"
#include "vtkObjectFactory.h"
#include "vtkVVSelectionFrame.h"
#include "vtkVVApplication.h"
#include "vtkVVDataItem.h"
#include "vtkKWImageWidget.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSelectionFrameLayoutManager);
vtkCxxRevisionMacro(vtkVVSelectionFrameLayoutManager, "$Revision: 1.14 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLVVSelectionFrameLayoutManagerReader.h"
#include "XML/vtkXMLVVSelectionFrameLayoutManagerWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkVVSelectionFrameLayoutManager, vtkXMLVVSelectionFrameLayoutManagerReader, vtkXMLVVSelectionFrameLayoutManagerWriter);

//----------------------------------------------------------------------------
vtkVVSelectionFrameLayoutManager::vtkVVSelectionFrameLayoutManager()
{
  this->Resolution[0] = 2;
  this->Resolution[1] = 2;
}

//----------------------------------------------------------------------------
vtkKWSelectionFrame* vtkVVSelectionFrameLayoutManager::AllocateWidget()
{
  vtkVVSelectionFrame *widget = vtkVVSelectionFrame::New();
  return widget;
}

//----------------------------------------------------------------------------
void vtkVVSelectionFrameLayoutManager::UpdateRenderWidgetsAnnotations()
{
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame && sel_frame->GetDataItem())
      {
      sel_frame->GetDataItem()->UpdateRenderWidgetsAnnotations();
      if (sel_frame->GetRenderWidget())
        {
        sel_frame->GetRenderWidget()->Render();
        }
      }
    }
}

//----------------------------------------------------------------------------
vtkVVSelectionFrame* 
vtkVVSelectionFrameLayoutManager::GetPreferredFrameForAnnotationsCheck()
{
  vtkVVSelectionFrame *selected_frame = vtkVVSelectionFrame::SafeDownCast(
    this->GetSelectedWidget());
  vtkVVSelectionFrame *found_frame = NULL;
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame && sel_frame->GetRenderWidget())
      {
      // Let's pick one at least, and favor any that is really visible
      // in the layout
      if (!found_frame || this->GetWidgetVisibility(sel_frame))
        {
        found_frame = sel_frame;
        }
      // Really favor the selected frame
      if (sel_frame == selected_frame)
        {
        break;
        }
      }
    }
  return found_frame;
}

//----------------------------------------------------------------------------
vtkKWSelectionFrame *vtkVVSelectionFrameLayoutManager::
  GetContainingSelectionFrame(vtkKWRenderWidget *ren)
{
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame && (sel_frame->GetRenderWidget()==ren))
      {
      return sel_frame;
      }
    }
  return 0;
}

//----------------------------------------------------------------------------
void vtkVVSelectionFrameLayoutManager::SetCornerAnnotationsVisibility(int v)
{
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame && sel_frame->GetRenderWidget())
      {
      sel_frame->GetRenderWidget()->SetCornerAnnotationVisibility(v);
      }
    }
}

//----------------------------------------------------------------------------
int vtkVVSelectionFrameLayoutManager::GetCornerAnnotationsVisibility()
{
  vtkVVSelectionFrame *found_frame = 
    this->GetPreferredFrameForAnnotationsCheck();
  return found_frame ? 
    found_frame->GetRenderWidget()->GetCornerAnnotationVisibility() : 0;
}

//----------------------------------------------------------------------------
void vtkVVSelectionFrameLayoutManager::SetScaleBarsVisibility(int v)
{
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame && sel_frame->GetRenderWidget())
      {
      vtkKWImageWidget *iw = vtkKWImageWidget::SafeDownCast(
        sel_frame->GetRenderWidget());
      if (iw)
        {
        iw->SetScaleBarVisibility(v);
        }
      }
    }
}

//----------------------------------------------------------------------------
int vtkVVSelectionFrameLayoutManager::GetScaleBarsVisibility()
{
  vtkVVSelectionFrame *found_frame = 
    this->GetPreferredFrameForAnnotationsCheck();
  vtkKWImageWidget *iw = vtkKWImageWidget::SafeDownCast(
    found_frame ? found_frame->GetRenderWidget() : NULL);
  return iw ? iw->GetScaleBarVisibility() : 0;
}

//----------------------------------------------------------------------------
void vtkVVSelectionFrameLayoutManager::ToggleAlmostAllAnnotationsVisibility()
{
  int vis = this->GetCornerAnnotationsVisibility() ? 0 : 1;
  this->SetCornerAnnotationsVisibility(vis);
  //this->SetScaleBarsVisibility(vis);
}

//----------------------------------------------------------------------------
int vtkVVSelectionFrameLayoutManager::GetNumberOfPaintbrushWidgets()
{
  int count = 0;
  for (int i = 0; i < this->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel_frame = vtkVVSelectionFrame::SafeDownCast(
      this->GetNthWidget(i));
    if (sel_frame)
      {
      count += sel_frame->GetNumberOfPaintbrushWidgets();
      }
    }
  return count;
}

//----------------------------------------------------------------------------
void vtkVVSelectionFrameLayoutManager::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
int vtkVVSelectionFrameLayoutManager
::SaveScreenshotAllWidgetsToFile(const char* filename)
{
  int quiet = 0;
  if (vtkKWApplicationPro *appPro = 
      vtkKWApplicationPro::SafeDownCast(this->GetApplication()))
    {
    quiet = appPro->GetTestingMode();
    }
  return this->Superclass::SaveScreenshotAllWidgetsToFile( filename, quiet );
}

