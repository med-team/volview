/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSelectionFrameWriter - vtkVVSelectionFrame XML Writer.
// .SECTION Description
// vtkXMLVVSelectionFrameWriter provides XML writing functionality to 
// vtkVVSelectionFrame.
// .SECTION See Also
// vtkXMLVVSelectionFrameReader

#ifndef __vtkXMLVVSelectionFrameWriter_h
#define __vtkXMLVVSelectionFrameWriter_h

#include "XML/vtkXMLKWSelectionFrameWriter.h"

class VTK_EXPORT vtkXMLVVSelectionFrameWriter : public vtkXMLKWSelectionFrameWriter
{
public:
  static vtkXMLVVSelectionFrameWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVSelectionFrameWriter,vtkXMLKWSelectionFrameWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the selection frames.
  static const char* GetRenderWidgetElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the selection frames.
  static const char* GetInteractorWidgetsElementName();

protected:
  vtkXMLVVSelectionFrameWriter() {};
  ~vtkXMLVVSelectionFrameWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVSelectionFrameWriter(const vtkXMLVVSelectionFrameWriter&);  // Not implemented.
  void operator=(const vtkXMLVVSelectionFrameWriter&);  // Not implemented.
};

#endif

