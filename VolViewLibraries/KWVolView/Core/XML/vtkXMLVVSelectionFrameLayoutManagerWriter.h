/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSelectionFrameLayoutManagerWriter - vtkVVSelectionFrameLayoutManager XML Writer.
// .SECTION Description
// vtkXMLVVSelectionFrameLayoutManagerWriter provides XML writing functionality to 
// vtkVVSelectionFrameLayoutManager.
// .SECTION See Also
// vtkXMLVVSelectionFrameLayoutManagerReader

#ifndef __vtkXMLVVSelectionFrameLayoutManagerWriter_h
#define __vtkXMLVVSelectionFrameLayoutManagerWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLVVSelectionFrameLayoutManagerWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLVVSelectionFrameLayoutManagerWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVSelectionFrameLayoutManagerWriter,vtkXMLObjectWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the selection frames.
  static const char* GetSelectionFramesElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store one selection frame.
  static const char* GetSelectionFrameContainerElementName();

protected:
  vtkXMLVVSelectionFrameLayoutManagerWriter() {};
  ~vtkXMLVVSelectionFrameLayoutManagerWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVSelectionFrameLayoutManagerWriter(const vtkXMLVVSelectionFrameLayoutManagerWriter&);  // Not implemented.
  void operator=(const vtkXMLVVSelectionFrameLayoutManagerWriter&);  // Not implemented.
};

#endif

