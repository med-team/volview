/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVApplicationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVApplication.h"
#include "vtkVVWindowBase.h"

vtkStandardNewMacro(vtkXMLVVApplicationWriter);
vtkCxxRevisionMacro(vtkXMLVVApplicationWriter, "$Revision: 1.8 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVApplicationWriter::GetRootElementName()
{
  return "VVApplication";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVApplicationWriter::GetWindowsElementName()
{
  return "Windows";
}

//----------------------------------------------------------------------------
int vtkXMLVVApplicationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVApplication *obj = vtkVVApplication::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVApplication is not set!");
    return 0;
    }

  // Windows

  vtkXMLDataElement *nested_elem = this->NewDataElement();
  nested_elem->SetName(this->GetWindowsElementName());
  elem->AddNestedElement(nested_elem);
  nested_elem->Delete();

  for (int i = 0; i < obj->GetNumberOfWindows(); i++)
    {
    vtkVVWindowBase *win = vtkVVWindowBase::SafeDownCast(obj->GetNthWindow(i));
    if (win)
      {
      vtkXMLObjectWriter *xmlw = win->GetNewXMLWriter();
      xmlw->CreateInElement(nested_elem);
      xmlw->Delete();
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVApplicationWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

