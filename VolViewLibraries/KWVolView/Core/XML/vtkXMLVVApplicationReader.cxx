/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVApplicationReader.h"

#include "vtkObjectFactory.h"
#include "vtkVVApplication.h"
#include "vtkXMLDataElement.h"
#include "vtkVVWindowBase.h"

#include "XML/vtkXMLVVApplicationWriter.h"

vtkStandardNewMacro(vtkXMLVVApplicationReader);
vtkCxxRevisionMacro(vtkXMLVVApplicationReader, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVApplicationReader::GetRootElementName()
{
  return "VVApplication";
}

//----------------------------------------------------------------------------
int vtkXMLVVApplicationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkVVApplication *obj = vtkVVApplication::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVApplication is not set!");
    return 0;
    }

  // Get Attributes

  // Get nested elements

  vtkXMLDataElement *nested_elem;

  // Windows
  
  nested_elem = elem->FindNestedElementWithName(
    vtkXMLVVApplicationWriter::GetWindowsElementName());
  if (nested_elem)
    {
    int idx_w = 0, idx, 
      nb_nested_elems_w = nested_elem->GetNumberOfNestedElements();
    for (idx = 0; idx < nb_nested_elems_w; idx++)
      {
      vtkXMLDataElement *nested_elem_w = nested_elem->GetNestedElement(idx);
      vtkVVWindowBase *win = vtkVVWindowBase::SafeDownCast(
        obj->GetNthWindow(idx_w));
      if (win)
        {
        vtkXMLObjectReader *xmlr = win->GetNewXMLReader();
        xmlr->Parse(nested_elem_w);
        xmlr->Delete();
        idx_w++;
        }
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVApplicationReader::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
