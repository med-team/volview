/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVWindowBaseWriter.h"

#include "vtkObjectFactory.h"
#include "vtkVVDataItemPool.h"
#include "vtkVVFileInstancePool.h"
#include "vtkVVSelectionFrameLayoutManager.h"
#include "vtkVVSnapshotPool.h"
#include "vtkVVWindowBase.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLVVWindowBaseWriter);
vtkCxxRevisionMacro(vtkXMLVVWindowBaseWriter, "$Revision: 1.10 $");

//----------------------------------------------------------------------------
vtkXMLVVWindowBaseWriter::vtkXMLVVWindowBaseWriter()
{
  this->OutputUserInterfaceElement = 0;
  this->OutputSnapshots = 1;
}

//----------------------------------------------------------------------------
const char* vtkXMLVVWindowBaseWriter::GetRootElementName()
{
  return "VVWindowBase";
}

//----------------------------------------------------------------------------
int vtkXMLVVWindowBaseWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVWindowBase *obj = vtkVVWindowBase::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVWindowBase is not set!");
    return 0;
    }

#if 0 
  // Not output so that people can not temper with the XML file and
  // bring more functionality to a customized app

  elem->SetIntAttribute("SupportVolumeWidget", 
                        obj->GetSupportVolumeWidget());

  elem->SetIntAttribute("SupportObliqueProbeWidget", 
                        obj->GetSupportObliqueProbeWidget());

  elem->SetIntAttribute("SupportLightboxWidget", 
                        obj->GetSupportLightboxWidget());

  elem->SetIntAttribute("MaximumNumberOfSimultaneousDataItems", 
                        obj->GetMaximumNumberOfSimultaneousDataItems());
#endif

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLVVWindowBaseWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVWindowBase *obj = vtkVVWindowBase::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVWindowBase is not set!");
    return 0;
    }

  // File Instance Pool
  
  vtkVVFileInstancePool *file_pool = obj->GetFileInstancePool();
  if (file_pool && file_pool->GetNumberOfFileInstances())
    {
    vtkXMLObjectWriter *xmlw = file_pool->GetNewXMLWriter();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  // Data Item Pool
  
  vtkVVDataItemPool *data_pool = obj->GetDataItemPool();
  if (data_pool && data_pool->GetNumberOfDataItems())
    {
    vtkXMLObjectWriter *xmlw = data_pool->GetNewXMLWriter();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  // Widget Layout Manager
  
  vtkVVSelectionFrameLayoutManager *mgr = obj->GetDataSetWidgetLayoutManager();
  if (mgr && mgr->GetNumberOfWidgets())
    {
    vtkXMLObjectWriter *xmlw = mgr->GetNewXMLWriter();
    xmlw->CreateInElement(elem);
    xmlw->Delete();
    }

  // Snapshot Pool
  
  if (this->OutputSnapshots)
    {
    vtkVVSnapshotPool *snapshot_pool = obj->GetSnapshotPool();
    if (snapshot_pool && snapshot_pool->GetNumberOfSnapshots())
      {
      vtkXMLObjectWriter *xmlw = snapshot_pool->GetNewXMLWriter();
      xmlw->CreateInElement(elem);
      xmlw->Delete();
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVWindowBaseWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "OutputSnapshots: "
     << (this->OutputSnapshots ? "On" : "Off") << endl;
}

