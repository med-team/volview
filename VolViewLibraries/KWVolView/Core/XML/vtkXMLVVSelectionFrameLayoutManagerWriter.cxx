/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLVVSelectionFrameLayoutManagerWriter.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkVVSelectionFrameLayoutManager.h"
#include "vtkVVSelectionFrame.h"

vtkStandardNewMacro(vtkXMLVVSelectionFrameLayoutManagerWriter);
vtkCxxRevisionMacro(vtkXMLVVSelectionFrameLayoutManagerWriter, "$Revision: 1.8 $");

//----------------------------------------------------------------------------
const char* vtkXMLVVSelectionFrameLayoutManagerWriter::GetRootElementName()
{
  return "VVSelectionFrameLayoutManager";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVSelectionFrameLayoutManagerWriter::GetSelectionFramesElementName()
{
  return "SelectionFrames";
}

//----------------------------------------------------------------------------
const char* vtkXMLVVSelectionFrameLayoutManagerWriter::GetSelectionFrameContainerElementName()
{
  return "SelectionFrameContainer";
}

//----------------------------------------------------------------------------
int vtkXMLVVSelectionFrameLayoutManagerWriter::AddAttributes(
  vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkVVSelectionFrameLayoutManager *obj = 
    vtkVVSelectionFrameLayoutManager::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSelectionFrameLayoutManager is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Resolution", 2, obj->GetResolution());

  elem->SetVectorAttribute("Origin", 2, obj->GetOrigin());

  elem->SetIntAttribute("ReorganizeWidgetPositionsAutomatically", 
                        obj->GetReorganizeWidgetPositionsAutomatically());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLVVSelectionFrameLayoutManagerWriter::AddNestedElements(
  vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkVVSelectionFrameLayoutManager *obj = 
    vtkVVSelectionFrameLayoutManager::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The VVSelectionFrameLayoutManager is not set!");
    return 0;
    }

  // Selection Frames

  vtkXMLDataElement *nested_elem = this->NewDataElement();
  nested_elem->SetName(
    vtkXMLVVSelectionFrameLayoutManagerWriter::GetSelectionFramesElementName());
  elem->AddNestedElement(nested_elem);
  nested_elem->Delete();

  for (int i = 0; i < obj->GetNumberOfWidgets(); i++)
    {
    vtkVVSelectionFrame *sel = 
      vtkVVSelectionFrame::SafeDownCast(obj->GetNthWidget(i));
    if (sel)
      {
      vtkXMLDataElement *container_elem = this->NewDataElement();
      container_elem->SetName(vtkXMLVVSelectionFrameLayoutManagerWriter::GetSelectionFrameContainerElementName());
      nested_elem->AddNestedElement(container_elem);
      container_elem->Delete();
      container_elem->SetAttribute("Tag", obj->GetWidgetTag(sel));
      container_elem->SetAttribute("Group", obj->GetWidgetGroup(sel));
      int pos[2];
      if (obj->GetWidgetPosition(sel, pos))
        {
        container_elem->SetVectorAttribute("Position", 2, pos);
        }

      vtkXMLObjectWriter *xmlw = sel->GetNewXMLWriter();
      xmlw->CreateInElement(container_elem);
      xmlw->Delete();
      }
    }
  
  return 1;
}

//----------------------------------------------------------------------------
void vtkXMLVVSelectionFrameLayoutManagerWriter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

