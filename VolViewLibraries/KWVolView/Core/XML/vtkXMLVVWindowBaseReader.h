/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVWindowBaseReader - vtkVVWindowBase XML Reader.
// .SECTION Description
// vtkXMLVVWindowBaseReader provides XML reading functionality to 
// vtkVVWindowBase.
// .SECTION See Also
// vtkXMLVVWindowBaseWriter

#ifndef __vtkXMLVVWindowBaseReader_h
#define __vtkXMLVVWindowBaseReader_h

#include "XML/vtkXMLKWWindowReader.h"

class VTK_EXPORT vtkXMLVVWindowBaseReader : public vtkXMLKWWindowReader
{
public:
  static vtkXMLVVWindowBaseReader* New();
  vtkTypeRevisionMacro(vtkXMLVVWindowBaseReader, vtkXMLKWWindowReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVWindowBaseReader() {};
  ~vtkXMLVVWindowBaseReader() {};

private:
  vtkXMLVVWindowBaseReader(const vtkXMLVVWindowBaseReader&); // Not implemented
  void operator=(const vtkXMLVVWindowBaseReader&); // Not implemented    
};

#endif

