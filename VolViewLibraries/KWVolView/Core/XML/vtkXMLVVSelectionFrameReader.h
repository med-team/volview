/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSelectionFrameReader - vtkVVSelectionFrame XML Reader.
// .SECTION Description
// vtkXMLVVSelectionFrameReader provides XML reading functionality to 
// vtkVVSelectionFrame.
// .SECTION See Also
// vtkXMLVVSelectionFrameWriter

#ifndef __vtkXMLVVSelectionFrameReader_h
#define __vtkXMLVVSelectionFrameReader_h

#include "XML/vtkXMLKWSelectionFrameReader.h"

class VTK_EXPORT vtkXMLVVSelectionFrameReader : public vtkXMLKWSelectionFrameReader
{
public:
  static vtkXMLVVSelectionFrameReader* New();
  vtkTypeRevisionMacro(vtkXMLVVSelectionFrameReader, vtkXMLKWSelectionFrameReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVSelectionFrameReader() {};
  ~vtkXMLVVSelectionFrameReader() {};

private:
  vtkXMLVVSelectionFrameReader(const vtkXMLVVSelectionFrameReader&); // Not implemented
  void operator=(const vtkXMLVVSelectionFrameReader&); // Not implemented    
};

#endif

