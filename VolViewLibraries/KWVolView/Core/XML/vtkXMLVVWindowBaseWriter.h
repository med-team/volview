/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVWindowBaseWriter - vtkVVWindowBase XML Writer.
// .SECTION Description
// vtkXMLVVWindowBaseWriter provides XML writing functionality to 
// vtkVVWindowBase.
// .SECTION See Also
// vtkXMLVVWindowBaseReader

#ifndef __vtkXMLVVWindowBaseWriter_h
#define __vtkXMLVVWindowBaseWriter_h

#include "XML/vtkXMLKWWindowWriter.h"

class VTK_EXPORT vtkXMLVVWindowBaseWriter : public vtkXMLKWWindowWriter
{
public:
  static vtkXMLVVWindowBaseWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVWindowBaseWriter,vtkXMLKWWindowWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Output snapshots. It can be useful to set it to off... when creating
  // the snapshot themselves (to avoid overwriting them)!
  vtkBooleanMacro(OutputSnapshots, int);
  vtkGetMacro(OutputSnapshots, int);
  vtkSetMacro(OutputSnapshots, int);

protected:
  vtkXMLVVWindowBaseWriter();
  ~vtkXMLVVWindowBaseWriter() {};

  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

  int OutputSnapshots;

private:
  vtkXMLVVWindowBaseWriter(const vtkXMLVVWindowBaseWriter&);  // Not implemented.
  void operator=(const vtkXMLVVWindowBaseWriter&);  // Not implemented.
};

#endif

