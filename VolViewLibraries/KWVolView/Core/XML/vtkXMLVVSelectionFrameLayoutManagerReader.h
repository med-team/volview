/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVSelectionFrameLayoutManagerReader - vtkVVSelectionFrameLayoutManager XML Reader.
// .SECTION Description
// vtkXMLVVSelectionFrameLayoutManagerReader provides XML reading functionality to 
// vtkVVSelectionFrameLayoutManager.
// .SECTION See Also
// vtkXMLVVSelectionFrameLayoutManagerWriter

#ifndef __vtkXMLVVSelectionFrameLayoutManagerReader_h
#define __vtkXMLVVSelectionFrameLayoutManagerReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLVVSelectionFrameLayoutManagerReader : public vtkXMLObjectReader
{
public:
  static vtkXMLVVSelectionFrameLayoutManagerReader* New();
  vtkTypeRevisionMacro(vtkXMLVVSelectionFrameLayoutManagerReader, vtkXMLObjectReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVSelectionFrameLayoutManagerReader() {};
  ~vtkXMLVVSelectionFrameLayoutManagerReader() {};

private:
  vtkXMLVVSelectionFrameLayoutManagerReader(const vtkXMLVVSelectionFrameLayoutManagerReader&); // Not implemented
  void operator=(const vtkXMLVVSelectionFrameLayoutManagerReader&); // Not implemented    
};

#endif

