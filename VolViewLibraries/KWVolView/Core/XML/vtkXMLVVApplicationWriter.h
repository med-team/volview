/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVApplicationWriter - vtkVVApplication XML Writer.
// .SECTION Description
// vtkXMLVVApplicationWriter provides XML writing functionality to 
// vtkVVApplication.
// .SECTION See Also
// vtkXMLVVApplicationReader

#ifndef __vtkXMLVVApplicationWriter_h
#define __vtkXMLVVApplicationWriter_h

#include "XML/vtkXMLKWApplicationWriter.h"

class VTK_EXPORT vtkXMLVVApplicationWriter : public vtkXMLKWApplicationWriter
{
public:
  static vtkXMLVVApplicationWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVApplicationWriter,vtkXMLKWApplicationWriter);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the data element used inside that tree to
  // store the windows.
  static const char* GetWindowsElementName();

protected:
  vtkXMLVVApplicationWriter() {};
  ~vtkXMLVVApplicationWriter() {};

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVApplicationWriter(const vtkXMLVVApplicationWriter&);  // Not implemented.
  void operator=(const vtkXMLVVApplicationWriter&);  // Not implemented.
};

#endif

