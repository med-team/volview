/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVApplicationReader - vtkVVApplication XML Reader.
// .SECTION Description
// vtkXMLVVApplicationReader provides XML reading functionality to 
// vtkVVApplication.
// .SECTION See Also
// vtkXMLVVApplicationWriter

#ifndef __vtkXMLVVApplicationReader_h
#define __vtkXMLVVApplicationReader_h

#include "XML/vtkXMLKWApplicationReader.h"

class VTK_EXPORT vtkXMLVVApplicationReader : public vtkXMLKWApplicationReader
{
public:
  static vtkXMLVVApplicationReader* New();
  vtkTypeRevisionMacro(vtkXMLVVApplicationReader, vtkXMLKWApplicationReader);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVApplicationReader() {};
  ~vtkXMLVVApplicationReader() {};

private:
  vtkXMLVVApplicationReader(const vtkXMLVVApplicationReader&); // Not implemented
  void operator=(const vtkXMLVVApplicationReader&); // Not implemented    
};

#endif

