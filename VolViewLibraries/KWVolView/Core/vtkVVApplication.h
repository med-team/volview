/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVApplication - the VolView application class.
// .SECTION Description
// A subclass of vtkKWApplicationPro specific to VolView.

#ifndef __vtkVVApplication_h
#define __vtkVVApplication_h

#include "vtkKWApplicationPro.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkKWRemoteIOManager;
class vtkKWDataTransfer;
class vtkVVFileAuthenticator;
class vtkXMLKWApplicationWriter;
class vtkXMLKWApplicationReader;

class VTK_EXPORT vtkVVApplication : public vtkKWApplicationPro
{
public:
  static vtkVVApplication* New();
  vtkTypeRevisionMacro(vtkVVApplication,vtkKWApplicationPro);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Start running the main application.
  virtual void Start() { this->Superclass::Start(); }
  virtual void Start(int argc, char *argv[]);

  // Description:
  // Initialize Tcl/Tk
  // Return NULL on error (eventually provides an ostream where detailed
  // error messages will be stored).
  //BTX
  static Tcl_Interp *InitializeTcl(int argc, char *argv[], ostream *err = 0);
  //ETX

  // Description:
  // Load/Save session.
  // The session file extensions (with dot included) can be customized.
  // Multiple space-separated extensions can be specified 
  // (only the first one will be used to save a session).
  // Return 1 on success, 0 otherwise.
  virtual int LoadSession(const char *filename);
  virtual int SaveSession(const char *filename);
  virtual int SaveSession();
  vtkGetStringMacro(SessionFileExtensions);
  vtkSetStringMacro(SessionFileExtensions);

  // Description:
  // Get the RemoteIO Manager. Creates one when its called for the first time
  virtual vtkKWRemoteIOManager * GetRemoteIOManager();

  // Description:
  // VolView can be set into a mode where it authenticates loading of files. 
  // The default strategy, if this flag is ON is to check the MD5 sum of the 
  // file via public/private key authentication. This empowers VolView with 
  // the following functionality.
  //
  // The actual authentication itself is delegated to 
  //   * vtkVVFileAuthenticator -     Base class of authenticators. This does
  //                                  no authentication.
  //   * vtkVVMD5FileAuthenticator - Authenticates based on MD5 encryption
  //                                 with a public/private key pair.
  //
  // You can extend this in your application by supplying your own 
  // authentication strategy and overriding GetAuthenticator().
  virtual void SetAuthenticateRead(int);
  vtkGetMacro( AuthenticateRead, int );
  vtkBooleanMacro( AuthenticateRead, int );

  // Description:
  // The default is to return a 
  //   vtkVVFileAuthenticator     if AuthenticateRead is OFF
  //   vtkVVMD5FileAuthenticator  if AuthenticateRead is ON.
  // Subclasses are free to overrid this and implement their own 
  // authentication strategies.
  vtkGetObjectMacro( Authenticator, vtkVVFileAuthenticator );

  // Description:
  // If true, the application supports loading data via URI's in the 
  // session file
  virtual int SupportRemoteIO() { return 0; }

  // Descrition:
  // Set/Get if the application is running in limited edition mode.
  // This can be used throughout the whole UI to enable or disable
  // features on the fly. Make sure it is *not* wrapped !
  // Override the superlcass method to update the annotations
  //BTX 
  virtual void SetLimitedEditionMode(int arg);
  //ETX

  // Description:
  // External Application.
  virtual int LaunchExternalApplication();
  vtkGetStringMacro(ExternalApplicationPath);
  vtkSetStringMacro(ExternalApplicationPath);
  vtkGetStringMacro(ExternalApplicationParameters);
  vtkSetStringMacro(ExternalApplicationParameters);

  // Descrition:
  // Save/Retrieve the application settings to/from registry.
  // Do not call that method before the application name is known and the
  // proper registry level set (if any).
  virtual void RestoreApplicationSettingsFromRegistry();
  virtual void SaveApplicationSettingsToRegistry();

  // Description:
  // Some constants
  //BTX
  static const char *ExternalApplicationPathRegKey;
  static const char *ExternalApplicationParametersRegKey;
  static const char *PaintbrushNotSavedInSessionDialogName;
  //ETX

protected:
  vtkVVApplication();
  ~vtkVVApplication();

  // Description:
  // Give a change to the application to parse the command-line arguments
  // and set some variables accordingly. Subclasses that do not intend
  // to call their super's Start(argc, argv) should make sure they
  // call their super's ParseCommandLineArguments.
  virtual void ParseCommandLineArguments(int argc, char *argv[]);

  // Description:
  // Initialize the readers
  virtual void InitializeReaders();

  // Description:
  // Create the splash screen
  virtual void CreateSplashScreen();

  // Description:
  // Session file extension
  char *SessionFileExtensions;

  // Description:
  // External application
  char *ExternalApplicationPath;
  char *ExternalApplicationParameters;

  // Description:
  // Send screenshot to Flickr
  virtual int SendScreenshotToFlickr();

  // Description:
  // Manages all the Remote IO.
  vtkKWRemoteIOManager * RemoteIOManager;

  // Description:
  // Authenticate loading of files.
  int AuthenticateRead;
  vtkVVFileAuthenticator * Authenticator;
  virtual void SetAuthenticator( vtkVVFileAuthenticator * );

  // Description:
  // The following callback is invoked any time the transfer status of a URI
  // handler managed by RemoteIOManager changes. For instance the method will
  // be called if a download completes etc.
  //  Args: 'caller' is  vtkKWRemoteIOManager
  //        'event' is usually TransferStatusChaangedEvent
  //        'calldata' is the vtkKWDataTransfer whose status changed.
  static void TransferUpdateCallback( vtkObject *caller,
    unsigned long event, void *, void *calldata );
  virtual void TransferCallback( vtkKWRemoteIOManager*,
    unsigned long event, vtkKWDataTransfer *);

  // Description:
  // Deallocate/delete/reparent some internal objects in order to solve
  // reference loops that would prevent this instance from being deleted.
  virtual void PrepareForDelete();  

  // Description:
  // Add more "About..." information
  virtual void AddAboutText(ostream &);

  // Description:
  // Get new application reader/writer instances. 
  // Reimplement to return app specific reader/writers.
  // Use Delete() after you are done.
  virtual vtkXMLKWApplicationWriter* GetXMLApplicationWriter();
  virtual vtkXMLKWApplicationReader* GetXMLApplicationReader();

private:

  vtkVVApplication(const vtkVVApplication&); // Not implemented
  void operator=(const vtkVVApplication&); // Not implemented
};

#endif



