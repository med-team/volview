/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVSaveFiducialsDialog - a dialog for saving VolView data
// .SECTION Description
// A simple dialog for saving VolView data.

#ifndef __vtkVVSaveFiducialsDialog_h
#define __vtkVVSaveFiducialsDialog_h

#include "vtkKWLoadSaveDialog.h"

class VTK_EXPORT vtkVVSaveFiducialsDialog : public vtkKWLoadSaveDialog
{
public:
  static vtkVVSaveFiducialsDialog* New();
  vtkTypeMacro(vtkVVSaveFiducialsDialog,vtkKWLoadSaveDialog);

protected:
  vtkVVSaveFiducialsDialog();
  ~vtkVVSaveFiducialsDialog() {};

private:
  vtkVVSaveFiducialsDialog(const vtkVVSaveFiducialsDialog&); // Not implemented
  void operator=(const vtkVVSaveFiducialsDialog&); // Not implemented
};


#endif



