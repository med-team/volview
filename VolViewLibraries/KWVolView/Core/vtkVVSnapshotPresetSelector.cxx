/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVSnapshotPresetSelector.h"

#include "vtkVVSnapshot.h"
#include "vtkKWInternationalization.h"
#include "vtkObjectFactory.h"
#include "vtkKWMultiColumnListWithScrollbars.h"
#include "vtkKWMultiColumnList.h"
#include "vtkKWToolbar.h"
#include "vtkKWPushButtonSet.h"
#include "vtkKWPushButton.h"
#include "vtkKWIcon.h"

#include <vtksys/stl/string>

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVSnapshotPresetSelector);
vtkCxxRevisionMacro(vtkVVSnapshotPresetSelector, "$Revision: 1.14 $");

//----------------------------------------------------------------------------
class vtkVVSnapshotPresetSelectorInternals
{
public:

  vtksys_stl::string SnapshotSlotName;
};

//----------------------------------------------------------------------------
vtkVVSnapshotPresetSelector::vtkVVSnapshotPresetSelector()
{
  this->Internals = new vtkVVSnapshotPresetSelectorInternals;
  this->Internals->SnapshotSlotName = "DefaultSnapshotSlot";

  this->SetPresetButtonsBaseIconToPredefinedIcon(
    vtkKWIcon::IconNuvola16x16DevicesCamera);
}

//----------------------------------------------------------------------------
vtkVVSnapshotPresetSelector::~vtkVVSnapshotPresetSelector()
{
  delete this->Internals;
}

//----------------------------------------------------------------------------
void vtkVVSnapshotPresetSelector::SetPresetSnapshotSlotName(
  const char *name)
{
  if (name && *name && 
      this->Internals && 
      this->Internals->SnapshotSlotName.compare(name))
    {
    this->Internals->SnapshotSlotName = name;
    this->ScheduleUpdatePresetRows();
    }
}

//----------------------------------------------------------------------------
const char* vtkVVSnapshotPresetSelector::GetPresetSnapshotSlotName()
{
  if (this->Internals)
    {
    return this->Internals->SnapshotSlotName.c_str();
    }
  return NULL;
}

//----------------------------------------------------------------------------
int vtkVVSnapshotPresetSelector::SetPresetSnapshot(
  int id, vtkVVSnapshot *snapshot)
{
  return this->SetPresetUserSlotAsObject(
    id, this->GetPresetSnapshotSlotName(), snapshot);
}

//----------------------------------------------------------------------------
vtkVVSnapshot* vtkVVSnapshotPresetSelector::GetPresetSnapshot(
  int id)
{
  return (vtkVVSnapshot*)
    this->GetPresetUserSlotAsObject(
      id, this->GetPresetSnapshotSlotName());
}

//----------------------------------------------------------------------------
int vtkVVSnapshotPresetSelector::GetIdOfPresetWithSnapshot(
  vtkVVSnapshot *snapshot)
{
  if (snapshot)
    {
    int i, nb_presets = this->GetNumberOfPresets();
    for (i = 0; i < nb_presets; i++)
      {
      int id = this->GetIdOfNthPreset(i);
      if (this->GetPresetSnapshot(id) == snapshot)
        {
        return id;
        }
      }
    }
  return -1;
}

//----------------------------------------------------------------------------
int vtkVVSnapshotPresetSelector::HasPresetWithSnapshot(vtkVVSnapshot *snapshot)
{
  return (this->GetIdOfPresetWithSnapshot(snapshot) >= 0 ? 1 : 0);
}

//----------------------------------------------------------------------------
int vtkVVSnapshotPresetSelector::UpdatePresetRow(int id)
{
  if (!this->Superclass::UpdatePresetRow(id))
    {
    return 0;
    }

  int row = this->GetPresetRow(id);
  if (row < 0)
    {
    return 0;
    }

  vtkKWMultiColumnList *list = this->PresetList->GetWidget();
  vtkVVSnapshot *snapshot = this->GetPresetSnapshot(id);
  if (snapshot && snapshot->GetInternalFlag())
    {
    list->SetCellBackgroundColor(row, this->GetCommentColumnIndex(), 
                                 //                             239.0/255.0, 223.0/255.0, 223.0/255.0);
                                     225.0/255.0, 239.0/255.0, 223.0/255.0);
    }

  return 1;
}

//---------------------------------------------------------------------------
const char* vtkVVSnapshotPresetSelector::PresetCellEditEndCallback(
  int, int, const char *text)
{
  this->PresetList->GetWidget()->SeeColumn(this->GetThumbnailColumnIndex());

  return text;
}

//----------------------------------------------------------------------------
void vtkVVSnapshotPresetSelector::SetToolbarPresetButtonsHelpStrings(vtkKWToolbar *toolbar)
{
  this->Superclass::SetToolbarPresetButtonsHelpStrings(toolbar);

  if (!toolbar)
    {
    return;
    }

  vtkKWWidget *toolbar_pb;

  // Select prev

  toolbar_pb = toolbar->GetWidget(this->GetSelectPreviousButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Select previous snapshot"));
    }
  
  // Select next

  toolbar_pb = toolbar->GetWidget(this->GetSelectNextButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Select next snapshot"));
    }

  // Add

  toolbar_pb = toolbar->GetWidget(this->GetAddButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Take a snapshot"));
    }
  
  // Apply

  toolbar_pb = toolbar->GetWidget(this->GetApplyButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Apply the selected snapshot(s)"));
    }
  
  // Update

  toolbar_pb = toolbar->GetWidget(this->GetUpdateButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Update the selected snapshot(s)"));
    }
  
  // Remove

  toolbar_pb = toolbar->GetWidget(this->GetRemoveButtonLabel());
  if (toolbar_pb)
    {
    toolbar_pb->SetBalloonHelpString(
      ks_("Snapshot Preset Selector|Delete the selected snapshot(s)"));
    }
}

//----------------------------------------------------------------------------
void vtkVVSnapshotPresetSelector::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
