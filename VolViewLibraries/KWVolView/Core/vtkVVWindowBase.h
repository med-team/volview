/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVWindowBase - the VolView top level window
// .SECTION Description
// This abstract base class represents a top level window with menu bar 
// and status line. It is the main window for a volview application. 

#ifndef __vtkVVWindowBase_h
#define __vtkVVWindowBase_h

#include "vtkKWWindow.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkKWImageWidget;
class vtkKWLightboxWidget;
class vtkKWOpenWizard;
class vtkKWStartupPageWidget;
class vtkKWRenderWidget;
class vtkKWVolumeWidget;
class vtkVVApplicationSettingsInterfaceBase;
class vtkVVDataItem;
class vtkVVDataItemPool;
class vtkVVFileInstance;
class vtkVVFileInstancePool;
class vtkVVSelectionFrame;
class vtkVVSelectionFrameLayoutManager;
class vtkVVSnapshot;
class vtkVVSnapshotPool;

class VTK_EXPORT vtkVVWindowBase : public vtkKWWindow
{
public:
  static vtkVVWindowBase * New();
  vtkTypeRevisionMacro(vtkVVWindowBase,vtkKWWindow);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Close this window, possibly prompting the user.
  // Return 1 if the window closed successfully, 0 otherwise (for example,
  // if some dialogs are still up, or the user did not confirm, etc).
  virtual int Close();

  // Description:
  // Render all widgets
  virtual void Render();

  // Description:
  // Disable/Restore rendering for this window.
  virtual void DisableRenderStates();
  virtual void RestoreRenderStates();

  // Description:
  // Open file.
  virtual void Open();
  virtual int  Open(const char *fname);
  virtual int  OpenRecentFile(const char *fname);

  // Description:
  // Close the data associated to the selected view.
  virtual void CloseSelectedDataItem();
  
  // Description:
  // Close a specified data item or all of them
  virtual int CloseDataItem(vtkVVDataItem *data);
  virtual void CloseAllDataItems();

  // Description:
  // Close all file instances (not all data items are associated to a file
  // though, but all data items can be closed using CloseAllDataItems())
  virtual void CloseAllFileInstances();

  // Description:
  // Save volume
  virtual int SaveSelectedVolume();
  virtual int SaveVolume(vtkVVDataItem* data_item, const char *fname);

  // Description:
  // Save volume
  virtual int SaveSelectedVolumeFiducials();
  virtual int SaveVolumeFiducials(vtkVVDataItem* data_item, const char *fname);

  // Description:
  // Release the file instance from memory, remove all data (and all widgets)
  // associated to that instance.
  // Return 1 of success, 0 otherwise
  virtual int ReleaseFileInstance(vtkVVFileInstance *);

  // Description:
  // Get/Show the interfaces. 
  virtual vtkKWApplicationSettingsInterface *GetApplicationSettingsInterface();

  // Description:
  // Get the dataset widget layout manager
  virtual vtkVVSelectionFrameLayoutManager* GetDataSetWidgetLayoutManager();

  // Description:
  // Set/Get if a given type of render widget should be supported by this
  // window or not.
  vtkSetMacro(SupportVolumeWidget, int);
  vtkGetMacro(SupportVolumeWidget, int);
  vtkBooleanMacro(SupportVolumeWidget, int);
  vtkSetMacro(SupportObliqueProbeWidget, int);
  vtkGetMacro(SupportObliqueProbeWidget, int);
  vtkBooleanMacro(SupportObliqueProbeWidget, int);
  vtkSetMacro(SupportLightboxWidget, int);
  vtkGetMacro(SupportLightboxWidget, int);
  vtkBooleanMacro(SupportLightboxWidget, int);

  // Description:
  // Set/Get the number of maximum data items that can be loaded 
  // simultaneously. The DataItemPool itself can hold more, but this 
  // setting allow different windows to hold a different number of data 
  // items. Default to a large number.
  virtual void SetMaximumNumberOfSimultaneousDataItems(int);
  vtkGetMacro(MaximumNumberOfSimultaneousDataItems, int);

  // Description:
  // Convenience methods to get the selected render widget and the 
  // corresponding data item. 
  // Sibblings can be retried too (i.e. all the renderwidgets that use
  // the same selected data).
  vtkVVSelectionFrame* GetSelectedSelectionFrame();
  vtkKWRenderWidget* GetSelectedRenderWidget();
  vtkVVDataItem* GetSelectedDataItem();
  virtual int GetNumberOfRenderWidgetsUsingSelectedDataItem();
  virtual void RenderAllRenderWidgetsUsingSelectedDataItem();
  virtual int GetNumberOfSelectionFramesUsingSelectedDataItem();
  virtual vtkVVSelectionFrame* GetNthSelectionFrameUsingSelectedDataItem(int idx);
  virtual vtkKWRenderWidget* GetNthRenderWidgetUsingSelectedDataItem(int idx);
  virtual vtkKWImageWidget* GetNthImageWidgetUsingSelectedDataItem(int idx);
  virtual vtkKWVolumeWidget* GetNthVolumeWidgetUsingSelectedDataItem(int idx);
  virtual vtkKWLightboxWidget* GetNthLightboxWidgetUsingSelectedDataItem(int idx);

  // Description:
  // Update the UI.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();
  virtual void UpdateMenuState();

  // Description:
  // Deallocate/delete/reparent some internal objects in order to solve
  // reference loops that would prevent this instance from being deleted.
  virtual void PrepareForDelete();

  // Description:
  // Toolbars are created by this class. Its the subclass' responsibility
  // to add the toolbars, so as to give freedom in the place/order in which 
  // they may be added.
  vtkGetObjectMacro(FileToolbar, vtkKWToolbar);
  vtkGetObjectMacro(QuickViewToolbar, vtkKWToolbar);
  vtkGetObjectMacro(ToolsToolbar, vtkKWToolbar);
  vtkGetObjectMacro(InteractionMode2DToolbar, vtkKWToolbar);
  vtkGetObjectMacro(InteractionMode3DToolbar, vtkKWToolbar);

  // Description:
  // Add all the default observers needed by that object, or remove
  // all the observers that were added through AddCallbackCommandObserver.
  // Subclasses can override these methods to add/remove their own default
  // observers, but should call the superclass too.
  virtual void AddCallbackCommandObservers();
  virtual void RemoveCallbackCommandObservers();

  // Description:
  // Callbacks.
  virtual void InteractionMode2DCallback(int mode);
  virtual void InteractionMode3DCallback(int mode);
  virtual void QuickViewImageCallback();
  virtual void QuickViewVolumeCallback();
  virtual void QuickViewLightboxCallback();
  virtual void NewWindowCallback();

  // Description:
  // Some constants
  vtkGetStringMacro(FileOpenMenuLabel);
  vtkGetStringMacro(FileSaveScreenshotMenuLabel);
  vtkGetStringMacro(FileSaveSessionMenuLabel);
  vtkGetStringMacro(FileSaveVolumeMenuLabel);
  vtkGetStringMacro(FileSaveFiducialsMenuLabel);
  vtkGetStringMacro(FilePrintAllMenuLabel);
  vtkGetStringMacro(FilePrintSelectedMenuLabel);
  vtkGetStringMacro(FileLauncheExternalApplicationMenuLabel);
  vtkGetStringMacro(EditCopyScreenshotMenuLabel);
  vtkGetStringMacro(HelpRegisterMenuLabel);
  vtkGetStringMacro(HelpEmailFeedbackMenuLabel);
  vtkGetStringMacro(FileCloseSelectedDataMenuLabel);

  // Description:
  // Convenience method that return the position where to safely insert 
  // entries in the corresponding menu without interferring with entries
  // that should stay at the end of the menu.
  virtual int GetFileMenuInsertPosition();

  // Description:
  // Access the file instance pool, i.e. a representation of the files loaded
  // so far in this window.
  // Note that from each FileInstance in that pool, one can find a pointer to
  // the DataItems that were created by loading the corresponding file.
  // However, each data item is actually *cross-referenced*, for convenience
  // purposes: ultimately *all* data items, whether they were created from a
  // file, from filtering, from a plug-in or programmatically, are referenced
  // in the DataItemPool instance.
  vtkGetObjectMacro(FileInstancePool, vtkVVFileInstancePool);

  // Description:
  // Access to the data pool.
  // All data items, whether creared by loading a file (see FileInstancePool)
  // or created from within the app (say, as a result of filtering some data)
  // are referenced in this pool.
  vtkGetObjectMacro(DataItemPool, vtkVVDataItemPool);

  // Description:
  // Access the snapshot pool.
  vtkGetObjectMacro(SnapshotPool, vtkVVSnapshotPool);

  // Description:
  // Take/restore/update a snapshot
  virtual vtkVVSnapshot* TakeSnapshot();
  virtual int UpdateSnapshot(vtkVVSnapshot*);
  virtual int RestoreSnapshot(vtkVVSnapshot*);

protected:
  vtkVVWindowBase();
  ~vtkVVWindowBase();

  // Description:
  // Create the widget
  virtual void CreateWidget();

  vtkKWToolbar *FileToolbar;
  vtkKWToolbar *QuickViewToolbar;
  vtkKWToolbar *ToolsToolbar;
  vtkKWToolbar *InteractionMode2DToolbar;
  vtkKWToolbar *InteractionMode3DToolbar;

  int RenderStatesSaveCount;
  int MaximumNumberOfSimultaneousDataItems;

  // Description:
  // Open wizard
  vtkKWOpenWizard *OpenWizard;

  // Description:
  // Startup page
  vtkKWStartupPageWidget *StartupPageWidget;

  // Description:
  // Create the toolbars. This method in turn call the methods to create
  // specific toolbars. Subclasses should override those methods to add
  // icons/tools to the toolbars.
  virtual void CreateToolbars();
  virtual void CreateFileToolbar();
  virtual void CreateQuickViewToolbar();
  virtual void CreateLayoutManagerToolbar();
  virtual void CreateInteractionMode2DToolbar();
  virtual void CreateInteractionMode3DToolbar();
  virtual void CreateToolsToolbar();

  // Description:
  // Load from open wizard
  // Return 1 of success, 0 otherwise
  virtual int LoadFromOpenWizard(vtkKWOpenWizard *);

  // Description:
  // Release the data item from memory, remove all widgets associated to 
  // that item. This method can be used to release several data items in a
  // row, as long as a call to Update() is performed at the end to update
  // the UI. A call to the public methods CloseDataItem() or 
  // CloseSelectedDataItem() will do just that (release and update).
  // Return 1 of success, 0 otherwise
  virtual int ReleaseDataItem(vtkVVDataItem *);

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can override this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // Description:
  // Some constants
  vtkSetStringMacro(FileOpenMenuLabel);
  vtkSetStringMacro(FileSaveScreenshotMenuLabel);
  vtkSetStringMacro(FileSaveSessionMenuLabel);
  vtkSetStringMacro(FileSaveVolumeMenuLabel);
  vtkSetStringMacro(FileSaveFiducialsMenuLabel);
  vtkSetStringMacro(FilePrintAllMenuLabel);
  vtkSetStringMacro(FilePrintSelectedMenuLabel);
  vtkSetStringMacro(FileLauncheExternalApplicationMenuLabel);
  vtkSetStringMacro(EditCopyScreenshotMenuLabel);
  vtkSetStringMacro(HelpRegisterMenuLabel);
  vtkSetStringMacro(HelpEmailFeedbackMenuLabel);
  vtkSetStringMacro(FileCloseSelectedDataMenuLabel);

  int SupportVolumeWidget;
  int SupportObliqueProbeWidget;
  int SupportLightboxWidget;

  // Description:
  // Dataset widget layout manager
  vtkVVSelectionFrameLayoutManager *DataSetWidgetLayoutManager;

  // Description:
  // Reduce the number of data items represented in the window according
  // to the MaximumNumberOfSimultaneousDataItems limit.
  // Return 1 on success, 0 otherwise.
  virtual int LimitMaximumNumberOfSimultaneousDataItems();

  // Description:
  // The file instances.
  vtkVVFileInstancePool *FileInstancePool;

  // Description:
  // The snapshots.
  vtkVVSnapshotPool *SnapshotPool;

  // Description:
  // Populate the menus.
  virtual void PopulateFileMenu();
  virtual void PopulateEditMenu();
  virtual void PopulateWindowMenu();
  virtual void PopulateHelpMenu();

  // Description:
  // Update window title (using data name for example)
  virtual void UpdateTitle();

  // Description:
  // Pack startup page
  virtual void PackStartupPageWidget();
  
private:

  // Description:
  // Some constants
  char *FileOpenMenuLabel;
  char *FileSaveScreenshotMenuLabel;
  char *FileSaveSessionMenuLabel;
  char *FileSaveVolumeMenuLabel;
  char *FileSaveFiducialsMenuLabel;
  char *FilePrintAllMenuLabel;
  char *FilePrintSelectedMenuLabel;
  char *FileLauncheExternalApplicationMenuLabel;
  char *EditCopyScreenshotMenuLabel;
  char *HelpRegisterMenuLabel;
  char *HelpEmailFeedbackMenuLabel;
  char *FileCloseSelectedDataMenuLabel;
  //BTX
  static const char *PaintbrushNotSavedInSnapshotDialogName;
  //ETX

  // Description:
  // The data items
  vtkVVDataItemPool *DataItemPool;

  vtkVVWindowBase(const vtkVVWindowBase&); // Not implemented
  void operator=(const vtkVVWindowBase&); // Not implemented
};

#endif
