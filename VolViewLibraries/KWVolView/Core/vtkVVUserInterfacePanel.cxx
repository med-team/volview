/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVUserInterfacePanel.h"

#include "vtkObjectFactory.h"
#include "vtkVVWindowBase.h"

//---------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVUserInterfacePanel);
vtkCxxRevisionMacro(vtkVVUserInterfacePanel, "$Revision: 1.5 $");

//---------------------------------------------------------------------------
vtkVVUserInterfacePanel::vtkVVUserInterfacePanel()
{
  this->Window = NULL;
}

//---------------------------------------------------------------------------
vtkVVUserInterfacePanel::~vtkVVUserInterfacePanel()
{
  this->SetWindow(NULL);
}

//---------------------------------------------------------------------------
void vtkVVUserInterfacePanel::SetWindow(vtkVVWindowBase *arg)
{
  if (this->Window == arg)
    {
    return;
    }
  this->Window = arg;
  this->Modified();

  this->Update();
}

//---------------------------------------------------------------------------
void vtkVVUserInterfacePanel::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << indent << "Window: " << this->Window << endl;
}

