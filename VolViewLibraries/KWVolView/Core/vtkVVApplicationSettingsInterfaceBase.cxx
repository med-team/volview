/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVApplicationSettingsInterfaceBase.h"
#include "vtkObjectFactory.h"

#include "vtkKWApplication.h"
#include "vtkKWCheckButton.h"
#include "vtkKWEntry.h"
#include "vtkKWEntryWithLabel.h"
#include "vtkKWFrameWithLabel.h"
#include "vtkKWInternationalization.h"
#include "vtkKWLoadSaveButton.h"
#include "vtkKWLoadSaveDialog.h"

#include "vtkVVApplication.h"
#include "vtkVVWindowBase.h"
#include "vtkVVSelectionFrameLayoutManager.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVApplicationSettingsInterfaceBase);
vtkCxxRevisionMacro(vtkVVApplicationSettingsInterfaceBase, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkVVApplicationSettingsInterfaceBase::vtkVVApplicationSettingsInterfaceBase()
{
  // Graphics settings

  this->UseAlphaChannelInScreenshotCheckButton = NULL;

  // External Applications

  this->ExternalApplicationsFrame = NULL;
  this->ExternalApplicationLoadSaveButton = NULL;
  this->ExternalApplicationParametersEntry = NULL;
}

//----------------------------------------------------------------------------
vtkVVApplicationSettingsInterfaceBase::~vtkVVApplicationSettingsInterfaceBase()
{
  // Graphics settings

  if (this->UseAlphaChannelInScreenshotCheckButton)
    {
    this->UseAlphaChannelInScreenshotCheckButton->Delete();
    this->UseAlphaChannelInScreenshotCheckButton = NULL;
    }

  // External Applications

  if (this->ExternalApplicationsFrame)
    {
    this->ExternalApplicationsFrame->Delete();
    this->ExternalApplicationsFrame = NULL;
    }

  if (this->ExternalApplicationLoadSaveButton)
    {
    this->ExternalApplicationLoadSaveButton->Delete();
    this->ExternalApplicationLoadSaveButton = NULL;
    }

  if (this->ExternalApplicationParametersEntry)
    {
    this->ExternalApplicationParametersEntry->Delete();
    this->ExternalApplicationParametersEntry = NULL;
    }
}

// ---------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::Create()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("The panel is already created.");
    return;
    }

  this->Superclass::Create();

  ostrstream tk_cmd;

  vtkKWWidget *page;
  vtkKWFrame *frame;

  page = this->GetPageWidget(this->GetName());
  
  // --------------------------------------------------------------
  // Graphics settings : main frame
  
  frame = this->GraphicsSettingsFrame->GetFrame();

  // --------------------------------------------------------------
  // Graphics settings : Use Alpha Channel In Screenshot

  if (!this->UseAlphaChannelInScreenshotCheckButton)
    {
    this->UseAlphaChannelInScreenshotCheckButton = vtkKWCheckButton::New();
    }

  this->UseAlphaChannelInScreenshotCheckButton->SetParent(frame);
  this->UseAlphaChannelInScreenshotCheckButton->Create();

  this->UseAlphaChannelInScreenshotCheckButton->SetText(
    ks_("Application Settings|Use Alpha channel in screenshot"));
  this->UseAlphaChannelInScreenshotCheckButton->SetCommand(
    this, "UseAlphaChannelInScreenshotCallback");
  this->UseAlphaChannelInScreenshotCheckButton->SetBalloonHelpString(
    k_("Turn this settings ON to make sure all screenshots are saved with "
       "their alpha channel. This may be useful to blend a screenshot with "
       "a different background illustration."));

  tk_cmd 
    << "pack " 
    << this->UseAlphaChannelInScreenshotCheckButton->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none" << endl;

  // --------------------------------------------------------------
  // External Applications : main frame

  if (!this->ExternalApplicationsFrame)
    {
    this->ExternalApplicationsFrame = vtkKWFrameWithLabel::New();
    }

  this->ExternalApplicationsFrame->SetParent(this->GetPagesParentWidget());
  this->ExternalApplicationsFrame->Create();
  this->ExternalApplicationsFrame->SetLabelText(
    ks_("Application Settings|External Application"));
    
  tk_cmd << "pack " << this->ExternalApplicationsFrame->GetWidgetName()
         << " -side top -anchor nw -fill x -padx 2 -pady 2 " 
         << " -in " << page->GetWidgetName() << endl;
  
  frame = this->ExternalApplicationsFrame->GetFrame();

  // --------------------------------------------------------------
  // External Applications : full path to the app

  if (!this->ExternalApplicationLoadSaveButton)
    {
    this->ExternalApplicationLoadSaveButton = vtkKWLoadSaveButton::New();
    }

  this->ExternalApplicationLoadSaveButton->SetParent(frame);
  this->ExternalApplicationLoadSaveButton->Create();
  this->ExternalApplicationLoadSaveButton->SetText(
    "Click to pick an executable");
  this->ExternalApplicationLoadSaveButton->SetBalloonHelpString(
    k_("Full path to the external application executable."));
  this->ExternalApplicationLoadSaveButton->SetCommand(
    this, "ExternalApplicationPathCallback");
  vtkKWLoadSaveDialog *dlg = 
    this->ExternalApplicationLoadSaveButton->GetLoadSaveDialog();
  dlg->SaveDialogOff(); 
#ifdef _WIN32
  dlg->SetFileTypes("{{Executable} {.exe}}");
#else
  dlg->SetFileTypes("{{Executable} {*}}");
#endif
  tk_cmd 
    << "pack " 
    << this->ExternalApplicationLoadSaveButton->GetWidgetName()
    << "  -side top -anchor w -expand no -fill none -padx 2 -pady 2" << endl;

  // --------------------------------------------------------------
  // External Applications : parameters

  if (!this->ExternalApplicationParametersEntry)
    {
    this->ExternalApplicationParametersEntry = vtkKWEntryWithLabel::New();
    }

  this->ExternalApplicationParametersEntry->SetParent(frame);
  this->ExternalApplicationParametersEntry->Create();
  this->ExternalApplicationParametersEntry->SetLabelText("Parameters");
  this->ExternalApplicationParametersEntry->SetBalloonHelpString(
    k_("Parameters to be passed to the application. Any instance of #f will be replaced with the full-path to the currently selected dataset, *if* it was loaded from a file (as opposed to created from a plugin or from a promoted label map). Any instance of #l will be replaced with the full-path to the last saved label map. IMPORTANT: note that the paths are not quoted, any path containing a space is likely to be interpreted as multiple space-separated parameters. Make sure to include quotes in this field around #f or #l if needed."));
  this->ExternalApplicationParametersEntry->GetWidget()->SetCommand(
    this, "ExternalApplicationParametersCallback");
  this->ExternalApplicationParametersEntry->GetWidget()->SetWidth(40);

  tk_cmd 
    << "pack " 
    << this->ExternalApplicationParametersEntry->GetWidgetName()
    << "  -side top -anchor w -expand yes -fill x -padx 2 -pady 2" << endl;

  // Pack 

  tk_cmd << ends;
  this->Script(tk_cmd.str());
  tk_cmd.rdbuf()->freeze(0);

  // Update according to the current Window

  this->Update();
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::Update()
{
  this->Superclass::Update();

  if (!this->IsCreated())
    {
    return;
    }

  vtkVVApplication *vvapp = 
    vtkVVApplication::SafeDownCast(this->GetApplication());

  vtkVVWindowBase *win = 
    vtkVVWindowBase::SafeDownCast(this->GetApplication()->GetNthWindow(0));

  vtkVVSelectionFrameLayoutManager *layout_mgr = 
    win ? win->GetDataSetWidgetLayoutManager() : NULL;

  // Graphics settings : Use Alpha Channel In Screenshot

  if (this->UseAlphaChannelInScreenshotCheckButton)
    {
    this->UseAlphaChannelInScreenshotCheckButton->SetSelectedState(
      layout_mgr ? layout_mgr->GetUseAlphaChannelInScreenshot() : 0);
    this->UseAlphaChannelInScreenshotCheckButton->SetEnabled(
      !layout_mgr ? 0 : this->GetEnabled());
    }

  // External Applications : full path to the app

  if (this->ExternalApplicationLoadSaveButton && vvapp && 
      vvapp->GetExternalApplicationPath())
    {
    this->ExternalApplicationLoadSaveButton->SetInitialFileName(
      vvapp->GetExternalApplicationPath());
    }

  // External Applications : parameters

  if (this->ExternalApplicationParametersEntry && vvapp &&
      vvapp->GetExternalApplicationParameters())
    {
    this->ExternalApplicationParametersEntry->GetWidget()->SetValue(
      vvapp->GetExternalApplicationParameters());
    }
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::UseAlphaChannelInScreenshotCallback(
  int state)
{
  vtkVVWindowBase *win = 
    vtkVVWindowBase::SafeDownCast(this->GetApplication()->GetNthWindow(0));

  vtkVVSelectionFrameLayoutManager *layout_mgr = 
    win ? win->GetDataSetWidgetLayoutManager() : NULL;

  if (layout_mgr)
    {
    layout_mgr->SetUseAlphaChannelInScreenshot(state);
    }
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::ExternalApplicationParametersCallback(
  const char *value)
{
  vtkVVApplication *vvapp = 
    vtkVVApplication::SafeDownCast(this->GetApplication());
  if (vvapp && value)
    {
    vvapp->SetExternalApplicationParameters(value);
    }
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::ExternalApplicationPathCallback()
{
  vtkVVApplication *vvapp = 
    vtkVVApplication::SafeDownCast(this->GetApplication());
  if (vvapp && this->ExternalApplicationLoadSaveButton)
    {
    vvapp->SetExternalApplicationPath(
      this->ExternalApplicationLoadSaveButton->GetFileName());
    vtkVVWindowBase *win = 
      vtkVVWindowBase::SafeDownCast(this->GetApplication()->GetNthWindow(0));
    win->UpdateMenuState();
    }
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

  // Graphics settings

  if (this->UseAlphaChannelInScreenshotCheckButton)
    {
    this->UseAlphaChannelInScreenshotCheckButton->SetEnabled(
      this->GetEnabled());
    }

  // External Applications

  if (this->ExternalApplicationsFrame)
    {
    this->ExternalApplicationsFrame->SetEnabled(this->GetEnabled());
    }

  if (this->ExternalApplicationLoadSaveButton)
    {
    this->ExternalApplicationLoadSaveButton->SetEnabled(this->GetEnabled());
    }

  if (this->ExternalApplicationParametersEntry)
    {
    this->ExternalApplicationParametersEntry->SetEnabled(this->GetEnabled());
    }
}

//----------------------------------------------------------------------------
void vtkVVApplicationSettingsInterfaceBase::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

