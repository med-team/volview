/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVApplicationSettingsInterface - a user interface panel.
// .SECTION Description
// Extends the user interface defined in vtkVVApplicationSettingsInterfaceBase.
// .SECTION See Also

#ifndef __vtkVVApplicationSettingsInterface_h
#define __vtkVVApplicationSettingsInterface_h

#include "vtkVVApplicationSettingsInterfaceBase.h"

class vtkKWRadioButtonSet;

class VTK_EXPORT vtkVVApplicationSettingsInterface : public vtkVVApplicationSettingsInterfaceBase
{
public:
  static vtkVVApplicationSettingsInterface* New();
  vtkTypeRevisionMacro(vtkVVApplicationSettingsInterface,
                       vtkVVApplicationSettingsInterfaceBase);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();
  
  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

protected:
  vtkVVApplicationSettingsInterface();
  ~vtkVVApplicationSettingsInterface();

private:
  vtkVVApplicationSettingsInterface(const vtkVVApplicationSettingsInterface&); // Not implemented
  void operator=(const vtkVVApplicationSettingsInterface&); // Not Implemented
};

#endif

