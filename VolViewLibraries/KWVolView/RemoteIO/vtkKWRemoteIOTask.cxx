/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWRemoteIOTask.h"

vtkCxxRevisionMacro(vtkKWRemoteIOTask, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkKWRemoteIOTask);

// vtkKWRemoteIOTask* vtkKWRemoteIOTask::New()
// {
//   // First try to create the object from the vtkObjectFactory
//   vtkObject* ret = vtkObjectFactory::CreateInstance("vtkKWRemoteIOTask");
//   if(ret)
//     {
//       return (vtkKWRemoteIOTask*)ret;
//     }
//   // If the factory was unable to create the object, then create it here.
//   return new vtkKWRemoteIOTask;
// }

vtkKWRemoteIOTask
::vtkKWRemoteIOTask()
{
  this->TaskObject = 0;
  this->TaskFunction = 0;
  this->Type = vtkKWRemoteIOTask::Undefined;
}

vtkKWRemoteIOTask
::~vtkKWRemoteIOTask()
{
}


void
vtkKWRemoteIOTask
::SetTaskFunction(vtkKWRemoteIOManager *object,
                  vtkKWRemoteIOTask::TaskFunctionPointer function,
                  void *clientdata)
{
  this->TaskObject = object;
  this->TaskFunction = function;
  this->TaskClientData = clientdata;
}

void
vtkKWRemoteIOTask
::Execute()
{
  if (this->TaskObject)
    {
    ((*this->TaskObject).*(this->TaskFunction))(this->TaskClientData);
    }
}


void
vtkKWRemoteIOTask
::PrintSelf(ostream& os, vtkIndent indent)
{
}
