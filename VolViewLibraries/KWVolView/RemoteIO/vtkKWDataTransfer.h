/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __vtkKWDataTransfer_h
#define __vtkKWDataTransfer_h

#include "vtkObject.h"
#include "vtkObjectFactory.h"
#include "vtkURIHandler.h"

class vtkKWDataTransferCommand;

class VTK_EXPORT vtkKWDataTransfer : public vtkObject 
{
public:
  static vtkKWDataTransfer *New();
  vtkTypeRevisionMacro(vtkKWDataTransfer, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Source URI hosted on the server. 
  //   Example:
  //     http://..../Paper001/foo.mha
  //     http://..../Paper001/foo.tar.gz
  vtkGetStringMacro(SourceURI);
  vtkSetStringMacro(SourceURI);
  
  // Description:
  // Destination URI downloaded to the client
  //   Example:
  //     /home/karthik/.VolView1.4/Paper001/foo.mha
  //     c:/...../foo.tar.gz
  vtkGetStringMacro(DestinationURI);
  vtkSetStringMacro(DestinationURI);

  // Description:
  // Optional - The identifier can be used for any arbitrary purpose and is 
  // not used by this class.
  vtkSetStringMacro(Identifier);
  vtkGetStringMacro(Identifier);
  
  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetMacro(TransferType, int);
  vtkSetMacro(TransferType, int);
  
  // Description:
  // Is the transfer synchronous or asynchronous ? Default is synchronous.
  // Asynchronous transfers are populated on a seperate non-blocking thread.
  vtkSetMacro(Asynchronous, int);
  vtkGetMacro(Asynchronous, int);

  // Description:
  // Downloaded files ending in .tar will automatically be untarred, unless
  // this is disabled. This allows you to download a whole shebang of files
  // from the server, when it is stored as a tar archive.
  vtkSetMacro(DisableTar, int);
  vtkGetMacro(DisableTar, int);
  
  // Description:
  // Get the handler. If the Source URI and the DestinationURI has been
  // set, this will create one, if it does not exist.
  virtual vtkURIHandler* GetHandler();
  virtual void SetHandler(vtkURIHandler *);

  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetMacro(TransferID, int);
  vtkSetMacro(TransferID, int);
  
  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetStringMacro(TransferNodeID);
  vtkSetStringMacro(TransferNodeID);  
  
  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetMacro(TransferStatus, int);
  vtkSetMacro(TransferStatus, int);

  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetMacro(CancelRequested, int);
  vtkSetMacro(CancelRequested, int);

  // Description:
  // Don't you wish that method was documented? Me too.
  vtkGetMacro(TransferCached, int);
  vtkSetMacro(TransferCached, int);

  // Description:
  // Don't you wish that method was documented? Me too.
  void SetTransferStatusNoModify(int val)
    {
    this->TransferStatus = val;
    }

  const char* GetTransferStatusString() 
    {
    switch (this->TransferStatus)
      {
      case vtkKWDataTransfer::Idle: return "Idle";
      case vtkKWDataTransfer::CancelPending: return "CancelPending";
      case vtkKWDataTransfer::Pending: return "Pending";
      case vtkKWDataTransfer::Running: return "Running";
      case vtkKWDataTransfer::Completed: return "Completed";
      case vtkKWDataTransfer::CompletedWithErrors: return "CompletedWithErrors";
      case vtkKWDataTransfer::TimedOut: return "TimedOut";
      case vtkKWDataTransfer::Cancelled: return "Cancelled";
      case vtkKWDataTransfer::Deleted: return "Deleted";
      case vtkKWDataTransfer::Ready: return "Ready";
      }
    return "Unknown";
    }

  const char* GetTransferTypeString() 
    {
    switch (this->TransferType)
      {
      case vtkKWDataTransfer::RemoteDownload: return "RemoteDownload";
      case vtkKWDataTransfer::RemoteUpload: return "RemoteUpload";
      case vtkKWDataTransfer::LocalLoad: return "LocalUpload";
      case vtkKWDataTransfer::LocalSave: return "LocalSave";
      case vtkKWDataTransfer::Unspecified: return "Unspecified";
      }
    return "Unknown";
    }


  //BTX
  // transfer status values
  // ready means ready to read into storage node
  enum
    {
    Idle=0,
    Pending,
    Running,
    Completed,
    CompletedWithErrors,
    CancelPending,
    Cancelled,
    Ready,
    Deleted,
    TimedOut
    };
  // transfer type values
  enum
    {
    RemoteDownload=0,
    RemoteUpload,
    LocalLoad,
    LocalSave,
    Unspecified
    };
  //ETX

  // Description:
  // INTERNAL - Do not use
  // Periodically invoked by a timer callback with vtkKWRemoteIOManager to 
  // check if the transfer status has changed and we can notify the main 
  // thread if it has. Typically the transfers occur on a seperate thread.
  // Returns 1 if the status has changed since the last check.
  int CheckAndUpdateTransferStatus();

protected:
  vtkKWDataTransfer();
  virtual ~vtkKWDataTransfer();

  // Description:
  char *SourceURI;
  char *DestinationURI;
  char *Identifier;
  vtkURIHandler *Handler;
  int TransferStatus;
  int TransferID;
  int TransferType;
  int TransferCached;
  char* TransferNodeID;
  int CancelRequested;
  int Asynchronous;
  int LastCheckedTransferStatus;
  int DisableTar;
  vtkKWDataTransferCommand *EventForwarderCommand;

private:
  vtkKWDataTransfer(const vtkKWDataTransfer&);
  void operator=(const vtkKWDataTransfer&);
};

#endif

