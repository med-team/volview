/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __vtkURIHandler_h
#define __vtkURIHandler_h

#include <vtksys/SystemTools.hxx>
#include <vtksys/Process.h>
#include "vtkObject.h"

#include <math.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

class VTK_EXPORT vtkURIHandler : public vtkObject 
{
public:
  // The Usual vtk class functions
  static vtkURIHandler *New() { return NULL; };
  vtkTypeRevisionMacro(vtkURIHandler, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // virtual methods to be defined in subclasses.
  // (Maybe these should be defined to handle default file operations)
  virtual void StageFileRead ( const char *source, const char * destination ) { };
  virtual void StageFileWrite ( const char *source, const char * destination ) { };

  // Description:
  // Get Progress.
  vtkGetMacro( Progress, double );
  vtkGetStringMacro( EstimatedTimeRemaining );

  // Description:
  // Get the size(s) in bytes of the total data to download and the data so far
  // downloaded.
  vtkGetMacro( TotalDownloadSize, double );
  vtkGetMacro( DownloadedSize, double );

  // Description:
  // Get the download fraction as a pretty string for instance
  //   10 bytes / 3 MB 
  // etc.
  const char * GetFormattedDownloadFractionAsString();

  //BTX
  // Description:
  // Determine whether protocol is appropriate for this handler.
  // NOTE: Subclasses should implement this method
  virtual int CanHandleURI ( const char *uri ) = 0;

  // Description:
  // This function writes the downloaded data in a buffered manner
  size_t BufferedWrite ( char *buffer, size_t size, size_t nitems );

  // Description:
  // Use this function to set LocalFile
  //virtual void SetLocalFile ( std::ofstream * localFile );
  virtual void SetLocalFile (FILE *localFile);
  //ETX

  // Description:
  // INTERNAL - do not use
  void SetProgress( double f );
  void SetTotalDownloadSize( double ); 
  void SetDownloadedSize( double ); 
  char EstimatedTimeRemaining[12];

private:

  //--- Methods to configure and close transfer
  // NOTE: Subclasses should implement these method
  virtual void InitTransfer ( ) {};
  virtual int CloseTransfer ( )
      {
      return 0;
      }


protected:
  vtkURIHandler();
  virtual ~vtkURIHandler();
  vtkURIHandler(const vtkURIHandler&);
  void operator=(const vtkURIHandler&);

  // Description:
  // local file, it gets passed to C functions in libcurl 
  //BTX
  //std::ofstream* LocalFile;
  //ETX
  FILE *LocalFile;

  double Progress;
  double LastReportedProgress;
  double TotalDownloadSize;
  double DownloadedSize;

private:

  //BTX
  vtkstd::string FormattedDownloadFractionString;
  //ETX
};

#endif






