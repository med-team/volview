/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWRemoteIOUtilities - helper class to cram unrelated functions.
// .SECTION Description
// Helper class to cram unclassifiable functions..
//
// All methods here are of course static 

#ifndef __vtkKWRemoteIOUtilities_h
#define __vtkKWRemoteIOUtilities_h

#include "vtkObject.h"
#include <vtkstd/vector>
#include <vtkstd/string>


class VTK_EXPORT vtkKWRemoteIOUtilities: public vtkObject
{
public:
  static vtkKWRemoteIOUtilities * New();
  vtkTypeRevisionMacro(vtkKWRemoteIOUtilities, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  //BTX
  // Create a tar archive of a series of files or folders.
  static int CreateTar(const char* outFileName, 
                       const std::vector<vtkstd::string> & filesOrFolders,
                       const char* rootDirectory,
                       bool gzip, bool verbose = 0);

  // Description:
  // Extract tarballname into the folder outDir. If outDir is NULL, the tar
  // ball is extracted into the current directory.
  // A tar.gz may be specified as well, if you turn on gunzip.
  static int ExtractTar( const char *tarballname, 
                         const char *outDir, 
                         bool gzip );
  //ETX
  
  // Description:
  // Rename a file
  static int RenameFile(const char* oldname, const char* newname);  

protected:
  vtkKWRemoteIOUtilities();
  ~vtkKWRemoteIOUtilities();

private:
  vtkKWRemoteIOUtilities(const vtkKWRemoteIOUtilities &); // Not implemented
  void      operator=(const vtkKWRemoteIOUtilities &); // Not implemented
};

#endif

