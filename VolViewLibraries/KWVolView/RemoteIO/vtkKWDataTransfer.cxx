/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWDataTransfer.h"
#include "vtkKWRemoteIOManager.h"
#include "vtkHTTPHandler.h"
#include "vtkEventForwarderCommand.h"

vtkStandardNewMacro ( vtkKWDataTransfer );
vtkCxxRevisionMacro ( vtkKWDataTransfer, "$Revision: 1.11 $" );

//----------------------------------------------------------------------------
class vtkKWDataTransferCommand : public vtkCommand
{
public:
  static vtkKWDataTransferCommand * New() 
    { return new vtkKWDataTransferCommand; }

  virtual void Execute( vtkObject *caller, 
                        unsigned long event, 
                        void* calldata )
    {
    this->DataTransfer->InvokeEvent( 
        vtkKWRemoteIOManager::TransferUpdateEvent, this->DataTransfer );
    }
  vtkKWDataTransfer * DataTransfer;
};

//----------------------------------------------------------------------------
vtkKWDataTransfer::vtkKWDataTransfer()
{
  this->SourceURI = NULL;
  this->DestinationURI = NULL;
  this->Identifier = NULL;
  this->Handler = NULL;
  this->TransferStatus = vtkKWDataTransfer::Ready;
  this->TransferID = -1;
  this->TransferType = vtkKWDataTransfer::Unspecified;
  this->TransferNodeID = NULL;
  this->CancelRequested = 0;
  this->TransferCached = 0;
  this->Asynchronous = 0;
  this->LastCheckedTransferStatus = this->TransferStatus;
  this->DisableTar = 0;
  this->EventForwarderCommand = vtkKWDataTransferCommand::New();
  this->EventForwarderCommand->DataTransfer = this;
}

//----------------------------------------------------------------------------
vtkKWDataTransfer::~vtkKWDataTransfer()
{
  this->SetIdentifier(NULL);
  this->SetSourceURI(NULL);
  this->SetDestinationURI(NULL);
  this->SetHandler(NULL);
  this->TransferStatus = vtkKWDataTransfer::Ready;
  this->TransferID = -1;
  this->TransferType = vtkKWDataTransfer::Unspecified;
  this->TransferNodeID = NULL;
  this->CancelRequested = 0;
  this->TransferCached = 0;
  this->EventForwarderCommand->Delete();
}

//----------------------------------------------------------------------------
vtkURIHandler * vtkKWDataTransfer::GetHandler()
{
  if (this->SourceURI && !this->Handler)
    {
    // Create one if it does not exist.
    vtkHTTPHandler *httpHandler = vtkHTTPHandler::New();
    if (httpHandler->CanHandleURI(this->SourceURI))
      {
      this->SetHandler(httpHandler);
      }
    httpHandler->Delete();
    }

  return this->Handler;
}

//----------------------------------------------------------------------------
int vtkKWDataTransfer::CheckAndUpdateTransferStatus()
{
  if (this->LastCheckedTransferStatus != this->TransferStatus)
    {
    this->LastCheckedTransferStatus = this->TransferStatus;
    return 1;
    }
  return 0;
}

//----------------------------------------------------------------------------
void vtkKWDataTransfer::SetHandler( vtkURIHandler * args )
{
  if (this->Handler != args)
    {                                                           
    vtkURIHandler* tempSGMacroVar = this->Handler;
    this->Handler = args;                                          
    if (this->Handler != NULL) { this->Handler->Register(this); }     
    if (tempSGMacroVar != NULL)                                 
      {                                                         
      tempSGMacroVar->UnRegister(this);                         
      }                                                         
    this->Modified();                                           

    if (this->Handler)
      {
      this->Handler->AddObserver( vtkKWRemoteIOManager::TransferUpdateEvent,
                                  this->EventForwarderCommand );
      }
    }                                                           
}

//----------------------------------------------------------------------------
void vtkKWDataTransfer::PrintSelf(ostream& os, vtkIndent indent)
{
  Superclass::PrintSelf ( os, indent );
  os << indent << "SourceURI: " <<
    ( this->SourceURI ? this->SourceURI : "(none)") << "\n";
  os << indent << "DestinationURI: " <<
    ( this->DestinationURI ? this->DestinationURI : "(none)") << "\n";
  os << indent << "Identifier: " <<
    ( this->Identifier ? this->Identifier : "(none)") << "\n";
  os << indent << "Handler: " << this->Handler << "\n";
  os << indent << "TransferCached: " << this->GetTransferCached() << "\n";
  os << indent << "TransferStatus: " << this->GetTransferStatus() << "\n";
  os << indent << "CancelRequested: " << this->GetCancelRequested() << "\n";
  os << indent << "TransferID: " << this->GetTransferID() << "\n";
  os << indent << "TransferType: " << this->GetTransferType() << "\n";
  os << indent << "TransferNodeID: " << this->GetTransferNodeID() << "\n";
  os << indent << "Asynchronous: " << this->Asynchronous << "\n";
}

