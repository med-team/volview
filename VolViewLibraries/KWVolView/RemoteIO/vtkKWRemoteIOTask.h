/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __vtkKWRemoteIOTask_h
#define __vtkKWRemoteIOTask_h

#include "vtkObject.h"
#include "vtkSmartPointer.h"
#include "vtkKWRemoteIOManager.h"

class VTK_EXPORT vtkKWRemoteIOTask : public vtkObject
{
public:
  static vtkKWRemoteIOTask *New();
  vtkTypeRevisionMacro(vtkKWRemoteIOTask,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);

  //BTX
  typedef void (vtkKWRemoteIOManager::*TaskFunctionPointer)(void *clientdata);
  //ETX
  
  // Description:
  // Set the function and object to call for the task.
  void SetTaskFunction(vtkKWRemoteIOManager*, 
                       TaskFunctionPointer, 
                       void *clientdata);
  
  // Description:
  // Execute the task.
  virtual void Execute();

  // Description:
  // The type of task - this can be used, for example, to decide
  // how many concurrent threads should be allowed
  //BTX
  enum
    {
    Undefined = 0,
    Processing,
    Networking
    };
  //ETX
 
  vtkSetClampMacro (Type, int, vtkKWRemoteIOTask::Undefined, vtkKWRemoteIOTask::Networking);
  vtkGetMacro (Type, int);
  void SetTypeToProcessing() {this->SetType(vtkKWRemoteIOTask::Processing);};
  void SetTypeToNetworking() {this->SetType(vtkKWRemoteIOTask::Networking);};

  const char* GetTypeAsString( ) {
    switch (this->Type)
      {
      case vtkKWRemoteIOTask::Undefined: return "Undefined";
      case vtkKWRemoteIOTask::Processing: return "Processing";
      case vtkKWRemoteIOTask::Networking: return "Networking";
      }
    return "Unknown";
  }

protected:
  vtkKWRemoteIOTask();
  virtual ~vtkKWRemoteIOTask();

private:
  //BTX
  vtkSmartPointer< vtkKWRemoteIOManager > TaskObject;
  TaskFunctionPointer TaskFunction;
  void *TaskClientData;
  //ETX
  
  int Type;

  vtkKWRemoteIOTask(const vtkKWRemoteIOTask&);
  void operator=(const vtkKWRemoteIOTask&);  
};
#endif


