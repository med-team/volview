##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
MACRO(KWVolView_TEST_LOAD_VOLUME TEST_NAME EXECUTABLE APPLICATION_NAME DATA BASELINE)
  MAKE_DIRECTORY("${PROJECT_BINARY_DIR}/Testing/Temporary")
  ADD_TEST(${TEST_NAME} ${EXECUTABLE} -L 0
    ${KWVolView_TESTING_DIR}/Tcl/KWVolViewLoadVolumeAndGrabScreenshot.tcl
    ${DATA}
    ${APPLICATION_NAME}
    -T ${PROJECT_BINARY_DIR}/Testing/Temporary
    -C ${KWVolView_TESTING_DIR}/Tcl/KWVolViewDiffImages.tcl
    -V ${BASELINE}
    --g=1024x768+0+0
    )
ENDMACRO(KWVolView_TEST_LOAD_VOLUME)
