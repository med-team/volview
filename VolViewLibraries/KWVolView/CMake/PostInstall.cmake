##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

set(CMDLINE "find" )
EXECUTE_PROCESS(COMMAND ${CMDLINE} . -iname *.dylib -exec ln -s {} \; -print
   WORKING_DIRECTORY "${CMAKE_INSTALL_PREFIX}/lib"
   OUTPUT_VARIABLE RESULT_STREAM 
   ERROR_VARIABLE ERROR_STREAM)

