##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
# Generate the KWVolViewConfig.cmake file in the build tree. Also configure
# one for installation. The file tells external projects how to use
# VolView.

# Help store a literal dollar in a string.  CMake 2.2 allows escaped
# dollars but we have to support CMake 2.0.
SET(DOLLAR "$")

#-----------------------------------------------------------------------------
# Settings shared between the build tree and install tree.

EXPORT_LIBRARY_DEPENDENCIES(
  ${KWVolView_BINARY_DIR}/KWVolViewLibraryDepends.cmake)
INCLUDE(${CMAKE_ROOT}/Modules/CMakeExportBuildSettings.cmake)
CMAKE_EXPORT_BUILD_SETTINGS(
  ${KWVolView_BINARY_DIR}/KWVolViewBuildSettings.cmake)

IF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
  INSTALL_FILES(${KWVolView_INSTALL_PACKAGE_DIR} FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/UseKWVolView.cmake
    ${KWVolView_BINARY_DIR}/KWVolViewLibraryDepends.cmake
    ${KWVolView_BINARY_DIR}/KWVolViewBuildSettings.cmake
    )
ENDIF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)

IF(WIN32)
  SET(EXE_EXT ".exe")
ELSE(WIN32)
  SET(EXE_EXT)
ENDIF(WIN32)

#-----------------------------------------------------------------------------
# Settings specific to the build tree.

# The install-only section is empty for the build tree.
SET(KWVolView_CONFIG_INSTALL_ONLY)

# The "use" file.
SET(KWVolView_USE_FILE_CONFIG 
  ${CMAKE_CURRENT_SOURCE_DIR}/UseKWVolView.cmake)

# The build settings file.
SET(KWVolView_BUILD_SETTINGS_FILE_CONFIG 
  ${KWVolView_BINARY_DIR}/KWVolViewBuildSettings.cmake)

# The library directories.
SET(KWVolView_LIBRARY_DIRS_CONFIG ${KWVolView_LIBRARY_DIRS})

# The runtime directories.
SET(KWVolView_RUNTIME_DIRS_CONFIG ${KWVolView_RUNTIME_DIRS})

# The include directories.
SET(KWVolView_INCLUDE_DIRS_CONFIG ${KWVolView_INCLUDE_PATH})

# The library dependencies file.
SET(KWVolView_LIBRARY_DEPENDS_FILE 
  ${KWVolView_BINARY_DIR}/KWVolViewLibraryDepends.cmake)

# The templates dir.
SET(KWVolView_TEMPLATES_DIR_CONFIG 
  ${KWVolView_TEMPLATES_DIR})

# The CMake macros dir.
SET(KWVolView_CMAKE_DIR_CONFIG 
  ${KWVolView_CMAKE_DIR})

# The testing dir.
SET(KWVolView_TESTING_DIR_CONFIG 
  ${KWVolView_TESTING_DIR})

# The KWWidgetsPro options.
SET(KWVolView_KWWidgetsPro_DIR_CONFIG ${KWWidgetsPro_DIR})

# The VTKEdge options.
SET(KWVolView_VTKEdge_DIR_CONFIG ${VTKEdge_DIR})

# The GDCM options.
SET(KWVolView_GDCM_DIR_CONFIG ${GDCM_DIR})

# The mouse recorder driver.
SET(KWVolView_MOUSE_RECORDER_DRIVER_EXE_CONFIG 
  ${KWVolView_MOUSE_RECORDER_DRIVER_EXE})

# The build configuration information.
SET(KWVolView_CONFIGURATION_TYPES_CONFIG ${KWVolView_CONFIGURATION_TYPES})
SET(KWVolView_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})

# Configure KWVolViewConfig.cmake for the build tree.
CONFIGURE_FILE(
  ${CMAKE_CURRENT_SOURCE_DIR}/KWVolViewConfig.cmake.in
  ${KWVolView_BINARY_DIR}/KWVolViewConfig.cmake @ONLY IMMEDIATE)

#-----------------------------------------------------------------------------
# Settings specific to the install tree.

# The "use" file.
SET(KWVolView_USE_FILE_CONFIG 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_PACKAGE_DIR}/UseKWVolView.cmake)

# The build settings file.
SET(KWVolView_BUILD_SETTINGS_FILE_CONFIG 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_PACKAGE_DIR}/KWVolViewBuildSettings.cmake)

# The library directories.
IF(CYGWIN AND KWVolView_BUILD_SHARED_LIBS)
  # In Cygwin programs directly link to the .dll files.
  SET(KWVolView_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_BIN_DIR})
ELSE(CYGWIN AND KWVolView_BUILD_SHARED_LIBS)
  SET(KWVolView_LIBRARY_DIRS_CONFIG 
    ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_LIB_DIR})
ENDIF(CYGWIN AND KWVolView_BUILD_SHARED_LIBS)

# The runtime directories.
IF(WIN32)
  SET(KWVolView_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_BIN_DIR})
ELSE(WIN32)
  SET(KWVolView_RUNTIME_DIRS_CONFIG 
    ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_LIB_DIR})
ENDIF(WIN32)

# The include directories.
SET(KWVolView_INCLUDE_DIRS_CONFIG
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_INCLUDE_DIR})

# The library dependencies file.
SET(KWVolView_LIBRARY_DEPENDS_FILE 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_PACKAGE_DIR}/KWVolViewLibraryDepends.cmake)

# The templates dir.
SET(KWVolView_TEMPLATES_DIR_CONFIG 
 ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_INCLUDE_DIR}/Templates)

# The CMake macros dir.
SET(KWVolView_CMAKE_DIR_CONFIG 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_PACKAGE_DIR}/CMake)

# The resources dir.
SET(KWVolView_TESTING_DIR_CONFIG 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_DATA_DIR}/Testing)

# The KWWidgetsPro options.
# If KWWidgetsPro_BINARY_DIR is set, then VolView is part of a larger parent
# project that also builds KWWidgetsPro. In that case, 
# KWVolView_KWWidgetsPro_INSTALL_PACKAGE_DIR should be set to the value of
# KWWidgetsPro_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside KWWidgetsPro whose location will not change when we install.
IF(KWWidgetsPro_BINARY_DIR)
  IF(NOT KWVolView_KWWidgetsPro_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "KWVolView_KWWidgetsPro_INSTALL_PACKAGE_DIR must be set by the parent project to the value of KWWidgetsPro_INSTALL_PACKAGE_DIR it uses to configure KWWidgetsPro.")
  ENDIF(NOT KWVolView_KWWidgetsPro_INSTALL_PACKAGE_DIR)
  SET(KWVolView_KWWidgetsPro_DIR_CONFIG ${KWVolView_KWWidgetsPro_INSTALL_PACKAGE_DIR})
ELSE(KWWidgetsPro_BINARY_DIR)
  SET(KWVolView_KWWidgetsPro_DIR_CONFIG ${KWWidgetsPro_DIR})
ENDIF(KWWidgetsPro_BINARY_DIR)

# The VTKEdge options.
# If VTKEdge_BINARY_DIR is set, then VolView is part of a larger parent
# project that also builds VTKEdge. In that case, 
# KWVolView_VTKEdge_INSTALL_PACKAGE_DIR should be set to the value of
# VTKEdge_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside VTKEdge whose location will not change when we install.
IF(VTKEdge_BINARY_DIR)
  IF(NOT KWVolView_VTKEdge_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "KWVolView_VTKEdge_INSTALL_PACKAGE_DIR must be set by the parent project to the value of VTKEdge_INSTALL_PACKAGE_DIR it uses to configure VTKEdge.")
  ENDIF(NOT KWVolView_VTKEdge_INSTALL_PACKAGE_DIR)
  SET(KWVolView_VTKEdge_DIR_CONFIG ${KWVolView_VTKEdge_INSTALL_PACKAGE_DIR})
ELSE(VTKEdge_BINARY_DIR)
  SET(KWVolView_VTKEdge_DIR_CONFIG ${VTKEdge_DIR})
ENDIF(VTKEdge_BINARY_DIR)

# The GDCM options.
# If GDCM_BINARY_DIR is set, then KWVolView is part of a larger parent
# project that also builds GDCM. In that case, 
# KWVolView_GDCM_INSTALL_PACKAGE_DIR should be set to the value of
# GDCM_INSTALL_PACKAGE_DIR. Otherwise, if not set, then we are using an
# outside GDCM whose location will not change when we install.
IF(GDCM_BINARY_DIR)
  IF(NOT KWVolView_GDCM_INSTALL_PACKAGE_DIR)
    MESSAGE(
      "KWVolView_GDCM_INSTALL_PACKAGE_DIR must be set by the parent project to the value of GDCM_INSTALL_PACKAGE_DIR it uses to configure GDCM.")
  ENDIF(NOT KWVolView_GDCM_INSTALL_PACKAGE_DIR)
  SET(KWVolView_GDCM_DIR_CONFIG ${KWVolView_GDCM_INSTALL_PACKAGE_DIR})
ELSE(GDCM_BINARY_DIR)
  SET(KWVolView_GDCM_DIR_CONFIG ${GDCM_DIR})
ENDIF(GDCM_BINARY_DIR)

# The mouse recorder driver.
SET(KWVolView_MOUSE_RECORDER_DRIVER_EXE_CONFIG 
  ${DOLLAR}{KWVolView_INSTALL_PREFIX}${KWVolView_INSTALL_BIN_DIR}/KWMouseRecorderDriver${EXE_EXT})

# The build configuration information.
# The install tree only has one configuration.
SET(KWVolView_CONFIGURATION_TYPES_CONFIG)

# Configure KWVolViewConfig.cmake for the install tree.

# Construct the proper number of GET_FILENAME_COMPONENT(... PATH)
# calls to compute the installation prefix from KWVolView_DIR.
STRING(REGEX REPLACE "/" ";" KWVolView_INSTALL_PACKAGE_DIR_COUNT
  "${KWVolView_INSTALL_PACKAGE_DIR}")
SET(KWVolView_CONFIG_INSTALL_ONLY "
# Compute the installation prefix from KWVolView_DIR.
SET(KWVolView_INSTALL_PREFIX \"${DOLLAR}{KWVolView_DIR}\")
")
FOREACH(p ${KWVolView_INSTALL_PACKAGE_DIR_COUNT})
  SET(KWVolView_CONFIG_INSTALL_ONLY
    "${KWVolView_CONFIG_INSTALL_ONLY}GET_FILENAME_COMPONENT(KWVolView_INSTALL_PREFIX \"${DOLLAR}{KWVolView_INSTALL_PREFIX}\" PATH)\n"
    )
ENDFOREACH(p)

IF(CMAKE_CONFIGURATION_TYPES)
  # There are multiple build configurations.  Configure one
  # KWVolViewConfig.cmake for each configuration.
  FOREACH(config ${CMAKE_CONFIGURATION_TYPES})
    SET(KWVolView_BUILD_TYPE_CONFIG ${config})
    CONFIGURE_FILE(
      ${CMAKE_CURRENT_SOURCE_DIR}/KWVolViewConfig.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${config}/KWVolViewConfig.cmake
      @ONLY IMMEDIATE)
  ENDFOREACH(config)

  # Install the config file corresponding to the build configuration
  # specified when building the install target.  The BUILD_TYPE variable
  # will be set while CMake is processing the install files.
  IF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${KWVolView_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${DOLLAR}{BUILD_TYPE}/KWVolViewConfig.cmake)
  ENDIF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
ELSE(CMAKE_CONFIGURATION_TYPES)
  # There is only one build configuration. Configure one KWVolViewConfig.cmake.
  SET(KWVolView_BUILD_TYPE_CONFIG ${CMAKE_BUILD_TYPE})
  CONFIGURE_FILE(
    ${CMAKE_CURRENT_SOURCE_DIR}/KWVolViewConfig.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/KWVolViewConfig.cmake @ONLY IMMEDIATE)

  # Setup an install rule for the config file.
  IF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
    INSTALL_FILES(${KWVolView_INSTALL_PACKAGE_DIR} FILES
      ${CMAKE_CURRENT_BINARY_DIR}/KWVolViewConfig.cmake)
  ENDIF(NOT KWVolView_INSTALL_NO_DEVELOPMENT)
ENDIF(CMAKE_CONFIGURATION_TYPES)
