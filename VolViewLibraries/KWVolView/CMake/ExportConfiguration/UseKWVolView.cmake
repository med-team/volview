##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
#
# This module is provided as KWVolView_USE_FILE by VolViewConfig.cmake.
# It can be INCLUDEd in a project to load the needed compiler and linker
# settings to use VolView:
#   FIND_PACKAGE(VolView REQUIRED)
#   INCLUDE(${KWVolView_USE_FILE})

IF(NOT KWVolView_USE_FILE_INCLUDED)
  SET(KWVolView_USE_FILE_INCLUDED 1)

  # Load the compiler settings used for KWVolView.
  IF(KWVolView_BUILD_SETTINGS_FILE)
    INCLUDE(${CMAKE_ROOT}/Modules/CMakeImportBuildSettings.cmake)
    CMAKE_IMPORT_BUILD_SETTINGS(${KWVolView_BUILD_SETTINGS_FILE})
  ENDIF(KWVolView_BUILD_SETTINGS_FILE)

  # Add compiler flags needed to use VolView.
  SET(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} ${KWVolView_REQUIRED_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} ${KWVolView_REQUIRED_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} ${KWVolView_REQUIRED_EXE_LINKER_FLAGS}")
  SET(CMAKE_SHARED_LINKER_FLAGS
    "${CMAKE_SHARED_LINKER_FLAGS} ${KWVolView_REQUIRED_SHARED_LINKER_FLAGS}")
  SET(CMAKE_MODULE_LINKER_FLAGS
    "${CMAKE_MODULE_LINKER_FLAGS} ${KWVolView_REQUIRED_MODULE_LINKER_FLAGS}")

  # Add include directories needed to use VolView.
  INCLUDE_DIRECTORIES(${KWVolView_INCLUDE_DIRS})

  # Add link directories needed to use VolView.
  LINK_DIRECTORIES(${KWVolView_LIBRARY_DIRS})

  # Add cmake module path.
  SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${KWVolView_CMAKE_DIR}")

  # Use KWWidgetsPro.
  IF(NOT KWVolView_NO_USE_KWWidgetsPro)
    SET(KWWidgetsPro_DIR ${KWVolView_KWWidgetsPro_DIR})
    FIND_PACKAGE(KWWidgetsPro)
    IF(KWWidgetsPro_FOUND)
      INCLUDE(${KWWidgetsPro_USE_FILE})
    ELSE(KWWidgetsPro_FOUND)
      MESSAGE("KWWidgetsPro not found in KWVolView_KWWidgetsPro_DIR=\"${KWVolView_KWWidgetsPro_DIR}\".")
    ENDIF(KWWidgetsPro_FOUND)
  ENDIF(NOT KWVolView_NO_USE_KWWidgetsPro)

  # Use VTKEdge.
  IF(NOT KWVolView_NO_USE_VTKEdge)
    SET(VTKEdge_DIR ${KWVolView_VTKEdge_DIR})
    FIND_PACKAGE(VTKEdge)
    IF(VTKEdge_FOUND)
      INCLUDE(${VTKEdge_USE_FILE})
    ELSE(VTKEdge_FOUND)
      MESSAGE("VTKEdge not found in KWVolView_VTKEdge_DIR=\"${KWVolView_VTKEdge_DIR}\".")
    ENDIF(VTKEdge_FOUND)
  ENDIF(NOT KWVolView_NO_USE_VTKEdge)

  # Use GDCM.
  IF(KWVolView_USE_GDCM)
    SET(GDCM_DIR ${KWVolView_GDCM_DIR})
    FIND_PACKAGE(GDCM)
    IF(GDCM_FOUND)
      INCLUDE(${GDCM_USE_FILE})
    ELSE(GDCM_FOUND)
      MESSAGE("GDCM not found in KWVolView_GDCM_DIR=\"${KWVolView_GDCM_DIR}\".")
    ENDIF(GDCM_FOUND)
  ENDIF(KWVolView_USE_GDCM)

ENDIF(NOT KWVolView_USE_FILE_INCLUDED)
