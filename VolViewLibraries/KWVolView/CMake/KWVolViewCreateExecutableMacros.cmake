##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
# ---------------------------------------------------------------------------
# KWVolView_CREATE_EXECUTABLE
# This macro can be used to create a parametrized VolView executable, that
# will automatically create an application instance, a window object then
# start the application.
# 
# For more flexibility, either subclass vtkVVApplication and/or vtkVVWindow
# and set APPLICATION_CLASS_NAME and/or WINDOW_CLASS_NAME accordingly.
#
# This macro accepts parameters as arg/value pairs or as a single arg if
# the arg is described as boolean (same as setting the arg to 1). The
# args can be specificied in any order and some of them are optionals.
#
# Required arguments:
# EXECUTABLE_NAME (filename): the basename of the executable to create
#    Default to ${PROJECT_NAME} if not found.
#
# Optional arguments:
# APPLICATION_NAME (string): the application name (ex: "VolView Clinical")
#    Default to ${EXECUTABLE_NAME} if not found.
# FILE_EXTENSIONS (list): the file extensions this application supports
#    An association between the app and files with these extensions will be
#    created automatically (Windows).
#    Default to "" if not found, i.e. the default file extension set if
#    vtkVVApplication will be used.
# MAJOR_VERSION (string): the application major version number
#    Default to 1 if not found.
# MINOR_VERSION (string): the application minor version number
#    Default to 0 if not found.
# RELEASE_NAME (string): the application release name if any (ex: "beta1", 
#    "beta2", "final", "patch1", "WIP", etc.)
#    Default to "" if not found.
# EXPIRE_TIME (string): set the application expiration date/time, if any.
#    An option is always available to set it, but this variable can be used
#    to override it (in YYYY-MM-DD HH:MM:SS format).
#    Default to empty string if not found.
# REQUIRE_INTERNATIONALIZATION (boolean): use this option if the application
#    requires the internationalization framework.
#    Default to 0 if not found.
# COMPANY_NAME (string): the name of company providing the application. 
#    Default to "Kitware, Inc." if not found.
# EMAIL_FEEDBACK_ADDRESS (string): the email address to send feedback to using
#    the "Help -> Send Feedback" menu entry.
#    Default to "support@kitware.com" if not found.
# PURCHASE_URL (string): the purchase URL, i.e. the web address where the
#    application can be purchased manually.
#    Default to "http://www.kitware.com/products/volview/purchase.html" if not
#    found.
# REGISTRATION_URL (string): the registration URL, i.e. the web address where
#    a license key for this application can be obtained manually.
#    Default to "http://www.kitware.com/products/volview/purchase.html" if not
#    found.
# COMPANY_SALES_CONTACT (string): the way to contact the company providing 
#    the application, for sales-related questions. 
#    Default to "kitware@kitware.com / (518) 371-3971" if not found.
# COMPANY_SUPPORT_CONTACT (string): the way to contact the company providing 
#    the application, for sales-related questions. 
#    Default to "support@kitware.com / (518) 371-3971" if not found.
# PRIMARY_COPYRIGHT (string): the primary copyright holder. it will show in
#    the "About" dialog box, on top of all other copyrights notice, including
#    Kitware's. 
#    Default to "" if not found.
# DEFAULT_LANGUAGE (string): the application default's language (XPG code).
#    Default to "en_US" if not found.
# APPLICATION_CLASS_NAME (string): the class name of the application object
#    to instantiate in the main executable.
#    Default to vtkVVApplication if not found.
# WINDOW_CLASS_NAME (string): the class name of the window object
#    to instantiate in the main executable. Set CREATE_WINDOW to 0 if you
#    do not want any window to be created for you.
#    Default to "vtkVVWindow" if not found.
# CREATE_WINDOW (boolean): use this option to create a window automatically.
#    The window class can be set using WINDOW_CLASS_NAME
#    Default to 1 if not found.
# EXTRA_SOURCES (list): list of extra source files to add to the executable
#    Default to "" if not found.
# EXTRA_LIBRARIES (list): list of extra libraries to link the executable
#    against.
#    Default to "" if not found.
# SUPPORT_PLUGINS (boolean): use this option if the application supports
#    plugins. This will create a plugin interface in VolView. If plugin
#    shared libraries are present in VolView's plugin path, they will be 
#    loaded at runtime and displayed. The appropriate application can build
#    its own plugins by defining a Plugins/ subdirectory within its
#    application folder.
#    Default to 0.
# AUTHENTICATED_READ (boolean): A constraint can be imposed prior to loading
#    every file in VolView. Prior to reading PathY/FileX, VolView will check
#    to see if PathY/FileX/.license/FileX.key is present. If present, it 
#    will compute the MD5 sum of FileX and validate it with the public key
#    present in the Application's User-data directory. If that does match
#    de-crypted key for the file, the file will not be read and a message
#    will be posted to that effect. Each Application is free to install its 
#    own public key
#    Default to 0.
# SUPPORT_TESTING (boolean): use this option if the application supports
#    testing. This will still create a CMake option that people can turn on
#    or off. If set to ON, this will subdir in Testing.
#    Default to 0 if not found.
# SUPPORT_TESTING_GUI (boolean): use this option if the application supports
#    testing use mouse-record. This will still create a CMake option that
#    people can turn on or off. If set to ON, this will subdir in 
#    Testing/MouseRecorder. SUPPORT_TEST should be set to On first.
#    Default to 0 if not found.
# LICENSE_FILE (filename): full path to the license file for this application.
#    Used in the installer.
#    Default to "" if not found.
# DOCUMENTATION_FILE (filename): full path to the documentation file (pdf/chm).
#    Default to "" if not found.
# TUTORIAL_FILE (filename): full path to the tutorial file (pdf/chm).
#    Default to "" if not found.
# INSTALL_DEFAULT_DATA_SAMPLES (boolean): use this option to install the
#    default data samples (found in VOLVIEW_DATA_ROOT).
#    Default to 0.

macro(KWVolView_CREATE_EXECUTABLE)

  set(notset_value             "__not_set__")

  # Provide some reasonable defaults

  set(EXECUTABLE_NAME              "${PROJECT_NAME}")
  set(APPLICATION_NAME             ${notset_value})
  set(FILE_EXTENSIONS              "")
  set(EXPIRE_TIME                  ${notset_value})
  set(REQUIRE_INTERNATIONALIZATION 0)
  set(SUPPORT_TRIAL_MODE           0)
  set(SUPPORT_LIMITED_EDITION_MODE 0)
  set(MAJOR_VERSION                1)
  set(MINOR_VERSION                0)
  set(RELEASE_NAME                 0)
  set(COMPANY_NAME                 "Kitware, Inc.")
  set(EMAIL_FEEDBACK_ADDRESS       "support@kitware.com")
  set(PURCHASE_URL                 "http://www.kitware.com/products/volview/purchase.html")
  set(REGISTRATION_URL             "http://www.kitware.com/VV3License/register.cgi")
  set(COMPANY_SALES_CONTACT        "mailto:kitware@kitware.com / (518) 371-3971")
  set(COMPANY_SUPPORT_CONTACT      "mailto:support@kitware.com / (518) 371-3971")
  set(PRIMARY_COPYRIGHT            "")
  set(DEFAULT_LANGUAGE             "en_US")
  set(APPLICATION_CLASS_NAME       "vtkVVApplication")
  set(WINDOW_CLASS_NAME            "vtkVVWindow")
  set(CREATE_WINDOW                1)
  set(EXTRA_SOURCES                "")
  set(EXTRA_LIBRARIES              "")
  set(AUTHENTICATED_READ           0)
  set(SUPPORT_PLUGINS              0)
  set(SUPPORT_TESTING              0)
  set(SUPPORT_TESTING_GUI          0)
  set(LICENSE_FILE                 "")
  set(DOCUMENTATION_FILE           "")
  set(TUTORIAL_FILE           "")
  set(INSTALL_DEFAULT_DATA_SAMPLES 0)

  # Parse the arguments

  set(valued_parameter_names "^(___EXECUTABLE_NAME|___APPLICATION_CLASS_NAME|___MAJOR_VERSION|___RELEASE_NAME|___MINOR_VERSION|___COMPANY_NAME|___APPLICATION_NAME|___EXPIRE_TIME|___WINDOW_CLASS_NAME|___DEFAULT_LANGUAGE|___PRIMARY_COPYRIGHT|___COMPANY_SUPPORT_CONTACT|___COMPANY_SALES_CONTACT|___REGISTRATION_URL|___PURCHASE_URL|___EMAIL_FEEDBACK_ADDRESS|___LICENSE_FILE|___DOCUMENTATION_FILE|___TUTORIAL_FILE)$")
  set(boolean_parameter_names "^(___SUPPORT_TRIAL_MODE|___SUPPORT_LIMITED_EDITION_MODE|___REQUIRE_INTERNATIONALIZATION|___SUPPORT_TESTING|___SUPPORT_TESTING_GUI|___CREATE_WINDOW|___SUPPORT_PLUGINS|___AUTHENTICATED_READ|___INSTALL_DEFAULT_DATA_SAMPLES)$")
  set(list_parameter_names "^(___EXTRA_SOURCES|___EXTRA_LIBRARIES|___FILE_EXTENSIONS)$")

  set(next_arg_should_be_value 0)
  set(prev_arg_was_boolean 0)
  set(prev_arg_was_list 0)
  set(unknown_parameters)
  
  string(REGEX REPLACE ";;" ";FOREACH_FIX;" parameter_list "${ARGV}")
  foreach(arg ${parameter_list})

    if("${arg}" STREQUAL "FOREACH_FIX")
      set(arg "")
    endif("${arg}" STREQUAL "FOREACH_FIX")

    set(___arg "___${arg}")
    set(matches_valued 0)
    if("${___arg}" MATCHES ${valued_parameter_names})
      set(matches_valued 1)
    endif("${___arg}" MATCHES ${valued_parameter_names})

    set(matches_boolean 0)
    if("${___arg}" MATCHES ${boolean_parameter_names})
      set(matches_boolean 1)
    endif("${___arg}" MATCHES ${boolean_parameter_names})

    set(matches_list 0)
    if("${___arg}" MATCHES ${list_parameter_names})
      set(matches_list 1)
    endif("${___arg}" MATCHES ${list_parameter_names})
      
    if(matches_valued OR matches_boolean OR matches_list)
      if(prev_arg_was_boolean)
        set(${prev_arg_name} 1)
      else(prev_arg_was_boolean)
        if(next_arg_should_be_value AND NOT prev_arg_was_list)
          message(FATAL_ERROR 
            "Found ${arg} instead of value for ${prev_arg_name}")
        endif(next_arg_should_be_value AND NOT prev_arg_was_list)
      endif(prev_arg_was_boolean)
      set(next_arg_should_be_value 1)
      set(prev_arg_was_boolean ${matches_boolean})
      set(prev_arg_was_list ${matches_list})
      set(prev_arg_name ${arg})
    else(matches_valued OR matches_boolean OR matches_list)
      if(next_arg_should_be_value)
        if(prev_arg_was_boolean)
          if(NOT "${arg}" STREQUAL "1" AND NOT "${arg}" STREQUAL "0")
            message(FATAL_ERROR 
              "Found ${arg} instead of 0 or 1 for ${prev_arg_name}")
          endif(NOT "${arg}" STREQUAL "1" AND NOT "${arg}" STREQUAL "0")
        endif(prev_arg_was_boolean)
        if(prev_arg_was_list)
          set(${prev_arg_name} ${${prev_arg_name}} ${arg})
        else(prev_arg_was_list)
          set(${prev_arg_name} ${arg})
          set(next_arg_should_be_value 0)
        endif(prev_arg_was_list)
      else(next_arg_should_be_value)
        set(unknown_parameters ${unknown_parameters} ${arg})
      endif(next_arg_should_be_value)
      set(prev_arg_was_boolean 0)
    endif(matches_valued OR matches_boolean OR matches_list)

  endforeach(arg)

  if(next_arg_should_be_value)
    if(prev_arg_was_boolean)
      set(${prev_arg_name} 1)
    else(prev_arg_was_boolean)
      if(prev_arg_was_list)
        set(${prev_arg_name} ${${prev_arg_name}} ${arg})
      else(prev_arg_was_list)
        message(FATAL_ERROR "Missing value for ${prev_arg_name}")
      endif(prev_arg_was_list)
    endif(prev_arg_was_boolean)
  endif(next_arg_should_be_value)
  if(unknown_parameters)
    #message(FATAL_ERROR "Unknown parameter(s): ${unknown_parameters}")
  endif(unknown_parameters)

  # Fix some defaults

  if(${APPLICATION_NAME} STREQUAL ${notset_value})
    set(APPLICATION_NAME ${EXECUTABLE_NAME})
  endif(${APPLICATION_NAME} STREQUAL ${notset_value})

  if(${EXPIRE_TIME} STREQUAL ${notset_value})
    set(EXPIRE_TIME "")
  endif(${EXPIRE_TIME} STREQUAL ${notset_value})

  # Few options

  mark_as_advanced(CMAKE_BACKWARDS_COMPATIBILITY)
  mark_as_advanced(CMAKE_BUILD_TYPE)
  if(VolViewLibraries_USE_RPATH)
    set(CMAKE_SKIP_RPATH OFF CACHE INTERNAL "" FORCE)
  else(VolViewLibraries_USE_RPATH)
    set(CMAKE_SKIP_RPATH ON CACHE INTERNAL "" FORCE)
  endif(VolViewLibraries_USE_RPATH)

  # Some options

  option(BUILD_${EXECUTABLE_NAME} "Build ${EXECUTABLE_NAME} (${APPLICATION_NAME} / ${COMPANY_NAME})" ON)
  if(BUILD_${EXECUTABLE_NAME})

    # Need internationalization ?

    if(REQUIRE_INTERNATIONALIZATION AND NOT
        KWWidgets_USE_INTERNATIONALIZATION)
      message(FATAL_ERROR "${APPLICATION_NAME} requires the internationalization framework. Please turn KWWidgets_USE_INTERNATIONALIZATION to ON in KWWidgets and rebuild the corresponding libraries.")
    endif(REQUIRE_INTERNATIONALIZATION AND NOT
      KWWidgets_USE_INTERNATIONALIZATION)

    # Build app as release ?

    if(VolViewLibraries_BUILD_SHARED_LIBS)
      set(BUILD_AS_RELEASE 0)
    else(VolViewLibraries_BUILD_SHARED_LIBS)
      set(BUILD_AS_RELEASE 1)
    endif(VolViewLibraries_BUILD_SHARED_LIBS)
    option(${EXECUTABLE_NAME}_BUILD_AS_RELEASE "Build ${APPLICATION_NAME} as if it was a production release (remove debug options such as \"Command Prompt\", Output Window, etc.)" ${BUILD_AS_RELEASE})
    #MARK_AS_ADVANCED(${EXECUTABLE_NAME}_BUILD_AS_RELEASE)
    if(${EXECUTABLE_NAME}_BUILD_AS_RELEASE)
      set(APP_BUILD_AS_RELEASE 1)
    else(${EXECUTABLE_NAME}_BUILD_AS_RELEASE)
      set(APP_BUILD_AS_RELEASE 0)
    endif(${EXECUTABLE_NAME}_BUILD_AS_RELEASE)

    set(${PROJECT_NAME}_BUILD_SHARED_LIBS ${VolViewLibraries_BUILD_SHARED_LIBS})

    # Install paths

    if(NOT ${PROJECT_NAME}_INSTALL_BIN_DIR)
      set(${PROJECT_NAME}_INSTALL_BIN_DIR "/bin")
    endif(NOT ${PROJECT_NAME}_INSTALL_BIN_DIR)

    if(NOT ${PROJECT_NAME}_INSTALL_LIB_DIR)
      set(${PROJECT_NAME}_INSTALL_LIB_DIR "/lib/${PROJECT_NAME}")
    endif(NOT ${PROJECT_NAME}_INSTALL_LIB_DIR)

    if(NOT ${PROJECT_NAME}_INSTALL_DATA_DIR)
      set(${PROJECT_NAME}_INSTALL_DATA_DIR "/share/${PROJECT_NAME}")
    endif(NOT ${PROJECT_NAME}_INSTALL_DATA_DIR)
    string(REGEX REPLACE "^/" "" ${PROJECT_NAME}_INSTALL_DATA_DIR_CM24 "${${PROJECT_NAME}_INSTALL_DATA_DIR}")

    if(NOT ${PROJECT_NAME}_INSTALL_INCLUDE_DIR)
      set(${PROJECT_NAME}_INSTALL_INCLUDE_DIR "/include/${PROJECT_NAME}")
    endif(NOT ${PROJECT_NAME}_INSTALL_INCLUDE_DIR)

    if(NOT ${PROJECT_NAME}_INSTALL_PACKAGE_DIR)
      set(${PROJECT_NAME}_INSTALL_PACKAGE_DIR ${${PROJECT_NAME}_INSTALL_LIB_DIR})
    endif(NOT ${PROJECT_NAME}_INSTALL_PACKAGE_DIR)
    
    if(NOT ${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)
      set(${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT 1)
    endif(NOT ${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)

    if(NOT ${PROJECT_NAME}_INSTALL_NO_RUNTIME)
      set(${PROJECT_NAME}_INSTALL_NO_RUNTIME 0)
    endif(NOT ${PROJECT_NAME}_INSTALL_NO_RUNTIME)
    
    if(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)
      set(${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION 0)
    endif(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)

    if(NOT ${PROJECT_NAME}_INSTALL_DOC_DIR)
      set(${PROJECT_NAME}_INSTALL_DOC_DIR "/doc")
    endif(NOT ${PROJECT_NAME}_INSTALL_DOC_DIR)

    set(${PROJECT_NAME}_INSTALL_NO_LIBRARIES)

    if(${PROJECT_NAME}_BUILD_SHARED_LIBS)
      if(${PROJECT_NAME}_INSTALL_NO_RUNTIME AND 
          ${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)
        set(${PROJECT_NAME}_INSTALL_NO_LIBRARIES 1)
      endif(${PROJECT_NAME}_INSTALL_NO_RUNTIME AND 
        ${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)
    else(${PROJECT_NAME}_BUILD_SHARED_LIBS)
      if(${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)
        set(${PROJECT_NAME}_INSTALL_NO_LIBRARIES 1)
      endif(${PROJECT_NAME}_INSTALL_NO_DEVELOPMENT)
    endif(${PROJECT_NAME}_BUILD_SHARED_LIBS)

    # Expire time

    set(msg "Set ${APPLICATION_NAME} expiration date/time (YYYY-MM-DD HH:MM:SS). This setting is independent of any other licensing/registration options. Leave this option empty if you do not want this application to expire automatically, enter a date/time otherwise (with MM (1-12), DD (1-31), HH (0-23)).")
    set(${EXECUTABLE_NAME}_EXPIRE_TIME "${EXPIRE_TIME}" CACHE STRING "${msg}")
    mark_as_advanced(${EXECUTABLE_NAME}_EXPIRE_TIME)
    if(EXPIRE_TIME)
      set(${EXECUTABLE_NAME}_EXPIRE_TIME "${EXPIRE_TIME}" CACHE STRING "${msg}" FORCE)
    endif(EXPIRE_TIME)
    set(APP_EXPIRE_TIME ${${EXECUTABLE_NAME}_EXPIRE_TIME})

    # Require license ?
    # This can be disabled using an option, unless we are in release mode
    # in that case the default is enforced.

    # GUI testing with a mouse/keyboard event recorder
    
    if(SUPPORT_TESTING)
      option(${EXECUTABLE_NAME}_BUILD_TESTING
        "Enable testing for ${APPLICATION_NAME} (if BUILD_TESTING is ON as well)" ${BUILD_TESTING})
      #MARK_AS_ADVANCED(${EXECUTABLE_NAME}_BUILD_TESTING)
      if(${EXECUTABLE_NAME}_BUILD_TESTING)
        if(NOT DEFINED BUILD_TESTING)
          include(CTest)
          mark_as_advanced(BUILD_TESTING DART_ROOT TCL_TCLSH)
        endif(NOT DEFINED BUILD_TESTING)
        enable_testing()
        if(BUILD_TESTING)
          find_path(VolView_DATA_ROOT VolViewDataREADME.txt
            $ENV{VolViewData_ROOT})
          if(NOT VolViewLibraries_BUILD_TESTING)
            message(FATAL_ERROR 
              "${EXECUTABLE_NAME}_BUILD_TESTING is set to ON but VolViewLibraries was not built with testing support. Please re-configure VolViewLibraries and set BUILD_TESTING to ON.")
          endif(NOT VolViewLibraries_BUILD_TESTING)
          subdirs(Testing)
          if(WIN32 AND SUPPORT_TESTING_GUI)
            option(${EXECUTABLE_NAME}_BUILD_TESTING_GUI
              "Enable GUI testing for ${APPLICATION_NAME}, using pre-recorded mouse-events. This is experimental and should only be set to ON for the computer the events were recorded on." OFF)
            #MARK_AS_ADVANCED(${EXECUTABLE_NAME}_BUILD_TESTING_GUI)
            if(${EXECUTABLE_NAME}_BUILD_TESTING_GUI)
              subdirs(Testing/MouseRecorder)
            endif(${EXECUTABLE_NAME}_BUILD_TESTING_GUI)
          endif(WIN32 AND SUPPORT_TESTING_GUI)
        endif(BUILD_TESTING)
      endif(${EXECUTABLE_NAME}_BUILD_TESTING)
    endif(SUPPORT_TESTING)

    # Support Plugins ?

    if(SUPPORT_PLUGINS)
      if(NOT KWVolView_BUILD_PLUGINS)
        message(FATAL_ERROR "${EXECUTABLE_NAME} supports plugins but KWVolView has not been built with plugin functionality. Please turn KWVolView_BUILD_PLUGINS ON.") 
      endif(NOT KWVolView_BUILD_PLUGINS)

      # Also check for plugins in the VolViewApplications/VolViewFoo/Plugins/
      if (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Plugins")
        subdirs(Plugins)
      endif (EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/Plugins")
    
      # Install the plugins at ProjectName/bin/Plugins. The plugins folder will
      # reside in the same directory as the Executable.
      set(PLUGIN_INSTALL_FILES "${KWVolView_PLUGINS_INSTALL_FILES};${${APPLICATION_NAME}_PLUGINS_INSTALL_FILES}")
      install_files(${${PROJECT_NAME}_INSTALL_BIN_DIR}/Plugins 
                     FILES ${PLUGIN_INSTALL_FILES})
    endif(SUPPORT_PLUGINS)

    # Authenticated Read

    if(AUTHENTICATED_READ)
      set(AUTHENTICATED_READ_PUBLIC_KEY_FILE "Authentication/license.pub")
      set(pub_key_file "${AUTHENTICATED_READ_PUBLIC_KEY_FILE}")
      set(pub_key_path "${CMAKE_CURRENT_SOURCE_DIR}/${pub_key_file}")
      option(${EXECUTABLE_NAME}_AUTHENTICATED_READ 
        "Loading files into ${APPLICATION_NAME} will require a public/private key authentication of the MD5 sum of the files. The key, expected as ${pub_key_path} will be automatically installed with the application." ${AUTHENTICATED_READ})
      
      # If ON for this application, check if the public key file is present in
      # this application's directory. Additionally, if the key file is present
      # for this application, install it.
      if(${EXECUTABLE_NAME}_AUTHENTICATED_READ)
        if(EXISTS "${pub_key_path}")
          get_filename_component(pub_key_subdir ${pub_key_file} PATH)
          install_files(
            "${${PROJECT_NAME}_INSTALL_DATA_DIR}/${pub_key_subdir}" 
            FILES ${pub_key_path})
          configure_file(
            ${pub_key_path}
            "${CMAKE_BINARY_DIR}/${${PROJECT_NAME}_INSTALL_DATA_DIR}/${pub_key_file}")
        else(EXISTS "${pub_key_path}")
          message("${EXECUTABLE_NAME}_AUTHENTICATED_READ is ON but the public key: \"${pub_key_path}\" cannot be found. Either turn ${EXECUTABLE_NAME}_AUTHENTICATED_READ OFF or add a public key at that location if you wish to authenticate files in this application.")
        endif(EXISTS "${pub_key_path}")
      endif(${EXECUTABLE_NAME}_AUTHENTICATED_READ)
    else(AUTHENTICATED_READ)
      set(AUTHENTICATED_READ_PUBLIC_KEY_FILE)
    endif(AUTHENTICATED_READ)

    # Extra sources need a library and need to be wrapped in Tcl
   
    if(EXTRA_SOURCES)
      set(HAS_EXTRA_SOURCES 1)
      set(EXTRA_SOURCES_LIB "${EXECUTABLE_NAME}Lib")
      string(LENGTH "${EXTRA_SOURCES_LIB}" len)
      math(EXPR lenm1 "${len} - 1")
      string(SUBSTRING "${EXTRA_SOURCES_LIB}" 0 1 first_char)
      string(TOUPPER "${first_char}" first_char_upper)
      string(SUBSTRING "${EXTRA_SOURCES_LIB}" 1 ${lenm1} rest)
      string(TOLOWER "${rest}" rest_lower)
      set(EXTRA_SOURCES_LIB_INIT "${first_char_upper}${rest_lower}_Init")
    else(EXTRA_SOURCES)
      set(HAS_EXTRA_SOURCES 0)
    endif(EXTRA_SOURCES)

    # Documentation file

    if(DOCUMENTATION_FILE)
      get_filename_component(
        DOCUMENTATION_FILENAME "${DOCUMENTATION_FILE}" NAME)
      configure_file("${DOCUMENTATION_FILE}"
        "${LIBRARY_OUTPUT_PATH}/../doc/${DOCUMENTATION_FILENAME}" COPYONLY)
      #if(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)
        install_files(${${PROJECT_NAME}_INSTALL_DOC_DIR} 
          FILES ${DOCUMENTATION_FILE})
      #endif(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)
    endif(DOCUMENTATION_FILE)

    # Tutorial file

    if(TUTORIAL_FILE)
      get_filename_component(
        TUTORIAL_FILENAME "${TUTORIAL_FILE}" NAME)
      configure_file("${TUTORIAL_FILE}"
        "${LIBRARY_OUTPUT_PATH}/../doc/${TUTORIAL_FILENAME}" COPYONLY)
      #if(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)
        install_files(${${PROJECT_NAME}_INSTALL_DOC_DIR} 
          FILES ${TUTORIAL_FILE})
      #endif(NOT ${PROJECT_NAME}_INSTALL_NO_DOCUMENTATION)
    endif(TUTORIAL_FILE)

    # Default samples

    if(INSTALL_DEFAULT_DATA_SAMPLES)
      find_path(VolView_DATA_ROOT VolViewDataREADME.txt
        $ENV{VolViewData_ROOT})
      if(VolView_DATA_ROOT)
        install(DIRECTORY
          "${VolView_DATA_ROOT}/Data/VolViewSamples/"
          DESTINATION ${${PROJECT_NAME}_INSTALL_DATA_DIR_CM24}/data
          PATTERN "CVS" EXCLUDE
          ) 
      endif(VolView_DATA_ROOT)
    endif(INSTALL_DEFAULT_DATA_SAMPLES)

    # Configure the executable file

    string(REPLACE ";" " " FILE_EXTENSIONS_SPACE "${FILE_EXTENSIONS}")

    configure_file(${KWVolView_TEMPLATES_DIR}/KWVolViewExecutable.cxx.in 
      ${CMAKE_CURRENT_BINARY_DIR}/${EXECUTABLE_NAME}.cxx)

    # On Win32 platforms, let's create a .rc resource file to setup a decent
    # application icon as well as some additional information in the "Version"
    # tab of the properties panel.

    if(WIN32 AND NOT BORLAND AND NOT CYGWIN)
      include("${KWWidgets_CMAKE_DIR}/KWWidgetsResourceMacros.cmake")
      set(RC_FILENAME "${CMAKE_CURRENT_BINARY_DIR}/${EXECUTABLE_NAME}.rc")
      kwwidgets_create_rc_file(
        RC_FILENAME "${RC_FILENAME}"
        RC_MAJOR_VERSION "${MAJOR_VERSION}"
        RC_MINOR_VERSION "${MINOR_VERSION}"
        RC_APPLICATION_NAME "${APPLICATION_NAME}"
        RC_APPLICATION_FILENAME "${EXECUTABLE_NAME}"
        RC_ICON_BASENAME "Resources/${EXECUTABLE_NAME}"
        RC_COMPANY_NAME "${COMPANY_NAME}")
    endif(WIN32 AND NOT BORLAND AND NOT CYGWIN)

    # Generate a launcher
    # Not need if we are inside a Complete tree on Windows
    # (on Unix, RPATH should be OFF and we still need a launcher)
    # (on Apple, there are issues with Tcl/Tk libs for example, which
    #  are assumed by the linker to be in the install tree, but we do not
    #  install them!)

    set(skip_rpath 0)
    if(NOT WIN32 AND CMAKE_SKIP_RPATH)
      set(skip_rpath 1)
    endif(NOT WIN32 AND CMAKE_SKIP_RPATH)

    set(generate_launcher 0)
    if(skip_rpath OR
        NOT VTK_SOURCE_DIR OR 
        NOT ITK_SOURCE_DIR OR 
        NOT VolViewLibraries_SOURCE_DIR)
      set(generate_launcher 1)
    endif(skip_rpath OR
      NOT VTK_SOURCE_DIR OR 
      NOT ITK_SOURCE_DIR OR 
      NOT VolViewLibraries_SOURCE_DIR)

    if(APPLE)
      if(BUILD_SHARED_LIBS OR ${PROJECT_NAME}_BUILD_SHARED_LIBS)
        set(generate_launcher 1)
      endif(BUILD_SHARED_LIBS OR ${PROJECT_NAME}_BUILD_SHARED_LIBS)
    endif(APPLE)

    if(generate_launcher)
      include("${KWWidgets_CMAKE_DIR}/KWWidgetsPathsMacros.cmake")
      kwwidgets_generate_setup_paths_scripts(
        "${EXECUTABLE_OUTPUT_PATH}" "${EXECUTABLE_NAME}SetupPaths"
        "${VolViewLibraries_LIBRARY_DIRS}")
      set(LAUNCHER_EXE_NAME "${EXECUTABLE_NAME}Launcher")
      kwwidgets_generate_setup_paths_launcher(
        "${CMAKE_CURRENT_BINARY_DIR}" "${LAUNCHER_EXE_NAME}" 
        "${EXECUTABLE_OUTPUT_PATH}" "${EXECUTABLE_NAME}"
        "${VolViewLibraries_LIBRARY_DIRS}")
    endif(generate_launcher)
    
    # If needed, copy the Tcl/Tk support files required at run-time 
    # to initialize Tcl/Tk. This is only triggered if VTK was built
    # against a Tcl/Tk static library.

    if(VTK_TCL_TK_COPY_SUPPORT_LIBRARY)
      if(VTK_TCL_SUPPORT_LIBRARY_PATH AND VTK_TK_SUPPORT_LIBRARY_PATH)
        include(${VTK_TCL_TK_MACROS_MODULE})
        vtk_copy_tcl_tk_support_files_to_dir(
          ${VTK_TCL_SUPPORT_LIBRARY_PATH} ${VTK_TK_SUPPORT_LIBRARY_PATH}
          "${LIBRARY_OUTPUT_PATH}/../lib")
      endif(VTK_TCL_SUPPORT_LIBRARY_PATH AND VTK_TK_SUPPORT_LIBRARY_PATH)
    endif(VTK_TCL_TK_COPY_SUPPORT_LIBRARY)

    # Create the executable

    include_directories(${CMAKE_CURRENT_SOURCE_DIR})

    add_executable(${EXECUTABLE_NAME} WIN32 
      ${CMAKE_CURRENT_BINARY_DIR}/${EXECUTABLE_NAME}.cxx 
      ${RC_FILENAME})

    if(EXTRA_SOURCES)
      include("${KWWidgets_CMAKE_DIR}/KWWidgetsWrappingMacros.cmake")
      kwwidgets_wrap_tcl(
        ${EXTRA_SOURCES_LIB}
        ${EXECUTABLE_NAME}TCL_SRCS
        "${EXTRA_SOURCES}"
        ""
        "${MAJOR_VERSION}.${MINOR_VERSION}")
      add_library(${EXTRA_SOURCES_LIB} 
        ${EXTRA_SOURCES} ${${EXECUTABLE_NAME}TCL_SRCS})
      target_link_libraries(${EXTRA_SOURCES_LIB} KWVolView ${EXTRA_LIBRARIES})
      if(NOT ${PROJECT_NAME}_INSTALL_NO_LIBRARIES)
        install_targets(
          ${${PROJECT_NAME}_INSTALL_LIB_DIR}
          RUNTIME_DIRECTORY ${${PROJECT_NAME}_INSTALL_BIN_DIR}
          ${EXTRA_SOURCES_LIB}
         )
      endif(NOT ${PROJECT_NAME}_INSTALL_NO_LIBRARIES)
      target_link_libraries(${EXECUTABLE_NAME} ${EXTRA_SOURCES_LIB})
      set(EXTRA_LIBRARIES ${EXTRA_LIBRARIES} ${EXTRA_SOURCES_LIB})
    else(EXTRA_SOURCES)
      target_link_libraries(${EXECUTABLE_NAME} KWVolView ${EXTRA_LIBRARIES})
    endif(EXTRA_SOURCES)

    install_targets(${${PROJECT_NAME}_INSTALL_BIN_DIR} ${EXECUTABLE_NAME})
    
    # Internationalization

    if(KWWidgets_USE_INTERNATIONALIZATION)
      find_package(Gettext REQUIRED)
      include_directories(${GETTEXT_INCLUDE_DIR})
      target_link_libraries(${EXECUTABLE_NAME} ${GETTEXT_LIBRARIES})
    endif(KWWidgets_USE_INTERNATIONALIZATION)

    # Installer

    if(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
      if(EXISTS "${CMAKE_ROOT}/Modules/InstallRequiredSystemLibraries.cmake")
        include(InstallRequiredSystemLibraries)
      endif(EXISTS "${CMAKE_ROOT}/Modules/InstallRequiredSystemLibraries.cmake")
      set(CPACK_PACKAGE_NAME "${EXECUTABLE_NAME}")
      # If we are building shared, let's install VTK, ITK and KWWidgets. 
      # Otherwise we assume everything was built static and we can skip VTK,
      # ITK and KWWidgets
      set(CPACK_INSTALL_CMAKE_PROJECTS)
      if(BUILD_SHARED_LIBS)
        set(CPACK_INSTALL_CMAKE_PROJECTS 
          "${VTK_DIR};VTK;ALL;/;${KWWidgets_DIR};KWWidgets;ALL;/;${ITK_DIR};ITK;ALL;/;")
        if(GDCM_DIR)
          set(CPACK_INSTALL_CMAKE_PROJECTS 
            "${CPACK_INSTALL_CMAKE_PROJECTS}${GDCM_DIR};GDCM;ALL;/;")
        endif(GDCM_DIR)
      endif(BUILD_SHARED_LIBS)

      foreach(lib ${EXTRA_LIBRARIES})
        if(${lib}_DIR)
          set(CPACK_INSTALL_CMAKE_PROJECTS 
            "${CPACK_INSTALL_CMAKE_PROJECTS}${${lib}_DIR};${lib};ALL;/;")
        endif(${lib}_DIR)
      endforeach(lib ${EXTRA_LIBRARIES})

      # If we had to copy the support files, we probably want to 
      # include them in the package also.
      if(VTK_TCL_TK_COPY_SUPPORT_LIBRARY)
        if(VTK_TCL_SUPPORT_LIBRARY_PATH AND VTK_TK_SUPPORT_LIBRARY_PATH)
        set(CPACK_INSTALL_CMAKE_PROJECTS "${CPACK_INSTALL_CMAKE_PROJECTS}${TclTk_BINARY_DIR};TclTk;ALL;/;")
        endif(VTK_TCL_SUPPORT_LIBRARY_PATH AND VTK_TK_SUPPORT_LIBRARY_PATH)
      endif(VTK_TCL_TK_COPY_SUPPORT_LIBRARY)

      set(CPACK_INSTALL_CMAKE_PROJECTS 
        "${CPACK_INSTALL_CMAKE_PROJECTS}${VolViewLibraries_DIR};VolViewLibraries;ALL;/;${CMAKE_CURRENT_BINARY_DIR};${PROJECT_NAME};ALL;/")

      set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${APPLICATION_NAME}")
      set(CPACK_PACKAGE_VENDOR "${COMPANY_NAME}")
      if(LICENSE_FILE)
        set(CPACK_PACKAGE_DESCRIPTION_FILE "${LICENSE_FILE}")
        set(CPACK_RESOURCE_FILE_LICENSE "${LICENSE_FILE}")
      else(LICENSE_FILE)
        set(CPACK_PACKAGE_DESCRIPTION_FILE)
        set(CPACK_RESOURCE_FILE_LICENSE)
      endif(LICENSE_FILE)
      set(CPACK_PACKAGE_VERSION_MAJOR "${MAJOR_VERSION}")
      set(CPACK_PACKAGE_VERSION_MINOR "${MINOR_VERSION}")
      set(full_version "${MAJOR_VERSION}.${MINOR_VERSION}")
      if(RELEASE_NAME)
        set(CPACK_PACKAGE_VERSION_PATCH "${RELEASE_NAME}")
        set(full_version "${full_version}-${RELEASE_NAME}")
      else(RELEASE_NAME)
        set(CPACK_PACKAGE_VERSION_PATCH)
      endif(RELEASE_NAME)
      set(CPACK_PACKAGE_INSTALL_DIRECTORY 
        "${APPLICATION_NAME} ${full_version}")
      set(CPACK_SOURCE_PACKAGE_FILE_NAME "${EXECUTABLE_NAME}-${full_version}")
      if(NOT DEFINED CPACK_SYSTEM_NAME)
        set(CPACK_SYSTEM_NAME ${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR})
      endif(NOT DEFINED CPACK_SYSTEM_NAME)
      if(${CPACK_SYSTEM_NAME} MATCHES Windows)
        if(CMAKE_CL_64)
          set(CPACK_SYSTEM_NAME win64-${CMAKE_SYSTEM_PROCESSOR})
        else(CMAKE_CL_64)
          set(CPACK_SYSTEM_NAME win32-${CMAKE_SYSTEM_PROCESSOR})
        endif(CMAKE_CL_64)
      endif(${CPACK_SYSTEM_NAME} MATCHES Windows)
      if(NOT DEFINED CPACK_PACKAGE_FILE_NAME)
        set(CPACK_PACKAGE_FILE_NAME 
          "${CPACK_SOURCE_PACKAGE_FILE_NAME}-${CPACK_SYSTEM_NAME}")
      endif(NOT DEFINED CPACK_PACKAGE_FILE_NAME)
      set(CPACK_PACKAGE_EXECUTABLES "${EXECUTABLE_NAME}" "${APPLICATION_NAME}")
      if(WIN32 AND NOT UNIX)
        # There is a bug in NSI that does not handle full unix paths properly.
        # Make sure there is at least one set of four (4) backlasshes.
        #    SET(CPACK_PACKAGE_ICON "${ParaQ_SOURCE_DIR}/Applications/Client\\\\m3dDicomLogo.png")
        set(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\${EXECUTABLE_NAME}.exe")
        set(CPACK_NSIS_DISPLAY_NAME "${CPACK_PACKAGE_INSTALL_DIRECTORY}")
        set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY 
          "${CPACK_PACKAGE_INSTALL_DIRECTORY}")
        if(PURCHASE_URL)
          set(CPACK_NSIS_HELP_LINK "${PURCHASE_URL}")
          set(CPACK_NSIS_URL_INFO_ABOUT "${PURCHASE_URL}")
        else(PURCHASE_URL)
          set(CPACK_NSIS_HELP_LINK)
          set(CPACK_NSIS_URL_INFO_ABOUT)
        endif(PURCHASE_URL)
        if(COMPANY_SALES_CONTACT)
          set(CPACK_NSIS_CONTACT "${COMPANY_SALES_CONTACT}")
        else(COMPANY_SALES_CONTACT)
          if(EMAIL_FEEDBACK_ADDRESS)
            set(CPACK_NSIS_CONTACT "${EMAIL_FEEDBACK_ADDRESS}")
          else(EMAIL_FEEDBACK_ADDRESS)
            if(COMPANY_SUPPORT_CONTACT)
              set(CPACK_NSIS_CONTACT "${COMPANY_SUPPORT_CONTACT}")
            else(COMPANY_SUPPORT_CONTACT)
              set(CPACK_NSIS_CONTACT)
            endif(COMPANY_SUPPORT_CONTACT)
          endif(EMAIL_FEEDBACK_ADDRESS)
        endif(COMPANY_SALES_CONTACT)
        set(CPACK_NSIS_MODIFY_PATH OFF)
      else(WIN32 AND NOT UNIX)
        set(CPACK_STRIP_FILES)
        set(CPACK_SOURCE_STRIP_FILES)
      endif(WIN32 AND NOT UNIX)
      set(CPACK_OUTPUT_CONFIG_FILE 
        "${CMAKE_CURRENT_BINARY_DIR}/${EXECUTABLE_NAME}CPackConfig.cmake")

      # FILE ASSOCIATION

      if(FILE_EXTENSIONS)
        if(WIN32 AND NOT UNIX)
          set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS)
          set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS)
          foreach(ext ${FILE_EXTENSIONS})
            string(LENGTH "${ext}" len)
            math(EXPR lenm1 "${len} - 1")
            string(SUBSTRING "${ext}" 1 ${lenm1} extwo)
            set(CPACK_NSIS_EXTRA_INSTALL_COMMANDS "${CPACK_NSIS_EXTRA_INSTALL_COMMANDS}
            WriteRegStr HKCR \\\"${APPLICATION_NAME}\\\" \\\"\\\" \\\"VolView session file\\\"
            WriteRegStr HKCR \\\"${APPLICATION_NAME}\\\\shell\\\\open\\\\command\\\" \\\"\\\" \\\"$\\\\\\\"$INSTDIR\\\\bin\\\\${EXECUTABLE_NAME}.exe$\\\\\\\" $\\\\\\\"%1$\\\\\\\"\\\"
            WriteRegStr HKCR \\\"${ext}\\\" \\\"\\\" \\\"${APPLICATION_NAME}\\\"
            WriteRegStr HKCR \\\"${ext}\\\" \\\"Content Type\\\" \\\"application/x-${extwo}\\\"
          ")
            set(CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS "${CPACK_NSIS_EXTRA_UNINSTALL_COMMANDS}
            DeleteRegKey HKCR \\\" ${APPLICATION_NAME}\\\"
            DeleteRegKey HKCR \\\"${ext}\\\"
          ")
          endforeach(ext)
        endif(WIN32 AND NOT UNIX)

        if (UNIX AND NOT WIN32)
          set(CPACK_FILE_ASSOCIATION_EXTENSION)
          foreach(ext ${FILE_EXTENSIONS})
            string(LENGTH "${ext}" len)
            math(EXPR lenm1 "${len} - 1")
            string(SUBSTRING "${ext}" 1 ${lenm1} extwo)
            set(CPACK_FILE_ASSOCIATION_EXTENSION "${CPACK_FILE_ASSOCIATION_EXTENSION}<string>${extwo}</string>")
          endforeach(ext)

          if(NOT CPACK_FILE_ASSOCIATION_TYPE)
            set(CPACK_FILE_ASSOCIATION_TYPE "VolViewSession")
          endif(NOT CPACK_FILE_ASSOCIATION_TYPE)

          if(NOT CPACK_FILE_ASSOCIATION_ROLE)
            set(CPACK_FILE_ASSOCIATION_ROLE "Editor")
          endif(NOT CPACK_FILE_ASSOCIATION_ROLE)
        endif(UNIX AND NOT WIN32)

      endif(FILE_EXTENSIONS)

      IF(APPLE AND BUILD_SHARED_LIBS)
        if (EXTRA_SOURCES)
          CONFIGURE_FILE(${KWVolView_SOURCE_DIR}/CMake/PostInstall.cmake.in 
            ${CMAKE_CURRENT_BINARY_DIR}/PostInstall.cmake @ONLY)
          SET_TARGET_PROPERTIES(${EXTRA_SOURCES_LIB} PROPERTIES POST_INSTALL_SCRIPT 
            ${CMAKE_CURRENT_BINARY_DIR}/PostInstall.cmake)
        else (EXTRA_SOURCES)
          CONFIGURE_FILE(${KWVolView_SOURCE_DIR}/CMake/PostInstall.cmake.in 
            ${CMAKE_CURRENT_BINARY_DIR}/PostInstall.cmake @ONLY)
          SET_TARGET_PROPERTIES(${EXECUTABLE_NAME} PROPERTIES POST_INSTALL_SCRIPT 
            ${CMAKE_CURRENT_BINARY_DIR}/PostInstall.cmake)
        endif (EXTRA_SOURCES)
      ENDIF(APPLE AND BUILD_SHARED_LIBS)

      include(CPack)
      get_filename_component(cmake_bin_dir ${CMAKE_COMMAND} PATH)

      if(CMAKE_CONFIGURATION_TYPES)
        add_custom_target(${EXECUTABLE_NAME}Installer
          COMMAND ${cmake_bin_dir}/cpack 
          -C ${CMAKE_CFG_INTDIR} --config ${CPACK_OUTPUT_CONFIG_FILE})
      else(CMAKE_CONFIGURATION_TYPES)
        add_custom_target(${EXECUTABLE_NAME}Installer
          COMMAND ${cmake_bin_dir}/cpack --config ${CPACK_OUTPUT_CONFIG_FILE})
      endif(CMAKE_CONFIGURATION_TYPES)

     endif(EXISTS "${CMAKE_ROOT}/Modules/CPack.cmake")
  endif(BUILD_${EXECUTABLE_NAME})

 endmacro(KWVolView_CREATE_EXECUTABLE)
