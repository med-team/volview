/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVPaintbrushWidgetToolbar - a frame that holds the tool buttons for the paintbrush widget editor
// .SECTION Description
// Simply a frame to hold a toolbuttons for the paintbrush widget editor

#ifndef __vtkVVPaintbrushWidgetToolbar_h
#define __vtkVVPaintbrushWidgetToolbar_h

#include "vtkKWToolbar.h"

class vtkKWPushButton;

class VTK_EXPORT vtkVVPaintbrushWidgetToolbar : public vtkKWToolbar
{
public:
  static vtkVVPaintbrushWidgetToolbar* New();
  vtkTypeRevisionMacro(vtkVVPaintbrushWidgetToolbar, vtkKWToolbar);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Callback commands.
  void SetAddSketchCommand(vtkObject *o, const char *method);
  void SetDeleteSketchCommand(vtkObject *o, const char *method);
  void SetLoadDrawingCommand(vtkObject *o, const char *method);
  void SetSaveDrawingCommand(vtkObject *o, const char *method);
  void SetUndoStrokeCommand(vtkObject *o, const char *method);
  void SetRedoStrokeCommand(vtkObject *o, const char *method);
  void SetMergeSketchesCommand(vtkObject *o, const char *method);
  void SetCopyToNextSliceCommand(vtkObject *o, const char *method);
  void SetCopyToPreviousSliceCommand(vtkObject *o, const char *method);
  void SetPromoteDrawingToVolumeCommand(vtkObject *o, const char *method);
  void SetConvertVolumeToDrawingCommand(vtkObject *o, const char *method);

  // Description:
  // Some toolbar buttons like MergeSketchesButton are meant to be enabled
  // when multiple sketches are selected in the UI. This method can be used
  // to say whether these buttons should be enabled or disabled.
  virtual void SetSupportMultiSketchOperations(int);
  vtkGetMacro(SupportMultiSketchOperations, int);
  vtkBooleanMacro(SupportMultiSketchOperations, int);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts subwidgets. This will, for example,
  // enable disable parts of the widget UI, enable disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

protected:
  vtkVVPaintbrushWidgetToolbar();
  ~vtkVVPaintbrushWidgetToolbar();

  // Description:
  // Create the widget.
  virtual void CreateWidget();
  virtual void CreateToolbarButtons();

  vtkKWPushButton *AddSketchButton;
  vtkKWPushButton *DeleteSketchButton;
  vtkKWPushButton *LoadDrawingButton;
  vtkKWPushButton *SaveDrawingButton;
  vtkKWPushButton *UndoStrokeButton;
  vtkKWPushButton *RedoStrokeButton;
  vtkKWPushButton *MergeSketchesButton;
  vtkKWPushButton *CopyToNextSliceButton;
  vtkKWPushButton *CopyToPreviousSliceButton;
  vtkKWPushButton *PromoteDrawingToVolumeButton;
  vtkKWPushButton *ConvertVolumeToDrawingButton;

  int SupportMultiSketchOperations;

private:
  vtkVVPaintbrushWidgetToolbar(const vtkVVPaintbrushWidgetToolbar&); // Not implemented
  void operator=(const vtkVVPaintbrushWidgetToolbar&); // Not implemented
};

#endif
