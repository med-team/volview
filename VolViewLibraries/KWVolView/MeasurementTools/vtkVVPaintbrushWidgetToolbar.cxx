/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkVVPaintbrushWidgetToolbar.h"

#include "vtkKWToolbar.h"
#include "vtkKWPushButton.h"
#include "vtkKWFrame.h"
#include "vtkKWIcon.h"
#include "vtkKWTkUtilities.h"
#include "vtkObjectFactory.h"
#include "vtkVVResources.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkVVPaintbrushWidgetToolbar );
vtkCxxRevisionMacro(vtkVVPaintbrushWidgetToolbar, "1.69");

//----------------------------------------------------------------------------
vtkVVPaintbrushWidgetToolbar::vtkVVPaintbrushWidgetToolbar()
{
  this->AddSketchButton = NULL;
  this->DeleteSketchButton = NULL;
  this->LoadDrawingButton = NULL;
  this->SaveDrawingButton = NULL;
  this->UndoStrokeButton = NULL;
  this->RedoStrokeButton = NULL;
  this->MergeSketchesButton = NULL;
  this->CopyToNextSliceButton = NULL;
  this->CopyToPreviousSliceButton = NULL;
  this->PromoteDrawingToVolumeButton = NULL;
  this->ConvertVolumeToDrawingButton = NULL;
  
  this->SupportMultiSketchOperations = 0;
}

//----------------------------------------------------------------------------
vtkVVPaintbrushWidgetToolbar::~vtkVVPaintbrushWidgetToolbar()
{
  if (this->AddSketchButton)
    {
    this->AddSketchButton->Delete();
    this->AddSketchButton = NULL;
    }
  if (this->DeleteSketchButton)
    {
    this->DeleteSketchButton->Delete();
    this->DeleteSketchButton = NULL;
    }
  if (this->LoadDrawingButton)
    {
    this->LoadDrawingButton->Delete();
    this->LoadDrawingButton = NULL;
    }
  if (this->SaveDrawingButton)
    {
    this->SaveDrawingButton->Delete();
    this->SaveDrawingButton = NULL;
    }
  if (this->UndoStrokeButton)
    {
    this->UndoStrokeButton->Delete();
    this->UndoStrokeButton = NULL;
    }
  if (this->RedoStrokeButton)
    {
    this->RedoStrokeButton->Delete();
    this->RedoStrokeButton = NULL;
    }
  if (this->MergeSketchesButton)
    {
    this->MergeSketchesButton->Delete();
    this->MergeSketchesButton = NULL;
    }
  if (this->CopyToNextSliceButton)
    {
    this->CopyToNextSliceButton->Delete();
    this->CopyToNextSliceButton = NULL;
    }
  if (this->CopyToPreviousSliceButton)
    {
    this->CopyToPreviousSliceButton->Delete();
    this->CopyToPreviousSliceButton = NULL;
    }
  if (this->PromoteDrawingToVolumeButton)
    {
    this->PromoteDrawingToVolumeButton->Delete();
    this->PromoteDrawingToVolumeButton = NULL;
    }
  if (this->ConvertVolumeToDrawingButton)
    {
    this->ConvertVolumeToDrawingButton->Delete();
    this->ConvertVolumeToDrawingButton = NULL;
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::CreateWidget()
{
  // Check if already created

  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }

  // Call the superclass to create the whole widget

  this->Superclass::CreateWidget();
  
  this->CreateToolbarButtons();  
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::CreateToolbarButtons()
{
  vtkKWIcon *base_icon = vtkKWIcon::New();
  base_icon->SetImage(vtkKWIcon::IconDocument);
  base_icon->TrimTop();
  base_icon->TrimRight();

  vtkKWIcon *icon = vtkKWIcon::New();
  bool use_sep = false;

  // Add sketch

  this->AddSketchButton = vtkKWPushButton::New();
  this->AddSketchButton->SetParent(this->GetFrame());
  this->AddSketchButton->Create();
  this->AddSketchButton->SetText("Add new label map");
  this->AddSketchButton->SetBalloonHelpString(this->AddSketchButton->GetText());
  icon->SetImage(base_icon);
  icon->Compose(vtkKWIcon::IconPresetAdd);
  this->AddSketchButton->SetImageToIcon(icon);
  this->AddWidget(this->AddSketchButton);

  // Delete sketch

  this->DeleteSketchButton = vtkKWPushButton::New();
  this->DeleteSketchButton->SetParent(this->GetFrame());
  this->DeleteSketchButton->Create();
  this->DeleteSketchButton->SetText("Delete selected label map(s)");
  this->DeleteSketchButton->SetBalloonHelpString(
    this->DeleteSketchButton->GetText());
  icon->SetImage(base_icon);
  icon->Compose(vtkKWIcon::IconPresetDelete);
  this->DeleteSketchButton->SetImageToIcon(icon);
  this->AddWidget(this->DeleteSketchButton);

  if (use_sep)
    {
    this->AddSeparator();
    }

  // Load sketch

  this->LoadDrawingButton = vtkKWPushButton::New();
  this->LoadDrawingButton->SetParent(this->GetFrame());
  this->LoadDrawingButton->Create();
  this->LoadDrawingButton->SetText("Load label maps");
  this->LoadDrawingButton->SetBalloonHelpString("Load label maps from a file.");
  this->LoadDrawingButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconNuvola16x16ActionsFileOpen);
  this->AddWidget(this->LoadDrawingButton);
   
  // Save sketch

  this->SaveDrawingButton = vtkKWPushButton::New();
  this->SaveDrawingButton->SetParent(this->GetFrame());
  this->SaveDrawingButton->Create();
  this->SaveDrawingButton->SetText("Save label maps");
  this->SaveDrawingButton->SetBalloonHelpString(
    "Save the label maps to a file.");
  this->SaveDrawingButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconNuvola16x16ActionsFileSave);
  this->AddWidget(this->SaveDrawingButton);
   
  if (use_sep)
    {
    this->AddSeparator();
    }

  // Undo

  this->UndoStrokeButton = vtkKWPushButton::New();
  this->UndoStrokeButton->SetParent(this->GetFrame());
  this->UndoStrokeButton->Create();
  this->UndoStrokeButton->SetText("Undo stroke");
  this->UndoStrokeButton->SetBalloonHelpString(
    "Undo the last draw/erase stroke");
  this->UndoStrokeButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconSilkArrowUndo);
  this->AddWidget(this->UndoStrokeButton);

  // Redo

  this->RedoStrokeButton = vtkKWPushButton::New();
  this->RedoStrokeButton->SetParent(this->GetFrame());
  this->RedoStrokeButton->Create();
  this->RedoStrokeButton->SetText("Redo stroke");
  this->RedoStrokeButton->SetBalloonHelpString(
    "Redo the last draw/erase stroke");
  this->RedoStrokeButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconSilkArrowRedo);
  this->AddWidget(this->RedoStrokeButton);
   
  if (use_sep)
    {
    this->AddSeparator();
    }

  // Merge sketches

  this->MergeSketchesButton = vtkKWPushButton::New();
  this->MergeSketchesButton->SetParent(this->GetFrame());
  this->MergeSketchesButton->Create();
  this->MergeSketchesButton->SetText("Merge multiple label maps");
  this->MergeSketchesButton->SetBalloonHelpString("Merge several label maps into one. Select more than one label map in this list first, then press this button. Selected label maps will be merged into the first selected label map.");
  this->MergeSketchesButton->SetImageToPredefinedIcon(vtkKWIcon::IconTreeClose);
  this->AddWidget(this->MergeSketchesButton);

  // Copy a sketch from one slice to the next
  
  this->CopyToNextSliceButton = vtkKWPushButton::New();
  this->CopyToNextSliceButton->SetParent(this->GetFrame());
  this->CopyToNextSliceButton->Create();
  this->CopyToNextSliceButton->SetText(
    "Copy the selected label map(s) to the next slice and move to that slice.");
  this->CopyToNextSliceButton->SetBalloonHelpString(
    this->CopyToNextSliceButton->GetText());
  this->CopyToNextSliceButton->SetImageToPixels(
    image_CopyLabelsToNextSlice,
    image_CopyLabelsToNextSlice_width, image_CopyLabelsToNextSlice_height,
    image_CopyLabelsToNextSlice_pixel_size,
    image_CopyLabelsToNextSlice_length);
  this->AddWidget(this->CopyToNextSliceButton);

  // Copy a sketch from one slice to the previous

  this->CopyToPreviousSliceButton = vtkKWPushButton::New();
  this->CopyToPreviousSliceButton->SetParent(this->GetFrame());
  this->CopyToPreviousSliceButton->Create();
  this->CopyToPreviousSliceButton->SetText(
    "Copy the selected label map(s) to the previous slice and move to that slice.");
  this->CopyToPreviousSliceButton->SetBalloonHelpString(
    this->CopyToPreviousSliceButton->GetText());
  this->CopyToPreviousSliceButton->SetImageToPixels(
    image_CopyLabelsToPreviousSlice,
    image_CopyLabelsToPreviousSlice_width, image_CopyLabelsToNextSlice_height,
    image_CopyLabelsToPreviousSlice_pixel_size,
    image_CopyLabelsToPreviousSlice_length);
  this->AddWidget(this->CopyToPreviousSliceButton);

  if (use_sep)
    {
    this->AddSeparator();
    }

  // Promote a drawing to a volume

  this->PromoteDrawingToVolumeButton = vtkKWPushButton::New();
  this->PromoteDrawingToVolumeButton->SetParent(this->GetFrame());
  this->PromoteDrawingToVolumeButton->Create();
  this->PromoteDrawingToVolumeButton->SetText(
    "Promote the set of label maps to a new, separate volume for 3D visualization.");
  this->PromoteDrawingToVolumeButton->SetBalloonHelpString(
    this->PromoteDrawingToVolumeButton->GetText());
  this->PromoteDrawingToVolumeButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconNuvola16x16FilesystemsFolderOutbox);
  this->AddWidget(this->PromoteDrawingToVolumeButton);

  // Promote a drawing to a volume

  this->ConvertVolumeToDrawingButton = vtkKWPushButton::New();
  this->ConvertVolumeToDrawingButton->SetParent(this->GetFrame());
  this->ConvertVolumeToDrawingButton->Create();
  this->ConvertVolumeToDrawingButton->SetText(
    "Convert a compatible, separate volume to a set of label maps.");
  this->ConvertVolumeToDrawingButton->SetBalloonHelpString(
    this->ConvertVolumeToDrawingButton->GetText());
  this->ConvertVolumeToDrawingButton->SetImageToPredefinedIcon(
    vtkKWIcon::IconNuvola16x16FilesystemsFolderInbox);
  this->AddWidget(this->ConvertVolumeToDrawingButton);

  // Release icon

  icon->Delete();
  base_icon->Delete();
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetAddSketchCommand(
  vtkObject *o, const char *method)
{
  if (this->AddSketchButton)
    {
    this->AddSketchButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetDeleteSketchCommand(vtkObject *o, const char *method)
{
  if (this->DeleteSketchButton)
    {
    this->DeleteSketchButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetLoadDrawingCommand(
  vtkObject *o, const char *method)
{
  if (this->LoadDrawingButton)
    {
    this->LoadDrawingButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetSaveDrawingCommand(
  vtkObject *o, const char *method)
{
  if (this->SaveDrawingButton)
    {
    this->SaveDrawingButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetUndoStrokeCommand(
  vtkObject *o, const char *method)
{
  if (this->UndoStrokeButton)
    {
    this->UndoStrokeButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetRedoStrokeCommand(
  vtkObject *o, const char *method)
{
  if (this->RedoStrokeButton)
    {
    this->RedoStrokeButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetMergeSketchesCommand(
  vtkObject *o, const char *method)
{
  if (this->MergeSketchesButton)
    {
    this->MergeSketchesButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetCopyToNextSliceCommand(
  vtkObject *o, const char *method)
{
  if (this->CopyToNextSliceButton)
    {
    this->CopyToNextSliceButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetCopyToPreviousSliceCommand(
  vtkObject *o, const char *method)
{
  if (this->CopyToPreviousSliceButton)
    {
    this->CopyToPreviousSliceButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetPromoteDrawingToVolumeCommand(
  vtkObject *o, const char *method)
{
  if (this->PromoteDrawingToVolumeButton)
    {
    this->PromoteDrawingToVolumeButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetConvertVolumeToDrawingCommand(
  vtkObject *o, const char *method)
{
  if (this->ConvertVolumeToDrawingButton)
    {
    this->ConvertVolumeToDrawingButton->SetCommand(o,method);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

  // Some toolbar buttons like MergeSketchesButton are meant to be enabled
  // when multiple sketches are selected in the UI.
  if (this->MergeSketchesButton)
    {
    this->MergeSketchesButton->SetEnabled(
      this->SupportMultiSketchOperations && this->GetEnabled());
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::SetSupportMultiSketchOperations(int e)
{
  if (e == this->SupportMultiSketchOperations)
    {
    return;
    }

  this->SupportMultiSketchOperations = e;
  this->Modified();
  
  this->UpdateEnableState();
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushWidgetToolbar::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "SupportMultiSketchOperations: " << (this->GetSupportMultiSketchOperations() ? "on":"off") << endl;
}