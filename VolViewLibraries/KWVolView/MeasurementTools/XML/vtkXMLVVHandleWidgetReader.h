/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVHandleWidgetReader - vtkVVHandleWidget XML Reader.
// .SECTION Description
// vtkXMLVVHandleWidgetReader provides XML reading functionality to 
// vtkVVHandleWidget.
// .SECTION See Also
// vtkXMLVVHandleWidgetWriter

#ifndef __vtkXMLVVHandleWidgetReader_h
#define __vtkXMLVVHandleWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLVVHandleWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLVVHandleWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLVVHandleWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLVVHandleWidgetReader() {};
  ~vtkXMLVVHandleWidgetReader() {};

private:
  vtkXMLVVHandleWidgetReader(const vtkXMLVVHandleWidgetReader&); // Not implemented
  void operator=(const vtkXMLVVHandleWidgetReader&); // Not implemented    
};

#endif

