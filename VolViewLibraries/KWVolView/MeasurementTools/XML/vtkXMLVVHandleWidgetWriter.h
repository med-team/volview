/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLVVHandleWidgetWriter - vtkVVHandleWidget XML Writer.
// .SECTION Description
// vtkXMLVVHandleWidgetWriter provides XML writing functionality to 
// vtkVVHandleWidget.
// .SECTION See Also
// vtkXMLVVHandleWidgetReader

#ifndef __vtkXMLVVHandleWidgetWriter_h
#define __vtkXMLVVHandleWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLVVHandleWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLVVHandleWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLVVHandleWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

  // Description:
  // Return the name of the component element used inside that tree to
  // store a component.
  static const char* GetIDElementName();

protected:
  vtkXMLVVHandleWidgetWriter() {};
  ~vtkXMLVVHandleWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLVVHandleWidgetWriter(const vtkXMLVVHandleWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLVVHandleWidgetWriter&);  // Not implemented.
};

#endif

