/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVPaintbrushWidgetEditor - a paintbrush widget editor
// .SECTION Description
// This class contains the UI components and methods to edit a 
// vtkKWEPaintbrushWidget.

#ifndef __vtkVVPaintbrushWidgetEditor_h
#define __vtkVVPaintbrushWidgetEditor_h

#include "vtkKWCompositeWidget.h"
#include <vtkstd/vector>

class vtkKWEPaintbrushDrawingStatistics;
class vtkKWEPaintbrushSketch;
class vtkKWEPaintbrushWidget;
class vtkKWFrameWithLabel;
class vtkKWImageWidget;
class vtkKWLabelWithLabel;
class vtkKWMenu;
class vtkKWMultiColumnListWithScrollbars;
class vtkVVDataItemVolume;
class vtkVVPaintbrushOptionsFrame;
class vtkVVPaintbrushWidgetToolbar;

class VTK_EXPORT vtkVVPaintbrushWidgetEditor : public vtkKWCompositeWidget
{
public:
  static vtkVVPaintbrushWidgetEditor* New();
  void PrintSelf(ostream& os, vtkIndent indent);
  vtkTypeRevisionMacro(vtkVVPaintbrushWidgetEditor,vtkKWCompositeWidget);

  // Description:
  // Get/Set the paintbrush widget to edit
  vtkGetObjectMacro(PaintbrushWidget, vtkKWEPaintbrushWidget);
  virtual void SetPaintbrushWidget(vtkKWEPaintbrushWidget*);

  // Description:
  // Get/Set the image widget this paintbrush widget is displayed on.
  // This is optional and used to increment/decrement the slice index
  // when copying a sketch to a slice from another
  vtkGetObjectMacro(ImageWidget, vtkKWImageWidget);
  virtual void SetImageWidget(vtkKWImageWidget*);

  // Description:
  // Update the whole UI depending on the value of the Ivars
  virtual void Update();

  // Description:
  // Return a basic color, given an index (i.e. any positive number). It
  // will loop over a list of basic colors.
  static double* GetBasicColor(int index);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Convert a volume to a drawing (and overwrite current drawing)
  // They need to be the same size
  virtual void ConvertVolumeToDrawing(vtkVVDataItemVolume *vol);

  // Description:
  // Callbacks. Internal, do not use.
  virtual void SketchSelectionChangedCallback();
  virtual void SketchCellUpdatedCallback(int row, int col, const char *text);
  virtual void OpacityChangedCallback(double);
  virtual void ShapeSizeChangedCallback(double);
  virtual void SingleSliceBrushChangedCallback(int);
  virtual void AddSketchCallback();
  virtual void DeleteSketchCallback();
  virtual void LoadDrawingCallback();
  virtual void SaveDrawingCallback();
  virtual void UndoStrokeCallback();
  virtual void RedoStrokeCallback();
  virtual void MergeSketchCallback();
  virtual void CopySketchToNextSliceCallback();
  virtual void CopySketchToPreviousSliceCallback();
  virtual void PromoteDrawingToVolumeCallback();
  virtual void ConvertVolumeToDrawingCallback();

  // Description:
  // Populate the sketch list
  virtual void PopulateSketchList();

  // Description:
  // Some constants
  //BTX
  static const char *LabelMapPathRegKey;
  static const char *LastSavedLabelMapRegKey;
  //ETX

protected:
  vtkVVPaintbrushWidgetEditor();
  ~vtkVVPaintbrushWidgetEditor();

  // Description:
  // Create the widget.
  virtual void CreateWidget();
  virtual void CreateSketchListColumns();

  vtkKWEPaintbrushWidget *PaintbrushWidget;
  vtkKWImageWidget *ImageWidget;

  virtual void UpdateSketchListSelection();
  virtual void UpdatePaintbrushPropertyWidgets();
  virtual void UpdateVolumesOfSketches();

  // Description:
  // Some constants
  //BTX
  static const char *IndexColumnName;
  static const char *LabelColumnName;
  static const char *VisibilityColumnName;
  static const char *ColorColumnName;
  static const char *MutableColumnName;
  static const char *IdentifierColumnName;
  static const char *VolumeColumnName;
  //ETX

  // Description:
  // Get the index of a given column.
  virtual int GetIndexColumnIndex();
  virtual int GetLabelColumnIndex();
  virtual int GetVisibilityColumnIndex();
  virtual int GetColorColumnIndex();
  virtual int GetMutableColumnIndex();
  virtual int GetIdentifierColumnIndex();
  virtual int GetVolumeColumnIndex();

  // GUI

  vtkKWFrameWithLabel                *EditorFrame;
  vtkKWMultiColumnListWithScrollbars *SketchList;
  vtkKWFrame                         *ControlFrame;
  vtkVVPaintbrushWidgetToolbar       *Toolbar;
  vtkKWLabelWithLabel                *HelpLabel;
  vtkVVPaintbrushOptionsFrame        *PaintbrushOptions;
  vtkKWMenu                          *ContextMenu;

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // Convenience
  virtual vtkKWEPaintbrushSketch* GetSelectedPaintbrushSketchInList();

  //BTX
  typedef std::vector< vtkKWEPaintbrushSketch* > SketchContainerType;
  typedef SketchContainerType::iterator SketchIteratorType;
  SketchContainerType GetSelectedPaintbrushSketchesInList();

  typedef std::vector< vtkVVDataItemVolume* > DataItemContainerType;
  typedef DataItemContainerType::iterator DataItemIteratorType;
  DataItemContainerType GetEligibleDataItemsForConversionToDrawing();
  //ETX

  // Some statistics on the drawing
  vtkKWEPaintbrushDrawingStatistics *DrawingStatistics;

private:
  vtkVVPaintbrushWidgetEditor(const vtkVVPaintbrushWidgetEditor&); // Not implemented
  void operator=(const vtkVVPaintbrushWidgetEditor&); // Not implemented
};

#endif
