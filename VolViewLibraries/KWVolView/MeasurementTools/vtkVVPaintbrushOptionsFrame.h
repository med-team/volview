/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVPaintbrushOptionsFrame - Frame that contains various widgets for the paintbrush properties
// .SECTION Description
// This frame contains the following
//  - Drawing Opacity scale
//  - Shape size scale
//  - Single slice segmentation check button

#ifndef __vtkVVPaintbrushOptionsFrame_h
#define __vtkVVPaintbrushOptionsFrame_h

#include "vtkKWFrame.h"

class vtkKWScaleWithEntry;
class vtkKWCheckButtonWithLabel;

class VTK_EXPORT vtkVVPaintbrushOptionsFrame : public vtkKWFrame
{
public:
  static vtkVVPaintbrushOptionsFrame* New();
  vtkTypeRevisionMacro(vtkVVPaintbrushOptionsFrame,vtkKWFrame);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set command callbacks.
  void SetOpacityChangedCommand(vtkObject *o, const char *method);
  void SetShapeSizeChangedCommand(vtkObject *o, const char *method);
  void SetSingleSliceBrushChangedCommand(vtkObject *o, const char *method);

  // Description:
  // Get the UI elements.
  vtkGetObjectMacro(OpacityScale, vtkKWScaleWithEntry);
  vtkGetObjectMacro(ShapeSizeScale, vtkKWScaleWithEntry);
  vtkGetObjectMacro(SingleSliceBrushCheckButton, vtkKWCheckButtonWithLabel);

  // Description:
  void SetOpacity(double v);
  void SetShapeSize(double v);
  void SetShapeSizeRange(double v1, double v2);
  void SetShapeSizeResolution(double v);
  void SetSingleSliceBrush(int);

  // Description:
  // Update the "enable" state of the object and its internal parts.
  virtual void UpdateEnableState();
  
protected:
  vtkVVPaintbrushOptionsFrame();
  ~vtkVVPaintbrushOptionsFrame();

  // Description:
  // Create the interface objects.
  virtual void CreateWidget();

  // GUI Items
  vtkKWScaleWithEntry            *OpacityScale;
  vtkKWScaleWithEntry            *ShapeSizeScale;
  vtkKWCheckButtonWithLabel      *SingleSliceBrushCheckButton;

private:
  vtkVVPaintbrushOptionsFrame(const vtkVVPaintbrushOptionsFrame&);
  void operator=(const vtkVVPaintbrushOptionsFrame&);
};

#endif

