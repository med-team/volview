/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVHandleWidget
// .SECTION Description

#ifndef __vtkVVHandleWidget_h
#define __vtkVVHandleWidget_h

#include "vtkHandleWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros
#include <vtkstd/string>

class vtkVVSelectionFrame;
class vtkVVDataItem;
class vtkVVHandleWidgetCommand;

class VTK_EXPORT vtkVVHandleWidget : public vtkHandleWidget
{
public:
  static vtkVVHandleWidget* New();
  vtkTypeRevisionMacro(vtkVVHandleWidget, vtkHandleWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Override superclass method to enable all the widgets in the group
  virtual void SetEnabled(int e);

  // Description:
  // Override superclass method to set the flag on all the widgets in the group
  virtual void SetProcessEvents(int e);

  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

  // Description:
  // INTERNAL - DO NOT USE.
  // Handle widgets exist on all the renderwindows/views for the selected 
  // dataitem. The corresponding widgets on multiple views are 
  // synchronized through events. To uniquely identify the set of widgets
  // that represent the same seed, the handle widgets have an ID. 
  vtkGetMacro( ID, int );
  virtual void SetID( int );

  // Description:
  // Get a new unique ID for this selection frame. This is equal to the largest
  // ID of all handles in this selection frame + 1
  static int GetNewUniqueID( vtkVVSelectionFrame * );

  // Description:
  // Set a name for the widget. You may use this to give the handle a textual 
  // description. It will be propagated to all the widgets in the group.
  void SetDescription( const char * );
  const char * GetDescription();

  // Description:
  // Set the selection frame
  virtual void SetSelectionFrame( vtkVVSelectionFrame * );
  vtkGetObjectMacro( SelectionFrame, vtkVVSelectionFrame );

  // Description:
  // Set whether the widget is the master or the slave. Default slave.
  vtkSetMacro( PlaceInteractively, int );
  vtkGetMacro( PlaceInteractively, int );
  vtkBooleanMacro( PlaceInteractively, int );

  // Description:
  // Get the number of seeds in this group.
  int GetNumberOfHandlesInGroup();

  // Description:
  // Get the nth seed in this group.
  vtkVVHandleWidget *GetNthHandleInGroup( int i );

  // Description:
  // Get the number of handle groups in a volume
  static int GetNumberOfHandlesInDataItem( vtkVVDataItem * );

  // Description:
  // Get the nth handle in the dataitem. Handles are sorted by their ID
  static vtkVVHandleWidget *GetNthHandleInDataItem(
                   vtkVVDataItem *data_item, int n);

  // Description:
  // Sync with other handle widgets in the same group
  virtual void Sync();

  // Description:
  // Get the position of the handle
  void GetWorldPosition(double pos[3]);
  
  // Description:
  // Get the position of the handle. Returns 0 if outside the volume, 
  // 1 if inside
  int GetPixelPosition(int pos[3]);

  // Description:
  // If the handle is displayed on a 2D render widget, this will return the
  // slice the handle exists. (See sliceorientations in vtkKW2DRenderWidget)
  int GetSlice( vtkVVSelectionFrame *, int sliceOrientation );

  // Description:
  // If the handle is displayed on a 2D render widget, this will set the
  // DisplayForAllSlices flag for all the representations in the group
  virtual void SetDisplayForAllSlices(int e);
  virtual int GetDisplayForAllSlices();

  // Description:
  // Raise all slices displaying this render widget.
  void Show();

  // Description:
  // Invokes Render for all the widgets in group.
  void RenderAllWidgetsInGroup();

  // Description:
  // Get annotations as strings.
  //BTX
  vtkstd::string GetWorldPositionAsString();
  vtkstd::string GetPixelPositionAsString();
  vtkstd::string GetPixelValueAsString();
  //ETX

  // Description:
  // INTERNAL DO NOT USE
  vtkSetMacro( WidgetState, int );

  // Description:
  // Set the color for the representation of all widgets in this group.
  virtual void SetColor( double rgb[3] );
  virtual void SetColor( double r, double g, double b );

protected:
  vtkVVHandleWidget();
  ~vtkVVHandleWidget();

  // Callback interface to capture events when
  // placing the widget.
  static void AddPointAction( vtkAbstractWidget* );

  int                        ID;
  int                        PlaceInteractively;
  vtkVVSelectionFrame      * SelectionFrame;
  vtkVVHandleWidgetCommand * HandleCommand;

  //BTX  
  vtkstd::string             Description;
  //ETX

private:
  vtkVVHandleWidget(const vtkVVHandleWidget&);  // Not implemented
  void operator=(const vtkVVHandleWidget&);  // Not implemented
};

#endif
