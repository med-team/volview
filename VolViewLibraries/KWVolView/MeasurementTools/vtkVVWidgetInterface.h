/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVWidgetInterface - a user interface panel.
// .SECTION Description
// A concrete implementation of a user interface panel.
// See vtkKWUserInterfacePanel for a more detailed description.
// .SECTION See Also
// vtkKWUserInterfacePanel vtkKWUserInterfaceManager

#ifndef __vtkVVWidgetInterface_h
#define __vtkVVWidgetInterface_h

#include "vtkVVUserInterfacePanel.h"

class vtkKWFrameWithLabel;
class vtkKWMultiColumnListWithScrollbars;
class vtkVVInteractorWidgetSelector;
class vtkVVSelectionFrame;
class vtkKWEntryWithLabel;
class vtkKWMenuButtonWithSpinButtons;
class vtkContourSegmentationFrame;
class vtkVVPaintbrushWidgetEditor;
class vtkAbstractWidget;

class VTK_EXPORT vtkVVWidgetInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVWidgetInterface* New();
  vtkTypeRevisionMacro(vtkVVWidgetInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Get the id of the page in this user interface
  vtkGetMacro(PageId, int);

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Retrieve internal objects
  vtkGetObjectMacro(InteractorWidgetSelector, vtkVVInteractorWidgetSelector);
  vtkGetObjectMacro(PaintbrushWidgetEditor, vtkVVPaintbrushWidgetEditor);
  vtkGetObjectMacro(InteractorWidgetFrame, vtkKWFrameWithLabel);

  // Description:
  // Callbacks.
  virtual void InteractorWidgetAddDefaultInteractorCallback(int type);
  virtual int InteractorWidgetDefaultInteractorIsSupportedCallback(int type);
  virtual int InteractorWidgetRemoveCallback(int id);
  virtual void InteractorWidgetHasChangedCallback(int id);
  virtual void InteractorWidgetPropertiesUpdatedCallback(int id);

protected:
  vtkVVWidgetInterface();
  ~vtkVVWidgetInterface();

  int PageId;

  // Description:
  // Rebuild the presets
  virtual void PopulateInteractorWidgets();

  // Description:
  // This method is called from InteractorWidgetRemoveCallback. It removes an
  // interactor widget from its selection frame.
  virtual int InteractorWidgetRemoveCallbackInternal(
       vtkAbstractWidget *interactor, vtkVVSelectionFrame *sel_frame);

  // Measurements

  vtkKWFrameWithLabel           *InteractorWidgetFrame;
  vtkVVInteractorWidgetSelector *InteractorWidgetSelector;

  // Description:
  // Update contour segmentation frame
  vtkContourSegmentationFrame *SegmentationFrame;
  virtual void UpdateContourSegmentationFrame(int id);
  
  // Description:
  // Update painbrush editor widget
  vtkVVPaintbrushWidgetEditor *PaintbrushWidgetEditor;
  virtual void UpdatePaintbrushWidgetEditor(int id);
  
  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

private:
  vtkVVWidgetInterface(const vtkVVWidgetInterface&); // Not implemented
  void operator=(const vtkVVWidgetInterface&); // Not Implemented
};

#endif

