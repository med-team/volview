/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkVVPaintbrushOptionsFrame.h"
#include "vtkObjectFactory.h"
#include "vtkKWCheckButtonWithLabel.h"
#include "vtkKWInternationalization.h"
#include "vtkKWScaleWithEntry.h"
#include "vtkKWEntry.h"
#include "vtkKWApplication.h"
#include "vtkKWLabel.h"
#include "vtkKWCheckButton.h"

//---------------------------------------------------------------------------
vtkStandardNewMacro(vtkVVPaintbrushOptionsFrame);
vtkCxxRevisionMacro(vtkVVPaintbrushOptionsFrame, "$Revision: 1.5 $");

//---------------------------------------------------------------------------
vtkVVPaintbrushOptionsFrame::vtkVVPaintbrushOptionsFrame()
{
  this->OpacityScale = NULL;
  this->ShapeSizeScale = NULL;
  this->SingleSliceBrushCheckButton = NULL;
}

//---------------------------------------------------------------------------
vtkVVPaintbrushOptionsFrame::~vtkVVPaintbrushOptionsFrame()
{
  if (this->OpacityScale)
    {
    this->OpacityScale->Delete();
    this->OpacityScale = 0;
    }  

  if (this->ShapeSizeScale)
    {
    this->ShapeSizeScale->Delete();
    this->ShapeSizeScale = 0;
    }

  if (this->SingleSliceBrushCheckButton)
    {
    this->SingleSliceBrushCheckButton->Delete();
    this->SingleSliceBrushCheckButton = 0;
    }
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::CreateWidget()
{
  if (this->IsCreated())
    {
    vtkErrorMacro("The widget is already created.");
    return;
    }

  this->Superclass::CreateWidget();

  const int label_width = 12;
  const int entry_width = 6;

  // --------------------------------------------------------------
  // Opacity scale

  if (!this->OpacityScale)
    {
    this->OpacityScale = vtkKWScaleWithEntry::New();
    }

  this->OpacityScale->SetParent(this);
  this->OpacityScale->Create();
  this->OpacityScale->SetLabelText("Opacity:");
  this->OpacityScale->SetLabelWidth(label_width);
  this->OpacityScale->SetEntryWidth(entry_width);
  this->OpacityScale->SetEntryPositionToRight();
  this->OpacityScale->SetRange(0.0, 1.0);
  this->OpacityScale->SetResolution(0.01);

  this->Script("pack %s -side top -anchor nw -fill x -expand n",
               this->OpacityScale->GetWidgetName());
  
  // --------------------------------------------------------------
  // Shape size scale

  if (!this->ShapeSizeScale)
    {
    this->ShapeSizeScale = vtkKWScaleWithEntry::New();
    }

  this->ShapeSizeScale->SetParent(this);
  this->ShapeSizeScale->Create();
  this->ShapeSizeScale->SetLabelText("Shape Size:");
  this->ShapeSizeScale->SetLabelWidth(label_width);
  this->ShapeSizeScale->SetEntryWidth(entry_width);
  this->ShapeSizeScale->SetEntryPositionToRight();

  this->Script("pack %s -side top -anchor nw -fill x -expand n",
               this->ShapeSizeScale->GetWidgetName());
  
  // --------------------------------------------------------------
  // Single slice mode

  this->SingleSliceBrushCheckButton = vtkKWCheckButtonWithLabel::New();
  this->SingleSliceBrushCheckButton->SetParent(this);
  this->SingleSliceBrushCheckButton->Create();
  this->SingleSliceBrushCheckButton->SetLabelText("Single Slice:");
  this->SingleSliceBrushCheckButton->SetLabelWidth(label_width);
  this->SingleSliceBrushCheckButton->GetWidget()->SetSelectedState(0);
  this->SingleSliceBrushCheckButton->SetBalloonHelpString(
   "If checked, use the brush as a 2D brush, a single slice thick, affecting only one slice at a time. If unchecked, use the brush as a 3D brush.");

  this->Script("pack %s -side top -anchor nw -expand n",
               this->SingleSliceBrushCheckButton->GetWidgetName());
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetOpacityChangedCommand(
  vtkObject *o, const char *method)
{
  this->OpacityScale->SetCommand(o, method);
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetShapeSizeChangedCommand(
  vtkObject *o, const char *method)
{
  this->ShapeSizeScale->SetCommand(o, method);
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetSingleSliceBrushChangedCommand(
  vtkObject *o, const char *method)
{
  this->SingleSliceBrushCheckButton->GetWidget()->SetCommand(o, method);
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetOpacity( double v)
{
  if (this->OpacityScale)
    {
    this->OpacityScale->SetValue(v);
    }
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetShapeSize( double v)
{
  if (this->ShapeSizeScale)
    {
    this->ShapeSizeScale->SetValue(v);
    }
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetShapeSizeRange( double v1, double v2)
{
  if (this->ShapeSizeScale)
    {
    this->ShapeSizeScale->SetRange(v1, v2);
    }
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetShapeSizeResolution( double v)
{
  if (this->ShapeSizeScale)
    {
    this->ShapeSizeScale->SetResolution(v);
    }
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::SetSingleSliceBrush(int v)
{
  if (this->SingleSliceBrushCheckButton)
    {
    this->SingleSliceBrushCheckButton->GetWidget()->SetSelectedState(v);
    }
}

//----------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::UpdateEnableState()
{
  this->Superclass::UpdateEnableState();

  this->PropagateEnableState(this->OpacityScale);
  this->PropagateEnableState(this->ShapeSizeScale);
  this->PropagateEnableState(this->SingleSliceBrushCheckButton);
}

//---------------------------------------------------------------------------
void vtkVVPaintbrushOptionsFrame::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

