/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef __itkDebugUtilities_h
#define __itkDebugUtilities_h

#include "itkLightObject.h"
#include "itkImage.h"
#include "itkImageFileWriter.h"

#define itkDebugWriteImage( image, text ) \
  { \
  std::ostringstream _nameStream; \
  _nameStream << text << std::ends; \
  itk::DebugUtilities::Write( image, _nameStream.str().c_str() ); \
  }


namespace itk
{

class ITK_EXPORT DebugUtilities : public LightObject 
{  
public:

  /** Standard class typedefs. */
  typedef DebugUtilities               Self;
  typedef LightObject                  Superclass;
  typedef SmartPointer<Self>           Pointer;
  typedef SmartPointer<const Self>     ConstPointer;  

  template< typename TImage >
  static void Write( SmartPointer< TImage > image, std::string fname )
    {
    std::cout << "Writing image: " << fname << std::endl;
    typedef ImageFileWriter< TImage > WriterType;
    typename WriterType::Pointer w = WriterType::New();
    w->SetInput( image );
    w->SetFileName( fname );
    w->UseCompressionOn();
    w->Update();      
    }

  template< typename TImage >
  static void Write( TImage * image, std::string fname )
    {
    std::cout << "Writing image: " << fname << std::endl;
    typedef ImageFileWriter< TImage > WriterType;
    typename WriterType::Pointer w = WriterType::New();
    w->SetInput( image );
    w->SetFileName( fname );
    w->UseCompressionOn();
    w->Update();      
    }
};
 
}

#endif

