/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkITKLesionSegmentationImageFilter4 - Wrapper class around itk::LesionSegmentationImageFilter4
// .SECTION Description
// vtkITKLesionSegmentationImageFilter4


#ifndef __vtkITKLesionSegmentationImageFilter4_h
#define __vtkITKLesionSegmentationImageFilter4_h


#include "vtkITKLesionSegmentationImageFilterBase.h"
#include "itkLesionSegmentationImageFilter4.h"

class VTK_EXPORT vtkITKLesionSegmentationImageFilter4 : public vtkITKLesionSegmentationImageFilterBase
{
 public:
  static vtkITKLesionSegmentationImageFilter4 *New();
  vtkTypeRevisionMacro(vtkITKLesionSegmentationImageFilter4, vtkITKLesionSegmentationImageFilterBase);

  //BTX
  RegionType GetRegionOfInterest()
    {
    return this->GetImageFilterPointer()->GetRegionOfInterest();
    }

  PointListType GetSeeds()
    {
    return this->GetImageFilterPointer()->GetSeeds();
    }

  double GetSigmoidBeta()
    {
    return this->GetImageFilterPointer()->GetSigmoidBeta();
    }

  void SetRegionOfInterest ( RegionType value )
    {
    GetImageFilterPointer()->SetRegionOfInterest( value );
    }

  void SetSeeds ( PointListType value )
    {
    GetImageFilterPointer()-> SetSeeds( value );
    }

  void SetSigmoidBeta( double d )
    {
    double dCurr = GetImageFilterPointer()->GetSigmoidBeta();
    if (d != dCurr)
      {
      GetImageFilterPointer()->SetSigmoidBeta(d);
      this->Modified();
      this->m_Process->Modified();
      }
    }

  //ETX

  const char *GetStatusMessage()
    {
    return this->GetImageFilterPointer()->GetStatusMessage();
    }

protected:
  //BTX
  typedef itk::LesionSegmentationImageFilter4<Superclass::InputImageType,Superclass::OutputImageType> ImageFilterType;
  vtkITKLesionSegmentationImageFilter4() : Superclass ( ImageFilterType::New() ){};
  ~vtkITKLesionSegmentationImageFilter4() {};
  ImageFilterType* GetImageFilterPointer() { return dynamic_cast<ImageFilterType*> ( m_Filter.GetPointer() ); }

  //ETX
  
private:
  vtkITKLesionSegmentationImageFilter4(const vtkITKLesionSegmentationImageFilter4&);  // Not implemented.
  void operator=(const vtkITKLesionSegmentationImageFilter4&);  // Not implemented.
};

#endif





