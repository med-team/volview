/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkITKLesionSegmentationImageFilter8 - Wrapper class around itk::LesionSegmentationImageFilter8ImageFilter
// .SECTION Description
// vtkITKLesionSegmentationImageFilter8


#ifndef __vtkITKLesionSegmentationImageFilter8_h
#define __vtkITKLesionSegmentationImageFilter8_h


#include "vtkITKLesionSegmentationImageFilterBase.h"
#include "itkLesionSegmentationImageFilter8.h"

class VTK_EXPORT vtkITKLesionSegmentationImageFilter8 : public vtkITKLesionSegmentationImageFilterBase
{
 public:
  static vtkITKLesionSegmentationImageFilter8 *New();
  vtkTypeRevisionMacro(vtkITKLesionSegmentationImageFilter8, vtkITKLesionSegmentationImageFilterBase);

  //BTX
  RegionType GetRegionOfInterest()
    {
    return this->GetImageFilterPointer()->GetRegionOfInterest();
    }

  PointListType GetSeeds()
    {
    return this->GetImageFilterPointer()->GetSeeds();
    }

  double GetSigmoidBeta()
    {
    return this->GetImageFilterPointer()->GetSigmoidBeta();
    }

  void SetRegionOfInterest ( RegionType value )
    {
    GetImageFilterPointer()->SetRegionOfInterest( value );
    }

  void SetSeeds ( PointListType value )
    {
    GetImageFilterPointer()-> SetSeeds( value );
    }

  void SetSigmoidBeta( double d )
    {
    double dCurr = GetImageFilterPointer()->GetSigmoidBeta();
    if (d != dCurr)
      {
      GetImageFilterPointer()->SetSigmoidBeta(d);
      this->Modified();
      this->m_Process->Modified();
      this->itkExporter->Modified();
      this->vtkImporter->Modified();
      }
    }
  //ETX

  virtual void SetAbortExecute( int boolean );

  const char *GetStatusMessage()
    {
    return this->GetImageFilterPointer()->GetStatusMessage();
    }

protected:
  //BTX
  typedef itk::LesionSegmentationImageFilter8<Superclass::InputImageType,Superclass::OutputImageType> ImageFilterType;
  vtkITKLesionSegmentationImageFilter8() : Superclass ( ImageFilterType::New() ) 
    { 
    this->GetImageFilterPointer()->SetAnisotropyThreshold(1); // was 1.5 in ISP2.3
    }
  ~vtkITKLesionSegmentationImageFilter8() {};
  ImageFilterType* GetImageFilterPointer() { return dynamic_cast<ImageFilterType*> ( m_Filter.GetPointer() ); }

  //ETX
  
private:
  vtkITKLesionSegmentationImageFilter8(const vtkITKLesionSegmentationImageFilter8&);  // Not implemented.
  void operator=(const vtkITKLesionSegmentationImageFilter8&);  // Not implemented.
};

#endif





