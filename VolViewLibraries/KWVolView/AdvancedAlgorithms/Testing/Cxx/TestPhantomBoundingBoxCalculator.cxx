/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "itkPhantomBoundingBox.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkRegionOfInterestImageFilter.h"
#include <iostream>

int main( int argc, char *argv[] )
{
  if (argc < 9)
    {
    std::cerr << "Usage args : PhantomCTData.mha OutputCroppedRegion.mha TeflonCenterX TeflonCenterY TeflonCenterZ DelrinCenterX DelrinCenterY DelrinCenterZ CylinderImage " << std::endl;
    return EXIT_FAILURE;
    }

  typedef itk::Image< short, 3 > ImageType;
  typedef itk::PhantomBoundingBox< ImageType > CalculatorType;

  itk::ImageFileReader< ImageType >::Pointer reader = 
    itk::ImageFileReader< ImageType >::New();
  reader->SetFileName( argv[1] );
  reader->Update();

  CalculatorType::Pointer calculator = CalculatorType::New();
  calculator->SetImage( reader->GetOutput() );
  CalculatorType::PointType teCenter, deCenter;
  teCenter[0] = atof(argv[3]); teCenter[1] = atof(argv[4]); teCenter[2] = atof(argv[5]);
  deCenter[0] = atof(argv[6]); deCenter[1] = atof(argv[7]); deCenter[2] = atof(argv[8]);
  calculator->SetTeflonCenter( teCenter );
  calculator->SetDelrinCenter( deCenter );
  calculator->Compute();
  calculator->Print(std::cout);

  //std::cout << "BBox: " << calculator->GetBounds() << std::endl;
  //std::cout << "BBox region: " << calculator->GetBoundingBoxRegion() << std::endl;

  /*
  typedef itk::RegionOfInterestImageFilter< ImageType, ImageType > ROIFilterType;
  ROIFilterType::Pointer roi = ROIFilterType::New();
  roi->SetInput( reader->GetOutput() );
  roi->SetRegionOfInterest(calculator->GetBoundingBoxRegion());
  roi->Update();

  itk::ImageFileWriter< ImageType >::Pointer writer = 
    itk::ImageFileWriter< ImageType >::New();
  writer->SetFileName( argv[2] );
  writer->SetInput(roi->GetOutput());
  writer->SetUseCompression(true);
  writer->Update();

  writer->SetFileName( argv[9] );
  writer->SetInput(calculator->GetPhantomImage());
  writer->Update();


  itk::ImageFileWriter< CalculatorType::BinaryImageType >::Pointer writer2 = 
    itk::ImageFileWriter< CalculatorType::BinaryImageType >::New();
  writer2->SetFileName( argv[10] );
  writer2->SetInput(calculator->GetCentralBitsImage(0));
  writer2->Update();
  */

  return EXIT_SUCCESS;
}

