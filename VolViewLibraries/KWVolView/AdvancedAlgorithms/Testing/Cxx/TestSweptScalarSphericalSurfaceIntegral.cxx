/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkSweepScalarSphericalSurfaceIntegral.h"
#include "vtkImageData.h"
#include "vtkImageGaussianSource.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

int main( int argc, char *argv[] )
{
  vtkSmartPointer< vtkImageGaussianSource > gaussian
    = vtkSmartPointer< vtkImageGaussianSource >::New();
  gaussian->SetCenter( 10, 10, 10 );
  gaussian->SetMaximum( 1000 );
  gaussian->SetStandardDeviation( 5 );
  gaussian->SetWholeExtent( -10, 30, -10, 30, -10, 30 );
  gaussian->Update();
  
  vtkSmartPointer< vtkSweepScalarSphericalSurfaceIntegral > sweptSphereIntegral
    = vtkSmartPointer< vtkSweepScalarSphericalSurfaceIntegral >::New();
  sweptSphereIntegral->SetInput( gaussian->GetOutput() );
  sweptSphereIntegral->SetCenter( 10, 10, 10 );
  sweptSphereIntegral->SetRadius( atof( argv[1] ) );
  sweptSphereIntegral->SetSweepDistance( atof(argv[2]) );
  sweptSphereIntegral->SetSweepResolution( atoi(argv[3]) );
  sweptSphereIntegral->Update();

  std::cout << "Maximum Surface Mean : " << sweptSphereIntegral->GetMaximumSurfaceMean() << std::endl;
  std::cout << "Maximum Surface Mean Radius : " << sweptSphereIntegral->GetMaximumSurfaceMeanRadius() << std::endl;
  std::cout << "Minimum Surface Mean : " << sweptSphereIntegral->GetMinimumSurfaceMean() << std::endl;
  std::cout << "Minimum Surface Mean Radius : " << sweptSphereIntegral->GetMinimumSurfaceMeanRadius() << std::endl;
  
  return EXIT_SUCCESS;
}



