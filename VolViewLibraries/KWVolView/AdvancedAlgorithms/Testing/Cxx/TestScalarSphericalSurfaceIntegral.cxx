/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkNormalizedSphereTesselatedPointSource.h"
#include "vtkScalarSphericalSurfaceIntegral.h"
#include "vtkImageData.h"
#include "vtkImageGaussianSource.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

int main( int argc, char *argv[] )
{
  vtkSmartPointer< vtkNormalizedSphereTesselatedPointSource >
    spherePoints = vtkSmartPointer< vtkNormalizedSphereTesselatedPointSource >::New();
  spherePoints->Update();

  vtkSmartPointer< vtkXMLPolyDataWriter > w = vtkSmartPointer< vtkXMLPolyDataWriter >::New();
  w->SetInput( spherePoints->GetOutput() );
  w->SetFileName( argv[1] );
  w->Write();

  vtkSmartPointer< vtkImageGaussianSource > gaussian
    = vtkSmartPointer< vtkImageGaussianSource >::New();
  gaussian->SetCenter( 10, 10, 10 );
  gaussian->SetMaximum( 1000 );
  gaussian->SetStandardDeviation( 5 );
  gaussian->SetWholeExtent( -10, 30, -10, 30, -10, 30 );
  gaussian->Update();
  
  vtkSmartPointer< vtkScalarSphericalSurfaceIntegral > sphereIntegral
    = vtkSmartPointer< vtkScalarSphericalSurfaceIntegral >::New();
  sphereIntegral->SetInput( gaussian->GetOutput() );
  sphereIntegral->SetCenter( 10, 10, 10 );
  sphereIntegral->SetRadius( atof( argv[2] ) );
  sphereIntegral->Update();

  std::cout << "Surface Integral : " << sphereIntegral->GetIntegral() << std::endl;
  std::cout << "Surface mean : " << sphereIntegral->GetMean() << std::endl;
  std::cout << "Number of sample points : " << sphereIntegral->GetNumberOfSamplePoints() << std::endl;
  std::cout << "Number of points in the PolyData : " << spherePoints->GetOutput()->GetNumberOfPoints() << std::endl;
  
  return EXIT_SUCCESS;
}


