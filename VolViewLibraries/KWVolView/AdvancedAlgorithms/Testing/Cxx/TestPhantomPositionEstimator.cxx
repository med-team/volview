/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "itkPhantomPositionCalculator.h"
#include "itkImageFileReader.h"
#include <iostream>

int main( int argc, char *argv[] )
{
  if (argc < 2)
    {
    std::cerr << "Usage args : PhantomCTData.mha EstimateOfForeGroundDensity Radius DensityDelta" << std::endl;
    return EXIT_FAILURE;
    }

  typedef itk::Image< short, 3 > ImageType;
  typedef itk::PhantomPositionCalculator< ImageType > CalculatorType;

  itk::ImageFileReader< ImageType >::Pointer reader = 
    itk::ImageFileReader< ImageType >::New();
  reader->SetFileName( argv[1] );
  reader->Update();

  CalculatorType::Pointer calculator = CalculatorType::New();
  calculator->SetImage( reader->GetOutput() );
  calculator->SetEstimateOfForegroundDensity( atoi( argv[2] ) );
  calculator->SetRadius( atoi( argv[3] ) );
  calculator->SetDensityDelta( atoi( argv[4] ) );
  std::vector< CalculatorType::PointType > positions = calculator->ComputePosition();

  return EXIT_SUCCESS;
}
