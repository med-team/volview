/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkType.h"
#include "itkPhantomSegmentationImageFilter.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include <iostream>

int main( int argc, char *argv[] )
{
  if (argc < 8)
    {
    std::cerr << "Usage args : PhantomCTData.mha ForeGroundDensity BackgroundDensity Center_x center_y center_z Radius segmentation" << std::endl;
    return EXIT_FAILURE;
    }

  typedef itk::Image< short, 3 > ImageType;
  typedef itk::PhantomSegmentationImageFilter< ImageType > SegmenterType;

  itk::ImageFileReader< ImageType >::Pointer reader = 
                itk::ImageFileReader< ImageType >::New();
  reader->SetFileName( argv[1] );
  reader->Update();

  SegmenterType::Pointer segmenter = SegmenterType::New();
  segmenter->SetInput( reader->GetOutput() );
  segmenter->SetForegroundDensity( atof( argv[2] ) );
  segmenter->SetBackgroundDensity( atof( argv[3] ) );
  double c[3] = { atof(argv[4]), atof(argv[5]), atof(argv[6]) };
  segmenter->SetCenter( c );
  segmenter->SetRadius( atoi( argv[7] ) );

  segmenter->Update();

  typedef SegmenterType::OutputImageType OutputImageType;
  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName( argv[8] );
  writer->SetInput( segmenter->GetOutput() );
  writer->Update();

  return EXIT_SUCCESS;
}

