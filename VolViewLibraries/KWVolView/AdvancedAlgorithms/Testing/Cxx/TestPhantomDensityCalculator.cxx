/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "itkPhantomDensityCalculator.h"
#include "itkImageFileReader.h"
#include <iostream>

int main( int argc, char *argv[] )
{
  if (argc < 8)
    {
    std::cerr << "Usage args : PhantomCTData.mha EstimateOfForeGroundDensity EstimateOfBackgroundDensity Center_x center_y center_z Radius " << std::endl;
    return EXIT_FAILURE;
    }

  typedef itk::Image< short, 3 > ImageType;
  typedef itk::PhantomDensityCalculator< ImageType > CalculatorType;

  itk::ImageFileReader< ImageType >::Pointer reader = 
    itk::ImageFileReader< ImageType >::New();
  reader->SetFileName( argv[1] );
  reader->Update();

  CalculatorType::Pointer calculator = CalculatorType::New();
  calculator->SetImage( reader->GetOutput() );
  calculator->SetEstimateOfForegroundDensity( atof( argv[2] ) );
  calculator->SetEstimateOfBackgroundDensity( atof( argv[3] ) );
  double c[3] = { atof(argv[4]), atof(argv[5]), atof(argv[6]) };
  calculator->SetCenter( c );
  calculator->SetRadius( atoi( argv[7] ) );
  calculator->EstimateBackgroundDensityOn();
  calculator->Compute();

  reader->GetOutput()->Print(std::cout);
  std::cout << "calculated FG: " << calculator->GetCalculatedForegroundDensity() << std::endl;
  std::cout << "calculated BG: " << calculator->GetCalculatedBackgroundDensity() << std::endl;
  std::cout << "calculated FGSD: " << calculator->GetCalculatedStandardDeviationInForegroundDensity() << std::endl;
  std::cout << "calculated BGSD: " << calculator->GetCalculatedStandardDeviationInBackgroundDensity() << std::endl;
  std::cout << "nPixelsUsedInevalBG: " << calculator->GetNumberOfPixelsUsedInEvaluatingBackgroundDensity() << std::endl;

  return EXIT_SUCCESS;
}
