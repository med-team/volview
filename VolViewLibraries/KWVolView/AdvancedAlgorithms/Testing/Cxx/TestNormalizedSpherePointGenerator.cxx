/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkNormalizedSphereTesselatedPointSource.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

int main( int argc, char *argv[] )
{
  if (argc < 4)
    {
    std::cerr << "Usage: outputFilename.vtp solidAngle direction" << std::endl;
    return EXIT_FAILURE;
    }

  vtkSmartPointer< vtkNormalizedSphereTesselatedPointSource >
    spherePoints = vtkSmartPointer< vtkNormalizedSphereTesselatedPointSource >::New();
  spherePoints->SetSolidAngle(atof(argv[2]));
  spherePoints->SetDirection(atof(argv[3]));
  spherePoints->Update();

  vtkSmartPointer< vtkXMLPolyDataWriter > w = vtkSmartPointer< vtkXMLPolyDataWriter >::New();
  w->SetInput( spherePoints->GetOutput() );
  w->SetFileName( argv[1] );
  w->Write();
  
  return EXIT_SUCCESS;
}

