/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWPSFLogDialog - a log dialog.
// .SECTION Description
// This widget can be used to display various types of records/events in the
// form of a multicolumn log. Each record is timestamped automatically, and 
/// the interface allow the user to sort the list by time, type, or 
// description.
// This dialog is a standalone toplevel, but uses a vtkKWPSFLogWidget internally.
// The vtkKWPSFLogWidget class can be inserted in any widget hierarchy.
// .SECTION Thanks
// This work is part of the National Alliance for Medical Image
// Computing (NAMIC), funded by the National Institutes of Health
// through the NIH Roadmap for Medical Research, Grant U54 EB005149.
// Information on the National Centers for Biomedical Computing
// can be obtained from http://nihroadmap.nih.gov/bioinformatics.
// .SECTION See Also
// vtkKWPSFLogWidget

#ifndef __vtkKWPSFLogDialog_h
#define __vtkKWPSFLogDialog_h

#include "vtkKWMessageDialog.h"

class vtkKWApplication;
class vtkKWPSFLogWidget;
class vtkKWPushButton;

class VTK_EXPORT vtkKWPSFLogDialog : public vtkKWMessageDialog
{
public:
  static vtkKWPSFLogDialog* New();
  vtkTypeRevisionMacro(vtkKWPSFLogDialog,vtkKWMessageDialog);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Get the internal log widget so that its API will be exposed directly
  vtkGetObjectMacro(LogWidget, vtkKWPSFLogWidget);   
   
protected:
  vtkKWPSFLogDialog();
  ~vtkKWPSFLogDialog();

  // Description:
  // Create the widget.
  virtual void CreateWidget();
  
  // Description:
  // Member variables
  vtkKWPSFLogWidget* LogWidget;

private:
  vtkKWPSFLogDialog(const vtkKWPSFLogDialog&); // Not implemented
  void operator=(const vtkKWPSFLogDialog&); // Not implemented
};

#endif
