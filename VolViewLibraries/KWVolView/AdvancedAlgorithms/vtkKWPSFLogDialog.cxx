/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkKWPSFLogDialog.h"
#include "vtkKWPSFLogWidget.h"
#include "vtkKWApplication.h"
#include "vtkObjectFactory.h"
#include "vtkKWPushButton.h"

#include <vtksys/stl/string>

//----------------------------------------------------------------------------
vtkStandardNewMacro( vtkKWPSFLogDialog );
vtkCxxRevisionMacro(vtkKWPSFLogDialog, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
vtkKWPSFLogDialog::vtkKWPSFLogDialog()
{
  this->LogWidget = vtkKWPSFLogWidget::New();

  this->Options = 
    vtkKWMessageDialog::YesDefault | vtkKWMessageDialog::Resizable;
  this->Modal = 0;
}

//----------------------------------------------------------------------------
vtkKWPSFLogDialog::~vtkKWPSFLogDialog()
{
  if (this->LogWidget)
    {
    this->LogWidget->Delete();  
    }
}

//----------------------------------------------------------------------------
void vtkKWPSFLogDialog::CreateWidget()
{
  // Check if already created

  if (this->IsCreated())
    {
    vtkErrorMacro(<< this->GetClassName() << " already created");
    return;
    }
  
  // Call the superclass to create the whole widget

  this->Superclass::CreateWidget();

  this->SetMinimumSize(400, 450);
  this->SetSize(650, 550);

  vtksys_stl::string title;
  if (this->GetApplication()->GetName())
    {
    title += this->GetApplication()->GetName();
    title += ": ";
    }
  title += "PSF Log Viewer";
  this->SetTitle(title.c_str());
  
  // Record viewer

  if (!this->LogWidget)
    {
    this->LogWidget = vtkKWPSFLogWidget::New();
    }
  this->LogWidget->SetParent(this->GetBottomFrame());
  this->LogWidget->Create();
  this->Script("pack %s -anchor nw -fill both -expand true -padx 2 -pady 2",
               this->LogWidget->GetWidgetName());
}

//----------------------------------------------------------------------------
void vtkKWPSFLogDialog::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  if(this->LogWidget)
    {
    this->LogWidget->PrintSelf(os, indent);
    }
}
