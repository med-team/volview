/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkITKLesionSegmentationImageFilterBase - Wrapper class around itk::LesionSegmentationImageFilterBaseImageFilter
// .SECTION Description
// vtkITKLesionSegmentationImageFilterBase


#ifndef __vtkITKLesionSegmentationImageFilterBase_h
#define __vtkITKLesionSegmentationImageFilterBase_h

#include "vtkITKImageToImageFilterSSF.h"
#include "itkImageRegion.h"
#include "itkLandmarkSpatialObject.h"

class VTK_EXPORT vtkITKLesionSegmentationImageFilterBase : public vtkITKImageToImageFilterSSF
{
public:
  vtkTypeMacro(vtkITKLesionSegmentationImageFilterBase, vtkITKImageToImageFilterSSF);

  //BTX
  typedef itk::ImageRegion< 3 > RegionType;
  typedef itk::LandmarkSpatialObject< 3 > SeedSpatialObjectType;
  typedef SeedSpatialObjectType::PointListType PointListType;

  virtual RegionType GetRegionOfInterest() = 0;
  virtual PointListType GetSeeds() = 0;
  virtual double GetSigmoidBeta() = 0;
  virtual void SetRegionOfInterest ( RegionType value ) = 0;
  virtual void SetSeeds ( PointListType value ) = 0;
  virtual void SetSigmoidBeta( double d ) = 0;
  //ETX

  virtual const char *GetStatusMessage() = 0;

protected:
  //BTX
  vtkITKLesionSegmentationImageFilterBase( GenericFilterType* filter ) : Superclass(filter) {}
  //ETX
  
private:
  vtkITKLesionSegmentationImageFilterBase(const vtkITKLesionSegmentationImageFilterBase&);  // Not implemented.
  void operator=(const vtkITKLesionSegmentationImageFilterBase&);  // Not implemented.
};

#endif






