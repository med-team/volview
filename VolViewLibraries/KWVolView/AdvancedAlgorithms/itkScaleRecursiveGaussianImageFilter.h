/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef __itkNegatedRecursiveGaussianImageFilter_h
#define __itkNegatedRecursiveGaussianImageFilter_h

#include "itkRecursiveGaussianImageFilter.h"

namespace itk
{

/** \class NegatedRecursiveGaussianImageFilter
 */
template <typename TInputImage, typename TOutputImage=TInputImage >
class ITK_EXPORT NegatedRecursiveGaussianImageFilter:
    public RecursiveGaussianImageFilter<TInputImage,TOutputImage>
{
public:
  /** Standard class typedefs. */
  typedef NegatedRecursiveGaussianImageFilter          Self;
  typedef ImageToImageFilter<TInputImage,TOutputImage> Superclass;
  typedef SmartPointer<Self>                           Pointer;
  typedef SmartPointer<const Self>                     ConstPointer;
  
  
  /** Pixel Type of the input image */
  typedef TInputImage                                    InputImageType;

  /** Image dimension. */
  itkStaticConstMacro(ImageDimension, unsigned int,
                      ::itk::GetImageDimension<TInputImage>::ImageDimension);

  /** Run-time type information (and related methods).   */
  itkTypeMacro( NegatedRecursiveGaussianImageFilter, RecursiveGaussianImageFilter);

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

protected:
  
  NegatedRecursiveGaussianImageFilter();
  virtual ~NegatedRecursiveGaussianImageFilter() {};
  
  virtual void SetUp(ScalarRealType spacing);

  double m_Negated;

private:

  NegatedRecursiveGaussianImageFilter(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkNegatedRecursiveGaussianImageFilter.txx"
#endif

#endif


