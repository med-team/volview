/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVLesionSizingInterface - Measurement user interface panel customized for lesion sizing
// .SECTION Description
// .SECTION See Also

#ifndef __vtkVVLesionSizingInterface_h
#define __vtkVVLesionSizingInterface_h

#include "vtkVVUserInterfacePanel.h"

class vtkKWSeparator;
class vtkKWFrame;
class vtkKWLabel;
class vtkKWMessage;
class vtkKWMessageWithLabel;
class vtkKWLabelWithLabel;
class vtkKWLabelWithLabelSet;
class vtkKWMenuButtonWithLabel;
class vtkKWFrameWithLabel;
class vtkKWMultiColumnListWithScrollbars;
class vtkKWEntryWithLabel;
class vtkKWMenuButtonWithSpinButtons;
class vtkKWPushButton;
class vtkKWRadioButtonSet;
class vtkVVContourSelector;
class vtkVVDataItemVolume;
class vtkImageData;
class vtkActor;
class vtkCutter;
class vtkITKLesionSegmentationImageFilterBase;
class vtkVVDataItemVolumeContour;
class vtkVVDataItem;

class VTK_EXPORT vtkVVLesionSizingInterface : public vtkVVUserInterfacePanel
{
public:
  static vtkVVLesionSizingInterface* New();
  vtkTypeRevisionMacro(vtkVVLesionSizingInterface,vtkVVUserInterfacePanel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Create the interface objects.
  virtual void Create();

  // Description:
  // Refresh the interface given the current value of the Window and its
  // views/composites/widgets.
  virtual void Update();

  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // Callbacks
  void StartCallback();
  void NextCallback();
  void CancelCallback();

protected:
  vtkVVLesionSizingInterface();
  ~vtkVVLesionSizingInterface();

  virtual void UpdateInternal();
  
  // Description:
  virtual void Segment();
  virtual vtkVVDataItemVolumeContour * Segment( vtkVVDataItemVolume * );

  virtual int DisplayErrorMessage( const char *text );

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // Description:
  virtual void SetInstructionsText( const char * string );
  virtual void SetInstructionsIconToPredefinedIcon(int icon_index);
  virtual void PopulateOptions();

  // Description:
  virtual void CreateNewLesionSegmentationStrategy( bool partsolid );

  // Description:
  // Sanity checks to ensure that the dataset is 3D etc should be placed here
  int IsSupported( vtkVVDataItem* data );

  vtkKWFrame                     * ButtonFrame;
  vtkKWPushButton                * StartButton;
  vtkKWPushButton                * NextButton;
  vtkKWPushButton                * CancelButton;
  vtkKWFrameWithLabel            * LesionSizingFrame;
  vtkKWFrameWithLabel            * ContourListFrame;
  vtkKWFrame                     * InstructionsFrame;
  vtkKWLabelWithLabel            * InstructionsMessage;
  vtkKWFrame                     * OptionsFrame;
  vtkKWFrameWithLabel            * AdvancedOptionsFrame;
  vtkKWSeparator                 * Sep1;
  vtkKWRadioButtonSet            * OptionsRadioButtonSet;
  vtkKWRadioButtonSet            * SegmentationAlgorithmRadioButtonSet;
  vtkVVContourSelector           * ContourSelector;
  int                              State;  

  //BTX
  enum { Start = 0, DefiningBoundingBox, DefiningSeeds, Segmenting, Segmented, Cancelling };
  //ETX

private:
  vtkVVLesionSizingInterface(const vtkVVLesionSizingInterface&); // Not implemented
  void operator=(const vtkVVLesionSizingInterface&); // Not Implemented

  vtkITKLesionSegmentationImageFilterBase * LesionSegmentationFilter;
};

#endif

