/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkITKLesionSegmentationImageFilter7.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkITKLesionSegmentationImageFilter7, "$Revision: 1.3 $");
vtkStandardNewMacro(vtkITKLesionSegmentationImageFilter7);

