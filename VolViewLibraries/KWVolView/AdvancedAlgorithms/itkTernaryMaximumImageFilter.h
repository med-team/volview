/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __itkTernaryMaximumImageFilter_h
#define __itkTernaryMaximumImageFilter_h

#include "itkTernaryFunctorImageFilter.h"

namespace itk
{
  
/** \class TernaryMaximumImageFilter
 * \brief Implements pixel-wise addition of three images.
 *
 * This class is parametrized over the types of the three 
 * input images and the type of the output image. 
 * Numeric conversions (castings) are done by the C++ defaults.
 * 
 * \ingroup IntensityImageFilters
 */
namespace Function {  
  
template< class TInput1, class TInput2, class TInput3, class TOutput>
class Maximum3
{
public:
  Maximum3() {}
  ~Maximum3() {}
  bool operator!=( const Maximum3 & ) const
    {
    return false;
    }
  bool operator==( const Maximum3 & other ) const
    {
    return !(*this != other);
    }
  inline TOutput operator()( const TInput1 & A, 
                             const TInput2 & B,
                             const TInput3 & C) const
  {
    if(A >= B && A >= C)
      {
      return static_cast<TOutput>(A);
      }
    if(B >= A && B >= C)
      {
      return static_cast<TOutput>(B);
      }
    if(C >= A && C >= B)
      {
      return static_cast<TOutput>(C);
      }
  }
}; 
}

template <class TInputImage1, class TInputImage2=TInputImage1, 
          class TInputImage3=TInputImage1, class TOutputImage=TInputImage1>
class ITK_EXPORT TernaryMaximumImageFilter :
    public
TernaryFunctorImageFilter<TInputImage1,TInputImage2,
                          TInputImage3,TOutputImage, 
                          Function::Maximum3< typename TInputImage1::PixelType, 
                                          typename TInputImage2::PixelType,
                                          typename TInputImage3::PixelType,
                                          typename TOutputImage::PixelType>   >
{
public:
  /** Standard class typedefs. */
  typedef TernaryMaximumImageFilter                            Self;
  typedef TernaryFunctorImageFilter<
    TInputImage1,TInputImage2,
    TInputImage3,TOutputImage, 
    Function::Maximum3< typename TInputImage1::PixelType,
                    typename TInputImage2::PixelType,
                    typename TInputImage3::PixelType,
                    typename TOutputImage::PixelType>   >  Superclass;
  typedef SmartPointer<Self>                               Pointer;
  typedef SmartPointer<const Self>                         ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);
  
  /** Runtime information support. */
  itkTypeMacro(TernaryMaximumImageFilter, 
               TernaryFunctorImageFilter);

protected:
  TernaryMaximumImageFilter() {}
  virtual ~TernaryMaximumImageFilter() {}

private:
  TernaryMaximumImageFilter(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

};

} // end namespace itk


#endif

