/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// .NAME vtkITKLesionSegmentationImageFilter7 - Wrapper class around itk::LesionSegmentationImageFilter7ImageFilter
// .SECTION Description
// vtkITKLesionSegmentationImageFilter7


#ifndef __vtkITKLesionSegmentationImageFilter7_h
#define __vtkITKLesionSegmentationImageFilter7_h


#include "vtkITKLesionSegmentationImageFilterBase.h"
#include "itkLesionSegmentationImageFilter7.h"

class VTK_EXPORT vtkITKLesionSegmentationImageFilter7 : public vtkITKLesionSegmentationImageFilterBase
{
 public:
  static vtkITKLesionSegmentationImageFilter7 *New();
  vtkTypeRevisionMacro(vtkITKLesionSegmentationImageFilter7, vtkITKLesionSegmentationImageFilterBase);

  //BTX
  RegionType GetRegionOfInterest()
    {
    return this->GetImageFilterPointer()->GetRegionOfInterest();
    }

  PointListType GetSeeds()
    {
    return this->GetImageFilterPointer()->GetSeeds();
    }

  double GetSigmoidBeta()
    {
    return this->GetImageFilterPointer()->GetSigmoidBeta();
    }

  void SetRegionOfInterest ( RegionType value )
    {
    GetImageFilterPointer()->SetRegionOfInterest( value );
    }

  void SetSeeds ( PointListType value )
    {
    GetImageFilterPointer()-> SetSeeds( value );
    }

  void SetSigmoidBeta( double d )
    {
    double dCurr = GetImageFilterPointer()->GetSigmoidBeta();
    if (d != dCurr)
      {
      GetImageFilterPointer()->SetSigmoidBeta(d);
      this->Modified();
      this->m_Process->Modified();
      }
    }
  //ETX

  const char *GetStatusMessage()
    {
    return this->GetImageFilterPointer()->GetStatusMessage();
    }

protected:
  //BTX
  typedef itk::LesionSegmentationImageFilter7<Superclass::InputImageType,Superclass::OutputImageType> ImageFilterType;
  vtkITKLesionSegmentationImageFilter7() : Superclass ( ImageFilterType::New() ){};
  ~vtkITKLesionSegmentationImageFilter7() {};
  ImageFilterType* GetImageFilterPointer() { return dynamic_cast<ImageFilterType*> ( m_Filter.GetPointer() ); }

  //ETX
  
private:
  vtkITKLesionSegmentationImageFilter7(const vtkITKLesionSegmentationImageFilter7&);  // Not implemented.
  void operator=(const vtkITKLesionSegmentationImageFilter7&);  // Not implemented.
};

#endif





