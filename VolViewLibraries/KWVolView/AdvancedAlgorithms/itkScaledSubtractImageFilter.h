/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __itkScaledSubtractImageFilter_h
#define __itkScaledSubtractImageFilter_h

#include "itkBinaryFunctorImageFilter.h"

namespace itk
{
  
/** \class ScaledSubtractImageFilter
 * \brief Implements an operator for pixel-wise subtraction of two images.
 *
 * Output = Input1 - Input2.
 * 
 * This class is parametrized over the types of the two 
 * input images and the type of the output image. 
 * Numeric conversions (castings) are done by the C++ defaults.
 *
 * \ingroup IntensityImageFilters Multithreaded
 */
namespace Function {  
  
template< class TInput1, class TInput2=TInput1, class TOutput=TInput1>
class ScaledSub2
{
public:
  typedef typename NumericTraits< TInput1 >::RealType       RealType;
  ScaledSub2() {}
  ~ScaledSub2() {}
  bool operator!=( const ScaledSub2 & ) const
    {
    return false;
    }
  bool operator==( const ScaledSub2 & other ) const
    {
    return !(*this != other);
    }
  inline TOutput operator()( const TInput1 & A, const TInput2 & B) const
    { return (TOutput)( A - m_Scale * B); }

  void SetScale( RealType alpha )
    { 
    m_Scale = alpha; 
    }
  RealType GetScale() const
    {
    return m_Scale;
    }
  
private:
  double m_Scale;
}; 
}

template <class TInputImage1, class TInputImage2=TInputImage1, class TOutputImage=TInputImage1>
class ITK_EXPORT ScaledSubtractImageFilter :
    public
BinaryFunctorImageFilter<TInputImage1,TInputImage2,TOutputImage, 
                         Function::ScaledSub2< 
  typename TInputImage1::PixelType, 
  typename TInputImage2::PixelType,
  typename TOutputImage::PixelType>   >
{
public:
  /** Standard class typedefs. */
  typedef ScaledSubtractImageFilter                            Self;
  typedef BinaryFunctorImageFilter<
    TInputImage1,TInputImage2,TOutputImage, 
    Function::ScaledSub2< typename TInputImage1::PixelType, 
                    typename TInputImage2::PixelType,
                    typename TOutputImage::PixelType> >  Superclass;
  typedef SmartPointer<Self>                             Pointer;
  typedef SmartPointer<const Self>                       ConstPointer;
  typedef typename Superclass::FunctorType    FunctorType;
  typedef typename FunctorType::RealType      RealType;


  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(ScaledSubtractImageFilter, 
               BinaryFunctorImageFilter);

#ifdef ITK_USE_CONCEPT_CHECKING
  /** Begin concept checking */
  itkConceptMacro(Input1Input2OutputAdditiveOperatorsCheck,
    (Concept::AdditiveOperators<typename TInputImage1::PixelType,
                                typename TInputImage2::PixelType,
                                typename TOutputImage::PixelType>));
  /** End concept checking */
#endif

  void SetScale( RealType alpha ) 
    {
    this->GetFunctor().SetScale( alpha );
    this->Modified();
    }

protected:
  ScaledSubtractImageFilter() {}
  virtual ~ScaledSubtractImageFilter() {}

private:
  ScaledSubtractImageFilter(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented


};

} // end namespace itk


#endif

