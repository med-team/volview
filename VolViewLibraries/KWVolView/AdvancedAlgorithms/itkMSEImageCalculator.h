/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __itkMSEImageCalculator_h
#define __itkMSEImageCalculator_h

#include "itkObject.h"
#include "itkObjectFactory.h"

namespace itk
{

/** \class MSEImageCalculator
 * This calculator computes the MSE between two images.
 * It is templated over the image types.  
 *
 * \ingroup Operators
 */
template <class TInputImage1, class TInputImage2 = TInputImage1>
class ITK_EXPORT MSEImageCalculator : public Object 
{
public:
  /** Standard class typedefs. */
  typedef MSEImageCalculator Self;
  typedef Object                        Superclass;
  typedef SmartPointer<Self>            Pointer;
  typedef SmartPointer<const Self>      ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(MSEImageCalculator, Object);

  /** Type definition for the input image. */
  typedef TInputImage1  Image1Type;
  typedef TInputImage2  Image2Type;

  /** Pointer type for the image. */
  typedef typename TInputImage1::Pointer  Image1Pointer;
  typedef typename TInputImage2::Pointer  Image2Pointer;
  
  /** Const Pointer type for the image. */
  typedef typename TInputImage1::ConstPointer Image1ConstPointer;
  typedef typename TInputImage2::ConstPointer Image2ConstPointer;

  /** Type definition for the input image pixel type. */
  typedef typename TInputImage1::PixelType Pixel1Type;
  typedef typename TInputImage2::PixelType Pixel2Type;
  
  /** Type definition for the input image index type. */
  typedef typename TInputImage1::IndexType Index1Type;
  typedef typename TInputImage2::IndexType Index2Type;
  
  /** Type definition for the input image region type. */
  typedef typename TInputImage1::RegionType RegionType;

  typedef typename TInputImage1::PointType  PointType;
  typedef typename PointType::VectorType    VectorType;

  /** Set the input image. */
  itkSetConstObjectMacro(Image1,Image1Type);
  itkSetConstObjectMacro(Image2,Image2Type);

  /** Compute the minimum and maximum values of intensity of the input image. 
   * Returns the MSE computed */
  double Compute();

  /** Return the minimum intensity value. */
  itkGetConstMacro(MSE,double); 
  
  /** Set the region over which the values will be computed */
  void SetRegion( const RegionType & region );

  /** Set the radius. This calculator computes the MSE of the sphere within a 
   * particular radius from the center */
  itkSetMacro( Radius, double );
  itkGetMacro( Radius, double );
  itkSetMacro( Center, PointType );
  itkGetMacro( Center, PointType );

protected:
  MSEImageCalculator();
  virtual ~MSEImageCalculator() {};
  void PrintSelf(std::ostream& os, Indent indent) const;

private:
  MSEImageCalculator(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented
  
  PointType            m_Center;
  double               m_Radius;
  double               m_MSE;
  Image1ConstPointer   m_Image1;
  Image2ConstPointer   m_Image2;

  RegionType           m_Region;
  bool                 m_RegionSetByUser;
};

} // end namespace itk


#ifndef ITK_MANUAL_INSTANTIATION
#include "itkMSEImageCalculator.txx"
#endif

#endif /* __itkMSEImageCalculator_h */


