/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef __itkMSEImageCalculator_txx
#define __itkMSEImageCalculator_txx

#include "itkMSEImageCalculator.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "itkNumericTraits.h"

namespace itk
{ 
    
/**
 * Constructor
 */
template<class TInputImage1, class TInputImage2>
MSEImageCalculator<TInputImage1, TInputImage2>
::MSEImageCalculator()
{
  m_MSE = 0;
  m_RegionSetByUser = false;
  m_Radius = 100000000.0; // some Large value.
  m_Center.Fill(0);
}


/**
 * Compute Min and Max of m_Image
 */
template<class TInputImage1, class TInputImage2>
double
MSEImageCalculator<TInputImage1, TInputImage2>
::Compute()
{
  if( !m_RegionSetByUser )
    {
    m_Region = m_Image1->GetRequestedRegion();
    m_Region.Crop(m_Image2->GetRequestedRegion());
    }

  ImageRegionConstIteratorWithIndex< TInputImage1 >  it1( m_Image1, m_Region );
  ImageRegionConstIterator< TInputImage2 >  it2( m_Image2, m_Region );

  Index1Type index;
  PointType point;
  VectorType distanceVector;
  typename VectorType::RealValueType distanceSq;
  const typename VectorType::RealValueType radiusSq = m_Radius * m_Radius;

  
  it1.GoToBegin();
  it2.GoToBegin();

  m_MSE = 0;
  unsigned long nPixels = 0;

  while( !it1.IsAtEnd() )
    {

    // Check if we are within the bounds
    index = it1.GetIndex();
    m_Image1->TransformIndexToPhysicalPoint( index, point );
    distanceVector = point - m_Center;
    distanceSq = distanceVector.GetSquaredNorm();
    if (distanceSq > radiusSq) 
      {
      ++it1;
      ++it2;
      continue;
      }

    const double v1 = it1.Get();  
    const double v2 = it2.Get();  
    const double diff = v1 - v2;
    m_MSE += (diff * diff);
    ++it1;
    ++it2;
    ++nPixels;
    }

  if( nPixels == 0 )
    {
    itkExceptionMacro(<< "No pixels to evaluate MSE");
    }  

  const double n = static_cast< double >(nPixels);
  m_MSE /= n;

  return m_MSE;
}

template<class TInputImage1, class TInputImage2>
void
MSEImageCalculator<TInputImage1, TInputImage2>
::SetRegion( const RegionType & region )
{
  m_Region = region;
  m_RegionSetByUser = true;
}


 
template<class TInputImage1, class TInputImage2>
void
MSEImageCalculator<TInputImage1, TInputImage2>
::PrintSelf( std::ostream& os, Indent indent ) const
{
  Superclass::PrintSelf(os,indent);
  os << indent << "Region: " << std::endl;
  m_Region.Print(os,indent.GetNextIndent());
  os << indent << "Region set by User: " << m_RegionSetByUser << std::endl;
}

} // end namespace itk

#endif

