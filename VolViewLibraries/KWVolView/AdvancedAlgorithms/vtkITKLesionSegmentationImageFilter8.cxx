/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkITKLesionSegmentationImageFilter8.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkITKLesionSegmentationImageFilter8, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkITKLesionSegmentationImageFilter8);

//---------------------------------------------------------------------------
void vtkITKLesionSegmentationImageFilter8::SetAbortExecute( int abort )
{
  this->GetImageFilterPointer()->SetAbortGenerateData( abort );
}

