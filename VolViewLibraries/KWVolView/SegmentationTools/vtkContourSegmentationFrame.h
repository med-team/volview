/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkContourSegmentationFrame - Frame that manages the contour segmentation UI.
// .SECTION Description
// Contour segmentation interface internals. 
//
// The class maintains GUI elements and a pointer to the 
// vtkContourSegmentationFilter that will do the actual segmentation based on 
// settings of the GUI.
//
// .SECTION Parameters and Usage
// This is how the class would usually be used:
// 
// \code
// SegmentationFrame->SetRenderWidget( rwp );
// SegmentationFrame->SetContourRepresentation( rep );
// \endcode
//
// Attach the \c Segment() method to a callback. For instance for a vtkKWPushButton,
//
// \code
// button->SetCommand( SegmentationFrame, Segment );
// \endcode
//
// The EnableContourSegmentationGUI() may be invoked to enable/disable the frame.
// Enable enables all the buttons, sets reasonable defaults based on the image
// data. Disable disables the GUI elements and collapses the frame. 
// 
// .SECTION See Also
// vtkContourSegmentationFilter, vtkVVInteractorWidgetSelector

#ifndef __vtkContourSegmentationFrame_h
#define __vtkContourSegmentationFrame_h

#include "vtkKWFrameWithLabel.h"

class vtkKWEntryWithLabel;
class vtkKWMenuButton;
class vtkKWPushButton;
class vtkImageData;
class vtkContourSegmentationFilter;
class vtkContourRepresentation;
class vtkKWRenderWidgetPro;
class vtkVVWindowBase;
class vtkVVInteractorWidgetSelector;

class VTK_EXPORT vtkContourSegmentationFrame : public vtkKWFrameWithLabel
{
public:
  static vtkContourSegmentationFrame* New();
  vtkTypeRevisionMacro(vtkContourSegmentationFrame,vtkKWFrameWithLabel);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Segmentation callbacks
  virtual void SegmentationTypeCallback();
  virtual void SegmentationReplaceValueCallback( const char* );
  virtual void ContourSegmentCallback();

  // Description:
  // Enable/Disable the contour segmentation GUI. 1 enables the buttons, sets
  // them to reasonable defaults based on the data. 0 disables the buttons and
  // collapses the frame. Returns 1 if frame was enabled.
  virtual int EnableContourSegmentationGUI( int enabled );

  // Description:
  // Set the contour representation for this contour. Reference count is not 
  // incremented. Reference count should not be incremented, since the interactor
  // widget selector could delete the contour too.
  void SetContourRepresentation( vtkContourRepresentation * );

  // Description:
  // Set the render widget on which the contour is drawn. No reference count is 
  // incremented.
  void SetRenderWidget( vtkKWRenderWidgetPro * );

  // Description:
  // Set the InteractorWidgetSelector from which the currently chosen widget
  // will be queried (to get the contour widget its pointing to)
  virtual void SetInteractorWidgetSelector( vtkVVInteractorWidgetSelector* );

  // Description:
  // Do the actual segmentation. The actual contour segmentation is invoked here.
  // This is the method that should usually be attached to some callback. This 
  // does an inplace segmentation of the image data held by the render widget.
  // Returns 0 if image data was unchanged. 
  int Segment();

  // Description:
  // Contour segmentation is an inplace operation. All render widgets using the 
  // selected data item must be updated.
  static void UpdateRenderWidgetsUsingSelectedDataItem( vtkVVWindowBase * );

  // Description:
  // Check the widget type to see if contours drawn on this widget support
  // segmentation. If you want to disable segmentation on the image / volume / 
  // oblique widget etc, you would do that here.
  static int RenderWidgetSupportSegmentation( vtkKWRenderWidgetPro *rwp );

  // Description:
  // Prompt before segmentation. Default yes.
  vtkSetMacro(PromptBeforeSegmentation, int);
  vtkGetMacro(PromptBeforeSegmentation, int);
  vtkBooleanMacro(PromptBeforeSegmentation, int);
  
  // Description:
  // Some constants
  //BTX
  static const char *SegmentationWarningDialogName;
  //ETX
  
protected:
  vtkContourSegmentationFrame();
  ~vtkContourSegmentationFrame();

  // Description:
  // Create the interface objects.
  virtual void CreateWidget();

  // Description:
  // Given the contour drawn on this->RenderWidget, this method adds the list 
  // of supported segmentation options. For instance, contours on the image
  // widgets can also support slice by slice segmentation in addition to 
  // extrusion based segmentation
  void EnableSupportedSegmentationOptions();

  // Description:
  // Display a warning dialog that segmentation is an inplace operation.
  // Return 1 if the user wants to proceed, 0 otherwise.
  virtual int DisplayWarningDialog();

  // GUI Items
  
  // Four GUI elements
  //   - Replace value for segmentation
  //   - Should replace Inside or outside the contour ?
  //   - Should segment the whole volume via an extrusion or just the slice ?
  //   - The button that does the segmentation
  vtkKWEntryWithLabel            *SegmentationReplaceValueEntry;
  vtkKWMenuButton                *SegmentationTypeMenuButton1;
  vtkKWMenuButton                *SegmentationTypeMenuButton2;
  vtkKWPushButton                *SegmentButton;
  
  vtkVVInteractorWidgetSelector  *InteractorWidgetSelector;
  vtkKWRenderWidgetPro           *RenderWidget;
  vtkContourSegmentationFilter   *ContourSegmentationFilter;
  int                            HasValidImageData;
  int                            HasValidPolyData;
  int                            PromptBeforeSegmentation;

private:
  vtkContourSegmentationFrame(const vtkContourSegmentationFrame&); // Not implemented
  void operator=(const vtkContourSegmentationFrame&); // Not Implemented
};

#endif

