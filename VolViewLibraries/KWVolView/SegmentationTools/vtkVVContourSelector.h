/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkVVContourSelector - a measurement widget selector.
// .SECTION Description
// This class is a widget that can be used to store measurement widget.
// .SECTION See Also
// vtkKWPresetSelector

#ifndef __vtkVVContourSelector_h
#define __vtkVVContourSelector_h

#include "vtkKWPresetSelector.h"

class vtkVVDataItemVolume;
class vtkVVDataItemVolumeContour;
class vtkVVContourSelectorInternals;

class VTK_EXPORT vtkVVContourSelector : public vtkKWPresetSelector
{
public:
  static vtkVVContourSelector* New();
  vtkTypeRevisionMacro(vtkVVContourSelector, vtkKWPresetSelector);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the contour associated to the preset in the pool.
  // Return 1 on success, 0 on error
  virtual int SetPresetContour(int id, vtkVVDataItemVolumeContour *contour);
  virtual vtkVVDataItemVolumeContour* GetPresetContour(int id);

  // Description:
  // Get the currently selected contour
  virtual vtkVVDataItemVolumeContour* GetCurrentlySelectedContour();

  // Description:
  // Query if the pool has a given contour in a group
  virtual int HasPresetWithGroupWithContour(
    const char *group, vtkVVDataItemVolumeContour *contour);

  // Description:
  // Get the ID of an contour if present. Returns -1 if absent.
  int GetIdOfContour( vtkVVDataItemVolumeContour *contour );

  // Description:
  // Callback invoked when the user successfully updated the preset field
  // located at ('row', 'col') with the new contents 'text', as a result
  // of editing the corresponding cell interactively.
  virtual void PresetCellUpdatedCallback(int row, int col, const char *text);

  // Description:
  // Callbacks
  virtual void PresetSelectionChangedCallback();
  
  // Description:
  // Update the "enable" state of the object and its internal parts.
  // Depending on different Ivars (this->Enabled, the application's 
  // Limited Edition Mode, etc.), the "enable" state of the object is updated
  // and propagated to its internal parts/subwidgets. This will, for example,
  // enable/disable parts of the widget UI, enable/disable the visibility
  // of 3D widgets, etc.
  virtual void UpdateEnableState();

  // Description:
  // See superclass for doc.
  virtual int RemovePreset(int id);

  // Description:
  // Populate new contours that are displayed on this data item
  virtual void PopulatePresets( vtkVVDataItemVolume * );

protected:
  vtkVVContourSelector();
  ~vtkVVContourSelector();

  // Description:
  // Create the widget.
  virtual void CreateWidget();
  
  // Description:
  // Create the columns.
  // Subclasses should override this method to add their own columns and
  // display their own preset fields (do not forget to call the superclass
  // first).
  virtual void CreateColumns();

  // Description:
  // Set the toolbar preset buttons balloon help strings
  // Subclass can override this method to change the help strings
  // associated to the buttons.
  virtual void SetToolbarPresetButtonsHelpStrings(vtkKWToolbar*);

  // Description:
  // Deallocate a preset.
  // Subclasses should override this method to release the memory allocated
  // by their own preset fields  (do not forget to call the superclass
  // first).
  virtual void DeAllocatePreset(int id);

  // Description:
  // Update the preset row, i.e. add a row for that preset if it is not
  // displayed already, hide it if it does not match GroupFilter, and
  // update the table columns with the corresponding preset fields.
  // Subclass should override this method to display their own fields.
  // Return 1 on success, 0 if the row was not (or can not be) updated.
  // Subclasses should call the parent's UpdatePresetRow, and abort
  // if the result is not 1.
  virtual int UpdatePresetRow(int id);

  // Description:
  // Update the value column only in the preset row
  // User for fast refresh during 3D widget interaction
  virtual int UpdatePresetRowValueColumn(int id);

  // Description:
  // Convenience methods to get the index of a given column
  virtual int GetContourValueColumnIndex();
  virtual int GetVisibilityColumnIndex();
  virtual int GetColorColumnIndex();

  // Description:
  // Processes the events that are passed through CallbackCommand (or others).
  // Subclasses can oberride this method to process their own events, but
  // should call the superclass too.
  virtual void ProcessCallbackCommandEvents(
    vtkObject *caller, unsigned long event, void *calldata);

  // PIMPL Encapsulation for STL containers
  //BTX
  vtkVVContourSelectorInternals *Internals;
  //ETX

  // Description:
  // Update the contour details area.
  virtual void UpdateContourDetails( int id );
  virtual void UpdateSelectedContourDetails();

  vtkKWMultiColumnListWithScrollbars *ContourDetails;

private:

  vtkVVContourSelector(const vtkVVContourSelector&); // Not implemented
  void operator=(const vtkVVContourSelector&); // Not implemented
};

#endif
