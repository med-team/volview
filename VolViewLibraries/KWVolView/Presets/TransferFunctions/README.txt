Here is how to add a transfer function preset to the VolView distribution.

- Launch VolView.
- Open some data.
- Switch to the "Display" tab in the left panel.
- Play with the "Volume Appearance Settings" until you are satisfied.
- Click on the "Add" button in the "Volume Appearance Presets" UI to save the
  current appearance settings as a preset. The preset group is automatically
  assigned the modality of the data, if any, and the preset is saved on disk.
- Double-click in the "Comment" cell and enter a comment or a short
  description for that preset, if any.
- Exit VolView.
- Locate the preset in your "My Documents" folder, under the 
  "VolView3.0\Presets\TransferFunctions" subdirectory. The preset filename was
  generated using the preset timestamp (date and time). 
  For example: "C:\Documents and Settings\barre\My Documents\VolView3.0\Presets\TransferFunctions\2005-09-29 11-18-59.vvt".
- Move the preset to the "GUI\VolView3\Presets\TransferFunctions" folder in the
  source directory and rename it to something more meaningful (keep the .vvt 
  extension). For example: "CT - Bone.vvt".
- Commit the file to the CVS repository.

Once the preset is in the source tree, CMake will copy it automatically 
to the build directory, under the "share/Presets/TransferFunctions" folder. 

When VolView is launched, it looks up and merge all presets in both the
user directory in "My Documents" and the "share/Presets/TransferFunctions" in
the build tree. Removing a preset from the UI will *delete* the corresponding
file in the folder it was found (i.e., the presets in the source tree
are never deleted).

