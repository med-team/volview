##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
GET_FILENAME_COMPONENT(DIR_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
FILE(GLOB FILES "${CMAKE_CURRENT_SOURCE_DIR}/*.vvt")
INSTALL_FILES("${KWVolView_INSTALL_DATA_DIR}/Presets/${DIR_NAME}" FILES ${FILES})
