/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/* 
 * Resource generated for file:
 *    VVLayout1Lightbox.png (zlib, base64) (image file)
 */
static const unsigned int  image_VVLayout1Lightbox_width          = 19;
static const unsigned int  image_VVLayout1Lightbox_height         = 19;
static const unsigned int  image_VVLayout1Lightbox_pixel_size     = 3;
static const unsigned long image_VVLayout1Lightbox_length         = 52;
static const unsigned long image_VVLayout1Lightbox_decoded_length = 1083;

static const unsigned char image_VVLayout1Lightbox[] = 
  "eNpjYCAf/H93EhkRKYIpSAwacjZCxEkiR0N1NFQHKlTJAACqsa39";

/* 
 * Resource generated for file:
 *    VVLayout1Stack2D.png (zlib, base64) (image file)
 */
static const unsigned int  image_VVLayout1Stack2D_width          = 19;
static const unsigned int  image_VVLayout1Stack2D_height         = 19;
static const unsigned int  image_VVLayout1Stack2D_pixel_size     = 3;
static const unsigned long image_VVLayout1Stack2D_length         = 104;
static const unsigned long image_VVLayout1Stack2D_decoded_length = 1083;

static const unsigned char image_VVLayout1Stack2D[] = 
  "eNpjYCAf/H93klSESyNBi/BofP/qIVaESyN+u/BrxGMXVo3E2IVLI0G7RoJGrAmAyIjATH"
  "L4NaLFOPEa8SRyknIQ1bMVkRrJAwA6fT2Z";

/* 
 * Resource generated for file:
 *    VVLayout1Volume3D.png (zlib, base64) (image file)
 */
static const unsigned int  image_VVLayout1Volume3D_width          = 19;
static const unsigned int  image_VVLayout1Volume3D_height         = 19;
static const unsigned int  image_VVLayout1Volume3D_pixel_size     = 3;
static const unsigned long image_VVLayout1Volume3D_length         = 108;
static const unsigned long image_VVLayout1Volume3D_decoded_length = 1083;

static const unsigned char image_VVLayout1Volume3D[] = 
  "eNpjYCAf/H93klSESyNBi7BqBIq8f/UQK8KjEb8uCImpkaAurBrx+wvZBEyNBK0b1UgwLk"
  "jViBbjRGrETGBEJgAicwfZ2YpIjeQBAOm0Y9U=";

