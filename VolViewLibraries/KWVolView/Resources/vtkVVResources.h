/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/* 
 * Resource generated for file:
 *    VVLung.png (zlib, base64) (image file)
 */
static const unsigned int  image_VVLung_width          = 22;
static const unsigned int  image_VVLung_height         = 22;
static const unsigned int  image_VVLung_pixel_size     = 4;
static const unsigned long image_VVLung_length         = 1756;
static const unsigned long image_VVLung_decoded_length = 1936;

static const unsigned char image_VVLung[] = 
  "eNqNlWtQlFUYx09T5qiDsCwLCwsoSkBesCLJW1+cakTzgw4yOlONozXVmMiy9xu77P3Org"
  "vLLisCy4AmJKSiCJJ3kAw0EkHwgtpUZmo2XcYvPp3n0JKWOX34z7yz55zf83+e/3nfDeQ2"
  "kMATdKriLKd2fZOtNNXzk3dpzedDB0ZJX3BoqZCv/l6Vab/Yom/Lq1gVJt/Uj5DRA2P/Uu"
  "DJ3Niy3B1HdKke0KaUgTWzEgb3jUyrfK2xRcg1gjjJBJocO7jzQh/+F/fGkVuPKbAmTFxz"
  "qw7qUlxQInCBjnKNs33QHx4s8C7ceUocRblcEwjTdVCUo4bjvt7V/4frXxUW6WeWgVbgBF"
  "1qGZTO8IBxlg+6378I7uwakESbQB5nBVmSBQrTNGBZ4rs91Ho57mnc0bYbybTnu2q+g3kt"
  "SR73a0r3w6GCM+CcVw3iaDMoeFbAPbIUK2jS3VD+Zl11784+8qj6Px2YUO3bTWH0gmfUiU"
  "4qB2gEbiid6YO2/NPgmLsDpBwLKOIdoEywg5rOSpfmAefLITgZ6Ml+Ere3/qsUS4b/Ic5P"
  "wbOBMp4qwQEqvpOxj757DuxzQsyvPG58DetirtoUN/iW1+wZ6b9MIup1X2Dy59d50Ksk2g"
  "Iyek7OszNfKr6Lsc9vHWNcURTON7JG+0mi69S7ea7/9wtnLgoe5fY4B6brX/H+iJlgn9JY"
  "K2MrKFtJmZIYC1wx3bpufTH4q3i6+bG6ygQnE/a4a8O+0L3Rnwmq03iKtCoO5ykS7SxrCe"
  "WiZFwbE/oVTjHAQOvwIuu8QLuE5WZnXHhQwJ6RL40xgWtB1Y07l+5ORu7tM3eIc0lVV9Ek"
  "LZudlGNl/pCN/aJfxm0c5muS3YcUHBuo412MhVzcg/WxR8x6sGXo1etdN8kP3benKvj2Xz"
  "4hJWx2yES2lHFtjIuehvdfnWx4wdeo4Iz7RCEv8oxcrB9e/5mtZUsH6VCfWL6FqGDrM1rG"
  "fdSv7K/cdTO2P7x04BoJr9lbgv50qV5WL8LD/djrtkmlYJ7jH7p+/FtyUH5sy8dECYXP6Q"
  "AzmeDG/J2fLbPq/glzH+lU9mxUJbpAn+YDNb0HyI3sFUeZofBZHdZ7cKVrLDr4VsPurUQD"
  "RZP1E5nhDCa4tFdfTv3A1e6bZLBzZL5GUAaGWeV0li5WE9fxDPotnmpkPe9d17FYn1XeXz"
  "xFD8XTjIwXuQcyypTH0hlzLVD9RtP+367+Qe4N34+1ZAbod6gC0Dfeicgc8N4jQxxlAJpv"
  "gVrg+K44xsDuGO7B+WFezAvl4r0Mrmpw17y3h1BNci+ovmNI2878Yp64L8JGrjC2FOo+at"
  "pczNU/EHMol0vfT64VlPgOU8kSrOw3/AaENzRvrHunGbkkuGz3Wf1sDyiTHOPvOxX2hP0x"
  "fwkmaDUc2iSfablSxCtlDFk89cu3MSn4dL4CE1jm+Ua/dA9y+rzDpFN3mrSKOj+wLqp4KE"
  "2j/fHN7HuJ56RxJhDxDCBKonfipfJNzkWVBwt5GhDF0Nzwv4BnBEmSEUqybGDM3r5v/7Yv"
  "ZiEzwmVs7eEC28LKa+J0PYhS9YwVkSTZAGU51Ss7DMcLVFk0zxRaL9kIynQTOHL953yraz"
  "c7s0OkXXyS/JPb6TpJjnl6Z3uWV/vkGab70gwDFKdpQZphBE2Wbax7R3/UwK7h54PrGgy2"
  "3IquYF5juHJteEW76mhiML+BPI3bU3me1K5tJqaF3vnNwjah6/XQEeviinbvytCy3pqvyZ"
  "+DDBGW";

/* 
 * Resource generated for file:
 *    CopyLabelsToNextSlice.png (zlib, base64) (image file)
 */
static const unsigned int  image_CopyLabelsToNextSlice_width          = 16;
static const unsigned int  image_CopyLabelsToNextSlice_height         = 16;
static const unsigned int  image_CopyLabelsToNextSlice_pixel_size     = 4;
static const unsigned long image_CopyLabelsToNextSlice_length         = 80;
static const unsigned long image_CopyLabelsToNextSlice_decoded_length = 1024;

static const unsigned char image_CopyLabelsToNextSlice[] = 
  "eNr7//8/w38qYSD4TwT+gVd/AxTjYdNUPxH4PxXDDH94/v9PiV4YpkQvqWZg00uuO0bTH+"
  "3THwByo1nB";

/* 
 * Resource generated for file:
 *    CopyLabelsToPreviousSlice.png (zlib, base64) (image file)
 */
static const unsigned int  image_CopyLabelsToPreviousSlice_width          = 16;
static const unsigned int  image_CopyLabelsToPreviousSlice_height         = 16;
static const unsigned int  image_CopyLabelsToPreviousSlice_pixel_size     = 4;
static const unsigned long image_CopyLabelsToPreviousSlice_length         = 76;
static const unsigned long image_CopyLabelsToPreviousSlice_decoded_length = 1024;

static const unsigned char image_CopyLabelsToPreviousSlice[] = 
  "eNr7//8/w38qYSD4TwzGq78BivGwaaqfMP7xn4phhukGEIEdU2IGJe6gxC+0DKvR9Ec5Bg"
  "CB2FnB";

