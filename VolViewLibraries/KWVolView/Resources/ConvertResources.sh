KWConvertImageToHeader --zlib --base64 KWVolViewDefaultSplashScreen.h \
    KWVolViewDefaultSplashScreen.png

KWConvertImageToHeader --zlib --base64 vtkVVWindowLayout.h \
    VVLayout1Lightbox.png \
    VVLayout1Stack2D.png \
    VVLayout1Volume3D.png

KWConvertImageToHeader --zlib --base64 vtkVVResources.h \
    VVLung.png \
    CopyLabelsToNextSlice.png \
    CopyLabelsToPreviousSlice.png

