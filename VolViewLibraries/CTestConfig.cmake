##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
set(CTEST_PROJECT_NAME "VolView")
set(CTEST_NIGHTLY_START_TIME "09:00:00 EST")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "www.kitware.com")
set(CTEST_DROP_LOCATION "/CDash/submit.php?project=VolView")
set(CTEST_DROP_SITE_CDASH TRUE)
