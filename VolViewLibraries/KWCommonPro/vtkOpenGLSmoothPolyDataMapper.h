/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkOpenGLSmoothPolyDataMapper - smooth polydata mapper
// .SECTION Description
// A simple subclass that turns on smoothing

#ifndef __vtkOpenGLSmoothPolyDataMapper_h
#define __vtkOpenGLSmoothPolyDataMapper_h

#include "vtkOpenGLPolyDataMapper.h"

class VTK_EXPORT vtkOpenGLSmoothPolyDataMapper : public vtkOpenGLPolyDataMapper
{
public:
  static vtkOpenGLSmoothPolyDataMapper *New();
  vtkTypeRevisionMacro(vtkOpenGLSmoothPolyDataMapper,vtkOpenGLPolyDataMapper);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  virtual void Render(vtkRenderer *ren, vtkActor *a);

protected:
  vtkOpenGLSmoothPolyDataMapper();
  ~vtkOpenGLSmoothPolyDataMapper();
  
private:
  vtkOpenGLSmoothPolyDataMapper(const vtkOpenGLSmoothPolyDataMapper&);  // Not implemented.
  void operator=(const vtkOpenGLSmoothPolyDataMapper&);  // Not implemented.
};

#endif
