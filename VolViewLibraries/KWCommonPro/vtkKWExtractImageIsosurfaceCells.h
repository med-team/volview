/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWExtractImageIsosurfaceCells
// .SECTION Description

#ifndef __vtkKWExtractImageIsosurfaceCells_h
#define __vtkKWExtractImageIsosurfaceCells_h

#include "vtkStructuredPointsToUnstructuredGridFilter.h"

class vtkImageData;
class vtkDataObject;

class VTK_EXPORT vtkKWExtractImageIsosurfaceCells : public vtkStructuredPointsToUnstructuredGridFilter
{
public:
  static vtkKWExtractImageIsosurfaceCells *New();
  vtkTypeRevisionMacro(vtkKWExtractImageIsosurfaceCells, vtkStructuredPointsToUnstructuredGridFilter);
  void PrintSelf(ostream& os, vtkIndent indent);

  vtkSetVector3Macro( StartingCell, int );
  
  vtkSetMacro( Isovalue, float );

  vtkSetMacro( ArrayComponent, int);
  vtkGetMacro( ArrayComponent, int);
  
protected:
  vtkKWExtractImageIsosurfaceCells();
  ~vtkKWExtractImageIsosurfaceCells();

  virtual void ExecuteData(vtkDataObject *output);

  int   StartingCell[3];
  float Isovalue;
  int ArrayComponent;
  
private:
  vtkKWExtractImageIsosurfaceCells(const vtkKWExtractImageIsosurfaceCells&);  // Not implemented.
  void operator=(const vtkKWExtractImageIsosurfaceCells&);  // Not implemented.
};

#endif

