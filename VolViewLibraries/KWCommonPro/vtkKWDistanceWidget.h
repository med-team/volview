/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWDistanceWidget
// .SECTION Description

#ifndef __vtkKWDistanceWidget_h
#define __vtkKWDistanceWidget_h

#include "vtkDistanceWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class VTK_EXPORT vtkKWDistanceWidget : public vtkDistanceWidget
{
public:
  static vtkKWDistanceWidget* New();
  vtkTypeRevisionMacro(vtkKWDistanceWidget, vtkDistanceWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

protected:
  vtkKWDistanceWidget() {};
  ~vtkKWDistanceWidget() {};
  
private:
  vtkKWDistanceWidget(const vtkKWDistanceWidget&);  // Not implemented
  void operator=(const vtkKWDistanceWidget&);  // Not implemented
};

#endif
