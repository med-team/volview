##=========================================================================
## 
##   Copyright (c) Kitware, Inc.
##   All rights reserved.
##   See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.
## 
##      This software is distributed WITHOUT ANY WARRANTY; without even
##      the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##      PURPOSE.  See the above copyright notice for more information.
## 
##=========================================================================
#
# This module is provided as KWCommonPro_USE_FILE by KWCommonProConfig.cmake.
# It can be INCLUDEd in a project to load the needed compiler and linker
# settings to use KWCommonPro:
#   FIND_PACKAGE(KWCommonPro REQUIRED)
#   INCLUDE(${KWCommonPro_USE_FILE})

IF(NOT KWCommonPro_USE_FILE_INCLUDED)
  SET(KWCommonPro_USE_FILE_INCLUDED 1)

  # Load the compiler settings used for KWCommonPro.
  IF(KWCommonPro_BUILD_SETTINGS_FILE)
    INCLUDE(${CMAKE_ROOT}/Modules/CMakeImportBuildSettings.cmake)
    CMAKE_IMPORT_BUILD_SETTINGS(${KWCommonPro_BUILD_SETTINGS_FILE})
  ENDIF(KWCommonPro_BUILD_SETTINGS_FILE)

  # Add compiler flags needed to use KWCommonPro.
  SET(CMAKE_C_FLAGS
    "${CMAKE_C_FLAGS} ${KWCommonPro_REQUIRED_C_FLAGS}")
  SET(CMAKE_CXX_FLAGS
    "${CMAKE_CXX_FLAGS} ${KWCommonPro_REQUIRED_CXX_FLAGS}")
  SET(CMAKE_EXE_LINKER_FLAGS
    "${CMAKE_EXE_LINKER_FLAGS} ${KWCommonPro_REQUIRED_EXE_LINKER_FLAGS}")
  SET(CMAKE_SHARED_LINKER_FLAGS
    "${CMAKE_SHARED_LINKER_FLAGS} ${KWCommonPro_REQUIRED_SHARED_LINKER_FLAGS}")
  SET(CMAKE_MODULE_LINKER_FLAGS
    "${CMAKE_MODULE_LINKER_FLAGS} ${KWCommonPro_REQUIRED_MODULE_LINKER_FLAGS}")

  # Add include directories needed to use KWCommonPro.
  INCLUDE_DIRECTORIES(${KWCommonPro_INCLUDE_DIRS})

  # Add link directories needed to use KWCommonPro.
  LINK_DIRECTORIES(${KWCommonPro_LIBRARY_DIRS})

  # Add cmake module path.
  SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${KWCommonPro_CMAKE_DIR}")

  # Use KWCommon.
  IF(NOT KWCommonPro_NO_USE_KWCommon)
    SET(KWCommon_DIR ${KWCommonPro_KWCommon_DIR})
    FIND_PACKAGE(KWCommon)
    IF(KWCommon_FOUND)
      INCLUDE(${KWCommon_USE_FILE})
    ELSE(KWCommon_FOUND)
      MESSAGE("KWCommon not found in KWCommonPro_KWCommon_DIR=\"${KWCommonPro_KWCommon_DIR}\".")
    ENDIF(KWCommon_FOUND)
  ENDIF(NOT KWCommonPro_NO_USE_KWCommon)

  # Use CTNLIB.
  IF(KWCommonPro_USE_CTNLIB)
    SET(CTNLIB_DIR ${KWCommonPro_CTNLIB_DIR})
    FIND_PACKAGE(CTNLIB)
    IF(CTNLIB_FOUND)
      INCLUDE(${CTNLIB_USE_FILE})
    ELSE(CTNLIB_FOUND)
      MESSAGE("CTNLIB not found in KWCommonPro_CTNLIB_DIR=\"${KWCommonPro_CTNLIB_DIR}\".")
    ENDIF(CTNLIB_FOUND)
  ENDIF(KWCommonPro_USE_CTNLIB)

  # Use GDCM.
  IF(KWCommonPro_USE_GDCM)
    SET(GDCM_DIR ${KWCommonPro_GDCM_DIR})
    FIND_PACKAGE(GDCM)
    IF(GDCM_FOUND)
      INCLUDE(${GDCM_USE_FILE})
    ELSE(GDCM_FOUND)
      MESSAGE("GDCM not found in KWCommonPro_GDCM_DIR=\"${KWCommonPro_GDCM_DIR}\".")
    ENDIF(GDCM_FOUND)
  ENDIF(KWCommonPro_USE_GDCM)

ENDIF(NOT KWCommonPro_USE_FILE_INCLUDED)
