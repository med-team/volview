/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkScalarsPassThroughFilter - pass point scalars component by component.
// .SECTION Description
// vtkScalarsPassThroughFilter is a filter object that passes only those
// point scalars components that have been allowed to go through.

#ifndef __vtkScalarsPassThroughFilter_h
#define __vtkScalarsPassThroughFilter_h

#include "vtkDataSetToDataSetFilter.h"

class VTK_EXPORT vtkScalarsPassThroughFilter : public vtkDataSetToDataSetFilter
{
public:
  // Description:
  // Create instance with all components allowed.
  static vtkScalarsPassThroughFilter *New();
  vtkTypeRevisionMacro(vtkScalarsPassThroughFilter,vtkDataSetToDataSetFilter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Allow a specific point scalar component to pass through
  virtual void SetOutputPointScalarComponent(int comp, int value);
  virtual void OutputPointScalarComponentOn(int comp)
    { this->SetOutputPointScalarComponent(comp, 1); };
  virtual void OutputPointScalarComponentOff(int comp)
    { this->SetOutputPointScalarComponent(comp, 0); };
  virtual int GetOutputPointScalarComponent(int comp);

protected:
  vtkScalarsPassThroughFilter();
  ~vtkScalarsPassThroughFilter() {};

  int OutputPointScalarComponent[VTK_MAX_VRCOMP];

  void Execute();

private:
  vtkScalarsPassThroughFilter(const vtkScalarsPassThroughFilter&);  // Not implemented.
  void operator=(const vtkScalarsPassThroughFilter&);  // Not implemented.
};

#endif


