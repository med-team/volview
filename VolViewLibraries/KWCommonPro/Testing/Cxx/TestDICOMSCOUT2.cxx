/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMReader.h"
#include "vtkDICOMCollector.h"
#include "vtkDICOMCollectorOptions.h"
#include "vtkExecutive.h"
#include "vtkGlobFileNames.h"
#include "vtkImageData.h"

#include "vtkKWCommonProBuildConfigure.h" // for VolView_DATA_ROOT

int TestReadSCOUT2(const char *filename, unsigned int numvolumes)
{
  // Need to use a glob, because of the warning file: DO NOT USE...
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  globFileNames->AddFileNames( filename );

  vtkDICOMReader *reader = vtkDICOMReader::New();
  reader->SetFileNames( globFileNames->GetFileNames() );
  reader->GetDICOMCollector()->GetOptions()->ExploreDirectoryOff();  //important
  reader->GetDICOMCollector()->GetOptions()->SkipProblematicFileOff();
  //reader->Update(); // we loose the return value
  vtkExecutive *exe = reader->GetExecutive();
  int r = exe->Update();
  //reader->GetOutput()->Print( cerr );
  if( !r )
    {
    cerr << "Failed to load: " << filename << endl;
    return 1;
    }
  if( numvolumes != reader->GetNumberOfOutputPorts() )
    {
    cerr << "Wrong number of volumes: " << reader->GetNumberOfOutputPorts() <<
      " instead of: " << numvolumes << endl;
    return 1;
    }

  globFileNames->Delete();
  reader->Delete();
  return 0;
}

int main(int argc, char *argv[])
{
  int r = 0;

  // Series is InstanceNumber: 2 4 6 8 ...
  // aka crash3:
  r += TestReadSCOUT2( VolView_DATA_ROOT "/Data/SCOUT/Skip2/*.dcm", 1);

  // A SCOUT with one slice (2) and then another orientation: 2-3
  // aka case1
  r += TestReadSCOUT2( VolView_DATA_ROOT "/Data/SCOUT/SCOUT-223/*.dcm", 3 );

  // A Single Series with a sagittal slice that also uses InstanceNumber=2
  // aka case2:
  r += TestReadSCOUT2( VolView_DATA_ROOT "/Data/SCOUT/Series-duplicate-2/*.dcm", 2 );

  // A regular 3-SCOUT acquisition except it's missing slice #2 from the coronal 
  // aka case3
  r += TestReadSCOUT2( VolView_DATA_ROOT "/Data/SCOUT/SCOUT-Missing-2/*.dcm", 4 );

  return r;
}

