/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMReader.h"
#include "vtkDICOMCollector.h"
#include "vtkDICOMCollectorOptions.h"
#include "vtkGlobFileNames.h"
#include "vtkImageData.h"

#include "vtkKWCommonProBuildConfigure.h" // for VolView_DATA_ROOT

#include <vtksys/SystemTools.hxx>


int TestDICOMRead(const char *filename, bool inverted = false)
{
  // Let the vtkDICOMCollector do it's job:
  vtkDICOMReader* reader1 = vtkDICOMReader::New();
  reader1->SetFileName( filename );
  reader1->Update();

  // Ok we know the correct answer:
  // Let's contruct a glob expression.
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0001.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0002.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0003.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0004.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0005.*
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  std::string path = vtksys::SystemTools::GetFilenamePath( filename );
  std::string fn = path + "/" + 
    vtksys::SystemTools::GetFilenameWithoutLastExtension( filename ) + ".*";
  //cerr << fn << endl;
  globFileNames->AddFileNames( fn.c_str() );

  vtkDICOMReader* reader2 = vtkDICOMReader::New();
  reader2->SetFileNames( globFileNames->GetFileNames() );
  reader2->GetDICOMCollector()->GetOptions()->ExploreDirectoryOff();
  reader2->Update();

  // Let's verify a couple of things:
  if ( reader1->GetNumberOfOutputPorts() != 1
    || reader2->GetNumberOfOutputPorts() != 1 )
    {
    cerr << "Wrong Number Of Ouput Ports" << endl;
    return 1;
    }
  vtkImageData *img1 = reader1->GetOutput();
  vtkImageData *img2 = reader2->GetOutput();
  if ( img1->GetNumberOfPoints() != img2->GetNumberOfPoints() )
    {
    cerr << "Wrong Number Of Points: " << img1->GetNumberOfPoints() <<
      " vs " << img2->GetNumberOfPoints() << endl;
    return 1;
    }
  double *range1 = img1->GetScalarRange();
  double *range2 = img2->GetScalarRange();
  if( range1[0] != range2[0] || range1[1] != range2[1] )
    {
    cerr << "Wrong range" << endl;
    return 1;
    }

  // Final tests, the list should be ordered in the ascending order, this works
  // nicely for a couple of dataset where filename also indicate order (very rare !)
  if( reader1->GetDICOMCollector()->GetNumberOfCollectedSlices() != 
      reader2->GetDICOMCollector()->GetNumberOfCollectedSlices() )
    {
    cerr << "Wrong Number of Collected Slices" << endl;
    return 1;
    }
  int n = reader1->GetDICOMCollector()->GetNumberOfCollectedSlices();
  for(int i = 0; i < n; ++i)
    {
    const char *fn1 = reader1->GetDICOMCollector()->GetSliceFileName(i);
    const char *fn2 = reader2->GetDICOMCollector()->GetSliceFileName(i);
    // filename should be identical
    if ( strcmp(fn1, fn2) != 0 )
      {
      cerr << "Wrong filenames: " << fn1 << " should be: " << fn2 << endl;
      return 1;
      }
    //cerr << fn << endl;
    vtksys_stl::string lastext = vtksys::SystemTools::GetFilenameLastExtension( fn1 );
    const char *ext = lastext.c_str();
    // The extension is increasing from 1 to n or n to 1
    int comp = inverted ? (n-i) : (i+1);
    
    if( atoi( ext+1 ) != comp )
      {
      cerr << "ext: " << ext << " instead of " << comp << endl;
      return 1;
      }
    }

  globFileNames->Delete();
  reader1->Delete();
  reader2->Delete();

  return 0;
}

// Let's see if we handle separating the series:
int TestDICOMMultiOutput(const char *filename)
{
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  globFileNames->AddFileNames( filename );

  vtkDICOMReader* reader = vtkDICOMReader::New();
  reader->SetFileNames( globFileNames->GetFileNames() );
  reader->GetDICOMCollector()->GetOptions()->ExploreDirectoryOff();
  reader->Update();

  cerr << reader->GetNumberOfOutputPorts() << endl;

  reader->Delete();
  globFileNames->Delete();

  return 0;
}

