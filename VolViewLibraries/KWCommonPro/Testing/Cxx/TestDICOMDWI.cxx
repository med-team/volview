/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMReader.h"
#include "vtkDICOMCollector.h"
#include "vtkDICOMCollectorOptions.h"
#include "vtkExecutive.h"
#include "vtkGlobFileNames.h"

#include "vtkKWCommonProBuildConfigure.h" // for VolView_DATA_ROOT

int TestReadDWI(const char *filename, bool expected_result, unsigned int numvolumes)
{
  // Need to use a glob, because of the warning file: DO NOT USE...
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  globFileNames->AddFileNames( filename );

  vtkDICOMReader *reader = vtkDICOMReader::New();
  reader->SetFileNames( globFileNames->GetFileNames() );
  reader->GetDICOMCollector()->GetOptions()->ExploreDirectoryOn();
  //reader->Update(); // we loose the return value
  vtkExecutive *exe = reader->GetExecutive();
  int r = exe->Update();
  bool res = (r ? true : false);
  if( res != expected_result ) // we do not respect the expected result
    {
    cerr << "Inconsistant result for: " << filename << endl;
    return 1;
    }

  // Get the failure status:
  vtkDICOMCollector::FailureStatusTypes type = (vtkDICOMCollector::FailureStatusTypes)
    reader->GetDICOMCollector()->GetFailureStatus();
  if( !expected_result ) // if we were not expected to succeed make sure we are
                         // reporting the error correctly
    {
    if( !(type & vtkDICOMCollector::FailureUnknownSeriesType) )
      {
      cerr << "No Failure was reported" << endl;
      return 1;
      }
    }
  else // we were supposed to succeed we should not report any error
    {
    if( type != vtkDICOMCollector::FailureNone )
      {
      cerr << "Failure was reported:" << type << endl;
      return 1;
      }
    if( numvolumes != (unsigned int)(reader->GetNumberOfOutputPorts()) )
      {
      cerr << "Wrong number of volumes found" << endl;
      return 1;
      }
    }
  //cerr << r << endl;
  //cerr << reader->GetDICOMCollector()->GetFailureStatus() << endl;

  globFileNames->Delete();
  reader->Delete();
  return 0;
}

int main(int argc, char *argv[])
{
  int r = 0;
  // Famous DWI from Siemens aka Mosaic. To make matter worse, Beacon remove
  // the private field that would allow to decompose the image into frames...
  // So there is no way we can ever read this.
  r += TestReadDWI( VolView_DATA_ROOT "/Data/DWI/2177/91*.dcm", false, 0);

  // We do support the Philips DTI/DWI:
  r += TestReadDWI( VolView_DATA_ROOT "/Data/DWI/SERIES4/IMAGE*", true, 8);

  // We do support the GEMS DTI/DWI:
  r += TestReadDWI( VolView_DATA_ROOT "/Data/DWI/SERIES5/IMAGE*", true, 7);

  return r;
}

