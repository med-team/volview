/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkGPXReader.h"
#include "vtkXMLPolyDataWriter.h"
#include "vtkPolyDataWriter.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"

int main(int argc, char* argv[])
{
  if ( argc < 3 )
    {
    cout << "Usage: " << argv[0] << " <infile> <outfile>" << endl;
    return 1;
    }
  vtkGPXReader* reader = vtkGPXReader::New();
  vtkXMLPolyDataWriter* writer = vtkXMLPolyDataWriter::New();
  //vtkPolyDataWriter* writer = vtkPolyDataWriter::New();
  writer->SetDataModeToAscii();
  reader->SetFileName(argv[1]);
  int cc;
  for ( cc = 0; cc < reader->GetNumberOfOutputs(); ++ cc )
    {
    char buffer[1024];
    sprintf(buffer, "%s_%d.vtp", argv[2], cc);
    writer->SetInput(reader->GetOutput(cc));
    writer->SetFileName(buffer);
    writer->Write();
    }

  vtkPoints* wpts = reader->GetOutput(0)->GetPoints();
  vtkIdType ii;
  for ( ii = 0; ii < wpts->GetNumberOfPoints(); ++ ii )
    {
    double *pt = wpts->GetPoint(ii);
    cout << "Way Point: " << ii
     << " ["
     << reader->GetWayPointLongitude(ii) << ", "
     << reader->GetWayPointLatitude(ii) << ", "
     << reader->GetWayPointElevation(ii) << "] ("
     << pt[0] << ", "
     << pt[1] << ", "
     << pt[2] << ") t: "
     << reader->GetWayPointTime(ii)
     << " n: " << reader->GetWayPointName(ii)
     << " d: " << reader->GetWayPointDescription(ii)
     << " s: " << reader->GetWayPointSymbol(ii)
     << endl;
    }
  vtkPoints* pts = reader->GetOutput(1)->GetPoints();
  for ( ii = 0; ii < pts->GetNumberOfPoints(); ++ ii )
    {
    double *pt = pts->GetPoint(ii);
    cout << "Route Point: " << ii
     << " ["
     << reader->GetRoutePointLongitude(ii) << ", "
     << reader->GetRoutePointLatitude(ii) << ", "
     << reader->GetRoutePointElevation(ii) << "] ("
     << pt[0] << ", "
     << pt[1] << ", "
     << pt[2] << ") t: "
     << reader->GetRoutePointTime(ii)
     << " n: " << reader->GetRoutePointName(ii)
     << " d: " << reader->GetRoutePointDescription(ii)
     << " s: " << reader->GetRoutePointSymbol(ii)
     << endl;
    }
  pts = reader->GetOutput(2)->GetPoints();
  for ( ii = 0; ii < pts->GetNumberOfPoints(); ++ ii )
    {
    double *pt = pts->GetPoint(ii);
    
    cout << "Track Point: " << ii
     << " ["
     << reader->GetTrackPointLongitude(ii) << ", "
     << reader->GetTrackPointLatitude(ii) << ", "
     << reader->GetTrackPointElevation(ii) << "] ("
     << pt[0] << ", "
     << pt[1] << ", "
     << pt[2] << ") t: "
     << reader->GetTrackPointTime(ii);

    // Does not work
    vtkIdType wpid = reader->GetWayPointFromTrackPoint(ii);
    if ( wpid >= 0 )
      {
      cout
        << " n: " << reader->GetWayPointName(wpid)
        << " d: " << reader->GetWayPointDescription(wpid)
        << " s: " << reader->GetWayPointSymbol(wpid);
      }
    
    cout << endl;
    }

  reader->Delete();
  writer->Delete();
  return 0;
}
