/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMReader.h"
#include "vtkDICOMCollector.h"
#include "vtkDICOMCollectorOptions.h"
#include "vtkExecutive.h"
#include "vtkGlobFileNames.h"
#include "vtkImageData.h"

#include "vtkKWCommonProBuildConfigure.h" // for VolView_DATA_ROOT

int TestReadSCOUT(const char *filename, unsigned int numvolumes)
{
  // Need to use a glob, because of the warning file: DO NOT USE...
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  globFileNames->AddFileNames( filename );

  vtkDICOMReader *reader = vtkDICOMReader::New();
  reader->SetFileNames( globFileNames->GetFileNames() );
  reader->GetDICOMCollector()->GetOptions()->ExploreDirectoryOff();  //important
  reader->GetDICOMCollector()->GetOptions()->SkipProblematicFileOff();
  //reader->Update(); // we loose the return value
  vtkExecutive *exe = reader->GetExecutive();
  int r = exe->Update();
  //reader->GetOutput()->Print( cerr );
  if( !r )
    {
    cerr << "Failed to load: " << filename << endl;
    return 1;
    }
  if( numvolumes != reader->GetNumberOfOutputPorts() )
    {
    cerr << "Wrong number of volumes: " << reader->GetNumberOfOutputPorts() <<
      " instead of: " << numvolumes << endl;
    return 1;
    }

  globFileNames->Delete();
  reader->Delete();
  return 0;
}

int main(int argc, char *argv[])
{
  int r = 0;
  // http://www.cvmt.dk/~hn/2005/Transport/Ear_project/Rosborg/DICOM/PA1/ST1/SE1/
  r += TestReadSCOUT( VolView_DATA_ROOT "/Data/SCOUT/SE1/IM*", 2);

  // http://www-creatis.insa-lyon.fr/~jpr/PUBLIC/gdcm/gdcmSampleData/ForSeriesTesting/VariousIncidences/ST1/SE1-9-scout-views/
  r += TestReadSCOUT( VolView_DATA_ROOT "/Data/SCOUT/SE1-9-scout-views/IM*", 3);

  return r;
}

