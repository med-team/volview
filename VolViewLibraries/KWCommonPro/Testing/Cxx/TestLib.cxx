/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkAnalyzeReader.h"
#include "vtkDICOMReader.h"
#include "vtkGESignaReader3D.h"
#include "vtkPICReader.h"

template<class DType>
int DoTest(DType*)
{
  DType * t = DType::New();
  t->Print(cout);
  t->GetClassName();
  t->Delete();
  return 0;
}

int main()
{
  int res = 0;
  res += ::DoTest(static_cast<vtkAnalyzeReader*>(0));
  res += ::DoTest(static_cast<vtkDICOMReader*>(0));
  res += ::DoTest(static_cast<vtkGESignaReader3D*>(0));
  res += ::DoTest(static_cast<vtkPICReader*>(0));
  return res;
}
