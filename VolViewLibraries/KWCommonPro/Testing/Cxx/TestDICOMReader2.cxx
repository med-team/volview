/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "TestDICOMReader.h"

int TestDICOMRead2(const char *filename, bool inverted = false)
{
  // Let the vtkDICOMCollector do it's job:
  vtkDICOMReader* reader1 = vtkDICOMReader::New();
  reader1->SetFileName( filename );
  reader1->Update();

  // Ok we know the correct answer:
  // Let's contruct a glob expression.
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0001.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0002.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0003.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0004.*
  // Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0005.*
  vtkGlobFileNames *globFileNames = vtkGlobFileNames::New();
  std::string path = vtksys::SystemTools::GetFilenamePath( filename );
  std::string fn = path + "/" + 
    vtksys::SystemTools::GetFilenameWithoutLastExtension( filename ) + ".*";
  //cerr << fn << endl;
  globFileNames->AddFileNames( fn.c_str() );

  vtkDICOMReader* reader2 = vtkDICOMReader::New();
  reader2->SetFileNames( globFileNames->GetFileNames() );
  reader2->Update();

  // Let's verify a couple of things:
  if ( reader1->GetNumberOfOutputPorts() != 1
    || reader2->GetNumberOfOutputPorts() != 1 )
    {
    return 1;
    }
  vtkImageData *img1 = reader1->GetOutput();
  vtkImageData *img2 = reader2->GetOutput();
  if ( img1->GetNumberOfPoints() != img2->GetNumberOfPoints() )
    {
    return 1;
    }
  double *range1 = img1->GetScalarRange();
  double *range2 = img2->GetScalarRange();
  if( range1[0] != range2[0] || range1[1] != range2[1] )
    {
    return 1;
    }

  // Final tests, the list should be ordered in the ascending order, this works
  // nicely for a couple of dataset where filename also indicate order (very rare !)
  if( reader1->GetDICOMCollector()->GetNumberOfCollectedSlices() != 
      reader2->GetDICOMCollector()->GetNumberOfCollectedSlices() )
    {
    return 1;
    }
  int n = reader1->GetDICOMCollector()->GetNumberOfCollectedSlices();
  for(int i = 0; i < n; ++i)
    {
    const char *fn1 = reader1->GetDICOMCollector()->GetSliceFileName(i);
    const char *fn2 = reader2->GetDICOMCollector()->GetSliceFileName(i);
    // filename should be identical
    if ( strcmp(fn1, fn2) != 0 )
      {
      return 1;
      }
    //cerr << fn << endl;
    // Eg: IM-0001-0355.dcm -> 355
    vtksys_stl::string temp = vtksys::SystemTools::GetFilenameName( fn1 );
    vtksys_stl::string::size_type dash_pos = temp.rfind( "-" );
    vtksys_stl::string ext;
    if(dash_pos != vtksys_stl::string::npos)
      {
      ext = temp.substr(dash_pos);
      }
    else
      {
      ext = temp;
      }
    ext = vtksys::SystemTools::GetFilenameWithoutLastExtension( ext );
    
    // The extension is increasing from 1 to n or n to 1
    int comp = inverted ? (n-i) : (i+1);
    if( atoi( ext.c_str()+1 ) != comp )
      {
      cerr << "ext: " << ext << " instead of " << comp << endl;
      return 1;
      }
    }

  globFileNames->Delete();
  reader1->Delete();
  reader2->Delete();

  return 0;
}

int main(int argc, char* argv[])
{
  // I need to check VolView_DATA_ROOT ...

  int r = 0;
  r += TestDICOMRead2( 
    VolView_DATA_ROOT
    "/Data/Osirix/AGECANONIX/Specials-1CoronaryCTA_with_spiral_CTA_pre/"
    "CorCTA-w-c-1.0-B20f/IM-0001-0001.dcm", true);

  return r;
}

