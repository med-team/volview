/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMReader.h"
#include "vtkDICOMCollector.h"
#include "vtkGlobFileNames.h"
#include "vtkImageData.h"

#include "vtkKWCommonProBuildConfigure.h" // for VolView_DATA_ROOT

#include <vtksys/SystemTools.hxx>

// Correct order is:
static const char *vtkKWCommonProAAAStrings[] = {
  "OK-000145_anon.D3I",
  "OK-000144_anon.D3I",
  "OK-000143_anon.D3I",
  "OK-000142_anon.D3I",
  "OK-000141_anon.D3I",
  "OK-000140_anon.D3I",
  NULL
};
static double vtkKWCommonProAAARange[2] = { -1024 , 1296 };

static const char *vtkKWCommonProCornellStrings[] = {
  "1.2.826.0.1.3680043.2.656.4.1.1.52.105.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.104.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.103.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.102.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.101.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.100.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.99.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.98.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.97.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.96.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.95.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.94.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.93.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.92.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.91.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.90.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.89.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.88.dcm",
  "1.2.826.0.1.3680043.2.656.4.1.1.52.87.dcm",
  NULL
};

static double vtkKWCommonProCornellRange[2] = { -1024 , 3071 };

static const char *vtkKWCommonProPHILIPSStrings[] = {
  "00000019",
  "00000018",
  "00000017",
  "00000016",
  "00000015",
  "00000014",
  "00000013",
  "00000012",
  "00000011",
  "00000010",
  "00000009",
  "00000008",
  "00000007",
  "00000006",
  "00000005",
  "00000004",
  "00000003",
  "00000002",
  NULL
};

static double vtkKWCommonProPHILIPSRange[2] = { 1 , 251 };

static const char *vtkKWCommonProSynarcStrings[] = {
  "Image100.dcm",
  "Image101.dcm",
  "Image102.dcm",
  "Image103.dcm",
  "Image104.dcm",
  "Image105.dcm",
  "Image106.dcm",
  "Image107.dcm",
  NULL
};

static double vtkKWCommonProSynarcRange[2] = { 0 , 127 };

int TestSeries(const char *filename, const char *refstrings[], double refrange[2])
{
  vtkDICOMReader *reader = vtkDICOMReader::New();
  reader->SetFileName( filename );
  reader->Update();
  //reader->GetOutput()->Print( cout );
  // Let's check a couple of properties:
  if( reader->GetNumberOfOutputPorts() != 1 )
    {
    cerr << "Wrong number of output ports" << endl;
    return 1;
    }
  // Scalar range:
  double *range = reader->GetOutput()->GetScalarRange();
  if ( range[0] != refrange[0]|| range[1] != refrange[1] )
    {
    cerr << "Error: " << range[0] << " " << range[1] << endl;
    return 1;
    }

  int n = reader->GetDICOMCollector()->GetNumberOfCollectedSlices();
  for(int i = 0; i < n; ++i)
    {
    const char *full = reader->GetDICOMCollector()->GetSliceFileName(i);
    vtksys_stl::string fn_str = vtksys::SystemTools::GetFilenameName( full );
    const char *fn = fn_str.c_str();
    if( !refstrings[i] )
      {
      cerr << "Too many file compared to ref:" << full << endl;
      return 1;
      }
    if( strcmp( fn, refstrings[i] ) != 0 )
      {
      cerr << fn << " != " << refstrings[i] << endl;
      return 1;
      }
    }
  // Check the n equal the number of string stored in the array of refstrings
  if( refstrings[n] )
    {
    cerr << "Found one more file" << refstrings[n] << endl;
    return 1;
    }

  reader->Delete();
  cout << "Sucess: " << filename << endl;
  return 0;
}

int main(int argc, char *argv[])
{
  int r = 0;
  r += TestSeries( 
    VolView_DATA_ROOT "/Data/AAA/CTSiemens/OK-000140_anon.D3I", vtkKWCommonProAAAStrings, 
    vtkKWCommonProAAARange );
  r += TestSeries( 
    VolView_DATA_ROOT "/Data/Cornell/TherapyAseess/PR0006/20000101-091423-2-3/" 
    "1.2.826.0.1.3680043.2.656.4.1.1.52.87.dcm" , vtkKWCommonProCornellStrings,
    vtkKWCommonProCornellRange );
  r += TestSeries( 
    VolView_DATA_ROOT "/Data/IHE-PDI-DICOM/PHILIPS/070801A/20010807/10083045/00000002", 
    vtkKWCommonProPHILIPSStrings, vtkKWCommonProPHILIPSRange );
  r += TestSeries( 
    VolView_DATA_ROOT "/Data/Synarc/MRSiemens/Image101.dcm",
    vtkKWCommonProSynarcStrings, vtkKWCommonProSynarcRange);

  return r;
}
