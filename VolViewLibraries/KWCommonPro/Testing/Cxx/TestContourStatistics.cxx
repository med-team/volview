/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPolyData.h"
#include "vtkContourStatistics.h"
#include "vtkImageData.h"
#include "vtkCell.h"
#include "vtkCellArray.h"
#include "vtkPoints.h"

int main()
{
  double contourPoints[4][3] = {{1.0,2.0,3.0},{5.2,1.1,3.0},{6.1,5.3,3.0},{0.6,4.8,3.0}};
  int imageExtent[6]  = {0,8,0,5,0,8};
  double imageSpacing[3] = {1.1,1.2,1.0};
  double imageOrigin[3]  = {0.0,0.0,0.0};
  unsigned int numberOfPoints = 4;
  vtkPoints *points = vtkPoints::New();
  vtkCellArray *lines = vtkCellArray::New();
  
  vtkIdType index = 0;
  
  points->SetNumberOfPoints(numberOfPoints);
  vtkIdType *lineIndices = new vtkIdType[numberOfPoints + 1];
  
  for ( unsigned int i=0; i<numberOfPoints; i++ )
    {
    points->InsertPoint( index, contourPoints[numberOfPoints-i-1] );
    lineIndices[index] = index;
    index++;
    }
      
  lineIndices[numberOfPoints] = 0;
  lines->InsertNextCell( numberOfPoints+1, lineIndices );
  delete [] lineIndices;
  
  vtkPolyData *contour = vtkPolyData::New();
  contour->SetPoints( points );
  contour->SetLines( lines );
  points->Delete();
  lines->Delete();

  vtkImageData *image = vtkImageData::New();
  image->SetExtent( imageExtent );
  image->SetSpacing( imageSpacing );
  image->SetOrigin( imageOrigin );
  image->SetScalarTypeToUnsignedChar();
  for (int z=imageExtent[4]; z<= imageExtent[5]; z++)
    {
    for (int y=imageExtent[2]; y<= imageExtent[3]; y++)
      {
      for (int x=imageExtent[0]; x<= imageExtent[1]; x++)
        {
        *((unsigned char *)(image->GetScalarPointer(x,y,z))) = (unsigned char)(x+y+z);
        }
      }
    }
  
  vtkContourStatistics *contourStats = vtkContourStatistics::New();
  contourStats->SetInput( contour );
  contourStats->SetImageData( image );
  double area = contourStats->GetArea();
  if ((area-17.025)>0.001) 
    {
    std::cerr << "Area:       " << area << std::endl;
    return EXIT_FAILURE;
    }
  
  double perimeter = contourStats->GetPerimeter();
  if ((perimeter-16.9418)>0.001) 
    {
    std::cout << "Perimeter:  " << perimeter << std::endl;
    return EXIT_FAILURE;
    }

  if (contourStats->GetStatisticsComputeFailed())
    {
    return EXIT_FAILURE;
    }
  std::cout << "Min:        " << contourStats->GetMinimum() << std::endl;
  std::cout << "Max:        " << contourStats->GetMaximum() << std::endl;
  std::cout << "Mean:       " << contourStats->GetMean() << std::endl;
  std::cout << "StdDev:     " << contourStats->GetStandardDeviation() << std::endl;
  std::cout << "NumberOfPix:" << contourStats->GetNumberOfPixelsInContour() << std::endl;

  contourStats->Delete();
  image->Delete();
  contour->Delete();
  
  return EXIT_SUCCESS;
}

