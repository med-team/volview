/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "TestDICOMReader.h"

int main(int argc, char* argv[])
{
  // I need to check VolView_DATA_ROOT ...

  int r = 0;
  r += TestDICOMRead( 
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0001.1" );
  r += TestDICOMRead( 
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0002.1" );
  r += TestDICOMRead( 
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0003.1" );
  r += TestDICOMRead( 
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0004.1" );
  r += TestDICOMRead( 
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/0005.1", true );

//  r += TestDICOMMultiOutput(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00010001/000*" );

  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00020001/0001.1" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00020001/0002.1" );

//  r += TestDICOMMultiOutput(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Head/00020001/000*" );

//  Gantry:
//  r += TestDICOMRead(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0001.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0002.001" );
//  Gantry:
//  r += TestDICOMRead(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0003.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0004.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0005.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0006.001" );
//  r += TestDICOMRead(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0007.001" );
//  r += TestDICOMRead(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0008.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0009.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0010.001" );
  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/0011.001" );

//  r += TestDICOMMultiOutput(
//    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-GE-HiSpeed-PixelPaddVal/00*" );

  r += TestDICOMRead(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Chest/1.2.392.200036.9116.2.2.2.1762676583.1077596893.116155.001", true );
  r += TestDICOMMultiOutput(
    VolView_DATA_ROOT "/Data/Coactiv/HelicalCT-ToshibaAquilion-Chest/1.2.392.200036.9116.2.2.2.1762676583.1077596893.116155.*" );

  return r;
}

