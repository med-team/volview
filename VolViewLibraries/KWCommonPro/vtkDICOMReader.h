/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkDICOMReader - read an DICOM volume file.
// .SECTION Description
// vtkDICOMReader reads an DICOM file and creates a structured point dataset.
// The size of the volume and the data spacing is set from the DICOM file
// header.

#ifndef __vtkDICOMReader_h
#define __vtkDICOMReader_h

#include "vtkMedicalImageReader2.h"

class vtkDICOMCollector;
class vtkEventForwarderCommand;
class vtkStringArray;
class vtkDICOMReaderInternals;

class VTK_EXPORT vtkDICOMReader : public vtkMedicalImageReader2
{
public:
  static vtkDICOMReader *New();
  vtkTypeRevisionMacro(vtkDICOMReader,vtkMedicalImageReader2);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Specify file name for the image file to query the series/volume it is
  // part of. It is also possible to specify the list of filenames explicitly:
  // this will bypass the collecting routine.
  virtual void SetFileName(const char *);
  virtual void SetFileNames(vtkStringArray *);

  // Description:
  // Is the given file name a DICOM file?
  virtual int CanReadFile(const char* fname);

  // Description:
  // Create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkDICOMReader::New();}

  // Description:
  // Get the internal DICOM collector object
  vtkGetObjectMacro(DICOMCollector, vtkDICOMCollector);

protected:
  vtkDICOMReader();
  ~vtkDICOMReader();

  vtkDICOMCollector *DICOMCollector;
  vtkEventForwarderCommand *CollectorEventForwarder;

  virtual int RequestInformation(vtkInformation *request,
                                 vtkInformationVector **inputVector,
                                 vtkInformationVector *outputVector);
  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);

  virtual int GetMedicalProperties();

private:
  vtkDICOMReader(const vtkDICOMReader&); // Not implemented
  void operator=(const vtkDICOMReader&); // Not implemented
};

#endif



