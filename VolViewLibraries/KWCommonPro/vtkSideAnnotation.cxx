/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkSideAnnotation.h"

#include "vtkObjectFactory.h"
#include "vtkTextActor.h"
#include "vtkTextProperty.h"
#include "vtkTextMapper.h"

//----------------------------------------------------------------------------
vtkCxxRevisionMacro( vtkSideAnnotation, "$Revision: 1.4 $");
vtkStandardNewMacro( vtkSideAnnotation );

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLSideAnnotationReader.h"
#include "XML/vtkXMLSideAnnotationWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkSideAnnotation, vtkXMLSideAnnotationReader, vtkXMLSideAnnotationWriter);

//----------------------------------------------------------------------------
vtkSideAnnotation::vtkSideAnnotation()
{
  //this->SetLinearFontScaleFactor(5);
  //  this->SetNonlinearFontScaleFactor(0.35);
  this->SetMaximumLineHeight(0.07);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetTextActorsPosition(int vsize[2])
{
  /*
    -X: 0
    +X: 1
    -Y: 2
    +Y: 3 

           original:         now:
          +---------+    +---------+
          |2       3|    |    3    |
          |         | => |         |
          |         |    |0       1|
          |         |    |         |
          |0       1|    |    2    | 
          +---------+    +---------+

  */

  int h_mid = vsize[0] / 2;
  int v_mid = vsize[1] / 2;
  int margin = 7;

  this->TextActor[0]->SetPosition(margin, v_mid);
  this->TextActor[1]->SetPosition(vsize[0] - margin, v_mid);
  this->TextActor[2]->SetPosition(h_mid, margin);
  this->TextActor[3]->SetPosition(h_mid, vsize[1] - margin);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetTextActorsJustification()
{
  vtkTextProperty *tprop = this->TextMapper[0]->GetTextProperty();
  tprop->SetJustificationToLeft();
  tprop->SetVerticalJustificationToCentered();

  tprop = this->TextMapper[1]->GetTextProperty();
  tprop->SetJustificationToRight();
  tprop->SetVerticalJustificationToCentered();
        
  tprop = this->TextMapper[2]->GetTextProperty();
  tprop->SetJustificationToCentered();
  tprop->SetVerticalJustificationToBottom();
        
  tprop = this->TextMapper[3]->GetTextProperty();
  tprop->SetJustificationToCentered();
  tprop->SetVerticalJustificationToTop();
}

//----------------------------------------------------------------------------
const char* vtkSideAnnotation::GetMinusXLabel() 
{
  return this->GetText(0);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetMinusXLabel(const char *l) 
{
  this->SetText(0, l);
}

//----------------------------------------------------------------------------
const char* vtkSideAnnotation::GetXLabel() 
{
  return this->GetText(1);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetXLabel(const char *l) 
{
  this->SetText(1, l);
}

//----------------------------------------------------------------------------
const char* vtkSideAnnotation::GetMinusYLabel() 
{
  return this->GetText(2);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetMinusYLabel(const char *l) 
{
  this->SetText(2, l);
}

//----------------------------------------------------------------------------
const char* vtkSideAnnotation::GetYLabel() 
{
  return this->GetText(3);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::SetYLabel(const char *l) 
{
  this->SetText(3, l);
}

//----------------------------------------------------------------------------
void vtkSideAnnotation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
