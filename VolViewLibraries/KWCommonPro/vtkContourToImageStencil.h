/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkContourToImageStencil - Rasterize a contour to produce a vtkImageStencilData
// 
// .SECTION Description
// vtkContourToImageStencil rasterizes a contour drawn on the XY plane to 
// produce a vtkImageStencilData. The contour is represented by a vtkPolyData. 
// The polydata must have one cell (must be a vtkPolyLine) and must be closed
// (with at least 3 points). It is assumed that the contour is drawn on the XY 
// plane. So the extents along the third dimension must be 0. 
//
// .SECTION Parameters and Usage
// \code
// myvtkContourToImageStencil->SetInput( contour );
// myvtkContourToImageStencil->Update();
// myvtkContourToImageStencil->SetSpacing(..); // spacing and origin of the image
// myvtkContourToImageStencil->SetOrigin(..);  // on which the contour is drawn 
// vtkImageStencilData *stencil = myvtkContourToImageStencil->GetOutput();
// \endcode
//
// .SECTION See Also
// vtkPolyDataToImageStencil
//
#ifndef __vtkContourToImageStencil_h
#define __vtkContourToImageStencil_h

#include "vtkImageStencilSource.h"

class vtkPolyData;

class VTK_EXPORT vtkContourToImageStencil : public vtkImageStencilSource
{
public:
  static vtkContourToImageStencil *New();
  vtkTypeRevisionMacro(vtkContourToImageStencil, vtkImageStencilSource);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Specify the polydata to convert into a stencil.
  void SetInput(vtkPolyData *input);
  vtkPolyData *GetInput();

  // Description:
  // Set the spacing (width,height,length) of the vtkImageData on which the 
  // contour is drawn
  vtkSetVector3Macro(Spacing,double);
  vtkGetVector3Macro(Spacing,double);
  
  // Description:
  // Set the origin of the vtkImageData on which the contour is drawn
  vtkSetVector3Macro(Origin,double);
  vtkGetVector3Macro(Origin,double);

protected:
  vtkContourToImageStencil();
  ~vtkContourToImageStencil();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  virtual int RequestInformation(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  virtual int FillInputPortInformation(int, vtkInformation*);
  
  // Description:
  // Determine whether to flip the stencil or not and flip if necessary. See
  // the .cxx for a detailed description
  int FlipStencil( int extent[6], vtkImageStencilData *stencilIn );

  double Origin[3];
  double Spacing[3];

private:
  vtkContourToImageStencil(const vtkContourToImageStencil&);  // Not implemented.
  void operator=(const vtkContourToImageStencil&);  // Not implemented.
};

#endif

