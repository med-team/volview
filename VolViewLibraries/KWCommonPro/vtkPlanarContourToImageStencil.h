/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPlanarContourToImageStencil - Rasterize a contour to produce a vtkImageStencilData
// 
// .SECTION Description
// vtkPlanarContourToImageStencil rasterizes a contour drawn on the XY plane to 
// produce a vtkImageStencilData. The contour is represented by a vtkPolyData. 
// The polydata must have one cell (must be a vtkPolyLine) and must be closed
// (with at least 3 points). It is assumed that the contour is drawn on the XY 
// plane. So the extents along the third dimension must be 0. 
//
// .SECTION Parameters and Usage
// \code
// myvtkPlanarContourToImageStencil->SetInput( contour );
// myvtkPlanarContourToImageStencil->Update();
// myvtkPlanarContourToImageStencil->SetSpacing(..); // spacing and origin of the image
// myvtkPlanarContourToImageStencil->SetOrigin(..);  // on which the contour is drawn 
// vtkImageStencilData *stencil = myvtkPlanarContourToImageStencil->GetOutput();
// \endcode
//
// .SECTION See Also
// vtkPolyDataToImageStencil
//
#ifndef __vtkPlanarContourToImageStencil_h
#define __vtkPlanarContourToImageStencil_h

#include "vtkImageStencilSource.h"

class vtkPolyData;

class VTK_EXPORT vtkPlanarContourToImageStencil : public vtkImageStencilSource
{
public:
  static vtkPlanarContourToImageStencil *New();
  vtkTypeRevisionMacro(vtkPlanarContourToImageStencil, vtkImageStencilSource);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Specify the polydata to convert into a stencil.
  void SetInput(vtkPolyData *input);
  vtkPolyData *GetInput();

  // Description:
  // Set the spacing (width,height,length) of the vtkImageData on which the 
  // contour is drawn
  vtkSetVector3Macro(Spacing,double);
  vtkGetVector3Macro(Spacing,double);
  
  // Description:
  // Set the origin of the vtkImageData on which the contour is drawn
  vtkSetVector3Macro(Origin,double);
  vtkGetVector3Macro(Origin,double);

protected:
  vtkPlanarContourToImageStencil();
  ~vtkPlanarContourToImageStencil();

  virtual int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  virtual int RequestInformation(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  virtual int FillInputPortInformation(int, vtkInformation*);
  
  // Description:
  // Determine whether to flip the stencil or not and flip if necessary. See
  // the .cxx for a detailed description
  int FlipStencil( int extent[6], vtkImageStencilData *stencilIn );

  double Origin[3];
  double Spacing[3];

private:
  vtkPlanarContourToImageStencil(const vtkPlanarContourToImageStencil&);  // Not implemented.
  void operator=(const vtkPlanarContourToImageStencil&);  // Not implemented.
};

#endif


