/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkDICOMCollectorOptions - 
// .SECTION Description
//

#ifndef __vtkDICOMCollectorOptions_h
#define __vtkDICOMCollectorOptions_h

#include "vtkObject.h"

class VTK_EXPORT vtkDICOMCollectorOptions : public vtkObject
{
public:
  static vtkDICOMCollectorOptions *New();
  vtkTypeRevisionMacro(vtkDICOMCollectorOptions, vtkObject);
  virtual void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Turning on/off the automatic exploration of directory.
  // WARNING: If multiple (>1) files are specified exploration of
  // directory will not happen (backward compat).
  vtkSetMacro(ExploreDirectory, int);
  vtkGetMacro(ExploreDirectory, int);
  vtkBooleanMacro(ExploreDirectory, int);

  //BTX
  typedef enum 
  {
    UnknownSeries = 0,
    SingleSeries,
    SCOUTSeries,
    DWISeries,
    // Handle missing slices...
    SingleSeriesMissingSlices,
    SCOUTSeriesMissingSlices,
    DWISeriesMissingSlices,
    InvalidSeries
  } SeriesTypes;
  //ETX

  // Description:
  // Explicitely require the Series to be of a specific type, in case of
  // missing slice
  // this information is required.
  vtkSetMacro(ForceSeriesType, int);
  vtkGetMacro(ForceSeriesType, int);

  // Description:
  // Decide or not to discard file that were not written with non-ascii
  // character set
  // Greek:   ISO_IR 126
  // Arab:    ISO_IR 127
  // Russian: ISO_IR 144
  // ... anything different from empty (backward compat with ACR-NEMA) or
  // ISO_IR 100
  vtkSetMacro(SupportASCIICharacterSetOnly, int);
  vtkGetMacro(SupportASCIICharacterSetOnly, int);
  vtkBooleanMacro(SupportASCIICharacterSetOnly, int);

  // Description:
  // Do we require that the DICOM file provide us with its class UID (MR,
  // CT ... )
  vtkSetMacro(RequireSOPClassUID, int);
  vtkGetMacro(RequireSOPClassUID, int);
  vtkBooleanMacro(RequireSOPClassUID, int);

  // Description:
  // Do we require to have :
  // Image Orientation (Patient)
  // Image Position (Patient)
  vtkSetMacro(RequireSpatialInformation, int);
  vtkGetMacro(RequireSpatialInformation, int);
  vtkBooleanMacro(RequireSpatialInformation, int);

  // Description:
  // Ignore problematic file within a serie ?
  vtkSetMacro(SkipProblematicFile, int);
  vtkGetMacro(SkipProblematicFile, int);
  vtkBooleanMacro(SkipProblematicFile, int);

  // Description:
  // Do we authorize loading of file with a gantry tilt
  vtkSetMacro(SupportGantryTilt, int);
  vtkGetMacro(SupportGantryTilt, int);
  vtkBooleanMacro(SupportGantryTilt, int);

  // Description:
  // Do we authorize loading of file with multiple samples per pixel
  vtkSetMacro(SupportMultipleSamplesPerPixel, int);
  vtkGetMacro(SupportMultipleSamplesPerPixel, int);
  vtkBooleanMacro(SupportMultipleSamplesPerPixel, int);

protected:
  vtkDICOMCollectorOptions();
  ~vtkDICOMCollectorOptions();

  // Description:
  // Instead of trying to figure out the series type, outside world
  // can specifically set it for us
  int ForceSeriesType;

  // Description:
  // When user specify only one single DICOM file the behavior of
  // DICOMCollectorOptions is to look for other DICOM file within the same
  // directory. Turning this variable off means we explicitely refuse
  // this behavior (reading only specified file).
  // Another effect of turning this var is that only related DICOM file 
  // will be looked at
  int ExploreDirectory;

  // Description:
  // Discard Non-ASCII character set.
  int SupportASCIICharacterSetOnly;

  // Description:
  // Do we require that DICOM file provide us with :
  // Image Orientation (Patient)
  // Image Position (Patient)
  // NB: Secondary Capture do not have Patient Information table. So they
  // are not required
  // to provide this information.
  int RequireSpatialInformation;

  // Description:
  int RequireSOPClassUID;

  // Description:
  int SkipProblematicFile;

  // Description:
  int SupportGantryTilt;

  // Description:
  int SupportMultipleSamplesPerPixel;

private:
  vtkDICOMCollectorOptions(const vtkDICOMCollectorOptions&); // Not implemented
  void operator=(const vtkDICOMCollectorOptions&); // Not implemented
};

#endif

