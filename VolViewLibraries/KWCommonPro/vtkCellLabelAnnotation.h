/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkCellLabelAnnotation - add labels along 2 edges of a dataset

// .SECTION Description
// The vtkCellLabelAnnotation labels the cells along 2 edges of a
// vtkStructuredGrid data set with the cell id numbers. The edges chosen
// are based on which corner of the dataset is drawn closest to the lower
// left corner of the viewport containing the actor for the data and
// this annotation.

#ifndef __vtkCellLabelAnnotation_h
#define __vtkCellLabelAnnotation_h

#include "vtkActor2D.h"

class vtkActor;
class vtkStructuredGrid;
class vtkTextActor;
class vtkViewport;

//BTX
struct vtkCellLabelAnnotationInternals;
//ETX

class VTK_EXPORT vtkCellLabelAnnotation : public vtkActor2D
{
public:
  void PrintSelf(ostream& os, vtkIndent indent);
  vtkTypeRevisionMacro(vtkCellLabelAnnotation, vtkActor2D);

  // Description:
  // Creates a cell label annotation
  static vtkCellLabelAnnotation* New();

  // Description:
  // Set the input structured grid this annotation is labeling.
  void SetInput(vtkStructuredGrid*);

  // Description:
  // Set the actor around which to draw this annotation.
  void SetDataActor(vtkActor*);

  // Description:
  // Render the annotation
  virtual int RenderOverlay(vtkViewport *viewport);
  virtual int RenderOpaqueGeometry(vtkViewport *viewport);
  virtual int RenderTranslucentGeometry(vtkViewport*) { return 0; }

protected:
  vtkCellLabelAnnotation();
  ~vtkCellLabelAnnotation();

  vtkTextActor *LabelActor;
  vtkStructuredGrid *Input;
  double ParallelScale;
  double ActorScale[3];
  int ViewportSize[2];
  int Extent[6];
  vtkActor *DataActor;
  vtkStructuredGrid *PreviousInput;
  int MissingDimension;

  void SetPreviousInput(vtkStructuredGrid*);
  vtkSetVector3Macro(ActorScale, double);
  vtkSetVector2Macro(ViewportSize, int);
  vtkSetVector6Macro(Extent, int);

  void ComputeLabelPositions(vtkViewport *viewport);
  void SetupActors(int idxMin, int idxMax, int slowInc, int fastInc,
                   int constantCoord, int constantCoord2, int currentAxis,
                   vtkViewport *viewport);

  // convenience methods to make the code more readable
  void GetWorldPoint(int slowCoord, int slowInc, int fastCoord, int fastInc,
                     double point[3]);
  void ScaleWorldPoint(double point[3]);
  void WorldToDisplay(double worldPoint[4], double displayPoint[3],
                      vtkViewport *viewport);
  void DisplayToWorld(double displayPoint[3], double worldPoint[4],
                      vtkViewport *viewport);
  void AddActorToList(double point[3], int idx);
  void MoveDisplayCoordinateFromEdge(double coord1[3], double coord2[3]);

private:
  vtkCellLabelAnnotationInternals *Internal;

  vtkCellLabelAnnotation(const vtkCellLabelAnnotation&);  // Not implemented.
  void operator=(const vtkCellLabelAnnotation&);  // Not implemented.
};

#endif
