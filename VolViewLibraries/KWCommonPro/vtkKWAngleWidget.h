/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWAngleWidget
// .SECTION Description

#ifndef __vtkKWAngleWidget_h
#define __vtkKWAngleWidget_h

#include "vtkAngleWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class VTK_EXPORT vtkKWAngleWidget : public vtkAngleWidget
{
public:
  static vtkKWAngleWidget* New();
  vtkTypeRevisionMacro(vtkKWAngleWidget, vtkAngleWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

protected:
  vtkKWAngleWidget() {};
  ~vtkKWAngleWidget() {};
  
private:
  vtkKWAngleWidget(const vtkKWAngleWidget&);  // Not implemented
  void operator=(const vtkKWAngleWidget&);  // Not implemented
};

#endif
