/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWCaptionWidget
// .SECTION Description

#ifndef __vtkKWCaptionWidget_h
#define __vtkKWCaptionWidget_h

#include "vtkCaptionWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkVolume;
class vtkRenderer;

class VTK_EXPORT vtkKWCaptionWidget : public vtkCaptionWidget
{
public:
  static vtkKWCaptionWidget* New();
  vtkTypeRevisionMacro(vtkKWCaptionWidget, vtkCaptionWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

  // Widget for the anchor point
  vtkGetObjectMacro(HandleWidget,vtkHandleWidget);

  // Volume that this widget is anchored on. This needs to
  // be set for UseAnchorPointOpacityOn to work.
  vtkGetObjectMacro(PickingVolume,vtkVolume);
  virtual void SetPickingVolume(vtkVolume*);

  // Description:
  // Create the default widget representation if one is not set. The representation
  // defines the geometry of the widget (i.e., how it appears) as well as providing
  // special methods for manipulating the state and appearance of the widget.
  virtual void CreateDefaultRepresentation();

  //virtual void UpdateLabelSize();
  virtual void SetEnabled(int enabling);

  // This is called from <Super>Action() in superclass vtkBorderWidget.
  // If a non-zero value is returned, the superclass will not process further;
  // If zero value is returned, the superclass will go ahead to process further.
  virtual int SubclassSelectAction();
  virtual int SubclassEndSelectAction();
  virtual int SubclassMoveAction();

  // Description:
  // Indicate whether to use point opacity as criteria while picking point.
  vtkSetMacro(UseAnchorPointOpacity,int);
  vtkGetMacro(UseAnchorPointOpacity,int);
  vtkBooleanMacro(UseAnchorPointOpacity,int);

  // Description:
  // Set the opacity limit to stop searching when UseAnchorPointOpacity On.
  vtkSetClampMacro(AccumulatedAnchorOpacityLimit,double,0.0,1.0);
  vtkGetMacro(AccumulatedAnchorOpacityLimit,double);

protected:
  vtkKWCaptionWidget() ;
  ~vtkKWCaptionWidget();

  // Define the initial label anchor position using the last 
  // left mouse press event position.
  void DefineInitialAnchorPosition();
  
  int UseAnchorPointOpacity;
  double AccumulatedAnchorOpacityLimit;
  vtkVolume* PickingVolume;

  // Return 1 on Success, 0 otherwise.
  int PickPositionWithOpacity(int X, int Y, vtkRenderer* viewport,
    double stoppingopacity, double pickpos[3]);

private:
  vtkKWCaptionWidget(const vtkKWCaptionWidget&);  // Not implemented
  void operator=(const vtkKWCaptionWidget&);  // Not implemented
};

#endif
