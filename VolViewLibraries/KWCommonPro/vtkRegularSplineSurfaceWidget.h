/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkRegularSplineSurfaceWidget - 3D widget for manipulating a spline surface patch.
// .SECTION Description
//
// This class was created as a variation of the vtkSplineSurfaceWidget.
// Here a surface is generated from the tensor product of two sets of Cardinal
// splines. The surface is defined by a rectangular grid of control points that
// become the control points of the cardinal splines along one of the surface
// parametric variables.
// 
// .SECTION See Also
// vtkSplineSurfaceWidget vtk3DWidget vtkBoxWidget vtkLineWidget vtkPointWidget
// vtkSphereWidget vtkImagePlaneWidget vtkImplicitPlaneWidget vtkPlaneWidget


#ifndef __vtkRegularSplineSurfaceWidget_h
#define __vtkRegularSplineSurfaceWidget_h

#include "vtkSplineSurfaceWidget.h"


class VTK_EXPORT vtkRegularSplineSurfaceWidget : public vtkSplineSurfaceWidget
{
public:
  // Description:
  // Instantiate the object.
  static vtkRegularSplineSurfaceWidget *New();

  vtkTypeRevisionMacro(vtkRegularSplineSurfaceWidget,vtkSplineSurfaceWidget);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Methods that satisfy the superclass' API.
  virtual void PlaceWidget(double bounds[6]);
  virtual void PlaceWidget()
    {this->Superclass::PlaceWidget();}
  virtual void PlaceWidget(double xmin, double xmax, double ymin, double ymax, 
                   double zmin, double zmax)
    {this->Superclass::PlaceWidget(xmin,xmax,ymin,ymax,zmin,zmax);}

  // Description:
  // Set/Get the number of handles for this widget.
  vtkGetMacro(NumberOfHandlesU, int);
  vtkGetMacro(NumberOfHandlesV, int);
  virtual void SetNumberOfHandles(int numberOfHandles)
    {this->Superclass::SetNumberOfHandles(numberOfHandles);}
  virtual void SetNumberOfHandles(int nu, int nv);

  // Description:
  // Set/Get the number of line segments representing the spline for
  // this widget.
  void SetResolutionU(int resolution );
  void SetResolutionV(int resolution );
  vtkGetMacro(ResolutionU,int);
  vtkGetMacro(ResolutionV,int);
  virtual void GenerateSurfacePoints();

protected:
  vtkRegularSplineSurfaceWidget();
  ~vtkRegularSplineSurfaceWidget();

  // The spline
  vtkCardinalSplinePatch* Spline;
  int NumberOfHandlesU;
  int NumberOfHandlesV;

  int NumberOfSplinePointsU;
  int NumberOfSplinePointsV;
  int ResolutionU;
  int ResolutionV;
  double* SplinePositionsU;
  double* SplinePositionsV;

  virtual void Initialize();
  virtual void BuildRepresentation();

private:
  vtkRegularSplineSurfaceWidget(const vtkRegularSplineSurfaceWidget&);  //Not implemented
  void operator=(const vtkRegularSplineSurfaceWidget&);  //Not implemented
};

#endif
