/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkGPXReader - read GPX GPS data
// .SECTION Description
// vtkGPXReader is a source object that reads GPX GPS data from EasyGPS
// The FileName must be specified
//

#ifndef __vtkGPXReader_h
#define __vtkGPXReader_h

#include "vtkPolyDataSource.h"

class vtkGPXReaderParser;

class VTK_EXPORT vtkGPXReader : public vtkPolyDataSource 
{
public:
  static vtkGPXReader* New();
  vtkTypeRevisionMacro(vtkGPXReader,vtkPolyDataSource);
  void PrintSelf(ostream& os, vtkIndent indent);

  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);

  double GetWayPointLongitude(vtkIdType id);
  double GetWayPointLatitude(vtkIdType id);
  double GetWayPointElevation(vtkIdType id);
  int GetWayPointTime(vtkIdType id);
  double GetRoutePointLongitude(vtkIdType id);
  double GetRoutePointLatitude(vtkIdType id);
  double GetRoutePointElevation(vtkIdType id);
  int GetRoutePointTime(vtkIdType id);
  double GetTrackPointLongitude(vtkIdType id);
  double GetTrackPointLatitude(vtkIdType id);
  double GetTrackPointElevation(vtkIdType id);
  int GetTrackPointTime(vtkIdType id);

  const char* GetWayPointName(vtkIdType id);
  const char* GetRoutePointName(vtkIdType id);
  const char* GetWayPointDescription(vtkIdType id);
  const char* GetRoutePointDescription(vtkIdType id);
  const char* GetWayPointSymbol(vtkIdType id);
  const char* GetRoutePointSymbol(vtkIdType id);

  vtkIdType GetWayPointFromTrackPoint(vtkIdType id);

protected:
  vtkGPXReader();
  ~vtkGPXReader();

  char *FileName;

  void Execute();
  void ExecuteInformation();

  vtkGPXReaderParser* Parser;

private:
  vtkGPXReader(const vtkGPXReader&);  // Not implemented.
  void operator=(const vtkGPXReader&);  // Not implemented.
};

#endif

