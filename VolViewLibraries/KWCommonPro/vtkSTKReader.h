/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkSTKReader - read ZEISS STK files
// .SECTION Description
// vtkSTKReader is a source object that reads basic STK files as specified
// 

#ifndef __vtkSTKReader_h
#define __vtkSTKReader_h

#include "vtkConfigure.h"

extern "C" {
#if ((VTK_MAJOR_VERSION <= 4) && (VTK_MINOR_VERSION <= 4))
#include "tiffio.h" // needed for TIFF
#else
#include "vtk_tiff.h" // needed for TIFF
#endif
}

#include "vtkImageReader2.h"

class VTK_EXPORT vtkSTKReader : public vtkImageReader2
{
public:
  static vtkSTKReader *New();
  vtkTypeRevisionMacro(vtkSTKReader,vtkImageReader2);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Is the given file an STK file?
  int CanReadFile(const char* fname);

  // Description:
  // Set/Get the units
  vtkGetStringMacro(Units);
  vtkSetStringMacro(Units);
  
  // Description:
  // Return file extention as a string for the factory .pic
  virtual const char* GetFileExensions()
    {
      return ".stk";
    }

  // Description: 
  // Return file description as a string for the factory STK
  virtual const char* GetDescriptiveName()
    {
      return "MetaMorph Stack";
    }
  //Description: 
  // create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkSTKReader::New(); }

  // Set/get methods to see if manual Origin/Spacing have
  // been set.
  vtkSetMacro( OriginSpecifiedFlag, bool );
  vtkGetMacro( OriginSpecifiedFlag, bool );
  vtkBooleanMacro( OriginSpecifiedFlag, bool );
  
  vtkSetMacro( SpacingSpecifiedFlag, bool );
  vtkGetMacro( SpacingSpecifiedFlag, bool );
  vtkBooleanMacro( SpacingSpecifiedFlag, bool );

protected:
  vtkSTKReader();
  ~vtkSTKReader();
  TIFF *Image;
  char *Units;
  
  int Open(const char *);
  void Clean();
  
  virtual void ExecuteInformation();
  virtual void ExecuteData(vtkDataObject *out);

private:
  vtkSTKReader(const vtkSTKReader&); // Not implemented
  void operator=(const vtkSTKReader&); // Not implemented

  bool OriginSpecifiedFlag;
  bool SpacingSpecifiedFlag;
};

#endif



