/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkBoundingBoxAnnotation.h"
#include "vtkObjectFactory.h"

#include "vtkCubeSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkProperty.h"

vtkStandardNewMacro(vtkBoundingBoxAnnotation);
vtkCxxRevisionMacro(vtkBoundingBoxAnnotation, "$Revision: 1.4 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLBoundingBoxAnnotationReader.h"
#include "XML/vtkXMLBoundingBoxAnnotationWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkBoundingBoxAnnotation, vtkXMLBoundingBoxAnnotationReader, vtkXMLBoundingBoxAnnotationWriter);

vtkBoundingBoxAnnotation::vtkBoundingBoxAnnotation()
{
  this->CubeSource = vtkCubeSource::New();

  vtkProperty *property = this->GetProperty();
  property->SetRepresentationToWireframe();
  property->EdgeVisibilityOn();  
  property->SetAmbient(1.0);
  property->SetDiffuse(0.0);
  property->SetSpecular(0.0);
  
  vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
  mapper->SetInput(this->CubeSource->GetOutput());
  
  this->SetMapper(mapper);

  mapper->Delete();
}

vtkBoundingBoxAnnotation::~vtkBoundingBoxAnnotation()
{
  this->CubeSource->Delete();
}

void vtkBoundingBoxAnnotation::SetInputBounds(
  float xmin, float xmax, float ymin, float ymax, float zmin, float zmax)
{
  this->CubeSource->SetXLength(((double)xmax - (double)xmin) * 1.05);
  this->CubeSource->SetYLength(((double)ymax - (double)ymin) * 1.05);
  this->CubeSource->SetZLength(((double)zmax - (double)zmin) * 1.05);
  this->CubeSource->SetCenter(((double)xmin + (double)xmax) * 0.5,
                              ((double)ymin + (double)ymax) * 0.5,
                              ((double)zmin + (double)zmax) * 0.5);
}

void vtkBoundingBoxAnnotation::Render(vtkRenderer *ren, vtkMapper *m)
{
  if ( ! this->Property)
    {
    this->GetProperty();
    }
  this->Property->Render(this, ren);
  m->Render(ren, this);
}

void vtkBoundingBoxAnnotation::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
