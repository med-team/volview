/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkGenericMathUtilities.h"
#include "vtkObjectFactory.h"

vtkCxxRevisionMacro(vtkGenericMathUtilities, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkGenericMathUtilities);

vtkGenericMathUtilities::vtkGenericMathUtilities()
{
}

vtkGenericMathUtilities::~vtkGenericMathUtilities()
{
}

void vtkGenericMathUtilities::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
