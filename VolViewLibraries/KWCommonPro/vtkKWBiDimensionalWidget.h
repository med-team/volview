/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWBiDimensionalWidget
// .SECTION Description

#ifndef __vtkKWBiDimensionalWidget_h
#define __vtkKWBiDimensionalWidget_h

#include "vtkBiDimensionalWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class VTK_EXPORT vtkKWBiDimensionalWidget : public vtkBiDimensionalWidget
{
public:
  static vtkKWBiDimensionalWidget* New();
  vtkTypeRevisionMacro(vtkKWBiDimensionalWidget, vtkBiDimensionalWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

protected:
  vtkKWBiDimensionalWidget() {};
  ~vtkKWBiDimensionalWidget() {};
  
private:
  vtkKWBiDimensionalWidget(const vtkKWBiDimensionalWidget&);  // Not implemented
  void operator=(const vtkKWBiDimensionalWidget&);  // Not implemented
};

#endif
