/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkBoundingBoxAnnotation
// .SECTION Description

#ifndef __vtkBoundingBoxAnnotation_h
#define __vtkBoundingBoxAnnotation_h

#include "vtkActor.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class vtkCubeSource;

class VTK_EXPORT vtkBoundingBoxAnnotation : public vtkActor
{
public:
  static vtkBoundingBoxAnnotation* New();
  vtkTypeRevisionMacro(vtkBoundingBoxAnnotation, vtkActor);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  virtual void SetInputBounds(
    float xmin, float xmax, float ymin, float ymax, float zmin, float zmax);
  virtual void SetInputBounds(float bounds[6])
    { this->SetInputBounds(bounds[0], bounds[1], 
                           bounds[2], bounds[3], 
                           bounds[4], bounds[5]); }

  void Render(vtkRenderer *ren, vtkMapper *m);
  
protected:
  vtkBoundingBoxAnnotation();
  ~vtkBoundingBoxAnnotation();
  
  vtkCubeSource *CubeSource;
  
private:
  vtkBoundingBoxAnnotation(const vtkBoundingBoxAnnotation&);  // Not implemented
  void operator=(const vtkBoundingBoxAnnotation&);  // Not implemented
};

#endif
