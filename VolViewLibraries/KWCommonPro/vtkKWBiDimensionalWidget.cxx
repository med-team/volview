/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWBiDimensionalWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkDistanceRepresentation2D.h"

vtkStandardNewMacro(vtkKWBiDimensionalWidget);
vtkCxxRevisionMacro(vtkKWBiDimensionalWidget, "$Revision: 1.5 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLKWBiDimensionalWidgetReader.h"
#include "XML/vtkXMLKWBiDimensionalWidgetWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkKWBiDimensionalWidget, vtkXMLKWBiDimensionalWidgetReader, vtkXMLKWBiDimensionalWidgetWriter);

//----------------------------------------------------------------------
void vtkKWBiDimensionalWidget::WidgetIsDefined()
{
  this->WidgetState = vtkBiDimensionalWidget::Manipulate;
  this->CurrentHandle = -1;
  this->HandleLine1Selected = 0;
  this->HandleLine2Selected = 0;
  this->Line1InnerSelected = 0;
  this->Line1OuterSelected = 0;
  this->Line2InnerSelected = 0;
  this->Line2OuterSelected = 0;
  this->CenterSelected = 0;
  this->SetEnabled(this->GetEnabled()); // show/hide the handles properly
  this->ReleaseFocus();
  this->Render();
}

//----------------------------------------------------------------------
int vtkKWBiDimensionalWidget::IsWidgetDefined()
{
  return this->WidgetState == vtkBiDimensionalWidget::Manipulate;
}

//----------------------------------------------------------------------
void vtkKWBiDimensionalWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
