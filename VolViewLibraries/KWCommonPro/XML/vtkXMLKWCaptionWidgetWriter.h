/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCaptionWidgetWriter - vtkCaptionWidget XML Writer.
// .SECTION Description
// vtkXMLKWCaptionWidgetWriter provides XML writing functionality to 
// vtkKWCaptionWidget.
// .SECTION See Also
// vtkXMLCaptionWidgetReader

#ifndef __vtkXMLKWCaptionWidgetWriter_h
#define __vtkXMLKWCaptionWidgetWriter_h

#include "XML/vtkXMLCaptionWidgetWriter.h"

class VTK_EXPORT vtkXMLKWCaptionWidgetWriter : public vtkXMLCaptionWidgetWriter
{
public:
  static vtkXMLKWCaptionWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWCaptionWidgetWriter,vtkXMLCaptionWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLKWCaptionWidgetWriter() {};
  ~vtkXMLKWCaptionWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWCaptionWidgetWriter(const vtkXMLKWCaptionWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWCaptionWidgetWriter&);  // Not implemented.
};

#endif



