/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLSideAnnotationReader.h"

#include "vtkSideAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLSideAnnotationReader);
vtkCxxRevisionMacro(vtkXMLSideAnnotationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLSideAnnotationReader::GetRootElementName()
{
  return "SideAnnotation";
}

//----------------------------------------------------------------------------
int vtkXMLSideAnnotationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkSideAnnotation *obj = vtkSideAnnotation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The SideAnnotation is not set!");
    return 0;
    }

  // Get attributes

  const char *cptr;

  cptr = elem->GetAttribute("MinusXLabel");
  if (cptr)
    {
    obj->SetMinusXLabel(cptr);
    }

  cptr = elem->GetAttribute("XLabel");
  if (cptr)
    {
    obj->SetXLabel(cptr);
    }

  cptr = elem->GetAttribute("MinusYLabel");
  if (cptr)
    {
    obj->SetMinusYLabel(cptr);
    }

  cptr = elem->GetAttribute("YLabel");
  if (cptr)
    {
    obj->SetYLabel(cptr);
    }
  
  return 1;
}

