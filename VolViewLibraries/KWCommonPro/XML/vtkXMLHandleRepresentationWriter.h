/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLHandleRepresentationWriter - vtkHandleRepresentation XML Writer.
// .SECTION Description
// vtkXMLHandleRepresentationWriter provides XML writing functionality to 
// vtkHandleRepresentation.
// .SECTION See Also
// vtkXMLHandleRepresentationReader

#ifndef __vtkXMLHandleRepresentationWriter_h
#define __vtkXMLHandleRepresentationWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLHandleRepresentationWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLHandleRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLHandleRepresentationWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLHandleRepresentationWriter() {};
  ~vtkXMLHandleRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLHandleRepresentationWriter(const vtkXMLHandleRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLHandleRepresentationWriter&);  // Not implemented.
};

#endif

