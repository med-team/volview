/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLColorTransferFunctionWriter.h"

#include "vtkObjectFactory.h"
#include "vtkColorTransferFunction.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLColorTransferFunctionWriter);
vtkCxxRevisionMacro(vtkXMLColorTransferFunctionWriter, "$Revision: 1.12 $");

//----------------------------------------------------------------------------
const char* vtkXMLColorTransferFunctionWriter::GetRootElementName()
{
  return "ColorTransferFunction";
}

//----------------------------------------------------------------------------
const char* vtkXMLColorTransferFunctionWriter::GetPointElementName()
{
  return "Point";
}

//----------------------------------------------------------------------------
int vtkXMLColorTransferFunctionWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkColorTransferFunction *obj = 
    vtkColorTransferFunction::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ColorTransferFunction is not set!");
    return 0;
    }

  elem->SetIntAttribute("Size", obj->GetSize());

  elem->SetIntAttribute("Clamping", obj->GetClamping());

  elem->SetIntAttribute("ColorSpace", obj->GetColorSpace());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLColorTransferFunctionWriter::AddNestedElements(
  vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkColorTransferFunction *obj = 
    vtkColorTransferFunction::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ColorTransferFunction is not set!");
    return 0;
    }

  // Iterate over all points and create a point XML data element for each one

  int size = obj->GetSize();

#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
  double val[6];
  for (int i = 0; i < size; ++i)
    {
    if (obj->GetNodeValue(i, val))
      {
      vtkXMLDataElement *point_elem = this->NewDataElement();
      elem->AddNestedElement(point_elem);
      point_elem->Delete();
      point_elem->SetName(this->GetPointElementName());
      point_elem->SetDoubleAttribute("X", val[0]);
      point_elem->SetVectorAttribute("Value", 3, val + 1);
      point_elem->SetDoubleAttribute("MidPoint", val[4]);
      point_elem->SetDoubleAttribute("Sharpness", val[5]);
      }
    }
#else
  double *val = obj->GetDataPointer();
  for (int i = 0; i < size; ++i, val += 4)
    {
    vtkXMLDataElement *point_elem = this->NewDataElement();
    elem->AddNestedElement(point_elem);
    point_elem->Delete();
    point_elem->SetName(this->GetPointElementName());
    point_elem->SetDoubleAttribute("X", val[0]);
    point_elem->SetVectorAttribute("Value", 3, val + 1);
    }
#endif

  return 1;
}


