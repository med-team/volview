/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBiDimensionalRepresentation2DReader.h"

#include "vtkObjectFactory.h"
#include "vtkBiDimensionalRepresentation2D.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty2D.h"
#include "vtkTextProperty.h"

vtkStandardNewMacro(vtkXMLBiDimensionalRepresentation2DReader);
vtkCxxRevisionMacro(vtkXMLBiDimensionalRepresentation2DReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBiDimensionalRepresentation2DReader::GetRootElementName()
{
  return "BiDimensionalRepresentation2D";
}

//----------------------------------------------------------------------------
int vtkXMLBiDimensionalRepresentation2DReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkBiDimensionalRepresentation2D *obj = 
    vtkBiDimensionalRepresentation2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BiDimensionalRepresentation2D is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;
  const char *cptr;
  unsigned long ulval;

  if (elem->GetVectorAttribute("Point1WorldPosition", 3, dbuffer3) == 3)
    {
    obj->SetPoint1WorldPosition(dbuffer3);
    }

  if (elem->GetVectorAttribute("Point2WorldPosition", 3, dbuffer3) == 3)
    {
    obj->SetPoint2WorldPosition(dbuffer3);
    }

  if (elem->GetVectorAttribute("Point3WorldPosition", 3, dbuffer3) == 3)
    {
    obj->SetPoint3WorldPosition(dbuffer3);
    }

  if (elem->GetVectorAttribute("Point4WorldPosition", 3, dbuffer3) == 3)
    {
    obj->SetPoint4WorldPosition(dbuffer3);
    }

  if (elem->GetScalarAttribute("Line1Visibility", ival))
    {
    obj->SetLine1Visibility(ival);
    }

  if (elem->GetScalarAttribute("Line2Visibility", ival))
    {
    obj->SetLine2Visibility(ival);
    }

  if (elem->GetScalarAttribute("Tolerance", ival))
    {
    obj->SetTolerance(ival);
    }

  cptr = elem->GetAttribute("LabelFormat");
  if (cptr)
    {
    obj->SetLabelFormat(cptr);
    }
  
  if (elem->GetScalarAttribute("ShowLabelAboveWidget", ival))
    {
    obj->SetShowLabelAboveWidget(ival);
    }

  if (elem->GetScalarAttribute("ID", ulval))
    {
    obj->SetID(ulval);
    }

  if (elem->GetVectorAttribute("WorldLabelPosition", 3, dbuffer3) == 3)
    {
    }

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    obj->GetLineProperty()->SetColor(dbuffer3);
    obj->GetTextProperty()->SetColor(obj->GetLineProperty()->GetColor());
    }

  return 1;
}
