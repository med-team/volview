/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWDistanceWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWDistanceWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkDistanceRepresentation.h"
#include "vtkXMLDistanceRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLKWDistanceWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWDistanceWidgetWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWDistanceWidgetWriter::GetRootElementName()
{
  return "DistanceWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWDistanceWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLKWDistanceWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWDistanceWidget *obj = vtkKWDistanceWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The DistanceWidget is not set!");
    return 0;
    }

  // Representation

  vtkDistanceRepresentation *rep = 
    vtkDistanceRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLDistanceRepresentationWriter *xmlw = 
      vtkXMLDistanceRepresentationWriter::New();
    xmlw->SetObject(rep);
    xmlw->CreateInNestedElement(
      elem, this->GetRepresentationElementName());
    xmlw->Delete();
    }

  return 1;
}

