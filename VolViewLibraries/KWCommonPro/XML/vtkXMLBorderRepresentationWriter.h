/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBorderRepresentationWriter - vtkBorderRepresentation XML Writer.
// .SECTION Description
// vtkXMLBorderRepresentationWriter provides XML writing functionality to 
// vtkBorderRepresentation.
// .SECTION See Also
// vtkXMLBorderRepresentationReader

#ifndef __vtkXMLBorderRepresentationWriter_h
#define __vtkXMLBorderRepresentationWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLBorderRepresentationWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLBorderRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLBorderRepresentationWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the border property.
  static const char* GetBorderPropertyElementName();

protected:
  vtkXMLBorderRepresentationWriter() {};
  ~vtkXMLBorderRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLBorderRepresentationWriter(const vtkXMLBorderRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLBorderRepresentationWriter&);  // Not implemented.
};

#endif



