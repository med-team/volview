/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLDistanceRepresentationReader - vtkDistanceRepresentation XML Reader.
// .SECTION Description
// vtkXMLDistanceRepresentationReader provides XML reading functionality to 
// vtkDistanceRepresentation.
// .SECTION See Also
// vtkXMLDistanceRepresentationWriter

#ifndef __vtkXMLDistanceRepresentationReader_h
#define __vtkXMLDistanceRepresentationReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLDistanceRepresentationReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLDistanceRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLDistanceRepresentationReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLDistanceRepresentationReader() {};
  ~vtkXMLDistanceRepresentationReader() {};

private:
  vtkXMLDistanceRepresentationReader(const vtkXMLDistanceRepresentationReader&); // Not implemented
  void operator=(const vtkXMLDistanceRepresentationReader&); // Not implemented    
};

#endif



