/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionActor2DWriter - vtkCaptionActor2D XML Writer.
// .SECTION Description
// vtkXMLCaptionActor2DWriter provides XML writing functionality to 
// vtkCaptionActor2D.
// .SECTION See Also
// vtkXMLCaptionActor2DReader

#ifndef __vtkXMLCaptionActor2DWriter_h
#define __vtkXMLCaptionActor2DWriter_h

#include "XML/vtkXMLActor2DWriter.h"

class VTK_EXPORT vtkXMLCaptionActor2DWriter : public vtkXMLActor2DWriter
{
public:
  static vtkXMLCaptionActor2DWriter* New();
  vtkTypeRevisionMacro(vtkXMLCaptionActor2DWriter,vtkXMLActor2DWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the properties.
  static const char* GetTextActorElementName();
  static const char* GetCaptionTextPropertyElementName();

protected:
  vtkXMLCaptionActor2DWriter() {};
  ~vtkXMLCaptionActor2DWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLCaptionActor2DWriter(const vtkXMLCaptionActor2DWriter&);  // Not implemented.
  void operator=(const vtkXMLCaptionActor2DWriter&);  // Not implemented.
};

#endif


