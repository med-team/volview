/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBiDimensionalRepresentation2DReader - vtkBiDimensionalRepresentation2D XML Reader.
// .SECTION Description
// vtkXMLBiDimensionalRepresentation2DReader provides XML reading functionality to 
// vtkBiDimensionalRepresentation2D.
// .SECTION See Also
// vtkXMLBiDimensionalRepresentation2DWriter

#ifndef __vtkXMLBiDimensionalRepresentation2DReader_h
#define __vtkXMLBiDimensionalRepresentation2DReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLBiDimensionalRepresentation2DReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLBiDimensionalRepresentation2DReader* New();
  vtkTypeRevisionMacro(vtkXMLBiDimensionalRepresentation2DReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLBiDimensionalRepresentation2DReader() {};
  ~vtkXMLBiDimensionalRepresentation2DReader() {};

private:
  vtkXMLBiDimensionalRepresentation2DReader(const vtkXMLBiDimensionalRepresentation2DReader&); // Not implemented
  void operator=(const vtkXMLBiDimensionalRepresentation2DReader&); // Not implemented    
};

#endif



