/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkXMLKWParser.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkXMLKWParser);
vtkCxxRevisionMacro(vtkXMLKWParser, "1.3" );

//-----------------------------------------------------------------------------
void vtkXMLKWParser::CharacterDataHandler( const char* inData, int inLength )
{
  this->OpenElements[this->NumberOfOpenElements - 1]->AddCharacterData(
    inData, inLength);
}

//-----------------------------------------------------------------------------
void vtkXMLKWParser::PrintSelf(ostream &os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

