/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLPiecewiseFunctionReader.h"

#include "vtkPiecewiseFunction.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLPiecewiseFunctionWriter.h"

vtkStandardNewMacro(vtkXMLPiecewiseFunctionReader);
vtkCxxRevisionMacro(vtkXMLPiecewiseFunctionReader, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
const char* vtkXMLPiecewiseFunctionReader::GetRootElementName()
{
  return "PiecewiseFunction";
}

//----------------------------------------------------------------------------
int vtkXMLPiecewiseFunctionReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkPiecewiseFunction *obj = vtkPiecewiseFunction::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The PiecewiseFunction is not set!");
    return 0;
    }

  // Get attributes

  int ival;

  if (elem->GetScalarAttribute("Clamping", ival))
    {
    obj->SetClamping(ival);
    }

  // Get the points

  obj->RemoveAllPoints();

  int nb_nested_elems = elem->GetNumberOfNestedElements();
  for (int idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);
    if (!strcmp(nested_elem->GetName(), 
                vtkXMLPiecewiseFunctionWriter::GetPointElementName()))
      {
      double x, val;
      if (nested_elem->GetScalarAttribute("X", x) &&
          nested_elem->GetScalarAttribute("Value", val))
        {
#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
        double midpoint, sharpness;
        if (nested_elem->GetScalarAttribute("MidPoint", midpoint) &&
            nested_elem->GetScalarAttribute("Sharpness", sharpness))
          {
          obj->AddPoint(x, val, midpoint, sharpness);
          }
        else
#endif
          {
          obj->AddPoint(x, val);
          }
        }
      }
    }
  
  return 1;
}


