/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLImplicitPlaneWidgetReader - vtkImplicitPlaneWidget XML Reader.
// .SECTION Description
// vtkXMLImplicitPlaneWidgetReader provides XML reading functionality to 
// vtkImplicitPlaneWidget.
// .SECTION See Also
// vtkXMLImplicitPlaneWidgetWriter

#ifndef __vtkXMLImplicitPlaneWidgetReader_h
#define __vtkXMLImplicitPlaneWidgetReader_h

#include "XML/vtkXMLPolyDataSourceWidgetReader.h"

class VTK_EXPORT vtkXMLImplicitPlaneWidgetReader : public vtkXMLPolyDataSourceWidgetReader
{
public:
  static vtkXMLImplicitPlaneWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLImplicitPlaneWidgetReader, vtkXMLPolyDataSourceWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLImplicitPlaneWidgetReader() {};
  ~vtkXMLImplicitPlaneWidgetReader() {};

private:
  vtkXMLImplicitPlaneWidgetReader(const vtkXMLImplicitPlaneWidgetReader&); // Not implemented
  void operator=(const vtkXMLImplicitPlaneWidgetReader&); // Not implemented    
};

#endif


