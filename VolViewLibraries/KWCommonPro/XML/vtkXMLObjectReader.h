/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLObjectReader - XML Object Reader.
// .SECTION Description
// vtkXMLObjectReader provides base functionalities for all XML readers.
// .SECTION See Also
// vtkXMLObjectWriter

#ifndef __vtkXMLObjectReader_h
#define __vtkXMLObjectReader_h

#include "XML/vtkXMLIOBase.h"

class vtkXMLDataElement;
class vtkXMLKWParser;

class VTK_EXPORT vtkXMLObjectReader : public vtkXMLIOBase
{
public:
  vtkTypeRevisionMacro(vtkXMLObjectReader,vtkXMLIOBase);

  // Description:
  // Parse an XML tree and modify the object accordingly.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Parse an XML stream.
  // Return 1 on success, 0 on error.
  virtual int ParseStream(istream&);

  // Description:
  // Parse an XML string.
  // Return 1 on success, 0 on error.
  virtual int ParseString(const char*);

  // Description:
  // Parse an XML file.
  // Return 1 on success, 0 on error.
  virtual int ParseFile(const char*);

  // Description:
  // Convenience method to look in 'parent' for an XML element matching 
  // the current root name and parse that element accordingly (see Parse()).
  // IsInElement() does not actually parse, but checks if the
  // element can be found on the path described previously.
  // Return a pointer to the element matching the current root name on success,
  // NULL on error (ParseInElement).
  // Return 1 on success, 0 on error (IsInElement).
  virtual vtkXMLDataElement* ParseInElement(vtkXMLDataElement *parent);
  virtual int IsInElement(vtkXMLDataElement *parent);

  // Description:
  // Convenience method to look in 'grandparent' for an XML 'parent' element
  // matching the 'name', then look inside 'parent' for an XML element matching
  // the current root name and parse that element accordingly 
  // (see ParseInElement()).
  // IsInNestedElement() does not actually parse, but checks if the
  // element can be found on the path described previously.
  // Return a pointer to the element matching the current root name on success,
  // NULL on error (ParseInNestedElement).
  // Return 1 on success, 0 on error (IsInNestedElement).
  virtual vtkXMLDataElement* ParseInNestedElement(
    vtkXMLDataElement *grandparent, const char *name);
  virtual int IsInNestedElement(
    vtkXMLDataElement *grandparent, const char *name);

  // Description:
  // Return the name of the attribute used in the root element of the tree
  // to store the name of the file the tree was read from..
  static const char* GetParsedFromFileAttributeName();

protected:
  vtkXMLObjectReader();
  ~vtkXMLObjectReader();  

  // Description:
  // Create and destroy the XML parser.
  virtual void CreateXMLParser();
  virtual void DestroyXMLParser();
  
  // Description:
  // InitializeParsing is called before each call to Parse(element).
  virtual int InitializeParsing();

  // A vtkXMLKWParser instance used to hide XML reading details
  // and create an XML tree from an XML stream.

  vtkXMLKWParser* XMLParser;

  // Store the element that was parsed by Parse()

  vtkXMLDataElement *LastParsedElement;

private:
  vtkXMLObjectReader(const vtkXMLObjectReader&);  // Not implemented.
  void operator=(const vtkXMLObjectReader&);  // Not implemented.
};

#endif


