/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLImplicitPlaneWidgetWriter - vtkImplicitPlaneWidget XML Writer.
// .SECTION Description
// vtkXMLImplicitPlaneWidgetWriter provides XML writing functionality to 
// vtkImplicitPlaneWidget.
// .SECTION See Also
// vtkXMLImplicitPlaneWidgetReader

#ifndef __vtkXMLImplicitPlaneWidgetWriter_h
#define __vtkXMLImplicitPlaneWidgetWriter_h

#include "XML/vtkXMLPolyDataSourceWidgetWriter.h"

class VTK_EXPORT vtkXMLImplicitPlaneWidgetWriter : public vtkXMLPolyDataSourceWidgetWriter
{
public:
  static vtkXMLImplicitPlaneWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLImplicitPlaneWidgetWriter,vtkXMLPolyDataSourceWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the properties.
  static const char* GetPlanePropertyElementName();
  static const char* GetSelectedPlanePropertyElementName();
  static const char* GetNormalPropertyElementName();
  static const char* GetSelectedNormalPropertyElementName();
  static const char* GetOutlinePropertyElementName();
  static const char* GetSelectedOutlinePropertyElementName();
  static const char* GetEdgesPropertyElementName();

protected:
  vtkXMLImplicitPlaneWidgetWriter() {};
  ~vtkXMLImplicitPlaneWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLImplicitPlaneWidgetWriter(const vtkXMLImplicitPlaneWidgetWriter&);  
  // Not implemented.
  void operator=(const vtkXMLImplicitPlaneWidgetWriter&);  
  // Not implemented.
};

#endif


