/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWBiDimensionalWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWBiDimensionalWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkBiDimensionalRepresentation2D.h"

#include "XML/vtkXMLBiDimensionalRepresentation2DWriter.h"

vtkStandardNewMacro(vtkXMLKWBiDimensionalWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWBiDimensionalWidgetWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWBiDimensionalWidgetWriter::GetRootElementName()
{
  return "BiDimensionalWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWBiDimensionalWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLKWBiDimensionalWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWBiDimensionalWidget *obj = 
    vtkKWBiDimensionalWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BiDimensionalWidget is not set!");
    return 0;
    }

  // Representation

  vtkBiDimensionalRepresentation2D *rep = 
    vtkBiDimensionalRepresentation2D::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLBiDimensionalRepresentation2DWriter *xmlw = 
      vtkXMLBiDimensionalRepresentation2DWriter::New();
    xmlw->SetObject(rep);
    xmlw->CreateInNestedElement(
      elem, this->GetRepresentationElementName());
    xmlw->Delete();
    }

  return 1;
}

