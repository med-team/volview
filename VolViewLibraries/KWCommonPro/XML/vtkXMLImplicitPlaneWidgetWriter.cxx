/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLImplicitPlaneWidgetWriter.h"

#include "vtkImplicitPlaneWidget.h"
#include "vtkObjectFactory.h"
#include "vtkProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLPropertyWriter.h"

vtkStandardNewMacro(vtkXMLImplicitPlaneWidgetWriter);
vtkCxxRevisionMacro(vtkXMLImplicitPlaneWidgetWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetRootElementName()
{
  return "ImplicitPlaneWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetPlanePropertyElementName()
{
  return "PlaneProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetSelectedPlanePropertyElementName()
{
  return "SelectedPlaneProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetNormalPropertyElementName()
{
  return "NormalProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetSelectedNormalPropertyElementName()
{
  return "SelectedNormalProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetOutlinePropertyElementName()
{
  return "OutlineProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetSelectedOutlinePropertyElementName()
{
  return "SelectedOutlineProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetWriter::GetEdgesPropertyElementName()
{
  return "EdgesProperty";
}

//----------------------------------------------------------------------------
int vtkXMLImplicitPlaneWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkImplicitPlaneWidget *obj = vtkImplicitPlaneWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ImplicitPlaneWidget is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Origin", 3, obj->GetOrigin());

  elem->SetVectorAttribute("Normal", 3, obj->GetNormal());

  elem->SetIntAttribute("NormalToXAxis", obj->GetNormalToXAxis());

  elem->SetIntAttribute("NormalToYAxis", obj->GetNormalToYAxis());

  elem->SetIntAttribute("NormalToZAxis", obj->GetNormalToZAxis());

  elem->SetIntAttribute("Tubing", obj->GetTubing());

  elem->SetIntAttribute("DrawPlane", obj->GetDrawPlane());

  elem->SetIntAttribute("OutlineTranslation", obj->GetOutlineTranslation());

  elem->SetIntAttribute("OutsideBounds", obj->GetOutsideBounds());

  elem->SetIntAttribute("ScaleEnabled", obj->GetScaleEnabled());

  elem->SetIntAttribute("OriginTranslation", obj->GetOriginTranslation());

  elem->SetDoubleAttribute("DiagonalRatio", obj->GetDiagonalRatio());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLImplicitPlaneWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkImplicitPlaneWidget *obj = 
    vtkImplicitPlaneWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ImplicitPlaneWidget is not set!");
    return 0;
    }

  // Plane Properties

  vtkXMLPropertyWriter *xmlw = vtkXMLPropertyWriter::New();
  vtkProperty *prop;

  prop = obj->GetNormalProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(elem, this->GetNormalPropertyElementName());
    }

  prop = obj->GetSelectedNormalProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(
      elem, this->GetSelectedNormalPropertyElementName());
    }

  prop = obj->GetPlaneProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(elem, this->GetPlanePropertyElementName());
    }
 
  prop = obj->GetSelectedPlaneProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(
      elem, this->GetSelectedPlanePropertyElementName());
    }

  prop = obj->GetOutlineProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(
      elem, this->GetOutlinePropertyElementName());
    }

  prop = obj->GetSelectedOutlineProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(
      elem, this->GetSelectedOutlinePropertyElementName());
    }

  prop = obj->GetEdgesProperty();
  if (prop)
    {
    xmlw->SetObject(prop);
    xmlw->CreateInNestedElement(
      elem, this->GetEdgesPropertyElementName());
    }

  xmlw->Delete();

  return 1;
}


