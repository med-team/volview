/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBoundingBoxAnnotationReader - vtkBoundingBoxAnnotation XML Reader.
// .SECTION Description
// vtkXMLBoundingBoxAnnotationReader provides XML reading functionality to 
// vtkBoundingBoxAnnotation.
// .SECTION See Also
// vtkXMLBoundingBoxAnnotationWriter

#ifndef __vtkXMLBoundingBoxAnnotationReader_h
#define __vtkXMLBoundingBoxAnnotationReader_h

#include "XML/vtkXMLActorReader.h"

class VTK_EXPORT vtkXMLBoundingBoxAnnotationReader : public vtkXMLActorReader
{
public:
  static vtkXMLBoundingBoxAnnotationReader* New();
  vtkTypeRevisionMacro(vtkXMLBoundingBoxAnnotationReader, vtkXMLActorReader);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLBoundingBoxAnnotationReader() {};
  ~vtkXMLBoundingBoxAnnotationReader() {};

private:
  vtkXMLBoundingBoxAnnotationReader(const vtkXMLBoundingBoxAnnotationReader&); // Not implemented
  void operator=(const vtkXMLBoundingBoxAnnotationReader&); // Not implemented    
};

#endif
