/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBiDimensionalRepresentation2DWriter.h"

#include "vtkObjectFactory.h"
#include "vtkBiDimensionalRepresentation2D.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty2D.h"

vtkStandardNewMacro(vtkXMLBiDimensionalRepresentation2DWriter);
vtkCxxRevisionMacro(vtkXMLBiDimensionalRepresentation2DWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBiDimensionalRepresentation2DWriter::GetRootElementName()
{
  return "BiDimensionalRepresentation2D";
}

//----------------------------------------------------------------------------
int vtkXMLBiDimensionalRepresentation2DWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkBiDimensionalRepresentation2D *obj = 
    vtkBiDimensionalRepresentation2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BiDimensionalRepresentation2D is not set!");
    return 0;
    }

  double pos[3];

  obj->GetPoint1WorldPosition(pos);
  elem->SetVectorAttribute("Point1WorldPosition", 3, pos);

  obj->GetPoint2WorldPosition(pos);
  elem->SetVectorAttribute("Point2WorldPosition", 3, pos);

  obj->GetPoint3WorldPosition(pos);
  elem->SetVectorAttribute("Point3WorldPosition", 3, pos);

  obj->GetPoint4WorldPosition(pos);
  elem->SetVectorAttribute("Point4WorldPosition", 3, pos);

  elem->SetIntAttribute("Line1Visibility", obj->GetLine1Visibility());

  elem->SetIntAttribute("Line2Visibility", obj->GetLine2Visibility());

  elem->SetIntAttribute("Tolerance", obj->GetTolerance());

  elem->SetAttribute("LabelFormat", obj->GetLabelFormat());

  elem->SetIntAttribute("ShowLabelAboveWidget", obj->GetTolerance());

  elem->SetUnsignedLongAttribute("ID", obj->GetID());

  obj->GetWorldLabelPosition(pos);
  elem->SetVectorAttribute("WorldLabelPosition", 3, pos);

  elem->SetVectorAttribute("Color", 3, obj->GetLineProperty()->GetColor());

  return 1;
}
