/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLWidgetRepresentationWriter - vtkWidgetRepresentation XML Writer.
// .SECTION Description
// vtkXMLWidgetRepresentationWriter provides XML writing functionality to 
// vtkWidgetRepresentation.
// .SECTION See Also
// vtkXMLWidgetRepresentationReader

#ifndef __vtkXMLWidgetRepresentationWriter_h
#define __vtkXMLWidgetRepresentationWriter_h

#include "XML/vtkXMLPropWriter.h"

class VTK_EXPORT vtkXMLWidgetRepresentationWriter : public vtkXMLPropWriter
{
public:
  static vtkXMLWidgetRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLWidgetRepresentationWriter,vtkXMLPropWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLWidgetRepresentationWriter() {};
  ~vtkXMLWidgetRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLWidgetRepresentationWriter(const vtkXMLWidgetRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLWidgetRepresentationWriter&);  // Not implemented.
};

#endif



