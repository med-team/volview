/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWBiDimensionalWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWBiDimensionalWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkBiDimensionalRepresentation2D.h"

#include "XML/vtkXMLBiDimensionalRepresentation2DReader.h"
#include "XML/vtkXMLKWBiDimensionalWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWBiDimensionalWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWBiDimensionalWidgetReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWBiDimensionalWidgetReader::GetRootElementName()
{
  return "BiDimensionalWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWBiDimensionalWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWBiDimensionalWidget *obj = vtkKWBiDimensionalWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWBiDimensionalWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  // Representation

  vtkBiDimensionalRepresentation2D *rep = 
    vtkBiDimensionalRepresentation2D::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLBiDimensionalRepresentation2DReader *xmlr = 
      vtkXMLBiDimensionalRepresentation2DReader::New();
    xmlr->SetObject(rep);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWBiDimensionalWidgetWriter::GetRepresentationElementName());
    xmlr->Delete();
    obj->WidgetIsDefined();
    }

  return 1;
}
