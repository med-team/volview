/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionWidgetReader - vtkCaptionWidget XML Reader.
// .SECTION Description
// vtkXMLCaptionWidgetReader provides XML reading functionality to 
// vtkCaptionWidget.
// .SECTION See Also
// vtkXMLCaptionWidgetWriter

#ifndef __vtkXMLCaptionWidgetReader_h
#define __vtkXMLCaptionWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLCaptionWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLCaptionWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLCaptionWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLCaptionWidgetReader() {};
  ~vtkXMLCaptionWidgetReader() {};

private:
  vtkXMLCaptionWidgetReader(const vtkXMLCaptionWidgetReader&); // Not implemented
  void operator=(const vtkXMLCaptionWidgetReader&); // Not implemented    
};

#endif



