/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLWidgetRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkWidgetRepresentation.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLWidgetRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLWidgetRepresentationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLWidgetRepresentationWriter::GetRootElementName()
{
  return "WidgetRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLWidgetRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkWidgetRepresentation *obj = 
    vtkWidgetRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The WidgetRepresentation is not set!");
    return 0;
    }

  elem->SetDoubleAttribute("PlaceFactor", obj->GetPlaceFactor());

  elem->SetDoubleAttribute("HandleSize", obj->GetHandleSize());

  return 1;
}
