/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAxisActor2DWriter - vtkAxisActor2D XML Writer.
// .SECTION Description
// vtkXMLAxisActor2DWriter provides XML writing functionality to 
// vtkAxisActor2D.
// .SECTION See Also
// vtkXMLAxisActor2DReader

#ifndef __vtkXMLAxisActor2DWriter_h
#define __vtkXMLAxisActor2DWriter_h

#include "XML/vtkXMLActor2DWriter.h"

class VTK_EXPORT vtkXMLAxisActor2DWriter : public vtkXMLActor2DWriter
{
public:
  static vtkXMLAxisActor2DWriter* New();
  vtkTypeRevisionMacro(vtkXMLAxisActor2DWriter,vtkXMLActor2DWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the properties.
  static const char* GetTitleTextPropertyElementName();
  static const char* GetLabelTextPropertyElementName();

protected:
  vtkXMLAxisActor2DWriter() {};
  ~vtkXMLAxisActor2DWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLAxisActor2DWriter(const vtkXMLAxisActor2DWriter&);  // Not implemented.
  void operator=(const vtkXMLAxisActor2DWriter&);  // Not implemented.
};

#endif


