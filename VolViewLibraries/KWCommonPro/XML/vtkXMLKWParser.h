/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWParser - subclass of vtkXMLDataParser that handles character data.
// .SECTION Description
// vtkXMLKWParser provides a XML parser.
// .SECTION See Also
// vtkXMLMaterialParser

#ifndef __vtkXMLKWParser_h
#define __vtkXMLKWParser_h

#include "vtkXMLDataParser.h"

class vtkXMLKWParserInternals;

class VTK_EXPORT vtkXMLKWParser : public vtkXMLDataParser
{
public:
  static vtkXMLKWParser *New();
  vtkTypeRevisionMacro(vtkXMLKWParser,vtkXMLDataParser);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkXMLKWParser() {};
  ~vtkXMLKWParser() {};

  // Description:
  // Handle character data
  virtual void CharacterDataHandler(const char* data, int length);

private:
  vtkXMLKWParser(const vtkXMLKWParser&); // Not implemented
  void operator=(const vtkXMLKWParser&); // Not implemented
};
#endif
