/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWCaptionWidgetReader - vtkKWCaptionWidget XML Reader.
// .SECTION Description
// vtkXMLKWCaptionWidgetReader provides XML reading functionality to 
// vtkKWCaptionWidget.
// .SECTION See Also
// vtkXMLKWCaptionWidgetWriter

#ifndef __vtkXMLKWCaptionWidgetReader_h
#define __vtkXMLKWCaptionWidgetReader_h

#include "XML/vtkXMLCaptionWidgetReader.h"

class VTK_EXPORT vtkXMLKWCaptionWidgetReader : public vtkXMLCaptionWidgetReader
{
public:
  static vtkXMLKWCaptionWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWCaptionWidgetReader, vtkXMLCaptionWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWCaptionWidgetReader() {};
  ~vtkXMLKWCaptionWidgetReader() {};

private:
  vtkXMLKWCaptionWidgetReader(const vtkXMLKWCaptionWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWCaptionWidgetReader&); // Not implemented    
};

#endif



