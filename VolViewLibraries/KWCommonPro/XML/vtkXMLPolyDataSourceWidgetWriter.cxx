/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLPolyDataSourceWidgetWriter.h"

#include "vtkPolyDataSourceWidget.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLPolyDataSourceWidgetWriter);
vtkCxxRevisionMacro(vtkXMLPolyDataSourceWidgetWriter, "$Revision: 1.7 $");

//----------------------------------------------------------------------------
const char* vtkXMLPolyDataSourceWidgetWriter::GetRootElementName()
{
  return "PolyDataSourceWidget";
}


