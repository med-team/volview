/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionRepresentationReader - vtkCaptionRepresentation XML Reader.
// .SECTION Description
// vtkXMLCaptionRepresentationReader provides XML reading functionality to 
// vtkCaptionRepresentation.
// .SECTION See Also
// vtkXMLCaptionRepresentationWriter

#ifndef __vtkXMLCaptionRepresentationReader_h
#define __vtkXMLCaptionRepresentationReader_h

#include "XML/vtkXMLBorderRepresentationReader.h"

class VTK_EXPORT vtkXMLCaptionRepresentationReader : public vtkXMLBorderRepresentationReader
{
public:
  static vtkXMLCaptionRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLCaptionRepresentationReader, vtkXMLBorderRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLCaptionRepresentationReader() {};
  ~vtkXMLCaptionRepresentationReader() {};

private:
  vtkXMLCaptionRepresentationReader(const vtkXMLCaptionRepresentationReader&); // Not implemented
  void operator=(const vtkXMLCaptionRepresentationReader&); // Not implemented    
};

#endif



