/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLTextPropertyReader.h"

#include "vtkObjectFactory.h"
#include "vtkTextProperty.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLTextPropertyReader);
vtkCxxRevisionMacro(vtkXMLTextPropertyReader, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLTextPropertyReader::GetRootElementName()
{
  return "TextProperty";
}

//----------------------------------------------------------------------------
int vtkXMLTextPropertyReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkTextProperty *obj = vtkTextProperty::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The TextProperty is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  float fval;
  int ival;

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    obj->SetColor(dbuffer3);
    }

  if (elem->GetScalarAttribute("Opacity", fval))
    {
    obj->SetOpacity(fval);
    }

  if (elem->GetScalarAttribute("FontFamily", ival))
    {
    obj->SetFontFamily(ival);
    }

  if (elem->GetScalarAttribute("FontSize", ival))
    {
    obj->SetFontSize(ival);
    }

  if (elem->GetScalarAttribute("Bold", ival))
    {
    obj->SetBold(ival);
    }

  if (elem->GetScalarAttribute("Italic", ival))
    {
    obj->SetItalic(ival);
    }

  if (elem->GetScalarAttribute("Shadow", ival))
    {
    obj->SetShadow(ival);
    }

  if (elem->GetScalarAttribute("Justification", ival))
    {
    obj->SetJustification(ival);
    }

  if (elem->GetScalarAttribute("VerticalJustification", ival))
    {
    obj->SetVerticalJustification(ival);
    }

  if (elem->GetScalarAttribute("LineOffset", fval))
    {
    obj->SetLineOffset(fval);
    }

  if (elem->GetScalarAttribute("LineSpacing", fval))
    {
    obj->SetLineSpacing(fval);
    }

  return 1;
}


