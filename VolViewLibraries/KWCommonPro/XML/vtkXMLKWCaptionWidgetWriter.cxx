/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCaptionWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWCaptionWidget.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLCaptionRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLKWCaptionWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWCaptionWidgetWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCaptionWidgetWriter::GetRootElementName()
{
  return "KWCaptionWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWCaptionWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLKWCaptionWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWCaptionWidget *obj = vtkKWCaptionWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCaptionWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("UseAnchorPointOpacity", obj->GetUseAnchorPointOpacity());

  elem->SetDoubleAttribute("AccumulatedAnchorOpacityLimit", 
    obj->GetAccumulatedAnchorOpacityLimit());

  return 1;
}

