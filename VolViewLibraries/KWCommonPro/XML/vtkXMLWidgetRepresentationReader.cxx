/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLWidgetRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkWidgetRepresentation.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLWidgetRepresentationReader);
vtkCxxRevisionMacro(vtkXMLWidgetRepresentationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLWidgetRepresentationReader::GetRootElementName()
{
  return "WidgetRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLWidgetRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkWidgetRepresentation *obj = 
    vtkWidgetRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The WidgetRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dval;

  if (elem->GetScalarAttribute("PlaceFactor", dval))
    {
    obj->SetPlaceFactor(dval);
    }

  if (elem->GetScalarAttribute("HandleSize", dval))
    {
    obj->SetHandleSize(dval);
    }

  return 1;
}
