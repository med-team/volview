/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionActor2DWriter.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionActor2D.h"
#include "vtkTextProperty.h"
#include "vtkTextActor.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLTextActorWriter.h"
#include "XML/vtkXMLTextPropertyWriter.h"

vtkStandardNewMacro(vtkXMLCaptionActor2DWriter);
vtkCxxRevisionMacro(vtkXMLCaptionActor2DWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionActor2DWriter::GetRootElementName()
{
  return "CaptionActor2D";
}

//----------------------------------------------------------------------------
const char* vtkXMLCaptionActor2DWriter::GetTextActorElementName()
{
  return "TextActor";
}

//----------------------------------------------------------------------------
const char* vtkXMLCaptionActor2DWriter::GetCaptionTextPropertyElementName()
{
  return "CaptionTextProperty";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionActor2DWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkCaptionActor2D *obj = vtkCaptionActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionActor2D is not set!");
    return 0;
    }

  elem->SetAttribute("Caption", obj->GetCaption());

  int csys;

  csys = obj->GetAttachmentPointCoordinate()->GetCoordinateSystem();
  obj->GetAttachmentPointCoordinate()->SetCoordinateSystemToWorld();
  elem->SetVectorAttribute("AttachmentPointCoordinate", 3, 
                           obj->GetAttachmentPointCoordinate()->GetValue());
  obj->GetAttachmentPointCoordinate()->SetCoordinateSystem(csys);

  elem->SetIntAttribute("Border", obj->GetBorder());

  elem->SetIntAttribute("Leader", obj->GetLeader());

  elem->SetIntAttribute("ThreeDimensionalLeader", 
                        obj->GetThreeDimensionalLeader());

  elem->SetDoubleAttribute("LeaderGlyphSize", obj->GetLeaderGlyphSize());

  elem->SetIntAttribute("MaximumLeaderGlyphSize", 
                        obj->GetMaximumLeaderGlyphSize());

  elem->SetIntAttribute("Padding", obj->GetPadding());

  elem->SetIntAttribute("AttachEdgeOnly", obj->GetAttachEdgeOnly());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLCaptionActor2DWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkCaptionActor2D *obj = vtkCaptionActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionActor2D is not set!");
    return 0;
    }

  // Text Actor

  if (obj->GetTextActor())
    {
    vtkXMLTextActorWriter *xmlw = vtkXMLTextActorWriter::New();
    xmlw->SetObject(obj->GetTextActor());
    xmlw->CreateInNestedElement(elem, this->GetTextActorElementName());
    xmlw->Delete();
    }
 
  // Caption Text Property

  if (obj->GetCaptionTextProperty())
    {
    vtkXMLTextPropertyWriter *xmlw = vtkXMLTextPropertyWriter::New();
    xmlw->SetObject(obj->GetCaptionTextProperty());
    xmlw->CreateInNestedElement(
      elem, this->GetCaptionTextPropertyElementName());
    xmlw->Delete();
    }
 
  return 1;
}


