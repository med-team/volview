/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAxisActor2DReader.h"

#include "vtkObjectFactory.h"
#include "vtkAxisActor2D.h"
#include "vtkTextProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLAxisActor2DWriter.h"
#include "XML/vtkXMLTextPropertyReader.h"

vtkStandardNewMacro(vtkXMLAxisActor2DReader);
vtkCxxRevisionMacro(vtkXMLAxisActor2DReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLAxisActor2DReader::GetRootElementName()
{
  return "AxisActor2D";
}

//----------------------------------------------------------------------------
int vtkXMLAxisActor2DReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkAxisActor2D *obj = vtkAxisActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AxisActor2D is not set!");
    return 0;
    }

  // Get attributes

  int ival;
  double dval;
  double dbuffer2[2];
  const char *cptr;

  if (elem->GetVectorAttribute("Range", 2, dbuffer2) == 2)
    {
    obj->SetRange(dbuffer2);
    }

  if (elem->GetScalarAttribute("NumberOfLabels", ival))
    {
    obj->SetNumberOfLabels(ival);
    }

  cptr = elem->GetAttribute("LabelFormat");
  if (cptr)
    {
    obj->SetLabelFormat(cptr);
    }

  if (elem->GetScalarAttribute("AdjustLabels", ival))
    {
    obj->SetAdjustLabels(ival);
    }
  
  cptr = elem->GetAttribute("Title");
  if (cptr)
    {
    obj->SetTitle(cptr);
    }
  
  if (elem->GetScalarAttribute("TickLength", ival))
    {
    obj->SetTickLength(ival);
    }
  
  if (elem->GetScalarAttribute("NumberOfMinorTicks", ival))
    {
    obj->SetNumberOfMinorTicks(ival);
    }
  
  if (elem->GetScalarAttribute("MinorTickLength", ival))
    {
    obj->SetMinorTickLength(ival);
    }
  
  if (elem->GetScalarAttribute("TickOffset", ival))
    {
    obj->SetTickOffset(ival);
    }
  
  if (elem->GetScalarAttribute("AxisVisibility", ival))
    {
    obj->SetAxisVisibility(ival);
    }
  
  if (elem->GetScalarAttribute("TickVisibility", ival))
    {
    obj->SetTickVisibility(ival);
    }
  
  if (elem->GetScalarAttribute("LabelVisibility", ival))
    {
    obj->SetLabelVisibility(ival);
    }
  
  if (elem->GetScalarAttribute("TitleVisibility", ival))
    {
    obj->SetTitleVisibility(ival);
    }
  
  if (elem->GetScalarAttribute("TitlePosition", dval))
    {
    obj->SetTitlePosition(dval);
    }
  
  if (elem->GetScalarAttribute("FontFactor", dval))
    {
    obj->SetFontFactor(dval);
    }
  
  if (elem->GetScalarAttribute("LabelFactor", dval))
    {
    obj->SetLabelFactor(dval);
    }
  
  // Get nested elements
  
  // Title and label text property

  vtkXMLTextPropertyReader *xmlr = vtkXMLTextPropertyReader::New();

  if (xmlr->IsInNestedElement(
        elem, vtkXMLAxisActor2DWriter::GetTitleTextPropertyElementName()))
    {
    vtkTextProperty *tprop = obj->GetTitleTextProperty();
    if (!tprop)
      {
      tprop = vtkTextProperty::New();
      obj->SetTitleTextProperty(tprop);
      tprop->Delete();
      }
    xmlr->SetObject(tprop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLAxisActor2DWriter::GetTitleTextPropertyElementName());
    }

  if (xmlr->IsInNestedElement(
        elem, vtkXMLAxisActor2DWriter::GetLabelTextPropertyElementName()))
    {
    vtkTextProperty *tprop = obj->GetLabelTextProperty();
    if (!tprop)
      {
      tprop = vtkTextProperty::New();
      obj->SetLabelTextProperty(tprop);
      tprop->Delete();
      }
    xmlr->SetObject(tprop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLAxisActor2DWriter::GetLabelTextPropertyElementName());
    }

  xmlr->Delete();

  return 1;
}


