/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWBiDimensionalWidgetReader - vtkKWBiDimensionalWidget XML Reader.
// .SECTION Description
// vtkXMLKWBiDimensionalWidgetReader provides XML reading functionality to 
// vtkKWBiDimensionalWidget.
// .SECTION See Also
// vtkXMLKWBiDimensionalWidgetWriter

#ifndef __vtkXMLKWBiDimensionalWidgetReader_h
#define __vtkXMLKWBiDimensionalWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLKWBiDimensionalWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLKWBiDimensionalWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWBiDimensionalWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWBiDimensionalWidgetReader() {};
  ~vtkXMLKWBiDimensionalWidgetReader() {};

private:
  vtkXMLKWBiDimensionalWidgetReader(const vtkXMLKWBiDimensionalWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWBiDimensionalWidgetReader&); // Not implemented    
};

#endif



