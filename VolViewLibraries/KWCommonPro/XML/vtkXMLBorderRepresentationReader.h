/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBorderRepresentationReader - vtkBorderRepresentation XML Reader.
// .SECTION Description
// vtkXMLBorderRepresentationReader provides XML reading functionality to 
// vtkBorderRepresentation.
// .SECTION See Also
// vtkXMLBorderRepresentationWriter

#ifndef __vtkXMLBorderRepresentationReader_h
#define __vtkXMLBorderRepresentationReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLBorderRepresentationReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLBorderRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLBorderRepresentationReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLBorderRepresentationReader() {};
  ~vtkXMLBorderRepresentationReader() {};

private:
  vtkXMLBorderRepresentationReader(const vtkXMLBorderRepresentationReader&); // Not implemented
  void operator=(const vtkXMLBorderRepresentationReader&); // Not implemented    
};

#endif



