/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkCaptionRepresentation.h"

#include "XML/vtkXMLCaptionRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLCaptionWidgetWriter);
vtkCxxRevisionMacro(vtkXMLCaptionWidgetWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionWidgetWriter::GetRootElementName()
{
  return "CaptionWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLCaptionWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkCaptionWidget *obj = vtkCaptionWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionWidget is not set!");
    return 0;
    }

  // Representation

  vtkCaptionRepresentation *rep = 
    vtkCaptionRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLCaptionRepresentationWriter *xmlw = 
      vtkXMLCaptionRepresentationWriter::New();
    xmlw->SetObject(rep);
    xmlw->CreateInNestedElement(
      elem, this->GetRepresentationElementName());
    xmlw->Delete();
    }

  return 1;
}

