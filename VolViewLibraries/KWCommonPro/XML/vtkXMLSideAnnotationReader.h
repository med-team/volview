/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLSideAnnotationReader - vtkSideAnnotation XML Reader.
// .SECTION Description
// vtkXMLSideAnnotationReader provides XML reading functionality to 
// vtkSideAnnotation.
// .SECTION See Also
// vtkXMLSideAnnotationWriter

#ifndef __vtkXMLSideAnnotationReader_h
#define __vtkXMLSideAnnotationReader_h

#include "XML/vtkXMLActor2DReader.h"

class VTK_EXPORT vtkXMLSideAnnotationReader : public vtkXMLActor2DReader
{
public:
  static vtkXMLSideAnnotationReader* New();
  vtkTypeRevisionMacro(vtkXMLSideAnnotationReader, vtkXMLActor2DReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLSideAnnotationReader() {};
  ~vtkXMLSideAnnotationReader() {};

private:
  vtkXMLSideAnnotationReader(const vtkXMLSideAnnotationReader&); // Not implemented
  void operator=(const vtkXMLSideAnnotationReader&); // Not implemented    
};

#endif

