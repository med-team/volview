/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLIOBase.h"

#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"
#include "vtkXMLUtilities.h"

#include <vtksys/ios/sstream>

vtkCxxRevisionMacro(vtkXMLIOBase, "$Revision: 1.7 $");

vtkCxxSetObjectMacro(vtkXMLIOBase, Object, vtkObject);

static int vtkXMLIOBaseDefaultCharacterEncoding = VTK_ENCODING_UTF_8;

//----------------------------------------------------------------------------
vtkXMLIOBase::vtkXMLIOBase()
{
  this->Object = 0;
  this->ErrorLog = NULL;
}

//----------------------------------------------------------------------------
vtkXMLIOBase::~vtkXMLIOBase()
{
  this->SetObject(0);
  this->SetErrorLog(NULL);
}

//----------------------------------------------------------------------------
void vtkXMLIOBase::SetDefaultCharacterEncoding(int val)
{
  if (val == vtkXMLIOBaseDefaultCharacterEncoding)
    {
    return;
    }

  if (val < VTK_ENCODING_NONE)
    {
    val = VTK_ENCODING_NONE;
    }
  else if (val > VTK_ENCODING_UNKNOWN)
    {
    val = VTK_ENCODING_UNKNOWN;
    }

  vtkXMLIOBaseDefaultCharacterEncoding = val;
}

//----------------------------------------------------------------------------
int vtkXMLIOBase::GetDefaultCharacterEncoding()
{
  return vtkXMLIOBaseDefaultCharacterEncoding;
}

//----------------------------------------------------------------------------
vtkXMLDataElement* vtkXMLIOBase::NewDataElement()
{
  vtkXMLDataElement *elem = vtkXMLDataElement::New();
  elem->SetAttributeEncoding(vtkXMLIOBaseDefaultCharacterEncoding);
  return elem;
}

//----------------------------------------------------------------------------
void vtkXMLIOBase::AppendToErrorLog(const char *msg)
{
  vtksys_ios::ostringstream str;
  if (this->ErrorLog)
    {
    str << this->ErrorLog << endl;
    }
  str << msg;
  this->SetErrorLog(str.str().c_str());
}

//----------------------------------------------------------------------------
void vtkXMLIOBase::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  if (this->Object)
    {
    os << indent << "Object: " << this->Object << "\n";
    }
  else
    {
    os << indent << "Object: (none)\n";
    }

  os << indent << "ErrorLog: " 
     << (this->ErrorLog ? this->ErrorLog : "(none)") << endl;
}
