/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLIOBaseMacros - Base XML I/O macros.
// .SECTION Description
// vtkXMLIOBaseMacros provides macros to help declaring XML readers/writers.
// .SECTION See Also
// vtkXMLObjectReader vtkXMLObjectWriter

#ifndef __vtkXMLIOBaseMacros_h
#define __vtkXMLIOBaseMacros_h

#include "vtkKWCommonProConfigure.h" // Needed for KWCommonPro_USE_XML_RW

#ifdef KWCommonPro_USE_XML_RW
class vtkXMLObjectReader;
class vtkXMLObjectWriter;
#endif

#ifdef KWCommonPro_USE_XML_RW
#define vtkKWGetXMLReaderObjectMacro()           \
virtual vtkXMLObjectReader* GetNewXMLReader();
#define vtkKWGetXMLWriterObjectMacro()           \
virtual vtkXMLObjectWriter* GetNewXMLWriter();
#define vtkKWGetXMLReaderWriterObjectsMacro()    \
vtkKWGetXMLReaderObjectMacro();                  \
vtkKWGetXMLWriterObjectMacro();                  
#else
#define vtkKWGetXMLReaderObjectMacro()             
#define vtkKWGetXMLWriterObjectMacro()             
#define vtkKWGetXMLReaderWriterObjectsMacro()
#endif

#ifdef KWCommonPro_USE_XML_RW
#define vtkKWGetXMLReaderObjectImplementationMacro(class,readerclass) \
vtkXMLObjectReader* class::GetNewXMLReader()                        \
  {                                                                 \
  readerclass *ptr = readerclass::New();                            \
  ptr->SetObject(this);                                             \
  return ptr;                                                       \
  }                                                                     
#define vtkKWGetXMLWriterObjectImplementationMacro(class,writerclass) \
vtkXMLObjectWriter* class::GetNewXMLWriter()                        \
  {                                                                 \
  writerclass *ptr = writerclass::New();                            \
  ptr->SetObject(this);                                             \
  return ptr;                                                       \
  }                                                             
#define vtkKWGetXMLReaderWriterObjectsImplementationMacro(class,readerclass,writerclass) \
vtkKWGetXMLReaderObjectImplementationMacro(class,readerclass); \
vtkKWGetXMLWriterObjectImplementationMacro(class,writerclass);
#else
#define vtkKWGetXMLReaderObjectImplementationMacro(class,xmlclass)
#define vtkKWGetXMLWriterObjectImplementationMacro(class,xmlclass)
#define vtkKWGetXMLReaderWriterObjectsImplementationMacro(class,readerclass,writerclass)
#endif

#endif


