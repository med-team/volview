/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLHandleRepresentationReader - vtkHandleRepresentation XML Reader.
// .SECTION Description
// vtkXMLHandleRepresentationReader provides XML reading functionality to 
// vtkHandleRepresentation.
// .SECTION See Also
// vtkXMLHandleRepresentationWriter

#ifndef __vtkXMLHandleRepresentationReader_h
#define __vtkXMLHandleRepresentationReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLHandleRepresentationReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLHandleRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLHandleRepresentationReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLHandleRepresentationReader() {};
  ~vtkXMLHandleRepresentationReader() {};

private:
  vtkXMLHandleRepresentationReader(const vtkXMLHandleRepresentationReader&); // Not implemented
  void operator=(const vtkXMLHandleRepresentationReader&); // Not implemented    
};

#endif

