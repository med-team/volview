/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWAngleWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWAngleWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkAngleRepresentation.h"

#include "XML/vtkXMLAngleRepresentationReader.h"
#include "XML/vtkXMLKWAngleWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWAngleWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWAngleWidgetReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWAngleWidgetReader::GetRootElementName()
{
  return "AngleWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWAngleWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWAngleWidget *obj = vtkKWAngleWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWAngleWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  // Representation

  vtkAngleRepresentation *rep = 
    vtkAngleRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLAngleRepresentationReader *xmlr = 
      vtkXMLAngleRepresentationReader::New();
    xmlr->SetObject(rep);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWAngleWidgetWriter::GetRepresentationElementName());
    xmlr->Delete();
    obj->WidgetIsDefined();
    }

  return 1;
}
