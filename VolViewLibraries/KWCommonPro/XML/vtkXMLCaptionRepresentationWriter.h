/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionRepresentationWriter - vtkCaptionRepresentation XML Writer.
// .SECTION Description
// vtkXMLCaptionRepresentationWriter provides XML writing functionality to 
// vtkCaptionRepresentation.
// .SECTION See Also
// vtkXMLCaptionRepresentationReader

#ifndef __vtkXMLCaptionRepresentationWriter_h
#define __vtkXMLCaptionRepresentationWriter_h

#include "XML/vtkXMLBorderRepresentationWriter.h"

class VTK_EXPORT vtkXMLCaptionRepresentationWriter : public vtkXMLBorderRepresentationWriter
{
public:
  static vtkXMLCaptionRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLCaptionRepresentationWriter,vtkXMLBorderRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the caption actor 2D.
  static const char* GetCaptionActor2DElementName();

protected:
  vtkXMLCaptionRepresentationWriter() {};
  ~vtkXMLCaptionRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLCaptionRepresentationWriter(const vtkXMLCaptionRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLCaptionRepresentationWriter&);  // Not implemented.
};

#endif



