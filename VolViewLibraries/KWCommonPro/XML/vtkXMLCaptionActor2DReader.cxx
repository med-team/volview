/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionActor2DReader.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionActor2D.h"
#include "vtkTextProperty.h"
#include "vtkTextActor.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLCaptionActor2DWriter.h"
#include "XML/vtkXMLTextPropertyReader.h"
#include "XML/vtkXMLTextActorReader.h"

vtkStandardNewMacro(vtkXMLCaptionActor2DReader);
vtkCxxRevisionMacro(vtkXMLCaptionActor2DReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionActor2DReader::GetRootElementName()
{
  return "CaptionActor2D";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionActor2DReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkCaptionActor2D *obj = vtkCaptionActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionActor2D is not set!");
    return 0;
    }

  // Get attributes

  int ival;
  double dval;
  double dbuffer3[3];
  const char *cptr;

  cptr = elem->GetAttribute("Caption");
  if (cptr)
    {
    obj->SetCaption(cptr);
    }

  if (elem->GetVectorAttribute("AttachmentPointCoordinate", 3, dbuffer3) == 3)
    {
    int s = obj->GetAttachmentPointCoordinate()->GetCoordinateSystem();
    obj->GetAttachmentPointCoordinate()->SetCoordinateSystemToWorld();
    obj->GetAttachmentPointCoordinate()->SetValue(dbuffer3);
    obj->GetAttachmentPointCoordinate()->SetCoordinateSystem(s);
    }

  if (elem->GetScalarAttribute("Border", ival))
    {
    obj->SetBorder(ival);
    }

  if (elem->GetScalarAttribute("Leader", ival))
    {
    obj->SetLeader(ival);
    }
  
  if (elem->GetScalarAttribute("ThreeDimensionalLeader", ival))
    {
    obj->SetThreeDimensionalLeader(ival);
    }
  
  if (elem->GetScalarAttribute("LeaderGlyphSize", dval))
    {
    obj->SetLeaderGlyphSize(dval);
    }
  
  if (elem->GetScalarAttribute("MaximumLeaderGlyphSize", ival))
    {
    obj->SetMaximumLeaderGlyphSize(ival);
    }
  
  if (elem->GetScalarAttribute("Padding", ival))
    {
    obj->SetPadding(ival);
    }
  
  if (elem->GetScalarAttribute("AttachEdgeOnly", ival))
    {
    obj->SetAttachEdgeOnly(ival);
    }
  
  // Get nested elements
  
  // Text Actor

  if (obj->GetTextActor())
    {
    vtkXMLTextActorReader *xmlr = vtkXMLTextActorReader::New();
    xmlr->SetObject(obj->GetTextActor());
    xmlr->ParseInNestedElement(
      elem, vtkXMLCaptionActor2DWriter::GetTextActorElementName());
    xmlr->Delete();
    }

  // Caption Text Property

  if (obj->GetCaptionTextProperty())
    {
    vtkXMLTextPropertyReader *xmlr = vtkXMLTextPropertyReader::New();
    xmlr->SetObject(obj->GetCaptionTextProperty());
    xmlr->ParseInNestedElement(
      elem, vtkXMLCaptionActor2DWriter::GetCaptionTextPropertyElementName());
    xmlr->Delete();
    }

  return 1;
}
