/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWAngleWidgetReader - vtkKWAngleWidget XML Reader.
// .SECTION Description
// vtkXMLKWAngleWidgetReader provides XML reading functionality to 
// vtkKWAngleWidget.
// .SECTION See Also
// vtkXMLKWAngleWidgetWriter

#ifndef __vtkXMLKWAngleWidgetReader_h
#define __vtkXMLKWAngleWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLKWAngleWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLKWAngleWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWAngleWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWAngleWidgetReader() {};
  ~vtkXMLKWAngleWidgetReader() {};

private:
  vtkXMLKWAngleWidgetReader(const vtkXMLKWAngleWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWAngleWidgetReader&); // Not implemented    
};

#endif



