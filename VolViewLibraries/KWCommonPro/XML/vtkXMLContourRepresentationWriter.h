/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLContourRepresentationWriter - vtkContourRepresentation XML Writer.
// .SECTION Description
// vtkXMLContourRepresentationWriter provides XML writing functionality to 
// vtkContourRepresentation.
// .SECTION See Also
// vtkXMLContourRepresentationReader

#ifndef __vtkXMLContourRepresentationWriter_h
#define __vtkXMLContourRepresentationWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLContourRepresentationWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLContourRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLContourRepresentationWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store a node.
  static const char* GetNodeElementName();
  static const char* GetIntermediatePointElementName();

protected:
  vtkXMLContourRepresentationWriter() {};
  ~vtkXMLContourRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLContourRepresentationWriter(const vtkXMLContourRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLContourRepresentationWriter&);  // Not implemented.
};

#endif



