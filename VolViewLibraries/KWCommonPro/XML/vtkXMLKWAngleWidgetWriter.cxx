/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWAngleWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWAngleWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkAngleRepresentation.h"

#include "XML/vtkXMLAngleRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLKWAngleWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWAngleWidgetWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWAngleWidgetWriter::GetRootElementName()
{
  return "AngleWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWAngleWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLKWAngleWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWAngleWidget *obj = vtkKWAngleWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AngleWidget is not set!");
    return 0;
    }

  // Representation

  vtkAngleRepresentation *rep = 
    vtkAngleRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLAngleRepresentationWriter *xmlw = 
      vtkXMLAngleRepresentationWriter::New();
    xmlw->SetObject(rep);
    xmlw->CreateInNestedElement(
      elem, this->GetRepresentationElementName());
    xmlw->Delete();
    }

  return 1;
}

