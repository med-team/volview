/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAngleRepresentationReader - vtkAngleRepresentation XML Reader.
// .SECTION Description
// vtkXMLAngleRepresentationReader provides XML reading functionality to 
// vtkAngleRepresentation.
// .SECTION See Also
// vtkXMLAngleRepresentationWriter

#ifndef __vtkXMLAngleRepresentationReader_h
#define __vtkXMLAngleRepresentationReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLAngleRepresentationReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLAngleRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLAngleRepresentationReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLAngleRepresentationReader() {};
  ~vtkXMLAngleRepresentationReader() {};

private:
  vtkXMLAngleRepresentationReader(const vtkXMLAngleRepresentationReader&); // Not implemented
  void operator=(const vtkXMLAngleRepresentationReader&); // Not implemented    
};

#endif



