/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLMedicalImagePropertiesWriter - vtkMedicalImageProperties XML Writer.
// .SECTION Description
// vtkXMLMedicalImagePropertiesWriter provides XML writing functionality to 
// vtkMedicalImageProperties.
// .SECTION See Also
// vtkXMLMedicalImagePropertiesReader

#ifndef __vtkXMLMedicalImagePropertiesWriter_h
#define __vtkXMLMedicalImagePropertiesWriter_h

#include "XML/vtkXMLObjectWriter.h"

class VTK_EXPORT vtkXMLMedicalImagePropertiesWriter : public vtkXMLObjectWriter
{
public:
  static vtkXMLMedicalImagePropertiesWriter* New();
  vtkTypeRevisionMacro(vtkXMLMedicalImagePropertiesWriter,vtkXMLObjectWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the user defined values.
  static const char* GetUserDefinedValuesElementName();
  static const char* GetUserDefinedValueElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the window level presets.
  static const char* GetWindowLevelPresetsElementName();
  static const char* GetWindowLevelPresetElementName();

protected:
  vtkXMLMedicalImagePropertiesWriter() {};
  ~vtkXMLMedicalImagePropertiesWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLMedicalImagePropertiesWriter(const vtkXMLMedicalImagePropertiesWriter&);  // Not implemented.
  void operator=(const vtkXMLMedicalImagePropertiesWriter&);  // Not implemented.
};

#endif

