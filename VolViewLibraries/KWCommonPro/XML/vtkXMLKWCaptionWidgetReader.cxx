/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWCaptionWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWCaptionWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkCaptionRepresentation.h"

#include "XML/vtkXMLCaptionRepresentationReader.h"

vtkStandardNewMacro(vtkXMLKWCaptionWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWCaptionWidgetReader, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWCaptionWidgetReader::GetRootElementName()
{
  return "KWCaptionWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWCaptionWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWCaptionWidget *obj = vtkKWCaptionWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWCaptionWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  int ival;
  if (elem->GetScalarAttribute("UseAnchorPointOpacity", ival))
    {
    obj->SetUseAnchorPointOpacity(ival);
    }
  
  double dval;
  if (elem->GetScalarAttribute("AccumulatedAnchorOpacityLimit", dval))
    {
    obj->SetAccumulatedAnchorOpacityLimit(dval);
    }
  
  // Representation

  vtkCaptionRepresentation *rep = 
    vtkCaptionRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    obj->WidgetIsDefined();
    }

  return 1;
}
