/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBiDimensionalRepresentation2DWriter - vtkBiDimensionalRepresentation2D XML Writer.
// .SECTION Description
// vtkXMLBiDimensionalRepresentation2DWriter provides XML writing functionality to 
// vtkBiDimensionalRepresentation2D.
// .SECTION See Also
// vtkXMLBiDimensionalRepresentation2DReader

#ifndef __vtkXMLBiDimensionalRepresentation2DWriter_h
#define __vtkXMLBiDimensionalRepresentation2DWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLBiDimensionalRepresentation2DWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLBiDimensionalRepresentation2DWriter* New();
  vtkTypeRevisionMacro(vtkXMLBiDimensionalRepresentation2DWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLBiDimensionalRepresentation2DWriter() {};
  ~vtkXMLBiDimensionalRepresentation2DWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLBiDimensionalRepresentation2DWriter(const vtkXMLBiDimensionalRepresentation2DWriter&);  // Not implemented.
  void operator=(const vtkXMLBiDimensionalRepresentation2DWriter&);  // Not implemented.
};

#endif



