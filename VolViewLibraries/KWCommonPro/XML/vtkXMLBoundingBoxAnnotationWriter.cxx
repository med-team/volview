/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBoundingBoxAnnotationWriter.h"

#include "vtkBoundingBoxAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkProperty.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLBoundingBoxAnnotationWriter);
vtkCxxRevisionMacro(vtkXMLBoundingBoxAnnotationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBoundingBoxAnnotationWriter::GetRootElementName()
{
  return "BoundingBoxAnnotation";
}
