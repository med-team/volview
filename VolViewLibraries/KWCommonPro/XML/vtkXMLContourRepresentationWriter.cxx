/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLContourRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkContourRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty.h"
#include "vtkProperty2D.h"
#include "vtkOrientedGlyphContourRepresentation.h"
#include "vtkOrientedGlyphFocalPlaneContourRepresentation.h"

vtkStandardNewMacro(vtkXMLContourRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLContourRepresentationWriter, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLContourRepresentationWriter::GetRootElementName()
{
  return "ContourRepresentation";
}

//----------------------------------------------------------------------------
const char* vtkXMLContourRepresentationWriter::GetNodeElementName()
{
  return "Node";
}

//----------------------------------------------------------------------------
const char* vtkXMLContourRepresentationWriter::GetIntermediatePointElementName()
{
  return "IntermediatePoint";
}

//----------------------------------------------------------------------------
int vtkXMLContourRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkContourRepresentation *obj = 
    vtkContourRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ContourRepresentation is not set!");
    return 0;
    }

  elem->SetIntAttribute("PixelTolerance", obj->GetPixelTolerance());

  elem->SetDoubleAttribute("WorldTolerance", obj->GetWorldTolerance());

  elem->SetIntAttribute("ClosedLoop", obj->GetClosedLoop());

  vtkOrientedGlyphContourRepresentation *obj_og =
    vtkOrientedGlyphContourRepresentation::SafeDownCast(obj);
  if (obj_og)
    {
    elem->SetVectorAttribute(
      "Color", 3, obj_og->GetLinesProperty()->GetColor());
    }
  else
    {
    vtkOrientedGlyphFocalPlaneContourRepresentation *obj_ogfp =
      vtkOrientedGlyphFocalPlaneContourRepresentation::SafeDownCast(obj);
    if (obj_ogfp)
      {
      elem->SetVectorAttribute(
        "Color", 3, obj_ogfp->GetLinesProperty()->GetColor());
      }
    }

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLContourRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkContourRepresentation *obj = 
    vtkContourRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ContourRepresentation is not set!");
    return 0;
    }

  // Store each node

  for (int n = 0; n < obj->GetNumberOfNodes(); ++n)
    {
    double pos[3], ori[9], slope[3];
    if (obj->GetNthNodeWorldPosition(n, pos) &&
        obj->GetNthNodeWorldOrientation(n, ori) &&
        obj->GetNthNodeSlope(n, slope))
      {
      vtkXMLDataElement *node_elem = this->NewDataElement();
      elem->AddNestedElement(node_elem);
      node_elem->Delete();
      node_elem->SetName(this->GetNodeElementName());
      node_elem->SetVectorAttribute("WorldPosition", 3, pos);
      node_elem->SetVectorAttribute("WorldOrientation", 9, ori);
      /* Not needed
      node_elem->SetVectorAttribute("Slope", 3, slope);
      for (int i = 0; i < obj->GetNumberOfIntermediatePoints(n); ++i)
        {
        double ipos[3];
        if (obj->GetIntermediatePointWorldPosition(n, i, ipos))
          {
          vtkXMLDataElement *ipoint_elem = this->NewDataElement();
          node_elem->AddNestedElement(ipoint_elem);
          ipoint_elem->Delete();
          ipoint_elem->SetName(this->GetIntermediatePointElementName());
          ipoint_elem->SetVectorAttribute("WorldPosition", 3, ipos);
          }
        }
      */
      }
    }

  return 1;
}


