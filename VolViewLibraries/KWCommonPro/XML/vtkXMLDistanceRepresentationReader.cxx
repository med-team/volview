/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLDistanceRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkDistanceRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkDistanceRepresentation2D.h"
#include "vtkAxisActor2D.h"
#include "vtkHandleRepresentation.h"
#include "vtkProperty2D.h"
#include "vtkTextProperty.h"

#include "XML/vtkXMLDistanceRepresentationWriter.h"
#include "XML/vtkXMLAxisActor2DReader.h"

vtkStandardNewMacro(vtkXMLDistanceRepresentationReader);
vtkCxxRevisionMacro(vtkXMLDistanceRepresentationReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLDistanceRepresentationReader::GetRootElementName()
{
  return "DistanceRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLDistanceRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkDistanceRepresentation *obj = 
    vtkDistanceRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The DistanceRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;
  const char *cptr;

  vtkDistanceRepresentation2D *rep2d = 
    vtkDistanceRepresentation2D::SafeDownCast(obj);
  vtkAxisActor2D *axis2d = rep2d ? rep2d->GetAxis() : NULL;

  if (elem->GetVectorAttribute("Point1WorldPosition", 3, dbuffer3) == 3)
    {
    obj->GetPoint1Representation()->SetWorldPosition(dbuffer3);
    if (axis2d)
      {
      int s = axis2d->GetPoint1Coordinate()->GetCoordinateSystem();
      axis2d->GetPoint1Coordinate()->SetCoordinateSystemToWorld();
      axis2d->GetPoint1Coordinate()->SetValue(dbuffer3);
      axis2d->GetPoint1Coordinate()->SetCoordinateSystem(s);
      }
    }

  if (elem->GetVectorAttribute("Point2WorldPosition", 3, dbuffer3) == 3)
    {
    obj->GetPoint2Representation()->SetWorldPosition(dbuffer3);
    if (axis2d)
      {
      int s = axis2d->GetPoint2Coordinate()->GetCoordinateSystem();
      axis2d->GetPoint2Coordinate()->SetCoordinateSystemToWorld();
      axis2d->GetPoint2Coordinate()->SetValue(dbuffer3);
      axis2d->GetPoint2Coordinate()->SetCoordinateSystem(s);
      }
    }

  if (elem->GetScalarAttribute("Tolerance", ival))
    {
    obj->SetTolerance(ival);
    }

  cptr = elem->GetAttribute("LabelFormat");
  if (cptr)
    {
    obj->SetLabelFormat(cptr);
    }
  
  if (axis2d)
    {
    if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
      {
      axis2d->GetProperty()->SetColor(dbuffer3);
      axis2d->GetTitleTextProperty()->SetColor(
        axis2d->GetProperty()->GetColor());
      }
    }

  // Get nested elements
  
  /*
  // Too complex, just to store the color...
  // Axis

  if (axis2d)
    {
    vtkXMLAxisActor2DReader *xmlr = vtkXMLAxisActor2DReader::New();
    xmlr->SetObject(axis2d);
    xmlr->ParseInNestedElement(
      elem, vtkXMLDistanceRepresentationWriter::GetAxisElementName());
    xmlr->Delete();
    }
  */

  return 1;
}
