/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLBoundingBoxAnnotationWriter - vtkBoundingBoxAnnotation XML Writer.
// .SECTION Description
// vtkXMLBoundingBoxAnnotationWriter provides XML writing functionality to 
// vtkBoundingBoxAnnotation.
// .SECTION See Also
// vtkXMLBoundingBoxAnnotationReader

#ifndef __vtkXMLBoundingBoxAnnotationWriter_h
#define __vtkXMLBoundingBoxAnnotationWriter_h

#include "XML/vtkXMLActorWriter.h"

class VTK_EXPORT vtkXMLBoundingBoxAnnotationWriter : public vtkXMLActorWriter
{
public:
  static vtkXMLBoundingBoxAnnotationWriter* New();
  vtkTypeRevisionMacro(vtkXMLBoundingBoxAnnotationWriter,vtkXMLActorWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLBoundingBoxAnnotationWriter() {};
  ~vtkXMLBoundingBoxAnnotationWriter() {};  
  
private:
  vtkXMLBoundingBoxAnnotationWriter(const vtkXMLBoundingBoxAnnotationWriter&);  // Not implemented.
  void operator=(const vtkXMLBoundingBoxAnnotationWriter&);  // Not implemented.
};

#endif
