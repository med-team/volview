/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWContourWidgetReader - vtkKWContourWidget XML Reader.
// .SECTION Description
// vtkXMLKWContourWidgetReader provides XML reading functionality to 
// vtkKWContourWidget.
// .SECTION See Also
// vtkXMLKWContourWidgetWriter

#ifndef __vtkXMLKWContourWidgetReader_h
#define __vtkXMLKWContourWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLKWContourWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLKWContourWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWContourWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWContourWidgetReader() {};
  ~vtkXMLKWContourWidgetReader() {};

private:
  vtkXMLKWContourWidgetReader(const vtkXMLKWContourWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWContourWidgetReader&); // Not implemented    
};

#endif



