/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWContourWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkKWContourWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkContourRepresentation.h"

#include "XML/vtkXMLContourRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLKWContourWidgetWriter);
vtkCxxRevisionMacro(vtkXMLKWContourWidgetWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWContourWidgetWriter::GetRootElementName()
{
  return "ContourWidget";
}

//----------------------------------------------------------------------------
const char* vtkXMLKWContourWidgetWriter::GetRepresentationElementName()
{
  return "Representation";
}

//----------------------------------------------------------------------------
int vtkXMLKWContourWidgetWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkKWContourWidget *obj = vtkKWContourWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ContourWidget is not set!");
    return 0;
    }

  // Representation

  vtkContourRepresentation *rep = 
    vtkContourRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLContourRepresentationWriter *xmlw = 
      vtkXMLContourRepresentationWriter::New();
    xmlw->SetObject(rep);
    xmlw->CreateInNestedElement(
      elem, this->GetRepresentationElementName());
    xmlw->Delete();
    }

  return 1;
}

