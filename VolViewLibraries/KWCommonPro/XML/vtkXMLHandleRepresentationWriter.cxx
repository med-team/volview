/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLHandleRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkHandleRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkPointHandleRepresentation3D.h"
#include "vtkProperty.h"

vtkStandardNewMacro(vtkXMLHandleRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLHandleRepresentationWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLHandleRepresentationWriter::GetRootElementName()
{
  return "HandleRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLHandleRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkHandleRepresentation *obj = 
    vtkHandleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The HandleRepresentation is not set!");
    return 0;
    }

  double pos[3];

  obj->GetWorldPosition(pos);
  elem->SetVectorAttribute("WorldPosition", 3, pos);

  elem->SetIntAttribute("Tolerance", obj->GetTolerance());
  elem->SetIntAttribute("ActiveRepresentation", obj->GetActiveRepresentation());

  vtkPointHandleRepresentation3D *rep3d = 
    vtkPointHandleRepresentation3D::SafeDownCast(obj);
  if (rep3d)
    {
    elem->SetVectorAttribute(
      "Color", 3, rep3d->GetProperty()->GetColor());
    elem->SetVectorAttribute(
      "SelectedColor", 3, rep3d->GetSelectedProperty()->GetColor());
    }

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLHandleRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkHandleRepresentation *obj = 
    vtkHandleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The HandleRepresentation is not set!");
    return 0;
    }

  return 1;
}

