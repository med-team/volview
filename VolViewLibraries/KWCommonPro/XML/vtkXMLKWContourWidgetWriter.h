/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWContourWidgetWriter - vtkKWContourWidget XML Writer.
// .SECTION Description
// vtkXMLKWContourWidgetWriter provides XML writing functionality to 
// vtkKWContourWidget.
// .SECTION See Also
// vtkXMLKWContourWidgetReader

#ifndef __vtkXMLKWContourWidgetWriter_h
#define __vtkXMLKWContourWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLKWContourWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLKWContourWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWContourWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLKWContourWidgetWriter() {};
  ~vtkXMLKWContourWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWContourWidgetWriter(const vtkXMLKWContourWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWContourWidgetWriter&);  // Not implemented.
};

#endif



