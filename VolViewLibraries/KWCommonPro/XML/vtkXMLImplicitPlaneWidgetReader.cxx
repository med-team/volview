/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLImplicitPlaneWidgetReader.h"

#include "vtkImplicitPlaneWidget.h"
#include "vtkObjectFactory.h"
#include "vtkProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLImplicitPlaneWidgetWriter.h"
#include "XML/vtkXMLPropertyReader.h"

vtkStandardNewMacro(vtkXMLImplicitPlaneWidgetReader);
vtkCxxRevisionMacro(vtkXMLImplicitPlaneWidgetReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLImplicitPlaneWidgetReader::GetRootElementName()
{
  return "ImplicitPlaneWidget";
}

//----------------------------------------------------------------------------
int vtkXMLImplicitPlaneWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkImplicitPlaneWidget *obj = 
    vtkImplicitPlaneWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ImplicitPlaneWidget is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3], dval;
  int ival;

  if (elem->GetVectorAttribute("Origin", 3, dbuffer3) == 3)
    {
    obj->SetOrigin(dbuffer3);
    }

  if (elem->GetVectorAttribute("Normal", 3, dbuffer3) == 3)
    {
    obj->SetNormal(dbuffer3);
    }

  if (elem->GetScalarAttribute("NormalToXAxis", ival))
    {
    obj->SetNormalToXAxis(ival);
    }

  if (elem->GetScalarAttribute("NormalToYAxis", ival))
    {
    obj->SetNormalToYAxis(ival);
    }

  if (elem->GetScalarAttribute("NormalToZAxis", ival))
    {
    obj->SetNormalToZAxis(ival);
    }

  if (elem->GetScalarAttribute("Tubing", ival))
    {
    obj->SetTubing(ival);
    }

  if (elem->GetScalarAttribute("DrawPlane", ival))
    {
    obj->SetDrawPlane(ival);
    }

  if (elem->GetScalarAttribute("OutlineTranslation", ival))
    {
    obj->SetOutlineTranslation(ival);
    }

  if (elem->GetScalarAttribute("OutsideBounds", ival))
    {
    obj->SetOutsideBounds(ival);
    }

  if (elem->GetScalarAttribute("ScaleEnabled", ival))
    {
    obj->SetScaleEnabled(ival);
    }

  if (elem->GetScalarAttribute("OriginTranslation", ival))
    {
    obj->SetOriginTranslation(ival);
    }

  if (elem->GetScalarAttribute("DiagonalRatio", dval))
    {
    obj->SetDiagonalRatio(dval);
    }

  // Get nested elements
  
  // Plane properties

  vtkXMLPropertyReader *xmlr = vtkXMLPropertyReader::New();
  vtkProperty *prop;

  prop = obj->GetNormalProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetNormalPropertyElementName());
    }

  prop = obj->GetSelectedNormalProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetSelectedNormalPropertyElementName());
    }

  prop = obj->GetPlaneProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetPlanePropertyElementName());
    }

  prop = obj->GetSelectedPlaneProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, 
      vtkXMLImplicitPlaneWidgetWriter::GetSelectedPlanePropertyElementName());
    }

  prop = obj->GetOutlineProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetOutlinePropertyElementName());
    }

  prop = obj->GetSelectedOutlineProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetSelectedOutlinePropertyElementName());
    }

  prop = obj->GetEdgesProperty();
  if (prop)
    {
    xmlr->SetObject(prop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLImplicitPlaneWidgetWriter::GetEdgesPropertyElementName());
    }

  xmlr->Delete();

  return 1;
}


