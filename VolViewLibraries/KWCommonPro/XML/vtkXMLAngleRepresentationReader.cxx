/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAngleRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkAngleRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkAngleRepresentation2D.h"
#include "vtkLeaderActor2D.h"
#include "vtkHandleRepresentation.h"
#include "vtkProperty2D.h"
#include "vtkTextProperty.h"

#include "XML/vtkXMLAngleRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLAngleRepresentationReader);
vtkCxxRevisionMacro(vtkXMLAngleRepresentationReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLAngleRepresentationReader::GetRootElementName()
{
  return "AngleRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLAngleRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkAngleRepresentation *obj = 
    vtkAngleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AngleRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;
  const char *cptr;

  vtkAngleRepresentation2D *rep2d = 
    vtkAngleRepresentation2D::SafeDownCast(obj);

  if (elem->GetVectorAttribute("Point1WorldPosition", 3, dbuffer3) == 3)
    {
    obj->GetPoint1Representation()->SetWorldPosition(dbuffer3);
    if (rep2d)
      {
      int s = rep2d->GetRay1()->GetPositionCoordinate()->GetCoordinateSystem();
      rep2d->GetRay1()->GetPositionCoordinate()->SetCoordinateSystemToWorld();
      rep2d->GetRay1()->GetPosition2Coordinate()->SetValue(dbuffer3);
      rep2d->GetRay1()->GetPositionCoordinate()->SetCoordinateSystem(s);
      }
    }

  if (elem->GetVectorAttribute("CenterWorldPosition", 3, dbuffer3) == 3)
    {
    obj->GetCenterRepresentation()->SetWorldPosition(dbuffer3);
    if (rep2d)
      {
      int s = rep2d->GetRay1()->GetPositionCoordinate()->GetCoordinateSystem();
      rep2d->GetRay1()->GetPositionCoordinate()->SetCoordinateSystemToWorld();
      rep2d->GetRay1()->GetPositionCoordinate()->SetValue(dbuffer3);
      rep2d->GetRay1()->GetPositionCoordinate()->SetCoordinateSystem(s);

      s = rep2d->GetRay2()->GetPositionCoordinate()->GetCoordinateSystem();
      rep2d->GetRay2()->GetPositionCoordinate()->SetCoordinateSystemToWorld();
      rep2d->GetRay2()->GetPositionCoordinate()->SetValue(dbuffer3);
      rep2d->GetRay2()->GetPositionCoordinate()->SetCoordinateSystem(s);
      }
    }

  if (elem->GetVectorAttribute("Point2WorldPosition", 3, dbuffer3) == 3)
    {
    obj->GetPoint2Representation()->SetWorldPosition(dbuffer3);
    if (rep2d)
      {
      int s = rep2d->GetRay2()->GetPositionCoordinate()->GetCoordinateSystem();
      rep2d->GetRay2()->GetPositionCoordinate()->SetCoordinateSystemToWorld();
      rep2d->GetRay2()->GetPosition2Coordinate()->SetValue(dbuffer3);
      rep2d->GetRay2()->GetPositionCoordinate()->SetCoordinateSystem(s);
      }
    }

  if (elem->GetScalarAttribute("Tolerance", ival))
    {
    obj->SetTolerance(ival);
    }

  cptr = elem->GetAttribute("LabelFormat");
  if (cptr)
    {
    obj->SetLabelFormat(cptr);
    }
  
  if (elem->GetScalarAttribute("Ray1Visibility", ival))
    {
    obj->SetRay1Visibility(ival);
    }

  if (elem->GetScalarAttribute("Ray2Visibility", ival))
    {
    obj->SetRay2Visibility(ival);
    }

  if (elem->GetScalarAttribute("ArcVisibility", ival))
    {
    obj->SetArcVisibility(ival);
    }

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    if (rep2d)
      {
      rep2d->GetArc()->GetProperty()->SetColor(dbuffer3);
      rep2d->GetArc()->GetLabelTextProperty()->SetColor(
        rep2d->GetArc()->GetProperty()->GetColor());
      rep2d->GetRay1()->GetProperty()->SetColor(
        rep2d->GetArc()->GetProperty()->GetColor());
      rep2d->GetRay2()->GetProperty()->SetColor(
        rep2d->GetArc()->GetProperty()->GetColor());
      }
    }

  // Get nested elements
  
  /*
  // Too complex, just to store the color...
  // Lead actors

  */

  return 1;
}
