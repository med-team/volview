/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBorderRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkBorderRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty2D.h"

#include "XML/vtkXMLBorderRepresentationWriter.h"
#include "XML/vtkXMLProperty2DReader.h"

vtkStandardNewMacro(vtkXMLBorderRepresentationReader);
vtkCxxRevisionMacro(vtkXMLBorderRepresentationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBorderRepresentationReader::GetRootElementName()
{
  return "BorderRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLBorderRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkBorderRepresentation *obj = 
    vtkBorderRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BorderRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer2[2];
  int ival, ibuffer2[3];

  if (elem->GetVectorAttribute("PositionCoordinate", 2, dbuffer2) == 2)
    {
    int s = obj->GetPositionCoordinate()->GetCoordinateSystem();
    obj->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
    obj->GetPositionCoordinate()->SetValue(dbuffer2);
    obj->GetPositionCoordinate()->SetCoordinateSystem(s);
    }

  if (elem->GetVectorAttribute("Position2Coordinate", 2, dbuffer2) == 2)
    {
    int s = obj->GetPosition2Coordinate()->GetCoordinateSystem();
    obj->GetPosition2Coordinate()->SetCoordinateSystemToNormalizedViewport();
    obj->GetPosition2Coordinate()->SetValue(dbuffer2);
    obj->GetPosition2Coordinate()->SetCoordinateSystem(s);
    }

  if (elem->GetScalarAttribute("ShowBorder", ival))
    {
    obj->SetShowBorder(ival);
    }

  if (elem->GetScalarAttribute("ProportionalResize", ival))
    {
    obj->SetProportionalResize(ival);
    }

  if (elem->GetVectorAttribute("MinimumSize", 2, ibuffer2) == 2)
    {
    obj->SetMinimumSize(ibuffer2);
    }

  if (elem->GetVectorAttribute("MaximumSize", 2, ibuffer2) == 2)
    {
    obj->SetMaximumSize(ibuffer2);
    }

  if (elem->GetScalarAttribute("Tolerance", ival))
    {
    obj->SetTolerance(ival);
    }

  // Border Property

  if (obj->GetBorderProperty())
    {
    vtkXMLProperty2DReader *xmlr = vtkXMLProperty2DReader::New();
    xmlr->SetObject(obj->GetBorderProperty());
    xmlr->ParseInNestedElement(
      elem, vtkXMLBorderRepresentationWriter::GetBorderPropertyElementName());
    xmlr->Delete();
    }

  return 1;
}
