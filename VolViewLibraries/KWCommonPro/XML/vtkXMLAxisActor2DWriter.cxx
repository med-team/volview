/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAxisActor2DWriter.h"

#include "vtkObjectFactory.h"
#include "vtkAxisActor2D.h"
#include "vtkTextProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLTextPropertyWriter.h"

vtkStandardNewMacro(vtkXMLAxisActor2DWriter);
vtkCxxRevisionMacro(vtkXMLAxisActor2DWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLAxisActor2DWriter::GetRootElementName()
{
  return "AxisActor2D";
}

//----------------------------------------------------------------------------
const char* vtkXMLAxisActor2DWriter::GetTitleTextPropertyElementName()
{
  return "TitleTextProperty";
}

//----------------------------------------------------------------------------
const char* vtkXMLAxisActor2DWriter::GetLabelTextPropertyElementName()
{
  return "LabelTextProperty";
}

//----------------------------------------------------------------------------
int vtkXMLAxisActor2DWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkAxisActor2D *obj = vtkAxisActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AxisActor2D is not set!");
    return 0;
    }

  elem->SetVectorAttribute("Range", 2, obj->GetRange());

  elem->SetIntAttribute("NumberOfLabels", obj->GetNumberOfLabels());

  elem->SetAttribute("LabelFormat", obj->GetLabelFormat());

  elem->SetIntAttribute("AdjustLabels", obj->GetAdjustLabels());

  elem->SetAttribute("Title", obj->GetTitle());

  elem->SetIntAttribute("TickLength", obj->GetTickLength());

  elem->SetIntAttribute("NumberOfMinorTicks", obj->GetNumberOfMinorTicks());

  elem->SetIntAttribute("MinorTickLength", obj->GetMinorTickLength());

  elem->SetIntAttribute("TickOffset", obj->GetTickOffset());

  elem->SetIntAttribute("AxisVisibility", obj->GetAxisVisibility());

  elem->SetIntAttribute("TickVisibility", obj->GetTickVisibility());

  elem->SetIntAttribute("LabelVisibility", obj->GetLabelVisibility());

  elem->SetIntAttribute("TitleVisibility", obj->GetTitleVisibility());

  elem->SetDoubleAttribute("TitlePosition", obj->GetTitlePosition());

  elem->SetDoubleAttribute("FontFactor", obj->GetFontFactor());

  elem->SetDoubleAttribute("LabelFactor", obj->GetLabelFactor());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLAxisActor2DWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkAxisActor2D *obj = vtkAxisActor2D::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AxisActor2D is not set!");
    return 0;
    }

  // Title text property

  vtkTextProperty *tprop = obj->GetTitleTextProperty();
  if (tprop)
    {
    vtkXMLTextPropertyWriter *xmlw = vtkXMLTextPropertyWriter::New();
    xmlw->SetObject(tprop);
    xmlw->CreateInNestedElement(elem, this->GetTitleTextPropertyElementName());
    xmlw->Delete();
    }
 
  // Label text property

  vtkTextProperty *lprop = obj->GetLabelTextProperty();
  if (lprop)
    {
    vtkXMLTextPropertyWriter *xmlw = vtkXMLTextPropertyWriter::New();
    xmlw->SetObject(lprop);
    xmlw->CreateInNestedElement(elem, this->GetLabelTextPropertyElementName());
    xmlw->Delete();
    }
 
  return 1;
}


