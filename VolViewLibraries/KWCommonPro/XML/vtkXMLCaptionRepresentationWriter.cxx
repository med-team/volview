/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkCaptionActor2D.h"

#include "XML/vtkXMLCaptionActor2DWriter.h"

vtkStandardNewMacro(vtkXMLCaptionRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLCaptionRepresentationWriter, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionRepresentationWriter::GetRootElementName()
{
  return "CaptionRepresentation";
}

//----------------------------------------------------------------------------
const char* vtkXMLCaptionRepresentationWriter::GetCaptionActor2DElementName()
{
  return "CaptionActor2D";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkCaptionRepresentation *obj = 
    vtkCaptionRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionRepresentation is not set!");
    return 0;
    }

  double pos[3];

  obj->GetAnchorPosition(pos);
  elem->SetVectorAttribute("AnchorPosition", 3, pos);

  elem->SetDoubleAttribute("FontFactor", obj->GetFontFactor());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLCaptionRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkCaptionRepresentation *obj = 
    vtkCaptionRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionRepresentation is not set!");
    return 0;
    }

  // Caption Actor 2D

  if (obj->GetCaptionActor2D())
    {
    vtkXMLCaptionActor2DWriter *xmlw = vtkXMLCaptionActor2DWriter::New();
    xmlw->SetObject(obj->GetCaptionActor2D());
    xmlw->CreateInNestedElement(elem, this->GetCaptionActor2DElementName());
    xmlw->Delete();
    }

  return 1;
}
