/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkCaptionActor2D.h"
#include "vtkPointHandleRepresentation3D.h"

#include "XML/vtkXMLCaptionRepresentationWriter.h"
#include "XML/vtkXMLCaptionActor2DReader.h"

vtkStandardNewMacro(vtkXMLCaptionRepresentationReader);
vtkCxxRevisionMacro(vtkXMLCaptionRepresentationReader, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionRepresentationReader::GetRootElementName()
{
  return "CaptionRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkCaptionRepresentation *obj = 
    vtkCaptionRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];

  if (elem->GetVectorAttribute("AnchorPosition", 3, dbuffer3) == 3)
    {
    obj->SetAnchorPosition(dbuffer3);
    if(obj->GetAnchorRepresentation())
      {
      obj->GetAnchorRepresentation()->SetWorldPosition(dbuffer3);
      }
    }

  double dval;

  if (elem->GetScalarAttribute("FontFactor", dval))
    {
    obj->SetFontFactor(dval);
    }

  // Caption Actor 2D

  if (obj->GetCaptionActor2D())
    {
    vtkXMLCaptionActor2DReader *xmlr = vtkXMLCaptionActor2DReader::New();
    xmlr->SetObject(obj->GetCaptionActor2D());
    xmlr->ParseInNestedElement(
      elem, vtkXMLCaptionRepresentationWriter::GetCaptionActor2DElementName());
    xmlr->Delete();
    }

  return 1;
}
