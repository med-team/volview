/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAbstractWidgetWriter.h"

#include "vtkObjectFactory.h"
#include "vtkAbstractWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLAbstractWidgetWriter);
vtkCxxRevisionMacro(vtkXMLAbstractWidgetWriter, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLAbstractWidgetWriter::GetRootElementName()
{
  return "AbstractWidget";
}

//----------------------------------------------------------------------------
int vtkXMLAbstractWidgetWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkAbstractWidget *obj = vtkAbstractWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AbstractWidget is not set!");
    return 0;
    }

  elem->SetIntAttribute("ProcessEvents", obj->GetProcessEvents());

  elem->SetIntAttribute("ManagesCursor", obj->GetManagesCursor());

  return 1;
}



