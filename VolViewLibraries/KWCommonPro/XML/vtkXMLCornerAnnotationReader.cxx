/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCornerAnnotationReader.h"

#include "vtkCornerAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkTextProperty.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLTextPropertyReader.h"
#include "XML/vtkXMLCornerAnnotationWriter.h"

vtkStandardNewMacro(vtkXMLCornerAnnotationReader);
vtkCxxRevisionMacro(vtkXMLCornerAnnotationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLCornerAnnotationReader::GetRootElementName()
{
  return "CornerAnnotation";
}

//----------------------------------------------------------------------------
int vtkXMLCornerAnnotationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkCornerAnnotation *obj = vtkCornerAnnotation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CornerAnnotation is not set!");
    return 0;
    }

  // Get attributes

  float fval;
  int ival;

  if (elem->GetScalarAttribute("MaximumLineHeight", fval))
    {
    obj->SetMaximumLineHeight(fval);
    }

  if (elem->GetScalarAttribute("MinimumFontSize", ival))
    {
    obj->SetMinimumFontSize(ival);
    }

  if (elem->GetScalarAttribute("LevelShift", fval))
    {
    obj->SetLevelShift(fval);
    }

  if (elem->GetScalarAttribute("LevelScale", fval))
    {
    obj->SetLevelScale(fval);
    }

  if (elem->GetScalarAttribute("ShowSliceAndImage", ival))
    {
    obj->SetShowSliceAndImage(ival);
    }

  // Get nested elements

  vtkXMLDataElement *nested_elem;

  // Text

  for (int i = 0; i < 4; i++)
    {
    char name[10];
    sprintf(name, "Text%d", i);
    nested_elem = elem->FindNestedElementWithName(name);
    obj->SetText(i, nested_elem ? nested_elem->GetCharacterData() : NULL);
    }

  // Text property

  vtkXMLTextPropertyReader *xmlr = vtkXMLTextPropertyReader::New();
  if (xmlr->IsInNestedElement(
        elem, vtkXMLCornerAnnotationWriter::GetTextPropertyElementName()))
    {
    vtkTextProperty *tprop = obj->GetTextProperty();
    if (!tprop)
      {
      tprop = vtkTextProperty::New();
      obj->SetTextProperty(tprop);
      tprop->Delete();
      }
    xmlr->SetObject(tprop);
    xmlr->ParseInNestedElement(
      elem, vtkXMLCornerAnnotationWriter::GetTextPropertyElementName());
    }
  xmlr->Delete();
  
  return 1;
}

