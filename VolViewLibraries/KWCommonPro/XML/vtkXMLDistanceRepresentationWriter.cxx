/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLDistanceRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkDistanceRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkDistanceRepresentation2D.h"
#include "vtkAxisActor2D.h"
#include "vtkProperty2D.h"

#include "XML/vtkXMLAxisActor2DWriter.h"

vtkStandardNewMacro(vtkXMLDistanceRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLDistanceRepresentationWriter, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLDistanceRepresentationWriter::GetRootElementName()
{
  return "DistanceRepresentation";
}

//----------------------------------------------------------------------------
const char* vtkXMLDistanceRepresentationWriter::GetAxisElementName()
{
  return "Axis";
}

//----------------------------------------------------------------------------
int vtkXMLDistanceRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkDistanceRepresentation *obj = 
    vtkDistanceRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The DistanceRepresentation is not set!");
    return 0;
    }

  double pos[3];

  obj->GetPoint1WorldPosition(pos);
  elem->SetVectorAttribute("Point1WorldPosition", 3, pos);

  obj->GetPoint2WorldPosition(pos);
  elem->SetVectorAttribute("Point2WorldPosition", 3, pos);

  elem->SetIntAttribute("Tolerance", obj->GetTolerance());

  elem->SetAttribute("LabelFormat", obj->GetLabelFormat());

  vtkDistanceRepresentation2D *rep2d = 
    vtkDistanceRepresentation2D::SafeDownCast(obj);
  if (rep2d)
    {
    elem->SetVectorAttribute(
      "Color", 3, rep2d->GetAxis()->GetProperty()->GetColor());
    }

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLDistanceRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkDistanceRepresentation *obj = 
    vtkDistanceRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The DistanceRepresentation is not set!");
    return 0;
    }

  /*
  // Too complex, just to store the color...
  // Axis

  vtkDistanceRepresentation2D *rep2d = 
    vtkDistanceRepresentation2D::SafeDownCast(obj);
  vtkAxisActor2D *axis2d = rep2d ? rep2d->GetAxis() : NULL;

  if (axis2d)
    {
    vtkXMLAxisActor2DWriter *xmlw = vtkXMLAxisActor2DWriter::New();
    xmlw->SetObject(axis2d);
    xmlw->CreateInNestedElement(elem, this->GetAxisElementName());
    xmlw->Delete();
    }
  */

  return 1;
}


