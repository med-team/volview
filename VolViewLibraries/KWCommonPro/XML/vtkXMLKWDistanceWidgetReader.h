/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWDistanceWidgetReader - vtkKWDistanceWidget XML Reader.
// .SECTION Description
// vtkXMLKWDistanceWidgetReader provides XML reading functionality to 
// vtkKWDistanceWidget.
// .SECTION See Also
// vtkXMLKWDistanceWidgetWriter

#ifndef __vtkXMLKWDistanceWidgetReader_h
#define __vtkXMLKWDistanceWidgetReader_h

#include "XML/vtkXMLAbstractWidgetReader.h"

class VTK_EXPORT vtkXMLKWDistanceWidgetReader : public vtkXMLAbstractWidgetReader
{
public:
  static vtkXMLKWDistanceWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLKWDistanceWidgetReader, vtkXMLAbstractWidgetReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLKWDistanceWidgetReader() {};
  ~vtkXMLKWDistanceWidgetReader() {};

private:
  vtkXMLKWDistanceWidgetReader(const vtkXMLKWDistanceWidgetReader&); // Not implemented
  void operator=(const vtkXMLKWDistanceWidgetReader&); // Not implemented    
};

#endif



