/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWContourWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWContourWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkContourRepresentation.h"

#include "XML/vtkXMLContourRepresentationReader.h"
#include "XML/vtkXMLKWContourWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWContourWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWContourWidgetReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWContourWidgetReader::GetRootElementName()
{
  return "ContourWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWContourWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWContourWidget *obj = vtkKWContourWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWContourWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  // Representation

  vtkContourRepresentation *rep = 
    vtkContourRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLContourRepresentationReader *xmlr = 
      vtkXMLContourRepresentationReader::New();
    xmlr->SetObject(rep);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWContourWidgetWriter::GetRepresentationElementName());
    xmlr->Delete();
    obj->WidgetIsDefined();
    }

  return 1;
}
