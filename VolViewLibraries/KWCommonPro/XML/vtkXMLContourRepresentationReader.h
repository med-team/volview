/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLContourRepresentationReader - vtkContourRepresentation XML Reader.
// .SECTION Description
// vtkXMLContourRepresentationReader provides XML reading functionality to 
// vtkContourRepresentation.
// .SECTION See Also
// vtkXMLContourRepresentationWriter

#ifndef __vtkXMLContourRepresentationReader_h
#define __vtkXMLContourRepresentationReader_h

#include "XML/vtkXMLWidgetRepresentationReader.h"

class VTK_EXPORT vtkXMLContourRepresentationReader : public vtkXMLWidgetRepresentationReader
{
public:
  static vtkXMLContourRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLContourRepresentationReader, vtkXMLWidgetRepresentationReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLContourRepresentationReader() {};
  ~vtkXMLContourRepresentationReader() {};

private:
  vtkXMLContourRepresentationReader(const vtkXMLContourRepresentationReader&); // Not implemented
  void operator=(const vtkXMLContourRepresentationReader&); // Not implemented    
};

#endif



