/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLHandleRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkHandleRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkPointHandleRepresentation3D.h"
#include "vtkProperty.h"

#include "XML/vtkXMLHandleRepresentationWriter.h"
#include "XML/vtkXMLAxisActor2DReader.h"

vtkStandardNewMacro(vtkXMLHandleRepresentationReader);
vtkCxxRevisionMacro(vtkXMLHandleRepresentationReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLHandleRepresentationReader::GetRootElementName()
{
  return "HandleRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLHandleRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkHandleRepresentation *obj = 
    vtkHandleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The HandleRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  int ival;

  vtkPointHandleRepresentation3D *rep3d = 
    vtkPointHandleRepresentation3D::SafeDownCast(obj);

  if (elem->GetVectorAttribute("WorldPosition", 3, dbuffer3) == 3)
    {
    obj->SetWorldPosition(dbuffer3);
    }

  if (elem->GetScalarAttribute("Tolerance", ival))
    {
    obj->SetTolerance(ival);
    }

  if (elem->GetScalarAttribute("ActiveRepresentation", ival))
    {
    obj->SetActiveRepresentation(ival);
    }
  
  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    rep3d->GetProperty()->SetColor(dbuffer3);
    }

  if (elem->GetVectorAttribute("SelectedColor", 3, dbuffer3) == 3)
    {
    rep3d->GetSelectedProperty()->SetColor(dbuffer3);
    }

  return 1;
}

