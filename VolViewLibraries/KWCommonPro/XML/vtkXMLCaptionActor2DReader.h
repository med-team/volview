/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionActor2DReader - vtkCaptionActor2D XML Reader.
// .SECTION Description
// vtkXMLCaptionActor2DReader provides XML reading functionality to 
// vtkCaptionActor2D.
// .SECTION See Also
// vtkXMLCaptionActor2DWriter

#ifndef __vtkXMLCaptionActor2DReader_h
#define __vtkXMLCaptionActor2DReader_h

#include "XML/vtkXMLActor2DReader.h"

class VTK_EXPORT vtkXMLCaptionActor2DReader : public vtkXMLActor2DReader
{
public:
  static vtkXMLCaptionActor2DReader* New();
  vtkTypeRevisionMacro(vtkXMLCaptionActor2DReader, vtkXMLActor2DReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLCaptionActor2DReader() {};
  ~vtkXMLCaptionActor2DReader() {};

private:
  vtkXMLCaptionActor2DReader(const vtkXMLCaptionActor2DReader&); // Not implemented
  void operator=(const vtkXMLCaptionActor2DReader&); // Not implemented    
};

#endif


