/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLPiecewiseFunctionWriter.h"

#include "vtkObjectFactory.h"
#include "vtkPiecewiseFunction.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLPiecewiseFunctionWriter);
vtkCxxRevisionMacro(vtkXMLPiecewiseFunctionWriter, "$Revision: 1.11 $");

//----------------------------------------------------------------------------
const char* vtkXMLPiecewiseFunctionWriter::GetRootElementName()
{
  return "PiecewiseFunction";
}

//----------------------------------------------------------------------------
const char* vtkXMLPiecewiseFunctionWriter::GetPointElementName()
{
  return "Point";
}

//----------------------------------------------------------------------------
int vtkXMLPiecewiseFunctionWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkPiecewiseFunction *obj = vtkPiecewiseFunction::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The PiecewiseFunction is not set!");
    return 0;
    }

  elem->SetIntAttribute("Size", obj->GetSize());

  elem->SetIntAttribute("Clamping", obj->GetClamping());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLPiecewiseFunctionWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkPiecewiseFunction *obj = vtkPiecewiseFunction::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The PiecewiseFunction is not set!");
    return 0;
    }

  // Iterate over all points and create a point XML data element for each one

  int size = obj->GetSize();

#if VTK_MAJOR_VERSION > 5 || (VTK_MAJOR_VERSION == 5 && VTK_MINOR_VERSION > 0)
  double val[4];
  for (int i = 0; i < size; ++i)
    {
    if (obj->GetNodeValue(i, val))
      {
      vtkXMLDataElement *point_elem = this->NewDataElement();
      elem->AddNestedElement(point_elem);
      point_elem->Delete();
      point_elem->SetName(this->GetPointElementName());
      point_elem->SetDoubleAttribute("X", val[0]);
      point_elem->SetDoubleAttribute("Value", val[1]);
      point_elem->SetDoubleAttribute("MidPoint", val[2]);
      point_elem->SetDoubleAttribute("Sharpness", val[3]);
      }
    }
#else
  double *val = obj->GetDataPointer();
  for (int i = 0; i < size; ++i, val += 2)
    {
    vtkXMLDataElement *point_elem = this->NewDataElement();
    elem->AddNestedElement(point_elem);
    point_elem->Delete();
    point_elem->SetName(this->GetPointElementName());
    point_elem->SetDoubleAttribute("X", val[0]);
    point_elem->SetDoubleAttribute("Value", val[1]);
    }
#endif

  return 1;
}


