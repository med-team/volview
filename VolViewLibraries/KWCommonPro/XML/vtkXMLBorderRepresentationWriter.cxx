/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBorderRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkBorderRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty2D.h"

#include "XML/vtkXMLProperty2DWriter.h"

vtkStandardNewMacro(vtkXMLBorderRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLBorderRepresentationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBorderRepresentationWriter::GetRootElementName()
{
  return "BorderRepresentation";
}

//----------------------------------------------------------------------------
const char* vtkXMLBorderRepresentationWriter::GetBorderPropertyElementName()
{
  return "BorderProperty";
}

//----------------------------------------------------------------------------
int vtkXMLBorderRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkBorderRepresentation *obj = 
    vtkBorderRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BorderRepresentation is not set!");
    return 0;
    }

  int csys;

  csys = obj->GetPositionCoordinate()->GetCoordinateSystem();
  obj->GetPositionCoordinate()->SetCoordinateSystemToNormalizedViewport();
  elem->SetVectorAttribute("PositionCoordinate", 2, 
                           obj->GetPositionCoordinate()->GetValue());
  obj->GetPositionCoordinate()->SetCoordinateSystem(csys);

  csys = obj->GetPosition2Coordinate()->GetCoordinateSystem();
  obj->GetPosition2Coordinate()->SetCoordinateSystemToNormalizedViewport();
  elem->SetVectorAttribute("Position2Coordinate", 2, 
                           obj->GetPosition2Coordinate()->GetValue());
  obj->GetPosition2Coordinate()->SetCoordinateSystem(csys);

  elem->SetIntAttribute("ShowBorder", obj->GetShowBorder());

  elem->SetIntAttribute("ProportionalResize", obj->GetProportionalResize());

  elem->SetVectorAttribute("MinimumSize", 2, obj->GetMinimumSize());

  elem->SetVectorAttribute("MaximumSize", 2, obj->GetMaximumSize());

  elem->SetIntAttribute("Tolerance", obj->GetTolerance());

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLBorderRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkBorderRepresentation *obj = 
    vtkBorderRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The BorderRepresentation is not set!");
    return 0;
    }

  // Border Property

  if (obj->GetBorderProperty())
    {
    vtkXMLProperty2DWriter *xmlw = vtkXMLProperty2DWriter::New();
    xmlw->SetObject(obj->GetBorderProperty());
    xmlw->CreateInNestedElement(elem, this->GetBorderPropertyElementName());
    xmlw->Delete();
    }

  return 1;
}


