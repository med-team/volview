/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAxisActor2DReader - vtkAxisActor2D XML Reader.
// .SECTION Description
// vtkXMLAxisActor2DReader provides XML reading functionality to 
// vtkAxisActor2D.
// .SECTION See Also
// vtkXMLAxisActor2DWriter

#ifndef __vtkXMLAxisActor2DReader_h
#define __vtkXMLAxisActor2DReader_h

#include "XML/vtkXMLActor2DReader.h"

class VTK_EXPORT vtkXMLAxisActor2DReader : public vtkXMLActor2DReader
{
public:
  static vtkXMLAxisActor2DReader* New();
  vtkTypeRevisionMacro(vtkXMLAxisActor2DReader, vtkXMLActor2DReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLAxisActor2DReader() {};
  ~vtkXMLAxisActor2DReader() {};

private:
  vtkXMLAxisActor2DReader(const vtkXMLAxisActor2DReader&); // Not implemented
  void operator=(const vtkXMLAxisActor2DReader&); // Not implemented    
};

#endif


