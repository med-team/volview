/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLMedicalImagePropertiesReader - vtkMedicalImageProperties XML Reader.
// .SECTION Description
// vtkXMLMedicalImagePropertiesReader provides XML reading functionality to 
// vtkMedicalImageProperties.
// .SECTION See Also
// vtkXMLMedicalImagePropertiesWriter

#ifndef __vtkXMLMedicalImagePropertiesReader_h
#define __vtkXMLMedicalImagePropertiesReader_h

#include "XML/vtkXMLObjectReader.h"

class VTK_EXPORT vtkXMLMedicalImagePropertiesReader : public vtkXMLObjectReader
{
public:
  static vtkXMLMedicalImagePropertiesReader* New();
  vtkTypeRevisionMacro(vtkXMLMedicalImagePropertiesReader, vtkXMLObjectReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLMedicalImagePropertiesReader() {};
  ~vtkXMLMedicalImagePropertiesReader() {};

private:
  vtkXMLMedicalImagePropertiesReader(const vtkXMLMedicalImagePropertiesReader&); // Not implemented
  void operator=(const vtkXMLMedicalImagePropertiesReader&); // Not implemented    
};

#endif

