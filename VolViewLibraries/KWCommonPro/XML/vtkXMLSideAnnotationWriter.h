/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLSideAnnotationWriter - vtkSideAnnotation XML Writer.
// .SECTION Description
// vtkXMLSideAnnotationWriter provides XML writing functionality to 
// vtkSideAnnotation.
// .SECTION See Also
// vtkXMLSideAnnotationReader

#ifndef __vtkXMLSideAnnotationWriter_h
#define __vtkXMLSideAnnotationWriter_h

#include "XML/vtkXMLActor2DWriter.h"

class VTK_EXPORT vtkXMLSideAnnotationWriter : public vtkXMLActor2DWriter
{
public:
  static vtkXMLSideAnnotationWriter* New();
  vtkTypeRevisionMacro(vtkXMLSideAnnotationWriter,vtkXMLActor2DWriter);

  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLSideAnnotationWriter() {};
  ~vtkXMLSideAnnotationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLSideAnnotationWriter(const vtkXMLSideAnnotationWriter&);  // Not implemented.
  void operator=(const vtkXMLSideAnnotationWriter&);  // Not implemented.
};

#endif

