/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLMedicalImagePropertiesReader.h"

#include "vtkObjectFactory.h"
#include "vtkMedicalImageProperties.h"
#include "vtkXMLDataElement.h"

#include "XML/vtkXMLMedicalImagePropertiesWriter.h"

#include <vtksys/SystemTools.hxx>

vtkStandardNewMacro(vtkXMLMedicalImagePropertiesReader);
vtkCxxRevisionMacro(vtkXMLMedicalImagePropertiesReader, "$Revision: 1.4 $");

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesReader::GetRootElementName()
{
  return "MedicalImageProperties";
}

//----------------------------------------------------------------------------
int vtkXMLMedicalImagePropertiesReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkMedicalImageProperties *obj = vtkMedicalImageProperties::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The MedicalImageProperties is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer6[6];
  int i;
  const char *cptr;

  cptr = elem->GetAttribute("PatientName");
  if (cptr)
    {
    obj->SetPatientName(cptr);
    }

  cptr = elem->GetAttribute("PatientID");
  if (cptr)
    {
    obj->SetPatientID(cptr);
    }

  cptr = elem->GetAttribute("PatientAge");
  if (cptr)
    {
    obj->SetPatientAge(cptr);
    }

  cptr = elem->GetAttribute("PatientSex");
  if (cptr)
    {
    obj->SetPatientSex(cptr);
    }

  cptr = elem->GetAttribute("PatientBirthDate");
  if (cptr)
    {
    obj->SetPatientBirthDate(cptr);
    }

  cptr = elem->GetAttribute("StudyDate");
  if (cptr)
    {
    obj->SetStudyDate(cptr);
    }

  cptr = elem->GetAttribute("AcquisitionDate");
  if (cptr)
    {
    obj->SetAcquisitionDate(cptr);
    }

  cptr = elem->GetAttribute("StudyTime");
  if (cptr)
    {
    obj->SetStudyTime(cptr);
    }

  cptr = elem->GetAttribute("AcquisitionTime");
  if (cptr)
    {
    obj->SetAcquisitionTime(cptr);
    }

  cptr = elem->GetAttribute("ImageDate");
  if (cptr)
    {
    obj->SetImageDate(cptr);
    }

  cptr = elem->GetAttribute("ImageTime");
  if (cptr)
    {
    obj->SetImageTime(cptr);
    }

  cptr = elem->GetAttribute("ImageNumber");
  if (cptr)
    {
    obj->SetImageNumber(cptr);
    }

  cptr = elem->GetAttribute("SeriesNumber");
  if (cptr)
    {
    obj->SetSeriesNumber(cptr);
    }

  cptr = elem->GetAttribute("SeriesDescription");
  if (cptr)
    {
    obj->SetSeriesDescription(cptr);
    }

  cptr = elem->GetAttribute("StudyID");
  if (cptr)
    {
    obj->SetStudyID(cptr);
    }

  cptr = elem->GetAttribute("StudyDescription");
  if (cptr)
    {
    obj->SetStudyDescription(cptr);
    }

  cptr = elem->GetAttribute("Modality");
  if (cptr)
    {
    obj->SetModality(cptr);
    }

  cptr = elem->GetAttribute("Manufacturer");
  if (cptr)
    {
    obj->SetManufacturer(cptr);
    }

  cptr = elem->GetAttribute("ManufacturerModelName");
  if (cptr)
    {
    obj->SetManufacturerModelName(cptr);
    }

  cptr = elem->GetAttribute("StationName");
  if (cptr)
    {
    obj->SetStationName(cptr);
    }

  cptr = elem->GetAttribute("InstitutionName");
  if (cptr)
    {
    obj->SetInstitutionName(cptr);
    }

  cptr = elem->GetAttribute("ConvolutionKernel");
  if (cptr)
    {
    obj->SetConvolutionKernel(cptr);
    }

  cptr = elem->GetAttribute("SliceThickness");
  if (cptr)
    {
    obj->SetSliceThickness(cptr);
    }

  cptr = elem->GetAttribute("KVP");
  if (cptr)
    {
    obj->SetKVP(cptr);
    }

  cptr = elem->GetAttribute("GantryTilt");
  if (cptr)
    {
    obj->SetGantryTilt(cptr);
    }

  cptr = elem->GetAttribute("EchoTime");
  if (cptr)
    {
    obj->SetEchoTime(cptr);
    }

  cptr = elem->GetAttribute("EchoTrainLength");
  if (cptr)
    {
    obj->SetEchoTrainLength(cptr);
    }

  cptr = elem->GetAttribute("RepetitionTime");
  if (cptr)
    {
    obj->SetRepetitionTime(cptr);
    }

  cptr = elem->GetAttribute("ExposureTime");
  if (cptr)
    {
    obj->SetExposureTime(cptr);
    }

  cptr = elem->GetAttribute("XRayTubeCurrent");
  if (cptr)
    {
    obj->SetXRayTubeCurrent(cptr);
    }

  cptr = elem->GetAttribute("Exposure");
  if (cptr)
    {
    obj->SetExposure(cptr);
    }

  if (elem->GetVectorAttribute("DirectionCosine", 6, dbuffer6) == 6)
    {
    obj->SetDirectionCosine(dbuffer6);
    }

  // User Defined Values

  obj->RemoveAllUserDefinedValues();

  vtkXMLDataElement *user_defined_elems = elem->FindNestedElementWithName(
    vtkXMLMedicalImagePropertiesWriter::GetUserDefinedValuesElementName());
  if (user_defined_elems)
    {
    int nb_nested_elems = user_defined_elems->GetNumberOfNestedElements();
    for (i = 0; i < nb_nested_elems; i)
      {
      vtkXMLDataElement *user_defined_elem = 
        user_defined_elems->GetNestedElement(i);
      if (user_defined_elem && 
          !strcmp(
            user_defined_elem->GetName(), 
            vtkXMLMedicalImagePropertiesWriter::GetUserDefinedValueElementName()))
        {
        const char *name = user_defined_elem->GetAttribute("Name");
        const char *value = user_defined_elem->GetAttribute("Value");
        if (name && value)
          {
          obj->AddUserDefinedValue(name, value);
          }
        }
      }
    }

  // W/L presets

  obj->RemoveAllWindowLevelPresets();

  vtkXMLDataElement *wl_preset_elems = elem->FindNestedElementWithName(
    vtkXMLMedicalImagePropertiesWriter::GetWindowLevelPresetsElementName());
  if (wl_preset_elems)
    {
    int nb_nested_elems = wl_preset_elems->GetNumberOfNestedElements();
    int nb_presets = 0;
    for (i = 0; i < nb_nested_elems; ++i)
      {
      vtkXMLDataElement *wl_preset_elem = 
        wl_preset_elems->GetNestedElement(i);
      if (wl_preset_elem && 
          !strcmp(
            wl_preset_elem->GetName(), 
            vtkXMLMedicalImagePropertiesWriter::GetWindowLevelPresetElementName()))
        {
        double w, l;
        if (wl_preset_elem->GetScalarAttribute("Window", w) &&
            wl_preset_elem->GetScalarAttribute("Level", l))
          {
          obj->AddWindowLevelPreset(w, l);
          const char *comment = wl_preset_elem->GetAttribute("Comment");
          if (comment)
            {
            obj->SetNthWindowLevelPresetComment(nb_presets, comment);
            }
          ++nb_presets;
          }
        }
      }
    }

  return 1;
}
