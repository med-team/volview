/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWAngleWidgetWriter - vtkKWAngleWidget XML Writer.
// .SECTION Description
// vtkXMLKWAngleWidgetWriter provides XML writing functionality to 
// vtkKWAngleWidget.
// .SECTION See Also
// vtkXMLKWAngleWidgetReader

#ifndef __vtkXMLKWAngleWidgetWriter_h
#define __vtkXMLKWAngleWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLKWAngleWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLKWAngleWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWAngleWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLKWAngleWidgetWriter() {};
  ~vtkXMLKWAngleWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWAngleWidgetWriter(const vtkXMLKWAngleWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWAngleWidgetWriter&);  // Not implemented.
};

#endif



