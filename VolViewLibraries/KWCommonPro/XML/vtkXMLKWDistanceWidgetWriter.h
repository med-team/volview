/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWDistanceWidgetWriter - vtkKWDistanceWidget XML Writer.
// .SECTION Description
// vtkXMLKWDistanceWidgetWriter provides XML writing functionality to 
// vtkKWDistanceWidget.
// .SECTION See Also
// vtkXMLKWDistanceWidgetReader

#ifndef __vtkXMLKWDistanceWidgetWriter_h
#define __vtkXMLKWDistanceWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLKWDistanceWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLKWDistanceWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWDistanceWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLKWDistanceWidgetWriter() {};
  ~vtkXMLKWDistanceWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWDistanceWidgetWriter(const vtkXMLKWDistanceWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWDistanceWidgetWriter&);  // Not implemented.
};

#endif



