/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAbstractWidgetWriter - vtkAbstractWidget XML Writer.
// .SECTION Description
// vtkXMLAbstractWidgetWriter provides XML writing functionality to 
// vtkAbstractWidget.
// .SECTION See Also
// vtkXMLAbstractWidgetReader

#ifndef __vtkXMLAbstractWidgetWriter_h
#define __vtkXMLAbstractWidgetWriter_h

#include "XML/vtkXMLInteractorObserverWriter.h"

class VTK_EXPORT vtkXMLAbstractWidgetWriter : public vtkXMLInteractorObserverWriter
{
public:
  static vtkXMLAbstractWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLAbstractWidgetWriter,vtkXMLInteractorObserverWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLAbstractWidgetWriter() {};
  ~vtkXMLAbstractWidgetWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

private:
  vtkXMLAbstractWidgetWriter(const vtkXMLAbstractWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLAbstractWidgetWriter&);  // Not implemented.
};

#endif



