/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLKWDistanceWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkKWDistanceWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkDistanceRepresentation.h"

#include "XML/vtkXMLDistanceRepresentationReader.h"
#include "XML/vtkXMLKWDistanceWidgetWriter.h"

vtkStandardNewMacro(vtkXMLKWDistanceWidgetReader);
vtkCxxRevisionMacro(vtkXMLKWDistanceWidgetReader, "$Revision: 1.6 $");

//----------------------------------------------------------------------------
const char* vtkXMLKWDistanceWidgetReader::GetRootElementName()
{
  return "DistanceWidget";
}

//----------------------------------------------------------------------------
int vtkXMLKWDistanceWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkKWDistanceWidget *obj = vtkKWDistanceWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The KWDistanceWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  // Representation

  vtkDistanceRepresentation *rep = 
    vtkDistanceRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLDistanceRepresentationReader *xmlr = 
      vtkXMLDistanceRepresentationReader::New();
    xmlr->SetObject(rep);
    xmlr->ParseInNestedElement(
      elem, vtkXMLKWDistanceWidgetWriter::GetRepresentationElementName());
    xmlr->Delete();
    obj->WidgetIsDefined();
    }

  return 1;
}
