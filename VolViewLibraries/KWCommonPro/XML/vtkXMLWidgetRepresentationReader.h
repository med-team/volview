/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLWidgetRepresentationReader - vtkWidgetRepresentation XML Reader.
// .SECTION Description
// vtkXMLWidgetRepresentationReader provides XML reading functionality to 
// vtkWidgetRepresentation.
// .SECTION See Also
// vtkXMLWidgetRepresentationWriter

#ifndef __vtkXMLWidgetRepresentationReader_h
#define __vtkXMLWidgetRepresentationReader_h

#include "XML/vtkXMLPropReader.h"

class VTK_EXPORT vtkXMLWidgetRepresentationReader : public vtkXMLPropReader
{
public:
  static vtkXMLWidgetRepresentationReader* New();
  vtkTypeRevisionMacro(vtkXMLWidgetRepresentationReader, vtkXMLPropReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLWidgetRepresentationReader() {};
  ~vtkXMLWidgetRepresentationReader() {};

private:
  vtkXMLWidgetRepresentationReader(const vtkXMLWidgetRepresentationReader&); // Not implemented
  void operator=(const vtkXMLWidgetRepresentationReader&); // Not implemented    
};

#endif



