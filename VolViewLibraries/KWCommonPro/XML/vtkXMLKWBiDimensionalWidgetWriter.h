/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLKWBiDimensionalWidgetWriter - vtkKWBiDimensionalWidget XML Writer.
// .SECTION Description
// vtkXMLKWBiDimensionalWidgetWriter provides XML writing functionality to 
// vtkKWBiDimensionalWidget.
// .SECTION See Also
// vtkXMLKWBiDimensionalWidgetReader

#ifndef __vtkXMLKWBiDimensionalWidgetWriter_h
#define __vtkXMLKWBiDimensionalWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLKWBiDimensionalWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLKWBiDimensionalWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLKWBiDimensionalWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLKWBiDimensionalWidgetWriter() {};
  ~vtkXMLKWBiDimensionalWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLKWBiDimensionalWidgetWriter(const vtkXMLKWBiDimensionalWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLKWBiDimensionalWidgetWriter&);  // Not implemented.
};

#endif



