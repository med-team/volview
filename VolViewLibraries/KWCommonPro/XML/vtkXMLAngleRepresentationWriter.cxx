/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAngleRepresentationWriter.h"

#include "vtkObjectFactory.h"
#include "vtkAngleRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkAngleRepresentation2D.h"
#include "vtkLeaderActor2D.h"
#include "vtkProperty2D.h"

vtkStandardNewMacro(vtkXMLAngleRepresentationWriter);
vtkCxxRevisionMacro(vtkXMLAngleRepresentationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLAngleRepresentationWriter::GetRootElementName()
{
  return "AngleRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLAngleRepresentationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkAngleRepresentation *obj = 
    vtkAngleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AngleRepresentation is not set!");
    return 0;
    }

  double pos[3];

  obj->GetPoint1WorldPosition(pos);
  elem->SetVectorAttribute("Point1WorldPosition", 3, pos);

  obj->GetCenterWorldPosition(pos);
  elem->SetVectorAttribute("CenterWorldPosition", 3, pos);

  obj->GetPoint2WorldPosition(pos);
  elem->SetVectorAttribute("Point2WorldPosition", 3, pos);

  elem->SetIntAttribute("Tolerance", obj->GetTolerance());

  // elem->SetAttribute("LabelFormat", obj->GetLabelFormat());

  elem->SetIntAttribute("Ray1Visibility", obj->GetRay1Visibility());

  elem->SetIntAttribute("Ray2Visibility", obj->GetRay2Visibility());

  elem->SetIntAttribute("ArcVisibility", obj->GetArcVisibility());

  vtkAngleRepresentation2D *rep2d = 
    vtkAngleRepresentation2D::SafeDownCast(obj);
  if (rep2d)
    {
    elem->SetVectorAttribute(
      "Color", 3, rep2d->GetArc()->GetProperty()->GetColor());
    }

  return 1;
}

//----------------------------------------------------------------------------
int vtkXMLAngleRepresentationWriter::AddNestedElements(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddNestedElements(elem))
    {
    return 0;
    }

  vtkAngleRepresentation *obj = 
    vtkAngleRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AngleRepresentation is not set!");
    return 0;
    }

  /*
  // Too complex, just to store the color...
  // Lead actors

  */

  return 1;
}


