/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLMedicalImagePropertiesWriter.h"

#include "vtkMedicalImageProperties.h"
#include "vtkObjectFactory.h"
#include "vtkMedicalImageProperties.h"
#include "vtkXMLDataElement.h"

#include <vtksys/SystemTools.hxx>

vtkStandardNewMacro(vtkXMLMedicalImagePropertiesWriter);
vtkCxxRevisionMacro(vtkXMLMedicalImagePropertiesWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesWriter::GetRootElementName()
{
  return "MedicalImageProperties";
}

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesWriter::GetUserDefinedValuesElementName()
{
  return "UserDefinedValues";
}

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesWriter::GetUserDefinedValueElementName()
{
  return "UserDefinedValue";
}

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesWriter::GetWindowLevelPresetsElementName()
{
  return "WindowLevelPresets";
}

//----------------------------------------------------------------------------
const char* vtkXMLMedicalImagePropertiesWriter::GetWindowLevelPresetElementName()
{
  return "WindowLevelPreset";
}

//----------------------------------------------------------------------------
int vtkXMLMedicalImagePropertiesWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkMedicalImageProperties *obj = vtkMedicalImageProperties::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The MedicalImageProperties is not set!");
    return 0;
    }

  int i;

  elem->SetAttribute("PatientName", obj->GetPatientName());
  elem->SetAttribute("PatientID", obj->GetPatientID());
  elem->SetAttribute("PatientAge", obj->GetPatientAge());
  elem->SetAttribute("PatientSex", obj->GetPatientSex());
  elem->SetAttribute("PatientBirthDate", obj->GetPatientBirthDate());
  elem->SetAttribute("StudyDate", obj->GetStudyDate());
  elem->SetAttribute("AcquisitionDate", obj->GetAcquisitionDate());
  elem->SetAttribute("StudyTime", obj->GetStudyTime());
  elem->SetAttribute("AcquisitionTime", obj->GetAcquisitionTime());
  elem->SetAttribute("ImageDate", obj->GetImageDate());
  elem->SetAttribute("ImageTime", obj->GetImageTime());
  elem->SetAttribute("ImageNumber", obj->GetImageNumber());
  elem->SetAttribute("SeriesNumber", obj->GetSeriesNumber());
  elem->SetAttribute("SeriesDescription", obj->GetSeriesDescription());
  elem->SetAttribute("StudyID", obj->GetStudyID());
  elem->SetAttribute("StudyDescription", obj->GetStudyDescription());
  elem->SetAttribute("Modality", obj->GetModality());
  elem->SetAttribute("Manufacturer", obj->GetManufacturer());
  elem->SetAttribute("ManufacturerModelName", obj->GetManufacturerModelName());
  elem->SetAttribute("StationName", obj->GetStationName());
  elem->SetAttribute("InstitutionName", obj->GetInstitutionName());
  elem->SetAttribute("ConvolutionKernel", obj->GetConvolutionKernel());
  elem->SetAttribute("SliceThickness", obj->GetSliceThickness());
  elem->SetAttribute("KVP", obj->GetKVP());
  elem->SetAttribute("GantryTilt", obj->GetGantryTilt());
  elem->SetAttribute("EchoTime", obj->GetEchoTime());
  elem->SetAttribute("EchoTrainLength", obj->GetEchoTrainLength());
  elem->SetAttribute("RepetitionTime", obj->GetRepetitionTime());
  elem->SetAttribute("ExposureTime", obj->GetExposureTime());
  elem->SetAttribute("XRayTubeCurrent", obj->GetXRayTubeCurrent());
  elem->SetAttribute("Exposure", obj->GetExposure());

  elem->SetVectorAttribute("DirectionCosine", 6, obj->GetDirectionCosine());

  // User Defined Values

  int nb_user_defined = obj->GetNumberOfUserDefinedValues();
  if (nb_user_defined)
    {
    vtkXMLDataElement *user_defined_elems = this->NewDataElement();
    user_defined_elems->SetName(this->GetUserDefinedValuesElementName());
    elem->AddNestedElement(user_defined_elems);
    user_defined_elems->Delete();
    for (i = 0; i < nb_user_defined; i++)
      {
      vtkXMLDataElement *user_defined_elem = this->NewDataElement();
      user_defined_elem->SetName(this->GetUserDefinedValueElementName());
      user_defined_elems->AddNestedElement(user_defined_elem);
      user_defined_elem->Delete();
      user_defined_elem->SetAttribute(
        "Name", obj->GetUserDefinedNameByIndex(i));
      user_defined_elem->SetAttribute(
        "Value", obj->GetUserDefinedValueByIndex(i));
      }
    }

  // W/L presets

  int nb_wl_preset = obj->GetNumberOfWindowLevelPresets();
  if (nb_wl_preset)
    {
    vtkXMLDataElement *wl_preset_elems = this->NewDataElement();
    wl_preset_elems->SetName(this->GetWindowLevelPresetsElementName());
    elem->AddNestedElement(wl_preset_elems);
    wl_preset_elems->Delete();
    for (i = 0; i < nb_wl_preset; i++)
      {
      vtkXMLDataElement *wl_preset_elem = this->NewDataElement();
      wl_preset_elem->SetName(this->GetWindowLevelPresetElementName());
      wl_preset_elems->AddNestedElement(wl_preset_elem);
      wl_preset_elem->Delete();
      double w, l;
      if (obj->GetNthWindowLevelPreset(i, &w, &l))
        {
        wl_preset_elem->SetDoubleAttribute("Window", w);
        wl_preset_elem->SetDoubleAttribute("Level", l);
        wl_preset_elem->SetAttribute(
          "Comment", obj->GetNthWindowLevelPresetComment(i));
        }
      }
    }
  
  return 1;
}

