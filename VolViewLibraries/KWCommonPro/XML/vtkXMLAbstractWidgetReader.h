/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAbstractWidgetReader - vtkAbstractWidget XML Reader.
// .SECTION Description
// vtkXMLAbstractWidgetReader provides XML reading functionality to 
// vtkAbstractWidget.
// .SECTION See Also
// vtkXMLAbstractWidgetWriter

#ifndef __vtkXMLAbstractWidgetReader_h
#define __vtkXMLAbstractWidgetReader_h

#include "XML/vtkXMLInteractorObserverReader.h"

class VTK_EXPORT vtkXMLAbstractWidgetReader : public vtkXMLInteractorObserverReader
{
public:
  static vtkXMLAbstractWidgetReader* New();
  vtkTypeRevisionMacro(vtkXMLAbstractWidgetReader, vtkXMLInteractorObserverReader);

  // Description:
  // Parse an XML tree.
  // Return 1 on success, 0 on error.
  virtual int Parse(vtkXMLDataElement*);

  // Description:
  // Return the name of the root element of the XML tree this reader
  // is supposed to read and process.
  virtual const char* GetRootElementName();

protected:  
  vtkXMLAbstractWidgetReader() {};
  ~vtkXMLAbstractWidgetReader() {};

private:
  vtkXMLAbstractWidgetReader(const vtkXMLAbstractWidgetReader&); // Not implemented
  void operator=(const vtkXMLAbstractWidgetReader&); // Not implemented    
};

#endif



