/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLCaptionWidgetWriter - vtkCaptionWidget XML Writer.
// .SECTION Description
// vtkXMLCaptionWidgetWriter provides XML writing functionality to 
// vtkCaptionWidget.
// .SECTION See Also
// vtkXMLCaptionWidgetReader

#ifndef __vtkXMLCaptionWidgetWriter_h
#define __vtkXMLCaptionWidgetWriter_h

#include "XML/vtkXMLAbstractWidgetWriter.h"

class VTK_EXPORT vtkXMLCaptionWidgetWriter : public vtkXMLAbstractWidgetWriter
{
public:
  static vtkXMLCaptionWidgetWriter* New();
  vtkTypeRevisionMacro(vtkXMLCaptionWidgetWriter,vtkXMLAbstractWidgetWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the representation.
  static const char* GetRepresentationElementName();

protected:
  vtkXMLCaptionWidgetWriter() {};
  ~vtkXMLCaptionWidgetWriter() {};  
  
  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLCaptionWidgetWriter(const vtkXMLCaptionWidgetWriter&);  // Not implemented.
  void operator=(const vtkXMLCaptionWidgetWriter&);  // Not implemented.
};

#endif



