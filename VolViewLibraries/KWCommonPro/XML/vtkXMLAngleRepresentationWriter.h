/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLAngleRepresentationWriter - vtkAngleRepresentation XML Writer.
// .SECTION Description
// vtkXMLAngleRepresentationWriter provides XML writing functionality to 
// vtkAngleRepresentation.
// .SECTION See Also
// vtkXMLAngleRepresentationReader

#ifndef __vtkXMLAngleRepresentationWriter_h
#define __vtkXMLAngleRepresentationWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLAngleRepresentationWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLAngleRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLAngleRepresentationWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

protected:
  vtkXMLAngleRepresentationWriter() {};
  ~vtkXMLAngleRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLAngleRepresentationWriter(const vtkXMLAngleRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLAngleRepresentationWriter&);  // Not implemented.
};

#endif



