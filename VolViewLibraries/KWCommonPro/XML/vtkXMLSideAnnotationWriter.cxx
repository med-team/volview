/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLSideAnnotationWriter.h"

#include "vtkSideAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLSideAnnotationWriter);
vtkCxxRevisionMacro(vtkXMLSideAnnotationWriter, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLSideAnnotationWriter::GetRootElementName()
{
  return "SideAnnotation";
}

//----------------------------------------------------------------------------
int vtkXMLSideAnnotationWriter::AddAttributes(vtkXMLDataElement *elem)
{
  if (!this->Superclass::AddAttributes(elem))
    {
    return 0;
    }

  vtkSideAnnotation *obj = vtkSideAnnotation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The SideAnnotation is not set!");
    return 0;
    }

  elem->SetAttribute("MinusXLabel", obj->GetMinusXLabel());

  elem->SetAttribute("XLabel", obj->GetXLabel());

  elem->SetAttribute("MinusYLabel", obj->GetMinusYLabel());

  elem->SetAttribute("YLabel", obj->GetYLabel());

  return 1;
}

