/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLAbstractWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkAbstractWidget.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLAbstractWidgetReader);
vtkCxxRevisionMacro(vtkXMLAbstractWidgetReader, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLAbstractWidgetReader::GetRootElementName()
{
  return "AbstractWidget";
}

//----------------------------------------------------------------------------
int vtkXMLAbstractWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkAbstractWidget *obj = vtkAbstractWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The AbstractWidget is not set!");
    return 0;
    }

  // Get attributes

  int ival;

  if (elem->GetScalarAttribute("ProcessEvents", ival))
    {
    obj->SetProcessEvents(ival);
    }

  if (elem->GetScalarAttribute("ManagesCursor", ival))
    {
    obj->SetManagesCursor(ival);
    }

  return 1;
}



