/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLCaptionWidgetReader.h"

#include "vtkObjectFactory.h"
#include "vtkCaptionWidget.h"
#include "vtkXMLDataElement.h"
#include "vtkCaptionRepresentation.h"

#include "XML/vtkXMLCaptionRepresentationReader.h"
#include "XML/vtkXMLCaptionWidgetWriter.h"

vtkStandardNewMacro(vtkXMLCaptionWidgetReader);
vtkCxxRevisionMacro(vtkXMLCaptionWidgetReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLCaptionWidgetReader::GetRootElementName()
{
  return "CaptionWidget";
}

//----------------------------------------------------------------------------
int vtkXMLCaptionWidgetReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkCaptionWidget *obj = vtkCaptionWidget::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The CaptionWidget is not set!");
    return 0;
    }

  // Get nested elements
  
  // Representation

  vtkCaptionRepresentation *rep = 
    vtkCaptionRepresentation::SafeDownCast(obj->GetRepresentation());
  if (rep)
    {
    vtkXMLCaptionRepresentationReader *xmlr = 
      vtkXMLCaptionRepresentationReader::New();
    xmlr->SetObject(rep);
    xmlr->ParseInNestedElement(
      elem, vtkXMLCaptionWidgetWriter::GetRepresentationElementName());
    xmlr->Delete();
    }

  return 1;
}
