/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLContourRepresentationReader.h"

#include "vtkObjectFactory.h"
#include "vtkContourRepresentation.h"
#include "vtkXMLDataElement.h"
#include "vtkProperty.h"
#include "vtkProperty2D.h"
#include "vtkOrientedGlyphContourRepresentation.h"
#include "vtkOrientedGlyphFocalPlaneContourRepresentation.h"

#include "XML/vtkXMLContourRepresentationWriter.h"

vtkStandardNewMacro(vtkXMLContourRepresentationReader);
vtkCxxRevisionMacro(vtkXMLContourRepresentationReader, "$Revision: 1.5 $");

//----------------------------------------------------------------------------
const char* vtkXMLContourRepresentationReader::GetRootElementName()
{
  return "ContourRepresentation";
}

//----------------------------------------------------------------------------
int vtkXMLContourRepresentationReader::Parse(vtkXMLDataElement *elem)
{
  if (!this->Superclass::Parse(elem))
    {
    return 0;
    }

  vtkContourRepresentation *obj = 
    vtkContourRepresentation::SafeDownCast(this->Object);
  if (!obj)
    {
    vtkWarningMacro(<< "The ContourRepresentation is not set!");
    return 0;
    }

  // Get attributes

  double dbuffer3[3];
  double dval;
  int ival;

  if (elem->GetScalarAttribute("PixelTolerance", ival))
    {
    obj->SetPixelTolerance(ival);
    }

  if (elem->GetScalarAttribute("WorldTolerance", dval))
    {
    obj->SetWorldTolerance(dval);
    }
  
  if (elem->GetScalarAttribute("ClosedLoop", ival))
    {
    obj->SetClosedLoop(ival);
    }

  if (elem->GetVectorAttribute("Color", 3, dbuffer3) == 3)
    {
    vtkOrientedGlyphContourRepresentation *obj_og =
      vtkOrientedGlyphContourRepresentation::SafeDownCast(obj);
    if (obj_og)
      {
      obj_og->GetLinesProperty()->SetColor(dbuffer3);
      }
    else
      {
      vtkOrientedGlyphFocalPlaneContourRepresentation *obj_ogfp =
        vtkOrientedGlyphFocalPlaneContourRepresentation::SafeDownCast(obj);
      if (obj_ogfp)
        {
        obj_ogfp->GetLinesProperty()->SetColor(dbuffer3);
        }
      }
    }

  // Get nested elements

  while (obj->GetNumberOfNodes())
    {
    obj->DeleteNthNode(0);
    }

  int nb_nested_elems = elem->GetNumberOfNestedElements();
  for (int idx = 0; idx < nb_nested_elems; idx++)
    {
    vtkXMLDataElement *nested_elem = elem->GetNestedElement(idx);
    if (!strcmp(nested_elem->GetName(), 
                vtkXMLContourRepresentationWriter::GetNodeElementName()))
      {
      double pos[3], ori[9];
      if (nested_elem->GetVectorAttribute("WorldPosition", 3, pos) == 3 &&
          nested_elem->GetVectorAttribute("WorldOrientation", 9, ori) == 9)
        {
        obj->AddNodeAtWorldPosition(pos, ori);
        }
      }
    }

  return 1;
}
