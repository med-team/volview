/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "XML/vtkXMLBoundingBoxAnnotationReader.h"

#include "vtkBoundingBoxAnnotation.h"
#include "vtkObjectFactory.h"
#include "vtkProperty.h"
#include "vtkXMLDataElement.h"

vtkStandardNewMacro(vtkXMLBoundingBoxAnnotationReader);
vtkCxxRevisionMacro(vtkXMLBoundingBoxAnnotationReader, "$Revision: 1.3 $");

//----------------------------------------------------------------------------
const char* vtkXMLBoundingBoxAnnotationReader::GetRootElementName()
{
  return "BoundingBoxAnnotation";
}
