/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkXMLDistanceRepresentationWriter - vtkDistanceRepresentation XML Writer.
// .SECTION Description
// vtkXMLDistanceRepresentationWriter provides XML writing functionality to 
// vtkDistanceRepresentation.
// .SECTION See Also
// vtkXMLDistanceRepresentationReader

#ifndef __vtkXMLDistanceRepresentationWriter_h
#define __vtkXMLDistanceRepresentationWriter_h

#include "XML/vtkXMLWidgetRepresentationWriter.h"

class VTK_EXPORT vtkXMLDistanceRepresentationWriter : public vtkXMLWidgetRepresentationWriter
{
public:
  static vtkXMLDistanceRepresentationWriter* New();
  vtkTypeRevisionMacro(vtkXMLDistanceRepresentationWriter,vtkXMLWidgetRepresentationWriter);
  
  // Description:
  // Return the name of the root element of the XML tree this writer
  // is supposed to write.
  virtual const char* GetRootElementName();

  // Description:
  // Return the name of the element used inside that tree to
  // store the axis.
  static const char* GetAxisElementName();

protected:
  vtkXMLDistanceRepresentationWriter() {};
  ~vtkXMLDistanceRepresentationWriter() {};  
  
  // Description:
  // Add the root element attributes.
  // Return 1 on success, 0 otherwise.
  virtual int AddAttributes(vtkXMLDataElement *elem);

  // Description:
  // Add the root element internal/nested elements
  // Return 1 on success, 0 otherwise.
  virtual int AddNestedElements(vtkXMLDataElement *elem);

private:
  vtkXMLDistanceRepresentationWriter(const vtkXMLDistanceRepresentationWriter&);  // Not implemented.
  void operator=(const vtkXMLDistanceRepresentationWriter&);  // Not implemented.
};

#endif



