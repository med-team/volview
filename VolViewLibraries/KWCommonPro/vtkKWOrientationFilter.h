/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWOrientationFilter - reorients a volume or image
// .SECTION Description
// vtkKWOrientationFilter will perform an in place reorientaiton or 
// its input based on the OutputAxes setting

#ifndef __vtkKWOrientationFilter_h
#define __vtkKWOrientationFilter_h

#include "vtkImageInPlaceFilter.h"

class VTK_EXPORT vtkKWOrientationFilter : public vtkImageInPlaceFilter
{
public:
  static vtkKWOrientationFilter *New();
  vtkTypeRevisionMacro(vtkKWOrientationFilter,vtkImageInPlaceFilter);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Sets/Gets the mapping from input axes to output axes. For example
  // a setting of 1,0,2 would indicate the the output's i axis is the 
  // input's j axis. The output's j axis is the input's i axis and the k
  // axis remains unchanged. Values above 2 indicate that the axis is negated.
  // So the negative I,J,K axes would be 3,4,5.
  vtkSetVector3Macro(OutputAxes, int);
  vtkGetVector3Macro(OutputAxes, int);

protected:
  vtkKWOrientationFilter();
  ~vtkKWOrientationFilter() {};

  int OutputAxes[3];
  
  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);

  virtual int RequestInformation(vtkInformation* request,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector);

  virtual int RequestUpdateExtent(vtkInformation*,
                                  vtkInformationVector**,
                                  vtkInformationVector*);

  void ComputeInputUpdateExtent(int inExt[6], int outExt[6]);

private:
  vtkKWOrientationFilter(const vtkKWOrientationFilter&);  // Not implemented.
  void operator=(const vtkKWOrientationFilter&);  // Not implemented.
};



#endif




