/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWAngleWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkWidgetRepresentation.h"

vtkStandardNewMacro(vtkKWAngleWidget);
vtkCxxRevisionMacro(vtkKWAngleWidget, "$Revision: 1.5 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLKWAngleWidgetReader.h"
#include "XML/vtkXMLKWAngleWidgetWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkKWAngleWidget, vtkXMLKWAngleWidgetReader, vtkXMLKWAngleWidgetWriter);

//----------------------------------------------------------------------
void vtkKWAngleWidget::WidgetIsDefined()
{
  this->WidgetState = vtkAngleWidget::Manipulate;
  this->CurrentHandle = -1;
  this->ReleaseFocus();
  this->GetRepresentation()->BuildRepresentation(); // update this->Angle
  this->SetEnabled(this->GetEnabled()); // show/hide the handles properly
  this->Render();
}

//----------------------------------------------------------------------
int vtkKWAngleWidget::IsWidgetDefined()
{
  return this->WidgetState == vtkAngleWidget::Manipulate;
}

//----------------------------------------------------------------------
void vtkKWAngleWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
