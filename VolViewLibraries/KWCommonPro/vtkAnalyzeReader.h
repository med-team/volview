/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkAnalyzeReader - read an Analyze volume file.
// .SECTION Description
// vtkAnalyzeReader reads an Analyze file and creates a structured point dataset.
// The size of the volume and the data spacing is set from the Analyze file
// header.

#ifndef __vtkAnalyzeReader_h
#define __vtkAnalyzeReader_h

#include "vtkMedicalImageReader2.h"

class vtkAnalyzeInfo;

class VTK_EXPORT vtkAnalyzeReader : public vtkMedicalImageReader2
{
public:
  static vtkAnalyzeReader *New();
  vtkTypeRevisionMacro(vtkAnalyzeReader,vtkMedicalImageReader2);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Was the read successful ?
  vtkGetMacro(ReadSuccessful, int);

  
  // Description: is the given file name a png file?
  virtual int CanReadFile(const char* fname);
  // Description:
  // Get the file extensions for this format.
  // Returns a string with a space separated list of extensions in 
  // the format .extension
  virtual const char* GetFileExensions()
    {
      return ".hdr";
    }

  // Description: 
  // Return a descriptive name for the file format that might be useful in a GUI.
  virtual const char* GetDescriptiveName()
    {
      return "Analyze";
    }
  //Description: create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkAnalyzeReader::New(); }

  // Set/get methods to see if manual Origin/Spacing have
  // been set.
  vtkSetMacro( OriginSpecifiedFlag, bool );
  vtkGetMacro( OriginSpecifiedFlag, bool );
  vtkBooleanMacro( OriginSpecifiedFlag, bool );
  
  vtkSetMacro( SpacingSpecifiedFlag, bool );
  vtkGetMacro( SpacingSpecifiedFlag, bool );
  vtkBooleanMacro( SpacingSpecifiedFlag, bool );


protected:
  int ReadSuccessful;
  
  vtkAnalyzeReader();
  ~vtkAnalyzeReader();

  // Reads the file name and builds a vtkStructuredPoints dataset.
  virtual void ExecuteData(vtkDataObject*);

  // Not used now, but will be needed when this is made into an 
  // imaging filter.
  // Sets WholeExtent, Origin, Spacing, ScalarType 
  // and NumberOfComponents of output.
  void ExecuteInformation();
private:  
  vtkAnalyzeReader(const vtkAnalyzeReader&); // Not implemented
  void operator=(const vtkAnalyzeReader&); // Not implemented

  bool OriginSpecifiedFlag;
  bool SpacingSpecifiedFlag;

};

#endif



