/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkImageActorPointHandleRepresentation3D - represent the position of a point in 3D space
// .SECTION Description
// This class is used to represent a vtkHandleWidget. It represents a position
// in 3D world coordinates using a x-y-z cursor. The cursor can be configured to
// show a bounding box and/or shadows.

// .SECTION See Also
// vtkHandleRepresentation vtkHandleWidget vtkCursor3D


#ifndef __vtkImageActorPointHandleRepresentation3D_h
#define __vtkImageActorPointHandleRepresentation3D_h

#include "vtkPointHandleRepresentation3D.h"

class vtkImageActor;

class VTK_EXPORT vtkImageActorPointHandleRepresentation3D : public vtkPointHandleRepresentation3D
{
public:
  // Description:
  // Instantiate this class.
  static vtkImageActorPointHandleRepresentation3D *New();

  // Description:
  // Standard methods for instances of this class.
  vtkTypeRevisionMacro(vtkImageActorPointHandleRepresentation3D,vtkPointHandleRepresentation3D);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the image actor the widget will lie on.
  virtual void SetImageActor( vtkImageActor * );
  vtkGetObjectMacro( ImageActor, vtkImageActor );
  
  // Description:
  // Build the widget representation.
  virtual void BuildRepresentation();

  // Description:
  // Set whether the representation is to be displayed for all slices, or
  // only for the slice that is the closest to the 3D position of the 
  // representation. Defaults to OFF.
  virtual void SetDisplayForAllSlices(int);
  vtkGetMacro(DisplayForAllSlices, int);
  vtkBooleanMacro(DisplayForAllSlices, int);

protected:
  vtkImageActorPointHandleRepresentation3D();
  ~vtkImageActorPointHandleRepresentation3D();

  double         ImageActorDisplayExtent[6];
  vtkImageActor *ImageActor;
  int            DisplayForAllSlices;

private:
  vtkImageActorPointHandleRepresentation3D(const vtkImageActorPointHandleRepresentation3D&);  //Not implemented
  void operator=(const vtkImageActorPointHandleRepresentation3D&);  //Not implemented
};

#endif

