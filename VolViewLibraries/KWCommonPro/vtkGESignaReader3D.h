/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkGESignaReader3D - read GE Signa ximg files into a volume
// .SECTION Description
// A subclass of vtkGESignaReader that reads the slices into a volume.

//
// .SECTION See Also
// vtkGESignaReader

#ifndef __vtkGESignaReader3D_h
#define __vtkGESignaReader3D_h

#include "vtkGESignaReader.h"

class VTK_EXPORT vtkGESignaReader3D : public vtkGESignaReader
{
public:
  static vtkGESignaReader3D *New();
  vtkTypeRevisionMacro(vtkGESignaReader3D,vtkGESignaReader);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  //Description: create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkGESignaReader3D::New();}

  void ComputeImageFileName( int slice );
  vtkGetStringMacro( ImageFileName );
  
  int   ImageExtent[2];

  int   OriginOffset;
  int   ImageIncrement[3];
  
protected:
  vtkGESignaReader3D();
  ~vtkGESignaReader3D() {};

  virtual void ExecuteInformation();
  virtual void ExecuteData(vtkDataObject *out);

  void ComputeImageExtent();
  
  char *ImageFileName;
  char *BaseImageFileName;
  int   ImageNumberOfDigits;
  char  ImageFormatString[16];

  int   ReadHeader( char *filename,
                    float TLHC[3], float TRHC[3],
                    float BRHC[3], float spacing[3],
                    int   size[2] );
  
private:
  vtkGESignaReader3D(const vtkGESignaReader3D&);  // Not implemented.
  void operator=(const vtkGESignaReader3D&);  // Not implemented.
};
#endif



