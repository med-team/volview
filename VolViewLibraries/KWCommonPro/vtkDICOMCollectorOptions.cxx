/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDICOMCollectorOptions.h"

#include "vtkObjectFactory.h"

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkDICOMCollectorOptions);
vtkCxxRevisionMacro(vtkDICOMCollectorOptions, "$Revision: 1.9 $");

//----------------------------------------------------------------------------
vtkDICOMCollectorOptions::vtkDICOMCollectorOptions()
{
  this->ForceSeriesType = UnknownSeries;
  this->ExploreDirectory = 1;
  this->SupportASCIICharacterSetOnly = 0;
  this->RequireSpatialInformation = 0;
  this->RequireSOPClassUID = 0;
  this->SkipProblematicFile = 1;
  this->SupportGantryTilt = 0;
  this->SupportMultipleSamplesPerPixel = 0;
}

//----------------------------------------------------------------------------
vtkDICOMCollectorOptions::~vtkDICOMCollectorOptions()
{
}

//----------------------------------------------------------------------------
void vtkDICOMCollectorOptions::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);

  os << "SupportGantryTilt:" <<
    (this->SupportGantryTilt ? "On" : "Off") << endl;
  os << "RequireSOPClassUID:" <<
    (this->RequireSOPClassUID ? "On" : "Off") << endl;
  os << "SupportASCIICharacterSetOnly:" <<
    (this->SupportASCIICharacterSetOnly ? "On" : "Off") << endl;
  os << "ForceSeriesType:" <<
    (this->ForceSeriesType ? "On" : "Off") << endl;
  os << "RequireSpatialInformation:" <<
    (this->RequireSpatialInformation ? "On" : "Off") << endl;
  os << "SupportMultipleSamplesPerPixel:" <<
    (this->SupportMultipleSamplesPerPixel ? "On" : "Off") << endl;
  os << "SkipProblematicFile:" <<
    (this->SkipProblematicFile ? "On" : "Off") << endl;
  os << "ExploreDirectory:" <<
    (this->ExploreDirectory ? "On" : "Off") << endl;
}

