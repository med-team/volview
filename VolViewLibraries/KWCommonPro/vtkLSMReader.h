/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkLSMReader - read ZEISS LSM files

// .SECTION Description
// vtkLSMReader is a source object that reads basic LSM files as specified
// 

#ifndef __vtkLSMReader_h
#define __vtkLSMReader_h

#include "vtkConfigure.h"

extern "C" {
#if ((VTK_MAJOR_VERSION <= 4) && (VTK_MINOR_VERSION <= 4))
#include "tiffio.h" // needed for TIFF
#else
#include "vtk_tiff.h" // needed for TIFF
#endif
}

#include "vtkImageReader2.h"

class VTK_EXPORT vtkLSMReader : public vtkImageReader2
{
public:
  static vtkLSMReader *New();
  vtkTypeRevisionMacro(vtkLSMReader,vtkImageReader2);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Is the given file an LSM file?
  int CanReadFile(const char* fname);

  // Description:
  // Return file extention as a string for the factory .pic
  virtual const char* GetFileExensions()
    {
      return ".lsm";
    }

  // Description: 
  // Return file description as a string for the factory LSM
  virtual const char* GetDescriptiveName()
    {
      return "Zeiss LSM";
    }
  //Description: 
  // create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkLSMReader::New(); }

  // Set/get methods to see if manual Origin/Spacing have
  // been set.
  vtkSetMacro( OriginSpecifiedFlag, bool );
  vtkGetMacro( OriginSpecifiedFlag, bool );
  vtkBooleanMacro( OriginSpecifiedFlag, bool );
  
  vtkSetMacro( SpacingSpecifiedFlag, bool );
  vtkGetMacro( SpacingSpecifiedFlag, bool );
  vtkBooleanMacro( SpacingSpecifiedFlag, bool );

protected:
  vtkLSMReader();
  ~vtkLSMReader();
  TIFF *Image;

  int Open(const char *);
  void Clean();
  
  virtual void ExecuteInformation();
  virtual void ExecuteData(vtkDataObject *out);

private:
  vtkLSMReader(const vtkLSMReader&); // Not implemented
  void operator=(const vtkLSMReader&); // Not implemented

  bool OriginSpecifiedFlag;
  bool SpacingSpecifiedFlag;
};

#endif



