/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkSideAnnotation - Create an axis with tick marks and labels
// .SECTION Description
// vtkSideAnnotation is an class that provides annotation for an image.
// Labels and tick marks are provided for the X and Y axes.

#ifndef __vtkSideAnnotation_h
#define __vtkSideAnnotation_h

#include "vtkCornerAnnotation.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class VTK_EXPORT vtkSideAnnotation : public vtkCornerAnnotation
{
public:
  static vtkSideAnnotation *New();
  vtkTypeRevisionMacro(vtkSideAnnotation, vtkCornerAnnotation);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX

  // Description:
  // Set the text for each side
  const char* GetMinusXLabel();
  void  SetMinusXLabel(const char *l);
  const char* GetXLabel();
  void  SetXLabel(const char *l);
  const char* GetMinusYLabel();
  void  SetMinusYLabel(const char *l);
  const char* GetYLabel();
  void  SetYLabel(const char *l);

protected:
  vtkSideAnnotation();
  ~vtkSideAnnotation() {};

  // Description:
  // Set text actor positions given a viewport size and justification
  virtual void SetTextActorsPosition(int vsize[2]);
  virtual void SetTextActorsJustification();

private:
  vtkSideAnnotation(const vtkSideAnnotation&); // Not implemented
  void operator=(const vtkSideAnnotation&); // Not implemented
};


#endif
