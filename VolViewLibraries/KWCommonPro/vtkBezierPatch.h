/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkBezierPatch - a 2D Bezier patch embedded in 3D
// .SECTION Description
//
// This class implements a 2D Bezier patch with nodes in 3D space.
// The patch is computed as using the Casteljau Algorithm as described
// in "Curves and Surfaces for CAGD" Gerald Farin, Third edition.
//

// .SECTION See Also
// vtkSpline


#ifndef __vtkBezierPatch_h
#define __vtkBezierPatch_h

#include "vtkObject.h"

class VTK_EXPORT vtkBezierPatch : public vtkObject
{
public:
  // Description:
  // Instantiate the object.
  static vtkBezierPatch *New();

  vtkTypeRevisionMacro(vtkBezierPatch,vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Set/Get the position of the spline handles. Call GetNumberOfHandlesU and
  // GetNumberOfHandlesV to determine the valid range of handle indices.
  void SetHandlePosition(vtkIdType id, double x, double y, double z);
  void SetHandlePosition(vtkIdType id, double xyz[3]);
  void GetHandlePosition(vtkIdType id, double xyz[3]);
  double* GetHandlePosition(vtkIdType id);

  // Description:
  // Evaluate the position of the Bezier patch at the parametric point (u,v)
  // The user must allocate the memory for "point". This method will simply
  // write at the position indicated by this pointer.
  void Evaluate( double u, double v, double * point );

protected:
  vtkBezierPatch();
  ~vtkBezierPatch();

  // an array of 3 x 10 doubles containing the
  // coordinates of all the control points.
  // A cubic bezier patch has 10 control points.
  // 10 is the triangle of 4.
  double Handles[30];  

private:
  vtkBezierPatch(const vtkBezierPatch&);  //Not implemented
  void operator=(const vtkBezierPatch&);  //Not implemented

//BTX
  // Performance of this function is critical. 
  // Therefore we try to make it inline
  inline void LinearInterpolation(double u, double v,double w, 
                    const double * p0, const double * p1, const double * p2,
                    double * result )
  {
    result[0] = u * p0[0] + v * p1[0] + w * p2[0];
    result[1] = u * p0[1] + v * p1[1] + w * p2[1];
    result[2] = u * p0[2] + v * p1[2] + w * p2[2];
  }
//ETX


};

#endif

