/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWContourWidget
// .SECTION Description

#ifndef __vtkKWContourWidget_h
#define __vtkKWContourWidget_h

#include "vtkContourWidget.h"
#include "XML/vtkXMLIOBaseMacros.h" // Needed for XML reader/writer macros

class VTK_EXPORT vtkKWContourWidget : public vtkContourWidget
{
public:
  static vtkKWContourWidget* New();
  vtkTypeRevisionMacro(vtkKWContourWidget, vtkContourWidget);
  void PrintSelf(ostream& os, vtkIndent indent);
  //BTX
  vtkKWGetXMLReaderWriterObjectsMacro();
  //ETX
  
  // Description:
  // Query/Set the state of the widget to "defined" (in case its representation
  // was created programmatically)
  virtual void WidgetIsDefined();
  virtual int IsWidgetDefined();

protected:
  vtkKWContourWidget() {};
  ~vtkKWContourWidget() {};
  
private:
  vtkKWContourWidget(const vtkKWContourWidget&);  // Not implemented
  void operator=(const vtkKWContourWidget&);  // Not implemented
};

#endif
