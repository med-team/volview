/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWColorImageConversionFilter - handles making sure an input
// volume is reasonable for volview 

// .SECTION 
// Description vtkKWColorImageConversionFilter will make sure an
// input volume is appropriate for volview in three regards. Convert double
// to float, convert rgb to rgba, rescale origin and spacing to be reasonable

#ifndef __vtkKWColorImageConversionFilter_h
#define __vtkKWColorImageConversionFilter_h

#include "vtkImageAlgorithm.h"

class VTK_EXPORT vtkKWColorImageConversionFilter : public vtkImageAlgorithm
{
public:
  static vtkKWColorImageConversionFilter *New();
  vtkTypeRevisionMacro(vtkKWColorImageConversionFilter,vtkImageAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set/Get the minimum value of the opacity value accepted in the RGB
  // to color conversion.  This allows us to limit the transparency of 
  // e.g. grayscale images encoded as three component RGB images ...
  vtkGetMacro(AlphaFloor, double);
  vtkSetClampMacro(AlphaFloor, double, 0.0, 1.0);

  // Description:
  // Get how the components are to be handled
  vtkGetMacro(IndependentComponents, int);
  vtkSetMacro(IndependentComponents, int);

  // Description: Get the conversions that will be applied. All results valid
  // after an UpdateInformation is called except for compressed ScalarRange
  // which is only set after the data is loaded)
  unsigned long GetConversions() 
    { return this->Conversions; }

  //BTX
  enum 
   {
     ShiftedOrigin = 0x01,
     ConvertedToColor = 0x02,
     ConvertedToFloat = 0x04,
     CompressedSpacing = 0x08,
     CompressedAspectRatio = 0x10,
     CompressedScalarRange = 0x20
   };
  //ETX

protected:
  vtkKWColorImageConversionFilter();
  ~vtkKWColorImageConversionFilter() {};

  int IndependentComponents;
  double AlphaFloor;

  void ComputeScaling(double totalRange[2], double &scale, double &shift);
  
  virtual int RequestData(vtkInformation* request,
                          vtkInformationVector** inputVector,
                          vtkInformationVector* outputVector);

  virtual int RequestInformation(vtkInformation* request,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector);

  // a flag indicating what had to be fixed in this data
  unsigned long Conversions;
  
  
private:
  vtkKWColorImageConversionFilter(const vtkKWColorImageConversionFilter&);  // Not implemented.
  void operator=(const vtkKWColorImageConversionFilter&);  // Not implemented.
};



#endif




