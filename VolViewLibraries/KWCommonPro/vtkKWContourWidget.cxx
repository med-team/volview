/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWContourWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkWidgetRepresentation.h"

vtkStandardNewMacro(vtkKWContourWidget);
vtkCxxRevisionMacro(vtkKWContourWidget, "$Revision: 1.6 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLKWContourWidgetReader.h"
#include "XML/vtkXMLKWContourWidgetWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkKWContourWidget, vtkXMLKWContourWidgetReader, vtkXMLKWContourWidgetWriter);

//----------------------------------------------------------------------
void vtkKWContourWidget::WidgetIsDefined()
{
  this->WidgetState = vtkContourWidget::Manipulate;
  this->ReleaseFocus();
  this->GetRepresentation()->BuildRepresentation(); // update this->Contour
  this->SetEnabled(this->GetEnabled()); // show/hide the handles properly
  this->Render();
}

//----------------------------------------------------------------------
int vtkKWContourWidget::IsWidgetDefined()
{
  return this->WidgetState == vtkContourWidget::Manipulate;
}

//----------------------------------------------------------------------
void vtkKWContourWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
