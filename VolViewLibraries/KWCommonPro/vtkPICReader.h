/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPICReader - read PIC files

// .SECTION Description
// vtkPICReader is a source object that reads basic PIC files as specified
// 



#ifndef __vtkPICReader_h
#define __vtkPICReader_h

#include "vtkImageReader.h"

class VTK_EXPORT vtkPICReader : public vtkImageReader
{
public:
  static vtkPICReader *New();
  vtkTypeRevisionMacro(vtkPICReader,vtkImageReader);
  void PrintSelf(ostream& os, vtkIndent indent);
  
  // Description:
  // Was there an error on the last read performed?
  vtkGetMacro(Error,int);

  // Description:
  // Is the given file an PIC file?
  int CanReadFile(const char* fname);

  // Description:
  // Return file extention as a string for the factory .pic
  virtual const char* GetFileExensions()
    {
      return ".pic";
    }

  // Description: 
  // Return file description as a string for the factory PIC
  virtual const char* GetDescriptiveName()
    {
      return "PIC";
    }
  //Description: 
  // create a clone of this object.
  virtual vtkImageReader2* MakeObject() { return vtkPICReader::New(); }

protected:
  vtkPICReader() {};
  ~vtkPICReader() {};

  void ExecuteInformation();

  int Error;
private:
  vtkPICReader(const vtkPICReader&); // Not implemented
  void operator=(const vtkPICReader&); // Not implemented
};

#endif



