/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkOpenGLSmoothPolyDataMapper.h"

#ifndef VTK_IMPLEMENT_MESA_CXX
#include "vtkObjectFactory.h"
#include "vtkOpenGL.h"
#endif

#include <math.h>


#ifndef VTK_IMPLEMENT_MESA_CXX
vtkCxxRevisionMacro(vtkOpenGLSmoothPolyDataMapper, "$Revision: 1.4 $");
vtkStandardNewMacro(vtkOpenGLSmoothPolyDataMapper);
#endif
      
// Construct empty object.
vtkOpenGLSmoothPolyDataMapper::vtkOpenGLSmoothPolyDataMapper()
{
}

// Destructor (don't call ReleaseGraphicsResources() since it is virtual
vtkOpenGLSmoothPolyDataMapper::~vtkOpenGLSmoothPolyDataMapper()
{
}

void vtkOpenGLSmoothPolyDataMapper::Render(vtkRenderer *ren, vtkActor *act)
{
  glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
  glEnable(GL_LINE_SMOOTH);
  glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
  // Not enabling polygon smoothing because it leads to 
  // internal lines
  //glEnable(GL_POLYGON_SMOOTH);
  //glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
  this->Superclass::Render(ren,act);
  glPopAttrib();
}

void vtkOpenGLSmoothPolyDataMapper::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}
 
