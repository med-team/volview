/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkKWDistanceWidget.h"
#include "vtkObjectFactory.h"
#include "vtkCallbackCommand.h"
#include "vtkWidgetRepresentation.h"

vtkStandardNewMacro(vtkKWDistanceWidget);
vtkCxxRevisionMacro(vtkKWDistanceWidget, "$Revision: 1.8 $");

#ifdef KWCommonPro_USE_XML_RW
#include "XML/vtkXMLKWDistanceWidgetReader.h"
#include "XML/vtkXMLKWDistanceWidgetWriter.h"
#endif
vtkKWGetXMLReaderWriterObjectsImplementationMacro(vtkKWDistanceWidget, vtkXMLKWDistanceWidgetReader, vtkXMLKWDistanceWidgetWriter);

//----------------------------------------------------------------------
void vtkKWDistanceWidget::WidgetIsDefined()
{
  this->WidgetState = vtkDistanceWidget::Manipulate;
  this->CurrentHandle = -1;
  this->ReleaseFocus();
  this->GetRepresentation()->BuildRepresentation(); // update this->Distance
  this->SetEnabled(this->GetEnabled()); // show/hide the handles properly
  this->Render();
}

//----------------------------------------------------------------------
int vtkKWDistanceWidget::IsWidgetDefined()
{
  return this->WidgetState == vtkDistanceWidget::Manipulate;
}

//----------------------------------------------------------------------
void vtkKWDistanceWidget::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
