/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkJPEG12Reader - read JPEG12 files
// .SECTION Description
// vtkJPEG12Reader is a source object that reads JPEG12 files.
// It should be able to read most any JPEG12 file
//
// .SECTION See Also
// vtkJPEG12Writer

#ifndef __vtkJPEG12Reader_h
#define __vtkJPEG12Reader_h

#include "vtkImageReader2.h"

class vtkUnsignedCharArray;

class VTK_EXPORT vtkJPEG12Reader : public vtkImageReader2
{
public:
  static vtkJPEG12Reader *New();
  vtkTypeRevisionMacro(vtkJPEG12Reader,vtkImageReader2);
  virtual void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Is the given file a JPEG12 file?
  int CanReadFile(const char* fname);

  // Description:
  // Get the file extensions for this format.
  // Returns a string with a space separated list of extensions in 
  // the format .extension
  virtual const char* GetFileExensions()
    {
      return ".jpeg .jpg";
    }

  // Description:
  // Return a descriptive name for the file format that might be useful in a
  // GUI.
  virtual const char* GetDescriptiveName()
    {
      return "JPEG12";
    }
  
  // Description:
  // Set a memory buffer to read from
  virtual void SetMemoryBuffer(vtkUnsignedCharArray *);
  vtkGetObjectMacro(MemoryBuffer,vtkUnsignedCharArray);
  
protected:
  vtkJPEG12Reader() {this->MemoryBuffer = 0;};
  ~vtkJPEG12Reader() {this->SetMemoryBuffer(0);};
  
  virtual void ExecuteInformation();
  virtual void ExecuteData(vtkDataObject *out);
  vtkUnsignedCharArray *MemoryBuffer;
  
private:
  vtkJPEG12Reader(const vtkJPEG12Reader&);  // Not implemented.
  void operator=(const vtkJPEG12Reader&);  // Not implemented.
};
#endif


