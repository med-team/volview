/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkPlanarPolyDataToImageStencil - rasterize a planar contour
// .SECTION Description
// Unlike the superclass vtkPolyDataToImageStencil, this assumes that the 
// contour is planar. Hence its able to achieve some optimizations, such
// as avoid cutting the dataset at every slice, by directly using the input
// polydata.
// .SECTION See Also

#ifndef __vtkPlanarPolyDataToImageStencil_h
#define __vtkPlanarPolyDataToImageStencil_h

#include "vtkPolyDataToImageStencil.h"

class VTK_EXPORT vtkPlanarPolyDataToImageStencil :
  public vtkPolyDataToImageStencil
{
public:
  static vtkPlanarPolyDataToImageStencil* New();
  vtkTypeMacro(vtkPlanarPolyDataToImageStencil, vtkPolyDataToImageStencil);
  void PrintSelf(ostream& os, vtkIndent indent);
  
protected:
  vtkPlanarPolyDataToImageStencil();
  ~vtkPlanarPolyDataToImageStencil();

  void PlanarThreadedExecute(vtkImageStencilData *output,
                       int extent[6], int threadId);

  virtual int RequestData(vtkInformation *, vtkInformationVector **,
                          vtkInformationVector *);

private:
  vtkPlanarPolyDataToImageStencil(const vtkPlanarPolyDataToImageStencil&);  // Not implemented.
  void operator=(const vtkPlanarPolyDataToImageStencil&);  // Not implemented.
};

#endif

