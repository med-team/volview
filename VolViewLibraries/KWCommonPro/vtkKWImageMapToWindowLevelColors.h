/*=========================================================================

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/VolViewCopyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkKWImageMapToWindowLevelColors - KW version that handles multi-component data
// .SECTION Description

#ifndef __vtkKWImageMapToWindowLevelColors_h
#define __vtkKWImageMapToWindowLevelColors_h

#include "vtkImageMapToWindowLevelColors.h"

class vtkColorTransferFunction;

class VTK_EXPORT vtkKWImageMapToWindowLevelColors : public vtkImageMapToWindowLevelColors
{
public:
  static vtkKWImageMapToWindowLevelColors *New();
  vtkTypeRevisionMacro(vtkKWImageMapToWindowLevelColors, vtkImageMapToWindowLevelColors);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the extent that all requests should map to. This is critical for the
  // lightbox to work correctly
  void SetMinimumUpdateExtent(int extent[6]);
  void SetMinimumUpdateExtent(int minX, int maxX, int minY, int maxY, 
                              int minZ, int maxZ);

  // Description:
  // Are the components of this data independent of each other?
  vtkSetClampMacro(IndependentComponents, int, 0, 1);
  vtkGetMacro(IndependentComponents, int);
  vtkBooleanMacro(IndependentComponents, int);
  
  // Description:
  // Should the opacity be used to modulate the displayed channels?
  vtkSetClampMacro(UseOpacityModulation, int, 0, 1);
  vtkGetMacro(UseOpacityModulation, int);
  vtkBooleanMacro(UseOpacityModulation, int);

  // Description: What channels of data should be displayed. In the case of
  // grayscale intensities, if th eimage is color it will be converted to
  // grayscale by using the average of the components.  
  //BTX
  enum
  {
    DISPLAY_INTENSITIES           = 0,
    DISPLAY_OPACITY               = 1,
    DISPLAY_POST_LUT_INTENSITIES  = 2,
    DISPLAY_GRAYSCALE_INTENSITIES = 3
  };
  //ETX
  vtkSetClampMacro(DisplayChannels, int, DISPLAY_INTENSITIES, 
                   DISPLAY_GRAYSCALE_INTENSITIES);
  vtkGetMacro(DisplayChannels, int);
  void SetDisplayChannelsToIntensities() {
    this->SetDisplayChannels(
      vtkKWImageMapToWindowLevelColors::DISPLAY_INTENSITIES); }
  void SetDisplayChannelsToOpacity() {
    this->SetDisplayChannels(
      vtkKWImageMapToWindowLevelColors::DISPLAY_OPACITY); }
  void SetDisplayChannelsToPostLookupTableIntensities() {
    this->SetDisplayChannels(
      vtkKWImageMapToWindowLevelColors::DISPLAY_POST_LUT_INTENSITIES); }
  void SetDisplayChannelsToGrayScaleIntensities() {
    this->SetDisplayChannels(
      vtkKWImageMapToWindowLevelColors::DISPLAY_GRAYSCALE_INTENSITIES); }

  // Description:
  // Are the two parameters a valid setting for the current input data ?
  // Get a valid combination given  the current input data.
  int IsValidCombination(int dispChannel, int useOpacityModulation);
  int GetValidCombination(int *dispChannel, int *useOpacityModulation);
  
  // Description:
  // Set/get the lookup table(s) to pass the data through.  If using
  // independent components, set a lookup table per component.
  virtual void SetLookupTable(vtkScalarsToColors *lut) 
    { this->SetLookupTable(0, lut); }
  virtual vtkScalarsToColors* GetLookupTable()
    { return this->GetLookupTable(0); }
  void SetLookupTable(int comp, vtkScalarsToColors *lut);
  vtkScalarsToColors* GetLookupTable(int comp);
  
  // Description:
  // If using multiple independent components, set the weight per component
  // for producing the final color.  The weights should sum to 1.
  void SetWeight(int comp, float weight);
  float GetWeight(int comp);
  
  // Description:
  // Get the MTime
  unsigned long GetMTime();
  
protected:
  vtkKWImageMapToWindowLevelColors();
  ~vtkKWImageMapToWindowLevelColors();

  virtual int RequestUpdateExtent (vtkInformation *, 
                                   vtkInformationVector **,
                                   vtkInformationVector *);
  virtual int RequestInformation(vtkInformation *request, 
                                 vtkInformationVector **inputVector, 
                                 vtkInformationVector *outputVector);

  void ThreadedRequestData(vtkInformation *request,
                           vtkInformationVector **inputVector,
                           vtkInformationVector *outputVector,
                           vtkImageData ***inData, vtkImageData **outData,
                           int extent[6], int id);
  virtual int RequestData(vtkInformation *request,
                          vtkInformationVector **inputVector,
                          vtkInformationVector *outputVector);
  
  int IndependentComponents;
  vtkColorTransferFunction *LookupTables[4];
  float Weights[4];
  int UseOpacityModulation;
  int DisplayChannels;

  int MinimumUpdateExtent[6];
  
private:
  vtkKWImageMapToWindowLevelColors(const vtkKWImageMapToWindowLevelColors&);  // Not implemented.
  void operator=(const vtkKWImageMapToWindowLevelColors&);  // Not implemented.
};

#endif








